'use strict';
const moment = require("moment");


// var success = '0';
// var successMessage = '';
// var errorMessage = '';
// var results = {};
 
// var statRes = 422;
// var jsonRes = {};


const HelperGeneral = require('./helpers/HelperGeneral.js');



var loginAccountLists= [];



var jazeCallMembers = [];

var user_id = [];
var user_logindetails = [];
var user_page = [];

var jCall = [];
var jConSchedule = [];


exports = module.exports = function(io){

 io.sockets.on('connection', function (socket) {
    

    // account login insert account array
    socket.on('setLoginAccount', function (data,callback) {

        if(parseInt(data.account_id) !== 0)
        {
            var indx =   getIndex(loginAccountLists,data.account_id);

             if(typeof(indx) !== "undefined"){ 
                 loginAccountLists[indx] = [data.account_id,data.fcm_tocken,data.active_page,moment().utc().format('YYYY-MM-DD HH:mm:ss'),'1'];
             }
             else{
                loginAccountLists.push([data.account_id,data.fcm_tocken,data.active_page,moment().utc().format('YYYY-MM-DD HH:mm:ss'),'1']); 
             }

            callback(true);
        }
        else
        { callback(false); }
    });


    //update fileds
    socket.on('updateAccountDetails', function (data) {

        if(parseInt(data.account_id) !== 0) {

            var indx =   getIndex(loginAccountLists,data.account_id);

            if(typeof(indx) !== "undefined"){ 

                if(typeof(data.fcm_tocken) !== "undefined" && data.fcm_tocken !== "" && data.fcm_tocken !== null){ 
                    loginAccountLists[indx][1] = data.fcm_tocken;
                }

                if(typeof(data.active_page) !== "undefined" && data.active_page !== "" && data.active_page !== null){ 
                    loginAccountLists[indx][2] = data.active_page;
                }

                loginAccountLists[indx][3] = moment().utc().format('YYYY-MM-DD HH:mm:ss');
            }
        }
    });

    //get Account Details
    socket.on('getAccountDetails', function (account_id,callback) {

        var indx =   getIndex(loginAccountLists,account_id);

        if(typeof(indx) !== "undefined"){ 
            callback(loginAccountLists[indx]);
        }
        else
        { callback(null); }       
    });

    // Logout Account *remove array row
    socket.on('setLogoutAccount', function (account_id) {
        if(parseInt(account_id) !== 0) {

            var indx =   getIndex(loginAccountLists,account_id);

             if(typeof(indx) !== "undefined"){ 
                loginAccountLists.splice(indx, 1);
             }           
        }
    });

    //------------------------------------------------------------------------------- Jazecall Module Start --------------------------------------------------------------------------------------------//
   
    // check Account is avilable
    socket.on('checkAccountJazecallStatus', function (account_id,callback) {

        //check account online or not
        var indx =   getIndex(loginAccountLists,account_id);

        if(typeof(indx) !== "undefined"){ 

            //check account already doing call
            if(jazeCallMembers.indexOf(account_id) != -1){
                callback('2');
            }
            else{
                callback('1');
            }
        }
        else
        { callback('0'); }
    });


    //make Jazecall
    socket.on('makeJazeCall', function (data) {

        if(typeof(data) !== "undefined" && data !== null){ 

            HelperGeneral.getAccountDetails(data,function (senderDetails){
                
                if(senderDetails !== null)
                {
                    var receiver_data = { account_id : parseInt(data.receiver_account_id) };

                    HelperGeneral.getAccountDetails(receiver_data,function (receiverDetails){

                        if(senderDetails !== null && receiverDetails !== null)
                        {
                            //push account in jazeCall Members array
                            jazeCallMembers.push(data.account_id);
                            
                            jazeCallMembers.push(data.receiver_account_id);

                            io.sockets.emit('jazeCallMemberList', jazeCallMembers);

                           //emit incoming jazeCall 
                           io.sockets.emit('jazeIncomingCall_'+receiverDetails.account_id+'', { receiver_account_id:receiverDetails.account_id, receiver_name : receiverDetails.name, receiver_logo : receiverDetails.logo, sender_account_id : senderDetails.account_id, sender_name : senderDetails.name, sender_logo : senderDetails.logo, jazecall_room_id : data.group_id, jazecall_type: data.call_type , call_status: data.status }); 

                         //  io.sockets.emit('jazeCallDetails_'+parseInt(data.group_id)+'', { receiver_account_id:receiverDetails.account_id, receiver_name : receiverDetails.name, receiver_logo : receiverDetails.logo, sender_account_id : senderDetails.account_id, sender_name : senderDetails.name, sender_logo : senderDetails.logo, jazecall_room_id : data.group_id, jazecall_type: data.call_type , call_status: data.status }); 
                        }
                    });
                }
             });
        }
    });

    //disconnect call
    socket.on('disconnectJazeCall', function (data) {

        if(typeof(data) !== "undefined" && data !== null){ 

             //remove account in jazeCall Members array
             jazeCallMembers.splice(jazeCallMembers.indexOf(data.account_id) , 1) ;
        
             jazeCallMembers.splice(jazeCallMembers.indexOf(data.receiver_account_id) , 1) ;

             io.sockets.emit('jazeCallMemberList', jazeCallMembers);

            //emit incoming jazeCall 
            io.sockets.emit('jazeIncomingCall_'+data.receiver_account_id, { jazecall_room_id : '0', call_status : '0' });

            io.sockets.emit('jazeIncomingCall_'+data.account_id, { jazecall_room_id : '0',  call_status : '0' });



           // socket.leave('jazeIncomingCall_'+data.receiver_account_id);

         //   socket.leave('jazeIncomingCall_'+data.account_id);

        //    socket.on.delete('jazeIncomingCall_'+data.account_id, stopHandler);
               
          //  socket.on.delete('jazeIncomingCall_'+data.receiver_account_id, stopHandler);
        }
    });

    function stopHandler (msg) {
        console.log(msg);
        clearInterval(refresh);
        socket.removeListener('jazeIncomingCall_'+data.receiver_account_id, stopHandler);
    }










    // OLD
    // insert jcall
    // socket.on('jcall_insert', function (data,callback) {
    
    // if(jCall.indexOf(data.company_user_id) == -1){
            
    //     jCall.push(data.company_user_id);
        
    //     jCall.push(data.user_id);
        
    //     updateJcallList();
        
    //     io.sockets.emit('jcall_notification_'+data.company_user_id, { status : data.status , user_id : data.user_id, user_name : data.company_name, user_logo : data.company_logo, meeting_id: data.meeting_id, chanal: data.chanal, from_call_type: data.from_call_type , to_call_type: data.to_call_type ,call_status: data.call_status }); 
        
    //     io.sockets.emit('jcall_details_'+data.meeting_id, { status : data.status, meeting_id: data.meeting_id, from_call_type: data.from_call_type , to_call_type: data.to_call_type ,call_status:  data.call_status });

    //     }
    // });



    // end jcall
    socket.on('jcall_end', function (data,callback) {
    if(data.user_id !== 0 && data.to_user_id !== 0)
    {
        jCall.splice(jCall.indexOf(data.user_id) , 1) ;
        
        jCall.splice(jCall.indexOf(data.to_user_id) , 1) ;
        
        updateJcallList();
        
        io.sockets.emit('jcall_notification_'+data.user_id, { status : 0 });
        
        // io.sockets.emit('jcall_details_'+data.meeting_id, { status : 0 });
    }
    });

    socket.on('removeJcall', function (data,callback) {
    
    io.sockets.emit('jcall_notification_'+data.user_id, { status : 0 });
    
    io.sockets.emit('jcall_details_'+data.meeting_id, { status : 0 });
    
    jCall.splice(jCall.indexOf(data.user_id) , 1) ;
        
    jCall.splice(jCall.indexOf(data.to_user_id) , 1) ;
        
    });




    // get jcall List
    socket.on('getJcallListt', function (data) {
    io.sockets.emit('jcall_list', jCall);
    });
    // jcall List
    function updateJcallList(){
    io.sockets.emit('jcall_list', jCall);
    }

    //jcall details
    socket.on('update_jCallDetails', function (data) {
    
    if(data.status !== '0') {
        io.sockets.emit('jcall_details_'+data.meeting_id, { status : data.status, meeting_id: data.meeting_id,  from_call_type: data.from_call_type , to_call_type: data.to_call_type ,call_status: data.call_status });
    }
    });

    //------------------------------------------------------------------------------- Jazecall Module End --------------------------------------------------------------------------------------------//   




















    socket.on('getAccountList', function (callback) {

        callback(loginAccountLists);    
    });















  socket.on('check_user', function (data,callback) {

    console.log(data);
    // if(user_id.indexOf(data) != -1){
    //     callback(false);
    // } else{
    //     socket.userid = data;
    //     user_id.push(socket.userid);
    //     updateUserActiTime(socket.userid); 
    //     updateUserId();
    //     callback(true);
    // }
});

    // user id update
    socket.on('checkUserID', function (data) {
        if(user_id.indexOf(data) != -1){
             updateUserActiTime(data); 
        } else{
            socket.userid = data;
            user_id.push(socket.userid);
            updateUserActiTime(socket.userid); 
            updateUserId();
        }
    });
  
    
  
  // remove user_id
  socket.on('user_disconnect', function (data) {
      if(data !== 0)
      {
         user_id.splice(user_id.indexOf(data) , 1) ;
         jCall.splice(jCall.indexOf(data) , 1) ;
         user_logindetails.splice(getIndex(user_logindetails,data), 1);
         updateUserId();
      }
  });
  
  // get User List
  socket.on('getUserList', function (data) {
       io.sockets.emit('user_list', user_id);
       io.sockets.emit('user_Ldetails', user_logindetails);
  });
  
  // User List
  function updateUserId(){
    io.sockets.emit('user_list', user_id);
    io.sockets.emit('user_Ldetails', user_logindetails);
  }
  // user is online
    socket.on('user_online', function (data,callback) {
        if(user_id.indexOf(data) != -1){
            callback(true);
        } else{
            callback(false);
        }
  });
  /// update user last activity time
    function updateUserActiTime(user_id) {
        if(parseInt(user_id) !== 0){
             var indx =   getIndex(user_logindetails,user_id);
             if(typeof(indx) !== "undefined"){ 
                 user_logindetails[indx][1] = new Date().toLocaleString("en-US", {timeZone: "Asia/Dubai"});   }
             else{
                 user_logindetails.push([user_id, new Date().toLocaleString("en-US", {timeZone: "Asia/Dubai"})]); }
        } 
  }
  // user current page
   socket.on('updateUserActPage', function (user_id,control_name,page_name) {
         
       if(parseInt(user_id) !== 0 && page_name !== "" && page_name !== null){
             var indx =   getIndex(user_page,user_id);
             if(typeof(indx) !== "undefined"){ 
                 user_page[indx][1] = control_name;   
                 user_page[indx][2] = page_name;   }
             else{
                 user_page.push([user_id, control_name, page_name]); }
           
            io.sockets.emit('getUserActPage_'+user_id, user_page);
        } 
  });
  // get User List
  socket.on('getUserActivPage', function (user_id,callback) {
       var indx =   getIndex(user_page,user_id);
       if(typeof(indx) !== "undefined"){    
          callback(user_page[indx]);
         }
         else{ 
         callback(''); 
         }
  });
  // get User Last active time
  socket.on('getUserLastActivTime', function (user_id,callback) {
        var indx =   getIndex(user_logindetails,user_id);
         if(typeof(indx) !== "undefined"){    
          callback(user_logindetails[indx][1]);
         }
         else{ 
         callback(''); 
         }
  });
  
  
  
  
  
  
//------------------------------------------------------------------------------- Jcon Module Start --------------------------------------------------------------------------------------------//   
     
    socket.on('setJSchedule', function (user_id,meeting_id,title,s_date) {
      
      var m_id = user_id+'_'+meeting_id+'_'+title;
      
      var j = schedule.scheduleJob(m_id,s_date.toLocaleString(), function(){
          
          var r_id = m_id.split('_');
          
          io.sockets.emit('getJSchedule_'+r_id[0], { status : 1, meeting_id:r_id[1] , meeting_title:r_id[2] });
          
           schedule.cancelJob(r_id[0]+'_'+r_id[1]);
          
        }, null, true, 'Asia/Dubai');
  });
  
  
  
   
//------------------------------------------------------------------------------- Jcon Module End --------------------------------------------------------------------------------------------//   
   




 //------------------------------------------------------------------------------- General Function Start  --------------------------------------------------------------------------------------------//   
  
   
 function getIndex(array_list,val) {
        for (var i = 0; i < array_list.length; i++) {
            if (parseInt(array_list[i][0]) === parseInt(val)) {
               return i;
            }
        }
    }

//------------------------------------------------------------------------------- General Function  End  --------------------------------------------------------------------------------------------//   
   



  
  
 });

}






// const request = require("request");
// const bodyParser = require('body-parser');
// const express = require('express');
// const router = express.Router();
// router.use(bodyParser.json());
// router.use(bodyParser.urlencoded({ extended: true }));
// const { check, validationResult } = require('express-validator');

// router.post('/generals/sendPushNotification', [

//     check('api_token').isAlphanumeric(),
//     check('account_id').isNumeric(),
//     check('request_account_id').isLength({ min: 1 }),
//     check('group_id').isLength({ min: 1 }),
//     check('flag').isNumeric()

// ], (req, res, next) => {

//     var _send = res.send;
//     var sent = false;

//     res.send = function (data) {
//         if (sent) return;
//         _send.bind(res)(data);
//         sent = true;
//     };

//     var errors = validationResult(req.body);
//     if (!errors.isEmpty()) {
//         return res.status(422).json({
//             success: success,
//             result: {},
//             successMessage: successMessage,
//             errorMessage: "No mandatory fileds entered."
//         });
//     }

//     var reqParams = {
//         api_token	         :req.body.api_token,
//         account_id	         :req.body.account_id,
//         request_account_id	 :req.body.request_account_id,
//         group_id             :req.body.group_id,
//         message              :req.body.message,
//         flag                 :req.body.flag
//     };    

//     console.log(" sendPushNotification",reqParams);

// //     HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

// //         if (queParamRes.length) {

// //                 HelperNotification.sendPushNotification(reqParams, function(returnNoti){

// //                     if(parseInt(returnNoti) === 1)
// //                     {
// //                         jsonRes = {
// //                             success: '1',	
// //                             result: {},
// //                             successMessage: "notification successfully sent.",
// //                             errorMessage: errorMessage 
// //                         };
// //                         res.status(201).json(jsonRes);
// //                         res.end();
// //                     }
// //                     else
// //                     {
// //                         jsonRes = {
// //                             success: success,
// //                             result: {},
// //                             successMessage: successMessage,
// //                             errorMessage: "unable to complete the process please try later."
// //                         };
    
// //                         res.status(statRes).json(jsonRes);
// //                         res.end();
// //                     }
// //                 });
// //         }
// //         else {
// //             jsonRes = {
// //                 success: success,
// //                 result: {},
// //                 successMessage: successMessage,
// //                 errorMessage: "Device not found."
// //             };

// //             res.status(statRes).json(jsonRes);
// //             res.end();
// //         }
// // });
// });






