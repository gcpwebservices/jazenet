const fs_methods = {};
const admin = require('firebase');
const fs = require('fs');
const path = require('path');
const moment = require("moment");
const dateFormat = require('dateformat');
const HelperNotification = require('./helpers/HelperNotification.js');
var firestore = admin.firestore();
const fs_conversation = firestore.doc('jaze_chat/jazechat_conversation');

var STATIC_IMG_PATH = G_STATIC_IMG_PATH+"jazechat/conversation/";
var STATIC_IMG_URL = G_STATIC_IMG_URL+"uploads/jazenet/jazechat/conversation/";


//document send or forward
fs_methods.createNewConversationDocument = function(reqParams,convers_id,message_type, return_result){

    if(convers_id !== "")
    {       
        const dateUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

        const dateInt = new Date(dateUTC).getTime();

        //step 1
        var fieldsConverstion = '';
        fieldsConverstion = {
            account_id           : reqParams.account_id.toString(),
            message              : reqParams.file_url,
            message_type         : message_type,
            reply_message_status : 0,
            reply_quote_details  : null,
            file_length          : "",
            read_status          : 0 ,
            date                 : dateInt,
            created_time         : dateUTC.toString(),
            time_zone            : 'UTC'.toString(),
        }, { merge: true };     

        // create conversation group 
        fs_conversation.collection(convers_id.toString()).doc(dateInt.toString()).set(fieldsConverstion);   
        

         //update conversation date
         HelperNotification.updateConversationChatDate(reqParams.request_account_id,convers_id,dateInt, function(retUrnUpdate){

            if(parseInt(retUrnUpdate) !== 0)
            {
                //send push notification
                var notiParams = {      
                    group_id            : convers_id.toString(),
                    account_id          : reqParams.account_id.toString(),
                    account_type        : reqParams.account_type.toString(),
                    request_account_id  : reqParams.request_account_id.toString(),
                    request_account_type: reqParams.request_account_type.toString(),
                    msg_type            : message_type.toString(),
                    msg                 : reqParams.file_url.toString(),
                    flag                : '4'
                };

                HelperNotification.sendPushNotification(notiParams, function(retUrnUpdate){
                    return_result(dateUTC);
                });
            }
            else
            {return_result(0); }
         });    
    }
    else
    {return_result(0); }
   
};


//const timeStamp = firebase_app.firestore.FieldValue.serverTimestamp();


/*
* Create New Conversation details and insert welcome message 
*/
fs_methods.createNewConversation = function(reqParams,convers_id,ownerDetails, return_result){

    if(convers_id !== "")
    {       
        const dateUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

        const dateInt = new Date(dateUTC).getTime();

        //step 1
        var fieldsConverstion = '';
        fieldsConverstion = {
            account_id      : ownerDetails.account_id.toString(),
            message         : ownerDetails.account_type.toString(),
            message_type    : 10,
            reply_message_status : 0,
            reply_quote_details : null,
            file_length          : "",
            read_status : 0 ,
            date : dateInt,
            created_time: dateUTC.toString(),
            time_zone :'UTC'.toString(),
        }, { merge: true };     

        // create conversation group 
        fs_conversation.collection(convers_id.toString()).doc(dateInt.toString()).set(fieldsConverstion);	
        

         //update conversation date
         HelperNotification.updateConversationChatDate(reqParams.request_account_id,convers_id,dateInt, function(retUrnUpdate){

            if(parseInt(retUrnUpdate) !== 0)
            {
                //send push notification
                var notiParams = {		
                    group_id	        : convers_id.toString(),
                    account_id	        : reqParams.account_id.toString(),
                    account_type        : reqParams.account_type.toString(),
                    request_account_id	: reqParams.request_account_id.toString(),
                    request_account_type: reqParams.request_account_type.toString(),
                    msg_type            : '10',
                    msg                 : " welcome, we are now connected.".toString(),
                    flag                : '4'
                };

                HelperNotification.sendPushNotification(notiParams, function(retUrnUpdate){
                    return_result(dateUTC);
                });
            }
            else
            {return_result(0); }
         });  	
    }
    else
    {return_result(0); }
   
};

/*
* Create New Broadcasting details
*/
fs_methods.createNewBroadcasting = function(reqParams,convers_id, return_result){

    if(convers_id !== "")
    {       
        const dateUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

        const dateInt = new Date(dateUTC).getTime();

        //step 1
        var fieldsConverstion = '';

        fieldsConverstion = {
            account_id   : reqParams.account_id.toString(),
            message      : "broadcast created".toString(),
            message_type : 12,
            reply_message_status : 0,
            reply_quote_details : null,
            file_length          : "",
            read_status  : 0 ,
            date         : dateInt,
            created_time : dateUTC.toString(),
            time_zone    :'UTC'.toString(),
        }, { merge: true };     

        fs_conversation.collection(convers_id.toString()).doc(dateInt.toString()).set(fieldsConverstion);	
        
        return_result(dateUTC);
    }
    else
    {return_result(0); }
   
};

/*
* Remove Broadcasting details
*/
fs_methods.deleteBroadcasting = function(reqParams, return_result){

    if(parseInt(reqParams.convers_id) !== 0)
    {       
        let cityRef = fs_conversation.collection(reqParams.convers_id.toString());
            let allCities = cityRef.get()
            .then(snapshot => {
                    snapshot.forEach(doc => {
                        if(typeof doc !== 'undefined' && doc !== null){                           
                            fs_conversation.collection(reqParams.convers_id.toString()).doc(doc.id).delete();
                            return_result(1);
                        }
                    });
            })
            .catch(err => {
                console.log('Error getting documents', err);
                return_result(0);
            });
    }
    else
    {return_result(0); }
};

/*
* send Broadcast Message
*/
fs_methods.sendBroadcastMessage = function(reqParams,reqMembers, return_result){

    if(reqParams !== null && reqMembers !== null)
    {       
        // send message personally 
        for (var i in reqMembers) {

            var conversation_id = reqMembers[i].member_id;

                //checking media
                var message_type_array = ['2','3','4','5','6','7'];

                if(message_type_array.indexOf(reqParams.message_type) != -1){
                    // media
                    __moveForwardMedia(reqParams.convers_id,conversation_id,reqParams.message,function(new_file_path){

                        if(new_file_path !== "")
                        {
                            //firebase insertion

                            var fieldsConverstion = {};

                            fieldsConverstion = {
                                account_id: reqParams.account_id.toString(),
                                message   : new_file_path,
                                message_type : parseInt(reqParams.message_type),
                                read_status : 0 ,
                                reply_message_status : 0,
                                reply_quote_details : null,
                                file_length          : "",
                                date         : parseInt(reqParams.date),
                                created_time : reqParams.created_time.toString(),
                                time_zone    : reqParams.time_zone.toString(),
                            }, { merge: true }; 


                            fs_conversation.collection(conversation_id.toString()).doc(reqParams.date.toString()).set(fieldsConverstion);

                            //send push notification
                            var notiParams = {		
                                group_id	        : conversation_id.toString(),
                                account_id	        : reqParams.account_id.toString(),
                                account_type        : reqParams.account_type.toString(),
                                request_account_id	: reqMembers[i].account_id.toString(),
                                request_account_type: reqMembers[i].account_type.toString(),
                                msg_type            : reqParams.message_type.toString(),
                                msg                 : reqParams.message.toString(),
                                flag                : '4'
                            };
                    
                            HelperNotification.sendPushNotification(notiParams, function(return_result) {

                                HelperNotification.updateConversationChatDate(notiParams.request_account_id,conversation_id,reqParams.date, function(returnStatus){ 

                                });	
                            });
                        }
                        else
                        { return_result(0); }
                    }); 
                }
                else
                {
                    //firebase insertion
                
                    var fieldsConverstion = {};

                    fieldsConverstion = {
                        account_id: reqParams.account_id.toString(),
                        message   : reqParams.message,
                        message_type : parseInt(reqParams.message_type),
                        read_status : 0 ,
                        reply_message_status : 0,
                        reply_quote_details : null,
                        file_length          : "",
                        date         : parseInt(reqParams.date),
                        created_time : reqParams.created_time.toString(),
                        time_zone    : reqParams.time_zone.toString(),
                    }, { merge: true }; 


                    fs_conversation.collection(conversation_id.toString()).doc(reqParams.date.toString()).set(fieldsConverstion);

                    //send push notification
                    var notiParams = {		
                        group_id	        : conversation_id.toString(),
                        account_id	        : reqParams.account_id.toString(),
                        account_type        : reqParams.account_type.toString(),
                        request_account_id	: reqMembers[i].account_id.toString(),
                        request_account_type: reqMembers[i].account_type.toString(),
                        msg_type            : reqParams.message_type.toString(),
                        msg                 : reqParams.message.toString(),
                        flag                : '4'
                    };
            
                    HelperNotification.sendPushNotification(notiParams, function(return_result) {

                        HelperNotification.updateConversationChatDate(notiParams.request_account_id,conversation_id,reqParams.date, function(returnStatus){ 

                        });	
                    });   
                }
        }

        return_result(1);
    }
    else
    {return_result(0); }
   
};

/*
* send message  Forward
*/
fs_methods.saveForwardMessages = function(arrParams,conversationIdList,return_result){

    try
    {
        var count_milisec = 1;

            arrParams.conversation.forEach(function(convers_row) {

                if(convers_row !== null && conversationIdList !== null)
                {
                    conversationIdList.forEach(function(conversation_id) {

                        //checking media
                                var message_type_array = ['2','3','4','5','6','7'];
            
                                if(message_type_array.indexOf(convers_row.message_type) != -1){

                                    __moveForwardMedia(arrParams.convers_id,conversation_id,convers_row.message,function(new_file_path){

                                        if(new_file_path !== "" && new_file_path !== null)
                                        {
                                            //firebase insertion

                                            var fieldsConverstion = {};

                                            var date = moment().add(count_milisec, 'milliseconds').utc().format('YYYY-MM-DD HH:mm:ss.SSS');

                                            const dateInt = new Date(date).getTime();

                                            fieldsConverstion = {
                                                account_id: arrParams.account_id.toString(),
                                                message   : new_file_path,
                                                message_type : parseInt(convers_row.message_type),
                                                read_status : 0 ,
                                                reply_message_status : 0,
                                                reply_quote_details : null,
                                                file_length          : "",
                                                date : dateInt,
                                                created_time: dateFormat(new Date(date), 'yyyy-mm-dd HH:MM:ss').toString(),
                                                time_zone :'UTC'.toString(),
                                            }, { merge: true }; 

                                            fs_conversation.collection(conversation_id.toString()).doc(dateInt.toString()).set(fieldsConverstion);

                                            //send push notification
                                            HelperNotification.getConversationAccountDetails(conversation_id,arrParams.account_id, function(memrb_result) {

                                                if(memrb_result !== null)
                                                {
                                                    var notiParams = {		
                                                        group_id	        : conversation_id.toString(),
                                                        account_id	        : arrParams.account_id.toString(),
                                                        account_type        : arrParams.account_type.toString(),
                                                        request_account_id	: memrb_result.account_id.toString(),
                                                        request_account_type: memrb_result.account_type.toString(),
                                                        msg_type            : convers_row.message_type.toString(),
                                                        msg                 : new_file_path.toString(),
                                                        flag                : '4'
                                                    };

                                                    HelperNotification.sendPushNotification(notiParams, function(return_result) {

                                                        HelperNotification.updateConversationChatDate(notiParams.request_account_id,conversation_id,dateInt, function(returnStatus){ 
        
                                                        });	
                                                    });
                                                }
                                            });

                                            count_milisec += 1;

                                            return_result(1);
                                        }
                                        else
                                        { return_result(0); }
                                    });         
                                }
                                else
                                { 
                                    //saving firebase
                                    var fieldsConverstion = {};
            
                                    var date = moment().add(count_milisec, 'milliseconds').utc().format('YYYY-MM-DD HH:mm:ss.SSS');

                                    const dateInt = new Date(date).getTime();

                                    fieldsConverstion = {
                                        account_id: arrParams.account_id.toString(),
                                        message   : convers_row.message.toString(),
                                        message_type : parseInt(convers_row.message_type),
                                        read_status : 0 ,
                                        reply_message_status : 0,
                                        reply_quote_details : null,
                                        file_length          : "",
                                        date : dateInt,
                                        created_time: dateFormat(new Date(date), 'yyyy-mm-dd HH:MM:ss').toString(),
                                        time_zone :'UTC'.toString(),
                                    }, { merge: true };  
            
                                    fs_conversation.collection(conversation_id.toString()).doc(dateInt.toString()).set(fieldsConverstion);	
                                    
                                     //send push notification
                                     HelperNotification.getConversationAccountDetails(conversation_id,arrParams.account_id, function(memrb_result) {

                                        if(memrb_result !== null)
                                        {
                                            var notiParams = {		
                                                group_id	        : conversation_id.toString(),
                                                account_id	        : arrParams.account_id.toString(),
                                                account_type        : arrParams.account_type.toString(),
                                                request_account_id	: memrb_result.account_id.toString(),
                                                request_account_type: memrb_result.account_type.toString(),
                                                msg_type            : convers_row.message_type.toString(),
                                                msg                 : convers_row.message.toString(),
                                                flag                : '4'
                                            };

                                            HelperNotification.sendPushNotification(notiParams, function(return_result) {

                                                HelperNotification.updateConversationChatDate(notiParams.request_account_id,conversation_id,dateInt, function(returnStatus){ 

                                                });	
                                            });
                                        }
                                    });

                                    count_milisec += 1;

                                    return_result(1);   
                                }
                    });
                }
                else
                { return_result(0); }
            });
    }
    catch(err)
	{
		console.log(err);
		return_result(0);
	}
};

/*
* move media for Forward case
*/
function __moveForwardMedia(orgin_convers_id,forward_convers_id,file_path,call_back){

    try{

        if(parseInt(orgin_convers_id) !== 0 &&  parseInt(forward_convers_id) !== 0 && orgin_file_path !== "")
        {
            var orgin_sourcPath = (file_path.split("conversation/")[1]).split("/");

            var orgin_file_path = STATIC_IMG_PATH+"/"+orgin_sourcPath[0]+"/"+orgin_sourcPath[1]+"/"+orgin_sourcPath[2];
            
            var destination_Path = STATIC_IMG_PATH+forward_convers_id;
            //checking convers_id folder
            if(!fs.existsSync(destination_Path)){
                fs.mkdirSync(destination_Path,'0757', true);             
            }

            //checking format folder
            if(!fs.existsSync(destination_Path+"/"+orgin_sourcPath[1])){
                fs.mkdirSync(destination_Path+"/"+orgin_sourcPath[1],'0757', true);             
            }

            var new_file_path = destination_Path+"/"+orgin_sourcPath[1]+"/"+orgin_sourcPath[2];

            //checking file exists
            var new_name = __checkRenameMedia(new_file_path,path.basename(new_file_path));

            if(new_name !== path.basename(new_file_path))
            { new_file_path = destination_Path+"/"+orgin_sourcPath[1]+"/"+new_name; }

             //copy
             fs.copyFile(orgin_file_path,new_file_path, (err) => {
                call_back(STATIC_IMG_URL+forward_convers_id+"/"+orgin_sourcPath[1]+"/"+path.basename(new_file_path));
            });
        }
        else
        { call_back(null); }
    }
    catch(err)
	{
        console.log(err);
        call_back(null);
	}
}

/*
* check file name exist, if exist rename 
*/
function __checkRenameMedia(file_path,file_name,count = 0){
    try
    {
         if (fs.existsSync(file_path)) {
        
            var n_count = parseInt(count) + 1;

            var dir_path =    path.dirname(file_path);

            var file_list =  file_name.split(".");

            var new_path = dir_path+"/"+file_list[0]+" ("+n_count+")."+file_list[1];

            return  __checkRenameMedia(new_path,file_name,n_count);
        }
        else
        {
            return path.basename(file_path);
        }
    }
    catch(err)
    {
        console.log(err);
    }
}

/*
 * remove media 
 */
fs_methods.removeChatMedia = function(arrParams,return_result){

    try
    {
        
        arrParams.conversation.forEach(function(convers_row) {

            if(convers_row !== null)
            {
                //checking is media or link
                var message_type_array = ['2','3','4','5','6','7'];

                if(convers_row.message !== "")
                {
                    if(message_type_array.indexOf(convers_row.message_type) != -1){

                        var file_path = convers_row.message.split("conversation/")[1];
                
                        if(typeof file_path !== 'undefined' && file_path !== null){

                            var sourcPath = G_STATIC_IMG_PATH+'jazechat/conversation/' + file_path;

                            if(parseInt(arrParams.flag) === 1)
                            {
                                let  colRef =fs_conversation.collection(arrParams.convers_id.toString()).doc(convers_row.timestamp.toString());	

                                let getDoc = colRef.get()
                                    .then(doc => {
                                        if (!doc.exists) 
                                        {
                                            return_result(0);
                                        } 
                                        else 
                                        {
                                            fs.unlink(sourcPath, function (err) {
                                                if (err){
                                                    return_result(2);
                                                }else{
                                                    colRef.update({
                                                    "message_type": 0,
                                                    "message": "&#9865&nbsp<i><font color='grey'>This message was deleted</font></i>"
                                                    });
                                                    return_result(1)
                                                }
                                            });
                                        }
                                    })
                                .catch(err => {
                                    return_result(2);
                                    console.log('Error getting document', err);
                                })
                            }
                            else  if(parseInt(arrParams.flag) === 0)
                            {
                                fs.unlink(sourcPath, function (err) {
                                    if (err){
                                        return_result(2);
                                    }else{
                                        return_result(1)
                                    }
                                });
                            }
                            else
                            { return_result(0); }
                        }
                        else
                        { return_result(0); }
                    }
                    else
                    {
                        // link and location
                        let  colRef =fs_conversation.collection(arrParams.convers_id.toString()).doc(convers_row.timestamp.toString());	

                        let getDoc = colRef.get()
                            .then(doc => {
                                if (!doc.exists) 
                                {
                                    return_result(0);
                                } 
                                else 
                                {
                                    colRef.update({
                                        "message_type": 0,
                                        "message": "&#9865&nbsp<i><font color='grey'>This message was deleted</font></i>"
                                        });
                                        return_result(1)
                                }
                            })
                        .catch(err => {
                            return_result(2);
                            console.log('Error getting document', err);
                        })
                    }
                }
                else
                { return_result(0); }    
            }
            else
            { return_result(0); }
        });
    }
    catch(err)
	{
		console.log(err);
		return_result(0);
	}
};


/*
 * Get Chat All Media and  synchronize server 
 */
fs_methods.getAllSynchronizedMedialist = function(conv_id, return_result){

    try {
            //geting server file list
            var file_list = __getAllFiles(conv_id);

            if(file_list !== null)
            {
                getFirebaseMediaList(conv_id,file_list,function (return_list){

                    if(return_list !== null)
                    {
                        if(return_list.delete_media_list !== null)
                        {
                            __deleteMedia(return_list.delete_media_list);
                        }

                         return_result(return_list.media_list);
                    }
                    else
                    { return_result(null); }
                });
            }
            else
            { return_result(null); }
        }
        catch (err) {
            console.log("catch error: ",err);
            return_result(null);
        }
}


function __getAllFiles(conv_id){

    var file_list = getDirectoryFiles(STATIC_IMG_PATH+conv_id);
   
    var return_result = [];

    for (var i in file_list) {

        if(file_list[i] !=null && file_list[i] != ""){

           return_result.push(STATIC_IMG_URL+conv_id+"/"+(file_list[i].split("conversation/"+conv_id+"/")[1]));
        }
      }

    return return_result;
} 

function getDirectoryFiles(dir, excludeDirs) {
    
        try{

            return fs.readdirSync(dir).reduce(function (list, file) {
                const name = path.join(dir, file);
                if (fs.statSync(name).isDirectory()) {
                    if (excludeDirs && excludeDirs.length) {
                        excludeDirs = excludeDirs.map(d => path.normalize(d));
                        const idx = name.indexOf(path.sep);
                        const directory = name.slice(0, idx === -1 ? name.length : idx);
                        if (excludeDirs.indexOf(directory) !== -1)
                            return list;
                    }
                    return list.concat(getDirectoryFiles(name, excludeDirs));
                }
                return list.concat([name]);
            }, []);
        }
        catch(error)
        { console.log("getDirectoryFiles",error); }
}

function  __deleteMedia(file_path){

    try
    {
        if(file_path !== null && file_path !== "")
        {
            file_path.forEach(function(row) {

                fs.unlink(STATIC_IMG_PATH+row.split(STATIC_IMG_URL)[1], function (err) {
                   
                });
            });
        }
    }
    catch (err) {
        console.log(err);
    }
    
}

function getFileTypes(ext){

	var type;

	var arrImages = ['.jpeg','.jpg','.gif','.png','.tif'];
	var arrVideos = ['.mp4','.avi','.mov','.flv','.wmv'];
	var arrAudio = ['.pcm','.wav','.aiff','.mp3','.aac','.wma','.flac','.alac'];
	var arrDocs = ['.odt','.xls','.xlsx','.ods','.ppt','.pptx','.txt','.doc'];
	var arrPdf = ['.pdf'];

	if(arrImages.includes(ext.toLowerCase())){
		type = '2';
	}else if(arrAudio.includes(ext.toLowerCase())){
		type = '3';
	}else if(arrVideos.includes(ext.toLowerCase())){
		type = '4';
	}else if(arrDocs.includes(ext.toLowerCase())){
		type = '5';
	}else if(arrPdf.includes(ext.toLowerCase())){
		type = '6';
	}else{
		type = '7';
	}

	return type;
};

function getExtension(path) {
    var basename = path.split(/[\\/]/).pop(),  // extract file name from full path ...
                                               // (supports `\\` and `/` separators)
        pos = basename.lastIndexOf(".");       // get last position of `.`

    if (basename === "" || pos < 1)            // if file name is empty or ...
        return "";                             //  `.` not found (-1) or comes first (0)

    return basename.slice(pos + 1);            // extract extension ignoring `.`
}

getFirebaseMediaList = function(conv_id,server_file_list, return_result){

    //geting  firebase messages
    let cityRef = fs_conversation.collection(conv_id.toString()).where('message_type', '>=', 2).where('message_type', '<=', 9);
    let allCities = cityRef.get()
    .then(snapshot => {
        //  if (snapshot.exists) {
            var media_list_array = [];

            snapshot.forEach(doc => {
                if(typeof doc !== 'undefined' && doc !== null){

                    //pushing array

                    var obj_msg = {
                        account_id  :doc.data().account_id,
                        time_stamp  :doc.data().date.toString(),
                        message     :doc.data().message,
                        type        :doc.data().message_type.toString(),
                    }

                    media_list_array.push(obj_msg);

                    //checking is in list 
                    if(server_file_list.indexOf(doc.data().message) !== -1){

                        server_file_list.splice(server_file_list.indexOf(doc.data().message), 1);
                    }
                }
            });

            var reslt = {
                media_list        : media_list_array,
                delete_media_list : server_file_list
            };
            
            return_result(reslt);

    })
    .catch(err => {
        console.log('Error getting documents', err);
        return_result(null);
    });
};




module.exports = fs_methods;



