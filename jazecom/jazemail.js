require('events').EventEmitter.prototype._maxListeners = 1000;
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const express = require('express');
const router = express.Router();

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperJazeMail	= require('./helpers/HelperJazeMail.js');
const HelperNotification = require('./helpers/HelperNotification.js');
const HelperJazecom = require('./helpers/HelperJazecom.js');


const { check, validationResult } = require('express-validator/check');

const dateFormat = require('dateformat');
const now = new Date();
const standardDT = dateFormat(now,'yyyy-mm-dd HH:MM:ss');

const standardDTNEW = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
const newdateForm = dateFormat(new Date(standardDTNEW), 'yyyy-mm-dd HH:MM:ss');

const mime = require('mime-types');
const multer = require('multer');

var Imap = require('imap');
var mimemessage = require('mimemessage');

const mailer = require('nodemailer-promise');
const simpleParser = require('mailparser').simpleParser;

const _ = require('lodash');
const fast_sort = require("fast-sort");

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};
var arrEmails = [];

const btoa = require('btoa');
const atob = require('atob');
const fs = require('fs');
const path = require('path')


process.on('uncaughtException', function (err) {
  	console.error((new Date).toUTCString() + ' uncaughtException:', err.message)
  	console.error(err.stack)
  	process.exit(1)
});


router.post('/jazemail/contact_accept', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('sender_account_id').isNumeric(),

	check('folder').isLength({ min: 1 }),
	check('seq').isLength({ min: 1 }),
	check('date').isLength({ min: 1 }),
	check('mail_id').isLength({ min: 1 }),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  api_token = req.body.api_token;
	var  account_id = req.body.account_id;
	var  sender_account_id = req.body.sender_account_id;
	
	var  folder = req.body.folder;
	var  message_id = req.body.seq;
	var  date = req.body.date;
	var  mail_id = req.body.mail_id;

	var arrParams = {		
		account_id         : account_id,
		sender_account_id  : sender_account_id,
		folder             : folder,
		message_id         : message_id,
		mail_id            : mail_id,
		date               : date
	};


	var arrTokens = {account_id:account_id,api_token:api_token};
	HelperRestriction.checkParametersApiToken(arrTokens, function(queParam){
		if(queParam.length)
		{	
			
			HelperJazeMail.getEmailAccountDetails(arrParams, function(retAccount){

				if(retAccount!=null)
				{
					HelperJazeMail.acceptMailContact(arrParams, function(retAcceptContact){

						if(parseInt(retAcceptContact) !== 0 )
						{
			
							var mailParam = {mail_type:retAccount.mail_type,folder:folder};
							HelperJazeMail.getMailFolder(mailParam, function(retFolder){

								if(retAccount!=null)
								{

									var ssl = false;
									if(retAccount.is_ssl == 1){
										ssl = true;
									}
							
									var imap = new Imap({
									  	user: retAccount.email_account,
									  	password: atob(retAccount.email_password),
									  	host: retAccount.imap_host,
									  	port: retAccount.imap_port,
									  	tls: true,
									  	authTimeout: 30000
									});

									function openInbox(cb) {
									  	imap.openBox(retFolder, false, cb);
									}

								    imap.once('ready', function () {

			    		    	  	 	if(imap.state != 'authenticated'){ 
									 		imap.connect();
									 	}
									 //	console.log('imap status: '+imap.state);

							        openInbox(function (err, box) {
							      
							            if (err){
						            	//  	console.log("error after open inbox: ",err);
				                			jsonRes = {
												success: success,
												result: {},
												successMessage: successMessage,
												errorMessage: "Unable to load mailbox. Please try again. "+err
											};

											res.status(201).json(jsonRes);
											res.end();

							            } 
							            else 
							            {
						            	 	let messageInbox = [];
											let objMessage = {};

				     						var attachArr = [];
				 							var objAttach = {};
											var seq_number;


		          			                imap.search([ 'ALL', ['ON', date] ], function (err, results) {
							                    if (err){
			  									//	console.log("error after imap search: ",err);
						                        	jsonRes = {
														success: success,
														result: {},
														successMessage: successMessage,
														errorMessage: "Unable to load mailbox. Please try again. "+err
													};

													res.status(201).json(jsonRes);
													res.end();
							                    } 
							                    else 
							                    {
							                        if (!results || !results.length){
							                         //   console.log('no mails!!!');
				                                     	jsonRes = {
															success: success,
															result: {},
															successMessage: successMessage,
															errorMessage: "Mailbox is empty." 
														};

														res.status(201).json(jsonRes);
														res.end();

							                            imap.end();
							                        } 
							                        else 
							                        {

					                        		  	imap.setFlags(results, ['\\Seen'], function(err) {
											                if (!err) {
											                  //  console.log("marked as read");
											                }
											            });

								                           	var f = imap.fetch(results, {bodies:''});
								                          
								                            f.on('message', function (msg, seqno) {
								                                msg.on('body', function (stream, info) {
																	
															
								                                	if(seqno == message_id)
								                                	{ 
					                                		          	async function main(stream) {

				                                		 			 	let parsed = await simpleParser(stream);

											          						if(parsed.attachments.length > 0)
														    				{
														    	
														    					parsed.attachments.forEach(function(row,key) {
																			
																					objAttach = {
																    					file_name:row.filename,
																    					file_type:row.contentType,
																    					file_size:row.size.toString()
																    				};

												    								attachArr.push(objAttach);

																			  	});
													    						
															    			}

									            							messageInbox.push(parsed);
								            								seq_number = seqno;
																		}

																		main(stream);

								                                	}
								                     
								                                });
								                                msg.once('end', function() {
								                         		    //console.log(seqno + ' finished.');
								                                });
								                            });
								                            f.once('error', function(err) {
								                               // console.log('failed to fetch: ' + err);

					                                           	jsonRes = {
																	success: success,
																	result: {},
																	successMessage: successMessage,
																	errorMessage: "Unable to load mailbox. Please try again. "+err
																};

																res.status(201).json(jsonRes);
																res.end();

								                            });
								                            f.once('end', function() {
				   												
								                            	imap.end();
																setTimeout(function(){

																	var cc  = [];
																	var bcc = [];
																	var to  = [];
																	var from  = '';
																	
									             					if(messageInbox != ''){

						     						       				messageInbox.forEach(function(row) {
																																		
																			if (row.headers.has('cc')) {
												  				        		cc = row.headers.get('cc').value;
																			}

																			if (row.headers.has('bcc')) {
												  				        		bcc = row.headers.get('bcc').value;
																			}
																						        
																			if (row.headers.has('to')) {
												  				        		to = row.headers.get('to').value;
																			}
													
																			var message = '';
																			if(row.html !== false){
																				message = row.html;
																			}else if(row.text !== false){
																				message = row.text;
																			}

																			var subject = '';
															    			if (typeof row.subject != 'undefined') {
																				subject = row.subject;
																			}

							    											objMessage = {
																				seq               : seq_number.toString(),
												          		          		id                : row.messageId,
																          		subject           : subject,
													          		    		date              : dateFormat(row.date,'yyyy-mm-dd HH:MM:ss'),
									          		    			    		from_account_id   : '',
													          		    		from_account_type : '',
													          		    		from_name         : row.from.value[0].name,
													          					from_logo         : '',
																        		from_email        : row.from.value[0].address,												        
																          		to                : '',
																          		cc                : '',
																          		bcc               : '',
																          		attachment        : attachArr,
																          		message           : message
																     
																          	};

													          	         	from = row.from.value[0].address;
																   
																	  	});

															
						     						       				// getting the details of accounts per emails
																		HelperJazeMail.getAccountCredentialsFrom(from, function(retFrom){	
														
																			HelperJazeMail.getAccountCredentialsTO(to, function(retTO){	

																				HelperJazeMail.getAccountCredentialsCC(cc, function(retCC){	

																					HelperJazeMail.getAccountCredentialsBCC(bcc, function(retBCC){	
																												
																						if(retFrom.name!="" && typeof retFrom.name !== "undefined"){objMessage.from_name = retFrom.name;}

																						objMessage.from_logo         = retFrom.logo;
																						objMessage.from_account_id   = retFrom.account_id;
																						objMessage.from_account_type = retFrom.account_type;

																						objMessage.to  = retTO;
																						objMessage.cc  = retCC;
																						objMessage.bcc = retBCC;
																			
																						jsonRes = {
																							success: '1',
																								result: {
																									email_details:objMessage
																								},
																							successMessage: 'Email is successfully added to contacts.',
																							errorMessage: errorMessage 
																				  		};

																						res.status(201).json(jsonRes);
																						res.end();
								  												
																					});
																				});
																			});
																		});

																		//update the read status
																		var notiDetails = {receiver_account_id:account_id, mail_id:objMessage.id};
																		HelperJazeMail.getNotificationDetails(notiDetails, function(retUrnUpdate){});  

									             					}else{

																		jsonRes = {
																			success: success,
																			result: {},
																			successMessage: successMessage,
																			errorMessage: "Mail not found." 
																		};

																		res.status(201).json(jsonRes);
																		res.end();

									             					}	

								             				

					             							 	}, 1000);	                    
								                               	imap.end();
								                            });
								                        }
								                    }
								                })
								            }
								        });
								    });

								    imap.once('error', function(err) {
							    	 	imap.end();
								        console.log(err);
								    });

								    imap.once('end', function() {
							    	 	imap.end();
								     //   console.log('finished!');
								    });

								    imap.connect();

							    }
							    else
							    {

			    					jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "Account not found." 
									};

									res.status(statRes).json(jsonRes);
									res.end();
			
							    }


					    	});

						}
						else
						{
							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Unable to process request. Please try again." 
							};

							res.status(statRes).json(jsonRes);
							res.end();

						}

					});
		
				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Account not found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();

				}

			});

		}
		else	
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});


router.post('/jazemail/contact_reject', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('sender_account_id').isNumeric(),

	check('folder').isLength({ min: 1 }),
	check('seq').isLength({ min: 1 }),
	check('date').isLength({ min: 1 }),
	check('mail_id').isLength({ min: 1 }),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  api_token = req.body.api_token;
	var  account_id = req.body.account_id;
	var  sender_account_id = req.body.sender_account_id;
	
	var  folder = req.body.folder;
	var  message_id = req.body.seq;
	var  date = req.body.date;
	var  mail_id = req.body.mail_id;

	var arrParams = {		
		account_id         : account_id,
		sender_account_id  : sender_account_id,
		folder             : folder,
		message_id         : message_id,
		mail_id            : mail_id,
		date               : date
	};

	var arrTokens = {account_id:account_id,api_token:api_token};
	HelperRestriction.checkParametersApiToken(arrTokens, function(queParam){
		if(queParam.length)
		{	
			
			HelperJazeMail.getEmailAccountDetails(arrParams, function(retAccount){

				if(retAccount!=null)
				{
					HelperJazeMail.rejectMailContact(arrParams, function(retAcceptContact){

						if(parseInt(retAcceptContact) !== 0 )
						{
						
							HelperJazeMail.getEmailAccountDetails(arrParams, function(retAccount){
							
								if(retAccount!=null){

								var mailDetails = {folder:folder,mail_type:retAccount.mail_type};

									HelperJazeMail.getMailFolder(mailDetails, function(retFolder){

										if(retAccount!=null)
										{

											var ssl = false;
											if(retAccount.is_ssl == 1){
												ssl = true;
											}
									
											var imap = new Imap({
											  	user: retAccount.email_account,
											  	password: atob(retAccount.email_password),
											  	host: retAccount.imap_host,
											  	port: retAccount.imap_port,
											  	tls: ssl
											});

											function openInbox(cb) {
											  	imap.openBox(retFolder, false, cb);
											}

										    imap.once('ready', function () {
									        openInbox(function (err, box) {
									            if (err){

						                			jsonRes = {
														success: success,
														result: {},
														successMessage: successMessage,
														errorMessage: "Unable to load mailbox. Please try again." 
													};

													res.status(statRes).json(jsonRes);
													res.end();

									            } 
									            else 
									            {
								            	 	let messageInbox = [];
													let objMessage = {};

						     						var attachArr = [];
						 							var objAttach = {};

						 							var seqforDel = [];									

						       	                   	var today = new Date();
													var tomorrow = new Date();
													var newNow = tomorrow.setDate(today.getDate()+1); 

										 			var currentMonth = new Date();
													var lastMonth = currentMonth.setMonth(currentMonth.getMonth() - 1);


								                   	var dateNowAdd1Day = dateFormat(newNow,'yyyy-mm-dd');
								                   	var dateLastMonth =  dateFormat(lastMonth,'yyyy-mm-dd');

									                imap.search([ 'ALL', ['SINCE', dateLastMonth], ['BEFORE', dateNowAdd1Day] ], function (err, results) {
									                    if (err){
						  
								                        	jsonRes = {
																success: success,
																result: {},
																successMessage: successMessage,
																errorMessage: "Unable to load mailbox. Please try again." 
															};

															res.status(201).json(jsonRes);
															res.end();
									                    } 
									                    else 
									                    {
									                        if (!results || !results.length){
									                           // console.log('no mails!!!');
						                                     	jsonRes = {
																	success: success,
																	result: {},
																	successMessage: successMessage,
																	errorMessage: "Mailbox is empty." 
																};

																res.status(201).json(jsonRes);
																res.end();

									                            imap.end();
									                        } 
									                        else 
									                        {
									                        
									                            //console.log('the length of results:'+results.length);
										                           	var f = imap.fetch(results, {bodies:''});
										                          
										                            f.on('message', function (msg, seqno) {
										                                msg.on('body', function (stream, info) {
					
										                                });
										                                msg.once('end', function() {
										                         		    //console.log(seqno + ' finished.');
										                                });
										                            });
										                            f.once('error', function(err) {
										                               // console.log('failed to fetch: ' + err);

							                                           	jsonRes = {
																			success: success,
																			result: {},
																			successMessage: successMessage,
																			errorMessage: "Unable to load mailbox. Please try again." 
																		};

																		res.status(201).json(jsonRes);
																		res.end();

										                            });
										                            f.once('end', function() {				

																		setTimeout(function(){

																			if(retAccount.imap_host=='imap.yandex.com'){

																		    imap.seq.move(message_id, 'Trash', function(err) {
																		  		if(!err){
															  						jsonRes = {
																						success: '1',
																							result: {},
																						successMessage: 'Request is successfully rejected.',
																						errorMessage: errorMessage 
																			  		};

																					res.status(201).json(jsonRes);
																					res.end();
																		  		}else{
																		  											
																	  				jsonRes = {
																						success: success,
																						result: {},
																						successMessage: successMessage,
																						errorMessage: "Unable to process request. Please try again." 
																					};

																					res.status(201).json(jsonRes);
																					res.end();
																		  		}
																		    });
																

																			}else{

																				imap.seq.addFlags(message_id, 'Deleted', function(err) {
																			
																			  		if(!err){

																						jsonRes = {
																							success: '1',
																								result: {},
																							successMessage: 'Request is successfully rejected.',
																							errorMessage: errorMessage 
																				  		};

																						res.status(201).json(jsonRes);
																						res.end();

																				  	}else{
																	

																		  				jsonRes = {
																							success: success,
																							result: {},
																							successMessage: successMessage,
																							errorMessage: "Unable to process request. Please try again." 
																						};

																						res.status(201).json(jsonRes);
																						res.end();
																				  			
																				  	}
																
																			
																				});

																			}

																		

																           	imap.end();

								                                	 	}, 500);

										                            });

										                        }
										                    }
										                })
										            }
										        });
										    });

										    imap.once('error', function(err) {
										       // console.log(err);
										    });

										    imap.once('end', function() {
										      //  console.log('finished!');
										    });

										    imap.connect();

						    			}
										else
										{

											jsonRes = {
												success: success,
												result: {},
												successMessage: successMessage,
												errorMessage: "Account not found." 
											};

											res.status(statRes).json(jsonRes);
											res.end();

										}

							      	});

								}
								else
								{

									jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "Account not found." 
									};

									res.status(statRes).json(jsonRes);
									res.end();

								}

							});


						}
						else
						{
							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Unable to process request. Please try again." 
							};

							res.status(statRes).json(jsonRes);
							res.end();

						}

					});
		
				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Account not found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();

				}

			});

		}
		else	
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});

/*
 * Search Own Contacts
 */
router.post('/jazemail/search_my_contacts', [
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('app_id').isNumeric(),
	check('key').isLength({ min: 1 })
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {	
		api_token	: req.body.api_token,
		account_id	: req.body.account_id,
		app_id		: req.body.app_id,
		key			: req.body.key
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(queParam.length)
		{	
			//search Contact 
			HelperJazeMail.searchEmailContacts(arrParams, function(contact_email){	

				//search external 
				HelperJazeMail.searchExternalEmailContacts(arrParams, function(external_email){	

					var result = [];

					if(contact_email !== null) { result = contact_email;}

					if(external_email !== null) 
					{ 
						if(result !== null) 
						{
							result = result.concat(external_email);
						}
						else 
						{ result = external_email; }
					}
					
					if(result !== null)
					{
						jsonRes = {
							success: '1',
							result: {searched_accounts:result},
							successMessage: 'Searched Account',
							errorMessage: errorMessage 
						};
				
						res.status(201).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: success,
							result: {searched_accounts : {}},
							successMessage: successMessage,
							errorMessage: "No result found. " 
						};
				
						res.status(201).json(jsonRes);
						res.end();
					}
				});
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});


router.post('/jazemail/search_email', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('key').isLength({ min: 1 })

	
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  api_token = req.body.api_token;
	var  account_id = req.body.account_id;
	var  to_email = req.body.key;
	

	var arrParams = {		
		account_id: account_id,
		to_email:to_email
	};

	var arrRows = [];
	var newObj = {};
	var account_list = {};
	var arrEmails = [];
	var newAllEmails = [];

	var arrTokens = {account_id:account_id,api_token:api_token};
	HelperRestriction.checkParametersApiToken(arrTokens, function(queParam){
		if(queParam.length)
		{	
			HelperJazeMail.getReceiverList(arrParams, function(returnReceivers){ 

				if(returnReceivers !=null)
 				{	

 					function filterItems(needle, heystack) {
					  	var query = needle.toLowerCase();
					  	return heystack.filter(function(item) {
				      		return item.toLowerCase().indexOf(query) >= 0;
					  	})
					}

					let returnEmail = filterItems(to_email, returnReceivers[0]);

					if(returnEmail.length > 0 )
					{	

						returnEmail.forEach(function(row) {
							arrEmails.push(row);
						});

						HelperJazeMail.searchIfBlockedAccount(arrEmails, function(retMailToID){ 

							//get the account_id
						
							if(retMailToID!=null)
							{

						
								var paramsForBlock = {account_id:account_id, searched_id:retMailToID.toString()};
				
								HelperJazeMail.checkSearchedAccountIdForBlock(paramsForBlock, function(retSearched){ 

									if(retSearched!=null){

										if(retSearched.includes(account_id)){

											HelperJazeMail.getEmailsFromCredentials(arrParams, function(retMails){ 

	
												if(retMails!=null){

								
													var index = retMails.indexOf(retMailToID.toString());
													if (index !== -1) retMails.splice(index, 1);

											
													HelperJazeMail.getEmailCredentialAll(retMails, function(retFiltered){ 

														account_list = {account_list:retFiltered};

														jsonRes = {
															success: '1',
															result: account_list,
															successMessage: "Email List. 1",
															errorMessage: "" 
														};

														res.status(201).json(jsonRes);
														res.end()

													});


												}else{

													account_list = {account_list:[]};

													jsonRes = {
														success: '1',
														result: account_list,
														successMessage: "Email List. 2",
														errorMessage: "" 
													};

													res.status(201).json(jsonRes);
													res.end()

												}
						
											
											});


										}else{

											HelperJazeMail.getEmailsFromCredentialsForSearch(arrParams, function(retMails){ 
							
												if(retMails!=null){
													account_list = {account_list:retMails};
												}else{
													account_list = {account_list:[]};
												}
													
												jsonRes = {
													success: '1',
													result: account_list,
													successMessage: "Email List. 4",
													errorMessage: "" 
												};

												res.status(201).json(jsonRes);
												res.end()
											});


										}
										
									}
									else
									{

										
										var newArrMails = [];
										arrEmails.forEach(function(row) {
											newObj = {
												name:'',
												logo:'',
												email_address: row
											}

											newArrMails.push(newObj);
									
										});


										account_list = {account_list:newArrMails};

										jsonRes = {
											success: '1',
											result: account_list,
											successMessage: "Email List.3",
											errorMessage: "" 
										};

										res.status(201).json(jsonRes);
										res.end()

									}


								});

							}else{

								HelperJazeMail.getEmailsFromCredentialsForSearch(arrParams, function(retMails){ 
							
									if(retMails!=null){
										account_list = {account_list:retMails};
									}else{
										account_list = {account_list:[]};
									}
										
									jsonRes = {
										success: '1',
										result: account_list,
										successMessage: "Email List. 4",
										errorMessage: "" 
									};

									res.status(201).json(jsonRes);
									res.end()
								});
												
							}
							
					
						});

					}
					else
					{

						HelperJazeMail.getEmailsFromCredentialsForSearch(arrParams, function(retMails){ 
							
							if(retMails!=null){
								account_list = {account_list:retMails};
							}else{
								account_list = {account_list:[]};
							}
								
							jsonRes = {
								success: '1',
								result: account_list,
								successMessage: "Email List. 5",
								errorMessage: "" 
							};

							res.status(201).json(jsonRes);
							res.end()
						});
						
					}

 				}
 				else
 				{

 		
					HelperJazeMail.getEmailsFromCredentialsForSearch(arrParams, function(retMails){ 
						
						if(retMails!=null){
							account_list = {account_list:retMails};
						}else{
							account_list = {account_list:[]};
						}
							
						jsonRes = {
							success: '1',
							result: account_list,
							successMessage: "Email List. 6",
							errorMessage: "" 
						};

						res.status(201).json(jsonRes);
						res.end()
					});

 				}

			});

		}
		else	
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});


/*
 * get List of External Email
 */

router.post('/jazemail/get_external_account_details', [
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('mail_id').isNumeric(),
	], (req,res,next) => {
	var _send = res.send;
	var sent = false;
	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}
	var arrParams = {	
		api_token			: req.body.api_token,
		account_id			: req.body.account_id,
		mail_id				: req.body.mail_id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(queParam.length)
		{	
			HelperJazeMail.getExternalEmailAccountDetails(arrParams, function(returnResult){

				if(returnResult !== null)
				{
					jsonRes = {
						success: '1',
						result: {external_mail_details : returnResult },
						successMessage: 'External Mail Details',
						errorMessage: errorMessage
					};

					statRes = 201;
					res.status(statRes).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to get details. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

/*
 * CURD of External Email
 */
router.post('/jazemail/save_email_account', [
	check('app_id').isNumeric(),
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('flag').isNumeric(),
	check('mail_id').isLength({ min: 1 }),
	], (req,res,next) => {
	var _send = res.send;
	var sent = false;
	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}
	var arrParams = {	
		api_token			: req.body.api_token,
		app_id				: req.body.app_id,
		account_id			: req.body.account_id,
		flag				: req.body.flag,
		mail_id				: req.body.mail_id,
		mail_account_type	: req.body.mail_account_type, 
		account_name		: req.body.account_name,
		imap_host			: req.body.imap_host,
		imap_port			: req.body.imap_port,
		smtp_host			: req.body.smtp_host,
		smtp_port			: req.body.smtp_port,
		email_address		: req.body.email_address,
		password			: atob(btoa(req.body.password)),
		is_ssl 				: req.body.is_ssl  
	};

	// account_id		=  master account_id
	// flag				=  1 : new , 2 : update, 0 : delete
	// mail_account_type =  1 : gmail, 2 : yahoo, 3 :  yandex, 4 : mail, -1 others
	// is_ssl  			=  1 : yes, 0 : no


	if(parseInt(arrParams.mail_account_type) === 1)
	{
		Object.assign(arrParams, 
			{
				imap_host			: "imap.gmail.com",
				imap_port			: "993",
				smtp_host			: "smtp.gmail.com",
				smtp_port			: "465",
			}
		);
	}
	else if(parseInt(arrParams.mail_account_type) === 2)
	{
		Object.assign(arrParams, 
			{
				imap_host			: "imap.mail.yahoo.com",
				imap_port			: "993",
				smtp_host			: "smtp.mail.yahoo.com",
				smtp_port			: "465",
			}
		);
	}
	else if(parseInt(arrParams.mail_account_type) === 3)
	{
		Object.assign(arrParams, 
			{
				imap_host			: "imap.yandex.com",
				imap_port			: "993",
				smtp_host			: "smtp.yandex.com",
				smtp_port			: "465",
			}
		);
	}


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(queParam.length)
		{	
			if(parseInt(arrParams.flag) === 0)
			{
				HelperJazeMail.createUpdateExternalEmailDetails(arrParams, function(returnResult){

					if(parseInt(returnResult)  !== 0)
					{
						//get account list
						HelperJazecom.getHomeModuleAccountList(arrParams, function(returnList){	
							
							if(returnList !== null && parseInt(returnList.length) > 0)
							{
								HelperJazecom.getExternalEmailList(arrParams.account_id, function(external_list){	

									var home_account_list = fast_sort(returnList[0].home_account_list).asc('account_type');

									if(external_list !== null)
									{
										home_account_list = home_account_list.concat(external_list);
									}

									jsonRes = {
										success: '1',
										result: {module_list : { home_account_list : home_account_list, notification_count: returnList[0].notification_count  } },
										successMessage: 'Email account is successfully deleted.',
										errorMessage: errorMessage
									};

									statRes = 201;
									res.status(statRes).json(jsonRes);
									res.end();
								});
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to get details. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
						});
					}
					else
					{
						jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to save email. Please try again." 
						};
			
						res.status(statRes).json(jsonRes);
						res.end();
					}
				});
			}
			else
			{
				//check mail account is valid
				var imap = new Imap();
	
				imap = new Imap({
					user	 : arrParams.email_address,
					password : req.body.password,
					host	 : arrParams.imap_host,
					port	 : arrParams.imap_port,
					secure	 : true,
					tls	 	 :1
				});
			
				imap.connect();

				imap.once('error', function(err) { console.log('err',err);
					jsonRes = {
						success: '0',
						result: {},
						successMessage: successMessage,
						errorMessage: err.textCode
					};

					res.status(201).json(jsonRes);
					res.end();
				});

				imap.once('ready', function() {
											
				imap.end();

					HelperJazeMail.checkExistingExternalEmailAccount(arrParams, function(mail_status){

						if(parseInt(mail_status) === 1)
						{
							HelperJazeMail.createUpdateExternalEmailDetails(arrParams, function(returnResult){
							
								if(parseInt(returnResult)  !== 0)
								{ 	
									//get account list
									HelperJazecom.getHomeModuleAccountList(arrParams, function(returnList){	
									
										if(returnList !== null && parseInt(returnList.length) > 0)
										{ 
											HelperJazecom.getExternalEmailList(arrParams.account_id, function(external_list){	
												
												var home_account_list = fast_sort(returnList[0].home_account_list).asc('account_type');

												if(external_list !== null)
												{
													home_account_list = home_account_list.concat(external_list);
												}

												jsonRes = {
													success: '1',
													result: {module_list : { home_account_list : home_account_list, notification_count: returnList[0].notification_count  } },
													successMessage: 'Email account is successfully saved.',
													errorMessage: errorMessage
												};

												statRes = 201;
												res.status(statRes).json(jsonRes);
												res.end();
											});
										}
										else
										{
											jsonRes = {
												success: success,
												result: {},
												successMessage: successMessage,
												errorMessage: "Unable to get details. Please try again." 
											};

											res.status(statRes).json(jsonRes);
											res.end();
										}
									});
								}
								else
								{
									jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to save email. Please try again." 
									};
						
									res.status(statRes).json(jsonRes);
									res.end();
								}
							});
						}
						else
						{
							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "The account already exists. Please use a different account" 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}
					});
				});
			}
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});



router.post('/jazemail/mail_list_refresh', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),

	check('flag').isLength({ min: 1 }),
	check('mail_id').isLength({ min: 1 }),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  api_token = req.body.api_token;
	var  account_id = req.body.account_id;
	
	var  folder = req.body.folder;

	if(folder == ''){
		folder = 'INBOX';
	}

	var  mail_id = req.body.mail_id;

	var  datefrom = req.body.datefrom;
	var  dateto = req.body.dateto;

	var arrParams = {		
		account_id: account_id,
		folder: folder,
		mail_id:mail_id
	};

	var arrRows = [];
	var newObj = {};

	var email_ob_arr = [];
	var email_obj = {};


	var arrTokens = {account_id:account_id,api_token:api_token};
	HelperRestriction.checkParametersApiToken(arrTokens, function(queParam){
		if(queParam.length)
		{	
			HelperJazeMail.getEmailAccountDetails(arrParams, function(retAccount){

				var mailDetails = {folder:folder,mail_type:retAccount.mail_type};

				if(retAccount!=null)
				{

					HelperJazeMail.getMailFolder(mailDetails, function(retFolder){

						if(retAccount!=null)
						{
							var ssl = false;
							if(retAccount.is_ssl == 1){
								ssl = true;
							}
					
							var imap = new Imap({
							  	user: retAccount.email_account,
							  	password: atob(retAccount.email_password),
							  	host: retAccount.imap_host,
							  	port: retAccount.imap_port,
		  					  	tls: true,
						  		authTimeout: 30000
							});

							function openInbox(cb) {
							  	imap.openBox(retFolder, true, cb);
							}

						    imap.once('ready', function () {

				    	  	 	if(imap.state != 'authenticated'){ 
							 		imap.connect();
							 	}
							 

					        	openInbox(function (err, box) {


					            if (err){
				            		//console.log('error after open inbox: '+err);
		                			jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "Unable to load mailbox. Please try again." 
									};

									res.status(statRes).json(jsonRes);
									res.end();

					            } 
					            else 
					            {
				            	 	var messageInbox = [];
									var objMessage = {};
					          		var messageStream  = [];


					          		var dateNow = dateFormat(new Date(),'yyyy-mm-dd');
								
				      				var forEmailParams = {
									  	user: retAccount.email_account,
									  	password: atob(retAccount.email_password),
									  	host: retAccount.imap_host,
									  	port: retAccount.imap_port,
									  	tls: ssl,
									  	account_id:account_id,
									  	mail_id:mail_id,
									  	mail_type:retAccount.mail_type,
									  	flag:0
									};	


               		              	//getInboxCountForMain(forEmailParams, function(retInboxCount){

               		              		//var retInboxCount ='0';
						                imap.search(['ALL',['ON', dateNow]], function (err, results) {
				                	    	
						                    if (err)
						                    {
	  										//	console.log('error after imap search: '+err);
					                        	jsonRes = {
													success: success,
													result: {},
													successMessage: successMessage,
													errorMessage: "Unable to load mailbox. Please try again." 
												};

												res.status(statRes).json(jsonRes);
												res.end();
						                    } 
						                    else 
						                    {

			                    
						                        if (!results || !results.length)
						                        {
						                          //  console.log('no mails!!!');

		              								getInboxCountForMain(forEmailParams, function(retInboxCounts){
					              						email_obj = {
		        											return_flag:"1",
		        											list_date:dateNow,
															total_email_count:box.messages.total.toString(),
															total_unread_count:retInboxCounts,
															message_count:results.length.toString(),
															email_list:messageInbox
		                         		 				};

		                         		 				email_ob_arr.push(email_obj);

														jsonRes = {
															success: '1',
																result: {
																	mail_account_list:[],
																	email_object: email_ob_arr
																},
															successMessage: 'Email List',
															errorMessage: errorMessage 
												  		};

														res.status(201).json(jsonRes);
														res.end();


							                         

						                            });

					                               imap.end();
		                               	
						                        } 
						                        else 
						                        {
				                                   	var f = imap.fetch(results, {bodies:''});

							                            f.on('message', function (msg, seqno) {

							                                msg.on('body', function (stream, info) {

		                               			        		msg.once('attributes', function(attributes) {

								                                	async function main(stream,info,results,attributes) {

							                                		   	let parsed = await simpleParser(stream);

						                                		   		var attachment = '0';
														    			if(parsed.attachments.length != 0){
														    				attachment = '1';
														    			}

														
														    			var subject = '';
														    			if (typeof parsed.subject != 'undefined') {
																			subject = parsed.subject;
																		}

																		var message = '';
																		if(parsed.html !== false){
																			message = parsed.html;
																		}
																		
																
				                                						var attri = '1';
				                                						if (attributes.flags === undefined || attributes.flags.length == 0){
				                                							attri = '0'; 
				                                						}
																		
																		var email_acc_par = {jazemail:parsed.from.value[0].address};
																		if(folder.toLowerCase() !='inbox'){
																			email_acc_par = {jazemail:parsed.to.value[0].address};
																		}

																		
        																HelperJazeMail.getEmailForAccDetails(email_acc_par, function(retLogo){	

        																	var new_logo = '';
																			if(retLogo!=null){
																				new_logo = retLogo.logo;
																			}

																			if(folder.toLowerCase() !='inbox'){
 
																	          	objMessage = {
																            		seq:info.seqno.toString(),
																	          		id:parsed.messageId,
																	          		logo:new_logo,
																	          		subject: subject,
														          		    		date:parsed.date,
																					from_account_id   : '',
																          			from_account_type : '',																	          	
																	          		from_name:parsed.from.value[0].name,
																          			from_logo         : '',
																	          		from_email:parsed.from.value[0].address,
																	          		attachment:attachment,
																	          		message:message,
																          			read_status: attri
																	     
																	          	};


																			}else{

																				objMessage = {
																	    
																          			seq:info.seqno.toString(),
																	          		id:parsed.messageId,
																	          		logo:new_logo,
																	          		subject: subject,
													          		    			date:dateFormat(parsed.date,'yyyy-mm-dd HH:MM:ss'),
																					from_account_id   : '',
																          			from_account_type : '',																	          	
																	          		from_name:parsed.from.value[0].name,
																          			from_logo         : '',
																	          		from_email:parsed.from.value[0].address,
																	          		attachment:attachment,
																	          		message:message,
																          			read_status: attri
																	     
																	          	};
																				
																			}

														          			messageInbox.push(objMessage);

												          				});
					                                					

																	}

											
																	main(stream,info,results,attributes);
															    });

							                                });
							                                msg.once('end', function() {
							                         		    //console.log(seqno + ' finished.');
							                                });
							                            });
							                            f.once('error', function(err) {

							                               // console.log('failed to fetch: ' + err);

				                                           	jsonRes = {
																success: success,
																result: {},
																successMessage: successMessage,
																errorMessage: "Unable to load mailbox. Please try again." 
															};

															res.status(statRes).json(jsonRes);
															res.end();

							                            });
							                            f.once('end', function() {
					                            	     	imap.end();
							                            	setTimeout(function(){

						                						if(messageInbox != '')
						                						{
					            	   		                   
		    							            	   			getInboxCountForMain(forEmailParams, function(retInboxCounts){

					            	   									HelperJazeMail.proceessArrayEmails(messageInbox,arrParams, function(retMessageInbox){

		            	   											 		retMessageInbox.sort(function(a, b){
																			    return b.seq-a.seq

																			})

				            	   											email_obj = {
				                    											return_flag:"1",
				                    											list_date:dateNow,
				    															total_email_count:box.messages.total.toString(),
																				total_unread_count:retInboxCounts,
																				message_count:results.length.toString(),
																				email_list:retMessageInbox
						                             		 				};

						                             		 				email_ob_arr.push(email_obj);

		            	   													jsonRes = {
																				success: '1',
																					result: {
																						mail_account_list:[],
																						email_object: email_ob_arr
																					},
																				successMessage: 'Email List',
																				errorMessage: errorMessage 
																	  		};

																			res.status(201).json(jsonRes);
																			res.end();
														           		
														           		});

																		// email_obj = {
			               //      											return_flag:"1",
			               //      											list_date:dateNow,
			    											// 				total_email_count:box.messages.total.toString(),
																		// 	total_unread_count:retInboxCounts,
																		// 	message_count:results.length.toString(),
																		// 	email_list:messageInbox
					             //                 		 				};

					             //                 		 				email_ob_arr.push(email_obj);

																		// jsonRes = {
																		// 	success: '1',
																		// 		result: {
																		// 			mail_account_list:[],
																		// 			email_object: email_ob_arr
																		// 		},
																		// 	successMessage: 'Email List',
																		// 	errorMessage: errorMessage 
																  // 		};

																		// res.status(201).json(jsonRes);
																		// res.end();

									                              	});

							                                 	

						                						}
						                						else
						                						{

						                					
						                							if(parseInt(results.length) > 0){

			            								           		jsonRes = {
																			success: success,
																			result: {},
																			successMessage: successMessage,
																			errorMessage: "Unable to load mailbox. Please try again." 
																		};

																		res.status(statRes).json(jsonRes);
																		res.end();

						                							}else{

	                													email_obj = {
			                    											return_flag:"1",
			                    											list_date:dateNow,
			    															total_email_count:box.messages.total.toString(),
																			total_unread_count:retInboxCount,
																			message_count:results.length.toString(),
																			email_list:messageInbox
					                             		 				};

					                             		 				email_ob_arr.push(email_obj);

																		jsonRes = {
																			success: '1',
																				result: {
																					mail_account_list:[],
																					email_object: email_ob_arr
																				},
																			successMessage: 'Email List',
																			errorMessage: errorMessage 
																  		};

																		res.status(201).json(jsonRes);
																		res.end();

						                							}
					                							

									                               
						                						}

				                						 	}, 500);
					                            	     	imap.end();
							                            });
							                        }
							                    }

							                })//end imap.search

										// });

						            }//imap if result end

						        });
						    });

						    imap.once('error', function(err) {
						        console.log('connection ' +err);
						    });

						    imap.once('end', function() {
						     //   console.log('finished!');
						    });

						    imap.connect();

						}
						else
						{

							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Account not found." 
							};

							res.status(statRes).json(jsonRes);
							res.end();

						}

					});

				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Account not found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();

				}

			});

		}
		else	
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});


router.post('/jazemail/mail_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),

	check('flag').isLength({ min: 1 }),
	check('mail_id').isLength({ min: 1 }),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  api_token = req.body.api_token;
	var  account_id = req.body.account_id;
	
	var  folder = req.body.folder;

	if(folder == ''){
		folder = 'INBOX';
	}

	var  flag = req.body.flag;
	var  mail_id = req.body.mail_id;

	var  datefrom = req.body.datefrom;
	var  dateto = req.body.dateto;

	var arrParams = {		
		account_id: account_id,
		folder: folder,
		flag: flag,
		mail_id:mail_id
	};

	var arrRows = [];
	var newObj = {};

	var email_ob_arr = [];
	var email_obj = {};


	var arrTokens = {account_id:account_id,api_token:api_token};
	HelperRestriction.checkParametersApiToken(arrTokens, function(queParam){
		if(queParam.length)
		{	
	
			HelperJazeMail.getEmailAccountDetails(arrParams, function(retAccount){

				//console.log(retAccount);

				if(retAccount!=null)
				{

					var mailDetails = {folder:folder,mail_type:retAccount.mail_type};
					
					HelperJazeMail.getMailFolder(mailDetails, function(retFolder){

							if(retAccount!=null)
							{
								var ssl = false;
								if(retAccount.is_ssl == 1){
									ssl = true;
								}
						
								var imap = new Imap({
								  	user: retAccount.email_account,
								  	password: atob(retAccount.email_password),
								  	host: retAccount.imap_host,
								  	port: retAccount.imap_port,
								  	tls: true,
								  	authTimeout: 30000
								});

					

								function openInbox(cb) {
								  	imap.openBox(retFolder, true, cb);
								}

							    imap.once('ready', function () {
						    	 	if(imap.state != 'authenticated'){ 
								 		imap.connect();
								 	}
								 	//console.log('imap status: '+imap.state);
						        	openInbox(function (err, box) {

							            if (err)
							            {
						            		//console.log('error after open inbox: '+err);
				                			jsonRes = {
												success: success,
												result: {},
												successMessage: successMessage,
												errorMessage: "Unable to load mailbox. Please try again. /after openInbox. "+err 
											};

											res.status(statRes).json(jsonRes);
											res.end();

							            } 
							            else 
							            {
						            	 	var messageInbox = [];
											var objMessage = {};
							          		var messageStream  = [];

							          		var ArrIUnReadCount = [];
									
											var CalculateFrom;
											var CalculateTo = new Date().setDate(new Date().getDate()+1); 

						            		var range_from;
						                   	var range_to;

					                   		var scope = 'ALL';
											var dateNow = dateFormat(new Date(),'yyyy-mm-dd');

						                   	if(flag != '0' || parseInt(flag) !=0){

			           	           				var CalculateFrom = new Date().setDate(new Date().getDate() - parseInt(flag)); 
			           	           				var newflag = parseInt(flag) - 1;
												var CalculateTo = new Date().setDate(new Date().getDate() - parseInt(newflag)); 

												range_from = dateFormat(CalculateFrom,'yyyy-mm-dd');
												range_to =  dateFormat(CalculateTo,'yyyy-mm-dd');

						                   	}else{

												if(datefrom!='' && dateto !=''){

							                   		range_from = datefrom;
													range_to =  dateto;

												}else{

													range_from = dateFormat(CalculateFrom,'yyyy-mm-dd');
													range_to = dateFormat(CalculateTo,'yyyy-mm-dd');

												}
						                   	}
		 	               				
		 	               					//console.log('date_from '+range_from+'   '+'date_to '+range_to);

		                    				var forEmailParams = {
											  	user: retAccount.email_account,
											  	password: atob(retAccount.email_password),
											  	host: retAccount.imap_host,
											  	port: retAccount.imap_port,
											  	tls: ssl,
											  	account_id:account_id,
											  	mail_id:mail_id,
											  	mail_type:retAccount.mail_type,
											  	date_from:range_from,
											  	date_to:range_to,
											  	flag:flag

											};	

								


							                imap.search(['ALL',['SINCE', range_from], ['BEFORE', range_to]], function (err, results) {
					                	    							                	
						                    	if (err)
						                    	{
				  									//console.log('error after imap search: '+err);
						                        	jsonRes = {
														success: success,
														result: {},
														successMessage: successMessage,
														errorMessage: "Unable to load mailbox. Please try again. /after search. "+err 
													};

													res.status(statRes).json(jsonRes);
													res.end();
						                    	} 
							                    else 
							                    {

							                        if (!results || !results.length)
							                        {
							                          // console.log('no mails!!!');

							                            if(parseInt(flag) == 0){

			              //   	            				HelperJazeMail.getAllEmailAccounts(arrParams, function(retAllAccounts){

			              //   	            					var myemailAccounts = [];
			              //   	            					if(retAllAccounts !=null ){
																	// myemailAccounts = retAllAccounts;
			              //   	            					}
															
																getInboxCountForMain(forEmailParams, function(retInboxCounts){

				    	            					  	      	var newFlag = parseInt(flag) + 1;

																	email_obj = {
				            											return_flag:newFlag.toString(),
				            											list_date:range_from,
																		total_email_count:box.messages.total.toString(),
																		total_unread_count:retInboxCounts,
																		message_count:results.length.toString(),
																		email_list:messageInbox
				                             		 				};

				                             		 				email_ob_arr.push(email_obj);				                             		 				

                     		 										jsonRes = {
																		success: '1',
																			result: {
																				mail_account_list:[],
											
																				email_object: [email_obj]
																			},
																		successMessage: 'Email List',
																		errorMessage: errorMessage 
															  		};

																	res.status(201).json(jsonRes);
																	res.end();

										                            imap.end();
				                             		
				        	            					   	});
			        	            					   	//});

							                            }
							                            else
							                            {


			                        			  	      	var newFlag = parseInt(flag) + 1;
				            					  	      	
															email_obj = {
			        											return_flag:newFlag.toString(),
			        											list_date:range_from,
																total_email_count:box.messages.total.toString(),
																total_unread_count:'0',
																message_count:results.length.toString(),
																email_list:messageInbox
			                         		 				};

			                         		 				email_ob_arr.push(email_obj);


				            								jsonRes = {
																success: '1',
																	result: {
																		mail_account_list:[],
																		email_object: [email_obj]
																	},
																successMessage: 'Email List',
																errorMessage: errorMessage 
													  		};

															res.status(201).json(jsonRes);
															res.end();

							                            }
				                               
							                        } 
							                        else 
							                        {
				                                   		var f = imap.fetch(results, {bodies:''});

							                            f.on('message', function (msg, seqno) {

							                                msg.on('body', function (stream, info) {

			                                		        	msg.once('attributes', function(attributes) {

								                                	async function main(stream,info,results,attributes) {

							                                		   	let parsed = await simpleParser(stream);

						                                		   		var attachment = '0';
														    			if(parsed.attachments.length != 0){
														    				attachment = '1';
														    			}

																		var message = '';
																		if(parsed.html !== false){
																			message = parsed.html;
																		}else if(parsed.text !== false){
																			message = parsed.text;
																		}

														
														    			var subject = '';
														    			if (typeof parsed.subject != 'undefined') {
																			subject = parsed.subject;
																		}

				                                						var attri = '1';
				                                						if (attributes.flags === undefined || attributes.flags.length == 0){
				                                							attri = '0'; 
				                                						}

																		var email_acc_par = {jazemail:parsed.from.value[0].address};
																		if(folder.toLowerCase() !='inbox'){
																			email_acc_par = {jazemail:parsed.to.value[0].address};
																		}

																		
        																HelperJazeMail.getEmailForAccDetails(email_acc_par, function(retLogo){	

        																	var new_logo = '';
																			if(retLogo!=null){
																				new_logo = retLogo.logo;
																			}


																			if(folder.toLowerCase() !='inbox'){
 
																	          	objMessage = {
																	          		seq:info.seqno.toString(),
																	          		id:parsed.messageId,
																	          		logo:new_logo,
																	          		subject: subject,
														          		    		date:dateFormat(parsed.date,'yyyy-mm-dd HH:MM:ss'),
																					from_account_id   : '',
																          			from_account_type : '',																	          	
																	          		from_name:parsed.from.value[0].name,
																          			from_logo         : '',
																	          		from_email:parsed.from.value[0].address,
																	          		attachment:attachment,
																	          		message:message,
																          			read_status: attri
																	     
																	          	};


																			}else{

																				objMessage = {
																	          		seq:info.seqno.toString(),
																	          		id:parsed.messageId,
																	          		logo:new_logo,
																	          		subject: subject,
														          		    		date:dateFormat(parsed.date,'yyyy-mm-dd HH:MM:ss'),
														          		    		from_account_id   : '',
																          			from_account_type : '',																	          	
																	          		from_name:parsed.from.value[0].name,
																          			from_logo         : '',
																	          		from_email:parsed.from.value[0].address,
																	          		attachment:attachment,
																	          		message:message,
																          			read_status: attri
																	     
																	          	};
																				
																			}

														          			messageInbox.push(objMessage);

												          				});
																	}
																			                            
																	main(stream,info,results,attributes);

															    });

							                                });
							                                msg.once('end', function() {
							                                	imap.end();
							                         		    //console.log(seqno + ' finished.');
							                                });
							                            });
							                            f.once('error', function(err) {

							                               //console.log('failed to fetch: ' + err);

				                                           	jsonRes = {
																success: success,
																result: {},
																successMessage: successMessage,
																errorMessage: "Unable to load mailbox. Please try again. /after imap once. "+err  
															};

															res.status(statRes).json(jsonRes);
															res.end();

							                            });
							                            f.once('end', function() {
															
															imap.end();

													
															getInboxCountForMain(forEmailParams, function(retInboxCount){

																setTimeout(function(){
   														
																  	var newFlag = parseInt(flag) + 1;
																    	      	
																	if(messageInbox != '')
																	{

																  //  		messageInbox.sort(function(a, b){
																		//     return b.seq-a.seq

																		// })


																    	if(parseInt(flag) == 0)
																    	{

																			//HelperJazeMail.getAllEmailAccounts(arrParams, function(retAllAccounts){

																				HelperJazeMail.proceessArrayEmails(messageInbox,arrParams, function(retMessageInbox){

																					// var email_accounts = [];
																					// if(retAllAccounts!=null){
																					// 	email_accounts = retAllAccounts;
																					// }

																					retMessageInbox.sort(function(a, b){
																					    return b.seq-a.seq

																					});
																				

																	 				email_obj = {
																						return_flag:newFlag.toString(),
																 						list_date:range_from,
																 						total_email_count:box.messages.total.toString(),
																						total_unread_count:retInboxCount,
																						message_count:results.length.toString(),
																						email_list:retMessageInbox
																	 				};

																	 				email_ob_arr.push(email_obj);

																					jsonRes = {
																						success: '1',
																							result: {
																								mail_account_list:[],
																								email_object: email_ob_arr
																							},
																						successMessage: 'Email List',
																						errorMessage: errorMessage 
																			  		};

																					res.status(201).json(jsonRes);
																					res.end();

																           		
																           		});

																           	//});

																		}
																		else
																		{

																			HelperJazeMail.proceessArrayEmails(messageInbox,arrParams, function(retMessageInbox){

																				retMessageInbox.sort(function(a, b){
																				    return b.seq-a.seq

																				});

																				email_obj = {
																					return_flag:newFlag.toString(),
																					list_date:range_from,
																					total_email_count:box.messages.total.toString(),
																					total_unread_count:retInboxCount,
																					message_count:results.length.toString(),
																					email_list:retMessageInbox
																 				};

																 				email_ob_arr.push(email_obj);

																				jsonRes = {
																					success: '1',
																						result: {
																							mail_account_list:[],
																							email_object: email_ob_arr
																						},
																					successMessage: 'Email List',
																					errorMessage: errorMessage 
																		  		};

																				res.status(201).json(jsonRes);
																				res.end();

 																			});

																		}

																	}
																	else
																	{


																		if(parseInt(results.length) > 0)
																		{

																       		jsonRes = {
																				success: success,
																				result: {},
																				successMessage: successMessage,
																				errorMessage: "Unable to load mailbox. Please try again. /after checking result. "+err 
																			};

																			res.status(statRes).json(jsonRes);
																			res.end();

																		}
																		else
																		{

																			email_obj = {
																				return_flag:newFlag.toString(),
																				list_date:range_from,
																				total_email_count:box.messages.total.toString(),
																				total_unread_count:retInboxCount,
																				message_count:results.length.toString(),
																				email_list:messageInbox
															 				};

															 				email_ob_arr.push(email_obj);

																			jsonRes = {
																				success: '1',
																					result: {
																						mail_account_list:[],
																						email_object: email_ob_arr
																					},
																				successMessage: 'Email List',
																				errorMessage: errorMessage 
																	  		};

																			res.status(201).json(jsonRes);
																			res.end();

																		}

																	}

																}, 1000);

														  	});//get unread end

															imap.end();

							                            });

							                        }//if else end result


							                    }//end if else err search

							                })//end imap.search
							  

							            }//if else end

							        });//imap openInbox end
							    });//imap ready end
								
							    imap.once('error', function(err) {
							    	imap.end();
							        //console.log('connection ' +err);
							    });

							    imap.once('end', function() {
							    	imap.end();
							       // console.log('finished');
							    });

							    imap.connect();

							}
							else
							{

								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Account not found." 
								};

								res.status(statRes).json(jsonRes);
								res.end();

							}

					});

				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Account not found.." 
					};

					res.status(statRes).json(jsonRes);
					res.end();

				}

			});

		}
		else	
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});



router.post('/jazemail/mail_details', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),

	check('folder').isLength({ min: 1 }),
	check('seq').isLength({ min: 1 }),
	check('date').isLength({ min: 1 }),
	check('mail_id').isLength({ min: 1 }),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  api_token = req.body.api_token;
	var  account_id = req.body.account_id;
	
	var  folder = req.body.folder;
	var  message_id = req.body.seq;
	var  date = req.body.date;
	var  mail_id = req.body.mail_id;

	var arrParams = {		
		account_id: account_id,
		folder: folder,
		message_id: message_id,
		mail_id:mail_id,
		date:date
	};


	var arrRows = [];
	var newObj = {};


	var arrTokens = {account_id:account_id,api_token:api_token};
	HelperRestriction.checkParametersApiToken(arrTokens, function(queParam){
		if(queParam.length)
		{	
			
			HelperJazeMail.getEmailAccountDetails(arrParams, function(retAccount){

				if(retAccount!=null)
				{

					var mailParam = {mail_type:retAccount.mail_type,folder:folder};
					HelperJazeMail.getMailFolder(mailParam, function(retFolder){

						if(retAccount!=null)
						{

							var ssl = false;
							if(retAccount.is_ssl == 1){
								ssl = true;
							}
					
							var imap = new Imap({
							  	user: retAccount.email_account,
							  	password: atob(retAccount.email_password),
							  	host: retAccount.imap_host,
							  	port: retAccount.imap_port,
							  	tls: true,
							  	authTimeout: 30000
							});

							function openInbox(cb) {
							  	imap.openBox(retFolder, false, cb);
							}

						    imap.once('ready', function () {

	    		    	  	 	if(imap.state != 'authenticated'){ 
							 		imap.connect();
							 	}
							 //	console.log('imap status: '+imap.state);

					        openInbox(function (err, box) {
					      
					            if (err){
				            	//  	console.log("error after open inbox: ",err);
		                			jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "Unable to load mailbox. Please try again. "+err
									};

									res.status(201).json(jsonRes);
									res.end();

					            } 
					            else 
					            {
				            	 	let messageInbox = [];
									let objMessage = {};

		     						var attachArr = [];
		 							var objAttach = {};
									var seq_number;


          			                imap.search([ 'ALL', ['ON', date] ], function (err, results) {
					                    if (err){
	  									//	console.log("error after imap search: ",err);
				                        	jsonRes = {
												success: success,
												result: {},
												successMessage: successMessage,
												errorMessage: "Unable to load mailbox. Please try again. "+err
											};

											res.status(201).json(jsonRes);
											res.end();
					                    } 
					                    else 
					                    {
					                        if (!results || !results.length){
					                         //   console.log('no mails!!!');
		                                     	jsonRes = {
													success: success,
													result: {},
													successMessage: successMessage,
													errorMessage: "Mailbox is empty." 
												};

												res.status(201).json(jsonRes);
												res.end();

					                            imap.end();
					                        } 
					                        else 
					                        {

			                        		  	imap.setFlags(results, ['\\Seen'], function(err) {
									                if (!err) {
									                  //  console.log("marked as read");
									                }
									            });

						                           	var f = imap.fetch(results, {bodies:''});
						                          
						                            f.on('message', function (msg, seqno) {
						                                msg.on('body', function (stream, info) {
															
													
						                                	if(seqno == message_id)
						                                	{ 
			                                		          	async function main(stream) {

		                                		 			 	let parsed = await simpleParser(stream);

									          						if(parsed.attachments.length > 0)
												    				{
												    	
												    					parsed.attachments.forEach(function(row,key) {
																	
																			objAttach = {
														    					file_name:row.filename,
														    					file_type:row.contentType,
														    					file_size:row.size.toString()
														    				};

										    								attachArr.push(objAttach);

																	  	});
											    						
													    			}

							            							messageInbox.push(parsed);
						            								seq_number = seqno;
																}

																main(stream);

						                                	}
						                     
						                                });
						                                msg.once('end', function() {
						                         		    //console.log(seqno + ' finished.');
						                                });
						                            });
						                            f.once('error', function(err) {
						                               // console.log('failed to fetch: ' + err);

			                                           	jsonRes = {
															success: success,
															result: {},
															successMessage: successMessage,
															errorMessage: "Unable to load mailbox. Please try again. "+err
														};

														res.status(201).json(jsonRes);
														res.end();

						                            });
						                            f.once('end', function() {
		   												
						                            	imap.end();
														setTimeout(function(){

															var cc  = [];
															var bcc = [];
															var to  = [];
															var from  = '';
															
							             					if(messageInbox != ''){

				     						       				messageInbox.forEach(function(row) {
																																
																	if (row.headers.has('cc')) {
										  				        		cc = row.headers.get('cc').value;
																	}

																	if (row.headers.has('bcc')) {
										  				        		bcc = row.headers.get('bcc').value;
																	}
																				        
																	if (row.headers.has('to')) {
										  				        		to = row.headers.get('to').value;
																	}
											
																	var message = '';
																	if(row.html !== false){
																		message = row.html;
																	}else if(row.text !== false){
																		message = row.text;
																	}

																	var subject = '';
													    			if (typeof row.subject != 'undefined') {
																		subject = row.subject;
																	}

					    											objMessage = {
																		seq               : seq_number.toString(),
										          		          		id                : row.messageId,
														          		subject           : subject,
											          		    		date              : dateFormat(row.date,'yyyy-mm-dd HH:MM:ss'),
							          		    			    		from_account_id   : '',
											          		    		from_account_type : '',
											          		    		from_name         : row.from.value[0].name,
											          					from_logo         : '',
														        		from_email        : row.from.value[0].address,												        
														          		to                : '',
														          		cc                : '',
														          		bcc               : '',
														          		attachment        : attachArr,
														          		message           : message
														     
														          	};

											          	         	from = row.from.value[0].address;
														   
															  	});

													
				     						       				// getting the details of accounts per emails
																HelperJazeMail.getAccountCredentialsFrom(from, function(retFrom){	
												
																	HelperJazeMail.getAccountCredentialsTO(to, function(retTO){	

																		HelperJazeMail.getAccountCredentialsCC(cc, function(retCC){	

																			HelperJazeMail.getAccountCredentialsBCC(bcc, function(retBCC){	
																										
																				if(retFrom.name!="" && typeof retFrom.name !== "undefined"){objMessage.from_name = retFrom.name;}

																				objMessage.from_logo         = retFrom.logo;
																				objMessage.from_account_id   = retFrom.account_id;
																				objMessage.from_account_type = retFrom.account_type;

																				objMessage.to  = retTO;
																				objMessage.cc  = retCC;
																				objMessage.bcc = retBCC;
																	
																				jsonRes = {
																					success: '1',
																						result: {
																							email_details:objMessage
																						},
																					successMessage: 'Email Details',
																					errorMessage: errorMessage 
																		  		};

																				res.status(201).json(jsonRes);
																				res.end();
						  												
																			});
																		});
																	});
																});

																//update the read status
																var notiDetails = {receiver_account_id:account_id, mail_id:objMessage.id};
																HelperJazeMail.getNotificationDetails(notiDetails, function(retUrnUpdate){});  

							             					}else{

																jsonRes = {
																	success: success,
																	result: {},
																	successMessage: successMessage,
																	errorMessage: "Mail not found." 
																};

																res.status(201).json(jsonRes);
																res.end();

							             					}	

						             				

			             							 	}, 1000);	                    
						                               	imap.end();
						                            });
						                        }
						                    }
						                })
						            }
						        });
						    });

						    imap.once('error', function(err) {
					    	 	imap.end();
						        console.log(err);
						    });

						    imap.once('end', function() {
					    	 	imap.end();
						     //   console.log('finished!');
						    });

						    imap.connect();

					    }
					    else
					    {

	    					jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Account not found." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
	
					    }


			    	});

				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Account not found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();

				}

			});

		}
		else	
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});


router.post('/jazemail/get_mail_details', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('group_id').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  api_token = req.body.api_token;
	var  account_id = req.body.account_id;
	var  group_id = req.body.group_id;

	var arrGroupId = group_id.split("|");

	var arrParams = {		
		account_id: account_id,
		to_email:arrGroupId[0],
		message_id:arrGroupId[1],
		date:arrGroupId[2],
	};

	var arrRows = [];
	var newObj = {};
	var account_list = {};

	var arrTokens = {account_id:account_id,api_token:api_token};
	HelperRestriction.checkParametersApiToken(arrTokens, function(queParam){
		if(queParam.length)
		{	
			HelperJazeMail.getEmailAccountPerParamEmail(arrParams, function(retAccount){ 

				if(retAccount!=null)
				{

					var ssl = false;
					if(retAccount.is_ssl == 1){
						ssl = true;
					}
			
					var imap = new Imap({
					  	user: retAccount.email_account,
					  	password: atob(retAccount.email_password),
					  	host: retAccount.imap_host,
					  	port: retAccount.imap_port,
	  				  	tls: true,
					  	authTimeout: 30000
					});

					function openInbox(cb) {
					  	imap.openBox('INBOX', false, cb);
					}

				    imap.once('ready', function () {

	    	  	 		if(imap.state != 'authenticated'){ 
					 		imap.connect();
					 	}
					 //	console.log('imap status: '+imap.state);

			        openInbox(function (err, box) {
			            if (err){
			            //	console.log('error after open inbox: '+err);
                			jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Unable to load mailbox. Please try again." 
							};

							res.status(201).json(jsonRes);
							res.end();

			            } 
			            else 
			            {
		            	 	let messageInbox = [];
							let objMessage = {};

     						var attachArr = [];
 							var objAttach = {};
							var seq_number;


  			                imap.search([ 'ALL', ['ON', arrParams.date] ], function (err, results) {
			                    if (err){
  									// console.log('error after imap search: '+err);
		                        	jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "Unable to load mailbox. Please try again." 
									};

									res.status(201).json(jsonRes);
									res.end();
			                    } 
			                    else 
			                    {
			                        if (!results || !results.length){
			                            // console.log('no mails!!!');
                                     	jsonRes = {
											success: success,
											result: {},
											successMessage: successMessage,
											errorMessage: "Mailbox is empty." 
										};

										res.status(201).json(jsonRes);
										res.end();

			                            imap.end();
			                        } 
			                        else 
			                        {

		                        	  	imap.setFlags(results, ['\\Seen'], function(err) {
							                if (!err) {
							                   // console.log("marked as read");
							                }
							            });

			                            //console.log('the length of results:'+results.length);
				                           	var f = imap.fetch(results, {bodies:''});
				                          
				                            f.on('message', function (msg, seqno) {
				                                msg.on('body', function (stream, info) {
				             
                                   		          	async function main(stream) {

                            		 			 		let parsed = await simpleParser(stream);

                            		 			 		if(parsed.messageId == arrParams.message_id)
                            		 			 		{
		 			 										if(parsed.attachments.length > 0)
									    					{
										    	
										    					parsed.attachments.forEach(function(row,key) {
															
																	objAttach = {
										    	    					file_name:row.filename,
												    					file_type:row.contentType,
												    					file_size:row.size.toString()
												    				};

								    								attachArr.push(objAttach);

															  	});
									    						
											    			}
									    					seq_number = seqno;
					            							messageInbox.push(parsed);
                            		 			 		}
													}

													main(stream);
				                     
				                                });
				                                msg.once('end', function() {
				                         		    //console.log(seqno + ' finished.');
				                                });
				                            });
				                            f.once('error', function(err) {
				                              //  console.log('failed to fetch: ' + err);

	                                           	jsonRes = {
													success: success,
													result: {},
													successMessage: successMessage,
													errorMessage: "Unable to load mailbox. Please try again." 
												};

												res.status(201).json(jsonRes);
												res.end();

				                            });
				                            f.once('end', function() {
											 	imap.end();
										 		HelperJazeMail.getAccountIDForEmailList(arrParams, function(retAllAccounts){
													setTimeout(function(){


														var cc  = [];
														var bcc = [];
														var to  = [];
														var from  = '';
														
						             					if(messageInbox != ''){

			     						       				messageInbox.forEach(function(row) {
																
																var cc = [];
																if (row.headers.has('cc')) {
									  				        		cc = row.headers.get('cc').value;
																}

																var bcc = [];
																if (row.headers.has('bcc')) {
									  				        		bcc = row.headers.get('bcc').value;
																}

				    											var to = '';
																if (row.headers.has('to')) {
									  				        		to = row.headers.get('to').value;
																}

																
																var message = '';
																if(row.html !== false){
																	message = row.html;
																}else if(row.text !== false){
																	message = row.text;
																}

															
				    											objMessage = {
																	seq:seq_number.toString(),
									          		          		id:row.messageId,
													          		subject: row.subject,
										          		    		date:dateFormat(row.date,'yyyy-mm-dd HH:MM:ss'),
									          		    			from_account_id:'',
										          		    		from_account_type: '',
										          		    		from_name:'',
										          					from_logo:'',
													        		from_email:row.from.value[0].address,
											          				from_name:row.from.value[0].name,
													          		to:to,
													          		cc:'',
													          		bcc:'', 
													          		attachment:attachArr,
													          		message:message
															     
													          	};

											          	      	from = row.from.value[0].address;
													   
														  	});

  		                	            					HelperJazeMail.getAllEmailAccounts(arrParams, function(retAllAccounts){

			                	            					var myemailAccounts = [];
			                	            					if(retAllAccounts !=null ){
																	myemailAccounts = retAllAccounts;
			                	            					}

											       				// getting the details of accounts per emails
																HelperJazeMail.getAccountCredentialsFrom(from, function(retFrom){	
												
																	HelperJazeMail.getAccountCredentialsTO(to, function(retTO){	

																		HelperJazeMail.getAccountCredentialsCC(cc, function(retCC){	

																			HelperJazeMail.getAccountCredentialsBCC(bcc, function(retBCC){	
																										
																				if(retFrom.name!="" && typeof retFrom.name !== "undefined"){objMessage.from_name = retFrom.name;}
																				objMessage.from_logo         = retFrom.logo;
																				objMessage.from_account_id   = retFrom.account_id;
																				objMessage.from_account_type = retFrom.account_type;

																				objMessage.to  = retTO;
																				objMessage.cc  = retCC;
																				objMessage.bcc = retBCC;
																	
																				jsonRes = {
																					success: '1',
																						result: {
																							mail_account_list:myemailAccounts,
																							email_details:objMessage
																						},
																					successMessage: 'Get Email Details',
																					errorMessage: errorMessage 
																		  		};

																				res.status(201).json(jsonRes);
																				res.end();
						  												
																			});
																		});
																	});
																});

															});

															//update the read status
															var notiDetails = {receiver_account_id:account_id, mail_id:objMessage.id};
															HelperJazeMail.getNotificationDetails(notiDetails, function(retUrnUpdate){});  

						             					}else{

															jsonRes = {
																success: success,
																result: {},
																successMessage: successMessage,
																errorMessage: "Email not found." 
															};

															res.status(201).json(jsonRes);
															res.end();

						             					}	

					             				

		             							 	}, 1000);	 
         							 	       	});             
				                               	imap.end();
				                            });
				                        }
				                    }
				                })
				            }
				        });
				    });

				    imap.once('error', function(err) {
				        console.log(err);
			         	imap.end();
				    });

				    imap.once('end', function() {
			    	 	imap.end();
				      //  console.log('finished!');
				    });

				    imap.connect();


				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Account not found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();	
				}

			});

		}
		else	
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});
// router.post('/jazemail/get_mail_details', [

// 	check('api_token').isLength({ min: 1 }),
// 	check('account_id').isNumeric(),
// 	check('group_id').isLength({ min: 1 })

// 	], (req,res,next) => {

// 	var _send = res.send;
// 	var sent = false;

// 	res.send = function(data){
// 	   if(sent) return;
// 	   _send.bind(res)(data);
// 	   sent = true;
// 	};

//   	var errors = validationResult(req);

//   	if (!errors.isEmpty()) {
//   		jsonRes = {
// 			success: success,
// 			result: {},
// 			successMessage: successMessage,
// 			errorMessage: "No mandatory fileds entered." 
//   		};

// 		res.status(statRes).json(jsonRes);
//  		res.end();
//   	}

// 	var  api_token = req.body.api_token;
// 	var  account_id = req.body.account_id;
// 	var  group_id = req.body.group_id;

// 	var arrGroupId = group_id.split("|");

// 	var arrParams = {		
// 		account_id: account_id,
// 		to_email:arrGroupId[0],
// 		message_id:arrGroupId[1],
// 		date:arrGroupId[2],
// 	};

// 	var arrRows = [];
// 	var newObj = {};
// 	var account_list = {};

// 	var arrTokens = {account_id:account_id,api_token:api_token};
// 	HelperRestriction.checkParametersApiToken(arrTokens, function(queParam){
// 		if(queParam.length)
// 		{	
// 			HelperJazeMail.getEmailAccountPerParamEmail(arrParams, function(retAccount){ 

// 				if(retAccount!=null)
// 				{

// 					var ssl = false;
// 					if(retAccount.is_ssl == 1){
// 						ssl = true;
// 					}
			
// 					var imap = new Imap({
// 					  	user: retAccount.email_account,
// 					  	password: atob(retAccount.email_password),
// 					  	host: retAccount.imap_host,
// 					  	port: retAccount.imap_port,
// 	  				  	tls: true,
// 					  	authTimeout: 30000
// 					});

// 					function openInbox(cb) {
// 					  	imap.openBox('INBOX', false, cb);
// 					}

// 				    imap.once('ready', function () {

// 	    	  	 		if(imap.state != 'authenticated'){ 
// 					 		imap.connect();
// 					 	}
// 					 //	console.log('imap status: '+imap.state);

// 			        openInbox(function (err, box) {
// 			            if (err){
// 			            //	console.log('error after open inbox: '+err);
//                 			jsonRes = {
// 								success: success,
// 								result: {},
// 								successMessage: successMessage,
// 								errorMessage: "Unable to load mailbox. Please try again." 
// 							};

// 							res.status(201).json(jsonRes);
// 							res.end();

// 			            } 
// 			            else 
// 			            {
// 		            	 	let messageInbox = [];
// 							let objMessage = {};

//      						var attachArr = [];
//  							var objAttach = {};
// 							var seq_number;


//   			                imap.search([ 'ALL', ['ON', arrParams.date] ], function (err, results) {
// 			                    if (err){
//   									// console.log('error after imap search: '+err);
// 		                        	jsonRes = {
// 										success: success,
// 										result: {},
// 										successMessage: successMessage,
// 										errorMessage: "Unable to load mailbox. Please try again." 
// 									};

// 									res.status(201).json(jsonRes);
// 									res.end();
// 			                    } 
// 			                    else 
// 			                    {
// 			                        if (!results || !results.length){
// 			                            // console.log('no mails!!!');
//                                      	jsonRes = {
// 											success: success,
// 											result: {},
// 											successMessage: successMessage,
// 											errorMessage: "Mailbox is empty." 
// 										};

// 										res.status(201).json(jsonRes);
// 										res.end();

// 			                            imap.end();
// 			                        } 
// 			                        else 
// 			                        {

// 		                        	  	imap.setFlags(results, ['\\Seen'], function(err) {
// 							                if (!err) {
// 							                   // console.log("marked as read");
// 							                }
// 							            });

// 			                            //console.log('the length of results:'+results.length);
// 				                           	var f = imap.fetch(results, {bodies:''});
				                          
// 				                            f.on('message', function (msg, seqno) {
// 				                                msg.on('body', function (stream, info) {
				             
//                                    		          	async function main(stream) {

//                             		 			 		let parsed = await simpleParser(stream);

//                             		 			 		if(parsed.messageId == arrParams.message_id)
//                             		 			 		{
// 		 			 										if(parsed.attachments.length > 0)
// 									    					{
										    	
// 										    					parsed.attachments.forEach(function(row,key) {
															
// 																	objAttach = {
// 										    	    					file_name:row.filename,
// 												    					file_type:row.contentType,
// 												    					file_size:row.size.toString()
// 												    				};

// 								    								attachArr.push(objAttach);

// 															  	});
									    						
// 											    			}
// 									    					seq_number = seqno;
// 					            							messageInbox.push(parsed);
//                             		 			 		}
// 													}

// 													main(stream);
				                     
// 				                                });
// 				                                msg.once('end', function() {
// 				                         		    //console.log(seqno + ' finished.');
// 				                                });
// 				                            });
// 				                            f.once('error', function(err) {
// 				                              //  console.log('failed to fetch: ' + err);

// 	                                           	jsonRes = {
// 													success: success,
// 													result: {},
// 													successMessage: successMessage,
// 													errorMessage: "Unable to load mailbox. Please try again." 
// 												};

// 												res.status(201).json(jsonRes);
// 												res.end();

// 				                            });
// 				                            f.once('end', function() {
// 											 	imap.end();
// 										 		HelperJazeMail.getAccountIDForEmailList(arrParams, function(retAllAccounts){
// 													setTimeout(function(){
														
// 						             					if(messageInbox != ''){

// 			     						       				messageInbox.forEach(function(row) {
																
// 																var cc = [];
// 																if (row.headers.has('cc')) {
// 									  				        		cc = row.headers.get('cc').value;
// 																}

// 																var bcc = [];
// 																if (row.headers.has('bcc')) {
// 									  				        		bcc = row.headers.get('bcc').value;
// 																}

// 				    											var to = '';
// 																if (row.headers.has('to')) {
// 									  				        		to = row.headers.get('to').value;
// 																}

																
// 																var message = '';
// 																if(row.html !== false){
// 																	message = row.html;
// 																}else if(row.text !== false){
// 																	message = row.text;
// 																}

															
// 				    											objMessage = {
// 																	seq:seq_number.toString(),
// 									          		          		id:row.messageId,
// 													          		subject: row.subject,
// 										          		    		date:dateFormat(row.date,'yyyy-mm-dd HH:MM:ss'),
// 													        		from_email:row.from.value[0].address,
// 											          				from_name:row.from.value[0].name,
// 													          		to:to,
// 													          		cc:cc,
// 													          		bcc:bcc, 
// 													          		attachment:attachArr,
// 													          		message:message
															     
// 													          	};
													   
// 														  	});

// 															jsonRes = {
// 																success: '1',
// 																	result: {
// 																		mail_account_list:retAllAccounts,
// 																		email_details:objMessage
// 																	},
// 																successMessage: 'Get Email Details',
// 																errorMessage: errorMessage 
// 													  		};

// 															res.status(201).json(jsonRes);
// 															res.end();

// 															//update the read status
// 															var notiDetails = {receiver_account_id:account_id, mail_id:objMessage.id};
// 															HelperJazeMail.getNotificationDetails(notiDetails, function(retUrnUpdate){});  

// 						             					}else{

// 															jsonRes = {
// 																success: success,
// 																result: {},
// 																successMessage: successMessage,
// 																errorMessage: "Email not found." 
// 															};

// 															res.status(201).json(jsonRes);
// 															res.end();

// 						             					}	

					             				

// 		             							 	}, 1000);	 
//          							 	       	});             
// 				                               	imap.end();
// 				                            });
// 				                        }
// 				                    }
// 				                })
// 				            }
// 				        });
// 				    });

// 				    imap.once('error', function(err) {
// 				        console.log(err);
// 			         	imap.end();
// 				    });

// 				    imap.once('end', function() {
// 			    	 	imap.end();
// 				      //  console.log('finished!');
// 				    });

// 				    imap.connect();


// 				}
// 				else
// 				{

// 					jsonRes = {
// 						success: success,
// 						result: {},
// 						successMessage: successMessage,
// 						errorMessage: "Account not found." 
// 					};

// 					res.status(statRes).json(jsonRes);
// 					res.end();	
// 				}

// 			});

// 		}
// 		else	
// 		{

// 			jsonRes = {
// 				success: success,
// 				result: {},
// 				successMessage: successMessage,
// 				errorMessage: "Device not found." 
// 			};

// 			res.status(statRes).json(jsonRes);
// 			res.end();

// 		}

// 	});

// });


router.post('/jazemail/download_attachment', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),

	check('mail_id').isNumeric(),

	check('seq').isLength({ min: 1 }),
	check('folder').isLength({ min: 1 }),

	check('date').isLength({ min: 1 }),
	check('flag').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  api_token = req.body.api_token;
	var  account_id = req.body.account_id;
	
	var  folder = req.body.folder;
	var  message_id = req.body.seq;
	var  attach_file_name = req.body.file_name;
	var  date = req.body.date;
	var  flag = req.body.flag;

	var arrParams = {		
		account_id: account_id,
		folder: folder,
		message_id: message_id,
		mail_id:req.body.mail_id,
		date:date,
		flag:flag
	};




	var arrRows = [];
	var newObj = {};

	var arrTokens = {account_id:account_id,api_token:api_token};
	HelperRestriction.checkParametersApiToken(arrTokens, function(queParam){
		if(queParam.length)
		{	
			
			HelperJazeMail.getEmailAccountDetails(arrParams, function(retAccount){

				if(retAccount!=null)
				{

					var mailDetails = {folder:folder,mail_type:retAccount.mail_type};
					HelperJazeMail.getMailFolder(mailDetails, function(retFolder){

						if(retAccount!=null)
						{

							var ssl = false;
							if(retAccount.is_ssl == 1){
								ssl = true;
							}
					
							var imap = new Imap({
							  	user: retAccount.email_account,
							  	password: atob(retAccount.email_password),
							  	host: retAccount.imap_host,
							  	port: retAccount.imap_port,
						  	  	tls: true,
						  		authTimeout: 30000
							});

							function openInbox(cb) {
							  	imap.openBox(retFolder, true, cb);
							}

						    imap.once('ready', function () {

    			    	  	 	if(imap.state != 'authenticated'){ 
							 		imap.connect();
							 	}
							 //	console.log('imap status: '+imap.state);

					        openInbox(function (err, box) {
					            if (err){

		                			jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "Unable to load mailbox. Please try again." 
									};

									res.status(201).json(jsonRes);
									res.end();

					            } 
					            else 
					            {
				            	 	let messageInbox = [];
									let objMessage = {};

		     						var attachArr = [];
		 							var objAttach = {};
		 	
				              	 	// console.log('the total of message:');
					               	// console.log(box);
					                //['SINCE', 'May 30, 2018']
					                //['UNSEEN', ['SUBJECT', 'HY']]
				                   	//[ 'ALL', ['SINCE', '2019-07-01'], ['BEFORE', '2019-0-30'] ]

		       // 	                   	var today = new Date();
									// var tomorrow = new Date();
									// var newNow = tomorrow.setDate(today.getDate()+1); 

						 		// 	var currentMonth = new Date();
									// var lastMonth = currentMonth.setMonth(currentMonth.getMonth() - 1);

				     //               	var dateNowAdd1Day = dateFormat(newNow,'yyyy-mm-dd');
				     //               	var dateLastMonth =  dateFormat(lastMonth,'yyyy-mm-dd');

				                 	imap.search([ 'ALL', ['ON', date] ], function (err, results) {
					                    if (err){
		  
				                        	jsonRes = {
												success: success,
												result: {},
												successMessage: successMessage,
												errorMessage: "Unable to load mailbox. Please try again." 
											};

											res.status(201).json(jsonRes);
											res.end();
					                    } 
					                    else 
					                    {
					                        if (!results || !results.length){
					                          //  console.log('no mails!!!');
		                                     	jsonRes = {
													success: success,
													result: {},
													successMessage: successMessage,
													errorMessage: "Mailbox is empty." 
												};

												res.status(201).json(jsonRes);
												res.end();

					                            imap.end();
					                        } 
					                        else 
					                        {
					                            //console.log('the length of results:'+results.length);
						                           	var f = imap.fetch(results, {bodies:''});
						                          
						                            f.on('message', function (msg, seqno) {
						                                msg.on('body', function (stream, info) {

						                                	if(seqno == message_id)
						                                	{

							                                    simpleParser(stream, function (err, mail) {
							                  												          
						   		  									if(mail.attachments.length > 0)
												    				{
												    					//var mail_path = "./uploads/jazemail/"+account_id;
												    					var mail_path = G_STATIC_IMG_PATH+'jazemail/'+account_id;
												    					//var inner_mail_path = "./uploads/jazemail/"+account_id+"/"+retAccount.email_account;
												    					var inner_mail_path = G_STATIC_IMG_PATH+'jazemail/'+account_id;

																		if(!fs.existsSync(mail_path)){
																			fs.mkdirSync(mail_path,'0757', true);    

																			if(!fs.existsSync(inner_mail_path))  {
																				fs.mkdirSync(inner_mail_path,'0757', true);    
																			}       
																		}else{

																			if(!fs.existsSync(inner_mail_path))  {
																				fs.mkdirSync(inner_mail_path,'0757', true);    
																			}   
																		}


																		if(parseInt(flag)==1)
											    						{

										    								mail.attachments.forEach(function(row,key) {

									    										if(row.filename == attach_file_name){
								    												require("fs").writeFile(G_STATIC_IMG_PATH+'jazemail/'+account_id+'/'+row.filename, row.content, 'base64', function(err) {});

																					var file_name = row.filename;
																					var attachment_type =  getFileTypes('.'+file_name.substring(file_name.indexOf(".") + 1));
																					var stats = fs.statSync(G_STATIC_IMG_PATH+'jazemail/'+account_id+'/'+file_name);

																					objAttach = {
																						file_name : file_name,
																						file_url  : G_STATIC_IMG_URL+'uploads/jazenet/jazemail/'+account_id+'/'+file_name,
																						file_size : formatSizeUnits(stats.size).toString(),
																						file_type : attachment_type.toString(),
																						//file_type : row.contentType
																					};
															    				}
								    									  	});	

		    									  	    					attachArr.push(objAttach);

										    							}
										    							else
										    							{
							    											mail.attachments.forEach(function(row,key) {

																				require("fs").writeFile(G_STATIC_IMG_PATH+'jazemail/'+account_id+'/'+row.filename, row.content, 'base64', function(err) {});

																				var file_name = row.filename;
																				var attachment_type =  getFileTypes('.'+file_name.substring(file_name.indexOf(".") + 1));
																				var stats = fs.statSync(G_STATIC_IMG_PATH+'jazemail/'+account_id+'/'+file_name);
																				
																				objAttach = {
																					file_name : file_name,
																					file_url  : G_STATIC_IMG_URL+'uploads/jazenet/jazemail/'+account_id+'/'+file_name,
																					file_size : formatSizeUnits(stats.size).toString(),
																					file_type : attachment_type.toString(),
																					//file_type : row.contentType
															    				};

														    				  	attachArr.push(objAttach);
								    									  	});
										    							}
													    			}
							                                    })
						                                    }
						                                });
						                                msg.once('end', function() {
						                                	imap.end();
						                         		    //console.log(seqno + ' finished.');
						                                });
						                            });
						                            f.once('error', function(err) {
						                              //  console.log('failed to fetch: ' + err);

			                                           	jsonRes = {
															success: success,
															result: {},
															successMessage: successMessage,
															errorMessage: "Unable to load mailbox. Please try again." 
														};

														res.status(201).json(jsonRes);
														res.end();

						                            });
						                            f.once('end', function() {
						                            	imap.end();
				                            			setTimeout(function(){
															
							             					if(attachArr != ''){

																jsonRes = {
																	success: '1',
																		result: {
																			attachments:attachArr
																		},
																	successMessage: 'Email attachment',
																	errorMessage: errorMessage 
														  		};

																res.status(201).json(jsonRes);
																res.end();


							             					}else{

																jsonRes = {
																	success: success,
																	result: {},
																	successMessage: successMessage,
																	errorMessage: "Unable to fetch files. Please try again." 
																};

																res.status(201).json(jsonRes);
																res.end();

							             					}		                    

						                         

		                                			 	}, 1000);

						                            });
						                        }
						                    }
						                })
						            }
						        });
						    });

						    imap.once('error', function(err) {
						    	imap.end();
						     //   console.log(err);
						    });

						    imap.once('end', function() {
						    	imap.end();
						      //  console.log('finished!');
						    });

						    imap.connect();

						}
						else
						{

							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Account not found." 
							};

							res.status(statRes).json(jsonRes);
							res.end();

						}

				   	});

				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Account not found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();

				}

			});

		}
		else	
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});



router.post('/jazemail/send_email', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('mail_id').isLength({ min: 1 }),

	check('to_email').isLength({ min: 1 }),
	check('subject').isLength({ min: 1 }),
	check('message').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  api_token = req.body.api_token;
	var  account_id = req.body.account_id;
	var  mail_id = req.body.mail_id;
	
	var  to_email = req.body.to_email;
	var  to_cc   = req.body.to_cc;
	var  to_bcc   = req.body.to_bcc;
	var  subject = req.body.subject;
	var  message = req.body.message;

	var  file_name = req.body.file_name;
	var  file_url = req.body.file_url;

	var arrParams = {		
		account_id: account_id,
		to_email: to_email,
		subject: subject,
		message: message,
		mail_id:mail_id
	};

	var arrFileName ="" , arrFileUrl = "";

	if(typeof file_name !== 'undefined' && file_name !== null && file_name != "")
	{
		arrFileName = file_name.split(",");
	}

	if(typeof file_url !== 'undefined' && file_url !== null && file_url != "")
	{
		arrFileUrl = file_url.split(",");
	}

	var arrToEmailClean = to_email.split(",");
	var arrToCcClean = to_cc.split(",");
	var arrToBCcClean = to_bcc.split(",");

	var arrFilesComplete = [];
	var newObj = {};

 	var source = '/home/chaaby/subdomain/img/images/master.resources/uploads/jazenet/jazemail/';

	var arrTokens = {account_id:account_id,api_token:api_token};
	HelperRestriction.checkParametersApiToken(arrTokens, function(queParam){
		if(queParam.length)
		{	

			HelperJazeMail.getEmailAccountDetails(arrParams, function(retAccount){

				if(retAccount!=null)
				{	


					var  arrToEmails = "'"+to_email.split(",").join("','") + "'";
					HelperJazeMail.getAccountIDperEmailForBlock(arrToEmails,arrParams, function(retToEmailBlock){


			


						var filteredToEmail;
						if(retToEmailBlock!=null){

							filteredToEmails =  arrToEmailClean.filter(function(hero) {
								return !retToEmailBlock.includes(hero);
							});

							filteredToEmail = filteredToEmails.toString();


						}else{

							filteredToEmail = to_email;

						}


						var arrToEmail = filteredToEmail.split(",");

						setTimeout(function(){
							var  arrToCC = "'"+to_cc.split(",").join("','") + "'";
							HelperJazeMail.getAccountIDperEmailForBlock(arrToCC,arrParams, function(retTOCcBlock){
								
								var filteredToCc;
								if(retTOCcBlock!=null){

									filteredToCc =  arrToCcClean.filter(function(hero) {
										return !retTOCcBlock.includes(hero);
									});


								}else{

									filteredToCc = to_cc;
								}
							
								setTimeout(function(){
									var  arrToBcc = "'"+to_bcc.split(",").join("','") + "'";
									HelperJazeMail.getAccountIDperEmailForBlock(arrToBcc,arrParams, function(retToBccBlock){


										var filteredToBcc;
										if(retToBccBlock!=null){

											filteredToBcc =  arrToBCcClean.filter(function(hero) {
												return !retToBccBlock.includes(hero);
											});


										}else{

											filteredToBcc = to_bcc;
										}


										var atobconvert = atob(retAccount.email_password);
										var password = "'" + atobconvert + "'";

										var sendEmail = mailer.config({
									  	    host: retAccount.smtp_host,
										    port: retAccount.smtp_port,
										    secure: true, // true for 465, false for other ports
										    auth: {
										      	user: retAccount.email_account, // generated ethereal user
										      	pass: atobconvert
										      	  // generated ethereal password
										    }
										});

										if((arrFileName.length > 0 && arrFileUrl.length > 0) && (file_name != '' && file_url != '') ){

											arrFilesComplete = arrFileName.map(function (x, i) { 
												var obj = {filename:x,path:arrFileUrl[i]};
												return	obj;
						                  	});
																	
										}

										var message_arr = {
									  	    from: retAccount.email_account, // sender address
										    to: filteredToEmail,
										    cc: filteredToCc,
										    bcc: filteredToBcc,
										    subject: subject, // Subject line
										    //text: message, // plain text body
										    html: message, // html body
									     	attachments: arrFilesComplete
										};

										var toUnlink = source+account_id;

							
										sendEmail(message_arr)
									    .then(function(info){

								

											//send notification process start
											var toCheckEmails = JSON.stringify(arrToEmail).replace(/[\[\]]+/g,"");
											var arrAccountIdtoNoti = [];
											var mail_push_notification = {};
					 	                   	var today = new Date();
					 	                   	var dateNow = dateFormat(today,'yyyy-mm-dd');

											HelperJazeMail.checkIfinCredentials(toCheckEmails, function(retAccountIds){

												

												if(retAccountIds!=null){
													arrAccountIdtoNoti.push(retAccountIds);
												}

												//console.log('accounts for notification ', arrAccountIdtoNoti);

												if(arrAccountIdtoNoti !=null){

													arrAccountIdtoNoti.forEach(function(val,idx) {

														mail_push_notification = {
															group_id:(val[idx].email_address+'|'+info.messageId+'|'+dateNow).toString(),
															account_id:account_id.toString(),
															request_account_id:(val[idx].account_id).toString(),
															msg:subject,
															flag:"12"
														};

														HelperNotification.sendPushNotification(mail_push_notification, function(returnNoti){

														//	console.log(returnNoti);

														});

													});
												}
											
											});
											//send notification process end

											//check receivers list
											HelperJazeMail.getReceiverList(arrParams, function(returnReceivers){

												var toInsertEmail;
												var toInsertGroupId;
												var newFiltered;

												if(returnReceivers !=null)
												{
													if(returnReceivers[0].length > 0){

														arrToEmail.forEach(function(row) {
															if(!returnReceivers[0].includes(row)){
																returnReceivers[0].push(row);
															}
													  	});

														var ArrClean = returnReceivers[0].filter(v=>v!='');
														toInsertEmail = JSON.stringify(ArrClean).replace(/[\[\]"]+/g,"");

													}else{
														toInsertEmail = JSON.stringify(filteredToEmail).replace(/[\[\]"]+/g,"");
													}

												
													toInsertGroupId = returnReceivers[1];

													var ToFilterEmails = toInsertEmail.split(",");

													var toRemove = '@jazenet.com',

													filtered = ToFilterEmails.filter(function (str) { return str.indexOf(toRemove) === -1; });

													newFiltered = JSON.stringify(filtered).replace(/[\[\]"]+/g,"");

													var arrUpdate = {
														group_id:toInsertGroupId,
														account_id:account_id,
														email_id:newFiltered
													};

													HelperJazeMail.updateReceivers(arrUpdate, function(retUpdate){});

												}else{

													var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_email_accounts_ids";
													var lastGrpId = 0;

													HelperJazeMail.getLastGroupId(queGroupMax, function(queGrpRes){		

														queGrpRes.forEach(function(row) {
															lastGrpId = row.lastGrpId;
												  		});

														toInsertGroupId = lastGrpId+1;
														toInsertEmail = filteredToEmail;

														var ToFilterEmails = toInsertEmail.split(",");

														var toRemove = '@jazenet.com',
														filtered = ToFilterEmails.filter(function (str) { return str.indexOf(toRemove) === -1; });

														newFiltered = JSON.stringify(filtered).replace(/[\[\]"]+/g,"");

														var arrInsert = {
															group_id:toInsertGroupId,
															account_id:account_id,
															email_id:newFiltered
														};

														HelperJazeMail.insertReceivers(arrInsert, function(retInsert){});

													});
														
												}


											});


								
											//receivListFunc(arrParams,to_email,arrToEmail,function(){});
											//receivListFunc(arrParams,filteredToEmail,arrToEmail,function(){});
					
											//check jaze_jazecom_contact_list
											//request removed by mustafa
											//jazeComContactFunc(arrParams,arrToEmail,function(){});


											jsonRes = {
												success: '1',
													result: {
														message_id:info.messageId
													},
												successMessage: 'Email Sent',
												errorMessage: errorMessage 
									  		};

											res.status(201).json(jsonRes);
											res.end();



											//copy message to sent items
											var emailContent = {
										  	    from: retAccount.email_account, // sender address
											    to: filteredToEmail,
									    		cc: filteredToCc,
									    		bcc: filteredToBcc,
											    subject: subject, // Subject line
											    body_mes: message, // plain text body
										     	message_id:info.messageId
											};

									
											copyToSent(retAccount,emailContent,arrFileName,arrFileUrl, function(returnSent){

												if(returnSent!=0){

													setTimeout(function(){
														if(arrFileName.length > 1 && arrFileUrl.length > 1){

															arrFileName.forEach(function(row) {
																fs.unlink(toUnlink+'/'+row, function (err) {});
														  	});

														}else{
															fs.unlink(toUnlink+'/'+file_name, function (err) {});
														}

												 	}, 500);
												}

											});


									    }).catch(function(err){
									    //	console.log('got error'); console.log(err);
					    					jsonRes = {
												success: success,
												result: {},
												successMessage: successMessage,
												errorMessage: "Unable to send email. Please try again." 
											};


											res.status(statRes).json(jsonRes);
											res.end();

									    });
							


						    		});

			    	 			}, 500);
					    	});

			    	 	}, 500);
					});
				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Account not found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});

		}
		else	
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});


router.post('/jazemail/reply_email', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('mail_id').isLength({ min: 1 }),

	check('to_email').isLength({ min: 1 }),
	check('message_id').isLength({ min: 1 }),

	check('subject').isLength({ min: 1 }),
	check('new_message').isLength({ min: 1 }),
	check('old_message').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  api_token = req.body.api_token;
	var  account_id = req.body.account_id;
	var  mail_id = req.body.mail_id;
	
	var  to_email = req.body.to_email;
	var  to_cc = req.body.to_cc;
	var  to_bcc = req.body.to_bcc;
	var  message_id = req.body.message_id;

	var  subject = req.body.subject;
	var  new_message = req.body.new_message;
	var  old_message = req.body.old_message;

	var file_name = req.body.file_name;
	var file_url = req.body.file_url;

	var arrParams = {		
		account_id: account_id,
		to_email: to_email,
		message_id: message_id,
		subject: subject,
		new_message: new_message,
		old_message: old_message,
		mail_id:mail_id
	};

	var arrFileName = file_name.split(",");
	var arrFileUrl = file_url.split(",");

	var arrToEmailClean = to_email.split(",");
	var arrToCcClean = to_cc.split(",");
	var arrToBCcClean = to_bcc.split(",");

	var arrFilesComplete = [];

	var arrRows = [];
	var newObj = {};


	var source = G_STATIC_IMG_PATH+'/jazemail/';

	var arrTokens = {account_id:account_id,api_token:api_token};
	HelperRestriction.checkParametersApiToken(arrTokens, function(queParam){
		if(queParam.length)
		{	

		
			HelperJazeMail.getEmailAccountDetails(arrParams, function(retAccount){

				if(retAccount!=null)
				{	
										
					var  arrToEmails = "'"+to_email.split(",").join("','") + "'";
					HelperJazeMail.getAccountIDperEmailForBlock(arrToEmails,arrParams, function(retToEmailBlock){

						var filteredToEmail;
						if(retToEmailBlock!=null){

							filteredToEmails =  arrToEmailClean.filter(function(hero) {
								return !retToEmailBlock.includes(hero);
							});

							filteredToEmail = filteredToEmails.toString();


						}else{

							filteredToEmail = to_email;

						}

		

						var arrToEmail = filteredToEmail.split(",");

						setTimeout(function(){
							var  arrToCC = "'"+to_cc.split(",").join("','") + "'";
							HelperJazeMail.getAccountIDperEmailForBlock(arrToCC,arrParams, function(retTOCcBlock){
								
								var filteredToCc;
								if(retTOCcBlock!=null){

									filteredToCc =  arrToCcClean.filter(function(hero) {
										return !retTOCcBlock.includes(hero);
									});


								}else{

									filteredToCc = to_cc;
								}


								setTimeout(function(){
									var  arrToBcc = "'"+to_bcc.split(",").join("','") + "'";
									HelperJazeMail.getAccountIDperEmailForBlock(arrToBcc,arrParams, function(retToBccBlock){


										var filteredToBcc;
										if(retToBccBlock!=null){

											filteredToBcc =  arrToBCcClean.filter(function(hero) {
												return !retToBccBlock.includes(hero);
											});


										}else{

											filteredToBcc = to_bcc;
										}



										var atobconvert = atob(retAccount.email_password);
										var password = "'" + atobconvert + "'";

										var sendEmail = mailer.config({
									  	    host: retAccount.smtp_host,
										    port: retAccount.smtp_port,
										    secure: true, // true for 465, false for other ports
										    auth: {
										      	user: retAccount.email_account, 
										      	pass: atobconvert
										    }
										});


										if((arrFileName.length > 0 && arrFileUrl.length > 0) && (file_name != '' && file_url != '') ){

											arrFilesComplete = arrFileName.map(function (x, i) { 
												var obj = {filename:x,path:arrFileUrl[i]};
												return	obj;
						                  	});
																	
										}

										var all_message = new_message.concat('<br/>','<br/>',old_message);

										var message_arr = {
									  	    from: retAccount.email_account, // sender address
									  	    to:filteredToEmail,
									  	    cc:filteredToCc,
									  	    bcc:filteredToCc,
									  	    replyTo:filteredToEmail,
									  	    inReplyTo: "'" + message_id + "'",
									  	    references: ["'" + message_id + "'"],
										    subject: subject, // Subject line
										    //text: all_message, // plain text body
										    html: all_message, // html body
									    	attachments:arrFilesComplete
										};
										var toUnlink = source+account_id;
										sendEmail(message_arr)
										    .then(function(info){

						    	 				jsonRes = {
													success: '1',
														result: {
															message_id:info.messageId
														},
													successMessage: 'Reply Sent',
													errorMessage: errorMessage 
										  		};

												res.status(201).json(jsonRes);
												res.end();
												
												//send notification process start
												var toCheckEmails = JSON.stringify(arrToEmail).replace(/[\[\]]+/g,"");
												var arrAccountIdtoNoti = [];
												var mail_push_notification = {};
						 	                   	var today = new Date();
						 	                   	var dateNow = dateFormat(today,'yyyy-mm-dd');

												HelperJazeMail.checkIfinCredentials(toCheckEmails, function(retAccountIds){


													if(retAccountIds!=null){
														arrAccountIdtoNoti.push(retAccountIds);
													}

													if(arrAccountIdtoNoti !=null){

														arrAccountIdtoNoti.forEach(function(val,idx) {

															mail_push_notification = {
																group_id:(val[idx].email_address+'|'+info.messageId+'|'+dateNow).toString(),
																account_id:account_id.toString(),
																request_account_id:(val[idx].account_id).toString() ,
																flag:"12"
															};

															HelperNotification.sendPushNotification(mail_push_notification, function(returnNoti){

															//	console.log(returnNoti);

															});

														});
													}
												
												});
												
												//send notification process end

												//check receivers list
												HelperJazeMail.getReceiverList(arrParams, function(returnReceivers){

													var toInsertEmail;
													var toInsertGroupId;
													var newFiltered;
													var toRemove = '@jazenet.com';
													var filtered,ToFilterEmails;

													if(returnReceivers !=null)
													{
														if(returnReceivers[0].length > 0){

															arrToEmail.forEach(function(row) {
																if(!returnReceivers[0].includes(row)){
																	returnReceivers[0].push(row);
																}
														  	});

															var ArrClean = returnReceivers[0].filter(v=>v!='');
															toInsertEmail = JSON.stringify(ArrClean).replace(/[\[\]"]+/g,"");

														}else{
															toInsertEmail = JSON.stringify(filteredToEmail).replace(/[\[\]"]+/g,"");
														}

													
														toInsertGroupId = returnReceivers[1];

														ToFilterEmails = toInsertEmail.split(",");

				
    													filtered = ToFilterEmails.filter(function (str) { return str.indexOf(toRemove) === -1; });

														newFiltered = JSON.stringify(filtered).replace(/[\[\]"]+/g,"");

											

														var arrUpdate = {
															group_id:toInsertGroupId,
															account_id:account_id,
															email_id:newFiltered
														};

														//console.log('update', arrUpdate);

														HelperJazeMail.updateReceivers(arrUpdate, function(retUpdate){});

													}else{

														var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_email_accounts_ids";
														var lastGrpId = 0;

														HelperJazeMail.getLastGroupId(queGroupMax, function(queGrpRes){		

															queGrpRes.forEach(function(row) {
																lastGrpId = row.lastGrpId;
													  		});

															toInsertGroupId = lastGrpId+1;
															toInsertEmail = filteredToEmail;

															ToFilterEmails = toInsertEmail.split(",");

														
    														filtered = ToFilterEmails.filter(function (str) { return str.indexOf(toRemove) === -1; });

															newFiltered = JSON.stringify(filtered).replace(/[\[\]"]+/g,"");

													

															var arrInsert = {
																group_id:toInsertGroupId,
																account_id:account_id,
																email_id:newFiltered
															};

															//console.log('insert', arrInsert);

															HelperJazeMail.insertReceivers(arrInsert, function(retInsert){});

														});
															
													}


												});

						
												//receivListFunc(arrParams,filteredToEmail,arrToEmail,function(){});
						
												//check jaze_jazecom_contact_list
												//request removed by mustafa
												//jazeComContactFunc(arrParams,arrToEmail,function(){});


												//copy message to sent items
												var emailContent = {
											  	    from: retAccount.email_account, // sender address
											    	to: filteredToEmail,
										    		cc: filteredToCc,
										    		bcc: filteredToBcc,
												    subject: subject, // Subject line
												    body_mes: all_message, // plain text body
											     	message_id:info.messageId
												};

									
												copyToSent(retAccount,emailContent,arrFileName,arrFileUrl, function(returnSent){

													if(returnSent!=0){

														setTimeout(function(){
															if(arrFileName.length > 1 && arrFileUrl.length > 1){

																arrFileName.forEach(function(row) {
																	fs.unlink(toUnlink+'/'+row, function (err) {});
															  	});

															}else{
																fs.unlink(toUnlink+'/'+file_name, function (err) {});
															}

													 	}, 500);
													}

												});

										    }).catch(function(err){
										    //	console.log('got error'); console.log(err);
						    					jsonRes = {
													success: success,
													result: {},
													successMessage: successMessage,
													errorMessage: "Unable to send reply. Please try again." 
												};

												res.status(statRes).json(jsonRes);
												res.end();
										    });


									});

								}, 500);

							});

				    	}, 500);





					});

				}

				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Account not found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}

			});

		}
		else	
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});


router.post('/jazemail/delete_email', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('mail_id').isLength({ min: 1 }),

	check('folder').isLength({ min: 1 }),
	check('seq').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  api_token = req.body.api_token;
	var  account_id = req.body.account_id;
	var  mail_id = req.body.mail_id;
	
	var  folder = req.body.folder;
	var  message_id = req.body.seq;

	var arrParams = {		
		account_id: account_id,
		folder: folder,
		message_id: message_id,
		mail_id:mail_id
	};

	var arrRows = [];
	var newObj = {};

	var arrMessageId = message_id.split(",");


	var arrTokens = {account_id:account_id,api_token:api_token};
	HelperRestriction.checkParametersApiToken(arrTokens, function(queParam){
		if(queParam.length)
		{	
			HelperJazeMail.getEmailAccountDetails(arrParams, function(retAccount){
			
				if(retAccount!=null){

				var mailDetails = {folder:folder,mail_type:retAccount.mail_type};

					HelperJazeMail.getMailFolder(mailDetails, function(retFolder){

						if(retAccount!=null)
						{

							var ssl = false;
							if(retAccount.is_ssl == 1){
								ssl = true;
							}
					
							var imap = new Imap({
							  	user: retAccount.email_account,
							  	password: atob(retAccount.email_password),
							  	host: retAccount.imap_host,
							  	port: retAccount.imap_port,
							  	tls: ssl
							});

							function openInbox(cb) {
							  	imap.openBox(retFolder, false, cb);
							}

						    imap.once('ready', function () {
					        openInbox(function (err, box) {
					            if (err){

		                			jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "Unable to load mailbox. Please try again." 
									};

									res.status(statRes).json(jsonRes);
									res.end();

					            } 
					            else 
					            {
				            	 	let messageInbox = [];
									let objMessage = {};

		     						var attachArr = [];
		 							var objAttach = {};

		 							var seqforDel = [];									

		       	                   	var today = new Date();
									var tomorrow = new Date();
									var newNow = tomorrow.setDate(today.getDate()+1); 

						 			var currentMonth = new Date();
									var lastMonth = currentMonth.setMonth(currentMonth.getMonth() - 1);


				                   	var dateNowAdd1Day = dateFormat(newNow,'yyyy-mm-dd');
				                   	var dateLastMonth =  dateFormat(lastMonth,'yyyy-mm-dd');

					                imap.search([ 'ALL', ['SINCE', dateLastMonth], ['BEFORE', dateNowAdd1Day] ], function (err, results) {
					                    if (err){
		  
				                        	jsonRes = {
												success: success,
												result: {},
												successMessage: successMessage,
												errorMessage: "Unable to load mailbox. Please try again." 
											};

											res.status(201).json(jsonRes);
											res.end();
					                    } 
					                    else 
					                    {
					                        if (!results || !results.length){
					                           // console.log('no mails!!!');
		                                     	jsonRes = {
													success: success,
													result: {},
													successMessage: successMessage,
													errorMessage: "Mailbox is empty." 
												};

												res.status(201).json(jsonRes);
												res.end();

					                            imap.end();
					                        } 
					                        else 
					                        {
					                        
					                            //console.log('the length of results:'+results.length);
						                           	var f = imap.fetch(results, {bodies:''});
						                          
						                            f.on('message', function (msg, seqno) {
						                                msg.on('body', function (stream, info) {
	
						                                });
						                                msg.once('end', function() {
						                         		    //console.log(seqno + ' finished.');
						                                });
						                            });
						                            f.once('error', function(err) {
						                               // console.log('failed to fetch: ' + err);

			                                           	jsonRes = {
															success: success,
															result: {},
															successMessage: successMessage,
															errorMessage: "Unable to load mailbox. Please try again." 
														};

														res.status(201).json(jsonRes);
														res.end();

						                            });
						                            f.once('end', function() {				

														setTimeout(function(){

															if(retAccount.imap_host=='imap.yandex.com'){

														    imap.seq.move(arrMessageId, 'Trash', function(err) {
														  		if(!err){
											  						jsonRes = {
																		success: '1',
																			result: {},
																		successMessage: 'Message has been deleted.',
																		errorMessage: errorMessage 
															  		};

																	res.status(201).json(jsonRes);
																	res.end();
														  		}else{
														  											
													  				jsonRes = {
																		success: success,
																		result: {},
																		successMessage: successMessage,
																		errorMessage: "Unable to delete mail. Please try again." 
																	};

																	res.status(201).json(jsonRes);
																	res.end();
														  		}
														    });
												

															}else{

																imap.seq.addFlags(arrMessageId, 'Deleted', function(err) {
															
															  		if(!err){

																		jsonRes = {
																			success: '1',
																				result: {},
																			successMessage: 'Message has been deleted.',
																			errorMessage: errorMessage 
																  		};

																		res.status(201).json(jsonRes);
																		res.end();

																  	}else{
													

														  				jsonRes = {
																			success: success,
																			result: {},
																			successMessage: successMessage,
																			errorMessage: "Unable to delete mail. Please try again." 
																		};

																		res.status(201).json(jsonRes);
																		res.end();
																  			
																  	}
												
															
																});

															}

														

												           	imap.end();

				                                	 	}, 500);

						                            });

						                        }
						                    }
						                })
						            }
						        });
						    });

						    imap.once('error', function(err) {
						       // console.log(err);
						    });

						    imap.once('end', function() {
						      //  console.log('finished!');
						    });

						    imap.connect();

		    			}
						else
						{

							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Account not found." 
							};

							res.status(statRes).json(jsonRes);
							res.end();

						}

			      	});

				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Account not found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();

				}

			});

		}
		else	
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});



router.post('/jazemail/delete_email_account', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('mail_id').isLength({ min: 1 })

	
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  api_token = req.body.api_token;
	var  account_id = req.body.account_id;
	var  mail_id = req.body.mail_id;
	

	var arrParams = {		
		account_id: account_id,
		mail_id:mail_id
	};

	var arrRows = [];
	var newObj = {};
	var account_list = {};


	var arrTokens = {account_id:account_id,api_token:api_token};
	HelperRestriction.checkParametersApiToken(arrTokens, function(queParam){
		if(queParam.length)
		{	
			HelperJazeMail.deleteMailAccount(arrParams, function(retDel){


				if(retDel!=0)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Email account is successfully removed.',
						errorMessage: errorMessage 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to remove email account. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}


			});
		}	
		else	
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});



var storageMulti = multer.diskStorage({
	destination: function (req, file, callback) {
	
	function asyncFunction (item, cb) {
	  	setTimeout(() => {
		
	      	var mail_path = G_STATIC_IMG_PATH+"jazemail/"+item+"/";

			if (!fs.existsSync(mail_path)){
		  		fs.mkdirSync(mail_path,'0757', true);  
			}
			callback(null, mail_path);

			cb();
	  	}, 100);
	}

	let requests = 0;

		if(req.body.api_token !=''){

			return new Promise((resolve) => {
				requests = req.body.account_id;
			
		  		asyncFunction(requests, resolve);

		    });

		Promise.all(requests);

		}
  },
  	filename: function (req, file, callback) {
    	callback(null, file.originalname);
  	}
});


router.post('/jazemail/multi_uploads', function(req, res) {

	var arrFiles = [];
	var arrObjects = [];
	var result = {};
	var uploadMulti = multer({
		storage: storageMulti,
	}).array('file',10);

	uploadMulti(req, res, function(err) {

	arrFiles.push(req.files);

		HelperJazeMail.getMultipleUploads(req.files,req.body.account_id, function(returnResult){
	
          	jsonRes = {
                success: '1',
                result: {multi_uploads:returnResult},
                successMessage: 'Files are successfully uploaded.',
                errorMessage: errorMessage 
            };
            res.status(201).json(jsonRes);
            res.end();

	  	});	

	})
});



router.post('/jazemail/get_email_folder_count', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('mail_id').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  api_token = req.body.api_token;
	var  account_id = req.body.account_id;
	var  mail_id = req.body.mail_id;



	var arrParams = {		
		account_id: account_id,
		mail_id:mail_id
	};

	var arrRows = [];
	var newObj = {};
	var account_list = {};

	var arrTokens = {account_id:account_id,api_token:api_token};
	HelperRestriction.checkParametersApiToken(arrTokens, function(queParam){
		if(queParam.length)
		{	
			HelperJazeMail.getEmailAccountDetails(arrParams, function(retAccount){
	
				if(retAccount!=null)
				{

					var ssl = false;
					if(retAccount.is_ssl == 1){
						ssl = true;
					}

					var imap = new Imap({
					  	user: retAccount.email_account,
					  	password: atob(retAccount.email_password),
					  	host: retAccount.imap_host,
					  	port: retAccount.imap_port,
					  	tls: ssl
					});

					var forEmailParams = {
					  	account_id:account_id,
					  	mail_id:mail_id,
					  	mail_type:retAccount.mail_type,
					  	imap:imap

					};	

					getInboxCount(forEmailParams, function(retInboxCount){

						getDraftsCount(forEmailParams, function(retDraftsCount){

							getTrashCount(forEmailParams, function(retTrashCount){


								var inbxCount = '0';
								if(retInboxCount!=0){
									inbxCount = retInboxCount;
								}

								var drftCount = '0';
								if(retDraftsCount!=0){
									drftCount = retDraftsCount;
								}

								var trshCount = '0';
								if(retTrashCount!=0){
									trshCount = retTrashCount;
								}


								var objCounts = {
									inbox:inbxCount,
									drafts:drftCount,
									trash:trshCount
								};

								jsonRes = {
									success: '1',
										result: {message_count:objCounts},
									successMessage: 'Email Messages count.',
									errorMessage: errorMessage 
						  		};

								res.status(201).json(jsonRes);
								res.end();

								imap.end();

							});

						});

					});


				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Account not found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();

				}

			});

		}
		else	
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});



let deleteAttachments = function(arrParams){

	var directory = '/home/chaaby/subdomain/img/images/master.resources/uploads/jazenet/jazemail/'+arrParams.account_id;

	fs.readdir(directory, (err, files) => {
	  	if (!err)
	  	{

		  	for (const file of files) {
			    fs.unlink(path.join(directory, file), err => {
			      if (err) throw err;
			    });
		  	}

	  	} 
	
	});

}


let getYesterdayFunc = function(forEmailParams,return_result){

	

	var imap = new Imap({
	  	user: forEmailParams.user,
	  	password: forEmailParams.password,
	  	host: forEmailParams.host,
	  	port: forEmailParams.port,
	  	tls: forEmailParams.tls
	});

	function openInbox(cb) {
	  	imap.openBox(forEmailParams.folder, true, cb);
	}

	

    imap.once('ready', function () {
        openInbox(function (err, box) {

			imap.search(['ALL',['SINCE', forEmailParams.date_from], ['BEFORE', forEmailParams.date_to]], function (err, resultsYesterday) {
				
				if(err)
				{
					console.log(err);
					return_result(null,null);

				}
				else
				{	

					var y = imap.fetch(resultsYesterday, {bodies:''});

					var email_ob_arr = [];
					var email_obj = {};

					var objMessage = {};
					var messageInbox  = [];

			    	y.on('message', function (msg, seqno) {

			            msg.on('body', function (stream, info) {

			            	async function main(stream,info,resultsYesterday) {

			        		   	let parsed = await simpleParser(stream);

			    		   		var attachment = '0';
				    			if(parsed.attachments.length != 0){
				    				attachment = '1';
				    			}

				
				    			var subject = '';
				    			if (typeof parsed.subject != 'undefined') {
									subject = parsed.subject;
								}

								var message = '';
				    			if (typeof parsed.html != 'undefined') {
									message = parsed.html;
								}
								
			            										                         
					          	objMessage = {
					          		seq:info.seqno.toString(),
					          		id:parsed.messageId,
					          		logo:'',
					          		subject: subject,
			      		    		date:dateFormat(parsed.date,'yyyy-mm-dd HH:MM:ss'),
					          		from_email:parsed.from.value[0].address,
					          		from_name:parsed.from.value[0].name,
					          		attachment:attachment,
					          		message:message
					     
					          	};

			          			messageInbox.push(objMessage);
							}

							main(stream,info,resultsYesterday);

							
			            });
			            msg.once('end', function() {
			     		    //console.log(seqno + ' finished.');
			            });
			        });
			        y.once('error', function(err) {

			          //  console.log('failed to fetch: ' + err);

		            	return_result(null,null);

			        });
			    	y.once('end', function() {

			    	
						setTimeout(function(){
			    	      	var newFlag = parseInt(forEmailParams.flag) + 1;
			            	      	
							if(messageInbox != ''){

			        	   		messageInbox.sort(function(a, b){
								    return b.seq-a.seq
								})

			       
		        				HelperJazeMail.getAllEmailAccounts(forEmailParams, function(retAllAccounts){

		     		 				email_obj = {
		 								return_flag:newFlag.toString(),
		     		 					list_date:forEmailParams.date_from,
		     		 					total_email_count:box.messages.total.toString(),
										message_count:resultsYesterday.length.toString(),
										email_list:messageInbox
		     		 				};

		     		 				email_ob_arr.push(email_obj);


		     		 				return_result(email_ob_arr,retAllAccounts);

		                       });


							}else{


								return_result(null,null);
						
				
							}

					 	}, 500);

			        });

				}

			});
	
		});

	});


    imap.once('error', function(err) {
        console.log('connection ' +err);
    });

    imap.once('end', function() {
      //  console.log('finished!');
    });

    imap.connect();


}

/*
* check mail module status
*/
router.post('/jazemail/check_jazemail_conversation', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('request_account_id').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id: req.body.account_id,
		request_account_id: req.body.request_account_id,
		api_token: req.body.api_token
	};

	HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

		if (queParamRes.length) {

			HelperJazeMail.checkAccountJazecomMailStatus(reqParams, function(retAccount){
				
					if(parseInt(retAccount) === 3)
					{
						jsonRes = {
							success: "1",
							result: {
								status : "0",
								receiver_email_id : "",
								receiver_account_id : reqParams.request_account_id
							},
							successMessage: successMessage,
							errorMessage: "Sorry, the service isn't available right now. Please try again later." 
						};

						res.status(201).json(jsonRes);
						res.end();
					}
					else if(parseInt(retAccount) === 2)
					{
						jsonRes = {
							success: "1",
							result: {
								status : "4",
								receiver_email_id : "",
								receiver_account_id : reqParams.request_account_id
							},
							successMessage: successMessage,
							errorMessage: 'Sorry, The account has been blocked.',
						};

						res.status(201).json(jsonRes);
						res.end();
					}
					else if(parseInt(retAccount) === 1)
					{

						HelperJazeMail.getjazemail(reqParams.request_account_id, function (returnJazemail) {
						
							if(returnJazemail !== null && returnJazemail !== ""){

								jsonRes = {
									success: "1",
									result: {
										status : "1",
										receiver_account_id : reqParams.request_account_id,
										receiver_email_id   : returnJazemail
									},
									successMessage: "Jazemail conversation details",
									errorMessage: errorMessage
								};
			
								res.status(201).json(jsonRes);
								res.end();
							}
							else
							{
								jsonRes = {
									success: "1",
									result: {
										status : "0",
										receiver_account_id : reqParams.request_account_id,
										receiver_email_id   : ""
									},
									successMessage: successMessage,
									errorMessage: "Sorry, the service isn't available right now. Please try again later." 
								};
			
								res.status(statRes).json(jsonRes);
								res.end();
							}
						});
					}
					else
					{
						jsonRes = {
							success: "0",
							result: {
								status : "0",
								receiver_email_id : "",
								receiver_account_id : reqParams.request_account_id
							},
							successMessage: successMessage,
							errorMessage: "Sorry, the service isn't available right now. Please try again later." 
						};

						res.status(201).json(jsonRes);
						res.end();
					}
			});
		}
		else
        {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };
            res.status(statRes).json(jsonRes);
            res.end();
        }
	});
});



router.post('/mail/checkuser', function(req, res) {



	var list = {
		account_id : "41218",
		request_account_id : "101",
		flag : "0",
	};

	

	HelperJazeMail.getAllEmailAccounts(list,function(account_results) {

		jsonRes = {
			success: '1',
			result: account_results[0],
			successMessage: 'Conversation List.',
			errorMessage: errorMessage
		};
		
		statRes = 201;
		res.status(statRes).json(jsonRes);
		res.end();
		 	
			
	});
	
});



router.post('/jazemail/send_system_email', [
	check('api_token').isLength({ min: 1 }),
	check('to_email').isLength({ min: 1 }),
	check('subject').isLength({ min: 1 }),
	check('message').isLength({ min: 1 })
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token   : req.body.api_token,
		account_id	: 0,
		to_email	: req.body.to_email,
		subject		: req.body.subject,
		message		: req.body.message,
	};


		if(reqParams.api_token === 'jazenet')
		{	
			HelperJazeMail.sendSystemMailer(reqParams,function(results) {
			
				jsonRes = {
					success: '1',
						result: {},
					successMessage: 'System mail sent',
					errorMessage: errorMessage 
		  		};

				res.status(201).json(jsonRes);
				res.end();

			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
});

let getInboxCountForMain = function(forEmailParams,return_result){


	var imap = new Imap({
	  	user: forEmailParams.user,
	  	password: forEmailParams.password,
	  	host: forEmailParams.host,
	  	port: forEmailParams.port,
	  	tls: forEmailParams.tls
	});

	var mailDetails = {folder:'inbox',mail_type:forEmailParams.mail_type};

	HelperJazeMail.getMailFolder(mailDetails, function(retFolder){


		if(retFolder!=null)
		{
			function openInbox(cb) {
			  	imap.openBox(retFolder, false, cb);
			}

			var searchParams = ['UNSEEN',['SINCE', forEmailParams.date_from], ['BEFORE', forEmailParams.date_to]];

			if(parseInt(forEmailParams.flag)==0){
				searchParams = ['UNSEEN'];
			}

		    imap.once('ready', function () {
		        openInbox(function (err, box) {

		        	if(!err){

						imap.search(searchParams, function (err, resultInbox) {

							var totalInbox = resultInbox.length.toString();
							
							if(err)
							{
								imap.end();
								return_result('0');
							}
							else
							{	
								imap.end();
								return_result(totalInbox);
							}

						});

		        	}else{
		        		imap.end();
						return_result('0');
		        	}
	
				});
			});


		    imap.once('error', function(err) {
    			imap.end();
		        console.log(err);
		    });

		    imap.once('end', function() {
				imap.end();

		    });

		    imap.connect();

		}
		else
		{
			return_result('0');
		}

   	});



}


let getInboxCount = function(forEmailParams,return_result){


	// var imap = new Imap({
	//   	user: forEmailParams.user,
	//   	password: forEmailParams.password,
	//   	host: forEmailParams.host,
	//   	port: forEmailParams.port,
	//   	tls: forEmailParams.tls
	// });

	var mailDetails = {folder:'inbox',mail_type:forEmailParams.mail_type};

	HelperJazeMail.getMailFolder(mailDetails, function(retFolder){

		//retFolder=null;
		if(retFolder!=null)
		{
			function openInbox(cb) {
			  	forEmailParams.imap.openBox(retFolder, true, cb);
			}


		    forEmailParams.imap.once('ready', function () {
		        openInbox(function (err, box) {
		        	if(!err){

        				forEmailParams.imap.search(['UNSEEN'], function (err, resultInbox) {

							var totalInbox = resultInbox.length.toString();
							
							if(err)
							{
								//imap.end();
								return_result(0);
							}
							else
							{	
								//imap.end();
								return_result(totalInbox);
							}

						});
		        				
		        	}else{
	        			return_result(0);
		        	}
			
				});
			});


		    forEmailParams.imap.once('error', function(err) {
       	        console.log('connection ' +err);
		    });

		    forEmailParams.imap.once('end', function() {
	    		//imap.end();
		      //  console.log('finished!');
		    });

		    forEmailParams.imap.connect();

		}
		else
		{
			return_result(0);
		}

   	});
}


let getDraftsCount = function(forEmailParams,return_result){

	// var imap = new Imap({
	//   	user: forEmailParams.user,
	//   	password: forEmailParams.password,
	//   	host: forEmailParams.host,
	//   	port: forEmailParams.port,
	//   	tls: forEmailParams.tls
	// });


	var mailDetails = {folder:'drafts',mail_type:forEmailParams.mail_type};

	HelperJazeMail.getMailFolder(mailDetails, function(retFolder){

		//retFolder=null;
		if(retFolder!=null)
		{
			function openInbox(cb) {
			  	forEmailParams.imap.openBox(retFolder, true, cb);
			}


		    forEmailParams.imap.once('ready', function () {
		        openInbox(function (err, box) {
		        	if(!err){

        				forEmailParams.imap.search(['ALL'], function (err, resultInbox) {

							var totalInbox = resultInbox.length.toString();
							
							if(err)
							{
								//imap.end();
								return_result(0);
							}
							else
							{	
								//imap.end();
								return_result(totalInbox);
							}

						});

		        	}else{
	        			return_result(0);
		        	}
			
				});
			});


		    forEmailParams.imap.once('error', function(err) {
    	        console.log('connection ' +err);
		    });

		    forEmailParams.imap.once('end', function() {
		    	//imap.end();
		      //  console.log('finished!');
		    });

		    forEmailParams.imap.connect();

		}
		else
		{
			return_result(0);
		}

   	});

}




let getTrashCount = function(forEmailParams,return_result){

	// var imap = new Imap({
	//   	user: forEmailParams.user,
	//   	password: forEmailParams.password,
	//   	host: forEmailParams.host,
	//   	port: forEmailParams.port,
	//   	tls: forEmailParams.tls
	// });


	var mailDetails = {folder:'trash',mail_type:forEmailParams.mail_type};

	HelperJazeMail.getMailFolder(mailDetails, function(retFolder){

		//retFolder=null;
		if(retFolder!=null)
		{
			function openInbox(cb) {
			  	forEmailParams.imap.openBox(retFolder, true, cb);
			}


		    forEmailParams.imap.once('ready', function () {
		        openInbox(function (err, box) {
		        	if(!err){

        				forEmailParams.imap.search(['ALL'], function (err, resultInbox) {

							var totalInbox = resultInbox.length.toString();
							
							if(err)
							{
								//imap.end();
								return_result(0);
							}
							else
							{	
								//imap.end();
								return_result(totalInbox);
							}

						});

		        	}else{
	        			return_result(0);
		        	}
			
				});
			});


		    forEmailParams.imap.once('error', function(err) {
      	        console.log('connection ' +err);
		    });

		    forEmailParams.imap.once('end', function() {
	    		//imap.end();
		       // console.log('finished!');
		    });

		    forEmailParams.imap.connect();

		}
		else
		{
			return_result(0);
		}

   	});
}




let receivListFunc = function(arrParams,to_email,arrToEmail){

	HelperJazeMail.getReceiverList(arrParams, function(returnReceivers){

		var toInsertEmail;
		var toInsertGroupId;

		if(returnReceivers !=null)
		{
			if(returnReceivers[0].length > 0){

				arrToEmail.forEach(function(row) {
					if(!returnReceivers[0].includes(row)){
						returnReceivers[0].push(row);
					}
			  	});

				toInsertEmail = JSON.stringify(returnReceivers[0]).replace(/[\[\]"]+/g,"");

			}else{
				toInsertEmail = to_email;
			}

			toInsertGroupId = returnReceivers[1];

			var arrUpdate = {
				group_id:toInsertGroupId,
				account_id:arrParams.account_id,
				email_id:toInsertEmail
			};


			HelperJazeMail.updateReceivers(arrUpdate, function(retUpdate){});

		}else{

			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_email_accounts_ids";
			var lastGrpId = 0;

			HelperJazeMail.getLastGroupId(queGroupMax, function(queGrpRes){		

				queGrpRes.forEach(function(row) {
					lastGrpId = row.lastGrpId;
		  		});

				toInsertGroupId = lastGrpId+1;
				toInsertEmail = to_email;

				var arrInsert = {
					group_id:toInsertGroupId,
					account_id:arrParams.account_id,
					email_id:toInsertEmail
				};

		
				HelperJazeMail.insertReceivers(arrInsert, function(retInsert){});

			});
				
		}

	});

}

let jazeComContactFunc = function(arrParams,arrMails){

	var toCheckEmails = JSON.stringify(arrMails).replace(/[\[\]]+/g,"");

	HelperJazeMail.checkContactsMail(arrParams, function(retMailContact){ 

		var arrAccountsFromContacts;
		var ArrAccountsFromCred;

		if(retMailContact!=null)
		{

		 	arrAccountsFromContacts = retMailContact.mail_account_id.split(",");
			//update
			HelperJazeMail.getAccountIDperEmail(toCheckEmails, function(retAccountIDs){ 

				if(retAccountIDs!=null)
				{	

					retAccountIDs = retAccountIDs.filter(val => !arrAccountsFromContacts.includes(val));
					allAccounts = arrAccountsFromContacts.concat(retAccountIDs);

					function removeDups(accounts) {
					  let unique = {};
					  accounts.forEach(function(i) {
					    if(!unique[i]) {
					      unique[i] = true;
					    }
					  });
					  return Object.keys(unique);
					}

					var AllAccConvert = removeDups(allAccounts.toString().split(","));

					var remove_last_coma = AllAccConvert.toString().replace(/,\s*$/, "");
					var	arrUpdate = {
						mail_account_id:remove_last_coma,
						create_date:standardDT
					};

					var updateQue = `update jaze_jazecom_contact_list SET meta_value =? WHERE meta_key =? AND group_id =?`;
					HelperJazeMail.updateQuery(updateQue,arrUpdate,retMailContact.group_id, function(updateStat){});
				
				}

			});

		}
		else
		{
			//insert
			HelperJazeMail.getAccountIDperEmail(toCheckEmails, function(retAccountIDs){ 
	
				if(retAccountIDs!=null){

					var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazecom_contact_list";
					var lastGrpId = 0;

					HelperJazeMail.getLastGroupId(queGroupMax, function(queGrpRes){

				  		queGrpRes.forEach(function(row) {
							lastGrpId = row.lastGrpId;
					  	});


		  		  		var	arrInsert = {
							account_id:arrParams.account_id,
							mail_account_id:retAccountIDs.toString(),
							call_account_id:'',
							chat_account_id:'',
							mail_blocked_account_id:'',
							chat_blocked_account_id:'',
							call_blocked_account_id:'',
							diary_blocked_account_id:'',
							diary_account_id:'',
							feed_blocked_account_id:'',
							feed_account_id:'',
							create_date:standardDT,
							status:1,
						};

				  		var plusOneGroupID = lastGrpId+1;

						var querInsert = `insert into jaze_jazecom_contact_list (group_id, meta_key, meta_value) VALUES (?,?,?)`;
						HelperJazeMail.insertResult(arrInsert,querInsert,plusOneGroupID, function(insertRes){});

					});
					
				}

			});

		}

	});

}



let copyToSent = function(arrParams,emailContent,filenames,fileurls, return_result){

	var arrFilesComplete = [];

	if((filenames.length > 0 && fileurls.length > 0) && (filenames != '' && fileurls != '') ){

		arrFilesComplete = filenames.map(function (x, i) { 
			var obj = {filename:x,path:fileurls[i]};
			return	obj;
      	});
								
	}

	var imap = new Imap({
	  	user: arrParams.email_account,
	  	password: atob(arrParams.email_password),
	  	host: arrParams.imap_host,
	  	port: arrParams.imap_port,
	  	tls: true
	});

	var msg, alternateEntity, htmlEntity, plainEntity, pngEntity;

	var mailDetails = {folder:'sent',mail_type:arrParams.mail_type};

	HelperJazeMail.getMailFolder(mailDetails, function(retFolder){

		if(retFolder!=null)
		{
			function openInbox(cb) {
			  	imap.openBox(retFolder, true, cb);
			}


		    imap.once('ready', function () {
		        openInbox(function (err, box) {
		        	if(!err){

				        msg = mimemessage.factory({
				          contentType: 'multipart/alternate',
				          body: []
				        });
			
				        plainEntity = mimemessage.factory({
			        	  contentType: 'text/html;charset=utf-8',
				          body: emailContent.body_mes
				        });

				        msg.header('Message-ID', emailContent.message_id);
				        msg.header('from', arrParams.email_account);
				        msg.header('to', emailContent.to);
			           	msg.header('cc', emailContent.cc);
			           	msg.header('bcc', emailContent.bcc);
				        msg.header('Subject', emailContent.subject);

				        msg.body.push(plainEntity);


				        if(filenames.length > 0){

				      		arrFilesComplete.forEach(function(row) {
				      			
							  	var source = '/home/chaaby/subdomain/img/images/master.resources/uploads';

						 	 	var trimed = row.path.substring(row.path.indexOf("uploads") + 7);

							  	var sourcPath = source+trimed;

			  					var file_convert = fs.readFileSync(sourcPath);
								var contents = new Buffer(file_convert).toString('base64');


		 						pngEntity = mimemessage.factory({
								    contentType: mime.lookup(sourcPath),
								    contentTransferEncoding: 'base64',
								    body: contents
								});
								pngEntity.header('Content-Disposition', 'attachment ;filename='+row.filename);

								msg.body.push(pngEntity);
			
						  	});


				        }


				        imap.append(msg.toString());
				        return_result(1);
 
		        	}else{
	        			return_result(0);
		        	}
			
				});
			});


		    imap.once('error', function(err) {
      	        console.log('connection ' +err);
		    });

		    imap.once('end', function() {
		    	imap.end();
		      //  console.log('finished!');
		    });

		    imap.connect();

		}
		else
		{
			return_result(0);
		}

   	});



}


let copyToSentMailer = function(arrParams,emailContent,return_result){

	var imap = new Imap({
	  	user      : 'do-not-reply@jazenet.com',
	  	password  : 'pv85@fn',
	  	host      : 'imap.yandex.com',
	  	port      :  993,
	  	tls       :  true
	});

	var msg, alternateEntity, htmlEntity, plainEntity, pngEntity;

	function openInbox(cb) {
	  	imap.openBox('Sent', true, cb);
	}

    imap.once('ready', function () {
        openInbox(function (err, box) {
        	if(!err){

		        msg = mimemessage.factory({
		          contentType: 'multipart/alternate',
		          body: []
		        });
	
		        plainEntity = mimemessage.factory({
	        	  contentType: 'text/html;charset=utf-8',
		          body: emailContent.body_mes
		        });

		        msg.header('Message-ID', emailContent.message_id);
		        msg.header('from', arrParams.email_account);
		        msg.header('to', emailContent.to);
	 
		        msg.header('Subject', emailContent.subject);

		        msg.body.push(plainEntity);

		        imap.append(msg.toString());
		        return_result(1);

        	}else{
    			return_result(0);
        	}
	
		});
	});


    imap.once('error', function(err) {
    });

    imap.once('end', function() {
    	imap.end();
    });

    imap.connect();

}


//------------------------------------------------------- General Functions Start -----------------------------------------------------------//


function getFileTypes(ext){

	var type;

	var arrImages = ['.jpeg','.jpg','.gif','.png','.tif'];
	var arrVideos = ['.mp4','.avi','.mov','.flv','.wmv'];
	var arrAudio = ['.pcm','.wav','.aiff','.mp3','.aac','.wma','.flac','.alac'];
	var arrDocs = ['.odt','.xls','.xlsx','.ods','.ppt','.pptx','.txt','.doc'];
	var arrPdf = ['.pdf'];

	if(arrImages.includes(ext.toLowerCase())){
		type = '2';
	}else if(arrAudio.includes(ext.toLowerCase())){
		type = '3';
	}else if(arrVideos.includes(ext.toLowerCase())){
		type = '4';
	}else if(arrDocs.includes(ext.toLowerCase())){
		type = '5';
	}else if(arrPdf.includes(ext.toLowerCase())){
		type = '6';
	}else{
		type = '7';
	}

	return type;
};


function formatSizeUnits(bytes){
	if      (bytes >= 1073741824) { bytes = (bytes / 1073741824).toFixed(2) + " GB"; }
	else if (bytes >= 1048576)    { bytes = (bytes / 1048576).toFixed(2) + " MB"; }
	else if (bytes >= 1024)       { bytes = Math.ceil((bytes / 1024)) + " KB"; }
	else if (bytes > 1)           { bytes = bytes + " bytes"; }
	else if (bytes == 1)          { bytes = bytes + " byte"; }
	else                          { bytes = ""; }
	return bytes;
  }

//------------------------------------------------------- General Functions End -----------------------------------------------------------//




module.exports = router;

