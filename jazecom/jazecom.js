'use strict';

require('events').EventEmitter.defaultMaxListeners = 100;
require('events').EventEmitter.prototype._maxListeners = 100;

const bodyParser = require('body-parser');
const express = require('express');
const router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
const app = express();

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperJazecom = require('./helpers/HelperJazecom.js');

const { check, validationResult } = require('express-validator');

const fast_sort = require("fast-sort");

const crypto = require('crypto');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};



//------------------------------------------------------------------- Login Module Start ---------------------------------------------------------------------------//

/*
 *  Device Login
 */
router.post('/jazecom/login', [

	check('device_id').isLength({ min: 1 }),
	check('device_name').isLength({ min: 1 }),
	check('device_type').isNumeric(),
	check('device_details').isLength({ min: 1 }),
	check('device_fcm_token').isLength({ min: 1 }),
	check('jazemail_id').isLength({ min: 1 }),
	check('password').isLength({ min: 3 }),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		device_id		: req.body.device_id,
		device_name		: req.body.device_name,
		device_type		: req.body.device_type,
		device_details	: req.body.device_details,
		device_fcm_token: req.body.device_fcm_token,
		jazemail_id		: req.body.jazemail_id,
		password		: crypto.createHash('md5').update(req.body.password).digest("hex")
	};
	
	//check Credential Details valid or not
	HelperJazecom.checkCredentialDetails(reqParams, function(accountDetails){
		
		if(accountDetails !== null)
		{	
			if(parseInt(accountDetails.account_type) === 1)
			{	
				Object.assign(reqParams, {account_id: accountDetails.account_id});

				// clear device previous accounts
				HelperJazecom.logoutAllAccountsDevice(reqParams, function(retLogout){

					if(retLogout!== null)
					{
						//save device
						HelperJazecom.saveDeviceDetails(reqParams, function(returnDevice){	

							if(returnDevice !== null)
							{
								Object.assign(reqParams, {app_id: returnDevice.device_details.app_id, api_token: returnDevice.device_details.api_token});

								//geting child accounts
								HelperJazecom.insertChildAccountDevice(reqParams, function(returnSub){	

									if(returnSub !== null)
									{
										HelperJazecom.getDeviceAccountList(reqParams, function(child_accounts){

											jsonRes = {
												success: '1',
												result: {device_details : returnDevice.device_details, master_account_details: returnDevice.master_account_details, child_accounts : child_accounts},
												successMessage: 'successfully login Jazecom',
												errorMessage: errorMessage 
											};
									
											res.status(201).json(jsonRes);
											res.end();
										});
									}
									else
									{
										jsonRes = {
											success: success,
											result: {},
											successMessage: successMessage,
											errorMessage: "Unable to login. Please try again." 
										};
			
										res.status(statRes).json(jsonRes);
										res.end()
									}
								});
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to login. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end()
							}
						});
					}
				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid account details. Please enter valide individual account details." 
				};
	
				res.status(statRes).json(jsonRes);
				res.end();
	
			}	
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Invalid account details. Please check Email and Password." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

/*
 *  Device Logout
 */
router.post('/jazecom/logout', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isLength({ min: 1 }),
	check('app_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		app_id	   : req.body.app_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperJazecom.removeAllDeviceDetails(reqParams, function(retLogout){

				if(retLogout!== null)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'You have been successfully logged out!',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to log out. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

/*
 *  Update existing Device details
 */
router.post('/jazecom/update_device', [

	check('account_id').isNumeric(),
	check('device_id').isLength({ min: 1 }),
	check('device_type').isNumeric(),
	check('device_fcm_token').isLength({ min: 1 }),
	check('app_id').isNumeric(),
	check('api_token').isLength({ min: 1 })
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		account_id		: req.body.account_id,
		device_id		: req.body.device_id,
		device_type		: req.body.device_type,
		device_fcm_token: req.body.device_fcm_token,
		app_id			: req.body.app_id,
		api_token		: req.body.api_token
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			//check device already exists
			HelperJazecom.checkDeviceDetails(reqParams, function(returDevice){

				if(returDevice !== null)
				{
					HelperJazecom.updateDeviceDetails(reqParams, function(returnDevice){	
					
						if(returnDevice !== null)
						{
							Object.assign(reqParams, {api_token: returnDevice.device_details.api_token},{ merge: true });
							
							HelperJazecom.getDeviceAccountList(reqParams, function(child_accounts){
								
								jsonRes = {
									success: '1',
									result: {device_details : returnDevice.device_details, master_account_details: returnDevice.master_account_details, child_accounts : child_accounts},
									successMessage: 'successfully update Jazecom',
									errorMessage: errorMessage 
								};
						
								res.status(201).json(jsonRes);
								res.end();
							});
						}
					});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to get account details. Please try login." 
					};

					res.status(statRes).json(jsonRes);
					res.end()
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

/*
 *  get Account details account_id basis existing Device details
 */
router.post('/jazecom/get_device_login_details', [

	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('jazecom_flag').isNumeric(),
	check('connection_key').isLength({ min: 1 }),
	check('app_id').isNumeric(),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		account_id		: req.body.account_id,
		account_type	: req.body.account_type,
		jazecom_flag	: req.body.jazecom_flag,
		connection_key	: req.body.connection_key,
		app_id			: req.body.app_id
	};

	// jazecom_flag =  0 : not installed, 1 : active account, 2 : different account

		if(reqParams.connection_key === crypto.createHash('md5').update("Jazenet-"+reqParams.account_id+reqParams.account_type+"-Jazecom").digest("hex"))
		{
			//get master account
			HelperJazecom.getParentAccountDetails(reqParams, function(master_account){

				if(parseInt(master_account) !== 0)
				{
					//logout and get device details

					HelperJazecom.setlogoutGetDeviceDetails(reqParams, function(device_details){	
					
						if(device_details !== null)
						{
							var reqParams = {		
								account_id		: master_account,
								device_id 		: device_details.device_id,
								device_name		: device_details.device_name,
								device_type		: device_details.device_type,
								device_fcm_token: device_details.fcm_token,
							};
						
							//save device
							HelperJazecom.saveDeviceDetails(reqParams, function(returnDevice){	

								if(returnDevice !== null)
								{
									Object.assign(reqParams, {app_id: returnDevice.device_details.app_id, api_token: returnDevice.device_details.api_token});

									//geting child accounts
									HelperJazecom.insertChildAccountDevice(reqParams, function(returnSub){	

										if(returnSub !== null)
										{
											HelperJazecom.getDeviceAccountList(reqParams, function(child_accounts){

												jsonRes = {
													success: '1',
													result: {device_details : returnDevice.device_details, master_account_details: returnDevice.master_account_details, child_accounts : child_accounts},
													successMessage: 'successfully login Jazecom',
													errorMessage: errorMessage 
												};
										
												res.status(201).json(jsonRes);
												res.end();
											});
										}
										else
										{
											jsonRes = {
												success: success,
												result: {},
												successMessage: successMessage,
												errorMessage: "Unable to login. Please try again." 
											};
				
											res.status(statRes).json(jsonRes);
											res.end()
										}
									});
								}
								else
								{
									jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "Unable to login, Please try again." 
									};

									res.status(statRes).json(jsonRes);
									res.end()
								}
							});
						}
						else
						{
							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Unable to get account details. Please try login." 
							};

							res.status(statRes).json(jsonRes);
							res.end()
						}
					});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to get account details, Please try login." 
					};

					res.status(statRes).json(jsonRes);
					res.end()
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
});

//------------------------------------------------------------------- Login Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- jazecom Child Account Module Start ---------------------------------------------------------------------------//

/*
 *	get saved account list
 */
router.post('/jazecom/get_child_account_list', [

	check('api_token').isAlphanumeric(),
	check('app_id').isNumeric(),
	check('account_id').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {		
		api_token		: req.body.api_token, 
		app_id			: req.body.app_id, 
		account_id		: req.body.account_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			//get account list
			HelperJazecom.getHomeModuleAccountList(reqParams, function(returnList){	
						
				if(returnList !== null)
				{
					jsonRes = {
						success: '1',
						result: {child_accounts : fast_sort(returnList[0].home_account_list).asc('account_type') },
						successMessage: 'device account list.',
						errorMessage: errorMessage
					};

					statRes = 201;
					res.status(statRes).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to get details. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

/*
 *  add new account in device
 */
router.post('/jazecom/add_child_account', [

	check('api_token').isAlphanumeric(),
	check('app_id').isNumeric(),
	check('account_id').isNumeric(),
	check('jazemail_id').isLength({ min: 1 }),
	check('password').isLength({ min: 3 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {	
		api_token		: req.body.api_token,
		app_id			: req.body.app_id,
		account_id		: req.body.account_id,
		jazemail_id		: req.body.jazemail_id,
		password		: crypto.createHash('md5').update(req.body.password).digest("hex")
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			//check Credential Details valid or not
			HelperJazecom.checkCredentialDetails(reqParams, function(accountDetails){
				
				if(accountDetails !== null)
				{	
					if(parseInt(accountDetails.account_type) !== 1)
					{
						//save to tb
						var rreqParams = {	
							app_id			: reqParams.app_id,
							account_id		: accountDetails.account_id,
							account_type	: accountDetails.account_type
						};
						HelperJazecom.saveDeviceAccountDetails(rreqParams, function(retrnDiv){

							if(retrnDiv !== null)
							{
								//get list
								HelperJazecom.getHomeModuleAccountList(reqParams, function(returnList){	
						
									if(returnList !== null)
									{
										jsonRes = {
											success: '1',
											result: {child_accounts : fast_sort(returnList[0].home_account_list).asc('account_type') },
											successMessage: 'successfully account added',
											errorMessage: errorMessage
										};
					
										statRes = 201;
										res.status(statRes).json(jsonRes);
										res.end();
									}
									else
									{
										jsonRes = {
											success: success,
											result: {},
											successMessage: successMessage,
											errorMessage: "Unable to get details. Please try again." 
										};
					
										res.status(statRes).json(jsonRes);
										res.end();
									}
								});
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "The account already exists. Please use a different account." 
								};
								res.status(statRes).json(jsonRes);
								res.end();
							}
						});
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Invalid account details. Please enter valide account details." 
						};
			
						res.status(statRes).json(jsonRes);
						res.end();
			
					}
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details. Please check Email and Password." 
					};
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};
			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

/*
 *  remove account in device
 */
router.post('/jazecom/remove_child_account', [

	check('api_token').isAlphanumeric(),
	check('app_id').isNumeric(),
	check('remove_account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {	
		api_token		: req.body.api_token,
		app_id			: req.body.app_id,
		account_id		: req.body.remove_account_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			//update status
			HelperJazecom.removeDeviceAccountDetails(reqParams, function(accountDetails){
				
				if(accountDetails !== null)
				{	
					//get list
					HelperJazecom.getHomeModuleAccountList(reqParams, function(returnList){	
						
						if(returnList !== null)
						{
							jsonRes = {
								success: '1',
								result: {child_accounts : fast_sort(returnList[0].home_account_list).asc('account_type') },
								successMessage: 'successfully account removed',
								errorMessage: errorMessage
							};
		
							statRes = 201;
							res.status(statRes).json(jsonRes);
							res.end();
						}
						else
						{
							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Unable to get details. Please try again." 
							};
		
							res.status(statRes).json(jsonRes);
							res.end();
						}
					});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request, please try again." 
					};
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};
			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

//------------------------------------------------------------------- jazecom Child Account Module End ---------------------------------------------------------------------------//
//------------------------------------------------------------------- jazecom Home Module Start ---------------------------------------------------------------------------//

/*
 *	get home module account list
 */

router.post('/jazecom/jazecom_home_details', [

	check('api_token').isAlphanumeric(),
	check('app_id').isNumeric(),
	check('account_id').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {		
		api_token		: req.body.api_token, 
		app_id			: req.body.app_id, 
		account_id		: req.body.account_id
	};
	//console.log('jazecom_home_details',reqParams);
	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){
		 
		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			//get account list
			HelperJazecom.getHomeModuleAccountList(reqParams, function(returnList){	
				
				if(returnList !== null && parseInt(returnList.length) > 0)
				{
					HelperJazecom.getExternalEmailList(reqParams.account_id, function(external_list){	

						var home_account_list = fast_sort(returnList[0].home_account_list).asc('account_type');

						if(external_list !== null)
						{
							home_account_list = home_account_list.concat(external_list);
						}

						jsonRes = {
							success: '1',
							result: {module_list : { home_account_list : home_account_list, notification_count: returnList[0].notification_count  } },
							successMessage: 'Jazecom home module list.',
							errorMessage: errorMessage
						};

						statRes = 201;
						res.status(statRes).json(jsonRes);
						res.end();
					});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to get details. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

//------------------------------------------------------------------- jazecom Home Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- jazecom Contact Module Start ---------------------------------------------------------------------------//

/* 
* Jazecom Contact List
*/
router.post('/jazecom/contact_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('app_id').isNumeric(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {	
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		app_id	   : req.body.app_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperJazecom.getContactList(arrParams, function(retAccountList){

				if(retAccountList!=null){

					jsonRes = {
						success: '1',
						result: {contact_list:retAccountList},
						successMessage: 'Contact List',
						errorMessage: errorMessage 
					};

					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: successMessage,
						errorMessage: "Empty contact list." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else	
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});


/* 
* Jazecom Contact Details
*/
router.post('/jazecom/contact_details', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('contact_account_id').isNumeric(),
	check('app_id').isNumeric(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {	
		api_token		: req.body.api_token,
		app_id	  		: req.body.app_id,
		account_id		: req.body.account_id,
		contact_account_id: req.body.contact_account_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperJazecom.getContactDetails(arrParams, function(retAccountList){

				if(retAccountList!=null){

					jsonRes = {
						success: '1',
						result: retAccountList,
						successMessage: 'Contact Details',
						errorMessage: errorMessage 
					};

					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Contact not found. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

/* 
* Delete Jazecom Contact Details
*/
router.post('/jazecom/delete_contact_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('contact_account_id').isNumeric(),
	check('app_id').isNumeric(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {	
		api_token		: req.body.api_token,
		app_id	  		: req.body.app_id,
		account_id		: req.body.account_id,
		contact_account_id: req.body.contact_account_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperJazecom.reomveContactDetails(arrParams, function(retAccountList){

				if(retAccountList!=null){

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Contact successfully deleted',
						errorMessage: errorMessage 
					};

					res.status(201).json(jsonRes);
					res.end();

				}else{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Please choose valid contact details." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});
 

//------------------------------------------------------------------- jazecom Contact Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- jazecom Favourites Module Start ---------------------------------------------------------------------------//

/* 
* Jazecom Favorite list
*/
router.post('/jazecom/favorite_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('app_id').isNumeric(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {	
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		app_id	   : req.body.app_id
	};
	
	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperJazecom.getFavoriteList(arrParams, function(retAccountList){

				if(retAccountList!=null){
					
					jsonRes = {
						success: '1',
						result: {favorite_list:retAccountList},
						successMessage: 'Favorite List',
						errorMessage: errorMessage 
					};

					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: successMessage,
						errorMessage: "Empty Favorite list." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else	
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

/* 
* Update Favourites Details
*/
router.post('/jazecom/update_favorite_account', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('account_list').isNumeric(),
	check('app_id').isNumeric(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

	var errors = validationResult(req.body[0]);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}
	 
	var arrParams = {
		api_token	 :req.body[0].api_token,
		account_id	 :req.body[0].account_id,
		app_id	 	 :req.body[0].app_id,
		fav_account_id	 	 :req.body[0].fav_account_id,
		fav_account_type	 	 :req.body[0].fav_account_type,
		account_list 	  :req.body[0].account_list
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperJazecom.updateFavouritesDetails(arrParams, function(retupdation){

				if(retupdation != null){

					Object.assign(arrParams, {contact_account_id: arrParams.fav_account_id});

					HelperJazecom.getContactDetails(arrParams, function(retAccountList){

						if(retAccountList!=null){

							jsonRes = {
								success: '1',
								result: retAccountList,
								successMessage: 'Contact Details',
								errorMessage: errorMessage 
							};

							res.status(201).json(jsonRes);
							res.end();

						}else{

							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Contact not found. Please try again." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}
					});
				}
				else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Contact not found. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

//------------------------------------------------------------------- jazecom Favourites Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- jazecom Block Module Start ---------------------------------------------------------------------------//

/* 
* Update Contact Block or Unblock
*/
router.post('/jazecom/update_contact_blockstatus', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('contact_account_id').isNumeric(),
	check('account_list').isLength({ min: 1 }),
	check('app_id').isNumeric(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

	var errors = validationResult(req.body);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}
	 
	var arrParams = {
		api_token	 		:req.body.api_token,
		account_id	 		:req.body.account_id,
		app_id	 	 		:req.body.app_id,
		contact_account_id	:req.body.contact_account_id,
		account_list 		:req.body.account_list
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperJazecom.updateContactBlockStatus(arrParams, function(retupdation){

				if(retupdation != null){

					HelperJazecom.getContactDetails(arrParams, function(retAccountList){

						if(retAccountList!=null){

							jsonRes = {
								success: '1',
								result: retAccountList,
								successMessage: 'Contact Details',
								errorMessage: errorMessage 
							};

							res.status(201).json(jsonRes);
							res.end();

						}else{

							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Contact not found. Please try again." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}
					});
				}
				else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Contact not found. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});


//------------------------------------------------------------------- jazecom Block Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- jazecom Followers Module Start ---------------------------------------------------------------------------//

/* 
* Get Contact follow list
*/
router.post('/jazecom/following_list',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('app_id').isNumeric(),
	check('contact_account_id').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		account_id		  : req.body.account_id,
		api_token		  : req.body.api_token,
		app_id			  : req.body.app_id,
		contact_account_id: req.body.contact_account_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperJazecom.getJazecomLinkAccountFollowList(arrParams, function(followResult){

				if(followResult !== null)
				{
					jsonRes = {
						success: '1',
						result: {follow_account_list : fast_sort(followResult).asc('account_type')},
						successMessage: "Follow List",
						errorMessage: "" 
					};
		
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
			  		};

		  			res.status(statRes).json(jsonRes);
		 			res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});

});


/* 
* Update Contact follow list
*/
router.post('/jazecom/update_follow_status', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('contact_account_id').isNumeric(),
	check('account_list').isLength({ min: 1 }),
	check('app_id').isNumeric(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

	var errors = validationResult(req.body);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}
	 
	var arrParams = {
		api_token	 		:req.body.api_token,
		account_id	 		:req.body.account_id,
		app_id	 	 		:req.body.app_id,
		contact_account_id	:req.body.contact_account_id,
		account_list 		:req.body.account_list
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperJazecom.updateJazecomLinkAccountFollowStatus(arrParams, function(retupdation){

				if(retupdation != null){

						jsonRes = {
							success: '1',
							result: {},
							successMessage: "successfully updated",
							errorMessage: "" 
						};
			
						res.status(201).json(jsonRes);
						res.end();
				}
				else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});


//------------------------------------------------------------------- jazecom Followers Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- jazecom Module Active Inactive Section Start ---------------------------------------------------------------------------//

/* 
* Get Jazecom Module Status
*/
router.post('/jazecom/get_jazecom_module_status', [
	check('api_token').isLength({ min: 1 }),
	check('app_id').isNumeric(),
	check('account_id').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

	var errors = validationResult(req.body);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token	 		:req.body.api_token,
		app_id				:req.body.app_id, 
		account_id	 		:req.body.account_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		
		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperJazecom.getJazecomModuleStatus(arrParams, function(returnList){	
				
				if(returnList !== null){

					jsonRes = {
						success: '1',
						result: {module_status : fast_sort(returnList).asc('account_type') },
						successMessage: 'device account list.',
						errorMessage: errorMessage
					};

					statRes = 201;
					res.status(statRes).json(jsonRes);
					res.end();
				}
				else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to get details. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

/* 
* Update Jazecom Module Status
*/
router.post('/jazecom/update_jazecom_module_status', [
	check('api_token').isLength({ min: 1 }),
	check('app_id').isNumeric(),
	check('account_id').isNumeric(),
	check('module_status').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

	var errors = validationResult(req.body);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token	 		:req.body.api_token,
		app_id				:req.body.app_id, 
		account_id	 		:req.body.account_id,
		module_status		:req.body.module_status
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		
		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperJazecom.updateJazecomModuleStatus(arrParams, function(retupdation){
				
				if(retupdation !== null){

					//get account list
					HelperJazecom.getJazecomModuleStatus(arrParams, function(returnList){	
						
						if(returnList !== null)
						{
							jsonRes = {
								success: '1',
								result: {module_status : fast_sort(returnList).asc('account_type') },
								successMessage: 'device account list.',
								errorMessage: errorMessage
							};
		
							statRes = 201;
							res.status(statRes).json(jsonRes);
							res.end();
						}
						else
						{
							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Unable to get details. Please try again." 
							};
		
							res.status(statRes).json(jsonRes);
							res.end();
						}
					});
				}
				else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

//------------------------------------------------------------------- jazecom Module Active Inactive Section End ---------------------------------------------------------------------------//


// ------------------------------------------------------------------- General Search Module Start  --------------------------------------------------------------//   


router.post('/jazecom/jazemainsearch', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
    check('page').isNumeric(),
    check('country').isNumeric(),
    check('state').isNumeric(),
	check('city').isNumeric(),
	check('geo_type').isNumeric(),
	check('category').isNumeric(),
	check('geo_child_id').isNumeric(),
    
], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id	: req.body.account_id,
        keyword 	: req.body.keyword,
        api_token	: req.body.api_token,
        limit 		: req.body.page,
        country 	: req.body.country,
        state 		: req.body.state,
		city 		: req.body.city,
		area 		: 0,
		geo_type 	: req.body.geo_type,
		category 	: req.body.category,
		geo_child_id 	: req.body.geo_child_id
	};

	// category  :  1= company,  2 = profile,  3 = user 

	// geo_type  :  1= international,  2 = national,  3 = local,  4 = selected state result,  5 = all result without current country,  6 = all result without current state
 
	// geo_child_id :  selected state id

	// detail_flag : 1 = company profile, 2 = professional profile, 3 = company profile, 4 =  work profile


	HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

        if (typeof queParamRes !== 'undefined' && queParamRes !== null && parseInt(queParamRes.length) !== 0)
        {  
			HelperJazecom.__getAllLinkAccountList(reqParams,function(linkAccountList) {

				Object.assign(reqParams, {link_account_list: linkAccountList});
				
				HelperJazecom.getCountResult(reqParams,function(category_count) {
					
					if(parseInt(reqParams.category) === 1)
					{
						HelperJazecom.getCompanyLocalCount(reqParams,function(type_count) {

							//Company
							HelperJazecom.getAutofillCompanyResults(reqParams,function(comp_results) {

								//Company International or National
								HelperJazecom.getAutofillCompanyInternationalNationalResults(reqParams,function(inter_result,national_result) {

									var company_result = [];

									var pagination_details  = {
										total_page   : "0",
										current_page : "0",
										next_page    : "0"
									};

									var status = 0;

									if(comp_results.result !== null)
									{ 
										company_result = sort_name_key(comp_results.result,reqParams.keyword);

										var next_page = (parseInt(reqParams.limit) + 1);

										if(parseInt(comp_results.tot_page) <= parseInt(next_page))
										{ next_page = "0"; }

										pagination_details  = {
												total_page   : comp_results.tot_page.toString(),
												current_page : reqParams.limit.toString(),
												next_page    : next_page.toString()
											};

										status = 1;	
									}


									if(inter_result !== null || national_result !== null)
									{ status = 1; }


									if(parseInt(status) === 1)
									{
										var search_param = {
												geo_type           : reqParams.geo_type,
												category           : reqParams.category,
												keyword            : reqParams.keyword,
												country            : reqParams.country,
												state              : reqParams.state,
												city               : reqParams.city
										} 
						
										var result = {
											company_result       : company_result,
											prof_result          : [],
											user_result          : [],
											international_result : inter_result,
											national_result      : national_result
										}
						
										var objectName = {
												search_param           : search_param,
												pagination_details     : pagination_details,
												category_count         : category_count,
												type_count		   	   : type_count,
												search_result_general  : result
										};
												
										
										jsonRes = {
											success: '1',
											result: objectName,
											successMessage: "General Search Result",
											errorMessage: "" 
										};		
																
										res.status(201).json(jsonRes);
										res.end();	
									}
									else
									{
										jsonRes = {
											success: '0',
											result: {},
											successMessage: "",
											errorMessage: "no result found" 
										};		
																
										res.status(201).json(jsonRes);
										res.end();	
									}	
								});		
							});
						});
					}
					else if(parseInt(reqParams.category) === 2)
					{  
						HelperJazecom.getProfileLocalCount(reqParams,function(type_count) {
							
							//Profile
							HelperJazecom.getAutofillProfileResults(reqParams,function(prof_results) {
								
								//Profile International or National
								HelperJazecom.getAutofillProfileInternationalNationalResults(reqParams,function(inter_result,national_result) {

									var profile_result = [];

									var pagination_details  = {
										total_page   : "0",
										current_page : "0",
										next_page    : "0"
									};

									var status = 0;

									if(prof_results.result !== null)
									{ 
										profile_result = sort_name_key(prof_results.result,reqParams.keyword); 

										var next_page = (parseInt(reqParams.limit) + 1);

										if(parseInt(prof_results.tot_page) <= parseInt(next_page))
										{ next_page = "0"; }

										pagination_details  = {
												total_page   : prof_results.tot_page.toString(),
												current_page : reqParams.limit.toString(),
												next_page    : next_page.toString()
											};

										status = 1;	
									}


									if(inter_result !== null || national_result !== null)
									{ status = 1; }


									if(parseInt(status) === 1)
									{
										var search_param = {
												geo_type           : reqParams.geo_type,
												category           : reqParams.category,
												keyword            : reqParams.keyword,
												country            : reqParams.country,
												state              : reqParams.state,
												city               : reqParams.city
										} 
						
										var result = {
											company_result       : [],
											prof_result          : profile_result,
											user_result          : [],
											international_result : inter_result,
											national_result      : national_result
										}
						
										var objectName = {
												search_param           : search_param,
												pagination_details     : pagination_details,
												category_count         : category_count,
												type_count		   	   : type_count,
												search_result_general  : result
										};
												
										
										jsonRes = {
											success: '1',
											result: objectName,
											successMessage: "General Search Result",
											errorMessage: "" 
										};		
																
										res.status(201).json(jsonRes);
										res.end();	
									}
									else
									{
										jsonRes = {
											success: '0',
											result: {},
											successMessage: "",
											errorMessage: "no result found" 
										};		
																
										res.status(201).json(jsonRes);
										res.end();	
									}	
								});		
							});
						});
					}
					else if(parseInt(reqParams.category) === 3)
					{
						HelperJazecom.getUserLocalCount(reqParams,function(type_count) {
						
							//Products
							HelperJazecom.getAutofillUserResults(reqParams,function(user_result) {
								
								//Products International or National
								HelperJazecom.getAutofillUserInternationalNationalResults(reqParams,function(inter_result,national_result) {

									var user_results = [];

									var pagination_details  = {
										total_page   : "0",
										current_page : "0",
										next_page    : "0"
									};

									var status = 0;

									if(user_result.result !== null)
									{ 
										user_results = sort_name_key(user_result.result,reqParams.keyword);

										var next_page = (parseInt(reqParams.limit) + 1);

										if(parseInt(user_result.tot_page) <= parseInt(next_page))
										{ next_page = "0"; }

										pagination_details  = {
												total_page   : user_result.tot_page.toString(),
												current_page : reqParams.limit.toString(),
												next_page    : next_page.toString()
											};

										status = 1;	
									}


									if(inter_result !== null || national_result !== null)
									{ status = 1; }


									if(parseInt(status) === 1)
									{
										var search_param = {
												geo_type           : reqParams.geo_type,
												category           : reqParams.category,
												keyword            : reqParams.keyword,
												country            : reqParams.country,
												state              : reqParams.state,
												city               : reqParams.city
										} 
						
										var result = {
											company_result       : [],
											prof_result          : [],
											user_result          : user_results,
											international_result : inter_result,
											national_result      : national_result
										}
						
										var objectName = {
												search_param           : search_param,
												pagination_details     : pagination_details,
												category_count         : category_count,
												type_count		   	   : type_count,
												search_result_general  : result
										};
												
										
										jsonRes = {
											success: '1',
											result: objectName,
											successMessage: "General Search Result",
											errorMessage: "" 
										};		
																
										res.status(201).json(jsonRes);
										res.end();	
									}
									else
									{
										jsonRes = {
											success: '0',
											result: {},
											successMessage: "",
											errorMessage: "no result found" 
										};		
																
										res.status(201).json(jsonRes);
										res.end();	
									}	
								});		
							});
						});
					}
				});
			});
        }
        else
        {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };
            res.status(statRes).json(jsonRes);
            res.end();
        }
   });
   
});

function sort_name_key(array,keyword)
{
return array.filter(o => o.name.toLowerCase().includes(keyword.toLowerCase()))
                  .sort((a, b) => a.name.toLowerCase().indexOf(keyword.toLowerCase()) - b.name.toLowerCase().indexOf(keyword.toLowerCase()));
}

// ------------------------------------------------------------------- General Search Module End  --------------------------------------------------------------//   


module.exports = router;