'use strict';
// content of fcm_node.js

require('events').EventEmitter.defaultMaxListeners = 100;
require('events').EventEmitter.prototype._maxListeners = 100;


const bodyParser = require('body-parser');
const express = require('express');
const router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
//const app = express();


const { check, validationResult } = require('express-validator');


const HelperRestriction = require('./../helpers/HelperRestriction.js');
const HelperJazechat = require('./helpers/HelperJazechat.js');
const Helperfirestore = require('./firestore.js');
const fast_sort = require("fast-sort");

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};




//------------------------------------------------------------------- Contact Module Start ---------------------------------------------------------------------------//

/*
* Pending contact list
* Not Use
*/
router.post('/jazechat/contact_list', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('flag').isNumeric()

], (req, res, next) => {

	var _send = res.send;
    var sent = false;

    res.send = function(data){
       if(sent) return;
       _send.bind(res)(data);
       sent = true;
	};
	

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id: req.body.account_id,
		account_type: req.body.account_type,
		flag: req.body.flag,
		api_token: req.body.api_token
	};

	HelperRestriction.checkParametersApiToken(reqParams, function (queParam) {

		if (queParam.length) {

			HelperJazechat.getAccountContactList(reqParams, function (conDet) {

					if(typeof conDet !== 'undefined' || conDet !== null || parseInt(conDet.length) !== 0){	

						if(parseInt(reqParams.flag) === 1)
						{
							// contact list
								jsonRes = {
									success: '1',
									result: { contact_list: conDet },
									successMessage:  ' contact list.',
									errorMessage: errorMessage
								};

								res.status(201).json(jsonRes);
								res.end();
						}
						else if(parseInt(reqParams.flag) === 2)
						{
							//incoming and outgoing request
							jsonRes = {
								success: '1',
								result: conDet,
								successMessage:  ' pending list.',
								errorMessage: errorMessage
							};

							res.status(201).json(jsonRes);
							res.end();
						}
						else {
							jsonRes = {
								success: success,
								result: {},
								successMessage: 'No result found.',
								errorMessage: errorMessage
							};					
				
							res.status(statRes).json(jsonRes);
							res.end();					
						}

					}
					else {
						jsonRes = {
							success: success,
							result: {},
							successMessage: 'No result found.',
							errorMessage: errorMessage
						};					
			
						res.status(statRes).json(jsonRes);
						res.end();					
					}
			});
		}
		else {
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found."
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

/*
* Jazechat Inbox list
*/
router.post('/jazechat/pending_contactlist', [
	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),

], (req, res, next) => {

	var _send = res.send;
    var sent = false;

    res.send = function(data){
       if(sent) return;
       _send.bind(res)(data);
       sent = true;
	};
	

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id: req.body.account_id,
		account_type: req.body.account_type,
		api_token: req.body.api_token
	};

	HelperRestriction.checkParametersApiToken(reqParams, function (queParam) {

		if (queParam.length) {

			HelperJazechat.getContactPendingList(reqParams, function (list) {

				if(typeof list !== 'undefined' || list !== null || parseInt(list.length) !== 0)
				{
					//incoming and outgoing request
					jsonRes = {
						success: '1',
						result: { pending_list: fast_sort(list).desc('date')  },
						successMessage:  ' pending list.',
						errorMessage: errorMessage
					};

					res.status(201).json(jsonRes);
					res.end();
				}
				else {
					jsonRes = {
						success: success,
						result: {},
						successMessage: 'No result found.',
						errorMessage: errorMessage
					};					
				
					res.status(statRes).json(jsonRes);
					res.end();					
				}
			});
		}
		else {
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found."
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

/*
* contact new request
*/
router.post('/jazechat/contact_request', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('request_account_id').isNumeric(),
	check('request_account_type').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id: req.body.account_id,
		account_type: req.body.account_type,
		request_account_id: req.body.request_account_id,
		request_account_type: req.body.request_account_type,
		api_token: req.body.api_token,
		message: req.body.message
	};

	
	HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

		if (queParamRes.length) {

			if(parseInt(reqParams.account_id) === parseInt(reqParams.request_account_id))
			{
				jsonRes = {
					success: "0",
					result: {},
					successMessage: successMessage,
					errorMessage: "Sorry, can't communicate same account." 
				};

				res.status(201).json(jsonRes);
				res.end();
			}
			else
			{
				HelperJazechat.checkAccountJazecomChatStatus(reqParams, function(retAccount){
				
					if(parseInt(retAccount) !== 1)
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Sorry, the service isn't available right now. Please try again later." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}else{

						HelperJazechat.checkContactAlreadyExists(reqParams, function (conDet) {
				
							if(conDet === null){
								
							//testing
							//if(typeof conDet !== 'undefined' || conDet !== null || parseInt(conDet.length) !== 0){	

								HelperJazechat.saveContactRequest(reqParams, function (checkResult) {
								
									if (parseInt(checkResult.flag) === 1) {
									
										//call converstion list
										HelperJazechat.getActiveConversationList(reqParams.account_id, function (activeList) {

											var converstion_list = {};
											
											if(typeof activeList !== 'undefined' && activeList !== null && parseInt(activeList.length) > 0){
													
												converstion_list = fast_sort(activeList).desc('order_by');
											}


											var obj_contact = {
												convers_id: checkResult.group_id.toString(),
												account_id: reqParams.request_account_id
											};

											var innerRes = {
												flag: '1',
												contact_conversation: {
													conversation_list: converstion_list,
													request_contacts: obj_contact
												}
											};

											jsonRes = {
												success: '1',
												result: innerRes,
												successMessage: 'Conversation List.',
												errorMessage: errorMessage
											};
											
											statRes = 201;
											res.status(statRes).json(jsonRes);
											res.end();
										});
									}
									else if (parseInt(checkResult.flag) == 2) {

										var innerRes = {
											flag: '0',
											contact_conversation: {}
										};

										jsonRes = {
											success: '1',
											result: innerRes,
											successMessage: 'contact request successfully sent.',
											errorMessage: errorMessage
										};

										statRes = 201;
										res.status(statRes).json(jsonRes);
										res.end();
									}
									else {

										jsonRes = {
											success: success,
											result: {},
											successMessage: successMessage,
											errorMessage: ' Cannot fulfil the process.'
										};
					
										res.status(statRes).json(jsonRes);
										res.end();
									}
								});
							}
							else {

								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: 'You have already send a contact request to this account.'
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
						});
					}
				});
			}
		}
		else {
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found."
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

/*
* contact new request
*/
router.post('/jazechat/check_jazechat_conversation', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('request_account_id').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id: req.body.account_id,
		request_account_id: req.body.request_account_id,
		api_token: req.body.api_token
	};

	HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

		if (queParamRes.length) {

			if(parseInt(reqParams.account_id) === parseInt(reqParams.request_account_id))
			{
				jsonRes = {
					success: "0",
					result: {},
					successMessage: successMessage,
					errorMessage: "Sorry, can't communicate same account." 
				};

				res.status(201).json(jsonRes);
				res.end();
			}
			else
			{
				HelperJazechat.checkAccountJazecomChatStatus(reqParams, function(retAccount){
					
					if(parseInt(retAccount) === 3)
					{
						jsonRes = {
							success: "1",
							result: {
								status : "0",
								convers_id : "0",
								receiver_account_id : reqParams.request_account_id
							},
							successMessage: successMessage,
							errorMessage: "Sorry, the service isn't available right now. Please try again later." 
						};

						res.status(201).json(jsonRes);
						res.end();
					}
					else if(parseInt(retAccount) === 2)
					{
						jsonRes = {
							success: "1",
							result: {
								status : "4",
								convers_id : "0",
								receiver_account_id : reqParams.request_account_id
							},
							successMessage: successMessage,
							errorMessage: 'Sorry, The account has been blocked.',
						};

						res.status(201).json(jsonRes);
						res.end();
					}
					else if(parseInt(retAccount) === 1)
					{
						HelperJazechat.checkContactAlreadyExists(reqParams, function (conDet) {
							
							if(conDet !== null){

								if(parseInt(conDet.status) === 1)
								{
									jsonRes = {
										success: "1",
										result: {
											status : "1",
											convers_id : conDet.conversa_id,
											receiver_account_id : reqParams.request_account_id
										},
										successMessage: "jazechat conversation details",
										errorMessage: errorMessage
									};
				
									res.status(201).json(jsonRes);
									res.end();
								}
								else {

									jsonRes = {
										success: "1",
										result: {
											status : "3",
											convers_id : "0",
											receiver_account_id : reqParams.request_account_id
										},
										successMessage: "jazechat conversation details",
										errorMessage: errorMessage
									};
				
									res.status(201).json(jsonRes);
									res.end();
								}
							}
							else
							{
								jsonRes = {
									success: "1",
									result: {
										status : "2",
										convers_id : "0",
										receiver_account_id : reqParams.request_account_id
									},
									successMessage: "jazechat conversation details",
									errorMessage: errorMessage
								};
			
								res.status(201).json(jsonRes);
								res.end();
							}
						});
					}
					else
					{
						jsonRes = {
							success: "0",
							result: {
								status : "0",
								convers_id : "0",
								receiver_account_id : reqParams.request_account_id
							},
							successMessage: successMessage,
							errorMessage: "Sorry, the service isn't available right now. Please try again later." 
						};

						res.status(201).json(jsonRes);
						res.end();
					}
				});
			}
		}
		else
        {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };
            res.status(statRes).json(jsonRes);
            res.end();
        }
	});
});


//contact request status
router.post('/jazechat/contact_acceptreject', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('convers_account_id').isNumeric(),
	check('convers_id').isNumeric(),
	check('status').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};


	var errors = validationResult(req);

	
	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id: req.body.account_id,
		account_type: req.body.account_type,
		request_account_id: req.body.convers_account_id,
		request_account_type : "0",
		convers_id: req.body.convers_id,
		status: req.body.status,
		api_token: req.body.api_token
	};

	HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

		if (queParamRes.length) {

			if(parseInt(reqParams.account_id) === parseInt(reqParams.request_account_id))
			{
				jsonRes = {
					success: "0",
					result: {},
					successMessage: successMessage,
					errorMessage: "Sorry, can't communicate same account." 
				};

				res.status(201).json(jsonRes);
				res.end();
			}
			else
			{
				HelperJazechat.checkContactAlreadyExists(reqParams, function (conDet) {
		
					if(conDet !== null){
					
						HelperJazechat.saveAcceptRejectBlockContactRequest(reqParams, function (rejResult) {
							
						//	const datefull = dateFormat(new Date(), 'HH:MM:ss');

							if(typeof rejResult !== 'undefined' && rejResult !== null && parseInt(rejResult.length) !== 0){

								var return_res = {
									flag: reqParams.status
								};
							
								if(parseInt(rejResult.flag) === 1)
								{
									//call converstion list
									HelperJazechat.getActiveConversationList(reqParams.account_id, function (activeList) {
										
										var converstion_list = [];
										
										if(typeof activeList !== 'undefined' && activeList !== null && parseInt(activeList.length) > 0){
												
											converstion_list = fast_sort(activeList).desc('order_by');
										}
										

										var obj_contact = {
											convers_id: rejResult.group_id.toString(),
											account_id: reqParams.request_account_id.toString()
										};

										var innerRes = {
											flag: '1',
											contact_conversation: {
												conversation_list: converstion_list,
												request_contacts: obj_contact
											}
										};
										
										jsonRes = {
											success: '1',
											result: innerRes,
											successMessage: 'Conversation List.',
											errorMessage: errorMessage
										};
										
										statRes = 201;
										res.status(statRes).json(jsonRes);
										res.end();
									});
								}
								else if(parseInt(rejResult.flag) === 2){

									jsonRes = {
										success: '1',
										result: return_res,
										successMessage: 'Contact request is rejected.',
										errorMessage: errorMessage
									};

									statRes = 201;
									res.status(statRes).json(jsonRes);
									res.end();
								}
								else if(parseInt(rejResult.flag) === 3){

									jsonRes = {
										success: '1',
										result: return_res,
										successMessage: 'Contact request is removed.',
										errorMessage: errorMessage
									};

									statRes = 201;
									res.status(statRes).json(jsonRes);
									res.end();
								}
								else if(parseInt(rejResult.flag) === 0){

									jsonRes = {
										success: '1',
										result: return_res,
										successMessage: 'Contact is blocked.',
										errorMessage: errorMessage
									};

									statRes = 201;
									res.status(statRes).json(jsonRes);
									res.end();
								}
								else {
									jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "No results found."
									};
		
									res.status(statRes).json(jsonRes);
									res.end();
								}

							}
							else {
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "No result found."
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}

						});	
					}
					else {
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: 'No result found.',
						};
			
						res.status(statRes).json(jsonRes);
						res.end();
					}
				});
			}
		}
		else {
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found."
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});
});


//------------------------------------------------------------------- Contact Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Conversation Module Start ---------------------------------------------------------------------------//

/*
*conversation list
*/
router.post('/jazechat/conversation_list', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var apiResult = [];

	var arrParams = {
		account_id: req.body.account_id,
		request_account_id: req.body.request_account_id,
		api_token: req.body.api_token
	};
	
	HelperRestriction.checkParametersApiToken(arrParams, function (queParam) {
		if (queParam.length) {
			
					HelperJazechat.getActiveConversationList(arrParams.account_id, function (activeList) {

						if(typeof activeList !== 'undefined' && activeList !== null && parseInt(activeList.length) > 0){

							jsonRes = {
								success: '1',
								result: { conversation_list: fast_sort(activeList).desc('order_by') },
								successMessage: "Conversation List",
								errorMessage: ""
							};
							res.status(201).json(jsonRes);
							res.end();
						}
						else {

									jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "No result found."
									};

									res.status(statRes).json(jsonRes);
									res.end();
						}
					});
					
		}
		else {
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found."
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});

/*
* contact new request
*/
router.post('/jazechat/delete_conversation', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('convers_id').isNumeric(),
	check('request_account_id').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id : req.body.account_id,
		convers_id : req.body.convers_id,
		request_account_id : req.body.request_account_id,
		api_token  : req.body.api_token
	};

	
	HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

		if (queParamRes.length) {

			HelperJazechat.checkContactAlreadyExists(reqParams, function (conDet) {
						
				if(conDet !== null){

					//delete
				}
			});


		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});
	


router.post('/jazechat/convers_report', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('convers_id').isNumeric(),
	check('report_message').isLength({ min: 1 })

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}

	var arrParams = {
		account_id: req.body.account_id,
		api_token: req.body.api_token,
		convers_id : req.body.convers_id,
		report_message : req.body.report_message
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(queParam.length)
		{
			HelperJazechat.saveConversationRreportDetails(arrParams.account_id, function (respo) {

				if(parseInt(respo) !== 0)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Report is successfully posted.',
						errorMessage: errorMessage
					};

					statRes = 201;
					res.status(statRes).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Failed to post the report. Please try again."
					};

						res.status(statRes).json(jsonRes);
						res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});


//contact block Unblock
router.post('/jazechat/contact_blockunblock', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('convers_id').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);

	
	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id: req.body.account_id,
		account_type: req.body.account_type,
		convers_id: req.body.convers_id,
		api_token: req.body.api_token
	};

	
	HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

		if (queParamRes.length) {

			// status :  1 = block , 0 = unblock 
			HelperJazechat.saveContactBlockUnblock(reqParams, function (block_status) {
				
				if(parseInt(block_status) !== null)
				{
						var successMsg = "";

						if(parseInt(block_status.block_out_status) === 1)
						{ successMsg = "contact has been blocked"; }
						else
						{ successMsg = "contact has been unblocked"; }
						
							//push notification
							HelperJazechat.sendPushBlockContact(reqParams,block_status.block_out_status,function (updaResult){
								
								jsonRes = {
									success: '1',
									result: {block_status : block_status},
									successMessage: successMsg,
									errorMessage: errorMessage
								};
		
								res.status(201).json(jsonRes);
								res.end();
							});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Failed to update block."
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});

});




router.post('/jazechat/clear_conversation', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('convers_id').isNumeric(),
	check('convers_flag').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	

	var arrParams = {
		api_token: req.body.api_token, 
		account_id: req.body.account_id, 
		convers_id : req.body.convers_id,
		convers_flag : req.body.convers_flag
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{	

			//insert or update Mute Details

			HelperJazechat.saveStartandEndDateDetails(arrParams, function(insertRes){	
				
				if(insertRes !== null)
				{
					jsonRes = {
						success: '1',
						result: {
							conv_date_details:insertRes
						},
						successMessage: 'conversation list successfully cleared.',
						errorMessage: errorMessage
					};

					statRes = 201;
					res.status(statRes).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "unable to clear conversation list. Please try again."
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

});


router.post('/jazechat/mute_conversation', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('convers_id').isNumeric(),
	check('mute_status').isNumeric(),
	check('mute_duration').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	

	var arrParams = {
		api_token: req.body.api_token, 
		account_id: req.body.account_id, 
		convers_id : req.body.convers_id, 
		group_id : req.body.convers_id, 
		mute_status : req.body.mute_status, 
		mute_duration : req.body.mute_duration 
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{	

			//insert or update Mute Details

			HelperJazechat.saveMuteDetails(arrParams, function(insertRes){	

				if(parseInt(insertRes) !== 0)
				{

					HelperJazechat.getMuteDetails(arrParams, function(retMuteStat){
				
						if(retMuteStat!=null)
						{
							jsonRes = {
								success: '1',
								result: {mute_details: retMuteStat},
								successMessage: 'Notification settings is successfully saved.',
								errorMessage: errorMessage
							};

							statRes = 201;
							res.status(statRes).json(jsonRes);
							res.end();
						}
					});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to save notification settings. Please try again."
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

});


router.post('/jazechat/get_mute_conversation', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('convers_id').isNumeric()


], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var arrParams = { 
		account_id: req.body.account_id,
		api_token: req.body.api_token,
		group_id:req.body.convers_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{	

			HelperJazechat.getMuteDetails(arrParams, function(retMuteStat){
				
				if(retMuteStat!=null)
				{
					jsonRes = {
						success: '1',
						result: {mute_details: retMuteStat},
						successMessage: 'Mute Details.',
						errorMessage: errorMessage
					};

					statRes = 201;
					res.status(statRes).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
			  		};
				}

			});

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

});


router.post('/jazechat/get_contact_setting', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('convers_id').isNumeric(),
	check('convers_flag').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}

	var arrParams = {
			account_id: req.body.account_id,
			api_token: req.body.api_token,
			convers_id:req.body.convers_id,
			convers_flag:req.body.convers_flag,
			group_id:req.body.convers_id
		};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{	
			HelperJazechat.getContactBlockStatus(arrParams, function(block_status){

				HelperJazechat.getMuteDetails(arrParams, function(retMuteStat){

					HelperJazechat.getBroadcastMembersConversList(arrParams, function(retMembCount){

						var members_count = "0";

						if(retMembCount !== null)
						{ members_count = retMembCount.length.toString(); }


						var innerObj = {
								convers_flag:arrParams.convers_flag,
								block_status:block_status,
								mute_status:retMuteStat,
								members_count:members_count
							};

							jsonRes = {
								success: '1',
							result: innerObj,
							successMessage: 'Contact Settings',
							errorMessage: errorMessage
						};

						res.status(201).json(jsonRes);
						res.end();
					});
				});
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

})


router.post('/jazechat/set_read_status', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('convers_id').isNumeric(),
	check('convers_flag').isNumeric(),
	check('flag').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}

	var arrParams = {
			account_id: req.body.account_id,
			api_token: req.body.api_token,
			convers_id:req.body.convers_id,
			convers_flag:req.body.convers_flag,
			flag:req.body.flag
		};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{	
			HelperJazechat.setMessageReadStatus(arrParams, function(retStatus){

				if(retStatus !== null)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'successfully updated',
						errorMessage: errorMessage
					};

					res.status(201).json(jsonRes);
					res.end();
				}
				else
			    {
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to load database. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
			    }
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

})




router.post('/jazechat/get_all_media', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('convers_id').isNumeric(),


], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var account_id = req.body.account_id;
	var api_token = req.body.api_token;
	var conv_id = req.body.convers_id;

	var arrParams = {
		account_id: account_id,
		api_token: api_token,
		conv_id: conv_id
	};


	HelperRestriction.checkParametersApiToken(arrParams, function (queResult) {

		if(queResult.length) 
		{

			Helperfirestore.getAllSynchronizedMedialist(arrParams.conv_id, function (retMedia) {
		
				if(retMedia!=null)
				{
	       			jsonRes = {
						success: '1',
						result: {media_list:retMedia},
						successMessage: "Media Lis",
						errorMessage: errorMessage 
					};

					res.status(201).json(jsonRes);
					res.end();
		
			    }
			    else
			    {
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to load database. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
			    }
	
	    	});
		}
		else 
		{

			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});


});




router.post('/jazechat/forward_chat', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('convers_id').isNumeric(),
	check('conversation').isLength({ min: 1 }),
	check('receivers_convers_id').isLength({ min: 1 })

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req.body[0]);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}


	var arrParams = {
		api_token	 	   :req.body[0].api_token,
		account_id	 	   :req.body[0].account_id,
		account_type	   :req.body[0].account_type,
		convers_id	 	   :req.body[0].convers_id,
		conversation 	   :req.body[0].conversation,
		receiv_convers_id  :req.body[0].receivers_convers_id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(queParam.length)
		{
			var i = 0;
			HelperJazechat.getForwardConversationList(arrParams, function (membList) {
			
				if(membList !== null && i === 0)
				{
					i++;
					Helperfirestore.saveForwardMessages(arrParams,membList, function(retUnlink){		

							if(retUnlink==1){

					            jsonRes = {
					    			success: '1',	
					                result: {},
					 				successMessage: "Chat has been forwarded.",
					                errorMessage: errorMessage 
					            };
					            res.status(201).json(jsonRes);
					            res.end();

							}else{

								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
						  		};

					  			res.status(statRes).json(jsonRes);
								res.end();
							}
					});
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});


router.post('/jazechat/delete_file', [

    check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('convers_id').isNumeric(),
	check('conversation').isLength({ min: 1 }),
	check('flag').isNumeric(),

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req.body[0]);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}

	var arrParams = {
		api_token	 :req.body[0].api_token,
		account_id	 :req.body[0].account_id,
		convers_id	 :req.body[0].convers_id,
		conversation :req.body[0].conversation,
		flag	 	 :req.body[0].flag
	};




	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(queParam.length)
		{
			Helperfirestore.removeChatMedia(arrParams, function(retUnlink){		

						if(parseInt(retUnlink)  === 1)
						{
		 		            jsonRes = {
	                			success: '1',	
				                result: {},
			     				successMessage: "File is successfully deleted.",
				                errorMessage: errorMessage 
				            };
				            res.status(201).json(jsonRes);
				            res.end();
						}
						else if(parseInt(retUnlink)  === 0)
						{

	    					jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "File not found." 
					  		};

				  			res.status(statRes).json(jsonRes);
							res.end();
						}
						else 
						{

	    					jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Unable to process request. Please try again." 
					  		};

				  			res.status(statRes).json(jsonRes);
							res.end();
						}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});
});



//------------------------------------------------------------------- Conversation Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- broadcasting Module Start ---------------------------------------------------------------------------//

/*
 * create new broadcasting
 */
router.post('/jazechat/create_broadcastdetails', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('account_type').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id: req.body.account_id,
		account_type: req.body.account_type,
		broadcast_name: req.body.broadcast_name,
		members_convers_id: req.body.members_convers_id,
		message: req.body.message,
		api_token: req.body.api_token
	};

	
	HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

		if (queParamRes.length) {

			HelperJazechat.checkBroadcastNameExist(reqParams, function (checkResult) {

				if(parseInt(checkResult)  === 1)
				{
					HelperJazechat.saveNewBroadcastingDetails(reqParams, function (rejResult) {
						
						if(rejResult !== 0)
						{
							//call converstion list
							HelperJazechat.getActiveConversationList(reqParams.account_id, function (activeList) {
									
								var converstion_list = [];
								
								if(typeof activeList !== 'undefined' && activeList !== null && parseInt(activeList.length) > 0){
		
									converstion_list = fast_sort(activeList).desc('order_by');
								}
								
								var obj_contact = {
									convers_id: rejResult.group_id.toString()
								};

								var innerRes = {
									flag: '1',
									contact_conversation: {
										conversation_list: converstion_list,
										request_contacts: obj_contact
									}
								};
								
								jsonRes = {
									success: '1',
									result: innerRes,
									successMessage: 'Conversation List.',
									errorMessage: errorMessage
								};
								
								statRes = 201;
								res.status(statRes).json(jsonRes);
								res.end();
							});
						}
					});	
				}
				else {
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "broadcast name already exists."
					};
		
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else {
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found."
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

/*
 * update broadcasting name and logo
 */
router.post('/jazechat/update_broadcastdetails', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('convers_id').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id: req.body.account_id,
		convers_id	 :req.body.convers_id,
		broadcast_name: req.body.broadcast_name,
		broadcast_logo: req.body.broadcast_logo,
		api_token: req.body.api_token
	};

	HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

		if (queParamRes.length) {

			HelperJazechat.checkBroadcastNameExist(reqParams, function (checkResult) {

				if(parseInt(checkResult)  === 1 || (parseInt(reqParams.convers_id) === parseInt(checkResult)))
				{
					HelperJazechat.getBroadcastGroupId(reqParams, function (broadcast_group_id) {

						HelperJazechat.updateBroadcastName(broadcast_group_id,reqParams, function (broadcast_name) {

							HelperJazechat.updateBroadcastingLogo(broadcast_group_id,reqParams, function (broadcast_logo) {

								if(parseInt(broadcast_name)  !== 0 && parseInt(broadcast_logo)  !== 0)
								{
									HelperJazechat.getBroadcastDetails(reqParams.convers_id, function (brodcast) {

										if(brodcast  !== null)
										{
											jsonRes = {
												success: '1',	
												result: {broadcast_details:brodcast},
												successMessage: "broadcast successfully updated.",
												errorMessage: errorMessage 
											};
											res.status(201).json(jsonRes);
											res.end();
										}
										else {
											jsonRes = {
												success: success,
												result: {},
												successMessage: successMessage,
												errorMessage: "please choose valid broadcast details."
											};
								
											res.status(statRes).json(jsonRes);
											res.end();
										}
									});
								}
								else {
									jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "please choose valid broadcast details."
									};
						
									res.status(statRes).json(jsonRes);
									res.end();
								}
							});
						});
					});
				}
				else {
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "broadcast name already exists."
					};
		
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});	
		}
		else {
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found."
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});


/*
 * delete  broadcasting details
 */
router.post('/jazechat/delete_broadcastdetails', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('convers_id').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id: req.body.account_id,
		convers_id	 :req.body.convers_id,
		api_token: req.body.api_token
	};

	HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

		if (queParamRes.length) {

			HelperJazechat.deleteBroadcastingDetails(reqParams, function (checkResult) {

				if(parseInt(checkResult)  === 1)
				{
					jsonRes = {
						success: '1',	
						result: {},
						successMessage: "broadcast successfully deleted.",
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else {
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "please choose valid broadcast details."
					};
		
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else {
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found."
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});



/*
 * get members in broadcasting list
 */
router.post('/jazechat/get_broadcast_group_memberslist', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('convers_id').isNumeric(),
	check('convers_flag').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req.body);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}

	var reqParams = {
		api_token	 :req.body.api_token,
		account_id	 :req.body.account_id,
		convers_id	 :req.body.convers_id,
		convers_flag :req.body.convers_flag
	};

	HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

		if (queParamRes.length) {

			if(parseInt(reqParams.convers_flag) === 3)
			{
				//broadcast
				HelperJazechat.getBroadcastMembersList(reqParams, function (memersResult) {

					if(memersResult  !== null)
					{
						jsonRes = {
							success: '1',	
							result: {members_list:memersResult},
							successMessage: "broadcast members list.",
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else {
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "please choose valid broadcast details."
						};
			
						res.status(statRes).json(jsonRes);
						res.end();
					}
				});
			}
		}
		else {
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found."
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

/*
 * add or remove members in broadcasting list
 */
router.post('/jazechat/update_broadcast_group_memberslist', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('convers_id').isNumeric(),
	check('convers_flag').isNumeric(),
	check('flag').isNumeric(),
	check('members_list').isLength({ min: 1 })

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req.body);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}

	var reqParams = {
		api_token	 :req.body.api_token,
		account_id	 :req.body.account_id,
		convers_id	 :req.body.convers_id,
		convers_flag :req.body.convers_flag,
		members_list :req.body.members_list,
		flag 		 :req.body.flag
	};

	HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

		if (queParamRes.length) {

			HelperJazechat.updateBroadcastMembersList(reqParams, function (checkResult) {

				if(parseInt(checkResult)  === 1)
				{
					if(parseInt(reqParams.convers_flag) === 3)
					{
						//broadcast
						HelperJazechat.getBroadcastMembersList(reqParams, function (memersResult) {

							if(memersResult  !== null)
							{
								jsonRes = {
									success: '1',	
									result: {members_list:memersResult},
									successMessage: "members successfully updated..",
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();
							}
							else {
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "please choose valid broadcast details."
								};
					
								res.status(statRes).json(jsonRes);
								res.end();
							}
						});
					}
				}
				else {
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "please choose valid broadcast details."
					};
		
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else {
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found."
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

/*
 * send new broadcast message
 */
router.post('/jazechat/send_broadcastmessage', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('convers_id').isNumeric(),

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id	 : req.body.account_id,
		account_type : req.body.account_type,
		convers_id	 : req.body.convers_id,
		message		 : req.body.message,
		message_type : req.body.message_type,
		date 		 : req.body.date,
		created_time : req.body.created_time,
		time_zone	 : req.body.time_zone,
		api_token: req.body.api_token
	};

	HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

		if (queParamRes.length) {

			HelperJazechat.getBroadcastMembersList(reqParams, function(retMemb){
			
				if(retMemb  !== null)
				{ 
					HelperJazechat.checkBroadcastMembersBlockStatus(retMemb, function(listMemb){

						if(listMemb !== null)
						{
							Helperfirestore.sendBroadcastMessage(reqParams,listMemb,function(account_results) {
								
								if(parseInt(account_results) === 1)
								{
									HelperJazechat.updateBroadcastChatDate(reqParams.convers_id,reqParams.date, function(returnStatus){ 
										
										if(parseInt(returnStatus) === 1)
										{
											jsonRes = {
												success: '1',	
												result: {},
												successMessage: "message successfully sent.",
												errorMessage: errorMessage 
											};
											res.status(201).json(jsonRes);
											res.end();
										}
										else {
											jsonRes = {
												success: success,
												result: {},
												successMessage: successMessage,
												errorMessage: "please choose valid broadcast details."
											};
								
											res.status(statRes).json(jsonRes);
											res.end();
										}
									});
								}
								else {
									jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "please choose valid broadcast details."
									};
						
									res.status(statRes).json(jsonRes);
									res.end();
								}
							});
						}
					});
				}
				else {
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "please choose valid broadcast details."
					};
		
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else {
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found."
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

//------------------------------------------------------------------- broadcasting Module End ---------------------------------------------------------------------------//



module.exports = router;

