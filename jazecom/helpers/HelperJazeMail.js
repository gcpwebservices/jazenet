const methods = {};
const DB = require('./../../helpers/db_base.js');
const HelperRestriction = require('../../helpers/HelperRestriction.js');
const HelperGeneral = require('../../helpers/HelperGeneral.js');
const HelperNotification = require('./HelperNotification.js');
const HelperGeneralJazenet = require('../../jazenet/helpers/HelperGeneralJazenet.js');
const dateFormat = require('dateformat');
const now = new Date();
const standardDT = dateFormat(now,'yyyy-mm-dd HH:MM:ss');
const promiseForeach = require('promise-foreach');

const mime = require('mime-types');
const multer = require('multer');
const moment = require("moment");
var Imap = require('imap');
var mimemessage = require('mimemessage');

const mailer = require('nodemailer-promise');
const simpleParser = require('mailparser').simpleParser;

const _ = require('lodash');

// ----------------------------------------------------- Mail Account Accept and Reject -------------------------------------------------------------//


methods.rejectMailContact = function(arrParams,return_result)
{
	try
	{	

		var query = `SELECT * FROM jaze_jazecom_contact_list WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value =?) 
		AND group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'mail_blocked_account_id' AND FIND_IN_SET(?,meta_value)>0 )`;
		DB.query(query, [arrParams.account_id,arrParams.sender_account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				return_result(2); 
			}
			else
			{
				var query = `SELECT * FROM jaze_jazecom_contact_list WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value =?)`;
				DB.query(query, [arrParams.account_id], function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{

						methods.convertMultiMetaArray(data, function(retConverted){	

							var account_list = arrParams.sender_account_id;
							if(retConverted[0].mail_blocked_account_id !=="")
							{
								account_list = retConverted[0].mail_blocked_account_id+','+arrParams.sender_account_id;
							}
						
							var updateQue = `update jaze_jazecom_contact_list SET meta_value =? WHERE meta_key =? AND group_id =?`;

							DB.query(updateQue, [account_list,'mail_blocked_account_id',retConverted[0].group_id], function(data, error){
						 		if(data !== null){
					 				return_result(1);
						 	   	}else{
					 	   			return_result(0);
						 	   	}
							});

						});
					}
					else
					{

						var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazecom_contact_list";
			  			var lastGrpId = 0;

						methods.getLastGroupId(queGroupMax, function(queGrpRes){

						  	lastGrpId = queGrpRes[0].lastGrpId;

						  	var full_filename = arrParams.file_name+'.'+arrParams.file_ext;

							var arrInsert = {
								account_id               : arrParams.account_id,
								mail_account_id          : '',
								call_account_id          : '',
								chat_account_id          : '',
								feed_account_id          : '',
								diary_account_id         : '',
								call_blocked_account_id  : '',
								chat_blocked_account_id  : '',
								mail_blocked_account_id  : arrParams.sender_account_id,
								feed_blocked_account_id  : '',
								diary_blocked_account_id : '',
								create_date              : standardDT,
								status                   : 1
							};
						
							var plusOneGroupID = lastGrpId+1;
					  		var querInsert = `insert into jaze_jazecom_contact_list (group_id, meta_key, meta_value) VALUES (?,?,?)`;

						 	for (var key in arrInsert) 
						 	{
								DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
						 		 	if(parseInt(data.affectedRows) > 0){
				 						return_result(1);
							 	   	}else{
					   					return_result(0);
							 	   	}
						 	 	});
						  	};
						  	
						});
					}
				});
			}

		});
	
	}
	catch(err)
	{ 
		console.log("rejectMailContact",err); 
		return_result(0); 
	}	
};


methods.acceptMailContact = function(arrParams,return_result)
{
	try
	{	

		var query = `SELECT * FROM jaze_jazecom_contact_list WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value =?) 
		AND group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'mail_account_id' AND FIND_IN_SET(?,meta_value)>0 )`;
		DB.query(query, [arrParams.account_id,arrParams.sender_account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				return_result(2); 
			}
			else
			{
				var query = `SELECT * FROM jaze_jazecom_contact_list WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value =?)`;
				DB.query(query, [arrParams.account_id], function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{

						methods.convertMultiMetaArray(data, function(retConverted){	


							var account_list = arrParams.sender_account_id;
							if(retConverted[0].mail_account_id !=="")
							{
								account_list = retConverted[0].mail_account_id+','+arrParams.sender_account_id;
							}

							var updateQue = `update jaze_jazecom_contact_list SET meta_value =? WHERE meta_key =? AND group_id =?`;

							DB.query(updateQue, [account_list,'mail_account_id',retConverted[0].group_id], function(data, error){
						 		if(data !== null){
					 				return_result(1);
						 	   	}else{
					 	   			return_result(0);
						 	   	}
							});

						});
					}
					else
					{

						var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazecom_contact_list";
			  			var lastGrpId = 0;

						methods.getLastGroupId(queGroupMax, function(queGrpRes){

						  	lastGrpId = queGrpRes[0].lastGrpId;

						  	var full_filename = arrParams.file_name+'.'+arrParams.file_ext;

							var arrInsert = {
								account_id               : arrParams.account_id,
								mail_account_id          : arrParams.sender_account_id,
								call_account_id          : '',
								chat_account_id          : '',
								feed_account_id          : '',
								diary_account_id         : '',
								call_blocked_account_id  : '',
								chat_blocked_account_id  : '',
								mail_blocked_account_id  : '',
								feed_blocked_account_id  : '',
								diary_blocked_account_id : '',
								create_date              : standardDT,
								status                   : 1
							};
						
							var plusOneGroupID = lastGrpId+1;
					  		var querInsert = `insert into jaze_jazecom_contact_list (group_id, meta_key, meta_value) VALUES (?,?,?)`;

						 	for (var key in arrInsert) 
						 	{
								DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
						 		 	if(parseInt(data.affectedRows) > 0){
				 						return_result(1);
							 	   	}else{
					   					return_result(0);
							 	   	}
						 	 	});
						  	};
						  	
						});
					}
				});
			}

		});
	
	}
	catch(err)
	{ 
		console.log("acceptMailContact",err); 
		return_result(0); 
	}	
};

// ----------------------------------------------------- Mail Account Details -------------------------------------------------------------//

methods.proceessArrayEmails = function(arrEmails,arrParams,return_result)
{
	
	try
	{	

		var arrResults = [];
		var newobjMessage = {};

		var counter = arrEmails.length;		
		var jsonItems = Object.values(arrEmails);

		promiseForeach.each(jsonItems,[function (jsonItems) {
			return new Promise(function (resolve, reject) {	

	
				methods.getAccountCredentialsFrom(jsonItems.from_email, function(retFrom){

					var mailParams = {account_id:arrParams.account_id, contact_account_id:retFrom.account_id, contact_email_id:jsonItems.from_email };
					methods.checkMailContactStatus(mailParams, function(retStatus){

						var read_status = jsonItems.read_status;
						if(retStatus!=null){
							read_status = retStatus;
						}

			          	newobjMessage = {
			          		seq               : jsonItems.seq,
			          		id                : jsonItems.id,
							logo              : jsonItems.logo,
			          		subject           : jsonItems.subject,
	      		    		date              : jsonItems.date,
			         		from_account_id   : '',
			          		from_account_type : '',
	      		    		from_name         : jsonItems.from_name,
	      		    		from_logo         : '',
			          		from_email        : jsonItems.from_email,
			          		attachment        : jsonItems.attachment,
			          		message           : jsonItems.message,
		          			read_status       : read_status
			     
			          	};

	  					if(retFrom.name!="" && typeof retFrom.name !== "undefined")
						{
				          	newobjMessage.from_name         = retFrom.name;
							newobjMessage.from_account_id   = retFrom.account_id;
							newobjMessage.from_account_type = retFrom.account_type;
							newobjMessage.from_logo         = retFrom.logo;
						}
					
	  					arrResults.push(newobjMessage);
						counter -= 1;
			
						if (counter === 0)
						{ 					
							resolve(arrResults);
						}

					});
				});
			})
		}],
		function (result, current) {

			if (counter === 0){ 
				return_result(result[0]); 
			}
		});

	}
	catch(err)
	{ 
		console.log("proceessArrayEmails",err); 
		return_result(account_details); 
	}	
};


methods.checkMailContactStatus = function(arrParams,return_result)
{
	try
	{	

		var check_email = arrParams.contact_email_id.split('@').pop();

		if(arrParams.contact_account_id !="" && check_email == "jazenet.com")
		{

			var query = `SELECT * FROM jaze_jazecom_contact_list WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value =?) 
					AND group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'mail_account_id' AND FIND_IN_SET(?,meta_value)>0 ) `;
			DB.query(query, [arrParams.account_id,arrParams.contact_account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result(null);
				}else{
					return_result('2');
				}

			});
		}
		else
		{
			return_result(null);
		}
	
	}
	catch(err)
	{ 
		console.log("checkMailContactStatus",err); 
		return_result(null); 
	}	
};


methods.getAccountCredentialsFrom = function(email_address,return_result)
{
	
	try
	{	

		var account_details = {
			account_id   : '',
			account_type : '',
			name         : '',
			logo         : '',
			email        : ''
		};

		var query = "SELECT * FROM jaze_user_credential_details WHERE jazemail_id ='"+email_address+"' ";
		DB.query(query, null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var accParams = {account_id: data[0].account_id};
	  			HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	
	  	
	  				if(retAccountDetails!=null){

						account_details = {

  							account_id   : retAccountDetails.account_id,
  							account_type : retAccountDetails.account_type,
							name         : retAccountDetails.name,
							logo         : retAccountDetails.logo						

		  				};
	  				
						return_result(account_details);
			
	  				}

				});	

			}
			else
			{
				return_result(account_details);
			}
		});	
	}
	catch(err)
	{ 
		console.log("getAccountCredentialsTO",err); 
		return_result(account_details); 
	}	
};

methods.getAccountCredentialsTO = function(email_address,return_result)
{
		
	try
	{	

		var arrResults = [];
		var arrEmails = [];

		for (var key in email_address) {
		    var obj = email_address[key].address;
		    arrEmails.push(obj);
		}

			
		if(parseInt(arrEmails.length) > 0)
		{

			var theString = arrEmails;
			var query = "SELECT * FROM jaze_user_credential_details WHERE jazemail_id ='"+theString+"' ";
			if(parseInt(arrEmails.length) > 1){
				theString = "'" + arrEmails.join("','") + "'";
				query = "SELECT * FROM jaze_user_credential_details WHERE jazemail_id IN("+theString+")";
			}

			DB.query(query, null, function(data, error){
	
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{

					var counter = data.length;		
					var jsonItems = Object.values(data);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

				  			var accParams = {account_id:jsonItems.account_id};

				  			HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

				  				if(retAccountDetails!=null){

									var account_details = {

			  							account_id:retAccountDetails.account_id,
			  							account_type:retAccountDetails.account_type,
										name:retAccountDetails.name,
										logo:retAccountDetails.logo,
										email:jsonItems.jazemail_id

					  				};

				  					arrResults.push(account_details);

		  							counter -= 1;
									if (counter === 0){ 
										resolve(arrResults);
									}
				  				}

							});	
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});


				}else{
					return_result([]);
				}
			});	


		}
		else
		{
			return_result([]);
		}

	}
	catch(err)
	{ 
		console.log("getAccountCredentialsTO",err); 
		return_result([]); 
	}	
};

methods.getAccountCredentialsCC = function(email_address,return_result)
{
		
	try
	{	

		var arrResults = [];
		var arrEmails = [];

		for (var key in email_address) {
		    var obj = email_address[key].address;
		    arrEmails.push(obj);
		}

			
		if(parseInt(arrEmails.length) > 0)
		{

			var theString = arrEmails;
			var query = "SELECT * FROM jaze_user_credential_details WHERE jazemail_id ='"+theString+"' ";
			if(parseInt(arrEmails.length) > 1){
				theString = "'" + arrEmails.join("','") + "'";
				query = "SELECT * FROM jaze_user_credential_details WHERE jazemail_id IN("+theString+")";
			}

			DB.query(query, null, function(data, error){
	
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{

					var counter = data.length;		
					var jsonItems = Object.values(data);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

				  			var accParams = {account_id:jsonItems.account_id};

				  			HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

				  				if(retAccountDetails!=null){

									var account_details = {

			  							account_id:retAccountDetails.account_id,
			  							account_type:retAccountDetails.account_type,
										name:retAccountDetails.name,
										logo:retAccountDetails.logo,
										email:jsonItems.jazemail_id

					  				};

				  					arrResults.push(account_details);

		  							counter -= 1;
									if (counter === 0){ 
										resolve(arrResults);
									}
				  				}

							});	
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});


				}else{
					return_result([]);
				}
			});	


		}
		else
		{
			return_result([]);
		}

	}
	catch(err)
	{ 
		console.log("getAccountCredentialsCC",err); 
		return_result([]); 
	}	
};

methods.getAccountCredentialsBCC = function(email_address,return_result)
{
		
	try
	{	

		var arrResults = [];
		var arrEmails = [];

		for (var key in email_address) {
		    var obj = email_address[key].address;
		    arrEmails.push(obj);
		}

			
		if(parseInt(arrEmails.length) > 0)
		{

			var theString = arrEmails;
			var query = "SELECT * FROM jaze_user_credential_details WHERE jazemail_id ='"+theString+"' ";
			if(parseInt(arrEmails.length) > 1){
				theString = "'" + arrEmails.join("','") + "'";
				query = "SELECT * FROM jaze_user_credential_details WHERE jazemail_id IN("+theString+")";
			}

			DB.query(query, null, function(data, error){
	
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{

					var counter = data.length;		
					var jsonItems = Object.values(data);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

				  			var accParams = {account_id:jsonItems.account_id};

				  			HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

				  				if(retAccountDetails!=null){

									var account_details = {

			  							account_id:retAccountDetails.account_id,
			  							account_type:retAccountDetails.account_type,
										name:retAccountDetails.name,
										logo:retAccountDetails.logo,
										email:jsonItems.jazemail_id

					  				};

				  					arrResults.push(account_details);

		  							counter -= 1;
									if (counter === 0){ 
										resolve(arrResults);
									}
				  				}

							});	
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});


				}else{
					return_result([]);
				}
			});	


		}
		else
		{
			return_result([]);
		}

	}
	catch(err)
	{ 
		console.log("getAccountCredentialsBCC",err); 
		return_result([]); 
	}	
};

// ----------------------------------------------------- Mail Account Details -------------------------------------------------------------//

//------------------------------------------------------ Mail Contacts Module Start  ------------------------------------------------------//

methods.searchEmailContacts = function(arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{
			var query ="SELECT meta_value FROM jaze_jazecom_contact_list WHERE meta_key = 'mail_account_id' "; 
				query += "and group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id'  AND meta_value ='"+arrParams.account_id+"')";

			DB.query(query,null, function(data, error){
		
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					if(data[0].meta_value != null)
					{
						var query = " SELECT `account_id`,`jazemail_id` FROM `jaze_user_credential_details` WHERE `status` = '1' and account_id in ("+data[0].meta_value+") and  `jazemail_id` like '%"+arrParams.key+"%' "; 
							
						DB.query(query,null, function(data, error){
								
							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

								var array_rows = [];

								var counter = data.length;

								if(parseInt(counter) !== 0)
								{
									promiseForeach.each(data,[function (jsonItems) {

										return new Promise(function (resolve, reject) {

											HelperGeneral.getAccountBasicDetails(jsonItems.account_id, function(account_result){	

												var result_obj  = { 
														account_id		: account_result.account_id.toString(),
														type			: account_result.account_type.toString(),
														name			: account_result.name.toString(),
														logo			: account_result.logo.toString(),
														email_address	: jsonItems.jazemail_id.toString()
												};

												array_rows.push(result_obj);
												counter -= 1;
												resolve(array_rows);
											});
										})
									}],
									function (result, current) {
										if (counter === 0)
										{return_result(result[0]); }
									});
								}
								else
								{return_result(null);}
							}
							else
							{return_result(null);}
						});
					}
					else
					{return_result(null);}
				}
				else
				{return_result(null);}
			});
		}
		else
		{return_result(null);}
	}
	catch(err)
	{
		console.log('searchEmailContacts',err);
		return_result(null);
	}





	// var arrContactsRaw = [];
	// var arrContacts = [];

	// let queGrpId = `SELECT t1.* FROM(
	// SELECT group_id,
	// 	(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
	// 	(SELECT a.meta_value FROM jaze_jazecom_contact_list a WHERE a.meta_key = 'mail_account_id' AND a.group_id = jaze_jazecom_contact_list.group_id) AS mail_account_id
	// FROM  jaze_jazecom_contact_list WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value =?) 
	// ) AS t1 WHERE account_id != ""`;


	// DB.query(queGrpId,arrParams.account_id, function(data, error){

	// 	if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

	// 		if(data[0].mail_account_id !=''){
	// 			data[0].mail_account_id.split(",").forEach(function(val) {
	// 				arrContactsRaw.push(val);
			
	// 	  		});
	// 		}

	// 		var uniqueArray = arrContactsRaw.filter(function(item, pos, self) {
	// 	    	return self.indexOf(item) == pos;
	// 		});

	// 		var valuesParams = {account_ids:uniqueArray,key:arrParams.key};

	// 		methods.checkCredentialsAccountIDandLike(valuesParams,arrParams, function(retCred){ 
				
   	// 			return_result(retCred);
	// 		});

	//    	}else{
	//    		return_result(null);
	//    	}

	// });

};

methods.searchExternalEmailContacts =  function(arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{
			var query =" SELECT email_id FROM `jaze_email_accounts_ids` WHERE account_id = '"+arrParams.account_id+"' and  CONCAT(',', email_id, ',') like '%"+arrParams.key+"%' ";

			DB.query(query,null, function(data, error){
								
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
					data = convert_array(data[0].email_id);

					var result = [];

					for( var i in data)
					{
						if(data[i].indexOf(arrParams.key)!= -1)
						{
							var result_obj  = { 
									account_id:'',
									name:'',
									type:'',
									logo:'',
									email_address:data[i]
							};

							result.push(result_obj);
						}
					}

					return_result(result);
				}
				else
				{ return_result(null); }
			});
		}
		else
		{return_result(null);}
	}
	catch(err)
	{
		console.log('searchExternalEmailContacts',err);
		return_result(null);
	}
}

function convert_array(array_val) {

	if(array_val !== null && array_val !== "")
	{
		var list = [];

		if(array_val.toString().indexOf(',') != -1 ){
			list = array_val.split(',');
		}
		else {
			list.push(array_val);
		}

		return list;
	}
	else
	{ return ""; }
}

//------------------------------------------------------ Mail Contacts Module End  ------------------------------------------------------//

//------------------------------------------------------ External Email Module Start  ------------------------------------------------------//

methods.getExternalEmailAccountDetails = function(arrParams,return_result){

	try
	{
		if(parseInt(arrParams.mail_id) !== 0)
		{
			var query = "SELECT * FROM jaze_email_accounts WHERE  "; 
				query += "group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'account_id'  AND meta_value ='"+arrParams.account_id+"' ";
				query += "and group_id  ='"+arrParams.mail_id+"')";

			DB.query(query,null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					HelperRestriction.convertMetaArray(data, function(retConverted){	

						var newObj = {	
								account_id			: retConverted.account_id.toString(),
								mail_id				: retConverted.group_id.toString(),
								mail_account_type	: retConverted.mail_type.toString(), 
								account_name		: retConverted.account_name,
								imap_host			: retConverted.imap_host,
								imap_port			: retConverted.imap_port,
								smtp_host			: retConverted.smtp_name,							
								smtp_port			: retConverted.smtp_port,
								email_address		: retConverted.email_id,
								is_ssl 				: retConverted.is_ssl .toString() 
							};
							
						return_result(newObj);

					});
				}
				else
				{ return_result(null); }
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("getExternalEmailAccountDetails",err); return_result(); }
};

methods.createUpdateExternalEmailDetails = function(arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{
			if(parseInt(arrParams.flag) === 1)
			{
				//create
				HelperRestriction.getNewGroupId('jaze_email_accounts', function (group_id) {

					if(parseInt(group_id) !== 0){

						var	arrInsert = {
								account_id	  	: arrParams.account_id,
								account_name  	: arrParams.account_name,
								mail_account_type :"5",
								mail_type		: arrParams.mail_account_type,
								email_id	  	: arrParams.email_address,
								email_password	: arrParams.password,
								imap_host		: arrParams.imap_host,
								imap_port		: arrParams.imap_port,
								smtp_name		: arrParams.smtp_host,
								smtp_port		: arrParams.smtp_port,
								is_default		: 0,
								is_ssl			: arrParams.is_ssl,
								create_date     : moment().utc().format('YYYY-MM-DD HH:mm:ss'),
								status 		    : '1'
							};
							
						HelperRestriction.insertTBQuery('jaze_email_accounts',group_id,arrInsert, function (queGrpId) {
							
							return_result(queGrpId);
						});
					}
					else
					{ return_result(0); }
				});
			}
			else if(parseInt(arrParams.flag) === 2 && parseInt(arrParams.mail_id) !== 0)
			{
				//update
				var	arrUpdate = {
					account_name  	: arrParams.account_name,
					mail_type		: arrParams.mail_account_type,
					email_id	  	: arrParams.email_address,
					email_password	: arrParams.password,
					imap_host		: arrParams.imap_host,
					imap_port		: arrParams.imap_port,
					smtp_name		: arrParams.smtp_host,
					smtp_port		: arrParams.smtp_port,
					is_ssl			: arrParams.is_ssl
				};

				HelperRestriction.updateQuery('jaze_email_accounts',arrParams.mail_id,arrUpdate, function (queGrpId) {

					return_result(queGrpId);
				});
			}
			else if(parseInt(arrParams.flag) === 0 && arrParams.mail_id !== "0")
			{ 
				var group_id = arrParams.mail_id.split(',');

				if(group_id !== null && parseInt(group_id.length) > 0)
				{
					//delete
					group_id.forEach(function(row) {
						var query = "delete from jaze_email_accounts where group_id = '"+row+"' ";
						DB.query(query,null, function(data, error){
							// 	return_result(1);
						});
					});

					return_result(1);
				}
				else
				{ return_result(0); }
			}
			else
			{ return_result(0); }
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{ console.log("createUpdateExternalEmailDetails",err); return_result(0); }
};

methods.checkExistingExternalEmailAccount = function(arrParams,return_result){

	if(parseInt(arrParams.flag) === 1)
	{
		var query = "SELECT * FROM jaze_email_accounts WHERE  "; 
			query += "group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'account_id'  AND meta_value ='"+arrParams.account_id+"' ";
			query += "and group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'email_id'  AND meta_value ='"+arrParams.email_address+"'))";

		DB.query(query,null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				return_result(0);
			}
			else
			{ return_result(1); }
		});
	}
	else
	{ return_result(1); }	
};

//------------------------------------------------------ External Email Module End  ------------------------------------------------------//






methods.getAccountDetails = function(arrParams,return_result){

	var queAccountDetails = '';
	if(arrParams.account_type == '1' || arrParams.account_type == '2'){

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type, 
			CONCAT(MAX(CASE WHEN meta_key = 'fname' THEN meta_value END),' ',MAX(CASE WHEN meta_key = 'lname' THEN meta_value END)) AS fullname,
			MAX(CASE WHEN meta_key = 'profile_logo' THEN meta_value END) AS logo 
			FROM jaze_user_basic_details WHERE account_id=?`;

	}else{

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type,
			MAX(CASE WHEN meta_key = 'company_name' THEN meta_value END) AS fullname, 
			MAX(CASE WHEN meta_key = 'company_logo' THEN meta_value END) AS  logo  
			FROM jaze_user_basic_details WHERE account_id =?`;
	}

	DB.query(queAccountDetails,[arrParams.account_id], function(data, error){

	   	if(data != null){

			var newObj = {};
			var logo = '';
			var fullnmae = '';

		  	data.forEach(function(val,key,arr) {

		  		if(val.logo){
			  		if(val.logo != ''){
			  			if(val.account_type == '1'){
			  				logo = G_STATIC_IMG_URL+'uploads/jazenet/individual_account/members_thumb/thumb_'+val.logo;
			  			}else{
			  				logo = G_STATIC_IMG_URL+'uploads/companylogo/'+val.logo;
			  			}
			  		}
		  		}

		  		if(val.fullname){
	  				if(val.fullname !=''){
						fullnmae = val.fullname;
		  			}
		  		}
		  	

				newObj = { 
					account_id: val.account_id.toString(),
					account_type: val.account_type.toString(),
					name: fullnmae,
					logo: logo,
				};
		  	});

	 		return_result(newObj);
	
 	   	}else{
 	   		return_result(null);
 	   	}

	});
};

methods.getEmailForAccDetails = function(arrParams,return_result){


	var arrRows = [];
	var newObj = {};
	var arrAccountParams = {};
	var queLinkAccoType = `SELECT account_id,account_type FROM jaze_user_credential_details WHERE jazemail_id =? `;
	DB.query(queLinkAccoType,[arrParams.jazemail], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){


	   		function asyncFunction (item, cb) {
			  setTimeout(() => {
					return_result(item);
				cb();
			  }, 100);
			}

			let requests = data.map((val, key, array) => {
				arrAccountParams = { account_id:val.account_id, account_type:val.account_type};
				methods.getAccountDetails(arrAccountParams, function(returned){		

			    	return new Promise((resolve) => {
			      		asyncFunction(returned, resolve);
				    });

				});
			})

			Promise.all(requests);
	
 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


methods.getAccountTypes = function(arrParams,return_result){


	var arrRows = [];
	var newObj = {};
	var arrAccountParams = {};
	var queLinkAccoType = `SELECT account_id,account_type FROM jaze_user_credential_details WHERE account_id =? `;
	DB.query(queLinkAccoType,[arrParams.account_id], function(data, error){

	   	if(data != null){

	   		function asyncFunction (item, cb) {
			  setTimeout(() => {
					return_result(item);
				cb();
			  }, 100);
			}

			let requests = data.map((val, key, array) => {
				arrAccountParams = { account_id:val.account_id, account_type:val.account_type};
				methods.getAccountDetails(arrAccountParams, function(returned){		

			    	return new Promise((resolve) => {
			      		asyncFunction(returned, resolve);
				    });

				});
			})

			Promise.all(requests);
	
 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


methods.getAccountDetailsFull = function(arrParams,return_result){

	var queAccountDetails = '';
	if(arrParams.account_type == '1' || arrParams.account_type == '2'){

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type, 
			CONCAT(MAX(CASE WHEN meta_key = 'fname' THEN meta_value END),' ',MAX(CASE WHEN meta_key = 'lname' THEN meta_value END)) AS fullname,
			MAX(CASE WHEN meta_key = 'profile_logo' THEN meta_value END) AS logo,
			MAX(CASE WHEN meta_key = 'email' THEN meta_value END) AS email,
			MAX(CASE WHEN meta_key = 'mobile' THEN meta_value END) AS mobile,
			MAX(CASE WHEN meta_key = 'mobile_country_code_id' THEN meta_value END) AS country_code
			FROM jaze_user_basic_details WHERE account_id=?`;

	}else{

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type,
			MAX(CASE WHEN meta_key = 'company_name' THEN meta_value END) AS fullname, 
			MAX(CASE WHEN meta_key = 'company_logo' THEN meta_value END) AS  logo,
			MAX(CASE WHEN meta_key = 'company_email' THEN meta_value END) AS email,
			MAX(CASE WHEN meta_key = 'company_phone' THEN meta_value END) AS mobile,
			MAX(CASE WHEN meta_key = 'company_phone_code' THEN meta_value END) AS country_code
			FROM jaze_user_basic_details WHERE account_id =?`;
	}

	DB.query(queAccountDetails,[arrParams.account_id], function(data, error){

	   	if(data != null){

			var newObj = {};
			var logo = G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png';
			var newPhoneCode;
			var fullname = '';
		  	data.forEach(function(val,key,arr) {

		  		if(val.logo != ''){
		  			if(val.account_type == '1'){
		  				logo = G_STATIC_IMG_URL+'uploads/jazenet/individual_account/members_thumb/thumb_'+val.logo;
		  			}else{
		  				logo = G_STATIC_IMG_URL+'uploads/companylogo/'+val.logo;
		  			}
		  		}

		  		if(val.fullname!=''){
		  			fullname = val.fullname;
		  		}

  				methods.getMobileCountryCode(val.country_code, function(phone_code){

  					if(phone_code!=null){
  						newPhoneCode = '+'+phone_code+val.mobile;
  					}else{
  						newPhoneCode = val.mobile;
  					}

					newObj = { 
						app_id: arrParams.new_app_id.toString(),
						api_token: arrParams.api_token,
						account_id: val.account_id.toString(),
						account_type: val.account_type.toString(),
						name: fullname,
						logo: logo,
						email: val.email,
						mobile: newPhoneCode
					};

					return_result(newObj);
		  		});

 	
		  	});

 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


methods.getChildAccounts = function(arrParams,return_result){

	let queEmail = `SELECT * FROM jaze_app_device_details WHERE parent_id  =? OR id =? `;
	
	let account_ids = [];

	DB.query(queEmail,[arrParams.app_id,arrParams.app_id], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
	
					account_ids.push(val.account_id);
				   
			  	}
		
		  		const result = await returnData(account_ids);
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
			
		   		return_result(account_ids);
			})


	   	}else{
	   		return_result(null);
	   	}

	});
};

methods.checkEmailAccountForSave = function(arrParams,arrAccounts, return_result){

	let queEmail = `SELECT id FROM jaze_user_credential_details WHERE LOWER(jazemail_id) =? AND STATUS = '1' AND account_id NOT IN (?) `;
		queEmail += `UNION ALL `;
		queEmail += `SELECT id FROM jaze_user_basic_details WHERE (meta_key = 'email' OR meta_key = 'company_email') AND LOWER(meta_value) = ? AND account_id NOT IN (?)`;

	DB.query(queEmail,[arrParams.email_address.toLowerCase(),arrAccounts,arrParams.email_address.toLowerCase(),arrAccounts], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
			return_result(data);
	   	}else{
	   		return_result(null);
	   	}

	});
};


methods.checkEmailAccountInJazemail = function(arrParams, return_result){

	let queEmail = `SELECT t1.* FROM(
			SELECT group_id,
			(CASE WHEN meta_key = 'email_id' THEN meta_value END) AS email_id
				FROM  jaze_email_accounts
			WHERE group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'account_id' AND meta_value =?) 
			AND group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'email_id' AND meta_value =?) 
			) AS t1 WHERE email_id != ""`;


	DB.query(queEmail,[arrParams.account_id,arrParams.email_address], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
			return_result(data[0]);
	   	}else{
	   		return_result(null);
	   	}

	});
};



methods.getAccountIDForEmailList = function(arrParams,return_result){

	var arrRows = [];
	var newObj = {};

	let queGrpId = `SELECT t1.* FROM(
	SELECT group_id,
	(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id
		FROM  jaze_email_accounts
	WHERE group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'email_id' AND meta_value =?) 
	) AS t1 WHERE account_id != ""`;

	DB.query(queGrpId,[arrParams.to_email], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			var arrAccountParams = {account_id:data[0].account_id, to_email:arrParams.to_email};
			methods.getAllEmailAccountsForGetDetails(arrAccountParams, function(returned){		
				return_result(returned);
			});
	   	}else{
	   		return_result(null);
	   	}

	});


};

methods.getAllEmailAccounts = function(arrParams,return_result){

	var arrRows = [];
	var newObj = {};

	let queGrpId = `SELECT t1.* FROM(
	SELECT group_id,
	(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
		(SELECT a.meta_value FROM jaze_email_accounts a WHERE a.meta_key = 'mail_type' AND a.group_id = jaze_email_accounts.group_id) AS mail_type,
		(SELECT a.meta_value FROM jaze_email_accounts a WHERE a.meta_key = 'account_name' AND a.group_id = jaze_email_accounts.group_id) AS account_name,
		(SELECT b.meta_value FROM jaze_email_accounts b WHERE b.meta_key = 'mail_type' AND b.group_id = jaze_email_accounts.group_id) AS mail_account_type,
		(SELECT c.meta_value FROM jaze_email_accounts c WHERE c.meta_key = 'email_id' AND c.group_id = jaze_email_accounts.group_id) AS email_address,
		(SELECT d.meta_value FROM jaze_email_accounts d WHERE d.meta_key = 'is_default' AND d.group_id = jaze_email_accounts.group_id) AS is_default
	FROM  jaze_email_accounts
	WHERE group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'account_id' AND meta_value = ? ) 
	) AS t1 WHERE account_id != ""`;

		//WHERE group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'account_id' AND meta_value = ? and  group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'mail_account_type' AND meta_value = '5' )) 

	DB.query(queGrpId,[arrParams.account_id], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			var newObj = {};
			var arrRows = [];

			var category = '';
			var counter = data.length;
			var jsonItems = Object.values(data);
			var status;



			promiseForeach.each(jsonItems,[function (jsonItems) {

				return new Promise(function (resolve, reject) {   

					var accParams = {account_id:jsonItems.account_id};
					HelperGeneralJazenet.getAccountDetails(accParams, function(returnData){   

						if(arrParams.mail_id === "0")
						{
							status = jsonItems.is_default.toString();
						}
						else if(jsonItems.group_id == arrParams.mail_id){
							status = '1';
						}
						else {
							status = '0';
						}

						newObj = {
							account_id:returnData.account_id,
							account_type:returnData.account_type,
							name:returnData.name,
							logo:returnData.logo,
							account_name:jsonItems.account_name,
							account_logo:'',
							mail_id:jsonItems.group_id.toString(),
							email_account: jsonItems.email_address,
							active_status: status,
							unread_count:'0',
							is_master:jsonItems.is_default.toString()

						};
						
						arrRows.push(newObj);
						counter -= 1;
						if (counter === 0){ 
							resolve(arrRows);
						}
						
					});
				})

			}],
			function (result, current) {        
				if (counter === 0){ 
					return_result(result[0]); 
				}
			}); 

	   	}else{
	   		return_result(null);
	   	}

	});


};

methods.getAllEmailAccountsForGetDetails = function(arrParams,return_result){

	var arrRows = [];
	var newObj = {};


	let queGrpId = `SELECT t1.* FROM(
	SELECT group_id,
	(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
		(SELECT a.meta_value FROM jaze_email_accounts a WHERE a.meta_key = 'account_name' AND a.group_id = jaze_email_accounts.group_id) AS account_name,
		(SELECT a.meta_value FROM jaze_email_accounts a WHERE a.meta_key = 'mail_type' AND a.group_id = jaze_email_accounts.group_id) AS mail_type,
		(SELECT b.meta_value FROM jaze_email_accounts b WHERE b.meta_key = 'mail_type' AND b.group_id = jaze_email_accounts.group_id) AS mail_account_type,
		(SELECT c.meta_value FROM jaze_email_accounts c WHERE c.meta_key = 'email_id' AND c.group_id = jaze_email_accounts.group_id) AS email_address,
		(SELECT d.meta_value FROM jaze_email_accounts d WHERE d.meta_key = 'is_default' AND d.group_id = jaze_email_accounts.group_id) AS is_default
	FROM  jaze_email_accounts
	WHERE group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'account_id' AND meta_value = ? OR  meta_key = 'link_account_id' AND meta_value = ? ) 
	) AS t1 WHERE account_id != ""`;

	DB.query(queGrpId,[arrParams.account_id,arrParams.account_id ], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			var newObj = {};
			var arrRows = [];

			var category = '';
			var counter = data.length;
			var jsonItems = Object.values(data);
			var status;


			promiseForeach.each(jsonItems,[function (jsonItems) {

				return new Promise(function (resolve, reject) {   

					if(jsonItems.email_address == arrParams.to_email){
						status = '1';
					}else{
						status = '0';
					}

					newObj = {
						account_name:jsonItems.account_name,
						account_logo:'',
						mail_id:jsonItems.group_id.toString(),
						email_account: jsonItems.email_address,
						active_status: status,
						unread_count:'0',
						is_master:jsonItems.is_default.toString()


					};
					
					arrRows.push(newObj);
					counter -= 1;
					resolve(arrRows);
				})

			}],
			function (result, current) {        
				if (counter === 0){ 
					return_result(result[0]); 
				}
			}); 

	   	}else{
	   		return_result(null);
	   	}

	});


};



methods.getEmailAccountDetails = function(arrParams,return_result){

	var arrRows = [];
	var newObj = {};

	var arrQuerParams;

	var query = `group_id =? ) AS t1 WHERE account_id != ""`;

	if(arrParams.mail_id == '0'){

		query = `group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'account_id' AND meta_value = ? OR  meta_key = 'link_account_id' AND meta_value = ? ) 
		AND group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'is_default' AND meta_value = '1' ) 
		) AS t1 WHERE account_id != ""`;

		arrQuerParams = [arrParams.account_id,arrParams.account_id ];

	}else{

		query = `group_id =? ) AS t1 WHERE account_id != ""`;
		arrQuerParams = [arrParams.mail_id];
	}

	
	let queGrpId = `SELECT t1.* FROM(
		SELECT group_id,
		(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
			(SELECT a.meta_value FROM jaze_email_accounts a WHERE a.meta_key = 'mail_type' AND a.group_id = jaze_email_accounts.group_id) AS mail_type,
			(SELECT b.meta_value FROM jaze_email_accounts b WHERE b.meta_key = 'mail_type' AND b.group_id = jaze_email_accounts.group_id) AS mail_account_type,
			(SELECT c.meta_value FROM jaze_email_accounts c WHERE c.meta_key = 'email_id' AND c.group_id = jaze_email_accounts.group_id) AS email_address,
			(SELECT d.meta_value FROM jaze_email_accounts d WHERE d.meta_key = 'email_password' AND d.group_id = jaze_email_accounts.group_id) AS email_password,
			(SELECT e.meta_value FROM jaze_email_accounts e WHERE e.meta_key = 'imap_host' AND e.group_id = jaze_email_accounts.group_id) AS imap_host,
			(SELECT e.meta_value FROM jaze_email_accounts e WHERE e.meta_key = 'imap_port' AND e.group_id = jaze_email_accounts.group_id) AS imap_port,
			(SELECT f.meta_value FROM jaze_email_accounts f WHERE f.meta_key = 'smtp_name' AND f.group_id = jaze_email_accounts.group_id) AS smtp_host,
			(SELECT f.meta_value FROM jaze_email_accounts f WHERE f.meta_key = 'smtp_port' AND f.group_id = jaze_email_accounts.group_id) AS smtp_port,
			(SELECT g.meta_value FROM jaze_email_accounts g WHERE g.meta_key = 'is_ssl' AND g.group_id = jaze_email_accounts.group_id) AS is_ssl
		FROM  jaze_email_accounts WHERE `+query;


	
	DB.query(queGrpId,arrQuerParams, function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			newObj = {
				mail_type:data[0].mail_type,
				mail_account_type:data[0].mail_account_type,
				email_account: data[0].email_address,
				email_password:data[0].email_password,
				imap_host:data[0].imap_host,
				imap_port:data[0].imap_port,
				smtp_host:data[0].smtp_host,
				smtp_port:data[0].smtp_port,
				is_ssl:data[0].is_ssl,
			}

			return_result(newObj);

	   	}else{
	   		return_result(null);
	   	}

	});


};


// methods.getEmailAccountDetails = function(arrParams,return_result){

// 	try
// 	{
// 		var query = " SELECT * FROM jaze_email_accounts WHERE  `group_id` IN (SELECT `group_id`  FROM jaze_email_accounts WHERE `meta_key` = 'account_id' AND meta_value = '"+arrParams.account_id+"'  ";
			
// 		if(parseInt(arrParams.mail_id) !== 0)
// 		{
// 			query += " and group_id = '"+arrParams.mail_id+"' ";
// 		}

// 		query += " ) ";

// 		DB.query(query,null, function(data, error){

// 			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

// 				HelperRestriction.convertMetaArray(data, function(returnResult){	
				
// 					if(returnResult !== null)
// 					{
// 						var newObj = {
// 								mail_id		 	 : returnResult.group_id,
// 								mail_type		 : returnResult.mail_type,
// 								mail_account_type: returnResult.mail_type, 
// 								email_account	 : returnResult.email_id,
// 								email_password	 : returnResult.email_password,
// 								imap_host		 : returnResult.imap_host,
// 								imap_port		 : returnResult.imap_port,
// 								smtp_host		 : returnResult.smtp_name,
// 								smtp_port		 : returnResult.smtp_port,
// 								is_ssl			 : returnResult.is_ssl,
// 							}
						
// 						return_result(newObj);
// 					}
// 					else 
// 					{ return_result(null); }
// 				});

// 			}else{
// 				return_result(null);
// 			}

// 		});
// 	}
// 	catch(err)
// 	{ console.log("getEmailAccountDetails",err); return_result(null); }	
// };


methods.getEmailAccountPerParamEmail = function(arrParams,return_result){

	var arrRows = [];
	var newObj = {};


	let queGrpId = `SELECT t1.* FROM(
			SELECT group_id,
			(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
			(SELECT a.meta_value FROM jaze_email_accounts a WHERE a.meta_key = 'mail_type' AND a.group_id = jaze_email_accounts.group_id) AS mail_type,
			(SELECT b.meta_value FROM jaze_email_accounts b WHERE b.meta_key = 'mail_type' AND b.group_id = jaze_email_accounts.group_id) AS mail_account_type,
			(SELECT c.meta_value FROM jaze_email_accounts c WHERE c.meta_key = 'email_id' AND c.group_id = jaze_email_accounts.group_id) AS email_address,
			(SELECT d.meta_value FROM jaze_email_accounts d WHERE d.meta_key = 'email_password' AND d.group_id = jaze_email_accounts.group_id) AS email_password,
			(SELECT e.meta_value FROM jaze_email_accounts e WHERE e.meta_key = 'imap_host' AND e.group_id = jaze_email_accounts.group_id) AS imap_host,
			(SELECT e.meta_value FROM jaze_email_accounts e WHERE e.meta_key = 'imap_port' AND e.group_id = jaze_email_accounts.group_id) AS imap_port,
			(SELECT f.meta_value FROM jaze_email_accounts f WHERE f.meta_key = 'smtp_name' AND f.group_id = jaze_email_accounts.group_id) AS smtp_host,
			(SELECT f.meta_value FROM jaze_email_accounts f WHERE f.meta_key = 'smtp_port' AND f.group_id = jaze_email_accounts.group_id) AS smtp_port,
			(SELECT g.meta_value FROM jaze_email_accounts g WHERE g.meta_key = 'is_ssl' AND g.group_id = jaze_email_accounts.group_id) AS is_ssl
			FROM  jaze_email_accounts WHERE group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'email_id' AND meta_value =?) 
	) AS t1 WHERE account_id != ""`;


	DB.query(queGrpId,arrParams.to_email, function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			newObj = {
				mail_type:data[0].mail_type,
				mail_account_type:data[0].mail_account_type,
				email_account: data[0].email_address,
				email_password:data[0].email_password,
				imap_host:data[0].imap_host,
				imap_port:data[0].imap_port,
				smtp_host:data[0].smtp_host,
				smtp_port:data[0].smtp_port,
				is_ssl:data[0].is_ssl,
			}

			return_result(newObj);

	   	}else{
	   		return_result(null);
	   	}

	});


};


methods.checkEmailAccount = function(arrParams,return_result){

	var imap = {};
	var smtp = {};
	var account_info = {};

	var imap;
	var imap_port;

	var smtp;
	var smtp_port;

	if(arrParams.account_type=='1')
	{
	
		imap = 'imap.gmail.com';
		imap_port = '993';

		smtp = 'smtp.gmail.com';
		smtp_port = '465';
	}
	else if(arrParams.account_type=='2')
	{
		imap = 'imap.mail.yahoo.com';
		imap_port = '993';

		smtp = 'smtp.bizmail.yahoo.com';
		smtp_port = '465';
	}
	else if(arrParams.account_type=='3')
	{
		imap = 'imap.yandex.com';
		imap_port = '993';

		smtp = 'smtp.yandex.com';
		smtp_port = '465';
	}
	else if(arrParams.account_type=='4')
	{
		imap = 'imap.mail.com';
		imap_port = '993';

		smtp = 'smtp.mail.com';
		smtp_port = '587';
	}

	account_info = {
		imap: {
			host:imap,
			port:imap_port
		},
		smtp:{
			host:smtp,
			port:smtp_port,
		}
	};

	return_result(account_info);
};


methods.getMailFolder = function(arrParams,return_result){

	var folder = '';

	if(arrParams.folder=='inbox')
	{
		if(arrParams.mail_type == '1'){
			folder = 'INBOX';
		}else{
			folder = 'Inbox';
		}
	}
	else if(arrParams.folder=='drafts')
	{
		if(arrParams.mail_type == '2'){
			folder = 'INBOX.Drafts';
		}else if(arrParams.mail_type == '1'){
			folder = '[Gmail]/Drafts';
		}else{
			folder = 'Drafts';
		}
	}
	else if(arrParams.folder=='trash')
	{
		if(arrParams.mail_type == '2'){
			folder = 'INBOX.trash';
		}else if(arrParams.mail_type == '1'){
			folder = '[Gmail]/Trash';
		}else{
			folder = 'Trash';
		}
	}
	else if(arrParams.folder=='spam')
	{
		if(arrParams.mail_type == '2'){
			folder = 'INBOX.Junk';
		}else if(arrParams.mail_type == '1'){
			folder = '[Gmail]/Spam';
		}else{
			folder = 'Spam';
		}
	}
	else if(arrParams.folder=='sent')
	{
		if(arrParams.mail_type == '2'){
			folder = 'INBOX.Sent';
		}else if(arrParams.mail_type == '1'){
			folder = '[Gmail]/Sent Mail';
		}else{
			folder = 'Sent';
		}
	}

	return_result(folder);
};



methods.checkDevice = function(arrParams,return_result){

	var quer = `SELECT * FROM jaze_app_device_details WHERE device_id =? AND device_type =? AND fcm_token=? AND account_id =? AND status = '1'`;
	DB.query(quer,[arrParams.device_id,arrParams.device_type,arrParams.device_fcm_token,arrParams.account_id], function(data, error){
	   	if(data != null)
	   	{
			return_result(data);
	   	}else{
	   		return_result(null);
	   	}

	});
};

methods.checkDeviceForDetails = function(arrParams,return_result){

	var quer = `SELECT * FROM jaze_app_device_details WHERE id=? AND device_id =? AND device_type =? AND account_id =? AND api_token=? AND status = '1'`;
	DB.query(quer,[arrParams.app_id,arrParams.device_id,arrParams.device_type,arrParams.account_id,arrParams.api_token], function(data, error){
	   	if(data != null)
	   	{
			return_result(data);
	   	}else{
	   		return_result(null);
	   	}

	});
};


methods.updateDevice = function(arrUpdate,return_result){

	methods.generateRandomString(function(retToken){	

		var querY = `UPDATE jaze_app_device_details SET fcm_token=?,api_token=?,last_update_date=?,status='1' WHERE account_id=? and device_id=?`;
		DB.query(querY, [arrUpdate.device_fcm_token,retToken,standardDT,arrUpdate.account_id,arrUpdate.device_id], function(data, error){
			if(data == null){
	 	   		return_result(0);
	 	   	}else{
	  			return_result(retToken);
	 	   	}
		});
	});

}

methods.insertEmail = function(arrInsert,return_result){

	var querY = 'INSERT INTO mail_user_account (user_id,account_type,account_name,imap_host_name,imap_port_no,smtp_host_name,smtp_port_no,`ssl`,user_name,password,sort_order,create_date,status) VALUES ?';

	var values = [[
		arrInsert.account_id,
		arrInsert.account_type,
		arrInsert.account_name,
		arrInsert.imap_host_name,
		arrInsert.imap_port_no,
		arrInsert.smtp_host_name,
		arrInsert.smtp_port_no,
		arrInsert.ssl,
		arrInsert.username,
		arrInsert.password,
		arrInsert.sort_order,
		arrInsert.create_date,
		arrInsert.status
	]];

	DB.query(querY,[values], function(data, error){
	 	if(data == null){
 	   		return_result(0);
 	   	}else{
			return_result(1);
 	   	}
 	});

}


methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
}

methods.insertResult = function(arrInsert,querY,grpId,return_result){

	var insertedGrpid;
	insertedGrpid=grpId;

	return_result(1);
	
 	for (var key in arrInsert) 
 	{
		DB.query(querY,[grpId,key,arrInsert[key]], function(data, error){

 		 	if(data == null){
	 	   		return_result(0);
	 	   	}
 	 	});

  	};
}


methods.updateQuery = function(querY,arrUpdate,group_id,return_result){

	for (var key in arrUpdate) 
 	{	
 		DB.query(querY, [arrUpdate[key],key,group_id], function(data, error){
 			if(data == null){
	 	   		return_result(0);
	 	   	}else{
	  			return_result(data.affectedRows);
	 	   	}
		});
  	};
}


methods.getFileTypes = function(ext,return_result){

	var type;

	var arrImages = ['.jpeg','.jpg','.gif','.png','.tif'];
	var arrVideos = ['.mp4','.avi','.mov','.flv','.wmv'];
	var arrAudio = ['.pcm','.wav','.aiff','.mp3','.aac','.wma','.flac','.alac'];
	var arrDocs = ['.odt','.xls','.xlsx','.ods','.ppt','.pptx','.txt','.doc'];
	var arrPdf = ['.pdf'];

	if(arrImages.includes(ext)){
		type = '2';
	}else if(arrAudio.includes(ext)){
		type = '3';
	}else if(arrVideos.includes(ext)){
		type = '4';
	}else if(arrDocs.includes(ext)){
		type = '5';
	}else if(arrPdf.includes(ext)){
		type = '6';
	}else{
		type = '7';
	}

	return_result(type);
};


methods.getMultipleUploads = function(arrFiles,account_id, return_result){

	var	file_type;
	var	extention;

	var newObj = {};
	var arrRows = [];



	if(typeof arrFiles !== 'undefined' && arrFiles !== null && parseInt(arrFiles.length) > 0){

		var array_rows = [];

		arrFiles.forEach(function (arrayItem) {

			methods.getExtension(arrayItem.filename, function(returnExt){	

				file_type = arrayItem.mimetype;
				if(returnExt !=null){
					extention = returnExt;
				}else{
					extention = arrayItem.filename;
				}
				

			  	var trimed = extention.substring(extention.indexOf(".") + 1);

	   	 		methods.getFileTypes('.'+trimed, function(retYpe){	

					newObj = {
				        file_name:arrayItem.filename,
				        file_format:file_type,
				        file_type:retYpe,
	                   	extention:trimed,
	                	file_path:G_STATIC_IMG_URL+'uploads/jazenet/jazemail/'+account_id+'/'+arrayItem.filename
				    };

				    arrRows.push(newObj);
				  
				});
			});

		});


		return_result(arrRows);


   	}else{
   		return_result(null);
   	}


};


// methods.getEmailsFromCredentialsForSearch = function(arrParams,return_result){

// 	var arrRows = [];
// 	var newObj = {};

// 	let qRy = "SELECT * FROM jaze_user_credential_details WHERE jazemail_id LIKE '%" + arrParams.to_email + "%' LIMIT 20" ;

// 	DB.query(qRy,null, function(data, error){

// 		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

// 			var newObj = {};
// 			var arrRows = [];

// 			var category = '';
// 			var counter = data.length;
// 			var jsonItems = Object.values(data);
// 			var status;


// 			promiseForeach.each(jsonItems,[function (jsonItems) {
// 				var acct_types = {account_id:jsonItems.account_id};
		
// 				return new Promise(function (resolve, reject) { 
				
// 					methods.getAccountTypes(acct_types, function(returned){ 
// 						newObj = {
// 							name:returned.name,
// 							logo:returned.logo,
// 							email_address:jsonItems.jazemail_id,
// 						};
						
// 						arrRows.push(newObj);
// 						counter -= 1;
// 						resolve(arrRows);

// 					});
// 				})
				
// 			}],
// 			function (result, current) {        
// 				if (counter === 0){ 
// 					return_result(result[0]); 
// 				}
// 			}); 

// 	   	}else{
// 	   		return_result(null);
// 	   	}

// 	});


// };


// methods.getEmailsFromCredentials = function(arrParams,return_result){

// 	var arrRows = [];
// 	var newObj = {};

// 	let qRy = "SELECT * FROM jaze_user_credential_details WHERE jazemail_id LIKE '%" + arrParams.to_email + "%' LIMIT 20";

// 	DB.query(qRy,null, function(data, error){

// 		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

// 			var newObj = {};
// 			var arrRows = [];

// 			var category = '';
// 			var counter = data.length;
// 			var jsonItems = Object.values(data);
// 			var status;


// 			promiseForeach.each(jsonItems,[function (jsonItems) {
				
		
// 				return new Promise(function (resolve, reject) { 

// 					arrRows.push(jsonItems.account_id.toString());
// 					counter -= 1;
// 					resolve(arrRows);
// 				})
				
// 			}],
// 			function (result, current) {        
// 				if (counter === 0){ 
// 					return_result(result[0]); 
// 				}
// 			}); 

// 	   	}else{
// 	   		return_result(null);
// 	   	}

// 	});


// };



methods.checkIfinCredentials = function(arrParams,return_result){

	var arrRows = [];
	var newObj = {};

	var qRy = `SELECT account_id,jazemail_id FROM jaze_user_credential_details WHERE jazemail_id IN(`+arrParams +`)`;

	DB.query(qRy,null, function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			var newObj = {};
			var arrRows = [];

			var category = '';
			var counter = data.length;
			var jsonItems = Object.values(data);
			var status;

			promiseForeach.each(jsonItems,[function (jsonItems) {
				var acct_types = {account_id:jsonItems.account_id};
		
				return new Promise(function (resolve, reject) { 
					
					newObj = {
						account_id:jsonItems.account_id,
						email_address:jsonItems.jazemail_id
					}
					arrRows.push(newObj);
					counter -= 1;
					resolve(arrRows);

				})
				
			}],
			function (result, current) {        
				if (counter === 0){ 
					return_result(result[0]); 
				}
			}); 

	   	}else{
	   		return_result(null);
	   	}

	});
};


// methods.getReceiverList = function(arrParams,return_result){

// 	var arrRows = [];
// 	var newObj = {};

// 	DB.query(`SELECT * FROM jaze_email_accounts_ids WHERE account_id = ?`,[arrParams.account_id], function(data, error){

// 		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

// 			var emails = data[0].email_id.split(",");
// 			arrRows.push(emails);
// 			arrRows.push(data[0].group_id);
	
// 			return_result(arrRows);

// 	   	}else{
// 	   		return_result(null);
// 	   	}

// 	});

// };

methods.insertReceivers = function(arrInsert,return_result){

  	var sql = "INSERT INTO jaze_email_accounts_ids (account_id, group_id, email_id) VALUES ?";
	var values = [[arrInsert.account_id,arrInsert.group_id,arrInsert.email_id]];
	return_result(1);
	DB.query(sql,[values], function(data, error){
	 	if(data == null){
 	   		return_result(0);
 	   	}
 	});
}


methods.updateReceivers = function(arrUpdate,return_result){

	var querY = `UPDATE jaze_email_accounts_ids SET email_id=? WHERE account_id=? and group_id=?`;
	return_result(1);
	DB.query(querY, [arrUpdate.email_id,arrUpdate.account_id,arrUpdate.group_id], function(data, error){
		if(data == null){
 	   		return_result(0);
 	   	}
	});
}


methods.deleteMailAccount = function(arrParams,return_result){

	var querY = `DELETE FROM jaze_email_accounts WHERE group_id=?`;

		DB.query(querY,[arrParams.mail_id], function(data, error){
 		 	if(data == null){
	 	   		return_result(0);
	 	   	}else{
   				return_result(data.affectedRows);
	 	   	}
 	 	});
}



methods.getExtension = function(path, return_result){

    var basename = path.split(/[\\/]/).pop(),
    pos = basename.lastIndexOf(".");     

    if (basename === "" || pos < 1){
		return_result(null);
    }else{
		return_result(basename.slice(pos + 1));
    }            
                         
};


methods.checkContacts = function(arrParams,return_result){

	var quer = `SELECT * FROM jaze_jazecom_contact_list 
		WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value =? ) AND 
		group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'status' AND meta_value ='1' )`;

	DB.query(quer,[arrParams.account_id], function(data, error){
	   	if(data != null)
	   	{
			return_result(data);
	   	}else{
	   		return_result(null);
	   	}

	});
};


methods.checkContactsMail = function(arrParams,return_result){

	var quer = `SELECT t1.* FROM(
	SELECT group_id,
		(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
		(SELECT a.meta_value FROM jaze_jazecom_contact_list a WHERE a.meta_key = 'mail_account_id' AND a.group_id = jaze_jazecom_contact_list.group_id) AS mail_account_id
	FROM  jaze_jazecom_contact_list WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value =?) 
	) AS t1 WHERE account_id != ""`;

	DB.query(quer,[arrParams.account_id], function(data, error){
	   	if(data != null)
	   	{
			return_result(data[0]);
	   	}else{
	   		return_result(null);
	   	}

	});
};


// methods.searchIfBlockedAccount = function(arrParams,return_result){

// 	var arrRows = [];
// 	var newObj = {};

// 	let qRy = "SELECT * FROM jaze_user_credential_details WHERE jazemail_id LIKE '%" + arrParams + "%' LIMIT 20";

// 	DB.query(qRy,null, function(data, error){

// 		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

// 			var newObj = {};
// 			var arrRows = [];

// 			var category = '';
// 			var counter = data.length;
// 			var jsonItems = Object.values(data);
// 			var status;


// 			promiseForeach.each(jsonItems,[function (jsonItems) {
				
		
// 				return new Promise(function (resolve, reject) { 

// 					arrRows.push(jsonItems.account_id.toString());
// 					counter -= 1;
// 					resolve(arrRows);
// 				})
				
// 			}],
// 			function (result, current) {        
// 				if (counter === 0){ 
// 					return_result(result[0]); 
// 				}
// 			}); 

// 	   	}else{
// 	   		return_result(null);
// 	   	}

// 	});


// };

// methods.checkSearchedAccountIdForBlock = function(arrParams,return_result){

// 	var arrRows = [];
// 	var newObj = {};

// 	let qRy = `SELECT t1.* FROM(
// 			SELECT group_id,
// 				(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
// 				(SELECT e.meta_value FROM jaze_jazecom_contact_list e WHERE e.meta_key = 'mail_blocked_account_id' AND e.group_id = jaze_jazecom_contact_list.group_id) AS mail_blocked_account_id
// 			FROM  jaze_jazecom_contact_list WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value =?) 
// 			) AS t1 WHERE account_id != ""`;

// 	DB.query(qRy,[arrParams.searched_id], function(data, error){

// 		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

// 			var newObj = {};
// 			var arrRows = [];

// 			var category = '';
// 			var counter = data.length;
// 			var jsonItems = Object.values(data);
// 			var status;


// 			promiseForeach.each(jsonItems,[function (jsonItems) {
				
// 				return new Promise(function (resolve, reject) { 

// 					arrRows.push(jsonItems.account_id.toString());
// 					counter -= 1;
// 					resolve(arrRows);
// 				})
				
// 			}],
// 			function (result, current) {        
// 				if (counter === 0){ 
// 					return_result(result[0]); 
// 				}
// 			}); 

// 	   	}else{
// 	   		return_result(null);
// 	   	}

// 	});



// };


// methods.checkiFBlock = function(arrParams,return_result){

// 	var arrMail = [];

// 	var quer = `SELECT t1.* FROM(
// 			SELECT group_id,
// 					(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
// 					(SELECT a.meta_value FROM jaze_jazecom_contact_list a WHERE a.meta_key = 'mail_account_id' AND a.group_id = jaze_jazecom_contact_list.group_id) AS mail_account_id,
// 					(SELECT e.meta_value FROM jaze_jazecom_contact_list e WHERE e.meta_key = 'mail_blocked_account_id' AND e.group_id = jaze_jazecom_contact_list.group_id) AS mail_blocked_account_id
// 			FROM  jaze_jazecom_contact_list WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value =?) 
// 			) AS t1 WHERE account_id != ""`;

// 	DB.query(quer,[arrParams.account_id], function(data, error){
// 	   	if(data != null)
// 	   	{
// 			data[0].mail_blocked_account_id.split(",").forEach(function(val) {
// 				arrMail.push(val);
// 	  		});

// 			methods.getEmailCredential(arrMail, function(retCred){ 
// 				return_result(retCred);
// 			});


// 	   	}else{
// 	   		return_result(null);
// 	   	}

// 	});
// };


// methods.checkiFBlock2 = function(arrParams,return_result){

// 	var arrMail = [];

// 	var quer = `SELECT t1.* FROM(
// 			SELECT group_id,
// 					(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
// 					(SELECT a.meta_value FROM jaze_jazecom_contact_list a WHERE a.meta_key = 'mail_account_id' AND a.group_id = jaze_jazecom_contact_list.group_id) AS mail_account_id,
// 					(SELECT e.meta_value FROM jaze_jazecom_contact_list e WHERE e.meta_key = 'mail_blocked_account_id' AND e.group_id = jaze_jazecom_contact_list.group_id) AS mail_blocked_account_id
// 			FROM  jaze_jazecom_contact_list WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value =?) 
// 			) AS t1 WHERE account_id != ""`;

// 	DB.query(quer,[arrParams.account_id], function(data, error){
// 	   	if(data != null)
// 	   	{
// 			data[0].mail_blocked_account_id.split(",").forEach(function(val) {
// 				arrMail.push(val);
// 	  		});


// 			return_result(arrMail);
	

// 	   	}else{
// 	   		return_result(null);
// 	   	}

// 	});
// };



methods.getEmailCredential = function(arrParams,return_result){

	var arrRows = [];
	var newObj = {};
	var quer = `SELECT * FROM jaze_user_credential_details WHERE account_id in (?) AND status = '1'`;

	DB.query(quer,[arrParams], function(data, error){

	if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
                            
			var array_rows = [];
			var counter = data.length;
			var jsonItems = Object.values(data);

			promiseForeach.each(jsonItems,[function (jsonItems) {

				return new Promise(function (resolve, reject) {                                            
		
					array_rows.push(jsonItems.jazemail_id);

					counter -= 1;

					resolve(array_rows);
				})
			}],
			function (result, current) {        
				if (counter === 0){ 
					return_result(result[0]); 
				}
			});                
        }
        else
        { 
        	return_result(null);
        }

	});
};


// methods.getEmailCredential2 = function(arrParams,return_result){


// 	var arrRows = [];
// 	var newObj = {};

// 	var quer = `SELECT * FROM jaze_user_credential_details WHERE account_id in (?) AND status = '1'`;

// 	DB.query(quer,[arrParams], function(data, error){

// 	if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
                            
// 			var array_rows = [];
// 			var counter = data.length;
// 			var jsonItems = Object.values(data);

// 			promiseForeach.each(jsonItems,[function (jsonItems) {
// 				var acct_types = {account_id:jsonItems.account_id};

// 				return new Promise(function (resolve, reject) {  

// 					methods.getAccountTypes(acct_types, function(returned){ 

// 						newObj = {
// 							name:returned.name,
// 							logo:returned.logo,
// 							email_address:jsonItems.jazemail_id,
// 						};
						
// 						array_rows.push(newObj);

// 						counter -= 1;
// 						resolve(array_rows);

// 					});                                          
			
// 				})
// 			}],
// 			function (result, current) {        
// 				if (counter === 0){ 
// 					return_result(result[0]); 
// 				}
// 			});                
//         }
//         else
//         { 
//         	return_result(null);
//         }

// 	});
// };

methods.getAccountIDperEmail = function(arrParams,return_result){

	var arrRows = [];
	var newObj = {};

	var qRy = `SELECT * FROM jaze_user_credential_details WHERE jazemail_id IN(`+arrParams +`) AND status = '1'`;

	DB.query(qRy,null, function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			data.forEach(function(row) {
				arrRows.push(row.account_id);
		  	});

		  	return_result(arrRows);
        }
        else
        { 
        	return_result(null);
        }

	});
};



//for blockchecking
methods.getReceiverList = function(arrParams,return_result){

	var arrRows = [];
	var newObj = {};

	DB.query(`SELECT * FROM jaze_email_accounts_ids WHERE account_id = ?`,[arrParams.account_id], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			var arrRows = [];

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
	  				var emails = data[0].email_id.split(",");
			  		arrRows.push(emails);
		  			arrRows.push(data[0].group_id);
				   	const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
		   		return_result(arrRows);
			})

	   	}else{
	   		return_result(null);
	   	}

	});

};


methods.searchIfBlockedAccount = function(arrParams,return_result){

	var arrRows = [];
	var newObj = {};

	let qRy = "SELECT * FROM jaze_user_credential_details WHERE jazemail_id LIKE '%" + arrParams + "%' LIMIT 20";

	DB.query(qRy,null, function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			var arrRows = [];

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
			  		arrRows.push(val.account_id.toString());
				   	const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
		   		return_result(arrRows);
			})

	   	}else{
	   		return_result(null);
	   	}

	});

};


methods.checkSearchedAccountIdForBlock = function(arrParams,return_result){

	var arrRows = [];
	var newObj = {};

	let qRy = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
				(SELECT e.meta_value FROM jaze_jazecom_contact_list e WHERE e.meta_key = 'mail_blocked_account_id' AND e.group_id = jaze_jazecom_contact_list.group_id) AS mail_blocked_account_id
			FROM  jaze_jazecom_contact_list WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value =?) 
			) AS t1 WHERE account_id != ""`;

	DB.query(qRy,[arrParams.searched_id], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			var arrRows = [];
			var blockedIDS = null;
			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){

			  		if(val.mail_blocked_account_id!=''){
			  			blockedIDS = val.mail_blocked_account_id.split(",");
			  		}
				   	const result = await returnData(blockedIDS);
		   			return_result(result);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {});

	   	}else{
	   		return_result(null);
	   	}

	});
};


methods.getEmailsFromCredentials = function(arrParams,return_result){

	let qRy = "SELECT * FROM jaze_user_credential_details WHERE jazemail_id LIKE '%" + arrParams.to_email + "%' LIMIT 20";

	DB.query(qRy,null, function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			var arrRows = [];

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
			  		arrRows.push(val.account_id.toString());
				   	const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
		   		return_result(arrRows);
			})

	   	}else{
	   		return_result(null);
	   	}

	});
};



methods.getEmailCredentialAll = function(arrParams,return_result){


	var arrRows = [];
	var newObj = {};

	var quer = `SELECT * FROM jaze_user_credential_details WHERE account_id in (?) AND status = '1'`;

	DB.query(quer,[arrParams], function(data, error){

	if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
                            
			var array_rows = [];
			var counter = data.length;
			var jsonItems = Object.values(data);

			promiseForeach.each(jsonItems,[function (jsonItems) {
				var acct_types = {account_id:jsonItems.account_id};

				return new Promise(function (resolve, reject) {  

					methods.getAccountTypes(acct_types, function(returned){ 

						newObj = {
							name:returned.name,
							logo:returned.logo,
							email_address:jsonItems.jazemail_id,
						};
						
						array_rows.push(newObj);

						counter -= 1;
						resolve(array_rows);

					});                                          
			
				})
			}],
			function (result, current) {        
				if (counter === 0){ 
					return_result(result[0]); 
				}
			});                
        }
        else
        { 
        	return_result(null);
        }

	});
};

methods.getEmailsFromCredentialsForSearch = function(arrParams,return_result){

	var arrRows = [];
	var newObj = {};

	let qRy = "SELECT * FROM jaze_user_credential_details WHERE jazemail_id LIKE '%" + arrParams.to_email + "%' LIMIT 20" ;

	DB.query(qRy,null, function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			var newObj = {};
			var arrRows = [];

			var category = '';
			var counter = data.length;
			var jsonItems = Object.values(data);
			var status;


			promiseForeach.each(jsonItems,[function (jsonItems) {
				var acct_types = {account_id:jsonItems.account_id};
		
				return new Promise(function (resolve, reject) { 
				
					methods.getAccountTypes(acct_types, function(returned){ 
						newObj = {
							name:returned.name,
							logo:returned.logo,
							email_address:jsonItems.jazemail_id,
						};
						
						arrRows.push(newObj);
						counter -= 1;
						resolve(arrRows);

					});
				})
				
			}],
			function (result, current) {        
				if (counter === 0){ 
					return_result(result[0]); 
				}
			}); 

	   	}else{
	   		return_result(null);
	   	}

	});


};


methods.getNotificationDetails = function(arrParams,return_result){

		var quer = `SELECT t1.* FROM(
					SELECT group_id,
					(CASE WHEN meta_key = 'receiver_account_id' THEN meta_value END) AS receiver_account_id,
						(SELECT a.meta_value FROM jaze_notification_list a WHERE a.meta_key = 'notify_type' AND a.group_id = jaze_notification_list.group_id) AS notify_type,
						(SELECT a.meta_value FROM jaze_notification_list a WHERE a.meta_key = 'notify_id' AND a.group_id = jaze_notification_list.group_id) AS notify_id,
						(SELECT a.meta_value FROM jaze_notification_list a WHERE a.meta_key = 'read_status' AND a.group_id = jaze_notification_list.group_id) AS read_status
					FROM  jaze_notification_list
					WHERE group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'receiver_account_id' AND meta_value = ? ) 
					AND group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'notify_type' AND meta_value = '112' ) 
					) AS t1 WHERE receiver_account_id != ""`;

	DB.query(quer,[arrParams.receiver_account_id], function(data, error){


		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
	   	{

			var mail_id = data[0].notify_id.split('|'); 

			if(mail_id[1] === arrParams.mail_id){
				if(data[0].read_status=='0'){
					methods.updateNotiDetails(data[0].group_id, function(returned){
						console.log(returned);
					});
				}
			}

	   	}else{
	   		return_result(null);
	   	}

	});
};




methods.updateNotiDetails = function(group_id,return_result){

	var updateQue = `update jaze_notification_list SET meta_value ='1' WHERE meta_key ='read_status' AND group_id =?`;

	return_result(1);
	DB.query(updateQue,[group_id], function(data, error){

		if(data == null){
 	   		return_result(0);
 	   	}

	});
};




//search my contacts

methods.getAppID = function(arrParams,return_result){

	var queLinkAccoType = `SELECT account_id,account_type,parent_id FROM jaze_app_device_details WHERE id =?`;
	DB.query(queLinkAccoType,[arrParams.app_id,arrParams.api_token], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			return_result(data[0]);
	
 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


methods.searchMyContactsEmail = function(arrParams,return_result){

	var arrRows = [];
	var queLinkAccoType = `SELECT * FROM jaze_email_accounts_ids WHERE account_id =?`;
	DB.query(queLinkAccoType,[arrParams.account_id], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
			if(data[0].email_id){

				var email = data[0].email_id.split(',');
				return_result(email);

			}else{
				return_result(null);
			}


	
 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


// search my contact

methods.searchMyContacts = function(arrParams,return_result){

	var arrContactsRaw = [];
	var arrContacts = [];

	let queGrpId = `SELECT t1.* FROM(
	SELECT group_id,
		(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
		(SELECT a.meta_value FROM jaze_jazecom_contact_list a WHERE a.meta_key = 'mail_account_id' AND a.group_id = jaze_jazecom_contact_list.group_id) AS mail_account_id
	FROM  jaze_jazecom_contact_list WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value =?) 
	) AS t1 WHERE account_id != ""`;


	DB.query(queGrpId,arrParams.account_id, function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			if(data[0].mail_account_id !=''){
				data[0].mail_account_id.split(",").forEach(function(val) {
					arrContactsRaw.push(val);
			
		  		});
			}

			var uniqueArray = arrContactsRaw.filter(function(item, pos, self) {
		    	return self.indexOf(item) == pos;
			});

			var valuesParams = {account_ids:uniqueArray,key:arrParams.key};

			methods.checkCredentialsAccountIDandLike(valuesParams,arrParams, function(retCred){ 
				
   				return_result(retCred);
			});

	   	}else{
	   		return_result(null);
	   	}

	});

};	


methods.checkCredentialsAccountIDandLike = function(valuesParams,arrParams,return_result){
		
	var arrContactsRaw = [];
	var arrContacts = [];
	var newObj = {};

	let queGrpId = " (SELECT t1.* FROM( ";
		queGrpId +=" SELECT account_id,account_type, "
		queGrpId +=" CONCAT( (CASE WHEN meta_key = 'fname' THEN meta_value END),' ',(SELECT a.meta_value FROM jaze_user_basic_details a WHERE a.meta_key = 'lname' AND a.account_id = jaze_user_basic_details.account_id) ) AS full_name, ";
		queGrpId +=" (SELECT b.meta_value FROM jaze_user_basic_details b WHERE b.meta_key = 'profile_logo' AND b.account_id = jaze_user_basic_details.account_id) AS logo ";
		queGrpId +=" FROM  jaze_user_basic_details ";
		queGrpId +=" WHERE account_type != '3' ";
		queGrpId +=" AND LOWER((CASE WHEN (meta_key = 'fname' OR meta_key = 'lname') THEN meta_value LIKE '%" + valuesParams.key + "%'  END)) ";
		queGrpId +=" AND account_id IN("+valuesParams.account_ids+")";
		queGrpId +=" ) AS t1 WHERE account_id !='') ";

		queGrpId +=" UNION ALL ";

		queGrpId += " (SELECT t1.* FROM( ";
		queGrpId +=" SELECT account_id,account_type, "
		queGrpId +=" (CASE WHEN meta_key = 'company_name' THEN meta_value END) as full_name, ";
		queGrpId +=" (SELECT b.meta_value FROM jaze_user_basic_details b WHERE b.meta_key = 'company_logo' AND b.account_id = jaze_user_basic_details.account_id) AS logo ";
		queGrpId +=" FROM  jaze_user_basic_details ";
		queGrpId +=" WHERE account_type = '3' ";
		queGrpId +=" AND LOWER((CASE WHEN (meta_key = 'company_name') THEN meta_value LIKE '%" + valuesParams.key + "%'  END)) ";
		queGrpId +=" AND account_id IN("+valuesParams.account_ids+")";
		queGrpId +=" ) AS t1 WHERE account_id !='') ";


	DB.query(queGrpId,null, function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			var arrRows = [];
			var newObj = {};

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){

					var blocked = {searched_id:val.account_id};

					methods.checkSearchedAccountIdForBlock(blocked, function(retCred){ 

						if(retCred!=null){

							if(!retCred.includes(arrParams.account_id)){

				  				methods.getjazemail(val.account_id, function(returned){		
											
							  		var logo = '';
							  		var fullname = '';

							 		if(val.logo){
								  		if(val.logo != ''){
								  			if(val.account_type == '1'){
								  				logo = G_STATIC_IMG_URL+'uploads/jazenet/individual_account/members_thumb/thumb_'+val.logo;
								  			}else{
								  				logo = G_STATIC_IMG_URL+'uploads/companylogo/'+val.logo;
								  			}
								  		}
							  		}	

									if(val.full_name){
										fullname = val.full_name;
						  			}
					  		
						  			newObj = {
						  				account_id: val.account_id.toString(),
										name: fullname,
										type: val.account_type.toString(),
						  				logo: logo,
						  				email_address:returned

						  			}

						  			arrRows.push(newObj);

				  				});

							}else{


				  				methods.getjazemail(val.account_id, function(returned){		
											
							  		var logo = '';
							  		var fullname = '';

							 		if(val.logo){
								  		if(val.logo != ''){
								  			if(val.account_type == '1'){
								  				logo = G_STATIC_IMG_URL+'uploads/jazenet/individual_account/members_thumb/thumb_'+val.logo;
								  			}else{
								  				logo = G_STATIC_IMG_URL+'uploads/companylogo/'+val.logo;
								  			}
								  		}
							  		}	

									if(val.full_name){
										fullname = val.full_name;
						  			}
					  		
						  			newObj = {
						  				account_id: val.account_id.toString(),
						  				name: fullname,
						  				logo: logo,
						  				email_address:returned
						  			}

						  			arrRows.push(newObj);

				  				});


							}

						}else{

			  				methods.getjazemail(val.account_id, function(returned){		
										
						  		var logo = '';
						  		var fullname = '';

						 		if(val.logo){
							  		if(val.logo != ''){
							  			if(val.account_type == '1'){
							  				logo = G_STATIC_IMG_URL+'uploads/jazenet/individual_account/members_thumb/thumb_'+val.logo;
							  			}else{
							  				logo = G_STATIC_IMG_URL+'uploads/companylogo/'+val.logo;
							  			}
							  		}
						  		}	

								if(val.full_name){
									fullname = val.full_name;
					  			}
				  		
					  			newObj = {
					  				account_id: val.account_id.toString(),
					  				name: fullname,
					  				logo: logo,
					  				email_address:returned
					  			}

					  			arrRows.push(newObj);

			  				});
						}

					});
				   	
			  	}

			  	const result = await returnData(arrRows);
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 300);
			  });
			}
			 
			forEach().then(() => {
		   		return_result(arrRows);
			})

	   	}else{
	   		return_result([]);
	   	}

	});
};



methods.getjazemail = function(arrParams,return_result){

	var arrRows = [];
	var queLinkAccoType = `SELECT jazemail_id FROM jaze_user_credential_details WHERE account_id =?`;
	DB.query(queLinkAccoType,[arrParams], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
			if(data[0].jazemail_id){

				var email = data[0].jazemail_id;
				return_result(email);

			}else{
				return_result(null);
			}


	
 	   	}else{
 	   		return_result(null);
 	   	}

	});
};




//manual block check



methods.getAccountIDperEmailForBlock = function(arrEmails,arrParams,return_result){


	var arrRows = [];
	var arrMails = [];
	var newObj = {};


	var qRy = `SELECT * FROM jaze_user_credential_details WHERE jazemail_id IN(`+arrEmails +`) AND status = '1'`;

	DB.query(qRy,null, function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
		
					arrRows.push(val.account_id);
			  	}


			  	const result = await returnData(arrRows);
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {

				methods.checkiFBlockEmails(arrRows,arrParams, function(returned){ 
					if(returned!=''){
						return_result(returned);
					}else{
			        	return_result(null);
					}
	  			

		   		});

			})
        }
        else
        { 
        	return_result(null);
        }

	});
};


methods.checkiFBlockEmails = function(accParams,arrParams, return_result){


	var arrRows = [];
	var arrReturn = [];
	var arrObject = {};

	var quer = `SELECT t1.* FROM(
			SELECT group_id,
					(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
					(SELECT a.meta_value FROM jaze_jazecom_contact_list a WHERE a.meta_key = 'mail_account_id' AND a.group_id = jaze_jazecom_contact_list.group_id) AS mail_account_id,
					(SELECT e.meta_value FROM jaze_jazecom_contact_list e WHERE e.meta_key = 'mail_blocked_account_id' AND e.group_id = jaze_jazecom_contact_list.group_id) AS mail_blocked_account_id
			FROM  jaze_jazecom_contact_list WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value IN(?)) 
			) AS t1 WHERE account_id != ""`;

	DB.query(quer,[accParams], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
	   	{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
					
					arrObject ={
						block_accounts:val.mail_blocked_account_id.split(","),
						blocker_account:val.account_id
					}
					arrRows.push(arrObject);
			  	}


			  	const result = await returnData(arrRows);
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
				
				for(let i = 0; i < arrRows.length; i++){

				   	let blocked_accounts = arrRows[i].block_accounts;
				   	let blocker_account = arrRows[i].blocker_account;

					if(blocked_accounts.includes(arrParams.account_id)){

						arrReturn.push(blocker_account);
					}
				}

		 		methods.getjazemailForBlock(arrReturn, function(returned){ 

					return_result(returned);
			   	
				});

			})

	   	}
	   	else
	   	{
	   		return_result([]);
	   	}

	});
};


methods.getjazemailForBlock = function(arrParams,return_result){


	var arrRows = [];
	var queLinkAccoType = `SELECT * FROM jaze_user_credential_details WHERE account_id IN(?)`;
	DB.query(queLinkAccoType,[arrParams], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){



			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
			 
					arrRows.push(val.jazemail_id);
			  	}


			  	const result = await returnData(arrRows);
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {

				return_result(arrRows);
		
			})

 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


//------------------------------------------------------------------- Jazecom Mail Status Module Start ---------------------------------------------------------------------------//

	/*
	 * save or Reject to jazecome contact table
	 */
	methods.checkAccountJazecomMailStatus =  function(arrParams,return_result){

		try
		{
			var account_id = arrParams.account_id;

			var request_account_id = arrParams.request_account_id;

			methods.__getJazeMailId(account_id, function (ownerMailStatus) {

				if(parseInt(ownerMailStatus) !== 0)
				{
					methods.__getJazeMailId(request_account_id, function (recivMailStatus) {

						if(parseInt(recivMailStatus) !== 0)
						{
							methods.__checkJazeMailActiveStatus(account_id, function (ownerStatus) {

								if(parseInt(ownerStatus) === 1)
								{
									methods.__checkJazeMailActiveStatus(request_account_id, function (recivStatus) {

										if(parseInt(recivStatus) === 1)
										{
											methods.__checkJazeMailBlockStatus(account_id,request_account_id, function (ownerBlockStatus) {

												if(parseInt(ownerBlockStatus) === 1)
												{
													methods.__checkJazeMailBlockStatus(request_account_id,account_id, function (recivBlockStatus) {

														if(parseInt(recivBlockStatus) === 1)
														{ return_result("1"); }
														else
														{ return_result("2"); }
													});
												}
												else
												{ return_result("2"); }
											});
										}
										else
										{ return_result("3"); }
									});
								}
								else
								{ return_result("3"); }
							});
						}
						else
						{ return_result("0"); }
					});
				}
				else
				{ return_result("0"); }
			});
		}
		catch(err)
		{
			console.log("checkAccountJazecomMailStatus",err);
			return_result(null);
		}
	}

	methods.__checkJazeMailActiveStatus =  function(account_id,return_result){

		try
		{
			var query ="  SELECT meta_value FROM jaze_user_jazecome_status WHERE meta_key in ('jaze_mail') and  group_id  in ( SELECT group_id FROM jaze_user_jazecome_status WHERE meta_key = 'account_id' AND meta_value ='"+account_id+"'  ) ";

			DB.query(query,null, function(data, error){
									
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].meta_value);
				}
				else
				{ return_result("1"); }
			});
		}
		catch(err)
		{
			console.log("__checkJazeMailActiveStatus",err);
			return_result("0");
		}
	}

	methods.__checkJazeMailBlockStatus =  function(account_id,request_account_id,return_result){

		try
		{
			var query =" SELECT meta_value FROM jaze_jazecom_contact_list WHERE meta_key != '' and meta_key in ('mail_blocked_account_id') and  group_id  in ( SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value ='"+account_id+"'  ) ";

			DB.query(query,null, function(data, error){
									
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					if(data[0].meta_value !== ""){
					
						var list = convert_array(data[0].meta_value);

						if(list.indexOf(request_account_id) !== -1)
						{ 
							return_result("0");
						}
						else
						{ return_result("1"); }
					}
					else
					{ return_result("1"); }
					
				}
				else
				{ return_result("1"); }
			});
		}
		catch(err)
		{
			console.log("__checkJazeMailBlockStatus",err);
			return_result("0");
		}
	}

	methods.__getJazeMailId = function(account_id,return_result)
	{ 
		try
		{
			if(parseInt(account_id) !== 0)
			{
				var query = " SELECT group_id FROM `jaze_email_accounts`  WHERE ";  
					query += " group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'account_id' AND meta_value = '"+account_id+"' ) group by group_id; ";

					DB.query(query, null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_result(data[0].group_id);
						}
						else
						{return_result("0");}

					});
			}
			else
			{ return_result("0"); }
		}
		catch(err)
		{ console.log("__getJazeMailId",err); return_result("0"); }	
	}

	function convert_array(array_val) {

		if(array_val !== null && array_val !== "")
		{
			var list = [];
	
			if(array_val.toString().indexOf(',') != -1 ){
				list = array_val.split(',');
			}
			else {
				list.push(array_val);
			}
	
			return list;
		}
		else
		{ return ""; }
	}

//------------------------------------------------------------------- Jazecom Mail Status Module Start ---------------------------------------------------------------------------//

//------------------------------------------------------------------- System Mailer Status Module Start ---------------------------------------------------------------------------//

methods.sendSystemMailer = function(arrParams,return_result)
{ 
	try
	{
	
		var to_email = arrParams.to_email;
		var subject = arrParams.subject;
		var message = arrParams.message;


		var sendEmail = mailer.config({
			host: 'smtp.gmail.com',
		  port: 465,
		  secure: true, // true for 465, false for other ports
		  auth: {
				user: 'donotreplyjazenet@gmail.com', // generated ethereal user
				pass: 'pv85@fnd'
				  // generated ethereal password
		  }
	  });


		var message_arr = {
	  	    from: 'do-not-reply@jazenet.com', // sender address
		    to: to_email,
		    cc: '',
		    bcc: '',
		    subject: subject, // Subject line
		    //text: message, // plain text body
		    html: message, // html body
	     	attachments: ''
		};

		sendEmail(message_arr)
	    .then(function(info){

			//send notification process start
			var toCheckEmails = JSON.stringify(to_email).replace(/[\[\]]+/g,"");
			var arrAccountIdtoNoti = [];
			var mail_push_notification = {};

           	var today = new Date();
           	var dateNow = dateFormat(today,'yyyy-mm-dd');

			HelperJazeMail.checkIfinCredentials(toCheckEmails, function(retAccountIds){

				
				if(retAccountIds!=null){
					arrAccountIdtoNoti.push(retAccountIds);
				}

				if(arrAccountIdtoNoti !=null){

					arrAccountIdtoNoti.forEach(function(val,idx) {

						mail_push_notification = {
							group_id:(val[idx].email_address+'|'+info.messageId+'|'+dateNow).toString(),
							account_id:"-1",
							request_account_id:(val[idx].account_id).toString(),
							msg:subject,
							flag:"12"
						};

						HelperNotification.sendPushNotification(mail_push_notification, function(returnNoti){

							
						});

					});
				}
			
			});
			//send notification process end

			return_result(1);

			


	    }).catch(function(err){
	    //	console.log('got error'); console.log(err);
	    	return_result(0);

	    });
	}
	catch(err)
	{ console.log("__SystemMailer",err); return_result("0"); }	
}

let copyToSentMailer = function(arrParams,emailContent,return_result){

	var imap = new Imap({
	  	user      : 'do-not-reply@jazenet.com',
	  	password  : 'pv85@fn',
	  	host      : 'imap.yandex.com',
	  	port      :  993,
	  	tls       :  true
	});

	var msg, alternateEntity, htmlEntity, plainEntity, pngEntity;

	function openInbox(cb) {
	  	imap.openBox('Sent', true, cb);
	}

    imap.once('ready', function () {
        openInbox(function (err, box) {
        	if(!err){

		        msg = mimemessage.factory({
	          		contentType: 'multipart/alternate',
		          	body: []
		        });
	
		        plainEntity = mimemessage.factory({
	        	  	contentType: 'text/html;charset=utf-8',
		          	body: emailContent.body_mes
		        });

		        msg.header('Message-ID', emailContent.message_id);
		        msg.header('from', arrParams.email_account);
		        msg.header('to', emailContent.to);
	 
		        msg.header('Subject', emailContent.subject);

		        msg.body.push(plainEntity);

		        imap.append(msg.toString());
		        return_result(1);

        	}else{
    			return_result(0);
        	}
	
		});
	});


    imap.once('error', function(err) {
    });

    imap.once('end', function() {
    	imap.end();
    });

    imap.connect();

}

//------------------------------------------------------------------- System Mailer Status Module End   ---------------------------------------------------------------------------//


methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].group_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}



module.exports = methods;
