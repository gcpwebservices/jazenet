const methods = {};
const DB = require('../../helpers/db_base.js');
const HelperGeneral = require('../../helpers/HelperGeneral.js');
const HelperRestriction = require('../../helpers/HelperRestriction.js');
const Helperfirestore = require('../firestore.js');
const HelperNotification = require('./HelperNotification.js');

var promiseForeach = require('promise-foreach');
const moment = require("moment");
const dateFormat = require('dateformat');
const fast_sort = require("fast-sort");

		/*
         * get Latest Convers_id
         */	
		methods.getLatestConversId = function(return_result){

			//check filed is exisit or not
			var query = "SELECT meta_value FROM jaze_jazechat_contact_list WHERE meta_key = 'last_convers_id' and  group_id = '0'";

			DB.query(query, null, function(lastdata, error){

				if (typeof lastdata[0] !== 'undefined' && lastdata[0] !== null)
				{
					var new_id = parseInt(lastdata[0].meta_value) + 1;
					//update last id
					var query = "UPDATE jaze_jazechat_contact_list SET meta_value = '"+new_id+"' WHERE meta_key = 'last_convers_id' and  group_id = '0' ";

					DB.query(query, null, function(data, error){
						return_result(new_id);
					});
				}
				else
				{
					var arrInsert = {
						last_convers_id: '1'
					};

					methods.insertTBQuery('jaze_jazechat_contact_list','0', arrInsert, function (queResGrpId) {

						return_result(arrInsert.last_convers_id);
					});
				}
			});
		};


		/*
		* save to jazecome contact table
		*/
		methods.saveJazechatContactList =  function(reqParams,return_result){

			try
			{
				//first check user have details
				var query = " SELECT group_id FROM jaze_jazechat_contact_list WHERE meta_key = 'account_id' AND meta_value ='"+reqParams.account_id+"' ";
				
				DB.query(query,null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						//update
						var group_id = data[0].group_id;

						var meta_fild_key,meta_fild_value = '';

						if(reqParams.single_convers_id !== "")
						{ meta_fild_key = "single_convers_id"; meta_fild_value = reqParams.single_convers_id; }
						else if(reqParams.group_convers_id !== "")
						{ meta_fild_key = "group_convers_id"; meta_fild_value = reqParams.group_convers_id; }
						else if(reqParams.broadcasting_convers_id !== "")
						{ meta_fild_key = "broadcasting_convers_id"; meta_fild_value = reqParams.broadcasting_convers_id; }

						//check insert or remove
						if(parseInt(reqParams.flag) === 1 && meta_fild_key !== "" && meta_fild_value !== "")
						{
							//insert
							//checking allready exisit
							var query = " SELECT meta_value FROM jaze_jazechat_contact_list WHERE meta_key = '"+meta_fild_key+"'  and group_id = '"+group_id+"' ";

							DB.query(query,null, function(data_check, error){

								var query_update = "";

								if(data_check[0].meta_value !== ''){

									query_update = " UPDATE jaze_jazechat_contact_list set meta_value = concat(meta_value, ',"+meta_fild_value+"')  WHERE meta_key = '"+meta_fild_key+"' and group_id = '"+group_id+"' ";
								}
								else {
									query_update = " UPDATE jaze_jazechat_contact_list set meta_value = '"+meta_fild_value+"'  WHERE meta_key = '"+meta_fild_key+"' and group_id = '"+group_id+"' ";
								}
			
								DB.query(query_update,null, function(data_update, error){
																	
									return_result(group_id);
								});	
							});	
						}
						else if(parseInt(reqParams.flag) === 0 && meta_fild_key !== "" && meta_fild_value !== "")
						{
							//remove members

							//get result
							var query_sel = "SELECT meta_value FROM jaze_jazechat_contact_list WHERE meta_key = '"+meta_fild_key+"' and group_id = '"+group_id+"';  ";

								DB.query(query_sel,null, function(data_update, error){
											
									var list = [];
											
									list = data_update[0].meta_value.split(",");

									list.splice(list.indexOf(meta_fild_value), 1);

									var query_update = "  UPDATE jaze_jazechat_contact_list SET meta_value = '"+list.join()+"' where meta_key = '"+meta_fild_key+"' and group_id = '"+group_id+"'; ";

									DB.query(query_update,null, function(data_update, error){
												return_result(group_id);
									});
								});
						}
					}
					else
					{
						//insert new 


						var query = " SELECT MAX(group_id) + 1 as group_id FROM jaze_jazechat_contact_list ";

							DB.query(query,null, function(dataG, error){ 

							if(typeof dataG !== 'undefined' && dataG !== null  && parseInt(dataG.length) > 0){
							
								var group_id = dataG[0].group_id;

								if(parseInt(group_id) !== 0){
								
									var arrConvInsert = {
										account_id: reqParams.account_id,
										single_convers_id:  (reqParams.single_convers_id !== "") ? reqParams.single_convers_id :"",
										group_convers_id: (reqParams.group_convers_id !== "") ? reqParams.group_convers_id :"",
										broadcasting_convers_id: (reqParams.broadcasting_convers_id !== "") ? reqParams.broadcasting_convers_id :"",
										status: '1'
									};

									methods.insertTBQuery('jaze_jazechat_contact_list',group_id, arrConvInsert, function (queResGrpId) {

										return_result(group_id);
									});
								}
							}
						});
					}
				});
			}
			catch(err)
			{
				console.log(err);
				return_result(null);
			}
		}

		/*
		* get Pending Contact List
		*/
		methods.getContactPendingList =  function(reqParams,return_result){

			try
			{
				var query = "";

				var array_rows = [];

				var meta_key_fild = "account_id";

				if(parseInt(reqParams.account_type) === 1)
				{  //incoming
					meta_key_fild = "contact_account_id";
				}

				query = " SELECT * ";
				query += "FROM jaze_jazechat_conversation_list WHERE  ";
				query += "group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = '"+meta_key_fild+"' AND meta_value ='"+reqParams.account_id+"' AND  ";
				query += "group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'status' AND meta_value ='0' )) order by meta_key ";
				
				DB.query(query,null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							
						HelperRestriction.convertMultiMetaArray(data, function(retrnResult){
							
							if(retrnResult !== null)
							{
								var counter = retrnResult.length;

								if(parseInt(counter) !== 0)
								{
									promiseForeach.each(retrnResult,[function (jsonItems) {
					
											return new Promise(function (resolve, reject) {											
											
													let arrAccountParams = {};

													if(parseInt(reqParams.account_type) === 1)
													{ arrAccountParams = { account_id:jsonItems.account_id, account_type:jsonItems.account_type }; }
													else
													{ arrAccountParams = { account_id:jsonItems.contact_account_id, account_type:jsonItems.contact_account_type }; }
													
													HelperGeneral.getAccountDetails(arrAccountParams, function(accDetails){	
														
														if(accDetails !== null)
														{
															var incoming_flag = (parseInt(reqParams.account_type)  === 1) ? "1": "0";

															var result_obj = { 
																convers_id: jsonItems.conversa_id.toString(),
																account_id: accDetails.account_id.toString(),
																account_type: accDetails.account_type.toString(),
																account_title: accDetails.title.toString(),
																name: accDetails.name,
																logo:accDetails.logo,
																category: accDetails.category,
																jazenet_id	: accDetails.jazenet_id,
																reason: jsonItems.reason,
																date: jsonItems.create_date,
																incoming_flag : incoming_flag
															};

															array_rows.push(result_obj);

															counter -= 1;

															resolve(array_rows);
														}
														else
														{ counter -= 1; resolve(array_rows);}
																
													});	
											})
									}],
									function (result, current) {
											
										if (counter === 0)
										{ return_result(result[0]); }
									});	
								}
								else
								{ return_result(array_rows);}
							}
							else
							{ return_result(array_rows);}
						});			
					}
					else
					{ return_result(array_rows);}
				});
			}
			catch(err)
			{
				console.log(err);
				return_result(array_rows);
			}
		}

			/*
		* get Pending Contact List
		*/
		methods.getContactPendingAccountList =  function(reqParams,return_result){

			try
			{
				var query = "";

				var array_rows = [];

				if(parseInt(reqParams.account_type) === 1)
				{
					//incoming
					query = " SELECT meta_value as contact_account_id ";
					query += "FROM jaze_jazechat_conversation_list WHERE  meta_key IN ('account_id') AND ";
					query += "group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'contact_account_id' AND meta_value ='"+reqParams.account_id+"' AND  ";
					query += "group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'status' AND meta_value ='0' )) order by meta_key ";
				}
				else
				{
					//outgoing
					query = " SELECT meta_value as contact_account_id ";
					query += "FROM jaze_jazechat_conversation_list WHERE  meta_key IN ('contact_account_id') AND ";
					query += "group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'account_id' AND meta_value ='"+reqParams.account_id+"' AND  ";
					query += "group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'status' AND meta_value ='0' )) order by meta_key ";
				}

				DB.query(query,null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							

						data.forEach(function(rows) {
							array_rows.push(parseInt(rows.contact_account_id));
						});
						
						return_result(array_rows);
					}
					else
					{ return_result(null);}
				});
			}
			catch(err)
			{
				console.log(err);
				return_result(null);
			}
		}


		/*
		* get Active Conversation List
		*/
		methods.getActiveConversationList =  function(account_id,return_result){

		//	console.log("-------------------------------- START "+account_id+" conversation list --------------------------------");
			
			if(parseInt(account_id) !== 0)
				{

					let query = " SELECT * FROM jaze_jazechat_contact_list WHERE  group_id in(SELECT group_id FROM jaze_jazechat_contact_list WHERE meta_key = 'account_id' AND meta_value ='"+account_id+"')  and meta_key not in('status','account_id') ";
				
					DB.query(query,null, function(jsonContact, error){
						
						if(typeof jsonContact !== 'undefined' && jsonContact !== null && parseInt(jsonContact.length) > 0){

							var counter = jsonContact.length;

							//	console.log("tot counter : "+counter);

								var array_rows = [];

								if(parseInt(counter) !== 0)
								{
									promiseForeach.each(jsonContact,[function (jsonContact) {

										return new Promise(function (resolve, reject) {
											
											methods.getSingleConversDetails(jsonContact,account_id, function(singDetails){	

												methods.getBroadcastingConversDetails(jsonContact,account_id, function(brodcasDetails){	
													
													if(singDetails !== null)
													{
														array_rows = array_rows.concat(singDetails);

														resolve(array_rows);
													}
													else if(brodcasDetails !== null)
													{
														array_rows = array_rows.concat(brodcasDetails);

														resolve(array_rows);
													}
													
													counter -= 1;

												});
											});
										});
									}],
									function (result, current) {
										//  console.log("count : "+counter);
										if (counter === 0)
										{ 
											return_result(result[0]); 
											//console.log("-------------------------------- END "+account_id+" conversation list  --------------------------------"); 
										}
									});
								}
								else
								{ return_result(null); }
						}
						else
						{ return_result(null); }
					});
				}
				else
				{ return_result(null); }
		};		

		/*
		 * get single convers details
		 */
		methods.getSingleConversDetails = function(reqParams,account_id,return_result)
		{
			if(reqParams !== null && reqParams.meta_key === "single_convers_id" && reqParams.meta_value !== "")
			{
				var list_single = [];

				if(reqParams.meta_value.toString().indexOf(',') != -1 ){
					list_single = reqParams.meta_value.split(',');
				}
				else {
					list_single.push(reqParams.meta_value);
				}

		//	 console.log("list_single : ",list_single);

				var counter = list_single.length;

				var array_rows = [];

				if(parseInt(counter) !== 0)
				{
				 	promiseForeach.each(list_single,[function (list_single) {

				 		return new Promise(function (resolve, reject) {

							if(list_single === '' || list_single === '0')
							{
								counter -= 1;
							}
							else
							{
								let query   = " SELECT group_id,meta_key,meta_value  FROM  jaze_jazechat_conversation_list WHERE   "; 
									query  += " group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'conversa_id' AND meta_value ='"+list_single+"') ";

								DB.query(query,null, function(data, error){

									if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

										HelperRestriction.convertMetaArray(data, function(jsonItems){	

											var reciver_account_id = '';

											if(parseInt(jsonItems.account_id) === parseInt(account_id))
											{
												reciver_account_id = jsonItems.contact_account_id;
											}
											else
											{
												reciver_account_id = jsonItems.account_id;
											}
		
											let arrAccountParams = { account_id:reciver_account_id };
											
												HelperGeneral.getAccountDetails(arrAccountParams, function(accDetails){	
												
												if(accDetails !== null) {
													//check Mute Status
													let arrMuteParams = { account_id:account_id, group_id:jsonItems.conversa_id };
											
													methods.getMuteDetails(arrMuteParams, function(muteDetails){
													

														//check block Status
														let arrBlockParams = { account_id:account_id, convers_id:jsonItems.conversa_id };

														methods.getContactBlockStatus(arrBlockParams, function(block_object){	

															//check start and end date
																methods.getStartandEndDateDetails (jsonItems.conversa_id,account_id,function(convdatedetails){
																	//console.log(jsonItems.status+"- "+jsonItems.conversa_id);

																	if(parseInt(jsonItems.status) !== 0)
																	{
																		//check block_status
																		var block_status = "1";

																		if(parseInt(block_object.block_out_status) === 1)
																		{ block_status = "2"; }

																			var datefull = dateFormat(jsonItems.update_date.toString(), 'yyyy-mm-dd HH:MM:ss');
																			
																			var result_obj = { 
																				convers_id: jsonItems.conversa_id.toString(),
																				owner_account_id: jsonItems.account_id.toString(),
																				account_id: accDetails.account_id.toString(),
																				account_type: accDetails.account_type.toString(),
																				account_title: accDetails.title.toString(),
																				name: accDetails.name,
																				logo:accDetails.logo,
																				category: accDetails.category,
																				jazenet_id	: accDetails.jazenet_id,
																				reason: "",
																				online_status:'0',
																				block_status:block_object,
																				mute_status:muteDetails.mute_status.toString(),
																				conv_date_details:convdatedetails,
																				unread_count: jsonItems['unread_count_'+account_id].toString(),
																				convers_date:  datefull,
																				order_by: datefull,
																				contact_status: block_status,
																				convers_flag: '1',
																				members_count: '',
																				contact_settings : {
																					convers_flag: '1',
																					block_status:block_object,
																					mute_status_settings:muteDetails,
																					members_count:'1'
																				}
																			};

																		//	convers_array.push(jsonItems.conversa_id);

																			array_rows.push(result_obj);

																			counter -= 1;

																			resolve(array_rows);
																				
																		//  });
																	}
																	else
																	{
																		if(parseInt(account_id) !== parseInt(jsonItems.account_id))
																		{
																			var newdate = new Date(moment().utc().format('YYYY-MM-DD HH:mm:ss'));
																				newdate.setDate(newdate.getDate() + 3);
																				const datefull = dateFormat(newdate, 'yyyy-mm-dd HH:MM:ss');

																			var result_obj = { 
																				convers_id: jsonItems.conversa_id.toString(),
																				owner_account_id: jsonItems.account_id.toString(),
																				account_id: accDetails.account_id.toString(),
																				account_type: accDetails.account_type.toString(),
																				account_title: accDetails.title.toString(),
																				name: accDetails.name,
																				logo:accDetails.logo,
																				category: accDetails.category,
																				jazenet_id	: accDetails.jazenet_id,
																				reason: jsonItems.reason,
																				online_status:'0',
																				block_status:block_object,
																				mute_status:'0',
																				unread_count: '0',
																				convers_date: dateFormat(jsonItems.update_date.toString(), 'yyyy-mm-dd HH:MM:ss'),
																				order_by: datefull,
																				contact_status: jsonItems.status,
																				convers_flag: '1',
																				members_count: '',
																				contact_settings : {
																					convers_flag: '1',
																					block_status:block_object,
																					mute_status_settings:{},
																					members_count:'1'
																				}
																			};
																			
																			array_rows.push(result_obj);

																			counter -= 1;

																			resolve(array_rows);
																		}
																		else
																		{ counter -= 1; resolve(array_rows);}
																	}
																});
														});
													});
												}
												else
												{ counter -= 1; resolve(array_rows); }
											});	
										});
									}
									else
									{ counter -= 1; resolve(array_rows); }
								});
							}
						})
					}],
					function (result, current) {
					//   console.log("count : ",counter);
					//   console.log(" convers_id : ",convers_array);
						if (counter === 0)
						{  return_result(result[0]);}
					});
				}
				else
				{return_result(null);}	
			}
			else
			{return_result(null);}
		}


		/*
		 * get Broadcasting convers details
		 */
		methods.getBroadcastingConversDetails = function(reqParams,account_id,return_result)
		{
			if(reqParams !== null && reqParams.meta_key === "broadcasting_convers_id" && reqParams.meta_value !== "")
			{
				var list_single = [];

				if(reqParams.meta_value.toString().indexOf(',') != -1 ){
					list_single = reqParams.meta_value.split(',');
				}
				else {
					list_single.push(reqParams.meta_value);
				}

				var counter = list_single.length;

				//console.log("tot count : ",counter);

				var array_rows = [];
				
				if(parseInt(counter) !== 0)
				{
				 	promiseForeach.each(list_single,[function (list_single) {

				 		return new Promise(function (resolve, reject) {

							if(list_single === '' || list_single === '0')
							{
								counter -= 1;
							}
							else
							{
								// conversation own account

								var query = " SELECT * from jaze_jazechat_broadcasting_list where  meta_key in ('account_id','update_date','broadcast_name','broadcast_logo','conversa_id','members_convers_id')  ";
									query += " AND group_id IN (SELECT group_id FROM  jaze_jazechat_broadcasting_list  WHERE meta_key = 'conversa_id' AND meta_value = '"+list_single+"' ";
									query += " AND group_id IN (SELECT group_id FROM  jaze_jazechat_broadcasting_list  WHERE meta_key = 'status' AND meta_value = '1' )) ";

									DB.query(query, null, function(data, error){
																
										if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

											HelperRestriction.convertMultiMetaArray(data, function(retrnResult){

												var data = retrnResult[0];

												//check start and end date
												methods.getStartandEndDateDetails (data.conversa_id,account_id,function(convdatedetails){
																		
													//console.log("- "+convers_id);

													var datefull = dateFormat(data.update_date.toString(), 'yyyy-mm-dd HH:MM:ss');

													var result_obj = { 
														convers_id	: data.conversa_id.toString(),
														owner_account_id: data.account_id.toString(),
														account_id	: "0",
														account_type: "0",
														account_title: "",
														name		: data.broadcast_name,
														logo		: data.broadcast_logo,
														category	: "broadcast",
														jazenet_id	: "",
														reason		: "",
														online_status:'0',
														block_status :{},
														mute_status	 :"0",
														conv_date_details:convdatedetails,
														unread_count: '0',
														convers_date:  datefull,
														order_by	: datefull,
														contact_status: "1",
														convers_flag: "3",
														members_count: (data.members_convers_id.toString().split(",")).length.toString(),
														contact_settings : {
															convers_flag: '3',
															block_status:{},
															mute_status_settings:{},
															members_count:(data.members_convers_id.toString().split(",")).length.toString()
														}
													};

													array_rows.push(result_obj);

													counter -= 1;

													resolve(array_rows);
												});		
											});
										}
										else
										{return_result(null);}
									});
							}
						})
					}],
					function (result, current) {
						// console.log("count : ",counter);
						if (counter === 0)
						{ return_result(result[0]); }
					});
				}
				else
				{return_result(null);}
			}
			else
			{return_result(null);}
		};
		
		 /*
         * get Account Contact List
         */
        methods.getAccountContactList = function(reqParams, return_result){
			
			try
			{
				var result_list = {};

				result_list = {
					total_count : "0",
					pending_list :[]
				};

				if(parseInt(reqParams.account_id) !== 0)
				{
					
					//flag == 1: contact list 2 = incoming and outgoing pending list

					if(parseInt(reqParams.flag) === 1)
					{
						// conversation own account
						let query  = " (SELECT ";
						query += " (SELECT a.meta_value FROM jaze_jazechat_conversation_list a WHERE a.meta_key = 'contact_account_id' AND a.group_id = jaze_jazechat_conversation_list.group_id) AS contact_account_id, ";
						query += " (SELECT b.meta_value FROM jaze_jazechat_conversation_list b WHERE b.meta_key = 'contact_account_type' AND b.group_id = jaze_jazechat_conversation_list.group_id) AS contact_account_type, ";
						query += " (SELECT i.meta_value FROM jaze_jazechat_conversation_list i WHERE i.meta_key = 'status' 		AND i.group_id = jaze_jazechat_conversation_list.group_id) AS contact_flag, ";
						query += " (SELECT j.meta_value FROM jaze_jazechat_conversation_list j WHERE j.meta_key = 'conversa_id' AND j.group_id = jaze_jazechat_conversation_list.group_id) AS convers_id ";
						query += " FROM  jaze_jazechat_conversation_list  ";
						query += " WHERE group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'account_id' AND meta_value ='"+reqParams.account_id+"' ";
						query += " AND group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'status' AND meta_value !='0')) group by group_id ) ";

						query += " union all ";

					// conversation link account and pending list 
						query += " (SELECT  ";
						query += " (SELECT e.meta_value FROM jaze_jazechat_conversation_list e WHERE e.meta_key = 'account_id'   AND e.group_id = jaze_jazechat_conversation_list.group_id) AS contact_account_id, ";
						query += " (SELECT f.meta_value FROM jaze_jazechat_conversation_list f WHERE f.meta_key = 'account_type' AND f.group_id = jaze_jazechat_conversation_list.group_id) AS contact_account_type, ";
						query += " (SELECT i.meta_value FROM jaze_jazechat_conversation_list i WHERE i.meta_key = 'status' 		AND i.group_id = jaze_jazechat_conversation_list.group_id) AS contact_flag, ";
						query += " (SELECT j.meta_value FROM jaze_jazechat_conversation_list j WHERE j.meta_key = 'conversa_id' AND j.group_id = jaze_jazechat_conversation_list.group_id) AS convers_id ";
						query += " FROM  jaze_jazechat_conversation_list  ";
						query += " WHERE group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'contact_account_id' AND meta_value ='"+reqParams.account_id+"' ";
						query += " AND group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'status' AND meta_value !='0')) group by group_id ) ";


							DB.query(query,null, function(data, error){
		
								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
				
									var array_rows = [];
				
									var counter = data.length;
											
									var jsonItems = Object.values(data);

									if(parseInt(counter) !== 0)
									{
										promiseForeach.each(jsonItems,[function (jsonItems) {
					
											return new Promise(function (resolve, reject) {											
												
												
													let arrAccountParams = { account_id:jsonItems.contact_account_id, account_type:jsonItems.contact_account_type };
						
													HelperGeneral.getAccountDetails(arrAccountParams, function(accDetails){	
						
														var result_obj = { 
															convers_id: jsonItems.convers_id.toString(),
															account_id: jsonItems.contact_account_id.toString(),
															account_type: jsonItems.contact_account_type.toString(),
															name: accDetails.name,
															logo:accDetails.logo,
															category: accDetails.category,
															title: accDetails.title.toString(),
															contact_status: jsonItems.contact_flag
														};
													
														array_rows.push(result_obj);

														counter -= 1;

														resolve(array_rows);
																
													});	
													
											})
										}],
										function (result, current) {		
											if (counter === 0)
											{ return_result(result[0]); }
										});
									}else{
										return_result(null);
									}
					
								}else{
									return_result(null);
								}
							});
					}
					else if(parseInt(reqParams.flag) === 2)
					{
						// account_type == 1 its incoming list 
						// account_type == 2 || 3  its outgoing list 

						methods.getContactPendingList(reqParams, function(list){	
												
							if(typeof list !== 'undefined' && list !== null && parseInt(list.length) > 0){							
								
								var count = (parseInt(reqParams.account_type)  !== 1) ? "": list.length.toString();

								result_list = {
									total_count : count,
									pending_list :fast_sort(list).desc('date')
								};								
							}

							return_result(result_list);	
						});
					}
					else 
					{ return_result(result_list); }
				}
				else
				{ return_result(result_list); }
			}
			catch(err)
			{
				console.log(err);
				return_result(result_list);
			}
		};

		/*
		* check contact already exists
		*/
		methods.checkContactAlreadyExists = function(arrParams,return_result){

			try{
				
				//will check if account_id has already request to request_account_id
				let query  =" SELECT * FROM jaze_jazechat_conversation_list WHERE meta_key in ('conversa_id','status') and ";
					query +=" (group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'contact_account_id' AND meta_value ='"+arrParams.account_id+"' and group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'account_id' AND meta_value ='"+arrParams.request_account_id+"')) ";
					query +=" or ";
					query +=" group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'contact_account_id' AND meta_value ='"+arrParams.request_account_id+"' and group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'account_id' AND meta_value ='"+arrParams.account_id+"'))";
					query +=") ";

				DB.query(query,null, function(data, error){
			
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0){	

						HelperRestriction.convertMetaArray(data, function(jsonItems){
							
							return_result(jsonItems);
						});
					}
					else
					{ return_result(null); }
				});

			}
			catch(err)
			{
				return_result(null);
			}
		};		

		/*
		* save Contact Request
		*/
		methods.saveContactRequest =  function(reqParams,return_result){

			try
			{
				//if flag : 
				// 1 =Conversation details save to tb and firebase create group_id ,  // open
				// 2 = request details save to tb  and push notification			 // request	

				var flag = 0;

				if(parseInt(reqParams.account_type) === 4 || parseInt(reqParams.account_type) === 3 || parseInt(reqParams.account_type) === 2)
				{ 
					if(parseInt(reqParams.request_account_type) === 4 || parseInt(reqParams.request_account_type) === 3 || parseInt(reqParams.request_account_type) === 2)
					{  flag= 1; }
					else
					{ flag= 2; }
				}
				else if(parseInt(reqParams.account_type) === 1 )
				{  flag= 1; }

				const dateUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

					// get Group_id
					HelperRestriction.getNewGroupId('jaze_jazechat_conversation_list', function (group_id) {

					if(parseInt(group_id) !== 0){

						//get latest convers id

						methods.getLatestConversId(function(conversa_id) {

							var callbackResult = {
								group_id: conversa_id,
								flag: flag
							};	

							//save to jazechat contact tb

							var arrOwnerContact = {
								account_id: reqParams.account_id,
								single_convers_id : conversa_id,
								group_convers_id : "",
								broadcasting_convers_id : "",
								flag: '1'
							};

							methods.saveJazechatContactList(arrOwnerContact,function (resultOwner){

								if(resultOwner !== null)
								{
									var arrReciverContact = {
										account_id: reqParams.request_account_id,
										single_convers_id : conversa_id,
										group_convers_id : "",
										broadcasting_convers_id : "",
										flag: '1'
									};
		
									methods.saveJazechatContactList(arrReciverContact,function (resultreciver){
	
										if(resultreciver !== null)
										{
											var owner_count = "unread_count_"+reqParams.account_id;
											var request_count = "unread_count_"+reqParams.request_account_id;

											//conv details save to tb
											var arrConvInsert = {};

											if (parseInt(flag) === 1)
											{
												arrConvInsert = {
													account_id: reqParams.account_id,
													account_type: reqParams.account_type,
													contact_account_id: reqParams.request_account_id,
													contact_account_type: reqParams.request_account_type,
													create_date: dateUTC,
													update_date: dateUTC,
													[owner_count]: '0',
													[request_count]: '0',
													conversa_id: conversa_id,
													status: '1'
												};
											}
											else
											{
												arrConvInsert = {
													account_id: reqParams.account_id,
													account_type: reqParams.account_type,
													contact_account_id: reqParams.request_account_id,
													contact_account_type: reqParams.request_account_type,
													reason: reqParams.message,
													create_date: dateUTC,
													update_date: dateUTC,
													[owner_count]: '0',
													[request_count]: '0',
													conversa_id: conversa_id,
													status: '0'
												};
											}
											

												methods.insertTBQuery('jaze_jazechat_conversation_list',group_id, arrConvInsert, function (queResGrpId) {

													if (parseInt(queResGrpId) !== 0) {

														//Jazecom Contact List  inseration
														if (parseInt(flag) === 1) {
															
															//save to Jazechat Contact List

															var arrOwnerJazeContact = {
																account_id: reqParams.account_id,
																request_account_id : reqParams.request_account_id,
																flag : "1"
															};
														
															methods.saveReomveJazecomContactList(arrOwnerJazeContact,function (resultJazeOwner){

																
																if(parseInt(resultJazeOwner) !== 0)
																{

																	var arrReciverJazeContact = {
																		account_id: reqParams.request_account_id,
																		request_account_id : reqParams.account_id,
																		flag : "1"
																	};
																
																	methods.saveReomveJazecomContactList(arrReciverJazeContact,function (resultJazeReciver){
		
																		
																		if(parseInt(resultJazeReciver) !== 0)
																		{
																			//get conversation Owner Details
																			methods.__getConversationOwnerDetails(conversa_id,function (ownerDetails){
																						
																				//firebase insertion
																				Helperfirestore.createNewConversation(reqParams,conversa_id,ownerDetails,function (firebaseStatus){

																					if(parseInt(firebaseStatus) !== 0)
																					{
																						return_result(callbackResult);
																					}
																					else
																					{return_result(0);}	
																				});
																			});
																		}
																	});
																}
																else
																{return_result(0);}	
															});
														}
														else
														{
															//send push notification
															var push_notification = {        
																group_id: group_id.toString(),
																account_id: reqParams.account_id,
																account_type: reqParams.account_type,
																request_account_id: reqParams.request_account_id,
																request_account_type: reqParams.request_account_type,
																message: reqParams.message,
																flag:1
															};
															
															HelperNotification.sendPushNotification(push_notification,function (pushStatus){
																//reqParams
																return_result(callbackResult);
															});
														}
												}
												else
												{return_result(0);}		
											});
										}
									});
								}
							});
						});		
					}
					else
					{return_result(0);}					
				});
			}
			catch(err)
			{return_result(0);}
		};

		/*
		* save Accept Reject Block Requests Contact 
		*/
		methods.saveAcceptRejectBlockContactRequest =  function(reqParams,return_result){
		
			try
			{
				//if status : 
				// 1 = Accept   (update status conversation table, insert contact table, insert firebase) , 
				// 2 = Reject   (delete conversation table)
				// 0 = Block    (update status conversation table ,  insert contact table)

				var callbackResult = {
					group_id: reqParams.convers_id,
					flag: reqParams.status
				};

				const dateUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

				//get jaze_jazechat_conversation_list group_id

				var query = "  select group_id from jaze_jazechat_conversation_list WHERE meta_key = 'conversa_id' and meta_value = '"+reqParams.convers_id+"' ";

				DB.query(query,null, function(queGrpId, error){

					if(typeof queGrpId !== 'undefined' && queGrpId !== null && parseInt(queGrpId.length) > 0){
						
						var group_id = queGrpId[0].group_id;
	
						if(parseInt(reqParams.status) === 1)
						{
							//Accept
							//remove reason fild
							methods.deleteQuery("jaze_jazechat_conversation_list"," group_id = '"+group_id+"'  and meta_key = 'reason' ",function (deleResult){

								if(parseInt(deleResult) !== 0)
								{
									//update status
									var arrConvUpdate = {
										status: '1',
										update_date : dateUTC.toString(),
									};
									
										methods.updateQuery("jaze_jazechat_conversation_list", group_id,arrConvUpdate,function (updatResult){

											if(parseInt(updatResult) !== 0)
											{
												//save Contact List
												var contact = {
													account_id: reqParams.account_id,
													request_account_id: reqParams.request_account_id,
													flag : 1
												};
												methods.saveReomveJazecomContactList(contact,function (contactGroupId){

													if(parseInt(contactGroupId) !== 0)
													{
														var contacts = {
															account_id: reqParams.request_account_id,
															request_account_id: reqParams.account_id,
															flag : 1
														};

														methods.saveReomveJazecomContactList(contacts,function (contactSRGroupId){

															if(parseInt(contactSRGroupId) !== 0)
															{
																//get conversation Owner Details
																methods.__getConversationOwnerDetails(reqParams.convers_id,function (ownerDetails){
																	//firebase insertion
																	Helperfirestore.createNewConversation(reqParams,reqParams.convers_id,ownerDetails,function (firebaseStatus){
																		
																		if(firebaseStatus !== "")
																		{
																			//update time in last conversation
																			methods.updateConversationChatDate(reqParams.request_account_id,reqParams.convers_id,new Date(firebaseStatus).getTime(),function (fireStatus){
																				
																				if(parseInt(fireStatus) !== 0)
																				{
																					//send push notification Accept
																					var push_notification = {        
																						account_id: reqParams.account_id,
																						account_type: reqParams.account_type,
																						request_account_id: reqParams.request_account_id,
																						request_account_type: 0,
																						group_id: reqParams.convers_id,
																						message: '',
																						flag:2
																					};

																					HelperNotification.sendPushNotification(push_notification,function (pushStatus){
																						return_result(callbackResult);
																					});
																				}
																			});
																		}
																	});
																});
															}
														});
													}
												});
											}
										});
								}
								else
								{return_result(null);}	
							});
						}
						else if(parseInt(reqParams.status) === 2 || parseInt(reqParams.status) === 3)
						{
							//Reject
							methods.deleteQuery("jaze_jazechat_conversation_list"," group_id = '"+group_id+"' ",function (deleResult){

								if(parseInt(deleResult) !== 0)
								{


									var push_notification = {};

									var flag = 17

									if(parseInt(reqParams.status) === 2)
									{
										flag = 3;
										//send push notification Reject
										
									}
								

									var push_notification = {        
										account_id: reqParams.account_id,
										account_type: reqParams.account_type,
										request_account_id: reqParams.request_account_id,
										request_account_type: 0,
										group_id: reqParams.convers_id,
										message: '',
										flag:flag
									};

									HelperNotification.sendPushNotification(push_notification,function (pushStatus){
										//reqParams
										return_result(callbackResult);
									});
								}
								else
								{return_result(null);}	
							});
						}
						else if(parseInt(reqParams.status) === 0)
						{
							//Block
							//remove reason fild
							methods.deleteQuery("jaze_jazechat_conversation_list"," group_id = '"+group_id+"' and meta_key = 'reason' ",function (deleResult){

								if(parseInt(deleResult) !== 0)
								{
									//update status
									var arrConvUpdate = {
										status: '2'
									};

									methods.updateQuery("jaze_jazechat_conversation_list", group_id,arrConvUpdate,function (updatResult){

										if(parseInt(updatResult) !== 0)
										{
											//insert conversation reject fild
											var contact = {
												account_id: reqParams.account_id,
												request_account_id: reqParams.convers_account_id,
												flag : "0"
											};
											methods.saveReomveJazecomContactList(contact,function (contactGroupId){

												if(parseInt(contactGroupId) !== 0)
												{
													return_result(callbackResult);
												}
												else
												{return_result(null);}	
											
											});
										}
										else
										{return_result(null);}	
									});
								}
								else
								{return_result(null);}						
							});
						}
				}
			});
			}
			catch(err)
			{ //return_result(null);
			}
		}


		/*
		* get jazecome Chat Contact List
		*/
		methods.getJazecomChatContactList =  function(reqParams,return_result){

			try
			{
				//first check user have details
				var query = " SELECT meta_key,meta_value FROM jaze_jazecom_contact_list WHERE  meta_key IN ('chat_blocked_account_id','chat_account_id') AND ";
					query += "group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value ='"+reqParams.account_id+"') order by meta_key ";

				DB.query(query,null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var arrgs = [];

						arrgs[data[0].meta_key] = data[0].meta_value;

						arrgs[data[1].meta_key] = data[1].meta_value;
							
						return_result(arrgs);
					}
					else
					{ return_result(null);}
				});
			}
			catch(err)
			{
				console.log(err);
				return_result(null);
			}
		}

		/*
		* save or Reject to jazecome contact table
		*/
		methods.saveReomveJazecomContactList =  function(reqParams,return_result){

			try
			{
				//first check user have details
				var query = " SELECT * FROM jaze_jazecom_contact_list WHERE  group_id  in ( SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value ='"+reqParams.account_id+"'  ) ";
				
				DB.query(query,null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMetaArray(data, function(retrnResult){

							if(retrnResult !== null)
							{
								var meta_fild_key_ins,meta_fild_value_ins = '';
								var meta_fild_key_rem,meta_fild_value_rem = '';

								//check insert or remove
								if(parseInt(reqParams.flag) === 1)
								{	
									meta_fild_key_ins   = "chat_account_id";
									meta_fild_value_ins = retrnResult.chat_account_id;

									meta_fild_key_rem = "chat_blocked_account_id";
									meta_fild_value_rem = retrnResult.chat_blocked_account_id;
								}
								else
								{
									meta_fild_key_ins   = "chat_blocked_account_id";
									meta_fild_value_ins = retrnResult.chat_blocked_account_id;

									meta_fild_key_rem = "chat_account_id";
									meta_fild_value_rem = retrnResult.chat_account_id;
								}

									var query_update = [];

									//remove account id
									if(meta_fild_value_rem != "" && meta_fild_value_rem !== null)
									{
										var list = [];

										list = convert_array(meta_fild_value_rem);

										// 
										if(list.indexOf(reqParams.request_account_id)!= -1)
										{
											list.splice(list.indexOf(reqParams.request_account_id), 1);

											query_update.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+list.join()+"' where meta_key = '"+meta_fild_key_rem+"' and group_id = '"+retrnResult.group_id+"'; ");
										}
									}

									//save account id

									var save_list = [];

									if(meta_fild_value_ins != "" && meta_fild_value_ins !== null)
									{
										save_list = convert_array(meta_fild_value_ins);

										if(save_list.indexOf(reqParams.request_account_id) == -1)
										{
											save_list.push(reqParams.request_account_id);
										}
									}
									else
									{
										save_list.push(reqParams.request_account_id);
									}
										
									query_update.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+save_list.join()+"' where meta_key = '"+meta_fild_key_ins+"' and group_id = '"+retrnResult.group_id+"'; ");
										

									
									if(parseInt(query_update.length) !== 0)
									{
										for( var i in query_update)
										{
											DB.query(query_update[i],null, function(data_check, error){

											});
										}

										return_result(retrnResult.group_id);
									}
							}
						});
					}
					else
					{
						//insert new 
						HelperRestriction.getNewGroupId('jaze_jazecom_contact_list', function (group_id) {
							
							if(parseInt(group_id) !== 0){
							
								var arrConvInsert = {};

								if(parseInt(reqParams.flag) === 1)
								{
									arrConvInsert = {
										account_id: reqParams.account_id,
										mail_account_id: "",
										mail_blocked_account_id: "",
										chat_account_id: reqParams.request_account_id,
										chat_blocked_account_id: "",
										call_account_id: "",
										call_blocked_account_id: "",
										feed_account_id: "",
										feed_blocked_account_id: "",
										diary_account_id: "",
										diary_blocked_account_id: "",
										create_date: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
										status: '1'
									};
								}
								else
								{
									arrConvInsert = {
										account_id: reqParams.account_id,
										mail_account_id: "",
										mail_blocked_account_id: "",
										chat_account_id: "",
										chat_blocked_account_id: reqParams.request_account_id,
										call_account_id: "",
										call_blocked_account_id: "",
										feed_account_id: "",
										feed_blocked_account_id: "",
										diary_account_id: "",
										diary_blocked_account_id: "",
										create_date: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
										status: '1'
									};
								}
								
								HelperRestriction.insertTBQuery('jaze_jazecom_contact_list',group_id, arrConvInsert, function (queResGrpId) {

									return_result(queResGrpId);
								});
							}
							else
							{ return_result(null); }
						});
					}
				});
			}
			catch(err)
			{
				console.log(err);
				return_result(null);
			}
		}

		/*
		* Update Conversation Last Message Date
		*/
		methods.updateConversationChatDate =  function(receiver_account_id,conversa_id,upd_date,return_result){

			try
			{
				var query = "  select group_id from jaze_jazechat_conversation_list WHERE meta_key = 'conversa_id' and meta_value = '"+conversa_id+"' ";

				DB.query(query,null, function(queGrpId, error){

					if(typeof queGrpId !== 'undefined' && queGrpId !== null && parseInt(queGrpId.length) > 0){
						
						var group_id = queGrpId[0].group_id;

						var update_date =dateFormat(new Date(parseInt(upd_date)), 'yyyy-mm-dd HH:MM:ss');

						var query  = "  UPDATE jaze_jazechat_conversation_list SET meta_value = '"+update_date+"' where meta_key = 'update_date' and group_id = '"+group_id+"'; ";

						DB.query(query,null, function(dataUpdate, error){

							var query = "  UPDATE jaze_jazechat_conversation_list SET meta_value = (meta_value + 1) where meta_key = 'unread_count_"+receiver_account_id+"' and group_id = '"+group_id+"'; ";

							DB.query(query,null, function(dataUpdate, error){

								return_result(group_id);

							});	
						});
					}
				});
				
			}
			catch(err)
			{
				console.log(err);
				return_result(0);
			}
		};

		/*
		* Get Mute Details
		*/
		methods.getMuteDetails = function(arrParams,return_result){
		
			try
			{
				var mute_status = {
					mute_status : "0",
					mute_duration : "0",
					mute_time_remaining : "0",
					create_date : "",
					expiry_date : ""
				}

					//check user have mute notification
						var query = " SELECT * FROM jaze_jazechat_conversation_setting WHERE ";
						query +=" meta_key IN ('mute_notification_start','mute_notification_end') AND ";
						query += " `group_id` IN (SELECT `group_id`   FROM jaze_jazechat_conversation_setting  WHERE `meta_key` = 'mute_notification' AND `meta_value` = '1'  AND  ";
						query += " `group_id` IN (SELECT `group_id`   FROM jaze_jazechat_conversation_setting  WHERE `meta_key` = 'account_id' AND `meta_value` = '"+arrParams.account_id+"'  AND  ";
						query += " `group_id` IN (SELECT `group_id` FROM jaze_jazechat_conversation_setting WHERE `meta_key` = 'convers_id' AND meta_value = '"+arrParams.group_id+"'))); ";
				
					DB.query(query, null, function(data, error){
	
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
						{
							HelperRestriction.convertMultiMetaArray(data, function(retrnResult){

								if(typeof retrnResult !== 'undefined' && retrnResult !== null && parseInt(retrnResult.length) > 0)
								{
									const dateUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

									//check time 
									var mute_duration_start = retrnResult[0].mute_notification_start;
									var mute_duration = retrnResult[0].mute_notification_end;

									if(parseInt(mute_duration) !== 0)
									{
										var start_date = new Date(mute_duration_start);
			
										var expiry_date =  start_date.setTime(start_date.getTime() + mute_duration * 60000);
			
										var current_datetime = new Date(dateUTC).getTime();
										
										if(current_datetime < expiry_date)
										{									
											mute_status = {
												mute_status : "1",
												mute_duration : mute_duration.toString(),
												mute_time_remaining : Math.floor((Math.abs(new Date(expiry_date) - new Date(current_datetime))/1000)/60).toString(),
												create_date : dateFormat(mute_duration_start, 'yyyy-mm-dd HH:MM:ss').toString(),
												expiry_date : dateFormat(expiry_date, 'yyyy-mm-dd HH:MM:ss').toString(),
											}
										}
									} 
									else if(parseInt(mute_duration) === 0)
									{
										mute_status = {
											mute_status : "1",
											mute_duration : "0",
											mute_time_remaining : "0",
											create_date : dateFormat(mute_duration_start, 'yyyy-mm-dd HH:MM:ss').toString(),
											expiry_date : dateFormat(expiry_date, 'yyyy-mm-dd HH:MM:ss').toString()
										}
									}
								}
							});
						}
					
						return_result(mute_status);

					});
			}	
			catch(err)
			{
			console.log(err);
			return_result(mute_status);
			}
			}

		/*
		* Save Mute Details
		*/
		methods.saveMuteDetails =  function(reqParams,return_result){

			try
			{
				//first check user have details
				var query = "SELECT group_id FROM jaze_jazechat_conversation_setting WHERE ";
					query += " `group_id` IN (SELECT `group_id`   FROM jaze_jazechat_conversation_setting  WHERE `meta_key` = 'account_id' AND `meta_value` = '"+reqParams.account_id+"'  AND ";
					query += " `group_id` IN (SELECT `group_id`  FROM jaze_jazechat_conversation_setting WHERE `meta_key` = 'convers_id' AND meta_value = '"+reqParams.convers_id+"')) group by group_id ";
			
				DB.query(query,null, function(data, error){
		
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						//update
						var group_id = data[0].group_id;

						//update status
						var	arrConvUpdate = {
							mute_notification	 :	reqParams.mute_status,
							mute_notification_end:	reqParams.mute_duration,
							mute_notification_start	:	moment().utc().format('YYYY-MM-DD HH:mm:ss')
						};
						
						methods.updateQuery("jaze_jazechat_conversation_setting", group_id,arrConvUpdate,function (updatResult){

							if(parseInt(updatResult) !== 0)
							{ return_result(group_id); }
							else 
							{ return_result(0); }

						});
					}
					else
					{
							//insert new 
						HelperRestriction.getNewGroupId('jaze_jazechat_conversation_setting', function (group_id) {

							if(parseInt(group_id) !== 0){

								var	arrConvInsert = {
									convers_id				:	reqParams.convers_id,
									account_id				:	reqParams.account_id,
									mute_notification	 	:	reqParams.mute_status,
									mute_notification_end	:	reqParams.mute_duration,
									mute_notification_start :	moment().utc().format('YYYY-MM-DD HH:mm:ss'),
									convers_start_date 		: "",
									convers_end_date 		: "",
									create_date			 	:	moment().utc().format('YYYY-MM-DD HH:mm:ss'),
									status					: '1'
								};
			

								HelperRestriction.insertTBQuery('jaze_jazechat_conversation_setting',group_id, arrConvInsert, function (queGrpId) {

									return_result(group_id);
								});
							}
						});
					}
				});
			}
			catch(err)
			{
				console.log(err);
				return_result(null);
			}
		}


		/*
		* Save Conversation report Details
		*/
		methods.saveConversationRreportDetails =  function(reqParams,return_result){

			try
			{
				HelperRestriction.getNewGroupId('jaze_jazechat_conversation_report', function (group_id) {

					if(parseInt(group_id) !== 0){

						var	arrConvInsert = {
							convers_id	:	reqParams.convers_id,
							account_id	:	reqParams.account_id,
							report_message:	reqParams.report_message,
							create_date			 :	moment().utc().format('YYYY-MM-DD HH:mm:ss'),
							read_status: '1',
							status: '1'
						};
	

						methods.insertTBQuery('jaze_jazechat_conversation_report',group_id, arrConvInsert, function (queGrpId) {

							return_result(group_id);
							
						});
					}
				});
			}
			catch(err)
			{
				console.log(err);
				return_result(null);
			}
		}



		/*
		 * Get contact block-Unblock Status
		 */
		methods.getContactBlockStatus =  function(reqParams,return_result){

			try
			{
				var status = {
					block_in_status : "0",
					block_out_status : "0"
				};

				//check is owner block or reciver block
				var query  = " SELECT  meta_value FROM  jaze_jazechat_conversation_list  WHERE `meta_key` = 'account_id' AND group_id in (SELECT `group_id`  FROM jaze_jazechat_conversation_list WHERE `meta_key` = 'conversa_id' AND meta_value = '"+reqParams.convers_id+"') ";

					DB.query(query,null, function(data, error){
						
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							//get status
							var query  = " SELECT  * FROM  jaze_jazechat_conversation_block_status  WHERE group_id in (SELECT `group_id`  FROM jaze_jazechat_conversation_block_status WHERE `meta_key` = 'convers_id' AND meta_value = '"+reqParams.convers_id+"') ";

							DB.query(query,null, function(dataStatus, error){
					
								if(typeof dataStatus !== 'undefined' && dataStatus !== null && parseInt(dataStatus.length) > 0){
									
									HelperRestriction.convertMetaArray(dataStatus, function(statusRes){

										//checking is owner or not 
										if(parseInt(reqParams.account_id) === parseInt(data[0].meta_value))
										{ 
											status = {
												block_in_status : statusRes.receiver_status.toString(),
												block_out_status : statusRes.owner_status.toString()
											};
										}
										else
										{
											status = {
												block_in_status : statusRes.owner_status.toString(),
												block_out_status : statusRes.receiver_status.toString()
											};
										}
									
										return_result(status); 

									});
								}
								else
								{ return_result(status); }
							});
						}
						else
						{ return_result(status); }
					});
			}
			catch(err)
			{
				console.log(err);
				return_result(null);
			}
		}

		/*
		 * Get Conversation block Status
		 */
		methods.getConversationBlockStatus =  function(convers_id,account_id,return_result){

			try
			{
				var status = "0";

				//check is owner block or reciver block
				var query  = " SELECT  meta_value FROM  jaze_jazechat_conversation_list  WHERE `meta_key` = 'account_id' AND group_id in (SELECT `group_id`  FROM jaze_jazechat_conversation_list WHERE `meta_key` = 'conversa_id' AND meta_value = '"+convers_id+"') ";

					DB.query(query,null, function(data, error){
					
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							//get status
							var query  = " SELECT  * FROM  jaze_jazechat_conversation_block_status  WHERE group_id in (SELECT `group_id`  FROM jaze_jazechat_conversation_block_status WHERE `meta_key` = 'convers_id' AND meta_value = '"+convers_id+"') ";

							DB.query(query,null, function(dataStatus, error){
					
								if(typeof dataStatus !== 'undefined' && dataStatus !== null && parseInt(dataStatus.length) > 0){

									HelperRestriction.convertMetaArray(dataStatus, function(statusRes){

										//checking is owner or not 
										if(account_id === data[0].meta_value)
										{ 
											if(parseInt(statusRes.owner_status) === 1)
											{
												status = "1";
											}
										}
										else
										{
											if(parseInt(statusRes.receiver_status) === 1)
											{
												status = "1";
											}
										}

										return_result(status); 
									});
								}
								else
								{ return_result(status); }
							});
						}
						else
						{ return_result(status); }
					});
			}
			catch(err)
			{
				console.log(err);
				return_result(null);
			}
		}


		/*
		 * Save contact block Unblock
		 */
		methods.saveContactBlockUnblock =  function(reqParams,return_result){
			
			try
			{
				var query  = " SELECT  meta_value FROM  jaze_jazechat_conversation_list  WHERE `meta_key` = 'account_id' AND group_id in (SELECT `group_id`  FROM jaze_jazechat_conversation_list WHERE `meta_key` = 'conversa_id' AND meta_value = '"+reqParams.convers_id+"') ";

					DB.query(query,null, function(data, error){
					
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							var admin_status = 0;

							if(reqParams.account_id === data[0].meta_value)
							{  admin_status = 1; }


							//check already exisit in status table
							var query  = " SELECT  * FROM  jaze_jazechat_conversation_block_status  WHERE group_id in (SELECT `group_id`  FROM jaze_jazechat_conversation_block_status WHERE `meta_key` = 'convers_id' AND meta_value = '"+reqParams.convers_id+"') ";

							DB.query(query,null, function(dataStatus, error){
								
								if(typeof dataStatus !== 'undefined' && dataStatus !== null && parseInt(dataStatus.length) > 0){
									
									HelperRestriction.convertMetaArray(dataStatus, function(statusRes){

										//update
										var group_id = statusRes.group_id;

										var	arrUpdate = {};

										//chcek its owner or not 
										if(parseInt(admin_status) === 1)
										{
											if(parseInt(statusRes.owner_status) === 1)
											{
												arrUpdate = {
													owner_status	 :	'0'
												};
											}
											else
											{
												arrUpdate = {
													owner_status	 :	'1'
												};
											}
										}
										else
										{
											if(parseInt(statusRes.receiver_status) === 1)
											{
												arrUpdate = {
													receiver_status	 :	'0'
												};
											}
											else
											{
												arrUpdate = {
													receiver_status	 :	'1'
												};
											}
										}
										
										
										var quer = " UPDATE  jaze_jazechat_conversation_block_status  SET    meta_value = '"+Object.values(arrUpdate)+"' where meta_key = '"+Object.keys(arrUpdate)+"' and  group_id =  '"+group_id+"';";
										
										DB.query(quer,null, function(data, error){
											
											if(data !== null)
											{
												methods.getContactBlockStatus(reqParams, function(block_status){
														
													return_result(block_status);

												});
											}
										});
									});
								}
								else
								{
									
									//insert
									HelperRestriction.getNewGroupId('jaze_jazechat_conversation_block_status', function (group_id) {

										if(parseInt(group_id) !== 0){

											var	arrInsert = {};

											if(parseInt(admin_status)  === 1)
											{
												//owner
												arrInsert = {
													convers_id		 :	reqParams.convers_id,
													owner_status	 :	'1',
													receiver_status :	'0',
													status: '1'
												};
											}
											else 
											{
												arrInsert = {
													convers_id		 :	reqParams.convers_id,
													owner_status	 :	'0',
													receiver_status :	'1',
													status: '1'
												};
											}

											methods.insertTBQuery('jaze_jazechat_conversation_block_status',group_id, arrInsert, function (queGrpId) {

												methods.getContactBlockStatus(reqParams, function(block_status){
													return_result(block_status);
												});
											});
										}
									});
								}
							});

						}
					});
			}
			catch(err)
			{
				console.log(err);
				return_result(null);
			}
		}


		/*
		 *
		 */
		methods.sendPushBlockContact =  function(reqParams,block_status,return_result){

			try{

				var query  = " SELECT  * FROM  jaze_jazechat_conversation_list  WHERE  ";
				    query += " `group_id` IN (SELECT `group_id` FROM jaze_jazechat_conversation_list WHERE `meta_key` = 'conversa_id' AND meta_value = '"+reqParams.convers_id+"') ";
				
			
					DB.query(query,null, function(data, error){
					
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							HelperRestriction.convertMetaArray(data, function(statusRes){

								if(statusRes !== null)
								{
									var flag = 0;

									if(reqParams.account_id === statusRes.account_id)
									{  flag = 1; }


									//send push notification
									var push_notification = {};

									var block_flag;

									if(parseInt(block_status) === 1)
									{  block_flag = 10; }
									else 
									{  block_flag = 11; }

									if(flag === 1)
									{
										// owner to reciver
										push_notification = {     
											group_id: reqParams.convers_id.toString(),     
											account_id: statusRes.account_id,
											account_type: statusRes.account_type,
											request_account_id: statusRes.contact_account_id,
											request_account_type: statusRes.contact_account_type,
											message: "",
											flag:block_flag
										};
									}
									else
									{
										// reciver to owner 
										push_notification = {    
											group_id: reqParams.convers_id.toString(),   
											account_id: statusRes.contact_account_id,
											account_type: statusRes.contact_account_type,
											request_account_id: statusRes.account_id,
											request_account_type: statusRes.account_type,
											message: "",
											flag:block_flag
										};
									}
									//update contact 
									var arrOwnerJazeContact = {
										account_id: push_notification.account_id,
										request_account_id : push_notification.request_account_id,
										flag :  (parseInt(block_flag) - 10).toString()
									};

									methods.saveReomveJazecomContactList(arrOwnerJazeContact,function (resultJazeOwner){
										
										HelperNotification.sendPushNotification(push_notification,function (pushStatus){
											//reqParams
											return_result(pushStatus);
										});
									});
								}
							});
						}
					});
			}
			catch(err)
			{
				console.log(err);
				return_result(null);
			}
		}


		/*
		* Get Conversation Start and End Date Details
		*/
		methods.getStartandEndDateDetails = function(group_id,account_id,return_result){
		
			try
			{
				var conv_date_details = {
					clear_flag: "0",
					conv_start_date: "",
					conv_end_date: ""
				}

					//check user have mute notification
						var query = " SELECT meta_key,meta_value FROM jaze_jazechat_conversation_setting WHERE ";
						query +=" meta_key IN ('convers_start_date','convers_end_date') AND ";
						query += " `group_id` IN (SELECT `group_id`   FROM jaze_jazechat_conversation_setting  WHERE `meta_key` = 'account_id' AND `meta_value` = '"+account_id+"'  AND  ";
						query += " `group_id` IN (SELECT `group_id` FROM jaze_jazechat_conversation_setting WHERE `meta_key` = 'convers_id' AND meta_value = '"+group_id+"')) order by meta_key desc; ";
				
						
					DB.query(query, null, function(data, error){
	
						if (typeof data[0] !== 'undefined' && data[0] !== null)
						{
							if (data[0]['meta_value'] !== "" || data[1]['meta_value'] !== "")
							{
								conv_date_details = {
									clear_flag: "1",
									conv_start_date:  data[0]['meta_value'],
									conv_end_date:  data[1]['meta_value']
								}
							}
						}

						return_result(conv_date_details);

					});
			}	
			catch(err)
			{
			console.log(err);
			return_result(conv_date_details);
			}
		}

		/*
		* Save Conversation Start and End Date Details
		*/
		methods.saveStartandEndDateDetails =  function(reqParams,return_result){

			try
			{
				if(parseInt(reqParams.convers_flag) === 1)
				{
					//single conversation
						//first check user have details
					var query = "SELECT group_id FROM jaze_jazechat_conversation_setting WHERE ";
					query += " `group_id` IN (SELECT `group_id`   FROM jaze_jazechat_conversation_setting  WHERE `meta_key` = 'account_id' AND `meta_value` = '"+reqParams.account_id+"'  AND ";
					query += " `group_id` IN (SELECT `group_id`  FROM jaze_jazechat_conversation_setting WHERE `meta_key` = 'convers_id' AND meta_value = '"+reqParams.convers_id+"')) group by group_id ";
					
				
					DB.query(query,null, function(data, error){

					
						//get last message from
						var query = " SELECT meta_value FROM jaze_jazechat_conversation_list WHERE meta_key IN ('update_date') AND `group_id`  IN (SELECT `group_id`  FROM jaze_jazechat_conversation_list WHERE `meta_key` = 'conversa_id' AND meta_value = '"+reqParams.convers_id+"') ";
						
						DB.query(query,null, function(dataDate, error){

							if(typeof dataDate !== 'undefined' && dataDate !== null && parseInt(dataDate.length) > 0){ 

								var update_date = dataDate[0].meta_value;
								
								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

									//update
									var group_id = data[0].group_id;

									//update status
									var	arrConvUpdate = {
										convers_end_date	:	update_date
									};
									
									methods.updateQuery("jaze_jazechat_conversation_setting", group_id,arrConvUpdate,function (updatResult){

										var conv_date_details = {
											clear_flag: "1",
											conv_start_date: "",
											conv_end_date: update_date
										}
										
										return_result(conv_date_details);

									});
								}
								else
								{
									//insert new 
									HelperRestriction.getNewGroupId('jaze_jazechat_conversation_setting', function (group_id) {

										if(parseInt(group_id) !== 0){

											var	arrConvInsert = {
												convers_id				:	reqParams.convers_id,
												account_id				:	reqParams.account_id,
												mute_notification	 	:	"",
												mute_notification_end	:	"",
												mute_notification_start :	"",
												convers_start_date 		: 	"",
												convers_end_date 		: 	update_date,
												create_date			 	:	moment().utc().format('YYYY-MM-DD HH:mm:ss'),
												status					: 	'1'
											};
						

											methods.insertTBQuery('jaze_jazechat_conversation_setting',group_id, arrConvInsert, function (queGrpId) {

												var conv_date_details = {
													clear_flag: "1",
													conv_start_date: "",
													conv_end_date: arrConvInsert.convers_end_date
												}
					
												return_result(conv_date_details);
											});
										}
									});
								}
							}
						});
					});
				}
				else if(parseInt(reqParams.convers_flag) === 2)
				{
					//group conversation

				}
				else if(parseInt(reqParams.convers_flag) === 3)
				{
					//broadcast conversation

						//first check user have details
						var query = "SELECT group_id FROM jaze_jazechat_conversation_setting WHERE ";
						query += " `group_id` IN (SELECT `group_id`   FROM jaze_jazechat_conversation_setting  WHERE `meta_key` = 'account_id' AND `meta_value` = '"+reqParams.account_id+"'  AND ";
						query += " `group_id` IN (SELECT `group_id`  FROM jaze_jazechat_conversation_setting WHERE `meta_key` = 'convers_id' AND meta_value = '"+reqParams.convers_id+"')) ";
						
					
						DB.query(query,null, function(data, error){
	
							//get last message from
							var query = " SELECT meta_value FROM jaze_jazechat_broadcasting_list WHERE meta_key IN ('update_date') AND `group_id`  IN (SELECT `group_id`  FROM jaze_jazechat_broadcasting_list WHERE `meta_key` = 'conversa_id' AND meta_value = '"+reqParams.convers_id+"') ";
							
							DB.query(query,null, function(dataDate, error){
	
								if(typeof dataDate !== 'undefined' && dataDate !== null && parseInt(dataDate.length) > 0){ 
	
									var update_date = dataDate[0].meta_value;
	
									if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
										//update
										var group_id = data[0].group_id;
	
										//update status
										var	arrConvUpdate = {
											convers_end_date	:	update_date
										};
										
										methods.updateQuery("jaze_jazechat_conversation_setting", group_id,arrConvUpdate,function (updatResult){
	
											var conv_date_details = {
												clear_flag: "1",
												conv_start_date: "",
												conv_end_date: update_date
											}
	
											return_result(conv_date_details);
	
										});
									}
									else
									{
										//insert new 
										HelperRestriction.getNewGroupId('jaze_jazechat_conversation_setting', function (group_id) {
	
											if(parseInt(group_id) !== 0){
	
												var	arrConvInsert = {
													convers_id				:	reqParams.convers_id,
													account_id				:	reqParams.account_id,
													mute_notification	 	:	"",
													mute_notification_end	:	"",
													mute_notification_start :	"",
													convers_start_date 		: 	"",
													convers_end_date 		: 	update_date,
													create_date			 	:	moment().utc().format('YYYY-MM-DD HH:mm:ss'),
													status					: 	'1'
												};
							
	
												methods.insertTBQuery('jaze_jazechat_conversation_setting',group_id, arrConvInsert, function (queGrpId) {
	
													var conv_date_details = {
														clear_flag: "1",
														conv_start_date: "",
														conv_end_date: arrConvInsert.convers_end_date
													}
						
													return_result(conv_date_details);
												});
											}
										});
									}
								}
							});
						});
				}



				
			}
			catch(err)
			{
				console.log(err);
				return_result(null);
			}
		}


		methods.getForwardConversationList = function(conv_array,return_result){

			if(conv_array !== null)
			{
				var list = [];
													
				list = conv_array.receiv_convers_id;
				
				var counter = list.length;

				//console.log("tot counter",counter);
		
				var array_rows = [];
						
				if(parseInt(counter) !== 0)
				{
					promiseForeach.each(list,[function (list) {
			
						return new Promise(function (resolve, reject) {

							if(parseInt(list.convers_flag) === 1 || parseInt(list.convers_flag) === 2)
							{  
									array_rows.push(list.conversation_id);
									counter -= 1;
									resolve(array_rows);
							}
							else if(parseInt(list.convers_flag) === 3)
							{
								//get conversation list
								var query  = " SELECT meta_value FROM jaze_jazechat_broadcasting_list WHERE meta_key = 'members_convers_id' and  group_id IN(SELECT group_id FROM jaze_jazechat_broadcasting_list WHERE meta_key ='status' AND meta_value = '1' ";
									query  += " and  group_id IN(SELECT group_id FROM jaze_jazechat_broadcasting_list WHERE meta_key = 'conversa_id' AND meta_value  = '"+list.conversation_id+"'))  ";

									DB.query(query,null, function(data, error){
										
										if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

											var listmembes = [];
																
											listmembes = data[0].meta_value.split(",");

											listmembes.push(list.conversation_id);

											array_rows = array_rows.concat(listmembes);
											counter -= 1;
											resolve(array_rows);	
										}			
									});
							}
						})
					}],
					function (result, current) {
						//console.log("count : "+counter);
						if (counter === 0)
						{  return_result(result[0]);}
					});
				}
			}
		} 

		/*
		* Save Conversation Start and End Date Details
		*/
		methods.setMessageReadStatus =  function(reqParams,return_result){

			try
			{
				if(parseInt(reqParams.convers_flag) === 1)
				{
					//single
					//first check user have details
					var query = "SELECT group_id FROM jaze_jazechat_conversation_list WHERE ";
						query += " `group_id` IN (SELECT `group_id`  FROM jaze_jazechat_conversation_list WHERE `meta_key` = 'conversa_id' AND meta_value = '"+reqParams.convers_id+"') ";
						
					DB.query(query,null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){ 

							var group_id = data[0].group_id;

							if(parseInt(group_id) !== 0)
							{
								var	arrConvUpdate = {
									["unread_count_"+reqParams.account_id+""] : "0"
								};

								methods.updateQuery("jaze_jazechat_conversation_list", group_id,arrConvUpdate,function (updatResult){

									
									return_result(updatResult);

								});
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }
					});
				}
				else if(parseInt(reqParams.convers_flag) === 2)
				{
						//group
						return_result(1);
				}
				else if(parseInt(reqParams.convers_flag) === 3)
				{
					//broadcasting
					return_result(1);
				}
			}
			catch(err)
			{
				console.log(err);
				return_result(null);
			}
		}

		/*
		 * Get Conversation Owner Details
		 */
		methods.__getConversationOwnerDetails = function(convers_id,return_result){

			if(parseInt(convers_id) !== 0)
			{
				var query  = " SELECT * FROM jaze_jazechat_conversation_list WHERE meta_key in ('account_id','account_type') ";
					query  += " and  group_id IN(SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'conversa_id' AND meta_value  = '"+convers_id+"')  ";

					DB.query(query,null, function(data, error){
					
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							HelperRestriction.convertMetaArray(data, function(statusRes){

								var obj_return = {
									account_id   : statusRes.account_id.toString(),
									account_type : statusRes.account_type.toString(),
								};

								return_result(obj_return);

							});
						}
						else
						{ return_result(null); }
					});
			}
		} 
	//------------------------------------------------------------------- Conversation Module End ---------------------------------------------------------------------------//

	//------------------------------------------------------------------- broadcasting Module Start ---------------------------------------------------------------------------//

		/*
		* get Broadcast details
		*/
		methods.getBroadcastDetails =  function(conversa_id,return_result){

			try
			{
				if(parseInt(conversa_id) !== 0)
				{
					// get Group_id
					var query =  " SELECT ";
						query += " (SELECT a.meta_value FROM jaze_jazechat_broadcasting_list a WHERE a.meta_key = 'broadcast_name' AND a.group_id = jaze_jazechat_broadcasting_list.group_id) AS broadcast_name,  ";
						query += " (SELECT b.meta_value FROM jaze_jazechat_broadcasting_list b WHERE b.meta_key = 'broadcast_logo' AND b.group_id = jaze_jazechat_broadcasting_list.group_id) AS broadcast_logo,  ";
						query += " (SELECT c.meta_value FROM jaze_jazechat_broadcasting_list c WHERE c.meta_key = 'members_convers_id' AND c.group_id = jaze_jazechat_broadcasting_list.group_id) AS members_list  ";
						query += " FROM  jaze_jazechat_broadcasting_list ";
						query += " WHERE group_id IN(SELECT group_id FROM jaze_jazechat_broadcasting_list WHERE meta_key = 'conversa_id' AND meta_value = '"+conversa_id+"')  group by group_id; ";
					

					DB.query(query,null, function(data, error){
					
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					
							return_result(data[0]); 
						}
						else
						{ return_result(null);}
					});
				}
				else
				{return_result(null);}
			}
			catch(err)
			{return_result(1);}
		};

		/*
		* check Broadcast name exist
		*/
		methods.checkBroadcastNameExist =  function(reqParams,return_result){

			try
			{
					// get Group_id
					var query = " SELECT GROUP_CONCAT(meta_value) as name FROM jaze_jazechat_broadcasting_list WHERE meta_key = 'broadcast_name' and  group_id IN(SELECT group_id FROM jaze_jazechat_broadcasting_list WHERE meta_key ='status' AND meta_value = '1' "; 
						query += "and  group_id IN(SELECT group_id FROM jaze_jazechat_broadcasting_list WHERE meta_key = 'account_id' AND meta_value = '"+reqParams.account_id+"'))  ";

					DB.query(query,null, function(data, error){
					
						var list_broadcast_name = data[0].name;
					
						if(typeof list_broadcast_name !== 'undefined' && list_broadcast_name !== null && parseInt(list_broadcast_name.length) > 0){
		
							var result = [];

							if(list_broadcast_name.toString().indexOf(',') != -1 ){
								result = list_broadcast_name.split(',');
							}
							else {
								result.push(list_broadcast_name);
							}

							if(result.indexOf(reqParams.broadcast_name)  === -1)
							{
								return_result(1);
							}
							else
							{ 
								//get conversation id
								var query = " SELECT meta_value FROM jaze_jazechat_broadcasting_list WHERE `meta_key`='conversa_id' AND `group_id` IN(SELECT group_id FROM jaze_jazechat_broadcasting_list WHERE meta_key = 'broadcast_name' and `meta_value` = '"+reqParams.broadcast_name+"');"

								DB.query(query,null, function(data, error){
					
									if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					
										return_result(data[0].meta_value); 
									}
								});
							}
						}
						else
						{ return_result(1);}
					});
			}
			catch(err)
			{return_result(1);}
		};

		/*
		* save New Broadcasting Details
		*/
		methods.saveNewBroadcastingDetails =  function(reqParams,return_result){

			try
			{
					// get Group_id
					HelperRestriction.getNewGroupId('jaze_jazechat_broadcasting_list', function (group_id) {

					if(parseInt(group_id) !== 0){

						//get latest convers id

						methods.getLatestConversId(function(conversa_id) {

							var callbackResult = {
								group_id: conversa_id
							};	

							//broadcast details save to tb
							var arrbroadcastInsert = {
								account_id	  : reqParams.account_id,
								broadcast_name: reqParams.broadcast_name,
								broadcast_logo: G_STATIC_IMG_URL+"uploads/jazenet/common_files/broadcast_default.png",
								conversa_id: conversa_id,
								members_convers_id: reqParams.members_convers_id,
								create_date: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
								update_date: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
								status: '1'
							};

								methods.insertTBQuery('jaze_jazechat_broadcasting_list',group_id, arrbroadcastInsert, function (queResGrpId) {

									if (parseInt(queResGrpId) !== 0) {

										//save to jazechat contact tb

										var arrOwnerContact = {
											account_id: reqParams.account_id,
											single_convers_id : "",
											group_convers_id : "",
											broadcasting_convers_id : conversa_id,
											flag: '1'
										};
										
										methods.saveJazechatContactList(arrOwnerContact,function (resultOwner){

											if(resultOwner !== null)
											{
												//firebase insertion
												Helperfirestore.createNewBroadcasting(reqParams,conversa_id,function (firebaseStatus){

													if(parseInt(firebaseStatus) !== 0)
													{
														//update brodcast latest time
														methods.updateBroadcastChatDate(conversa_id,new Date(firebaseStatus).getTime(), function(returnStatus){ 

															return_result(callbackResult);
														});	
													}
													else
													{return_result(0);}	
												});
											}
											else
											{return_result(0);}	
										});
								}
								else
								{return_result(0);}		
							});

						});		
					}
					else
					{return_result(0);}					
				});
			}
			catch(err)
			{return_result(0);}
		};

		/*
		* Get Broadcast Group id
		*/
		methods.getBroadcastGroupId =  function(reqParams,return_result){

			try
			{
				// checking Broadcast valid or not
				var query = "SELECT `group_id` FROM jaze_jazechat_broadcasting_list WHERE `meta_key` = 'conversa_id' AND `meta_value` = '"+reqParams.convers_id+"' ";

				DB.query(query,null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
						var group_id = data[0].group_id;

						if(parseInt(group_id) !== 0)
						{ return_result(group_id); }	
						else
						{return_result(0);}		
					}
					else
					{return_result(0);}					
				});
			}
			catch(err)
			{return_result(0);}
		};

		/*
		* update Broadcasting Details
		*/
		methods.updateBroadcastName =  function(group_id,reqParams,return_result){

			try
			{
				if(parseInt(group_id) !== 0 && reqParams.broadcast_name !== "")
				{
					var query = " UPDATE jaze_jazechat_broadcasting_list set meta_value = '"+reqParams.broadcast_name+"' WHERE meta_key = 'broadcast_name' and group_id = '"+group_id+"'; ";

					DB.query(query,null, function(data, error){
					
						if(data !== null){
						
							return_result(group_id);
						}
						else
						{return_result(0);}	
					});
				}
				else
				{return_result(1);}	
			}
			catch(err)
			{return_result(0);}
		};

		/*
		* update Broadcasting Details
		*/
		methods.updateBroadcastingLogo =  function(group_id,reqParams,return_result){

			try
			{
				if(parseInt(group_id) !== 0 && reqParams.broadcast_logo !== "")
				{
					var query = " UPDATE jaze_jazechat_broadcasting_list set meta_value = '"+reqParams.broadcast_logo+"' WHERE meta_key = 'broadcast_logo' and group_id = '"+group_id+"'; ";

					DB.query(query,null, function(data, error){
					
						if(data !== null){
						
							return_result(group_id);
						}
						else
						{return_result(0);}	
					});
				}
				else
				{return_result(1);}	
			}
			catch(err)
			{return_result(0);}
		};

		/*
		* delete Broadcasting Details
		*/
		methods.deleteBroadcastingDetails =  function(reqParams,return_result){

			try
			{
				// checking Broadcast valid or not
				var query = "SELECT `group_id` FROM jaze_jazechat_broadcasting_list WHERE `meta_key` = 'conversa_id' AND `meta_value` = '"+reqParams.convers_id+"' ";

				DB.query(query,null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
						var group_id = data[0].group_id;
					
						if(parseInt(group_id) !== 0)
						{
							//generate query

							var query = "DELETE FROM `jaze_jazechat_broadcasting_list` WHERE `group_id` = '"+group_id+"' ";

							DB.query(query,null, function(data_update, error){

									// remove to jazechat contact tb

									var arrOwnerContact = {
										account_id: reqParams.account_id,
										single_convers_id : "",
										group_convers_id : "",
										broadcasting_convers_id : reqParams.convers_id,
										flag: '0'
									};
									
									methods.saveJazechatContactList(arrOwnerContact,function (resultOwner){

										if(resultOwner !== null)
										{
											//firebase deletion
											Helperfirestore.deleteBroadcasting(reqParams,function (firebaseStatus){

												if(parseInt(firebaseStatus) !== 0)
												{
													return_result(1);
												}
												else
												{return_result(0);}	
											});
										}
										else
										{return_result(0);}	
									});
							});								
						}	
						else
						{return_result(0);}		
					}
					else
					{return_result(0);}					
				});
			}
			catch(err)
			{return_result(0);}
		};

		/*
		* get Broadcasting Members List
		*/
		methods.getBroadcastMembersConversList =  function(reqParams,return_result){

			try
			{
				if(parseInt(reqParams.convers_flag) === 3)
				{
					// checking Broadcast valid or not
					var query  = " SELECT meta_value FROM jaze_jazechat_broadcasting_list WHERE meta_key = 'members_convers_id' and  group_id IN(SELECT group_id FROM jaze_jazechat_broadcasting_list WHERE meta_key ='status' AND meta_value = '1' ";
						query  += " and  group_id IN(SELECT group_id FROM jaze_jazechat_broadcasting_list WHERE meta_key = 'conversa_id' AND meta_value  = '"+reqParams.convers_id+"'))  ";

					DB.query(query,null, function(data, error){
						
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							var list = [];
												
							list = data[0].meta_value.split(",");

							if(list !== null)
							{
								return_result(list);
							}	
							else
							{return_result(null);}		
						}
						else
						{return_result(null);}					
					});
				}
				else
				{return_result(null);}
			}
			catch(err)
			{return_result(null);}
		};

		/*
		* get Broadcasting Members List
		*/
		methods.getBroadcastMembersList =  function(reqParams,return_result){

			try
			{
				// checking Broadcast valid or not
				var query  = " SELECT meta_value FROM jaze_jazechat_broadcasting_list WHERE meta_key = 'members_convers_id' and  group_id IN(SELECT group_id FROM jaze_jazechat_broadcasting_list WHERE meta_key ='status' AND meta_value = '1' ";
					query  += " and  group_id IN(SELECT group_id FROM jaze_jazechat_broadcasting_list WHERE meta_key = 'conversa_id' AND meta_value  = '"+reqParams.convers_id+"'))  ";

				DB.query(query,null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var list = [];
											
						list = data[0].meta_value.split(",");

						if(list !== null)
						{
							var counter = list.length;

							var array_rows = [];

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(list,[function (list) {

									return new Promise(function (resolve, reject) {

										methods.__getConversationAccountDetails(list,reqParams.account_id,function (account_result){

													var result_obj = {
														member_id:list.toString(),
														account_id: account_result.account_id.toString(),
														account_type: account_result.account_type.toString(),
														name: account_result.name.toString(),
														logo: account_result.logo.toString(),
														category: account_result.category.toString(),
														master_status: '0'
													};
				
													array_rows.push(result_obj);

													counter -= 1;

													resolve(array_rows);
										});
									})
								}],
								function (result, current) {
									//console.log("count : "+counter);
									if (counter === 0)
									{  return_result(result[0]);}
								});
							}	
							else
							{return_result(null);}	
						}	
						else
						{return_result(null);}		
					}
					else
					{return_result(null);}					
				});
			}
			catch(err)
			{return_result(null);}
		};

		/*
		* get Conversation Receiver Details
		*/
		methods.__getConversationAccountDetails = function(conversation_id,account_id,return_result){

			try
			{
				if(parseInt(conversation_id) !== 0 && parseInt(account_id) !== 0)
				{
					let query  = " SELECT (SELECT b.meta_value FROM jaze_jazechat_conversation_list b WHERE b.meta_key = 'conversa_id' AND b.group_id = jaze_jazechat_conversation_list.group_id) AS convers_id, ";
							query += " (SELECT a.meta_value FROM jaze_jazechat_conversation_list a WHERE a.meta_key = 'contact_account_id' AND a.group_id = jaze_jazechat_conversation_list.group_id) AS contact_account_id, ";
							query += " (SELECT g.meta_value FROM jaze_jazechat_conversation_list g WHERE g.meta_key = 'account_id' 		AND g.group_id = jaze_jazechat_conversation_list.group_id) AS account_id ";
							query += " FROM  jaze_jazechat_conversation_list  ";
							query += " WHERE group_id IN(SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'conversa_id' AND meta_value = '"+conversation_id+"')  group by group_id;  ";		

							
						DB.query(query,null, function(data, error){
						
							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

								let arrAccountParams = {};

								if(parseInt(account_id) === parseInt(data[0].contact_account_id))
								{ arrAccountParams = { account_id:data[0].account_id }; }
								else
								{ arrAccountParams = { account_id:data[0].contact_account_id }; }
		
								HelperGeneral.getAccountDetails(arrAccountParams, function(accDetails){	

									return_result(accDetails);
								});
							}
						});
				}
				else
				{ return_result(null); }
			}
			catch(err)
			{ console.log("__getConversationAccountDetails",err); return_result(null);}
		}; 


		/*
		* update Broadcasting Members List
		*/
		// Want rework
		methods.updateBroadcastMembersList =  function(reqParams,return_result){

			try
			{
				// checking Broadcast valid or not
				var query = "SELECT `group_id` FROM jaze_jazechat_broadcasting_list WHERE `meta_key` = 'conversa_id' AND `meta_value` = '"+reqParams.convers_id+"' ";

				DB.query(query,null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
						var group_id = data[0].group_id;

						if(parseInt(group_id) !== 0)
						{
							var list = [];

							if(reqParams.members_list.toString().indexOf(',') != -1 ){
								list = reqParams.members_list.split(',');
							}
							else {
								list.push(reqParams.members_list);
							}

							if(list !== null)
							{
								if(parseInt(reqParams.flag) === 1)
								{
									//insert members
									list.forEach(function(members) {

										var query_update = " UPDATE jaze_jazechat_broadcasting_list set meta_value = concat(meta_value, ',"+members+"')  WHERE meta_key = 'members_convers_id' and group_id = '"+group_id+"'; ";
											
										DB.query(query_update,null, function(data_update, error){
											return_result(1);
										});
									});
								}
								else if(parseInt(reqParams.flag) === 0)
								{
									//remove members
	
										//get result
										var query_sel = "SELECT meta_value FROM jaze_jazechat_broadcasting_list WHERE meta_key = 'members_convers_id' and group_id = '"+group_id+"';  ";
	
										DB.query(query_sel,null, function(data_update, error){

											if(typeof data_update !== 'undefined' && data_update !== null && parseInt(data_update.length) > 0){

												var members_list = [];

												members_list = data_update[0].meta_value.split(",");

												list.forEach(function(members) {	

													members_list.splice(members_list.indexOf(members), 1);
												});

												if(members_list !== null)
												{
													var query_update = "  UPDATE jaze_jazechat_broadcasting_list SET meta_value = '"+members_list.join()+"' where meta_key = 'members_convers_id' and group_id = '"+group_id+"'; ";
		
													DB.query(query_update,null, function(data_update, error){
														return_result(1);
													});
												}
											}
										});
								}
								else
								{return_result(0);}	
							}
							else
							{return_result(0);}	
						}	
						else
						{return_result(0);}		
					}
					else
					{return_result(0);}					
				});
			}
			catch(err)
			{return_result(0);}
		};

		
		/*
		* Update Broadcast Last Message Date
		*/
		methods.updateBroadcastChatDate =  function(conversation_id,upd_date,return_result){

			try
			{
				if(parseInt(conversation_id) !== 0 && upd_date !== "")
				{ 

						var query = "  select group_id from jaze_jazechat_broadcasting_list WHERE meta_key = 'conversa_id' and meta_value = '"+conversation_id+"' ";

						DB.query(query,null, function(queGrpId, error){
		
							if(typeof queGrpId !== 'undefined' && queGrpId !== null && parseInt(queGrpId.length) > 0){
								
								var group_id = queGrpId[0].group_id;
		
								var update_date =dateFormat(new Date(parseInt(upd_date)), 'yyyy-mm-dd HH:MM:ss');
		
								var query = "  UPDATE jaze_jazechat_broadcasting_list SET meta_value = '"+update_date+"' where meta_key = 'update_date' and group_id = '"+group_id+"'; ";
		
								DB.query(query,null, function(dataUpdate, error){
		
								});	
							}
						});
					
					return_result(1);
				}
			}
			catch(err)
			{
				console.log(err);
				return_result(0);
			}
		}	
		
		/*
		* Check Broadcast List Members Block Status
		*/
		methods.checkBroadcastMembersBlockStatus =  function(memberslist,return_result){

			try
			{
				if(memberslist != null)
				{
							var counter = memberslist.length;
							var array_rows = [];

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(memberslist,[function (memberslist) {

									return new Promise(function (resolve, reject) {

										//get status
										var query  = " SELECT  (SELECT a.meta_value FROM jaze_jazechat_conversation_block_status a WHERE a.meta_key = 'owner_status' AND a.group_id = jaze_jazechat_conversation_block_status.group_id) AS owner_status, ";
										query += " (SELECT b.meta_value FROM jaze_jazechat_conversation_block_status b WHERE b.meta_key = 'receiver_status' AND b.group_id = jaze_jazechat_conversation_block_status.group_id) AS receiver_status  ";
										query += " FROM  jaze_jazechat_conversation_block_status  WHERE ";
										query += " group_id in (SELECT group_id FROM jaze_jazechat_conversation_block_status  WHERE meta_key = 'convers_id' AND  meta_value = '"+memberslist.member_id+"') group by group_id ";
										
										DB.query(query,null, function(dataStatus, error){

											if(typeof dataStatus !== 'undefined' && dataStatus !== null && parseInt(dataStatus.length) > 0){
												
												if(parseInt(dataStatus[0].owner_status) === 0 && parseInt(dataStatus[0].receiver_status) === 0)
												{
													array_rows.push(memberslist);
													resolve(array_rows);
													counter -= 1;
												}
												else
												{
													counter -= 1;
												}
											}
											else
											{ 
												array_rows.push(memberslist);
												resolve(array_rows); 
												counter -= 1; 
											}
										});
									})
								}],
								function (result, current) {
									//  console.log("count : "+counter);
									if (counter === 0)
									{  return_result(result[0]);}
								});
							}
							else
							{return_result(null);}
				}
				else
				{return_result(null);}
			}
			catch(err)
			{
				console.log(err);
				return_result(null);
			}
	};

	//------------------------------------------------------------------- broadcasting Module End ---------------------------------------------------------------------------//

	//------------------------------------------------------------------- check Chat Status Module Start ---------------------------------------------------------------------------//

	/*
	 * save or Reject to jazecome contact table
	 */
	methods.checkAccountJazecomChatStatus =  function(arrParams,return_result){

		try
		{
			var account_id = arrParams.account_id;

			var request_account_id = arrParams.request_account_id;

			methods.__checkJazeChatActiveStatus(arrParams.account_id,arrParams.account_type, function (ownerStatus) {

				if(parseInt(ownerStatus) === 1)
				{
					methods.__checkJazeChatActiveStatus(arrParams.request_account_id,arrParams.request_account_type, function (recivStatus) {

						if(parseInt(recivStatus) === 1)
						{
							methods.__checkJazeChatBlockStatus(account_id,request_account_id, function (ownerBlockStatus) {

								if(parseInt(ownerBlockStatus) === 1)
								{
									methods.__checkJazeChatBlockStatus(request_account_id,account_id, function (recivBlockStatus) {

										if(parseInt(recivBlockStatus) === 1)
										{ return_result("1"); }
										else
										{ return_result("2"); }
									});
								}
								else
								{ return_result("2"); }
							});
						}
						else
						{ return_result("3"); }
					});
				}
				else
				{ return_result("3"); }
			});
		}
		catch(err)
		{
			console.log("checkAccountJazecomChatStatus",err);
			return_result(null);
		}
	}


	methods.__checkJazeChatActiveStatus =  function(account_id,account_type,return_result){

		try
		{
			if(parseInt(account_id) !== 0 && parseInt(account_type) !== 0)
			{
				var query  = "";

				if(parseInt(account_type) === 4)
				{
					    query  = " SELECT meta_value FROM jaze_user_basic_details WHERE meta_key in ('team_chat_status') and  `account_id` IN (SELECT `account_id`  FROM jaze_user_basic_details WHERE `meta_key` = 'team_status' AND meta_value = '1' ) ";
						query += " and account_id = '"+account_id+"' and account_type = '4' ";
				}
				else
				{
					   query = "SELECT meta_value FROM jaze_user_jazecome_status WHERE meta_key in ('jaze_chat') and `group_id` IN (SELECT `group_id`  FROM jaze_user_jazecome_status WHERE `meta_key` = 'account_id' AND meta_value = '"+account_id+"' ) ";
				}

				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0].meta_value);
					}
					else
					{ return_result('0'); }
				});
			}
			else
			{ return_result('0'); }
		}
		catch(err)
		{
			console.log("__checkJazeChatActiveStatus",err);
			return_result("0");
		}
	}


	methods.__checkJazeChatBlockStatus =  function(account_id,request_account_id,return_result){

		try
		{
			var query =" SELECT meta_value FROM jaze_jazecom_contact_list WHERE meta_key != '' and meta_key in ('chat_blocked_account_id') and  group_id  in ( SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value ='"+account_id+"'  ) ";

			DB.query(query,null, function(data, error){
									
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					if(data[0].meta_value !== ""){
					
						var list = convert_array(data[0].meta_value);

						if(list.indexOf(request_account_id) !== -1)
						{ 
							return_result("0");
						}
						else
						{ return_result("1"); }
					}
					else
					{ return_result("1"); }
					
				}
				else
				{ return_result("1"); }
			});
		}
		catch(err)
		{
			console.log("__checkJazeChatBlockStatus",err);
			return_result("0");
		}
	}

	//------------------------------------------------------------------- check Chat Status Module End ---------------------------------------------------------------------------//


		/*
		*
		*/
		methods.insertTBQuery = function(tbName,grpId,arrInsert,return_result){

				try{

					var count = Object.keys(arrInsert).length;

					for (var meta_keys in arrInsert) 
					{
						var post  = {group_id: grpId, meta_key: meta_keys,meta_value: arrInsert[meta_keys] };

						DB.query("  insert into "+tbName+" SET ?  ",post, function(result,error){ 
							if(result !== null)
							{ 	count--; 
								if(parseInt(count) === 1)
									{ return_result(grpId); }
							}
						});
					};
				}
				catch(err)
				{
					console.log(err);
					return_result(0);
				}
		}

		/*
		*
		*/   
		methods.selectQuery = function(tbName,where = '',fields = '*',return_result){
			try
			{
				if(tbName != "")
				{
					var quer = "SELECT  "+fields+" FROM "+tbName+where;

					DB.query(quer,null, function(data, error){
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_result(data);
						}
						else
						{ return_result(null); }
					});
				}   
				else
				{ return_result(null); }
			}
			catch(err)
			{ 
				console.log(err);
				return_result(null); 
			}
		}

		/*
		*
		*/   
		methods.updateQuery = function(tbName,grpId,arrInsert,return_result){

			try{
				if(tbName !== "" && grpId !== "" && arrInsert !== null)
				{
					var count = Object.keys(arrInsert).length;
					
					for (var meta_key in arrInsert) 
					{
						var quer = " UPDATE  "+tbName+"  SET    meta_value = '"+arrInsert[meta_key]+"' where meta_key = '"+meta_key+"' and  group_id =  '"+grpId+"';";
							DB.query(quer,null, function(data, error){ 
								
								if(data !== null)
								{ if(parseInt(count) === 1)
								{ return_result(grpId); } count--; }
							});
					};
					
				}	
				else
				{ return_result(0); }
			}
			catch(err)
			{
				console.log(err);
				return_result(0);
			}
		}
		
		/*
		*
		*/   
		methods.deleteQuery = function(tbName,where,return_result){
			try
			{

				if(tbName !== "" && where !== "")
				{
					var quer = "DELETE FROM  "+tbName+" where "+where;

					DB.query(quer,null, function(data, error){
						if(typeof error === 'undefined' || error === null ){

							return_result(1);
						}
					});
				}   
				else
				{ return_result(0); }
			}
			catch(err)
			{ 
				console.log(err);
				return_result(0); 
			}
		}






//------------------------------------------------------- General Functions Start -----------------------------------------------------------//
 

  function convert_array(array_val) {

	if(array_val !== null && array_val !== "")
	{
		var list = [];

		if(array_val.toString().indexOf(',') != -1 ){
			list = array_val.split(',');
		}
		else {
			list.push(array_val);
		}

		return list;
	}
	else
	{ return ""; }
}

//------------------------------------------------------- General Functions End -----------------------------------------------------------//



module.exports = methods;
