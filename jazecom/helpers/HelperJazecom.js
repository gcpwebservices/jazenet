const methods = {};
const DB = require('../../helpers/db_base.js');
const HelperRestriction = require('../../helpers/HelperRestriction.js');
const HelperGeneral = require('../../helpers/HelperGeneral.js');

const promiseForeach = require('promise-foreach');
const HelperJazechat = require('../helpers/HelperJazechat.js');
const moment = require("moment");
const fast_sort = require("fast-sort");
var str_replace = require('str_replace');
var rtrim = require('rtrim');
var ltrim = require('ltrim');

	//------------------------------------------------------------------- Login Module Start ---------------------------------------------------------------------------//

	/*
	 * check email and password
	 */
	methods.checkCredentialDetails = function(arrParams,return_result){

		try
		{
			var jazemail_id = ltrim(rtrim(arrParams.jazemail_id));
			
			if(!emailIsValid(arrParams.jazemail_id))
			{
				jazemail_id = jazemail_id+"@jazenet.com";
			}
			
			var query = " select account_id,account_type from jaze_user_credential_details where jazemail_id= '"+jazemail_id+"' and password = '"+ltrim(rtrim(arrParams.password))+"' and status = '1' ";
		
			DB.query(query, null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0]);
				}else{
					return_result(null);
				}
			});
		}
		catch(err)
		{ console.log("checkCredentialDetails",err); return_result(null); }	
	};

	function emailIsValid (email) {
		return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
	  }

	/*
	* get sub account list
	*/
	methods.insertChildAccountDevice = function(arrParams,return_result){

		try
		{
			if(parseInt(arrParams.account_id) !== 0)
			{
				var query = " SELECT prof_id,company_id,work_id FROM jaze_user_account_details WHERE status = 1 and user_id = "+arrParams.account_id+" ";
			
				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var query = "SELECT * FROM jaze_app_device_details WHERE status = '1' AND  app_type = '2' AND account_id = '"+arrParams.account_id+"' AND  id = '"+arrParams.app_id+"' ";
			
							DB.query(query, null, function(dataAppDetails, error){

								if(typeof dataAppDetails !== 'undefined' && dataAppDetails !== null && parseInt(dataAppDetails.length) > 0){

									methods.__saveProfessionalAccountDevice(arrParams,dataAppDetails,data[0].prof_id, function(profRres){

										methods.__saveCompanyAccountDevice(arrParams,dataAppDetails,data[0].company_id, function(compRres){

											methods.__saveWorkAccountDevice(arrParams,dataAppDetails,data[0].work_id, function(workRres){

												methods.__saveWorkCompanyAccountDevice(arrParams,dataAppDetails,data[0].work_id, function(workCompanyRres){

												return_result(1);

												});
											});
										});
									});
								}
								else
								{ return_result(null); }
							});
					}
					else
					{ return_result(1); }
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("insertChildAccountDevice",err); return_result(null); }	
	};

	
	/*
	 * check Device Details
	 */
	methods.checkDeviceDetails  = function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{
				var query = "SELECT * FROM jaze_app_device_details WHERE device_id = '"+arrParams.device_id+"' AND device_type = '"+arrParams.device_type+"' AND  status = '1' AND  app_type = '2' AND parent_id = '0' AND account_id = '"+arrParams.account_id+"' ";

				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						//get child accounts
						return_result(data[0]);
					}
					else
					{
						const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

						var query = "UPDATE `jaze_app_device_details` SET status = '0', last_update_date = '"+standardDTUTC+"' WHERE device_id = '"+arrParams.device_id+"' AND device_type = '"+arrParams.device_type+"'  AND status = '1' AND  app_type = '2' ";

						DB.query(query, null, function(data, error){
							
							return_result(null);
							
						});
					}
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("checkDeviceDetails",err); return_result(null); }	
	};

	/*
	 * save Device Details
	 */
	methods.saveDeviceDetails  = function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{
				//save Device
				var retAPIToken =  generateMobileAPI();

				if(retAPIToken !== null)
				{
					const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

					var query = " INSERT INTO jaze_app_device_details SET ";
						query += " account_id = '"+arrParams.account_id+"', ";
						query += " account_type = '1', ";
						query += " app_type  = '2', ";
						query += " parent_id = '0', ";
						query += " device_id = '"+arrParams.device_id+"', ";
						query += " device_name = '"+arrParams.device_name+"', ";
						query += " device_type = '"+arrParams.device_type+"', ";
						query += " fcm_token = '"+arrParams.device_fcm_token+"', ";
						query += " api_token = '"+retAPIToken+"', ";
						query += " create_date = '"+standardDTUTC+"', ";
						query += " last_update_date = '"+standardDTUTC+"', ";
						query += " status  = '1' ";

					DB.query(query,null, function(data, error){

						var arrParam = {account_id:arrParams.account_id };
								
								HelperGeneral.getAccountDetails(arrParam, function(accRres){	

									if(accRres !== null)
									{
										methods.__getJazeMailId(accRres.email,accRres.account_id, function(jazemailID){	

											var objReturn = {
												device_details  :{
													app_id 	  	: data.insertId.toString(),
													api_token 	: retAPIToken,
													account_id	: arrParams.account_id.toString(),
												},
												master_account_details :{
													account_id	: accRres.account_id,
													account_type: accRres.account_type,
													name		: accRres.name,
													logo		: accRres.logo,
													email		: accRres.email,
													mobile		: accRres.mobile_code+" "+accRres.mobile_no,
													category_name : accRres.category,
													master		: "1",
													mail_id 	: jazemailID.toString(),
													jazenet_id	  : accRres.jazenet_id,
													flag		: "1"
												}
											};

											return_result(objReturn);
										});
									}
								});
					});
				}
				else{
					return_result(null);
				}
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("checkDeviceAlreadyExists",err); return_result(null); }	
	};
	
	/*
	 * update Device Details
	 */
	methods.updateDeviceDetails = function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{
				var retAPIToken =  generateMobileAPI();

				if(retAPIToken !== null)
				{
					const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

					var query  = " UPDATE jaze_app_device_details SET ";
						query += " fcm_token = '"+arrParams.device_fcm_token+"', ";
						query += " api_token = '"+retAPIToken+"', ";
						query += " last_update_date = '"+standardDTUTC+"' ";
						query += " where  ";
						query += " status = '1' and app_type = 2 and ";
						query += " device_id = '"+arrParams.device_id+"' and ";
						query += " device_type = '"+arrParams.device_type+"'  ";
	
						DB.query(query,null, function(data, error){

								var arrParam = {account_id:arrParams.account_id };
								
								HelperGeneral.getAccountDetails(arrParam, function(accRres){	

									if(accRres !== null)
									{
										methods.__getJazeMailId(accRres.email,accRres.account_id, function(jazemailID){	

											var objReturn = {
												device_details  :{
													app_id 	  	: arrParams.app_id.toString(),
													api_token 	: retAPIToken,
													account_id	: accRres.account_id,
												},
												master_account_details :{
													account_id	: accRres.account_id,
													account_type: accRres.account_type,
													name		: accRres.name,
													logo		: accRres.logo,
													email		: accRres.email,
													mobile		: accRres.mobile_code+" "+accRres.mobile_no,
													category_name : accRres.category,
													master		: "1",
													mail_id 	: jazemailID.toString(),
													jazenet_id	  : accRres.jazenet_id,
													flag		: "1"
												}
											};

											return_result(objReturn);
										});
									}
								});
						});
				}
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("updateDeviceDetails",err); return_result(null); }	
	};

	/*
	 * remove Device Details
	 */
	methods.removeAllDeviceDetails = function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{
				var query = "UPDATE `jaze_app_device_details` SET status = '0' WHERE status = '1' AND  app_type = '2' AND api_token = '"+arrParams.api_token+"' AND (id = '"+arrParams.app_id+"' or parent_id = '"+arrParams.app_id+"') ";

				DB.query(query,null, function(data, error){
					return_result(1);
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("removeAllDeviceDetails",err); return_result(null); }	
	};

    /*
	 * logout Device Details
	 */
	methods.logoutAllAccountsDevice = function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{
				var query = "UPDATE `jaze_app_device_details` SET status = '0' WHERE status = '1' AND  app_type = '2' AND device_id = '"+arrParams.device_id+"' AND device_name = '"+arrParams.device_name+"' ";

				DB.query(query,null, function(data, error){
					return_result(1);
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("removeAllDeviceDetails",err); return_result(null); }	
	};


	/*
	* get Device account list
	*/
	methods.getDeviceAccountList = function(arrParams,return_result){

		try
		{
			if(parseInt(arrParams.account_id) !== 0)
			{
				var query = "SELECT * FROM jaze_app_device_details WHERE  status = '1' AND  app_type = '2' AND  api_token = '"+arrParams.api_token+"' AND (id = '"+arrParams.app_id+"' or parent_id = '"+arrParams.app_id+"') ";
	
				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var counter = data.length;

						var array_rows = [];

						if(parseInt(counter) !== 0)
						{
							promiseForeach.each(data,[function (data) {

								return new Promise(function (resolve, reject) {

									var arrParams = {account_id:data.account_id };
									
									HelperGeneral.getAccountDetails(arrParams, function(accRres){	
										
										if(accRres !== null)
										{
											methods.__getJazeMailId(accRres.email,accRres.account_id, function(jazemailID){	
												
												
												var is_master = "0";

												if(parseInt(data.parent_id) === 0)
												{ is_master = "1"; }

												var objPersonal = {
													account_id	: accRres.account_id,
													account_type: accRres.account_type,
													name		: accRres.name,
													logo		: accRres.logo,
													email		: accRres.email,
													mobile		: accRres.mobile_code+" "+accRres.mobile_no,
													jazenet_id	  : accRres.jazenet_id,
													category_name : accRres.category,
													master		: is_master,
													mail_id 	: jazemailID.toString(),
													flag		: "1"
												};
												
												array_rows.push(objPersonal);

												counter -= 1;

												resolve(array_rows);
											});
										}
										else
										{ counter -= 1; resolve(array_rows);}
									});
								})
							}],
							function (result, current) {
								
								if (counter === 0)
								{return_result(fast_sort(result[0]).asc('account_type')); }
							});
						}
						else{
							return_result(null);
						}
						
					}else{
						return_result(null);
					}
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("getDeviceAccountList",err); return_result(null); }	
	};

	/*
	* save Device account details
	*/
	methods.saveDeviceAccountDetails  = function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{
				//checking already exists
				var query = "SELECT * FROM jaze_app_device_details WHERE status = '1' AND  app_type = '2' AND account_id = '"+arrParams.account_id+"' AND  parent_id = '"+arrParams.app_id+"' ";
				
				DB.query(query, null, function(data, error){
					
					if(data === undefined || data.length == 0){
					
						//get master app details
						var query = "SELECT * FROM jaze_app_device_details WHERE status = '1' AND  app_type = '2' AND parent_id = '0' AND  id = '"+arrParams.app_id+"' ";
					
						DB.query(query, null, function(data, error){

							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
		
								const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

								var query = " INSERT INTO jaze_app_device_details SET ";
								query += " account_id = '"+arrParams.account_id+"', ";
								query += " account_type = '"+arrParams.account_type+"', ";
								query += " app_type = '2', ";
								query += " parent_id = '"+arrParams.app_id+"', ";
								query += " device_id = '"+data[0].device_id+"', ";
								query += " device_name = '"+data[0].device_name+"', ";
								query += " device_type = '"+data[0].device_type+"', ";
								query += " fcm_token = '"+data[0].fcm_token+"', ";
								query += " api_token = '"+data[0].api_token+"', ";
								query += " create_date = '"+standardDTUTC+"', ";
								query += " last_update_date = '"+standardDTUTC+"', ";
								query += " status  = '1' ";

								DB.query(query,null, function(data, error){
									
									if(data !== null){
										return_result(data.insertId);
									}
								});
							}
							else
							{ return_result(null); }
						});
					}
					else
					{ return_result(null); }
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("saveDeviceAccountDetails",err); return_result(null); }	
	};

	/*
	* remove Device account details
	*/
	methods.removeDeviceAccountDetails  = function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{
				//checking exists
				var query = "SELECT id FROM jaze_app_device_details WHERE status = '1' AND  app_type = '2' AND account_id = '"+arrParams.account_id+"' AND  parent_id = '"+arrParams.app_id+"' ";
			
				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						//update status
						var query = "UPDATE  jaze_app_device_details SET  status = '0'  WHERE status = '1' AND  app_type = '2' AND account_id = '"+arrParams.account_id+"' AND  parent_id = '"+arrParams.app_id+"' ";

						DB.query(query, null, function(data, error){

							if(error == null){
								return_result(1);
							}
							else
							{ return_result(null); }
						});
					}
					else
					{ return_result(null); }
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("removeDeviceAccountDetails",err); return_result(null); }	
	};


	//Generate Mobile API
	function generateMobileAPI(){
		try
		{
			var randomString = '';

			var characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			
			for(var i = 0; i < 25; i++) {
				randomString += characters[Math.floor(Math.random() * characters.length)];
			}

			//checking allready Exists
			var query = " SELECT `id` FROM `jaze_app_device_details` WHERE  app_type = '2' and `api_token` = '"+randomString+"'; ";
			
			DB.query(query, null, function(data, error){
				
				if(typeof data === 'undefined' && data === null){
					randomString = generateMobileAPI();
				}
			});

			return randomString;
		}
		catch(err)
		{ console.log("generateRandomString",err); return null; }	
	};

	/*
	* save Professional Account details in Device
	*/
	methods.__saveProfessionalAccountDevice = function(arrParams,dataAppDetails,prof_id,return_result){

		try
		{
			if(arrParams !== null && dataAppDetails !== null && parseInt(prof_id) !== 0)
			{
				const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

				var query = " INSERT INTO jaze_app_device_details SET ";
					query += " account_id = '"+prof_id+"', ";
					query += " account_type = '2', ";
					query += " app_type = '2', ";
					query += " parent_id = '"+arrParams.app_id+"', ";
					query += " device_id = '"+dataAppDetails[0].device_id+"', ";
					query += " device_name = '"+dataAppDetails[0].device_name+"', ";
					query += " device_type = '"+dataAppDetails[0].device_type+"', ";
					query += " fcm_token = '"+dataAppDetails[0].fcm_token+"', ";
					query += " api_token = '"+dataAppDetails[0].api_token+"', ";
					query += " create_date = '"+standardDTUTC+"', ";
					query += " last_update_date = '"+standardDTUTC+"', ";
					query += " status  = '1' ";

					DB.query(query,null, function(data, error){
						if(data != null){
							return_result(1);
						}
					});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("saveProfessionalAccountDevice",err); return_result(null); }	
	};

	/*
	* save Company Account details in Device
	*/
	methods.__saveCompanyAccountDevice = function(arrParams,dataAppDetails,comp_id,return_result){

		try
		{
			if(arrParams !== null && dataAppDetails !== null  && parseInt(comp_id) !== 0)
			{
				const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

				var query = " INSERT INTO jaze_app_device_details SET ";
					query += " account_id = '"+comp_id+"', ";
					query += " account_type = '3', ";
					query += " app_type = '2', ";
					query += " parent_id = '"+arrParams.app_id+"', ";
					query += " device_id = '"+dataAppDetails[0].device_id+"', ";
					query += " device_name = '"+dataAppDetails[0].device_name+"', ";
					query += " device_type = '"+dataAppDetails[0].device_type+"', ";
					query += " fcm_token = '"+dataAppDetails[0].fcm_token+"', ";
					query += " api_token = '"+dataAppDetails[0].api_token+"', ";
					query += " create_date = '"+standardDTUTC+"', ";
					query += " last_update_date = '"+standardDTUTC+"', ";
					query += " status  = '1' ";

					DB.query(query,null, function(data, error){
						if(data != null){
							return_result(1);
						}
					});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("saveCompanyAccountDevice",err); return_result(null); }	
	};


	/*
	* save Work Account details in Device
	*/
	methods.__saveWorkAccountDevice = function(arrParams,dataAppDetails,work_id,return_result){

		try
		{
			if(arrParams !== null && dataAppDetails !== null  && parseInt(work_id) !== 0)
			{
				const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

				var query = " INSERT INTO jaze_app_device_details SET ";
					query += " account_id = '"+work_id+"', ";
					query += " account_type = '4', ";
					query += " app_type = '2', ";
					query += " parent_id = '"+arrParams.app_id+"', ";
					query += " device_id = '"+dataAppDetails[0].device_id+"', ";
					query += " device_name = '"+dataAppDetails[0].device_name+"', ";
					query += " device_type = '"+dataAppDetails[0].device_type+"', ";
					query += " fcm_token = '"+dataAppDetails[0].fcm_token+"', ";
					query += " api_token = '"+dataAppDetails[0].api_token+"', ";
					query += " create_date = '"+standardDTUTC+"', ";
					query += " last_update_date = '"+standardDTUTC+"', ";
					query += " status  = '1' ";

					DB.query(query,null, function(data, error){
						if(data != null){
							return_result(1);
						}
					});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("__saveWorkAccountDevice",err); return_result(null); }	
	};

	/*
	* save Work Company Account details in Device
	*/
	methods.__saveWorkCompanyAccountDevice = function(arrParams,dataAppDetails,work_id,return_result){

		try
		{
			if(arrParams !== null && dataAppDetails !== null  && parseInt(work_id) !== 0)
			{
				//check user have company jazecom permision

				let query  = " SELECT meta_value FROM `jaze_user_jazecome_status` WHERE meta_key = 'company_id' and `group_id` IN ";
				    query += " (SELECT `group_id` FROM jaze_user_jazecome_status  WHERE `meta_key` = 'account_id' AND `meta_value` = '"+work_id+ "') ";
				
					DB.query(query, null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

							var query = " INSERT INTO jaze_app_device_details SET ";
								query += " account_id = '"+data[0].meta_value+"', ";
								query += " account_type = '3', ";
								query += " app_type = '2', ";
								query += " parent_id = '"+arrParams.app_id+"', ";
								query += " device_id = '"+dataAppDetails[0].device_id+"', ";
								query += " device_name = '"+dataAppDetails[0].device_name+"', ";
								query += " device_type = '"+dataAppDetails[0].device_type+"', ";
								query += " fcm_token = '"+dataAppDetails[0].fcm_token+"', ";
								query += " api_token = '"+dataAppDetails[0].api_token+"', ";
								query += " create_date = '"+standardDTUTC+"', ";
								query += " last_update_date = '"+standardDTUTC+"', ";
								query += " status  = '1' ";

								DB.query(query,null, function(data, error){
									if(data != null){
										return_result(1);
									}
								});
						}
						else
						{ return_result(null); }
					});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("__saveWorkCompanyAccountDevice",err); return_result(null); }	
	};

	/*
	* save Company Account details in Device
	*/
	methods.__getAccountDetails = function(arrParams,comp_id,return_result){

		try
		{
			if(arrParams !== null && parseInt(comp_id) !== 0)
			{
				//checking already exists
				var query = "SELECT * FROM jaze_app_device_details WHERE status = '1' AND  app_type = '2' AND account_id = '"+arrParams.account_id+"' AND  id = '"+arrParams.app_id+"' ";
			
				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

						var query = " INSERT INTO jaze_app_device_details SET ";
							query += " account_id = '"+comp_id+"', ";
							query += " account_type = '3', ";
							query += " app_type = '2', ";
							query += " parent_id = '"+arrParams.app_id+"', ";
							query += " device_id = '"+data[0].device_id+"', ";
							query += " device_name = '"+data[0].device_name+"', ";
							query += " device_type = '"+data[0].device_type+"', ";
							query += " fcm_token = '"+data[0].fcm_token+"', ";
							query += " api_token = '"+data[0].api_token+"', ";
							query += " create_date = '"+standardDTUTC+"', ";
							query += " last_update_date = '"+standardDTUTC+"', ";
							query += " status  = '1' ";

							DB.query(query,null, function(data, error){
								if(data != null){
								return_result(1);
								}
							});
					}
					else
					{ return_result(null); }
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("saveCompanyAccountDevice",err); return_result(null); }	
	};

	//------------------------------------------------------------------- Login Module End ---------------------------------------------------------------------------//
	//------------------------------------------------------------------- jazecom Home Module Start ---------------------------------------------------------------------------//

	/*
	* get  jazecom child account list
	*/
	methods.getHomeModuleAccountList = function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{
				//get child account list
				var query = "SELECT account_id FROM  `jaze_app_device_details` WHERE status = '1'  AND  app_type = '2' and   api_token = '"+arrParams.api_token+"' and  (id = '"+arrParams.app_id+"' or parent_id = '"+arrParams.app_id+"') ";

				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var counter = data.length;
						
						var array_rows = [];
				
						if(parseInt(counter) !== 0)
						{
							promiseForeach.each(data,[function (account_list) {
				
								return new Promise(function (resolve, reject) {	
									 
									 methods.__getJazecomChildAccountList(arrParams.account_id,account_list.account_id, function(accRres){  
											
										if(accRres !== null)
										{ 
											if(array_rows.length !== 0)
											{
												Object.assign({},array_rows[0]).home_account_list.push(accRres);

												array_rows[0].notification_count.chat_count =  (parseInt(array_rows[0].notification_count.chat_count ) +  parseInt(accRres.chat.count)).toString();
												array_rows[0].notification_count.mail_count =  (parseInt(array_rows[0].notification_count.mail_count ) +  parseInt(accRres.mail.count)).toString();
												array_rows[0].notification_count.feed_count =  (parseInt(array_rows[0].notification_count.feed_count ) +  parseInt(accRres.feed.count)).toString();
												array_rows[0].notification_count.diary_count =  (parseInt(array_rows[0].notification_count.diary_count ) +  parseInt(accRres.diary.count)).toString();
												array_rows[0].notification_count.call_count =  (parseInt(array_rows[0].notification_count.call_count ) +  parseInt(accRres.call.count)).toString();
											}
											else
											{
												var obgRet = {
														home_account_list: [accRres],
														notification_count : {
															chat_count: accRres.chat.count,
															mail_count: accRres.mail.count,
															feed_count: accRres.feed.count,
															diary_count: accRres.diary.count,
															call_count: accRres.call.count
														}
													};   

												array_rows.push(obgRet);
											}

											counter -= 1;

											resolve(array_rows);
										}
										else
										{counter -= 1; resolve(array_rows);}
									 });
								})
							}],
							function (result, current) {
								
								if (counter === 0)
								{ return_result(result[0]); }
							});	
						}
						else
						{ return_result(null); }
					}
					else
					{ return_result(null); }
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("getHomeModuleAccountList",err); return_result(null); }
	};

	
	methods.__getJazecomChildAccountList = function(master_account_id,account_id,return_result)
	{
		try
		{
			if(parseInt(account_id) !== 0)
			{
				var arrParams = {account_id:account_id};
				
				HelperGeneral.getAccountDetails(arrParams, function(accRres){
					
					if(accRres !== null)
					{ 
						
							var chat = { status: "0", count : "0" };
							var mail = { status: "0", count : "0"  };
							var feed = { status: "0", count : "0" };
							var diary = { status: "0", count : "0" };
							var call = { status: "0", count : "0" };
							

							//check is he admin or not
							methods.__getWorkAccountAdminMasterStatus(master_account_id,accRres, function(is_master){
								
								//get jazecom status							
								methods.___getJazecomAccountStatus(master_account_id,accRres, function(statusRes){	
								
									methods.__getChatUnReadCount(statusRes,accRres.account_id, function(chatCount){	

										methods.__getEmailUnReadCount(statusRes,accRres.account_id, function(mailCount){	
											
											methods.__getDiaryUnReadCount(statusRes,accRres.account_id, function(diaryCount){	

												 methods.__getFeedUnReadCount(statusRes,accRres.account_id,accRres.account_type, function(feedCount){	

													methods.__getJazeMailId(accRres.email,accRres.account_id, function(jazemailID){	
														
															if(statusRes !== null)
															{ 
																//Chat									
																if(parseInt(statusRes.jaze_chat) === 1)
																{ chat = { status: "1", count : chatCount .toString() }; }

																//Mail
																if(parseInt(jazemailID) !== 0)
																{
																	if(parseInt(statusRes.jaze_mail) === 1)
																	{ mail = { status: "1",  count : mailCount.toString() }; }
																}
																else
																{ mail = { status: "0",  count : "0" }; }


																//Feed									
																if(parseInt(statusRes.jaze_feed) === 1)
																{ feed = { status: "1", count : feedCount.toString() }; }
																
																//Diary
																if(parseInt(statusRes.jaze_diary) === 1)
																{ diary = { status: "1", count :diaryCount.toString() }; }

																//Call
																if(parseInt(statusRes.jaze_call) === 1)
																{ call = { status: "0", count : "0" }; }

															}
																
																var objAccRres = {
																	account_id	: accRres.account_id,
																	account_type: accRres.account_type,
																	name		: accRres.name,
																	logo		: accRres.logo,
																	email		: accRres.email,
																	mail_id 	: jazemailID.toString(),
																	mobile		: accRres.mobile_code+" "+accRres.mobile_no,
																	jazenet_id	: accRres.jazenet_id,
																	category_name : accRres.category,
																	title		: accRres.title,
																	master		: is_master.toString(),
																	external_flag	: "0",
																	chat 		: chat,
																	mail		: mail,
																	feed        : feed,
																	diary       : diary,
																	call        : call
																};
																
															return_result(objAccRres); 
													});
												});
											});
										});
									});								
								});
							});
					}
					else
					{ return_result(null); }
				});
			}
		}
		catch(err)
		{ console.log("__getJazecomChildAccountList",err); return_result(null); }	
	};


	methods.___getAccountJazecomStatus= function(arrParams,return_result)
	{ 
		try
		{
			if(arrParams !== null)
			{
				if(parseInt(arrParams.account_type) === 4)
				{
					var query  = " SELECT * FROM jaze_user_basic_details WHERE meta_key in ('team_diary_status','team_mail_status','team_chat_status','team_call_status') and  `account_id` IN (SELECT `account_id`  FROM jaze_user_basic_details WHERE `meta_key` = 'team_status' AND meta_value = '1' ) ";
						query += " and account_id = '"+arrParams.account_id+"' and account_type = '4' ";

						DB.query(query, null, function(data, error){

							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

								var list = [];

								Object.assign(list, {account_id : data[0].account_id});
								Object.assign(list, {account_type : data[0].account_type});

								for (var i in data) 
								{
									Object.assign(list, {[data[i].meta_key]:  data[i].meta_value});
								}

								var objResult = { 
										account_id : list.account_id.toString(),
										jaze_diary : list.team_diary_status.toString(),
										jaze_feed  : '0',
										jaze_call  : list.team_call_status.toString(),
										jaze_mail  : list.team_mail_status.toString(),
										jaze_chat  : list.team_chat_status.toString(),
									};

								return_result(objResult);
							}
							else
							{ return_result(null); }
						});
				}
				else
				{
					var query = "SELECT * FROM jaze_user_jazecome_status WHERE  `group_id` IN (SELECT `group_id`  FROM jaze_user_jazecome_status WHERE `meta_key` = 'account_id' AND meta_value = '"+arrParams.account_id+"' ) ";
						
					DB.query(query, null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							
							HelperRestriction.convertMetaArray(data, function(statusRes){

								return_result(statusRes);
							});
						}
						else
						{ return_result(null); }
					});
				}
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("___getAccountJazecomStatus",err); return_result(null); }	
	}

	methods.___getJazecomAccountStatus= function(master_account_id,arrParams,return_result)
	{ 
		try
		{
			if(parseInt(master_account_id) !== 0 && arrParams !== null)
			{
				if(parseInt(arrParams.account_type) === 4)
				{
					var query  = " SELECT * FROM jaze_user_basic_details WHERE meta_key in ('team_diary_status','team_mail_status','team_chat_status','team_call_status') and  `account_id` IN (SELECT `account_id`  FROM jaze_user_basic_details WHERE `meta_key` = 'team_status' AND meta_value = '1' ) ";
						query += " and account_id = '"+arrParams.account_id+"' and account_type = '4' ";

						DB.query(query, null, function(data, error){

							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

								var list = [];

								Object.assign(list, {account_id : data[0].account_id});
								Object.assign(list, {account_type : data[0].account_type});

								for (var i in data) 
								{
									Object.assign(list, {[data[i].meta_key]:  data[i].meta_value});
								}

								var objResult = { 
										account_id : list.account_id.toString(),
										jaze_diary : list.team_diary_status.toString(),
										jaze_feed  : '0',
										jaze_call  : list.team_call_status.toString(),
										jaze_mail  : list.team_mail_status.toString(),
										jaze_chat  : list.team_chat_status.toString(),
									};

								return_result(objResult);
							}
							else
							{ return_result(null); }
						});
				}
				else if(parseInt(arrParams.account_type) === 3)
				{
						// check company is my own
						var query = "SELECT id FROM jaze_user_account_details WHERE  status = '1' and user_id = '"+master_account_id+"' and company_id = '"+arrParams.account_id+"'";
						
						DB.query(query, null, function(data, error){

							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

								var query = "SELECT * FROM jaze_user_jazecome_status WHERE  `group_id` IN (SELECT `group_id`  FROM jaze_user_jazecome_status WHERE `meta_key` = 'account_id' AND meta_value = '"+arrParams.account_id+"' ) ";
						
								DB.query(query, null, function(data, error){

									if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
										
										HelperRestriction.convertMetaArray(data, function(statusRes){

											return_result(statusRes);
										});
									}
									else
									{ return_result(null); }
								});
							}
							else
							{ 
								//check my work profile
								var query = "SELECT work_id FROM jaze_user_account_details WHERE  status = '1' and user_id = '"+master_account_id+"'";
						
								DB.query(query, null, function(data, error){
									
									if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

										var query = "SELECT * FROM jaze_user_jazecome_status WHERE  ";
											query += " `group_id` IN (SELECT `group_id`  FROM jaze_user_jazecome_status WHERE `meta_key` = 'account_id' AND meta_value = '"+data[0].work_id+"'  ";
											query += " and `group_id` IN (SELECT `group_id`  FROM jaze_user_jazecome_status WHERE `meta_key` = 'company_id' AND meta_value = '"+arrParams.account_id+"' )) ";
											
										DB.query(query, null, function(data, error){
											
											if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
												
												HelperRestriction.convertMetaArray(data, function(statusRes){

													return_result(statusRes);
												});
											}
											else
											{ return_result(null); }
										});
									}
									else
									{ return_result(null); }
								});
							}
						});
				 }
				else
				{
					var query = "SELECT * FROM jaze_user_jazecome_status WHERE  `group_id` IN (SELECT `group_id`  FROM jaze_user_jazecome_status WHERE `meta_key` = 'account_id' AND meta_value = '"+arrParams.account_id+"' ) ";
						
					DB.query(query, null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							
							HelperRestriction.convertMetaArray(data, function(statusRes){

								return_result(statusRes);
							});
						}
						else
						{ return_result(null); }
					});
				}
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("___getJazecomAccountStatus",err); return_result(null); }	
	}

	methods.__getJazeMailId = function(email_id,account_id,return_result)
	{ 
		try
		{
			if(parseInt(account_id) !== 0 && email_id !== "")
			{
				var query = " SELECT group_id FROM `jaze_email_accounts`  WHERE ";
				    query += " group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'email_id' AND meta_value = '"+email_id+"' AND ";    
					query += " group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'account_id' AND meta_value = '"+account_id+"' )) group by group_id; ";

					DB.query(query, null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_result(data[0].group_id);
						}
						else
						{return_result("0");}

					});
			}
			else
			{ return_result("0"); }
		}
		catch(err)
		{ console.log("__getJazeMailId",err); return_result("0"); }	
	}

	methods.__getChatUnReadCount = function(statusRes,account_id,return_result)
	{ 
		try
		{
			if(statusRes !== null)
			{
				if(parseInt(statusRes.jaze_chat) === 1 && parseInt(account_id) !== 0)
				{
						//get  conversation id list
						var query = " SELECT group_concat(meta_value) as meta_value FROM `jaze_jazechat_contact_list`  WHERE `meta_key` = 'single_convers_id'  and group_id IN (SELECT group_id FROM jaze_jazechat_contact_list WHERE meta_key = 'status' AND meta_value = 1   AND group_id IN (SELECT group_id FROM jaze_jazechat_contact_list WHERE meta_key = 'account_id' AND meta_value = '"+account_id+"' )) ";

						DB.query(query, null, function(data, error){

							if(typeof data !== 'undefined' && data[0].meta_value !== null && parseInt(data.length) > 0){

								var list =[];

								if(data[0].meta_value.toString().indexOf(',') != -1 ){
									list = data[0].meta_value.split(',');
								}
								else {
									list.push(data[0].meta_value);
								}

								var count = 0;

								var counter = list.length;

								if(parseInt(counter) !== 0)
								{
									promiseForeach.each(list,[function (lists) {
 
										return new Promise(function (resolve, reject) {
										
											var query = " SELECT meta_value as count FROM `jaze_jazechat_conversation_list`  WHERE `meta_key` = 'unread_count_"+account_id+"' and  group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'status' AND meta_value = 1   AND group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'conversa_id' AND meta_value = '"+lists+"' ));  ";
												
											DB.query(query, null, function(dataCoun, error){

												if(typeof dataCoun !== 'undefined' && dataCoun !== null && parseInt(dataCoun.length) > 0){

													count =  count + parseInt(dataCoun[0].count);

													counter -= 1;

													resolve(count);
												}
												else
												{counter -= 1; resolve(count);}
											});
										});
									}],
									function (result, current) {
									//   console.log("count : ",counter);
										if (counter === 0)
										{ return_result(result[0]); }
									});	
								}
								else
								{ return_result("0"); }
							}
							else
							{ return_result("0"); }
						});
				}
				else
				{ return_result("0"); }
			}
			else
			{ return_result("0"); }
		}
		catch(err)
		{ console.log("__getChatUnReadCount",err); return_result("0"); }	
	}

	methods.__getEmailUnReadCount = function(statusRes,account_id,return_result)
	{ 
		try
		{
			if(statusRes !== null)
			{
				if(parseInt(statusRes.jaze_mail) === 1 && parseInt(account_id) !== 0)
				{
						//get  conversation id list
						var query  = " SELECT count(meta_value) as count FROM `jaze_notification_list`  WHERE meta_key = 'account_id' AND  ";
						    query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'status' AND meta_value = 1   AND   ";
							query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'notify_type' AND meta_value = '112' AND   ";
							query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'read_status' AND meta_value = '0' AND   ";
							query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'receiver_account_id' AND meta_value = '"+account_id+"' ))));  ";

						DB.query(query, null, function(data, error){

							var data_count = data[0].count;

							if(typeof data_count !== 'undefined' && data_count !== null  && parseInt(data_count) !== 0){
								return_result(data_count);
							}
							else
							{ return_result("0"); }

						});
				}
				else
				{ return_result("0"); }
			}
			else
			{ return_result("0"); }
		}
		catch(err)
		{ console.log("__getChatUnReadCount",err); return_result("0"); }	
	}

	methods.__getDiaryUnReadCount = function(statusRes,account_id,return_result)
	{ 
		try
		{
			if(statusRes !== null)
			{
				if(parseInt(statusRes.jaze_diary) === 1 && parseInt(account_id) !== 0)
				{
						//get  conversation id list
						var query  = " SELECT count(meta_value) as count FROM `jaze_notification_list`  WHERE meta_key = 'account_id' AND  ";
						    query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'status' AND meta_value = 1   AND   ";
							query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'notify_type' AND meta_value = '107' AND   ";
							query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'read_status' AND meta_value = '0' AND   ";
							query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'receiver_account_id' AND meta_value = '"+account_id+"' ))));  ";

						DB.query(query, null, function(data, error){

							var data_count = data[0].count;

							if(typeof data_count !== 'undefined' && data_count !== null  && parseInt(data_count) !== 0){
								return_result(data_count);
							}
							else
							{ return_result("0"); }

						});
				}
				else
				{ return_result("0"); }
			}
			else
			{ return_result("0"); }
		}
		catch(err)
		{ console.log("__getChatUnReadCount",err); return_result("0"); }	
	}

	methods.__getFeedUnReadCount = function(statusRes,account_id,account_type,return_result)
	{ 
		try
		{
			if(statusRes !== null)
			{  
				if(parseInt(statusRes.jaze_feed) === 1 && parseInt(account_id) !== 0 && parseInt(account_type) !== 2)
				{
						//get  conversation id list
						var query  = " SELECT count(meta_value) as count FROM `jaze_notification_list`  WHERE meta_key = 'account_id' AND  ";
						    query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'status' AND meta_value = 1   AND   ";
							query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'notify_type' AND (meta_value = '105' or meta_value = '106') AND   ";
							query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'read_status' AND meta_value = '0' AND   ";
							query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'receiver_account_id' AND meta_value = '"+account_id+"' ))));  ";

						DB.query(query, null, function(data, error){

							var data_count = data[0].count;

							if(typeof data_count !== 'undefined' && data_count !== null  && parseInt(data_count) !== 0){
								return_result(data_count);
							}
							else
							{ return_result("0"); }

						});
				}
				else
				{ return_result("0"); }
			}
			else
			{ return_result("0"); }
		}
		catch(err)
		{ console.log("__getChatUnReadCount",err); return_result("0"); }	
	}


	methods.__getWorkAccountAdminMasterStatus = function(master_account_id,arrParams,return_result)
	{ 
		// is_master  :  0 = child account, 1 =  access company account 

		try
		{ 
			if(parseInt(arrParams.account_type) === 3)
			{
				var query  = " SELECT `id` FROM `jaze_user_account_details` WHERE `status` = 1 and `user_id` = '"+master_account_id+"' and `company_id` = '"+arrParams.account_id+"'  ";

				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null  && parseInt(data.length) > 0){
								
						return_result(0);
					}
					else
					{ return_result(1); }
				});
			}
			else
			{ return_result(0); }
		}
		catch(err)
		{ console.log("__getWorkAccountAdminMasterStatus",err); return_result(0); }	
	};


	methods.getExternalEmailList = function(account_id,return_result)
	{ 
		try
		{
			if(parseInt(account_id) !== 0)
			{  
				var query  = " SELECT * FROM `jaze_email_accounts`  WHERE   ";
					query += " group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'mail_account_type' AND meta_value = 5   AND   ";
					query += " group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'account_id' AND meta_value = '"+account_id+"' ));  ";

					DB.query(query, null, function(data, error){

						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
						{ 
							HelperRestriction.convertMultiMetaArray(data, function(data_result){
								
								var counter = data_result.length;
								
									var array_rows = [];

									if(parseInt(counter) !== 0)
									{
										promiseForeach.each(data_result,[function (list) {
								
											return new Promise(function (resolve, reject) {

												var objAccRres = {
													account_id		: account_id,
													account_type	: '1',
													name			: '',
													logo			: '',
													email			: list.email_id,
													mail_id 		: list.group_id.toString(),
													mobile			: "",
													jazenet_id		: "",
													category_name 	: list.account_name,
													title			: list.email_id.replace(/.*@/, "").split('.')[0],
													master			: "0",
													external_flag	: "1",
													chat 			: { status: "0", count : "0" },
													mail			: { status: "1", count : "0" },
													feed        	: { status: "0", count : "0" },
													diary       	: { status: "0", count : "0" },
													call        	: { status: "0", count : "0" }
												};
												
												array_rows.push(objAccRres);
																						
												counter -= 1;
																						
												resolve(array_rows);
											});
										}],
										function (result, current) {
											if (counter === 0)
											{ return_result(result[0]); }
										});
									}
									else
									{ return_result(null); }	
							});
						}
						else
						{ return_result(null); }
					});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("getExternalEmailList",err); return_result(null); }	
	}

	//------------------------------------------------------------------- jazecom Home Module End ---------------------------------------------------------------------------//
	//------------------------------------------------------------------- jazecom Contact Module Start ---------------------------------------------------------------------------//

	/*
	 * get Jazecom Contact List
	 */
	methods.getContactList = function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{ 
				methods.__getAccountContactList(arrParams, function(account_list){	
					
					if(account_list !== null)
					{
						var array_rows = [];
	
						var counter = account_list.length;

						if(parseInt(counter) !== 0)
						{
							promiseForeach.each(account_list,[function (account_list) {
						
								return new Promise(function (resolve, reject) {	

									var arrParams = {account_id:account_list.contact_account_id};

									HelperGeneral.getAccountDetails(arrParams, function(accRres){

										if(accRres !== null)
										{
											var objAccRres = {
												owner_account_id	: account_list.account_id,
												account_id			: accRres.account_id,
												account_type		: accRres.account_type,
												name				: accRres.name,
												logo				: accRres.logo,
												email				: accRres.email,
												mobile				: accRres.mobile_code+" "+accRres.mobile_no,
												category_name 		: accRres.category,
												jazenet_id			: accRres.jazenet_id
											};

											array_rows.push(objAccRres);

											counter -= 1;
					
											resolve(array_rows);
										}
										else
										{ counter -= 1; resolve(array_rows); }
									});
								});
							}],
							function (result, current) {
								if (counter === 0)
								{ return_result(fast_sort(result[0]).asc('name')); }
							});	
						}
						else
						{ return_result(null); }
					}
					else
					{ return_result(null); }
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("erro getContactList ",err); return_result(null); }
	};	

	/*
	 * get Jazecom Contact  Details
	 */
	methods.getContactDetails = function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{
				var reqparm = {account_id : arrParams.contact_account_id};

				HelperGeneral.getAccountDetails(reqparm, function(accRres){

					if(accRres !== null)
					{					
						Object.assign(arrParams, {contact_account_type: accRres.account_type});

						methods.__getContactChildAccountDetails(arrParams, function(childAccountList){

							methods.__getLinkAccountContactList(arrParams, function(linkAccountList){

								var follow_status = "0";

								var fav_status = "0";

								if(typeof childAccountList !== 'undefined' && childAccountList !== null && parseInt(childAccountList.length) > 0)
								{
									if(parseInt(childAccountList[0].follow_status.status) !== 0 && parseInt(accRres.account_type) !== 1 )
									{ follow_status = "1"; }

									fav_status = "1";;
								}

								var objretrn = {
									contact_details : {
										account_id			: accRres.account_id,
										account_type		: accRres.account_type,
										name				: accRres.name,
										logo				: accRres.logo,
										email				: accRres.email,
										mobile				: accRres.mobile_code+" "+accRres.mobile_no,
										category_name 		: accRres.category,
										jazenet_id			: accRres.jazenet_id,
										follow_status 		: follow_status,
										fav_status 			: fav_status,
										link_account_list : linkAccountList
									},
									child_account_list : childAccountList
									
								};

								return_result(objretrn);
							});
						});
					}
					else
					{ return_result(null); }
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("erro getContactDetails ",err); return_result(null); }
	};

	/*
	 * delete Jazecom Contact  Details
	 */
	methods.reomveContactDetails =  function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{			
				var query = "SELECT group_concat(account_id) as account_id FROM  `jaze_app_device_details` WHERE status = '1' AND  app_type = '2' and   api_token = '"+arrParams.api_token+"' and  (id = '"+arrParams.app_id+"' or parent_id = '"+arrParams.app_id+"') ";

				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data[0].account_id !== null && parseInt(data.length) > 0){

						var list_account = [];

						list_account = convert_array(data[0].account_id);

						if(list_account !== null)
						{ 
							list_account.forEach(function(account_id) {

								//first check user have details
								var query = " SELECT * FROM jaze_jazecom_contact_list WHERE meta_key not in ('status','create_date','account_id') and   group_id  in ( SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value ='"+account_id+"'  ) ";
								
								DB.query(query,null, function(data, error){
									
									if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

										HelperRestriction.convertMetaArray(data, function(retrnResult){

											if(retrnResult !== null)
											{ 
												var query_update = [];

												var group_id = retrnResult.group_id;

												var key = Object.keys(retrnResult);

												var list = [];

												for( var i in key)
												{
													if(key[i] !== "group_id")
													{										
														if(retrnResult[key[i]] !== "")
														{
															list = convert_array(retrnResult[key[i]]);

															if(list !== null)
															{
																if(list.indexOf(arrParams.contact_account_id)!= -1)
																{
																	list.splice(list.indexOf(arrParams.contact_account_id), 1);

																	query_update.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+list.join()+"' where meta_key = '"+key[i]+"' and group_id = '"+group_id+"'; ");
																}
															}
														}
													}
												}

												if(parseInt(query_update.length) !== 0)
												{
													for( var i in query_update)
													{
														DB.query(query_update[i],null, function(data_check, error){
														});
													}
													
													return_result(group_id);
												}
												else
												{ return_result(null); }
											}
										});
									}
								});
							});

							return_result(arrParams);
						}
						else
						{ return_result(null); }
					}
					else
					{ return_result(null); }
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{
			console.log(err);
			return_result(null);
		}
	}



	/*
	 * get Jazecom Contact account list
	 */
	methods.__getAccountContactList = function(arrParams,return_result){

		try
		{
			var query = "SELECT group_concat(account_id) as account_id FROM  `jaze_app_device_details` WHERE status = '1'  AND  app_type = '2' and   api_token = '"+arrParams.api_token+"' and  (id = '"+arrParams.app_id+"' or parent_id = '"+arrParams.app_id+"') ";

					DB.query(query, null, function(data, error){
	
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
							if(data[0].account_id !== null && data[0].account_id !== "")
							{
								var account_list = [];
		
								if(data[0].account_id.toString().indexOf(',') != -1 ){
									account_list = data[0].account_id.split(',');
								}
								else {
									account_list.push(data[0].account_id);
								}

								var array_rows = [];

								var array_contact_id = [];
		
								var counter = account_list.length;
						
								if(parseInt(counter) !== 0)
								{
									promiseForeach.each(account_list,[function (account_list) {
						
										return new Promise(function (resolve, reject) {	

											//geting contact list
											var query  = " SELECT group_concat(meta_value) as contacts FROM `jaze_jazecom_contact_list`  WHERE `meta_key` not in ('create_date','account_id','status') and "; 
											query += " meta_value != '' and group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'status' AND meta_value = 1 "; 
											query += "  AND group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value = '"+account_list+"' )) "; 
										
											DB.query(query, null, function(data, error){
											
												if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

													if(data[0].contacts  !== null && typeof data[0].contacts !== 'undefined')
													{
															var value = convert_array(data[0].contacts);
															if(value !== null)
															{
																for(var i in value)
																{
																	if(array_contact_id.indexOf(value[i]) === -1 && value[i] !== "")
																	{
																		var obj_return = {
																			account_id : account_list,
																			contact_account_id : value[i]
																		};
			
																		array_rows.push(obj_return);

																		array_contact_id.push(value[i]);
																	}
																}
															}
															
															counter -= 1;
			
															resolve(array_rows);
													}
													else
													{ counter -= 1; resolve(array_rows); }
												}
												else
												{ counter -= 1; resolve(array_rows);}
											});

										});	
									}],
									function (result, current) {
										if (counter === 0)
										{ return_result(result[0]); }
									});	
								}
								else
								{return_result(null);}	
							}
							else
							{return_result(null);}	
						}
						else
						{return_result(null);}
					});
		}
		catch(err)
		{ console.log("erro __getAccountContactList ",err); return_result(null); }
	};

	methods.__getContactChildAccountDetails = function(arrParams,return_result){
			try
			{
				if(arrParams !== null)
				{
					//get child account list
					var query = "SELECT group_concat(account_id) as account_id FROM  `jaze_app_device_details` WHERE status = '1'  AND  app_type = '2' and   api_token = '"+arrParams.api_token+"' and  (id = '"+arrParams.app_id+"' or parent_id = '"+arrParams.app_id+"') ";
	
					DB.query(query, null, function(data, error){
	
						if(typeof data !== 'undefined' && data[0].account_id !== null && parseInt(data.length) > 0){
	
							var account_list = [];
	
							if(data[0].account_id.toString().indexOf(',') != -1 ){
								account_list = data[0].account_id.split(',');
							}
							else {
								account_list.push(data[0].account_id);
							}
							var array_rows = [];
	
							var counter = account_list.length;
							
							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(account_list,[function (account_list) {
					
									return new Promise(function (resolve, reject) {	

										methods.__getContactJazecomChildAccountList(arrParams.account_id,account_list,arrParams.contact_account_id,arrParams.contact_account_type, function(accRres){

											if(accRres !== null)
											{
												array_rows.push(accRres);
											}
										
											counter -= 1;
											resolve(array_rows);
										});
									})
								}],
								function (result, current) {
									
									if (counter === 0)
									{ 
										if(result[0] !== null)
										{
											return_result(fast_sort(result[0]).asc('account_type'));
										}
										else
										{ return_result(null); }
									}
								});	
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }
					});
				}
				else
				{ return_result(null); }
			}
			catch(err)
			{ console.log("__getContactChildAccountDetails",err); return_result(null); }
	};

	methods.__getLinkAccountContactList = function(arrParams,return_result){

		try
		{
			var query = "SELECT `account_id`, `account_type` FROM  `jaze_app_device_details` WHERE status = '1'  AND  app_type = '2' and   api_token = '"+arrParams.api_token+"' and  (id = '"+arrParams.app_id+"' or parent_id = '"+arrParams.app_id+"') ";

					DB.query(query, null, function(dataList, error){
	
						if(typeof dataList !== 'undefined' && dataList !== null && parseInt(dataList.length) > 0){
	
							var array_rows = [];

							var array_contact_id = [];

							var counter = dataList.length;
						
							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(dataList,[function (account_list) {
						
									return new Promise(function (resolve, reject) {	

											//geting contact list
											var query  = " SELECT group_concat(meta_value) as contacts FROM `jaze_jazecom_contact_list`  WHERE meta_value in("+arrParams.contact_account_id+") and `meta_key` not in ('create_date','account_id','status') and "; 
											query += " meta_value != '' and group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'status' AND meta_value = 1 "; 
											query += "  AND group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value = '"+account_list.account_id+"' )) "; 
										
											DB.query(query, null, function(data, error){
											
												if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

													if(data[0].contacts  !== null && typeof data[0].contacts !== 'undefined')
													{
														var obj_return = {
															account_id   : account_list.account_id.toString(),
															account_type : account_list.account_type.toString()
														};

														array_rows.push(obj_return);
	
														counter -= 1;
			
														resolve(array_rows);
													}
													else
													{ counter -= 1; resolve(array_rows); }
												}
												else
												{ counter -= 1; resolve(array_rows);}
											});
										});	
									}],
									function (result, current) {
										if (counter === 0)
										{ return_result(fast_sort(result[0]).asc('account_type')); }
									});	
							}
							else
							{return_result(null);}		
						}
						else
						{return_result(null);}
					});
		}
		catch(err)
		{ console.log("erro __getLinkAccountContactList ",err); return_result(null); }
	};

	methods.__getContactJazecomChildAccountList = function(master_account_id,account_id,contact_account_id,contact_account_type,return_result)
	{
		try
		{ 
			if(parseInt(account_id) !== 0 && parseInt(contact_account_type) !== 0 && parseInt(account_id) !== parseInt(contact_account_id))
			{
				var arrParams = {account_id:account_id};

				HelperGeneral.getAccountDetails(arrParams, function(accRres){

					if(accRres !== null )
					{ 
						if(parseInt(accRres.account_type) === 1 && parseInt(contact_account_type) === 1)
						{
							return_result(null);
						}
						else
						{
							var is_master = "0";

							if(parseInt(accRres.account_type) === 1)
							{ is_master = "1"; }

							//get owner Module and block Status
							methods.__checkModuleBlockStatus(master_account_id,1,accRres.account_id,accRres.account_type,contact_account_id, function(ownerBlockStatus){	
								
								if(ownerBlockStatus !== null)
								{
									//get contact Module and block Status
									methods.__checkModuleBlockStatus(master_account_id,0,contact_account_id,contact_account_type,accRres.account_id, function(contactBlockStatus){	

										if(contactBlockStatus !== null)
										{ 
											//get contact Favourite Status
											methods.__checkFavouriteStatus(account_id,contact_account_id, function(favStatus){	

												//get contact Favourite Status
												methods.__checkFollowStatus(account_id,contact_account_id, function(followStatus){	
		
													var chat_status = __checkModuleBlockCombination(ownerBlockStatus.chat,contactBlockStatus.chat);

													//get conversation id
													methods.__getJazechatConversationId(chat_status,account_id,contact_account_id, function(returnConversId){	

														var mail_status = __checkModuleBlockCombination(ownerBlockStatus.mail,contactBlockStatus.mail);
														
														//get mail status	
														methods.__getJazeMailStatus(mail_status,account_id,contact_account_id, function(mailStatus){		
														
															//console.log(accRres.account_id+" contactBlockStatus",contactBlockStatus);
															if(returnConversId !== null)
															{  
																	var objAccRres = {
																			account_id	: accRres.account_id,
																			account_type: accRres.account_type,
																			name		: accRres.name,
																			logo		: accRres.logo,
																			category_name : accRres.category,
																			title		: accRres.title,
																			// email		: accRres.email,
																			// mobile		: accRres.mobile_code+" "+accRres.mobile_no,
																			// master		: is_master,
																			jazenet_id 	: accRres.jazenet_id,
																			chat_status : {
																					status 	   : returnConversId.chat_status,
																					convers_id : returnConversId.conversa_id
																			},
																			mail_status : {
																				status : mailStatus.toString()
																			},	
																			feed_status : {
																				status : __checkModuleBlockCombination(ownerBlockStatus.feed,contactBlockStatus.feed).toString()
																			},
																			diary_status : {
																				status : __checkModuleBlockCombination(ownerBlockStatus.diary,contactBlockStatus.diary).toString()
																			},
																			call_status : {
																				status : __checkModuleBlockCombination(ownerBlockStatus.call,contactBlockStatus.call).toString()
																			}, 
																			fav_status : {
																				status : favStatus.toString()
																			}, 
																			follow_status : {
																				status : followStatus.status.toString(),
																				group_id : followStatus.group_id.toString()
																			},     
																	};

																return_result(objAccRres);
															}
														});
													});
												});
											});
										}
										else
										{ return_result(null); }
									});
								}
								else
								{ return_result(null); }
							});
						}
					}
					else
					{ return_result(null); }
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("__getContactJazecomChildAccountList",err); return_result(null); }	
	};

	methods.__checkModuleBlockStatus = function(master_account_id,owner_status,owner_account_id,owner_account_type,contact_account_id,return_result)
	{
		try
		{
			var return_status  = { 
				account_id: "0", 
				chat: "1", 
				call : "0", 
				mail : "1", 
				feed : "1",
				diary : "1"
			};
			
			if(parseInt(owner_account_id) !== 0 && parseInt(contact_account_id) !== 0)
			{
				var accRres = { account_id : owner_account_id , account_type : owner_account_type };

					methods.___getJazecomAccountStatus(master_account_id,accRres, function(owner_module_status){	

						methods.___getAccountJazecomStatus(accRres, function(account_module_status){	

							var module_status = null;
						
							if(parseInt(owner_status) === 1)
							{ module_status = owner_module_status; }
							else
							{ module_status = account_module_status; }
							
							if(module_status !== null)
							{
								var query = "SELECT * FROM `jaze_jazecom_contact_list`  WHERE `meta_key` not in ('account_id','create_date','status') AND   group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'status' AND meta_value = 1   AND    group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value = '"+owner_account_id+"')); ";
					
								DB.query(query, null, function(data, error){
									
									HelperRestriction.convertMetaArray(data, function(contactResult){	
										
										if(contactResult !== null)
										{ 
											if(parseInt(module_status.jaze_chat) === 1)
											{
												if(convert_array(contactResult.chat_account_id).indexOf(contact_account_id)!= -1)
												{
													return_status.chat = "2";
												}
												else if(convert_array(contactResult.chat_blocked_account_id).indexOf(contact_account_id)!= -1)
												{
													return_status.chat = "3";
												}
											}
											else
											{ return_status.chat = "0"; }

											if(parseInt(module_status.jaze_mail) === 1)
											{
												if(convert_array(contactResult.mail_account_id).indexOf(contact_account_id)!= -1)
												{
													return_status.mail = "2";
												}
												else if(convert_array(contactResult.mail_blocked_account_id).indexOf(contact_account_id)!= -1)
												{
													return_status.mail = "3";
												}
											}
											else
											{ return_status.mail = "0"; }

											if(parseInt(module_status.jaze_feed) === 1)
											{
												if(convert_array(contactResult.feed_account_id).indexOf(contact_account_id)!= -1)
												{
													return_status.feed = "2";
												}
												else if(convert_array(contactResult.feed_blocked_account_id).indexOf(contact_account_id)!= -1)
												{
													return_status.feed = "3";
												}
											}
											else
											{ return_status.feed = "0"; }
											
											if(parseInt(module_status.jaze_diary) === 1)
											{
												if(convert_array(contactResult.diary_account_id).indexOf(contact_account_id)!= -1)
												{
													return_status.diary = "1";
												}
												else if(convert_array(contactResult.diary_blocked_account_id).indexOf(contact_account_id)!= -1)
												{
													return_status.diary = "3";
												}
											}
											else
											{ return_status.diary = "0"; }

											if(parseInt(module_status.jaze_call) === 1)
											{
												// if(convert_array(contactResult.call_account_id).indexOf(contact_account_id)!= -1)
												// {
												// 	return_status.call = "1";
												// }
												// else if(convert_array(contactResult.call_blocked_account_id).indexOf(contact_account_id)!= -1)
												// {
												// 	return_status.call = "3";
												// }
											}
											else
											{ return_status.call = "0"; }

											return_result(return_status);
										}
										else
										{ 
											return_status  = { 
												chat	: module_status.jaze_chat, 
												call 	: module_status.jaze_call, 
												mail 	: module_status.jaze_mail, 
												feed 	: module_status.jaze_diary, 
												diary 	: module_status.jaze_diary
											};	

											return_result(return_status);
										}
									});
								});
							}
							else
							{
								return_status  = { 
									chat: "0", 
									call : "0", 
									mail : "0", 
									feed : "0",
									diary : "0"
								};	

								return_result(return_status);
							}
						});
					});
			}
			else
			{ return_result(return_status); }
		}
		catch(err)
		{ console.log("__checkContactBlockStatus",err); return_result(return_status); }	
	};

	function __checkModuleBlockCombination(owner_status,contact_status){

		var return_status = "0";

		if(owner_status !== "" && contact_status !== "")
		{
			var new_array = ['1-1','1-2'];

			var continue_array = ['2-2','2-1'];

			var block_array = ['2-3','3-2','3-3','1-3','3-1','0-3','3-0'];

			var inactive_array = ['2-0','0-2','0-0','0-1','1-0'];

			var id = owner_status+"-"+contact_status;

			if(inactive_array.indexOf(id) != -1 ){
				//inactive
				return_status = "0";
			}
			else if(new_array.indexOf(id) != -1 ){
				// new
				return_status = "1";
			}
			else if(continue_array.indexOf(id) != -1 ){
				//continue
				return_status = "2";
			}
			else if(block_array.indexOf(id) != -1 ){
				//block
				return_status = "3";
			}
			else {
				//service not avilable
				return_status = "4";
			}

			return return_status;
		}
		else
		{ return "0" }
	}

	methods.__checkFavouriteStatus = function(account_id,contact_account_id,return_result)
	{
		try
		{
			if(parseInt(account_id) !== 0 && parseInt(contact_account_id) !== 0)
			{
					var query  = " SELECT meta_value FROM `jaze_user_favorite_list` where meta_key = 'fav_type' and  group_id IN ";
					    query += " (SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'status' AND meta_value = '1' ";
						query += " 	and  group_id IN  ";
						query += " 	(SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'account_id'  AND meta_value = '"+account_id+"' ";
						query += " 	and  group_id IN  (SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'fav_account_id' AND meta_value = '"+contact_account_id+"'))); ";

					DB.query(query, null, function(data, error){
						
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0 && parseInt(data[0].meta_value) !== 0)
						{ return_result('2'); }
						else
						{ return_result('1'); }
					});
			}
			else
			{ return_result('1'); }
		}
		catch(err)
		{ console.log("__checkFavouriteStatus",err); return_result('1'); }	
	};

	methods.__checkFollowStatus = function(account_id,contact_account_id,return_result)
	{
		try
		{
			var follow_status = {
				status : "0",
				group_id : "0"
			};

			if(parseInt(account_id) !== 0 && parseInt(contact_account_id) !== 0)
			{
				// Check have follower groups
				var query   = " SELECT id FROM `jaze_follower_groups` WHERE `account_id` = '"+contact_account_id+"' AND `group_type` = '1' AND `status` = '1' ";

				DB.query(query, null, function(data, error){
						
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{ 
						var group_id = "0";

						if(parseInt(data.length) !== 1)
						{ group_id = "0"; }
						else
						{ group_id = data[0].id; }

						//checking already followed
						var query   = " SELECT id FROM `jaze_follower_groups` WHERE `account_id` = '"+contact_account_id+"' AND `group_type` = '1' AND `status` = '1' and id IN ";
						    query  += " (SELECT group_concat(meta_value) FROM `jaze_follower_groups_meta` where meta_key = 'follower_group_id' and  group_id IN  ";
						    query  += " (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'status' AND meta_value = '1'  ";
						    query  += " and  group_id IN   ";
						    query  += " (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'follower_account_id'  AND meta_value = '"+account_id+"'))) ";

						DB.query(query, null, function(data, error){

							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
							{
								follow_status = {
									status : "2",
									group_id : group_id.toString()
								};

								return_result(follow_status); 
							}
							else
							{ 
								follow_status = {
									status : "1",
									group_id : group_id.toString()
								};

								return_result(follow_status); 
							}
						});
					}
					else
					{ return_result(follow_status); }
				});
			}
			else
			{ return_result(follow_status); }
		}
		catch(err)
		{ console.log("__checkFollowStatus",err); return_result('0'); }	
	};

	methods.__getJazechatConversationId = function(chat_status,account_id,contact_account_id,return_result)
	{
		try
		{  
				var obj_return = {
						conversa_id : "0",
						chat_status : "1"
					};


			if(parseInt(account_id) !== 0 && parseInt(contact_account_id) !== 0)	
			{
				if((parseInt(chat_status) === 1 || parseInt(chat_status) === 2))
				{
					// get conversation id
					let query  =" SELECT * FROM jaze_jazechat_conversation_list WHERE  meta_key in('conversa_id','status') and ";
						query +=" (group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'contact_account_id' AND meta_value ='"+account_id+"' and group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'account_id' AND meta_value ='"+contact_account_id+"')) ";
						query +=" or ";
						query +=" group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'contact_account_id' AND meta_value ='"+contact_account_id+"' and group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'account_id' AND meta_value ='"+account_id+"'))";
						query +=") ";

					DB.query(query, null, function(data, error){
							
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
						{ 
							HelperRestriction.convertMetaArray(data, function(retrnResult){
							
								if(parseInt(retrnResult.status) === 1 )
								{
									var obj_return = {
										conversa_id : retrnResult.conversa_id.toString(),
										chat_status : "2"
									};

									return_result(obj_return); 
								}
								else
								{
									var obj_return = {
										conversa_id : "0",
										chat_status : "4"
									};

									return_result(obj_return); 
								}
							});
						}
						else
						{  return_result(obj_return); }
					});
				}
				else
				{ 
					var obj_return = {
						conversa_id : "0",
						chat_status : chat_status
					};

					return_result(obj_return); 
				 }
			}
			else
			{ return_result(obj_return); }
		}
		catch(err)
		{ console.log("__getJazechatConversationId",err); return_result('0'); }	
	}

	methods.__getJazeMailStatus = function(mail_status,account_id,contact_account_id,return_result)
	{
		try
		{   
			if(parseInt(account_id) !== 0 && parseInt(contact_account_id) !== 0)	
			{
				if(parseInt(mail_status) === 1 || parseInt(mail_status) === 2)
				{ 
					var query = " SELECT group_id FROM `jaze_email_accounts`  WHERE ";  
					query += " group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'account_id' AND meta_value = '"+account_id+"' ) group by group_id; ";
					
					DB.query(query, null, function(data, error){
						
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							if(parseInt(data[0].group_id) !== 0)
							{
								query = " SELECT group_id FROM `jaze_email_accounts`  WHERE ";  
								query += " group_id IN (SELECT group_id FROM jaze_email_accounts WHERE meta_key = 'account_id' AND meta_value = '"+contact_account_id+"' ) group by group_id; ";

								DB.query(query, null, function(data, error){

									if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

										if(parseInt(data[0].group_id) !== 0)
										{
											 return_result(mail_status); 
										}
										else
										{ return_result("0"); }
									}
									else
									{ return_result("0"); }
								});
							}	
							else
							{ return_result("0"); }
						}
						else
						{ return_result("0"); }
					});
				}
				else
				{ return_result(mail_status); }
			}
			else
			{ return_result(mail_status); }
		}
		catch(err)
		{ console.log("__getJazechatConversationId",err); return_result('0'); }	
	}

	/*
	 * split values push to array
	 */
	function convert_array(array_val) {

		var list = [];

		if(typeof array_val !== 'undefined' && array_val !== null && array_val !== "")
		{
			if(array_val.toString().indexOf(',') != -1 ){
				list = array_val.split(',');
			}
			else {
				list.push(array_val);
			}
		}
		
		return list;
	}

	/*
	 * array merge and remove existing
	 */
	function merge_array(array1, array2) {
			var result_array = [];
			var arr = array1.concat(array2);
			var len = arr.length;
			var assoc = {};
		
			while(len--) {
				var item = arr[len];
		
				if(!assoc[item]) 
				{ 
					result_array.unshift(item);
					assoc[item] = true;
				}
			}
		
			return result_array;
	}

		
	//------------------------------------------------------------------- jazecom Contact Module End ---------------------------------------------------------------------------//


	//------------------------------------------------------------------- jazecom Favourites Module Start ---------------------------------------------------------------------------//

	/*
	 * get Jazecom Favorite list
	 */
	methods.getFavoriteList = function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{
				methods.__getAccountFavoriteList(arrParams, function(account_list){	

					if(account_list !== null)
					{
						var array_rows = [];

						var array_contact_id = [];

						var counter = account_list.length;

						if(parseInt(counter) !== 0)
						{
							promiseForeach.each(account_list,[function (account_list) {
						
								return new Promise(function (resolve, reject) {	

									if(array_contact_id.indexOf(account_list.fav_account_id) === -1)
									{
										array_contact_id.push(account_list.fav_account_id);

										var arrParamss = {account_id:account_list.fav_account_id};

										HelperGeneral.getAccountDetails(arrParamss, function(accRres){

											if(accRres !== null)
											{
												var objAccRres = {
													owner_account_id	: arrParams.account_id,
													account_id			: accRres.account_id,
													account_type		: accRres.account_type,
													name				: accRres.name,
													logo				: accRres.logo,
													email				: accRres.email,
													mobile				: accRres.mobile_code+" "+accRres.mobile_no,
													category_name 		: accRres.category
												};
											
												array_rows.push(objAccRres);

												counter -= 1;
						
												resolve(array_rows);
											}
											else
											{ counter -= 1; resolve(array_rows); }
										});
									}
									else
									{ counter -= 1; resolve(array_rows);}
								});
							}],
							function (result, current) {
								if (counter === 0)
								{ return_result(fast_sort(result[0]).asc('name')); }
							});	
						}
						else
						{ return_result(null); }
					}
					else
					{ return_result(null); }
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("erro getContactList ",err); return_result(null); }
	};	

	/*
	 * get Jazecom Contact account list
	 */
	methods.__getAccountFavoriteList = function(arrParams,return_result){

		try
		{
			var query = "SELECT group_concat(account_id) as account_id FROM  `jaze_app_device_details` WHERE status = '1' AND  app_type = '2' and   api_token = '"+arrParams.api_token+"' and  (id = '"+arrParams.app_id+"' or parent_id = '"+arrParams.app_id+"') ";

					DB.query(query, null, function(data, error){
	
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
							if(data[0].account_id !== null && data[0].account_id !== "")
							{
								var account_list = [];
		
								if(data[0].account_id.toString().indexOf(',') != -1 ){
									account_list = data[0].account_id.split(',');
								}
								else {
									account_list.push(data[0].account_id);
								}

								var array_rows = [];

								var counter = account_list.length;
						
								if(parseInt(counter) !== 0)
								{
									promiseForeach.each(account_list,[function (account_list) {
						
										return new Promise(function (resolve, reject) {	

											//geting contact list
											var query  = " SELECT group_id,meta_key,meta_value FROM `jaze_user_favorite_list` where meta_key in ('fav_account_id','fav_account_type')  and group_id IN ";
											    query += " (SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'status' AND meta_value = '1' ";
											    query += " and  group_id IN  ";
											    query += " (SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'account_id'  AND meta_value = '"+account_list+"' )) ";

											DB.query(query, null, function(data, error){
											
												if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

													HelperRestriction.convertMultiMetaArray(data, function(retrnResult){

														if(retrnResult !== null)
														{
															array_rows = array_rows.concat(retrnResult);

															counter -= 1;
												
															resolve(array_rows);
														}
														else
														{ counter -= 1; 	resolve(array_rows); }
													});
												}
												else
												{ counter -= 1; 	resolve(array_rows); }
											});
										});	
									}],
									function (result, current) {
										if (counter === 0)
										{ return_result(result[0]); }
									});	
								}
								else
								{return_result(null);}
							}
							else
							{return_result(null);}	
						}
						else
						{return_result(null);}
					});
		}
		catch(err)
		{ console.log("erro __getAccountContactList ",err); return_result(null); }
	};


	/*
	 * Update Favourites Details
	 */
	methods.updateFavouritesDetails =  function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{			
				var list = [];

				list = arrParams.account_list;

				var counter = list.length;

				var array_rows = [];

				if(parseInt(counter) !== 0)
				{
					promiseForeach.each(list,[function (lists) {

						return new Promise(function (resolve, reject) {

						var query  = " SELECT group_id FROM `jaze_user_favorite_list` where  group_id IN ";
							query += " (SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'status' AND meta_value = '1' ";
							query += " 	and  group_id IN  ";
							query += " 	(SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'account_id'  AND meta_value = '"+lists.account_id+"' ";
							query += " 	and  group_id IN  (SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'fav_account_id' AND meta_value = '"+arrParams.fav_account_id+"')))group by group_id; ";
							
							DB.query(query,null, function(data, error){
							
								var m_group_id = 0;

								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					
									m_group_id = data[0].group_id;					
								}

								//checking is add or remove
								if(parseInt(lists.flag) === 1)
								{
									//add
									//checking allready existing
									if(parseInt(m_group_id) === 0)
									{
										HelperRestriction.getNewGroupId('jaze_user_favorite_list', function (groupId) {

											if(groupId !== null) {

												var arrfavInsert = {
													account_id	   : lists.account_id,
													fav_account_id : arrParams.fav_account_id,
													fav_account_type : arrParams.fav_account_type,
													fav_type: '1',
													create_date: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
													status: '1'
												};

												HelperRestriction.insertTBQuery('jaze_user_favorite_list',groupId,arrfavInsert, function (queResGrpId) {
					
													array_rows.push(queResGrpId);
													counter -= 1;
													resolve(counter);
												});
											}
										});
									}
									else
									{ counter -= 1; resolve(array_rows);}
								}
								else
								{
									//remove
									var query = "DELETE FROM `jaze_user_favorite_list` WHERE `group_id`  = '"+m_group_id+"'; ";

									DB.query(query,null, function(data_check, error){	

										array_rows.push(m_group_id);
										counter -= 1;
										resolve(counter);

									});
								}
							});
						})
					}],
					function (result, current) {
					
						if (counter === 0)
						{  return_result(result[0]);}
					});
				}
				else
				{ return_result(null); }
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{
			console.log(err);
			return_result(null);
		}
	}

	//------------------------------------------------------------------- jazecom Favourites Module End ---------------------------------------------------------------------------//

	//------------------------------------------------------------------- jazecom Block Module Start ---------------------------------------------------------------------------//

	/*
	 * Update Contact Block or Unblock
	 */
	methods.updateContactBlockStatus =  function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{			
				var list = [];

				list = arrParams.account_list;

				var counter = list.length;

				if(parseInt(counter) !== 0)
				{
					promiseForeach.each(list,[function (lists) {

						return new Promise(function (resolve, reject) {

						var query  = " SELECT * FROM `jaze_jazecom_contact_list` where meta_key not in('status','create_date','pay_account_id','pay_blocked_account_id') and  group_id IN ";
							query += " (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id'  AND meta_value = '"+lists.account_id+"') ";
							
							DB.query(query,null, function(data, error){
							
								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
									HelperRestriction.convertMultiMetaArray(data, function(retrnResult){
									
										if(retrnResult !== null)
										{
											retrnResult = retrnResult[0];

											var query_array = [];

											//chat
											var chat_account_id = convert_array(retrnResult.chat_account_id);
											var chat_blocked_account_id = convert_array(retrnResult.chat_blocked_account_id);

											if(parseInt(lists.conversa_id) === 0 )
											{
												if(parseInt(lists.chat_status) === 1 )
												{
													//add block, remove active list

													//remove active account list
													if(chat_account_id.indexOf(arrParams.contact_account_id) !== -1)
													{
														chat_account_id.splice(chat_account_id.indexOf(arrParams.contact_account_id), 1);
													}

													//add block list
													if(chat_blocked_account_id.indexOf(arrParams.contact_account_id) === -1)
													{
														chat_blocked_account_id.push(arrParams.contact_account_id);
													}
												}
												else
												{
													//remove block, add active list

													//remove block account list
													if(chat_blocked_account_id.indexOf(arrParams.contact_account_id) !== -1)
													{
														chat_blocked_account_id.splice(chat_blocked_account_id.indexOf(arrParams.contact_account_id), 1);
													}

													//add active list
													if(chat_account_id.indexOf(arrParams.contact_account_id) === -1)
													{
														chat_account_id.push(arrParams.contact_account_id);
													}
												}

												query_array.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+chat_account_id.join()+"' where meta_key = 'chat_account_id' and group_id = '"+retrnResult.group_id+"'; ");
												query_array.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+chat_blocked_account_id.join()+"' where meta_key = 'chat_blocked_account_id' and group_id = '"+retrnResult.group_id+"'; ");
											} 
											else if(parseInt(lists.conversa_id) !== 0)
											{
												var chatt_all = chat_account_id.concat(chat_blocked_account_id);

												if(chatt_all.indexOf(arrParams.contact_account_id) !== -1)
												{
													var chat_reqParams = {
														account_id: lists.account_id,
														account_type: 0,
														convers_id: lists.conversa_id,
														api_token: arrParams.api_token
													};

													HelperJazechat.saveContactBlockUnblock(chat_reqParams, function (block_status) {
														
													});
												}
											}
	
											//mail
											var mail_account_id = convert_array(retrnResult.mail_account_id);
											var mail_blocked_account_id = convert_array(retrnResult.mail_blocked_account_id);

											if(parseInt(lists.mail_status) === 1 )
											{
												//add block, remove active list

												//remove active account list
												if(mail_account_id.indexOf(arrParams.contact_account_id) !== -1)
												{
													mail_account_id.splice(mail_account_id.indexOf(arrParams.contact_account_id), 1);
												}

												//add block list
												if(mail_blocked_account_id.indexOf(arrParams.contact_account_id) === -1)
												{
													mail_blocked_account_id.push(arrParams.contact_account_id);
												}
											}
											else
											{
												//remove block, add active list

												var mail_all = mail_account_id.concat(mail_blocked_account_id);

												if(mail_all.indexOf(arrParams.contact_account_id) !== -1)
												{
													//remove block account list
													if(mail_blocked_account_id.indexOf(arrParams.contact_account_id) !== -1)
													{
														mail_blocked_account_id.splice(mail_blocked_account_id.indexOf(arrParams.contact_account_id), 1);
													}

													//add active list
													if(mail_account_id.indexOf(arrParams.contact_account_id) === -1)
													{
														mail_account_id.push(arrParams.contact_account_id);
													}
												}
											}

											query_array.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+mail_account_id.join()+"' where meta_key = 'mail_account_id' and group_id = '"+retrnResult.group_id+"'; ");
											query_array.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+mail_blocked_account_id.join()+"' where meta_key = 'mail_blocked_account_id' and group_id = '"+retrnResult.group_id+"'; ");

										

											//feed
											var feed_account_id = convert_array(retrnResult.feed_account_id);
											var feed_blocked_account_id = convert_array(retrnResult.feed_blocked_account_id);
		
											if(parseInt(lists.feed_status) === 1 )
											{
												//add block, remove active list
		
												//remove active account list
												if(feed_account_id.indexOf(arrParams.contact_account_id) !== -1)
												{
													feed_account_id.splice(feed_account_id.indexOf(arrParams.contact_account_id), 1);
												}
		
												//add block list
												if(feed_blocked_account_id.indexOf(arrParams.contact_account_id) === -1)
												{
													feed_blocked_account_id.push(arrParams.contact_account_id);
												}
											}
											else
											{
												//remove block, add active list
		
												var feed_all = feed_account_id.concat(feed_blocked_account_id);

												if(feed_all.indexOf(arrParams.contact_account_id) !== -1)
												{
													//remove block account list
													if(feed_blocked_account_id.indexOf(arrParams.contact_account_id) !== -1)
													{
														feed_blocked_account_id.splice(feed_blocked_account_id.indexOf(arrParams.contact_account_id), 1);
													}
			
													//add active list
													if(feed_account_id.indexOf(arrParams.contact_account_id) === -1)
													{
														feed_account_id.push(arrParams.contact_account_id);
													}
												}
											}
											
											query_array.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+feed_account_id.join()+"' where meta_key = 'feed_account_id' and group_id = '"+retrnResult.group_id+"'; ");
											query_array.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+feed_blocked_account_id.join()+"' where meta_key = 'feed_blocked_account_id' and group_id = '"+retrnResult.group_id+"'; ");
		
											//call
											var call_account_id = convert_array(retrnResult.call_account_id);
											var call_blocked_account_id = convert_array(retrnResult.call_blocked_account_id);
										
											if(parseInt(lists.call_status) === 1 )
											{
												//add block, remove active list
		
												//remove active account list
												if(call_account_id.indexOf(arrParams.contact_account_id) !== -1)
												{
													call_account_id.splice(call_account_id.indexOf(arrParams.contact_account_id), 1);
												}
		
												//add block list
												if(call_blocked_account_id.indexOf(arrParams.contact_account_id) === -1)
												{
													call_blocked_account_id.push(arrParams.contact_account_id);
												}
											}
											else
											{
												//remove block, add active list

												var call_all = call_account_id.concat(call_blocked_account_id);
												
												if(call_all.indexOf(arrParams.contact_account_id) !== -1)
												{
													//remove block account list
													if(call_blocked_account_id.indexOf(arrParams.contact_account_id) !== -1)
													{
														call_blocked_account_id.splice(call_blocked_account_id.indexOf(arrParams.contact_account_id), 1);
													}

													//add active list
													if(call_account_id.indexOf(arrParams.contact_account_id) === -1)
													{
														call_account_id.push(arrParams.contact_account_id);
													}
												}
											}
		
											query_array.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+call_account_id.join()+"' where meta_key = 'call_account_id' and group_id = '"+retrnResult.group_id+"'; ");
											query_array.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+call_blocked_account_id.join()+"' where meta_key = 'call_blocked_account_id' and group_id = '"+retrnResult.group_id+"'; ");
		
		
											//diary
											var diary_account_id = convert_array(retrnResult.diary_account_id);
											var diary_blocked_account_id = convert_array(retrnResult.diary_blocked_account_id);
			
											if(parseInt(lists.diary_status) === 1 )
											{
												//add block, remove active list
			
												//remove active account list
												if(diary_account_id.indexOf(arrParams.contact_account_id) !== -1)
												{
													diary_account_id.splice(diary_account_id.indexOf(arrParams.contact_account_id), 1);
												}
			
												//add block list
												if(diary_blocked_account_id.indexOf(arrParams.contact_account_id) === -1)
												{
													diary_blocked_account_id.push(arrParams.contact_account_id);
												}
											}
											else
											{
												//remove block, add active list


												var diary_all = diary_account_id.concat(diary_blocked_account_id);

												if(diary_all.indexOf(arrParams.contact_account_id) !== -1)
												{
												//remove block account list
													if(diary_blocked_account_id.indexOf(arrParams.contact_account_id) !== -1)
													{
														diary_blocked_account_id.splice(diary_blocked_account_id.indexOf(arrParams.contact_account_id), 1);
													}
				
													//add active list
													if(diary_account_id.indexOf(arrParams.contact_account_id) === -1)
													{
														diary_account_id.push(arrParams.contact_account_id);
													}
												}
											}
			
											query_array.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+diary_account_id.join()+"' where meta_key = 'diary_account_id' and group_id = '"+retrnResult.group_id+"'; "); 
											query_array.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+diary_blocked_account_id.join()+"' where meta_key = 'diary_blocked_account_id' and group_id = '"+retrnResult.group_id+"'; ");


											counter -= 1;

											if(query_array !== null)
											{
												for (var i in query_array) 
												{ 
														DB.query(query_array[i],null, function(data, error){
															
															if(data !== null)
															{ 
																resolve(counter);
															}
													});
												};
											}
										}
										else
										{ counter -= 1; }
									});
								}
								else
								{ counter -= 1; }
							});
						})
					}],
					function (result, current) {
					
						if (counter === 0)
						{  return_result(arrParams);}
					});
				}
				else
				{ return_result(null); }
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{
			console.log(err);
			return_result(null);
		}
	}

	//------------------------------------------------------------------- jazecom Block Module End ---------------------------------------------------------------------------//

	//------------------------------------------------------------------- jazecom Followers Module Start ---------------------------------------------------------------------------//

	methods.getJazecomLinkAccountFollowList = function(arrParams, return_result){

		try
		{
			if(arrParams !== null)
			{	
				methods.__getJazecomContactFollowersGroups(arrParams, function(retrnGroups){

					if(retrnGroups !== null)
					{
							var query = "SELECT group_concat(account_id) as account_id FROM  `jaze_app_device_details` WHERE status = '1'  AND  app_type = '2' and   api_token = '"+arrParams.api_token+"' and  (id = '"+arrParams.app_id+"' or parent_id = '"+arrParams.app_id+"') ";

							DB.query(query, null, function(data, error){
				
									if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
				
										if(data[0].account_id !== null && data[0].account_id !== "")
										{
											var accountlist = [];
					
											if(data[0].account_id.toString().indexOf(',') != -1 ){
												accountlist = data[0].account_id.split(',');
											}
											else {
												accountlist.push(data[0].account_id);
											}

											var array_rows = [];

											var counter = accountlist.length;
										
											if(parseInt(counter) !== 0)
											{
												promiseForeach.each(accountlist,[function (account_list) {
									
													return new Promise(function (resolve, reject) {	

														if(parseInt(account_list) !== parseInt(arrParams.contact_account_id))
														{
															methods.__getJazecomContactFollowersStatus(retrnGroups,account_list, function(returnList){
															
																if(returnList !== null)
																{
																	var arrParamss = {account_id:account_list};
	
																	HelperGeneral.getAccountDetails(arrParamss, function(accRres){
	
																		if(accRres !== null)
																		{
																			var objAccRres = {
																				account_id			: accRres.account_id,
																				account_type		: accRres.account_type,
																				name				: accRres.name,
																				logo				: accRres.logo,
																				category_name 		: accRres.category,
																				category_title 		: accRres.title,
																				follow_status 		: returnList.follow_status,
																				follow_groups		: returnList.result
																			};
																			
																			array_rows.push(objAccRres);
	
																			counter -= 1;
													
																			resolve(array_rows);
																		}
																		else
																		{ counter -= 1; resolve(array_rows);}
																	});
																}
																else
																{ counter -= 1; resolve(array_rows);}
															});
														}
														else
														{ counter -= 1; resolve(array_rows);}
													});	
												}],
												function (result, current) {

													if (counter == 0)
													{ return_result(result[0]); }
												});	
											}
											else
											{return_result(null);}
										}
										else
										{return_result(null);}	
									}
									else
									{return_result(null);}
							});
					}
					else
					{ return_result = null;}
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{
			console.log(err);
			return_result(null);
		}
	};

	/*
	 * get Jazecom Contact Followers Groups
	 */
	methods.__getJazecomContactFollowersGroups = function(arrParams,return_result){

		try
		{
			var query = "SELECT id, group_name FROM  `jaze_follower_groups` WHERE status = '1' and  group_type = '1' and account_id = '"+arrParams.contact_account_id+"'  ";

					DB.query(query, null, function(data, error){
	
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
							var group_list = [];

							for(var i in data)
							{
								var obj = {
									group_id : data[i].id.toString(),
									group_name : data[i].group_name,
									group_status 	   : "0",
								};
								
								group_list.push(obj);
							}

							return_result(group_list);
						}
						else
						{return_result(null);}
					});
		}
		catch(err)
		{ console.log("erro __getJazecomContactFollowersGroups ",err); return_result(null); }
	};

	/*
	 * get Jazecom Contact Followers Status
	 */
	methods.__getJazecomContactFollowersStatus = function(groupList,account_id,return_result){

		try
		{
			if(parseInt(account_id) !== 0 && groupList !== null)
			{
				
				var follow_status = "0";
				
				//get user followe list
				var query  = " SELECT meta_value FROM jaze_follower_groups_meta where meta_key in ('follow_groups') and group_id IN  ";
					query += " (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'status' AND meta_value = '1' ";
					query += " and  group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'account_id'  AND meta_value = '"+account_id+"' )) ";

					DB.query(query, null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							var account_groupList = [];
					
							if(data[0].meta_value.toString().indexOf(',') != -1 ){
								account_groupList = data[0].meta_value.split(',');
							}
							else {
								account_groupList.push(data[0].meta_value);
							}

							var groups_array = [];

							groupList.forEach(function (items) {

								if(account_groupList.indexOf(items.group_id) != -1 ){

									var obj_group = {
										group_id : items.group_id.toString(),
										group_name : items.group_name,
										group_status : '1'
									};
								
									groups_array.push(obj_group);
		
									follow_status = "1";	
								}
								else
								{ groups_array.push(items); }
							});

							var obj = {
									follow_status : follow_status,
									result : groups_array
								};

							return_result(obj); 
						}
						else
						{ 
							var obj = {
								follow_status : follow_status,
								result : groupList
							};
							return_result(obj);  
						}
					});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("erro __getJazecomContactFollowersStatus ",err); return_result(null); }
	};

	/*
	 * Update Jazecom Contact Followers Status
	 */
	// methods.updateJazecomLinkAccountFollowStatus =  function(arrParams,return_result){

	// 	try
	// 	{
	// 		if(arrParams !== null)
	// 		{		
	// 			methods.__getJazecomContactFollowersGroups(arrParams, function(retrnGroups){

	// 				if(retrnGroups !== null)
	// 				{	
	// 					var list = [];

	// 					list = arrParams.account_list;

	// 					var counter = list.length;
					
	// 					promiseForeach.each(list,[function (lists) {

	// 						return new Promise(function (resolve, reject) {

	// 							 setTimeout(function() {

	// 							var group_query  = " SELECT  group_id FROM jaze_follower_groups_meta   ";
	// 								group_query += " where meta_key = 'follow_groups' and  group_id IN ";
	// 								group_query += " (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'account_id'  AND meta_value = '"+lists.account_id+"' ";
	// 								group_query += " and  group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'status'  AND meta_value = '1' ";
	// 								group_query += " and  group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'follow_account_id'  AND meta_value = '"+arrParams.contact_account_id+"' ))) ";
									
	// 								DB.query(group_query, null, function(data, error){

	// 									if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
											
	// 										if(parseInt(data[0].group_id) !== 0)
	// 										{
	// 											var query = "";

	// 											if(lists.group_id !== "")
	// 											{
	// 												var groupid_list = convert_array(lists.group_id);

	// 												query  = " UPDATE jaze_follower_groups_meta  SET meta_value  = '"+groupid_list.join()+"' where meta_key = 'follow_groups' and  group_id = '"+data[0].group_id+"' ";
	// 											}
	// 											else
	// 											{
	// 												//remove
	// 												query  = " DELETE FROM jaze_follower_groups_meta where group_id = '"+data[0].group_id+"' ";
	// 											}

	// 											DB.query(query, null, function(data, error){

	// 												counter -= 1;
	
	// 												resolve(counter);
	
	// 											});
	// 										}
	// 										else
	// 										{ counter -= 1; resolve(counter); }
	// 									}
	// 									else
	// 									{ 
	// 										if(lists.group_id !== "")
	// 										{
	// 											var query = " SELECT  CASE COALESCE(group_id, 0) WHEN  '0' THEN '1'  ELSE MAX(group_id) + 1   END as group_id FROM  jaze_follower_groups_meta ";

	// 												DB.query(query,null, function(dataG, error){ 

	// 													if(typeof dataG !== 'undefined' && dataG !== null  && dataG[0].group_id !== null  && parseInt(dataG.length) > 0){

	// 														var group_id = dataG[0].group_id;

	// 														var query = "  insert into jaze_follower_groups_meta (group_id, meta_key, meta_value) VALUES  ";
	// 														query += "  ('"+group_id+"','account_id','"+lists.account_id+"') ";
	// 														query += " ,('"+group_id+"','follow_account_id','"+arrParams.contact_account_id+"') ";
	// 														query += " ,('"+group_id+"','follow_groups','"+convert_array(lists.group_id).join()+"') ";
	// 														query += " ,('"+group_id+"','create_date','"+moment().utc().format('YYYY-MM-DD HH:mm:ss l')+"') ";
	// 														query += " ,('"+group_id+"','status','1') ";

	// 														DB.query(query,null, function(result,error){

	// 															if (error) {
	// 																console.log("err ",error);
	// 															}else{
	// 																if(result.insertId !== 0)
	// 																{
	// 																	counter -= 1;
	// 																	 resolve(counter);	
	// 																}
	// 															}
	// 														});				
	// 													}
	// 												});
	// 										}
	// 										else
	// 										{ counter -= 1; resolve(counter); }
	// 									 }
	// 								});		
	// 							 }, 5000);
											
	// 						});
	// 					}],
	// 					function (result, current) {
	// 						if (counter === 0)
	// 						{  return_result(arrParams);}
	// 					});
	// 				}
	// 				else
	// 				{ return_result(null); }
	// 			});
	// 		}
	// 		else
	// 		{ return_result(null); }
	// 	}
	// 	catch(err)
	// 	{
	// 		console.log(err);
	// 		return_result(null);
	// 	}
	// }


methods.updateJazecomLinkAccountFollowStatus =  function(arrParams,return_result){

	var arr_values = [];
	var obj_data = {};
	var delay_trigger = 0;
	var delay = 1000;
	try
	{
		if(arrParams !== null)
		{		
			methods.__getJazecomContactFollowersGroups(arrParams, function(retrnGroups){

				if(retrnGroups !== null)
				{	
					setTimeout(function() {

						var list = [];

						//console.log('account_list ',arrParams.account_list);

						list = arrParams.account_list;

						var counter = list.length;

						const forEach = async () => {
						  	const values_l = list;
						  	for (const val_l of values_l){

						  		var group_query  = " SELECT  group_id FROM jaze_follower_groups_meta   ";
									group_query += " where meta_key = 'follow_groups' and  group_id IN ";
									group_query += " (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'account_id'  AND meta_value = '"+val_l.account_id+"' ";
									group_query += " and  group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'status'  AND meta_value = '1' ";
									group_query += " and  group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'follow_account_id'  AND meta_value = '"+arrParams.contact_account_id+"' ))) ";
									
								DB.query(group_query, null, function(data, error){


									if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
											
										if(parseInt(data[0].group_id) !== 0)
										{
											var query = "";

											if(val_l.group_id !== "")
											{
												var groupid_list = convert_array(val_l.group_id);
												//update
												query  = " UPDATE jaze_follower_groups_meta  SET meta_value  = '"+groupid_list.join()+"' where meta_key = 'follow_groups' and  group_id = '"+data[0].group_id+"' ";
											}
											else
											{
												//remove
												query  = " DELETE FROM jaze_follower_groups_meta where group_id = '"+data[0].group_id+"' ";
											}

											DB.query(query, null, function(resQue, error){
												counter -= 1;
											});
										}
										else
										{
											counter -= 1;
										}
							
									}
									else
									{ 
										//insert
										obj_data = {
											account_id:        val_l.account_id,
											follow_account_id: arrParams.contact_account_id,
											follow_groups:     convert_array(val_l.group_id).join(),
											create_date:       moment().utc().format('YYYY-MM-DD HH:mm:ss l'),
											status:            1
										};

								

										arr_values.push(obj_data);
										counter -= 1;

										delay_trigger = 1;

								 	}
						
								});

						  	}

					  		const result = await returnData(counter);

						}

						const returnData = x => {
						  return new Promise((resolve, reject) => {
						  	if(delay_trigger == 1){
			  					if ( parseInt(arr_values.length) !== 0) {
						  			delay = parseInt(arr_values.length) * 1000;
					  		  	}
						  	}

						    setTimeout(() => {
						      resolve(x);
						    }, delay);
						  });
						}
						 
						forEach().then(() => {

						  	if(delay_trigger == 1){
			  					if ( parseInt(arr_values.length) >  1) {
						  			delay = parseInt(arr_values.length) * 1000;
					  		  	}
						  	}

						    setTimeout(() => {
				     			return_result(arrParams);
						    }, delay);
							
							methods.inSertMetaFollowers(arr_values, function(retrnGroups){	
								console.log('status insert '+ retrnGroups);
							});
			
						})

	    			}, 1000);

				}	
				else
				{ return_result(null); }
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}


methods.inSertMetaFollowers =  function(arrInsert,return_result){


	const timeoutPromise = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));
	
	const loopIntervalFunc = async () => {
		let results = [];

		for (let i = 0; i < arrInsert.length; i++) {
			await timeoutPromise(1000);

			var follow_groups = arrInsert[i]['follow_groups'];
			var rows = arrInsert[i];

			if(follow_groups!==""){

			 	var query = " SELECT  CASE COALESCE(group_id, 0) WHEN  '0' THEN '1'  ELSE MAX(group_id) + 1   END as group_id FROM  jaze_follower_groups_meta ";
				DB.query(query,null, function(dataG, error){ 
					var group_id = dataG[0].group_id;
				 	for (var key in rows) 
				 	{	

						DB.query('insert into jaze_follower_groups_meta (group_id, meta_key, meta_value) VALUES (?,?,?)',[group_id,key,rows[key]], function(data, error){
				 		 	if(data == null){
			 		 			results.push(0);
					 	   	}else{
					 	   		results.push(data.affectedRows);
		 	   					
				 	   		}
				 	 	});
				  	};

				});

			}
	


		}
		return_result(results);
		return results;
	}

	loopIntervalFunc();

}

//------------------------------------------------------------------- jazecom Followers Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- jazecom Module Active Inactive Section Start ---------------------------------------------------------------------------//

	/*
	 * Get Jazecom Link Account Status
	 */
	methods.getJazecomModuleStatus = function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{
				//get child account list
				var query = "SELECT account_id FROM  `jaze_app_device_details` WHERE status = '1'  AND  app_type = '2' and   api_token = '"+arrParams.api_token+"' and  (id = '"+arrParams.app_id+"' or parent_id = '"+arrParams.app_id+"') ";
	
				DB.query(query, null, function(data, error){
	
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
						var counter = data.length;
	
						var array_rows = [];
					
						if(parseInt(counter) !== 0)
						{
							promiseForeach.each(data,[function (account_list) {
					
								return new Promise(function (resolve, reject) {	
	
									methods.__getJazecomChildAccountModuleStatus(arrParams.account_id,account_list.account_id, function(accRres){
	
											if(accRres !== null)
											{ 

												array_rows.push(accRres);

												counter -= 1;
	
												resolve(array_rows);
											}
											else
											{counter -= 1; resolve(array_rows);}
										});
									})
								}],
								function (result, current) {
									if (counter === 0)
									{ return_result(result[0]); }
								});	
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }
					});
				}
				else
				{ return_result(null); }
			}
			catch(err)
			{ console.log("getHomeModuleAccountList",err); return_result(null); }
		};
	
		
		methods.__getJazecomChildAccountModuleStatus = function(master_account_id,account_id,return_result)
		{
			try
			{
				if(parseInt(account_id) !== 0)
				{
					var arrParams = {account_id:account_id};
	
					HelperGeneral.getAccountDetails(arrParams, function(accRres){
	
						if(accRres !== null)
						{ 
	
								var chat = "0";
								var mail = "0";
								var feed = "0";
								var diary = "0";
								var call = "2";
								
	
								//check is he admin or not
								methods.__getWorkAccountAdminMasterStatus(master_account_id,accRres, function(is_master){
	
									//get jazecom status							
									methods.___getJazecomAccountStatus(master_account_id,accRres, function(statusRes){	

										methods.__getJazeMailId(accRres.email,accRres.account_id, function(jazemailID){	

											if(statusRes !== null)
											{
												//Chat									
												if(parseInt(statusRes.jaze_chat) === 1)
												{ chat = "1"; }
	
												//Mail
												if(parseInt(jazemailID) !== 0)
												{
													if(parseInt(statusRes.jaze_mail) === 1)
													{ mail = "1"; }
												}
												else
												{ mail =  "0"; }
	
	
												//Feed						
												if(parseInt(statusRes.jaze_feed) === 1)
												{ feed = "1"; }
	
												//Diary
												if(parseInt(statusRes.jaze_diary) === 1)
												{ diary = "1"; }
	
												//Call
												// if(parseInt(statusRes.jaze_call) === 1)
												// { call = "1"; }
											}

											if(parseInt(accRres.account_type) === 2 || parseInt(accRres.account_type) === 4)
											{ feed = "2"; }

											var objAccRres = {
												account_id	  : accRres.account_id,
												account_type  : accRres.account_type,
												account_logo  : accRres.logo,
												access_status : is_master.toString(),
												chat 		: chat,
												mail		: mail,
												feed        : feed,
												diary       : diary,
												call        : call
											};

											return_result(objAccRres); 

										});							
									});
								});
						}
						else
						{ return_result(null); }
					});
				}
			}
			catch(err)
			{ console.log("__getJazecomChildAccountModuleStatus",err); return_result(null); }	
		};
			

	/*
	 * Update Jazecom Link Account Status
	 */
	methods.updateJazecomModuleStatus =  function(arrParams,return_result){

		try
		{
			if(arrParams !== null)
			{		
				var obj = arrParams.module_status;

				var counter = obj.length;
						
				if(parseInt(counter) !== 0)
				{
					promiseForeach.each(obj,[function (values) {
				
						return new Promise(function (resolve, reject) {
							
							if(parseInt(values.account_type) === 4)
							{
								var query = " UPDATE jaze_user_basic_details  SET meta_value  = '"+values.jaze_diary+"' where meta_key = 'team_diary_status' and  account_id = '"+values.account_id+"' and account_type = '4'; ";
							
								DB.query(query,null, function(data, error){ 

									if(data !== null){
										
										query = " UPDATE jaze_user_basic_details  SET meta_value  = '"+values.jaze_mail+"' where meta_key = 'team_mail_status' and  account_id = '"+values.account_id+"' and account_type = '4'; ";

										DB.query(query,null, function(data, error){ 

											if(data !== null){
												
												query = " UPDATE jaze_user_basic_details  SET meta_value  = '"+values.jaze_chat+"' where meta_key = 'team_chat_status' and  account_id = '"+values.account_id+"' and account_type = '4'; ";

												DB.query(query,null, function(data, error){ 

													if(data !== null){
														
														query = " UPDATE jaze_user_basic_details  SET meta_value  = '"+values.jaze_call+"' where meta_key = 'team_call_status' and  account_id = '"+values.account_id+"' and account_type = '4'; ";

														DB.query(query,null, function(data, error){ 

															if(data !== null){

																// query = " UPDATE jaze_user_basic_details  SET meta_value  = '"+values.jaze_feed+"' where meta_key = 'team_feed_status' and  account_id = '"+values.account_id+"' account_type = '4'; ";

																// DB.query(query,null, function(data, error){ 

																// 	if(data !== null){

																		resolve('1');
			
																		counter -= 1;
																// 	}
																// });
															}
														});
													}
												});
											}
										});
									}
								});
							}
							else
							{
								//Get group id
								var query ="  SELECT group_id FROM jaze_user_jazecome_status WHERE meta_key = 'account_id' AND meta_value ='"+values.account_id+"'; ";
					
								DB.query(query, null, function (data, error) {
				
									if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0 )
									{	
										var keys = Object.keys(values);
							
										for (var j = 0; j < keys.length; j++) {
										
											if(keys[j] !== "account_id" && keys[j] !== "account_type")
											{
												var query = " UPDATE jaze_user_jazecome_status  SET meta_value  = '"+values[keys[j]]+"' where meta_key = '"+keys[j]+"' and  group_id = '"+data[0].group_id+"'; ";
							
												DB.query(query,null, function(data_check, error){ });
											}
										}
	
										resolve('1');
	
										counter -= 1;
									}
									else
									{resolve('1'); counter -= 1;}
								});
							}
						});
					}],
					function (result, current) {	
																	
						if (counter === 0)
						{ return_result('1'); }
					});	
				}
				else
				{ return_result(null); }
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{
			console.log(err);
			return_result(null);
		}
	}


//------------------------------------------------------------------- jazecom Module Active Inactive Section End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Jazenet to Jazecom Module Start ---------------------------------------------------------------------------//


methods.getParentAccountDetails =  function(arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{		
			if(parseInt(arrParams.account_type) !== 1)
			{
				var query =" SELECT `user_id` FROM `jaze_user_account_details` WHERE "+arrParams.account_id+" in (prof_id,company_id,work_id) and status = '1' ";

				DB.query(query, null, function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0 && parseInt(data[0].group_id) !== 0)
					{	
						return_result(data[0].user_id); 
					}
					else
					{ return_result(0); }
				});
			}
			else
			{ return_result(arrParams.account_type); }
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

methods.setlogoutGetDeviceDetails = function(arrParams,return_result){

	try
	{
		if(parseInt(arrParams.app_id) !== 0)
		{
			//logout all acounts
			var query = " UPDATE `jaze_app_device_details` SET status = '0' WHERE status = '1' AND  app_type = '2' AND (id = '"+arrParams.app_id+"' or parent_id = '"+arrParams.app_id+"') ";

			DB.query(query,null, function(data, error){

				if(data !== null){

					//get device details
					var query = " SELECT  device_id,device_name,device_type,fcm_token FROM  jaze_app_device_details  WHERE id = '"+arrParams.app_id+"' ";
					
					DB.query(query,null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							return_result(data[0]); 
						}
						else
						{ return_result(null); }
					});
				}
				else
				{ return_result(null); }
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("setlogoutGetDeviceDetails",err); return_result(null); }	
};

//------------------------------------------------------------------- Jazenet to Jazecom Module Start ---------------------------------------------------------------------------//

// ------------------------------------------------------------------- General Search Module Start  --------------------------------------------------------------//   

methods.getCountResult = function(arrParams,return_result)
{
	var return_obj = {
			company_count      : "0",
			prof_count         : "0",
			prod_count         : "0",
			category_count     : "0",
			keywords_count     : "0"					                        
		};

	try
	{
		//company count
		methods.__getCountCompany(0,arrParams,0,function(comp_t_count){
			
			methods.__getCountProfile(0,arrParams,function(prof_t_count){
				
				methods.__getCountUser(0,arrParams,function(user_t_count){

					return_obj = {
						company_count       : comp_t_count.company_count.toString(),
						prof_count          : prof_t_count.profile_count.toString(),
						user_count          : user_t_count.user_count.toString()			                        
					};
					
					return_result(return_obj);
				});		
			});	
		});
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.__getCountCompany = function(flag,arrParams,keyword_flag,return_result){

	var return_obj = {
		company_count      : "0",
		company_id         : ""					                        
	};

	try{
		
		// flag  :  0 = total, 1= international,  2 = national,  3 = local, 4 = selected state result, 5 = international company , 6 = national company 

				var query  = " SELECT account_id FROM jaze_user_basic_details WHERE account_id not in ("+arrParams.link_account_list+") and ( account_type = 3 or account_type = 5)  ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";

					if(parseInt(keyword_flag) === 0)
					{
						var keyword = str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())));

						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('company_name')  AND LOWER(meta_value) REGEXP '^("+keyword+")') ";
					}

					if(parseInt(flag) !== 0)
					{
						if(parseInt(arrParams.country) !== 0 && parseInt(arrParams.city) !== 0 &&  parseInt(flag) === 1)
						{
							query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state+"') ";
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '0') ";
						}
						else if(parseInt(arrParams.country) !== 0 && parseInt(arrParams.city) !== 0 && parseInt(flag) === 2)
						{
							query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '0') ";
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";
						}
						else if(parseInt(arrParams.city) !== 0 && parseInt(flag) === 3)
						{
							query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') "; 
						}
						else if(parseInt(arrParams.geo_child_id) !== 0 &&  parseInt(flag) === 4)
						{
							query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.geo_child_id+"') ";
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '0') ";
						}
						else if(parseInt(arrParams.country) !== 0 &&  parseInt(flag) === 5)
						{
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '0') ";
						}
						else if(parseInt(arrParams.state) !== 0 &&  parseInt(flag) === 6)
						{
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state+"') ";
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '0') ";
						}
						else 
						{
							query += " and  account_id  = 0 ";
						}
					}

					query += " ) ";

					query += " group by account_id  ";

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
						var account_id_id_list = "";

						data.forEach(function(row){
							account_id_id_list += (account_id_id_list  == "") ? row.account_id: ','+row.account_id;	
						});

						return_obj = {
							company_count      : data.length,
							company_id         : account_id_id_list					                        
						};

						return_result(return_obj);
					}
					else
					{return_result(return_obj)}
				});
	}
	catch(err)
	{
		return_result(return_obj)
	}
};

methods.__getCountProfile = function(flag,arrParams,return_result){

	var return_obj = {
		profile_count      : "0",
		profile_id         : ""					                        
	};


	try{
			//get work profile details
			methods.___getCountWorkProfile(flag,arrParams,function(work_prof){

				var keyword = str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())));
		
				// flag  :  0 = total, 1= international,  2 = national,  3 = local, 4 = selected state result, 5 = international company , 6 = national company 

				var query  = " SELECT account_id FROM jaze_user_basic_details WHERE account_id not in ("+arrParams.link_account_list+") and  account_type = 2  ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('fname','lname')  AND LOWER(meta_value) like '%"+rtrim(ltrim(arrParams.keyword.toLowerCase()))+"%' ";
						
					if(parseInt(flag) !== 0)
					{	
						if(parseInt(arrParams.country) !== 0 &&  parseInt(arrParams.city) !== 0 && parseInt(flag) === 1)
						{
							query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";
						}
						else if(parseInt(arrParams.country) !== 0 && parseInt(arrParams.city) !== 0 && parseInt(flag) === 2)
						{
							query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";
						}
						else if(parseInt(arrParams.city) !== 0 && parseInt(flag) === 3)
						{
							query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";
						}
						else if(parseInt(arrParams.geo_child_id) !== 0 &&  parseInt(flag) === 4)
						{
							query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.geo_child_id+"') ";
						}
						else if(parseInt(arrParams.country) !== 0 &&  parseInt(flag) === 5)
						{
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
						}
						else if(parseInt(arrParams.state) !== 0 &&  parseInt(flag) === 6)
						{
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state+"') ";
						}
						else 
						{
							query += " and  account_id  = 0 ";
						}
					}

					query += " )) ";

					query += " group by account_id  ";
					
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
						var profile_account_id_id_list = "";

						var profile_account_count = data.length;

						data.forEach(function(row){
							profile_account_id_id_list += (profile_account_id_id_list  == "") ? row.account_id: ','+row.account_id;	
						});

							return_obj = {
								profile_count      : parseInt(profile_account_count) +  parseInt(work_prof.profile_count),
								profile_id         : (work_prof.profile_id  == "") ? profile_account_id_id_list : profile_account_id_id_list+','+work_prof.profile_id					                        
							};

							return_result(return_obj);
					}
					else
					{ 
						return_obj = {
							profile_count      : work_prof.profile_count,
							profile_id         : work_prof.profile_id					                        
						};

						return_result(return_obj); 
					}
				});
			});
	}
	catch(err)
	{ return_result(return_obj); }
};

methods.___getCountWorkProfile = function(flag,arrParams,return_result){

	var return_obj = {
		profile_count      : "0",
		profile_id         : ""					                        
	};


	try{
			//	var keyword = str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())));

				var query  = " SELECT account_id FROM jaze_user_basic_details WHERE account_id not in ("+arrParams.link_account_list+") and  account_type = 4  ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'team_status'  AND meta_value = '1' ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'show_in_team'  AND meta_value = '1' ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('team_fname','team_lname')  AND LOWER(meta_value) like '%"+rtrim(ltrim(arrParams.keyword.toLowerCase()))+"%' ";

					  if(parseInt(flag) !== 0)
					  {	
						  if(parseInt(arrParams.country) !== 0 &&  parseInt(arrParams.city) !== 0 && parseInt(flag) === 1)
						  {
							  query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'team_country_id'  AND meta_value = '"+arrParams.country+"') ";
							  query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'team_city_id'  AND meta_value = '"+arrParams.city+"') ";
						  }
						  else if(parseInt(arrParams.country) !== 0 && parseInt(arrParams.city) !== 0 && parseInt(flag) === 2)
						  {
							  query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'team_country_id'  AND meta_value = '"+arrParams.country+"') ";
							  query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'team_city_id'  AND meta_value = '"+arrParams.city+"') ";
						  }
						  else if(parseInt(arrParams.city) !== 0 && parseInt(flag) === 3)
						  {
							  query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'team_city_id'  AND meta_value = '"+arrParams.city+"') ";
						  }
						  else if(parseInt(arrParams.geo_child_id) !== 0 &&  parseInt(flag) === 4)
						  {
							  query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'team_state_id'  AND meta_value = '"+arrParams.geo_child_id+"') ";
						  }
						  else if(parseInt(arrParams.country) !== 0 &&  parseInt(flag) === 5)
						  {
							  query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'team_country_id'  AND meta_value = '"+arrParams.country+"') ";
						  }
						  else if(parseInt(arrParams.state) !== 0 &&  parseInt(flag) === 6)
						  {
							  query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'team_country_id'  AND meta_value = '"+arrParams.country+"') ";
							  query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'team_state_id'  AND meta_value = '"+arrParams.state+"') ";
						  }
						  else 
						  {
							  query += " and  account_id  = 0 ";
						  }
					  }

					query += " ))) ";
					query += " group by account_id  ";
					
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
						var profile_account_id_id_list = "";

						var profile_account_count = data.length;

						data.forEach(function(row){
							profile_account_id_id_list += (profile_account_id_id_list  == "") ? row.account_id: ','+row.account_id;	
						});

						return_obj = {
							profile_count      : profile_account_count,
							profile_id         : profile_account_id_id_list					                        
						};

						return_result(return_obj);
					}
					else
					{ return_result(return_obj); }
				});
	}
	catch(err)
	{ return_result(return_obj); }
};

methods.__getCountUser = function(flag,arrParams,return_result){

	var return_obj = {
		user_count      : "0",
		user_id         : ""					                        
	};


	try{
		
		//var keyword = str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())));

				// flag  :  0 = total, 1= international,  2 = national,  3 = local, 4 = selected state result, 5 = international company , 6 = national company 

				var query  = " SELECT account_id FROM jaze_user_basic_details WHERE account_id not in ("+arrParams.link_account_list+") and  account_type = 1  ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";
					//query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('fname','lname')  AND LOWER(meta_value) REGEXP '^("+keyword+")' ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('fname','lname')  AND LOWER(meta_value) like '%"+rtrim(ltrim(arrParams.keyword.toLowerCase()))+"%' ";

					if(parseInt(flag) !== 0)
					{	
						if(parseInt(arrParams.country) !== 0 &&  parseInt(arrParams.city) !== 0 && parseInt(flag) === 1)
						{
							query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";
						}
						else if(parseInt(arrParams.country) !== 0 && parseInt(arrParams.city) !== 0 && parseInt(flag) === 2)
						{
							query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";
						}
						else if(parseInt(arrParams.city) !== 0 && parseInt(flag) === 3)
						{
							query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";
						}
						else if(parseInt(arrParams.geo_child_id) !== 0 &&  parseInt(flag) === 4)
						{
							query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.geo_child_id+"') ";
						}
						else if(parseInt(arrParams.country) !== 0 &&  parseInt(flag) === 5)
						{
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
						}
						else if(parseInt(arrParams.state) !== 0 &&  parseInt(flag) === 6)
						{
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
							query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state+"') ";
						}
						else 
						{
							query += " and  account_id  = 0 ";
						}
					}

					query += " )) ";

					query += " group by account_id  ";
					
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
						var user_account_id_list = "";

						data.forEach(function(row){
							user_account_id_list += (user_account_id_list  == "") ? row.account_id: ','+row.account_id;	
						});

							return_obj = {
								user_count      : data.length,
								user_id         : user_account_id_list					                        
							};

							return_result(return_obj);
					}
					else
					{  return_result(return_obj);  }
			});
	}
	catch(err)
	{ return_result(return_obj); }
};

methods.__getAllLinkAccountList = function(arrParams,return_result){

	try
	{
		var query = "SELECT group_concat(account_id) as account_id FROM  `jaze_app_device_details` WHERE status = '1'  AND  app_type = '2' and   api_token = '"+arrParams.api_token+"' ";
		
				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						if(data[0].account_id !== null && data[0].account_id !== "")
						{
							return_result(data[0].account_id);
						}
						else
						{return_result(null);}	
					}
					else
					{return_result(null);}
				});
	}
	catch(err)
	{ console.log("erro __getAllLinkAccountList ",err); return_result(null); }
};

// ------------------------------------------------------  Search Company  Module Start  --------------------------------------------------------------//   

methods.getAutofillCompanyResults = function(arrParams,return_result)
{
	var return_obj = {
		result : null,
		tot_count : "0",
		tot_page : "0"
	};

	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 1 && (parseInt(arrParams.geo_type) === 3 || parseInt(arrParams.geo_type) === 4 || parseInt(arrParams.geo_type) === 5 || parseInt(arrParams.geo_type) === 6))
		{	
				methods.__getCountCompany(arrParams.geo_type,arrParams,0,function(comp_loc_count){
					
					if(comp_loc_count.company_id != "")
					{
						var start = '0';

						if(parseInt(arrParams.limit) !== 0)
						{  start = parseInt(arrParams.limit) * 30; }
						
						var company_array = (comp_loc_count.company_id.split(",")).splice(start, 30); 

						var query  = " SELECT * FROM jaze_user_basic_details WHERE ( account_type = 3 or account_type = 5)  and account_id in ("+company_array.join()+") and  meta_key IN ('company_name','company_logo','category_id','city_id','area_id') ";
						
						DB.query(query, null, function (data, error) {

							if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
							{ 
								methods.convertAccountMultiMetaArray(data, function(data_result){
									
									var counter = data_result.length;
									
									var array_rows = [];

									if(parseInt(counter) !== 0)
									{
										promiseForeach.each(data_result,[function (list) {
							
											return new Promise(function (resolve, reject) {
				
												methods.__getCompanyCategoryName(list, function(category){	

													methods.__getLocationDetails(list.area_id,list.city_id, function(location_details){	

														var default_logo = '' + G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png'+'';

														if(list.company_logo !== "" && list.company_logo !== null)
														{
															default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+list.account_id+'/logo/' + list.company_logo + '';
														}

														var newObj = {
															account_id	  : list.account_id.toString(),
															account_type  : list.account_type.toString(),
															name		  : list.company_name.toString(),
															logo		  : default_logo.toString(),
															title	  	  : category.title_name.toString(),
															category	  : category.category_name.toString(),
															area_name	  : location_details.area_name,
															city_name	  : location_details.city_name,
															detail_flag   : "1"
														};
								
														array_rows.push(newObj);
																								
														counter -= 1;
																								
														resolve(array_rows);
													});
												});
											});
										}],
										function (result, current) {
											
											if (counter === 0)
											{  
												var obj = {
													result : result[0],
													tot_count : comp_loc_count.company_count.toString(),
													tot_page  : Math.ceil(parseInt(comp_loc_count.company_count) / 30)
												}

												return_result(obj); 
											}
										});
									}
									else
									{ return_result(return_obj); }	
								});
							}
							else
							{ return_result(return_obj); }
						});
					}
					else
					{ return_result(return_obj); }
				});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getCompanyLocalCount = function(arrParams,return_result)
{
	var return_obj = {
			local_count      	 : "0",
			national_count       : "0",
			international_count  : "0"					                        
		};

	try
	{
		//company count
		methods.__getCountCompany(3,arrParams,0,function(comp_loc_count){
			
			methods.__getCountCompany(2,arrParams,0,function(comp_nat_count){

				methods.__getCountCompany(5,arrParams,0,function(comp_inter_count){

					return_obj = {
						local_count             : comp_loc_count.company_count.toString(),
						national_count          : comp_nat_count.company_count.toString(),
						international_count     : comp_inter_count.company_count.toString()				                        
					};
								
					return_result(return_obj);

				});							
			});	
		});
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getAutofillCompanyInternationalNationalResults = function(arrParams,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 1 && (parseInt(arrParams.geo_type) === 1 || parseInt(arrParams.geo_type) === 2))
		{
			if(parseInt(arrParams.geo_type) === 1)
			{
				//International Result
				methods.__getCountCompany(5,arrParams,0,function(comp_count){

					if(comp_count.company_id != "")
					{
						methods.__getCompanyCountryList(comp_count.company_id,function(comp_inter_count){

							if(comp_inter_count !== null)
							{ 
								return_result(comp_inter_count,[]);
							}
							else
							{ return_result([],[]); }
						});
					}
					else
					{ return_result([],[]); }
				});
			}
			else if(parseInt(arrParams.geo_type) === 2)
			{
				//National Result
				methods.__getCountCompany(2,arrParams,0,function(comp_count){

					if(comp_count.company_id != "")
					{
						methods.__getCompanyStateList(comp_count.company_id,arrParams.country,arrParams.city,1,function(comp_nation_count){

							if(comp_nation_count !== null)
							{ 
								return_result([],comp_nation_count);
							}
							else
							{ return_result([],[]); }
						});
					}
					else
					{ return_result([],[]); }
				});
			}
			else
			{ return_result([],[]); }
		}
		else
		{ return_result([],[]); }
	}
	catch(err)
	{
		console.log(err);
		return_result([],[]);
	}	
}

//Get Company Country List
methods.__getCompanyCountryList = function (account_list,return_result){
	
	try
	{
		if(account_list !== null)
		{
			var query  = " SELECT meta_value as country_id, (SELECT `country_name` FROM `country` WHERE `status` = '1' and `id` = meta_value) as country_name  FROM jaze_user_basic_details WHERE  account_id in ("+account_list+") and meta_key in ('country_id') ";
			    query += " and account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key = 'status' and meta_value = '1') ";
				
				DB.query(query, null, function(arrvalues, error){
					
					if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0){

						var array_rows = [];

						var counter = arrvalues.length;
						
						if(parseInt(counter) !== 0)
						{
								promiseForeach.each(arrvalues,[function (list) {
									
									return new Promise(function (resolve, reject) {

										methods.__getCompanyStateList(account_list,list.country_id,0,0,function(comp_state_list){

											if(list.country_name !== "" && list.country_name !== null)
											{
												var index = __getCountryGroupIndex(array_rows,list.country_id,'country_id');

												if(index != ""){

													var count = parseInt(array_rows[index].country_count) + 1;
							
													Object.assign(array_rows[index], {country_count :  count.toString()});
												}
												else {
							
													var obj_return = {
															country_id    :  list.country_id.toString(),
															country_name  :  list.country_name,
															country_count :  "1",
															state_list	  : comp_state_list
														};
																	
													array_rows.push(obj_return);
												}
											}

											counter -= 1;

											resolve(array_rows);
										
										});	
									});
								}],
								function (result, current) {
									
									if (counter === 0)
									{ 
										var resul = result[0];

											fast_sort(resul).asc('country_name'); 

											var obj_return = {
												country_id    :  "0",
												country_name  :  "world",
												country_count :  arrvalues.length.toString(),
												state_list	  : []
												};
				
											resul.unshift(obj_return);

											return_result(resul); 
									}
								});	
						}
						else
						{return_result(null)}
					}
					else
					{ return_result(null); }
				});
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("__getCompanyCountryList",err); return_result(null); }	
};

//Get Company State List
methods.__getCompanyStateList = function (account_list,country_id,city_id,all_status,return_result){

	try
	{
		if(account_list !== null)
		{
			var query  = " SELECT meta_value as state_id, (SELECT `state_name` FROM `states` WHERE `status` = '1' and `id` = meta_value) as state_name  FROM jaze_user_basic_details WHERE  account_id in ("+account_list+") and meta_key in ('state_id') ";
			    query += " and account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key = 'status' and meta_value = '1') ";
				
				if(parseInt(country_id) !== 0)
				{
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+country_id+"') ";
				}

				if(parseInt(city_id) !== 0)
				{
					query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+city_id+"') ";
				}

				DB.query(query, null, function(arrvalues, error){
					
					if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0){

						if(parseInt(arrvalues[0].state_id) !== 0)
						{
							var list = [];

							for (var i in arrvalues) 
							{
								var index = __getCountryGroupIndex(list,arrvalues[i].state_id,'state_id');

								if(index != ""){

									var count = parseInt(list[index].state_count) + 1;

									Object.assign(list[index], {state_count :  count.toString()});
								}
								else {

									var obj_return = {
										state_id :  arrvalues[i].state_id.toString(),
										state_name :  arrvalues[i].state_name,
										state_count :  "1"
										};

									list.push(obj_return);
								}								
							}

							fast_sort(list).asc('state_name');

							if(parseInt(all_status) === 1)
							{
								var obj_return = {
										state_id    :  "0",
										state_name  :  "all",
										state_count :  arrvalues.length.toString()
								};

								list.unshift(obj_return);
							}

							return_result(list);
						}
						else
						{ return_result([]); }
					}
					else
					{ return_result([]); }
				});
		}
		else
		{
			return_result([]);
		}
	}
	catch(err)
	{ console.log("__getCompanyStateList",err); return_result([]); }	
};

function __getCountryGroupIndex(list_array,index_value,fild_key)
{
	var retern_index = "";

		if(list_array !== null)
		{
			Object.keys(list_array).forEach(function(prop) {

				var value = "";

				if(fild_key === "country_id")
				{ value = list_array[prop].country_id; }
				else if(fild_key === "state_id")
				{ value = list_array[prop].state_id; }
				else if(fild_key === "keyword")
				{ value = list_array[prop].keyword; }
				
 
				if(parseInt(value) === parseInt(index_value))
				{
					retern_index = prop;
				}
			  });
		}
		
		return retern_index;
}

//Get Company Category Name 
methods.__getCompanyCategoryName =  function (account_list,return_result){
	
	var return_obj = {
			account_type  : "",
			title_name 	  : "",
			category_name : ""
		};

	try
	{
		if(account_list !== null)
		{
			if(parseInt(account_list.account_type) === 1)
			{
				return_obj = {
					account_type  : "1",
					title_name 	  : "personal",
					category_name : "personal"
				};

				return_result(return_obj);
			}
			else if(parseInt(account_list.account_type) === 2)
			{
				return_obj = {
					account_type  : "2",
					title_name 	  : "professional",
					category_name : "professional"
				};

				return_result(return_obj);
			}
			else if(parseInt(account_list.account_type) === 3)
			{
				if((Object.keys(account_list)).indexOf("category_id") !== -1)
				{
					var query = "  SELECT meta_value  FROM jaze_category_list WHERE  meta_key = 'cate_name' and group_id = '"+account_list.category_id+"'; ";
					
					DB.query(query, null, function(data, error){
					
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_obj = {
								account_type  : "3",
								title_name 	  : "company",
								category_name : data[0].meta_value
							};

							return_result(return_obj);
						}
						else
						{
							return_obj = {
								account_type  : "3",
								title_name 	  : "company",
								category_name : "company"
							};

							return_result(return_obj);
						}
					});
				}
				else
				{
					return_obj = {
						account_type  : "3",
						title_name 	  : "company",
						category_name : "company"
					};

					return_result(return_obj);
				}
			}
			else if(parseInt(account_list.account_type) === 5)
			{
				return_obj = {
					account_type  : "5",
					title_name 	  : "government",
					category_name : "government"
				};

				return_result(return_obj);
			}
		}
		else
		{
			return_result(return_obj);
		}
	}
	catch(err)
	{ console.log("__getCompanyCategoryName",err); return_result(return_obj); }	
};

methods.__getLocationDetails = function(area_id,city_id,return_result){

	try{
			var obj_return = {
				country_id : "0",
				country_name : "",
				state_id : "0",
				state_name : "",
				city_id : "0",
				city_name : "",
				area_id : "0",
				area_name : "",
			};

			if(parseInt(area_id) !== 0)
			{
				var query  =" SELECT  con.id as country_id,con.country_name ,st.id as state_id,st.state_name,city.id as city_id,city.city_name,ar.area_id,ar.area_name ";
					query +=" FROM area ar  ";
					query +=" INNER JOIN cities city ON city.id = ar.city_id ";
                    query +=" INNER JOIN states st ON st.id = city.state_id ";
                    query +=" INNER JOIN country con ON con.id=st.country_id ";
					query +=" WHERE ar.status = '1' and city.status = '1' and st.status = '1' and con.status = '1' and ar.area_id = '"+area_id+"' " ;
 
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0]);
					}
					else
					{return_result(obj_return)}
				});
			}
			else if(parseInt(city_id) !== 0)
			{
				var query  =" SELECT  con.id as country_id,con.country_name ,st.id as state_id,st.state_name,city.id as city_id,city.city_name, '0' as area_id, '' as area_name ";
					query +=" FROM cities city  ";
                    query +=" INNER JOIN states st ON st.id = city.state_id ";
                    query +=" INNER JOIN country con ON con.id=st.country_id ";
					query +=" WHERE city.status = '1' and st.status = '1' and con.status = '1' and city.id = '"+city_id+"' " ;
 
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0]);
					}
					else
					{return_result(obj_return)}
				});
			}
			else
			{ return_result(obj_return); }

			
	}
	catch(err)
	{
		return_result(obj_return)
	}
};

// ------------------------------------------------------  Search Company  Module End  --------------------------------------------------------------//   
// ------------------------------------------------------  Search Profile  Module Start  --------------------------------------------------------------// 

methods.getAutofillProfileResults = function(arrParams,return_result)
{
	var return_obj = {
		result : null,
		tot_count : "0",
		tot_page : "0"
	};

	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 2 && (parseInt(arrParams.geo_type) === 3 || parseInt(arrParams.geo_type) === 4 || parseInt(arrParams.geo_type) === 5 || parseInt(arrParams.geo_type) === 6))
		{	
				methods.__getCountProfile(arrParams.geo_type,arrParams,function(prof_loc_count){
					
					if(prof_loc_count.profile_id != "")
					{
						var start = '0';

						if(parseInt(arrParams.limit) !== 0)
						{  start = parseInt(arrParams.limit) * 30; }
						
						var prof_array = (prof_loc_count.profile_id.split(",")).splice(start, 30); 

						var query  = " SELECT * FROM jaze_user_basic_details WHERE account_id in ("+prof_array.join()+") and  meta_key IN ('fname','lname','professional_logo','job_title','city_id','area_id','team_company_id') ";
						
						DB.query(query, null, function (data, error) {

							if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
							{ 
								methods.convertAccountMultiMetaArray(data, function(data_result){
									
									var counter = data_result.length;
									
									var array_rows = [];

									if(parseInt(counter) !== 0)
									{
										promiseForeach.each(data_result,[function (list) {
							
											return new Promise(function (resolve, reject) {

													if(parseInt(list.account_type) === 4)
													{
														methods.___getWorkAccountDetails(list.account_id, function(work_details){	

															if(work_details !== null)
															{ array_rows.push(work_details); }
																										
															counter -= 1;
																									
															resolve(array_rows);

														});
													}
													else
													{
														methods.__getLocationDetails(list.area_id,list.city_id, function(location_details){	

																var default_logo = '' + G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png'+'';

																if(list.professional_logo !== "" && list.professional_logo !== null)
																{
																	default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/professional_account/'+list.account_id+'/logo_thumb/thumb_' + list.professional_logo + '';
																}

																var title = "professional";

																if(typeof list.job_title !== 'undefined' && list.job_title !== null &&  list.job_title !== "" )
																{ title = list.job_title.toString(); }

																var newObj = {
																	account_id	  : list.account_id.toString(),
																	account_type  : list.account_type.toString(),
																	name		  : (list.fname +' '+list.lname).toString(),
																	logo		  : default_logo.toString(),
																	title	  	  : title,
																	area_name	  : location_details.area_name,
																	city_name	  : location_details.city_name,
																	detail_flag   : "2"
																};
										
																array_rows.push(newObj);
																										
																counter -= 1;
																										
																resolve(array_rows);
															});
													}
												});
											}],
											function (result, current) {
												
												if (counter === 0)
												{  
													var obj = {
														result : result[0],
														tot_count : prof_loc_count.profile_count.toString(),
														tot_page  : Math.ceil(parseInt(prof_loc_count.profile_count) / 30)
													}

													return_result(obj); 
												}
											});
									}
									else
									{ return_result(return_obj); }	
								});
							}
							else
							{ return_result(return_obj); }
						});
					}
					else
					{ return_result(return_obj); }
				});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getProfileLocalCount = function(arrParams,return_result)
{
	var return_obj = {
			local_count      	 : "0",
			national_count       : "0",
			international_count  : "0"					                        
		};

	try
	{
		//profile count
		methods.__getCountProfile(3,arrParams,function(prof_loc_count){
			
			methods.__getCountProfile(2,arrParams,function(prof_nat_count){

				methods.__getCountProfile(5,arrParams,function(prof_inter_count){

					return_obj = {
						local_count             : prof_loc_count.profile_count.toString(),
						national_count          : prof_nat_count.profile_count.toString(),
						international_count     : prof_inter_count.profile_count.toString()				                        
					};
						
					return_result(return_obj);

				});							
			});	
		});
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getAutofillProfileInternationalNationalResults = function(arrParams,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 2 && (parseInt(arrParams.geo_type) === 1 || parseInt(arrParams.geo_type) === 2))
		{
			if(parseInt(arrParams.geo_type) === 1)
			{
				//International Result
				methods.__getCountProfile(5,arrParams,function(prof_count){

					if(prof_count.profile_id != "")
					{
						methods.__getCompanyCountryList(prof_count.profile_id,function(prof_inter_count){

							if(prof_inter_count !== null)
							{ 
								return_result(prof_inter_count,[]);
							}
							else
							{ return_result([],[]); }
						});
					}
					else
					{ return_result([],[]); }
				});
			}
			else if(parseInt(arrParams.geo_type) === 2)
			{
				//National Result
				methods.__getCountProfile(2,arrParams,function(prof_count){

					if(prof_count.profile_id != "")
					{
						methods.__getCompanyStateList(prof_count.profile_id,arrParams.country,arrParams.city,1,function(prof_nation_count){

							if(prof_nation_count !== null)
							{ 
								return_result([],prof_nation_count);
							}
							else
							{ return_result([],[]); }
						});
					}
					else
					{ return_result([],[]); }
				});
			}
			else
			{ return_result([],[]); }
		}
		else
		{ return_result([],[]); }
	}
	catch(err)
	{
		console.log(err);
		return_result([],[]);
	}	
}

methods.___getWorkAccountDetails = function(account_id,return_result)
{
	try
		{
			if(parseInt(account_id) !== 0)
			{		
				var query = " SELECT *  FROM jaze_user_basic_details WHERE meta_key in ('team_fname','team_lname','team_display_pic','team_position','team_company_id') and  account_type = '4'  and account_id = '" + account_id + "' ";
					query += " and account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key = 'show_in_team' and meta_value = '1' ";
					query += " and account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key = 'team_status' and meta_value = '1')) ";

				DB.query(query, null, function(data, error){
		
					if (typeof data[0] !== 'undefined' && data[0] !== null)
					{
						var list = [];
			
						Object.assign(list, {account_id : data[0].account_id});
						Object.assign(list, {account_type : data[0].account_type});

						for (var i in data) 
						{
							Object.assign(list, {[data[i].meta_key]:  data[i].meta_value});
						}

						var accParams = { account_id  : list.team_company_id };

						HelperGeneral.getAccountDetails(accParams, function(compAccRres){	

							var newObj = {
								account_id	  : list.account_id.toString(),
								account_type  : list.account_type.toString(),
								name		  : (list.team_fname +' '+list.team_lname).toString(),
								logo		  : G_STATIC_IMG_URL + 'uploads/jazenet/'+list.team_display_pic,
								title	  	  : compAccRres.name,
								area_name	  : compAccRres.area_name,
								city_name	  : compAccRres.city_name,
								detail_flag   : "4"
							};
	
							return_result(newObj);

						});
					}
					else
					{ return_result(null); }
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("___getWorkAccountDetails",err); return_result(null); }	
}

// ------------------------------------------------------  Search Profile  Module End  --------------------------------------------------------------// 
// ------------------------------------------------------  Search User  Module Start  --------------------------------------------------------------// 

methods.getAutofillUserResults = function(arrParams,return_result)
{
	var return_obj = {
		result : null,
		tot_count : "0",
		tot_page : "0"
	};

	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 3 && (parseInt(arrParams.geo_type) === 3 || parseInt(arrParams.geo_type) === 4 || parseInt(arrParams.geo_type) === 5 || parseInt(arrParams.geo_type) === 6))
		{	
				methods.__getCountUser(arrParams.geo_type,arrParams,function(user_loc_count){
					
					if(user_loc_count.user_id != "")
					{
						var start = '0';

						if(parseInt(arrParams.limit) !== 0)
						{  start = parseInt(arrParams.limit) * 30; }
						
						var prof_array = (user_loc_count.user_id.split(",")).splice(start, 30); 

						var query  = " SELECT * FROM jaze_user_basic_details WHERE account_id in ("+prof_array.join()+") and  meta_key IN ('fname','lname','profile_logo','city_id','area_id') ";
						
						DB.query(query, null, function (data, error) {

							if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
							{ 
								methods.convertAccountMultiMetaArray(data, function(data_result){
									
									var counter = data_result.length;
									
									var array_rows = [];

									if(parseInt(counter) !== 0)
									{
										promiseForeach.each(data_result,[function (list) {
							
											return new Promise(function (resolve, reject) {

												methods.__getLocationDetails(list.area_id,list.city_id, function(location_details){	

													var default_logo = '' + G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png'+'';

													if(list.professional_logo !== "" && list.professional_logo !== null)
													{
														default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + list.profile_logo + '';
													}

													var newObj = {
														account_id	  : list.account_id.toString(),
														account_type  : list.account_type.toString(),
														name		  : (list.fname +' '+list.lname).toString(),
														logo		  : default_logo.toString(),
														title	  	  : "personal",
														area_name	  : location_details.area_name,
														city_name	  : location_details.city_name,
														detail_flag   : "3"
													};
							
													array_rows.push(newObj);
																							
													counter -= 1;
																							
													resolve(array_rows);
												});
													
											});
										}],
										function (result, current) {
												
											if (counter === 0)
											{  
												var obj = {
													result : result[0],
													tot_count : user_loc_count.user_count.toString(),
													tot_page  : Math.ceil(parseInt(user_loc_count.user_count) / 30)
												}

												return_result(obj); 
											}
										});
									}
									else
									{ return_result(return_obj); }	
								});
							}
							else
							{ return_result(return_obj); }
						});
					}
					else
					{ return_result(return_obj); }
				});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getUserLocalCount = function(arrParams,return_result)
{
	var return_obj = {
			local_count      	 : "0",
			national_count       : "0",
			international_count  : "0"					                        
		};

	try
	{
		//profile count
		methods.__getCountUser(3,arrParams,function(prof_loc_count){
			
			methods.__getCountUser(2,arrParams,function(prof_nat_count){

				methods.__getCountUser(5,arrParams,function(prof_inter_count){

					return_obj = {
						local_count             : prof_loc_count.user_count.toString(),
						national_count          : prof_nat_count.user_count.toString(),
						international_count     : prof_inter_count.user_count.toString()				                        
					};
						
					return_result(return_obj);

				});							
			});	
		});
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getAutofillUserInternationalNationalResults = function(arrParams,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 3 && (parseInt(arrParams.geo_type) === 1 || parseInt(arrParams.geo_type) === 2))
		{
			if(parseInt(arrParams.geo_type) === 1)
			{
				//International Result
				methods.__getCountUser(5,arrParams,function(prof_count){

					if(prof_count.profile_id != "")
					{
						methods.__getCompanyCountryList(prof_count.user_id,function(prof_inter_count){

							if(prof_inter_count !== null)
							{ 
								return_result(prof_inter_count,[]);
							}
							else
							{ return_result([],[]); }
						});
					}
					else
					{ return_result([],[]); }
				});
			}
			else if(parseInt(arrParams.geo_type) === 2)
			{
				//National Result
				methods.__getCountUser(2,arrParams,function(prof_count){

					if(prof_count.user_id != "")
					{
						methods.__getCompanyStateList(prof_count.user_id,arrParams.country,arrParams.city,1,function(prof_nation_count){

							if(prof_nation_count !== null)
							{ 
								return_result([],prof_nation_count);
							}
							else
							{ return_result([],[]); }
						});
					}
					else
					{ return_result([],[]); }
				});
			}
			else
			{ return_result([],[]); }
		}
		else
		{ return_result([],[]); }
	}
	catch(err)
	{
		console.log(err);
		return_result([],[]);
	}	
}

// ------------------------------------------------------  Search User  Module End  --------------------------------------------------------------// 
methods.convertAccountMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getAccountGroupIndex(list,arrvalues[i].account_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							account_type : arrvalues[i].account_type,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};

function __getAccountGroupIndex(list_array,index_value)
{
	var retern_index = "";

		if(list_array !== null)
		{
			Object.keys(list_array).forEach(function(prop) {

				var value = list_array[prop].account_id;
 
				if(parseInt(value) === parseInt(index_value))
				{
					retern_index = prop;
				}
			  });
		}
		
		return retern_index;
}



// ------------------------------------------------------------------- General Search Module Start  --------------------------------------------------------------//   


module.exports = methods;
