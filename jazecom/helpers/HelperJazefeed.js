const methods = {};
const DB = require('./../../helpers/db_base.js');
const promiseForeach = require('promise-foreach');

const HelperRestriction = require('../../helpers/HelperRestriction.js');
const HelperGeneral = require('../../helpers/HelperGeneral.js');
const HelperNotification = require('./HelperNotification.js');

const fs = require('fs');
const moment = require("moment");
const urlJazeFeed = G_STATIC_IMG_URL+"uploads/jazenet/jazefeed/";
const imagePath = G_STATIC_IMG_PATH+"jazefeed/";


//---------------------------------------------------------------------------  Check Feed Active  Status Module Start ------------------------------------------------------------------------------//

	/*
	 * save or Reject to jazecome contact table
	 */
	methods.checkAccountJazecomFeedStatus =  function(arrParams,return_result){

		try
		{
			var account_id = arrParams.account_id;

			var request_account_id = arrParams.company_account_id;

			methods.__checkJazeFeedActiveStatus(account_id, function (ownerStatus) {

				if(parseInt(ownerStatus) === 1)
				{
					methods.__checkJazeFeedActiveStatus(request_account_id, function (recivStatus) {

						if(parseInt(recivStatus) === 1)
						{
							methods.__checkJazeFeedBlockStatus(account_id,request_account_id, function (ownerBlockStatus) {

								if(parseInt(ownerBlockStatus) === 1)
								{
									methods.__checkJazeFeedBlockStatus(request_account_id,account_id, function (recivBlockStatus) {

										if(parseInt(recivBlockStatus) === 1)
										{ return_result("1"); }
										else
										{ return_result("0"); }
									});
								}
								else
								{ return_result("0"); }
							});
						}
						else
						{ return_result("0"); }
					});
				}
				else
				{ return_result("0"); }
			});
		}
		catch(err)
		{
			console.log(err);
			return_result(null);
		}
	}

	methods.__checkJazeFeedActiveStatus =  function(account_id,return_result){

		try
		{
			var query ="  SELECT meta_value FROM jaze_user_jazecome_status WHERE meta_key in ('jaze_feed') and  group_id  in ( SELECT group_id FROM jaze_user_jazecome_status WHERE meta_key = 'account_id' AND meta_value ='"+account_id+"'  ) ";

			DB.query(query,null, function(data, error){
									
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].meta_value);
				}
				else
				{ return_result("1"); }
			});
		}
		catch(err)
		{
			console.log(err);
			return_result("0");
		}
	}

	methods.__checkJazeFeedBlockStatus =  function(account_id,request_account_id,return_result){

		try
		{
			var query =" SELECT meta_value FROM jaze_jazecom_contact_list WHERE meta_key in ('feed_blocked_account_id') and  group_id  in ( SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value ='"+account_id+"'  ) ";

			DB.query(query,null, function(data, error){
									
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var list = convert_array(data[0].meta_value);

					if(list.indexOf(request_account_id) !== -1)
					{ 
						return_result("0");
					}
					else
					{ return_result("1"); }
					
				}
				else
				{ return_result("1"); }
			});
		}
		catch(err)
		{
			console.log(err);
			return_result("0");
		}
	}
//---------------------------------------------------------------------------  Check Feed Active  Status Module End ------------------------------------------------------------------------------//
//---------------------------------------------------------------------------   Feed Module Start ------------------------------------------------------------------------------//
	/*
	*  Create New Feed Details
	*/
	methods.createFeedDetails = function(arrParams,return_result){

		try
		{
			//create jazecom contact
			methods.__saveReomveJazecomContactList(1,arrParams.account_id,arrParams.company_account_id, function(returnJazecom){
				
				methods.__saveReomveJazecomContactList(1,arrParams.company_account_id,arrParams.account_id, function(returnJazecom){
					
					//create Feed Group tb
					HelperRestriction.getNewGroupId('jaze_jazefeed_groups', function (group_id) {

						if(parseInt(group_id) !== 0){
							
							var	arrInsert = {
								account_id: arrParams.account_id,
								company_account_id: arrParams.company_account_id,
								subject : arrParams.subject,
								feed_status : '0',
								delete_account : '',
								create_date : moment().utc().format('YYYY-MM-DD HH:mm:ss'),
								update_date : moment().utc().format('YYYY-MM-DD HH:mm:ss'),
								status : '1'
							};

							HelperRestriction.insertTBQuery('jaze_jazefeed_groups',group_id,arrInsert, function (queGrpId) {

								if(parseInt(queGrpId) !== 0)
								{ 
									HelperRestriction.getNewGroupId('jaze_jazefeed_groups_meta', function (meta_group_id) {

										if(parseInt(meta_group_id) !== 0){
											
											var	metaInsert = {
												owner_account_id : arrParams.account_id,
												master_id 		 : group_id,
												message			 : arrParams.message,
												create_date		 : moment().utc().format('YYYY-MM-DD HH:mm:ss'),
												read_status		 : '0',
												status			 : '1',
											};

											//attachment 
											if(arrParams.file_name != '')
											{
												var arrNewPath = [];

												var arrFiles = arrParams.file_name.split(",");

												if(arrFiles.length > 1){

													arrFiles.forEach(function(row) {
														arrNewPath.push(arrParams.account_id+'/'+row);
													});

												}else{
													arrNewPath.push(arrParams.account_id+'/'+arrParams.file_name);
												}

												Object.assign(metaInsert, {attachment_url: arrNewPath.toString()});
											}
	

											HelperRestriction.insertTBQuery('jaze_jazefeed_groups_meta',meta_group_id,metaInsert, function (quemGrpId) {

												if(parseInt(quemGrpId) !== 0)
												{ 
													//send push notification

													//get sender details
													HelperGeneral.getAccountTypeDetails(arrParams.account_id, function(account_type){	

														if (parseInt(account_type) !== 0)
														{ 
															//get reciver details
															HelperGeneral.getAccountTypeDetails(arrParams.company_account_id, function(request_account_type){	

																if (parseInt(request_account_type) !== 0)
																{ 	
																	var notiParams = {	
																		group_id			 : meta_group_id.toString(),
																		account_id			 : arrParams.account_id,
																		account_type	     : account_type,
																		request_account_id   : arrParams.company_account_id,
																		request_account_type : request_account_type,
																		message				 : arrParams.subject,
																		flag				 : '5'
																	}; 

																	HelperNotification.sendPushNotification(notiParams, function(retNoti){	
																						
																		return_result(notiParams);
																	});
																}
																else
																{return_result(null);}
															});
														}
														else
														{return_result(null);}
													});
												}
												else
												{return_result(null);}
											});
										}
										else
										{return_result(null);}								
									});
								}
								else
								{return_result(null);}
							});
						}
						else
						{return_result(null);}
					});
				});
			});
		}
		catch(err)
		{
			console.log(err);
			return_result(null);
		}
	};

	/*
	*  Replay Feed
	*/
	methods.saveReplayFeedDetails = function(arrParams,return_result){

		try
		{
			//create Feed Group tb
			HelperRestriction.getNewGroupId('jaze_jazefeed_groups_meta', function (group_id) {

				if(parseInt(group_id) !== 0){

					var	metaInsert = {
							owner_account_id : arrParams.account_id,
							master_id : arrParams.master_id,
							message: arrParams.message,
							create_date:moment().utc().format('YYYY-MM-DD HH:mm:ss'),
							read_status: '0',
							status: '1',
						};

					if(arrParams.file_name != '')
					{
						var arrNewPath = [];

						var arrFiles = arrParams.file_name.split(",");
												
						if(arrFiles.length > 1){

							arrFiles.forEach(function(row) {
								arrNewPath.push(arrParams.account_id+'/'+row);
							});

						}else{
								arrNewPath.push(arrParams.account_id+'/'+arrParams.file_name);
						}

						Object.assign(metaInsert, {attachment_url: arrNewPath.toString()});
					}

					HelperRestriction.insertTBQuery('jaze_jazefeed_groups_meta',group_id,metaInsert, function (queGrpId) {
						
						if(parseInt(queGrpId) !== 0)
						{
							//update 
							var groupInsert = {
								update_date	:	metaInsert.create_date
							};

							HelperRestriction.updateQuery('jaze_jazefeed_groups',arrParams.master_id,groupInsert, function (queGrpId) {

								if(parseInt(queGrpId) !== 0)
								{
									var senParams = {account_id:arrParams.account_id};

									//get sender details
									HelperGeneral.getAccountDetails(senParams, function(sender_details){	

										if (sender_details !== null)
										{
											//get reciver details
											HelperGeneral.getAccountTypeDetails(arrParams.company_id, function(request_account_type){	

												if(parseInt(request_account_type) !== 0)
												{
													var notiParams = {	
															group_id			 : arrParams.master_id.toString(),
															account_id			 : sender_details.account_id,
															account_type		 : sender_details.account_type,
															request_account_id	 : arrParams.company_id,
															request_account_type : request_account_type,
															message				 : arrParams.message,
															flag				 : '6'
													}; 
													
													HelperNotification.sendPushNotification(notiParams, function(retNoti){	
													
															var sendbackObj = { 
																feed_id:arrParams.master_id.toString(),
																account_id: sender_details.account_id,
																account_type : sender_details.account_type,
																name: sender_details.name,
																logo: sender_details.logo,
																date: metaInsert.create_date,
																description: metaInsert.message
															};

															return_result(sendbackObj);
													});
												}
												else
												{return_result(null);}
											});
										}
										else
										{return_result(null);}
									});
								}
								else
								{return_result(null);}
							});
						}
						else
						{return_result(null);}
					});
				}
				else
				{return_result(null);}
	    	});
		}
		catch(err)
		{
			console.log(err);
			return_result(null);
		}
	};

	/*
	 * Delete Feed
	 *
	 */
	methods.deleteFeed = function(arrParams,return_result){

		try{

			if(arrParams !== null)
			{
				var list =[];

				if(arrParams.feed_id.toString().indexOf(',') != -1 ){
					list = arrParams.feed_id.split(',');
				}
				else {
					list.push(arrParams.feed_id);
				}

				var counter = list.length;

				if(parseInt(counter) !== 0)
				{
						promiseForeach.each(list,[function (lists) {
 
							return new Promise(function (resolve, reject) {

								var query = " SELECT meta_value FROM `jaze_jazefeed_groups`  WHERE `meta_key` = 'delete_account' ";
									query += " and  group_id IN (SELECT group_id FROM jaze_jazefeed_groups WHERE meta_key = 'status' AND meta_value = 1 and group_id = '"+lists+"')";
												
								DB.query(query, null, function(data, error){

									if(data[0].meta_value !== "" && parseInt(data[0].meta_value) !== parseInt(arrParams.account_id)){

										//update account id
										var query = " UPDATE `jaze_jazefeed_groups` SET `meta_value` = '"+data[0].meta_value+","+arrParams.account_id+"' WHERE  `meta_key` = 'delete_account' and group_id = '"+lists+"' ";

										DB.query(query, null, function(data, error){

											var query = " UPDATE `jaze_jazefeed_groups` SET `meta_value` = '0' WHERE  `meta_key` = 'status' and group_id = '"+lists+"' ";

											DB.query(query, null, function(data, error){

												counter -= 1; resolve(counter);

											});
										});
									}
									else
									{
										//update account id
										var query = " UPDATE `jaze_jazefeed_groups` SET `meta_value` = '"+arrParams.account_id+"' WHERE  `meta_key` = 'delete_account' and group_id = '"+lists+"' ";

										DB.query(query, null, function(data, error){

											counter -= 1; resolve(counter);

										});
									}
								});
							});
						}],
						function (result, current) {
							if (counter === 0)
							{ return_result(arrParams); }
						});	
				}
				else
				{return_result(null)}
			}
			else
			{return_result(null)}
		}
		catch(err)
		{
			return_result(null)
		}
	};


	/*
	 *  Get Jaze Feed Group List
	*/
	methods.getJazeFeedGroupList = function(arrParams,return_result){

		try{
			 let query  = " SELECT group_id FROM `jaze_jazefeed_groups` WHERE meta_key = 'update_date'  ";
				 query += " and `group_id` IN (SELECT `group_id` FROM jaze_jazefeed_groups  WHERE (`meta_key` = 'account_id' OR `meta_key` = 'company_account_id') AND `meta_value` = '"+arrParams.account_id+ "'  ";
				 query += " AND `group_id` NOT IN (SELECT `group_id` FROM jaze_jazefeed_groups WHERE `meta_key` in ('delete_account') AND FIND_IN_SET('" + arrParams.account_id + "', meta_value) ";
				 query += " AND `group_id` IN (SELECT `group_id` FROM jaze_jazefeed_groups WHERE `meta_key` = 'status' AND meta_value = '1'))) ";
				 query += " group by group_id ORDER BY  CASE WHEN meta_key = 'update_date' THEN meta_value END DESC ";

					if(parseInt(arrParams.page_no) !== 1)
					{            
						var page_no = parseInt(arrParams.page_no) - 1;

						query +=" LIMIT "+(page_no * 50)+" , 50 ";                
					}
					else
					{
						query +=" LIMIT 0 , 50 ";
					}
				
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var group_id_list = "";

						data.forEach(function(row){
							group_id_list += (group_id_list  == "") ? row.group_id: ','+row.group_id;
							
						});

						query  = " SELECT * FROM `jaze_jazefeed_groups` WHERE `group_id` IN ("+group_id_list+")";

						DB.query(query,null, function(data, error){

							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
				
								HelperRestriction.convertMultiMetaArray(data, function(retrnResult){
				
									if(retrnResult !== null)
									{
										var array_rows = [];

										var counter = retrnResult.length;

										if(parseInt(counter) !== 0)
										{
											promiseForeach.each(retrnResult,[function (jsonItems) {

												return new Promise(function (resolve, reject) {

													let arrAccountParams = {};

													if(jsonItems.account_id === arrParams.account_id)
													{ arrAccountParams = { account_id:jsonItems.company_account_id}; }
													else
													{ arrAccountParams = { account_id:jsonItems.account_id}; }


													HelperGeneral.getAccountDetails(arrAccountParams, function(user_details){	

														if (user_details !== null)
														{								
															// get last conversation details
															methods.__getFeedLastMessageResult(arrParams.account_id,arrAccountParams.account_id,jsonItems.group_id, function (message) {

																var read_status = "1";

																if(parseInt(message.account_id) !== parseInt(arrParams.account_id))
																{ read_status = message.read_status; }


																var terminate_status = "0";

																if(jsonItems.delete_account !== "")
																{ terminate_status = "1"; }

																	var result_obj  = { 
																		feed_id: jsonItems.group_id.toString(),
																		account_id: user_details.account_id,
																		account_type: user_details.account_type,
																		name: user_details.name,
																		logo: user_details.logo,
																		subject: jsonItems.subject,
																		status: jsonItems.status,
																		date: jsonItems.update_date,
																		read_status : read_status,
																		terminate_status:terminate_status,
																		last_conversation: message
																		
																};
							
																	array_rows.push(result_obj);
																	counter -= 1;
																	resolve(array_rows);
															});
														}
														else
														{  
															counter -= 1;
															resolve(array_rows);
														}
													});
												})
											}],
											function (result, current) {
												//console.log(counter);
												if (counter === 0)
												{return_result(result[0]); }
											});
										}
										else
										{return_result(null)}
									}
									else
									{return_result(null)}
								});
							}
							else
							{return_result(null)}
						});
					}
					else
					{return_result(null)}
				});
		}
		catch(err)
		{
			return_result(null)
		}
	};

	/*
	*  Get Jaze Feed Detail Last Result
	*/
	methods.__getFeedLastMessageResult = function(account_id,receiver_account_id,master_id,return_result){

		try{
			let query = " SELECT * FROM jaze_jazefeed_groups_meta WHERE   ";
				query += "group_id IN (SELECT max(group_id) FROM jaze_jazefeed_groups_meta WHERE meta_key = 'master_id'  AND meta_value ='"+master_id+"') ";

				  DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMetaArray(data, function(retrnResult){

							if(retrnResult !== null)
							{
								//check fedd avtive and block status
								var status_arrParams = { account_id : account_id, company_account_id: receiver_account_id};

								methods.checkAccountJazecomFeedStatus(status_arrParams, function(feedStatus){

									methods.__getFeedMessageObjectResult(retrnResult, function(feed_details){

										Object.assign(feed_details, {replay_status: feedStatus.toString()});

										return_result(feed_details);
									});
								});
							}
							else
							{return_result({});}
						});
					}
					else
					{return_result({});}
				});
		}
		catch(error)
		{
			console.log(error);
			return_result(null);
		}
	};

	/*
	*  Get Jaze Feed List Result Object
	*/
	methods.__getFeedMessageObjectResult = function(result_array,return_result){

		try{
			if(result_array !== null ){

				methods.__getFeedMessageRowCount(result_array.master_id,result_array.group_id, function(rowCount){	

					let arrAccountParams = { account_id:result_array.owner_account_id};

						HelperGeneral.getAccountDetails(arrAccountParams, function(user_details){	

						if (user_details !== null)
						{								
							var attachment_list = [];

								if(typeof result_array.attachment_url !== 'undefined' && result_array.attachment_url != "" && result_array.attachment_url != null)
								{ 	
									var attachArr = result_array.attachment_url.split(",");
																	
										if(attachArr.length > 1){
											attachArr.forEach(function(row) {

												var file_name = row.split(result_array.owner_account_id+"/")[1];

												var attachment_type =  getFileTypes('.'+file_name.substring(file_name.indexOf(".") + 1));

												var path = imagePath+row;

												var f_size = "0";

												if (fs.existsSync(path)) {
													f_size = fs.statSync(path);
													f_size = formatSizeUnits(f_size.size).toString();
												  } 

												var objFiles = {
													attachment_name: file_name,
													attachment_url: urlJazeFeed+row,
													attachment_size: f_size,
													attachment_type: attachment_type.toString()
												}

												attachment_list.push(objFiles);
											});
																		
										}else{

												var file_name = result_array.attachment_url.split(result_array.owner_account_id+"/")[1];

												var attachment_type =  getFileTypes('.'+file_name.substring(file_name.indexOf(".") + 1));

												var path = imagePath+result_array.attachment_url;

												var f_size = "0";

												if (fs.existsSync(path)) {
													f_size = fs.statSync(path);
													f_size = formatSizeUnits(f_size.size).toString();
												  } 

												var objFiles = {
													attachment_name: file_name,
													attachment_url: urlJazeFeed+result_array.attachment_url,
													attachment_size: f_size,
													attachment_type: attachment_type.toString()
												}

											attachment_list.push(objFiles);
										}
								}


											var read_status = "1";

											if(typeof result_array.read_status !== 'undefined' && result_array.read_status != "" && result_array.read_status != null)
											{ read_status = result_array.read_status.toString(); } 


											var result_obj  = { 
												account_id	: user_details.account_id,
												account_type: user_details.account_type,
												name		: user_details.name,
												logo		: user_details.logo,
												description : result_array.message,
												attachment_list : attachment_list,
												date		: result_array.create_date,
												read_status  : read_status,
												row_count  : rowCount.toString()
											};

										return_result(result_obj);
						}
						else
						{return_result("")}
					});
				});
			}
			else
			{return_result(null);}
		}
		catch(error)
		{
			console.log(error);
			return_result(null);
		}
	};

	/*
	*  Get Jaze Feed Message Count
	*/
	methods.__getFeedMessageRowCount = function(master_id,row_id,return_result){

		try{
			let query = " SELECT group_id FROM jaze_jazefeed_groups_meta WHERE meta_key = 'master_id'  AND meta_value ='"+master_id+"' order by id asc ";

				  DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var list = [];

						data.forEach(function(row) {
							list.push(row.group_id);
						});

						return_result(parseInt(list.indexOf(row_id)) + 1);
					}
					else
					{return_result("");}
				});
		}
		catch(error)
		{
			console.log(error);
			return_result(null);
		}

	};

	/*
	* add or Reomve Jazecom Contact List
	*/
	methods.__saveReomveJazecomContactList =  function(flag,account_id,request_account_id,return_result){

		try
		{
			if(parseInt(flag) === 1)
			{
				//save feed_account_id
				//first check user have details
				var query = " SELECT group_id,meta_value FROM jaze_jazecom_contact_list WHERE meta_key in ('feed_account_id') and  group_id  in ( SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value ='"+account_id+"'  ) ";
					
				DB.query(query,null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMetaArray(data, function(retrnResult){

							if(retrnResult !== null)
							{
								var list = convert_array(retrnResult.meta_value);

								if(typeof list !== 'undefined' && list !== null && parseInt(list.length) > 0){

									if(list.indexOf(request_account_id) === -1)
									{
										list.push(request_account_id);

										var query = "  UPDATE jaze_jazecom_contact_list SET meta_value = '"+list.join()+"' where meta_key = 'feed_account_id' and group_id = '"+retrnResult.group_id+"'; ";

										DB.query(query,null, function(data_check, error){

											return_result("1");

										});
									}
									else
									{
										return_result("1");
									}
								}
								else
								{
									var query = "  UPDATE jaze_jazecom_contact_list SET meta_value = '"+request_account_id+"' where meta_key = 'feed_account_id' and group_id = '"+retrnResult.group_id+"'; ";

										DB.query(query,null, function(data_check, error){

											return_result("1");

										});
								}
							}
						});
					}
					else
					{
						//insert new 
						HelperRestriction.getNewGroupId('jaze_jazecom_contact_list', function (group_id) {

							if(parseInt(group_id) !== 0){
							
								var arrConvInsert = {
									account_id: account_id,
									mail_account_id: "",
									mail_blocked_account_id: "",
									chat_account_id: "",
									chat_blocked_account_id: "",
									call_account_id: "",
									call_blocked_account_id: "",
									feed_account_id: request_account_id,
									feed_blocked_account_id: "",
									diary_account_id: "",
									diary_blocked_account_id: "",
									create_date: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
									status: '1'
								};

								HelperRestriction.insertTBQuery('jaze_jazecom_contact_list',group_id, arrConvInsert, function (queResGrpId) {

									return_result(queResGrpId);
								});
							}
							else
							{ return_result(null); }
						});
					}
				});
			}
			else if(parseInt(flag) === 2)
			{
					//save feed_blocked_account_id
					var query = " SELECT * FROM jaze_jazecom_contact_list WHERE meta_key in ('feed_account_id','feed_blocked_account_id') and  group_id  in ( SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value ='"+account_id+"'  ) ";
					
					DB.query(query,null, function(data, error){
						
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
							HelperRestriction.convertMetaArray(data, function(retrnResult){
	
								if(retrnResult !== null)
								{
									var query_update = [];

									//check is in feed_account_id row
									var feed_account_id_list = convert_array(retrnResult.feed_account_id);

									if(typeof feed_account_id_list !== 'undefined' && feed_account_id_list !== "" && feed_account_id_list !== null && parseInt(feed_account_id_list.length) > 0){
										// 
										if(feed_account_id_list.indexOf(request_account_id)!= -1)
										{
											feed_account_id_list.splice(feed_account_id_list.indexOf(request_account_id), 1);

											query_update.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+feed_account_id_list.join()+"' where meta_key = 'feed_account_id' and group_id = '"+retrnResult.group_id+"'; ");
										}
									}


									//check is in feed_blocked_account_id row
									var feed_blocked_account_list = convert_array(retrnResult.feed_blocked_account_id);

									if(typeof feed_blocked_account_list !== 'undefined' && feed_blocked_account_list !== "" && feed_blocked_account_list !== null && parseInt(feed_blocked_account_list.length) > 0){

										if(feed_blocked_account_list.indexOf(request_account_id) === -1)
										{
											feed_blocked_account_list.push(request_account_id);

											query_update.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+feed_blocked_account_list.join()+"' where meta_key = 'feed_blocked_account_id' and group_id = '"+retrnResult.group_id+"'; ");
										}
									}
									else
									{
										query_update.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+request_account_id+"' where meta_key = 'feed_blocked_account_id' and group_id = '"+retrnResult.group_id+"'; ");
									}


									if(parseInt(query_update.length) !== 0)
									{
										for( var i in query_update)
										{
											DB.query(query_update[i],null, function(data_check, error){

											});
										}

										return_result(retrnResult.group_id);
									}
									else
									{
										return_result(retrnResult.group_id);
									}
								}
							});
						}
						else
						{
							//insert new 
							HelperRestriction.getNewGroupId('jaze_jazecom_contact_list', function (group_id) {
	
								if(parseInt(group_id) !== 0){
								
									var arrConvInsert = {
										account_id: account_id,
										mail_account_id: "",
										mail_blocked_account_id: "",
										chat_account_id: "",
										chat_blocked_account_id: "",
										call_account_id: "",
										call_blocked_account_id: "",
										feed_account_id: "",
										feed_blocked_account_id: request_account_id,
										diary_account_id: "",
										diary_blocked_account_id: "",
										create_date: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
										status: '1'
									};
	
									HelperRestriction.insertTBQuery('jaze_jazecom_contact_list',group_id, arrConvInsert, function (queResGrpId) {
	
										return_result(queResGrpId);
									});
								}
								else
								{ return_result(0); }
							});
						}
					});	
			}
			else if(parseInt(flag) === 3)
			{
				//unblock  feed account id

				var query = " SELECT * FROM jaze_jazecom_contact_list WHERE meta_key in ('feed_account_id','feed_blocked_account_id') and  group_id  in ( SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value ='"+account_id+"'  ) ";
					
					DB.query(query,null, function(data, error){
						
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
							HelperRestriction.convertMetaArray(data, function(retrnResult){
	
								if(retrnResult !== null)
								{
									var query_update = [];

									//check is in feed_blocked_account_id row
									var feed_blocked_account_list = convert_array(retrnResult.feed_blocked_account_id);

									if(typeof feed_blocked_account_list !== 'undefined' && feed_blocked_account_list !== "" && feed_blocked_account_list !== null && parseInt(feed_blocked_account_list.length) > 0){

										if(feed_blocked_account_list.indexOf(request_account_id)!= -1)
										{
											feed_blocked_account_list.splice(feed_blocked_account_list.indexOf(request_account_id), 1);

											query_update.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+feed_blocked_account_list.join()+"' where meta_key = 'feed_blocked_account_id' and group_id = '"+retrnResult.group_id+"'; ");
										}
									}
									else
									{
										query_update.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+request_account_id+"' where meta_key = 'feed_blocked_account_id' and group_id = '"+retrnResult.group_id+"'; ");
									}



									//check is in feed_account_id row
									var feed_account_id = convert_array(retrnResult.feed_account_id);

									if(typeof feed_account_id !== 'undefined' && feed_account_id !== "" && feed_account_id !== null && parseInt(feed_account_id.length) > 0){

										if(feed_account_id.indexOf(request_account_id) === -1)
										{
											feed_account_id.push(request_account_id);

											query_update.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+feed_account_id.join()+"' where meta_key = 'feed_account_id' and group_id = '"+retrnResult.group_id+"'; ");
										}
									}
									else
									{
										query_update.push("  UPDATE jaze_jazecom_contact_list SET meta_value = '"+request_account_id+"' where meta_key = 'feed_account_id' and group_id = '"+retrnResult.group_id+"'; ");
									}
									
									if(parseInt(query_update.length) !== 0)
									{
										for( var i in query_update)
										{
											DB.query(query_update[i],null, function(data_check, error){

											});
										}

										return_result(retrnResult.group_id);
									}
									else
									{
										return_result(retrnResult.group_id);
									}
								}
							});
						}
						else
						{ return_result(null); }
					});	
			}
		}
		catch(err)
		{
			console.log(err);
			return_result(null);
		}
	}

	/*
	* Feed List
	*/
	methods.getCountJazeFeedGroupList = function(arrParams,return_result){

		try{
			if(parseInt(arrParams.total_count) === 0)
			{
				let query  = " SELECT group_id FROM jaze_jazefeed_groups  WHERE (`meta_key` = 'account_id' OR `meta_key` = 'company_account_id') AND `meta_value` = '"+arrParams.account_id+ "'  group by group_id ";

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(Math.ceil(parseInt(data.length) / 50));
					}
					else
					{return_result("0")}
				});
			}
			else
			{
				return_result(arrParams.total_count)
			}
		}
		catch(err)
		{
			return_result(null)
		}
	};

	/*
	*  Get Jaze Feed Group Details
	*/
	methods.getJazeFeedGroupDetails = function(arrParams,return_result){

		try{
			if(arrParams !== null)
			{
				let query  = " SELECT * ";
					query += " FROM jaze_jazefeed_groups WHERE  ";
					query += " group_id IN (SELECT group_id FROM jaze_jazefeed_groups WHERE meta_key = 'status' AND meta_value ='1'  AND group_id = '"+arrParams.master_id+"') ";

		
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMetaArray(data, function(retrnResult){

							if(retrnResult !== null)
							{
								var arrAccountParams = {};

								if(parseInt(retrnResult.account_id) === parseInt(arrParams.account_id))
								{ arrAccountParams = { account_id:retrnResult.company_account_id}; }
								else
								{ arrAccountParams = { account_id: retrnResult.account_id}; }

								HelperGeneral.getAccountDetails(arrAccountParams, function(user_details){	

									if(user_details !== null)
									{
										//check fedd avtive and block status
										var status_arrParams = { account_id : arrParams.account_id, company_account_id: arrAccountParams.account_id};

										methods.checkAccountJazecomFeedStatus(status_arrParams, function(feedStatus){

											var terminate_status = "0";

											if(retrnResult.delete_account !== "")
											{ terminate_status = "1"; }

											var head_details = {
												receiver_account_id : user_details.account_id.toString(),
												receiver_type		: user_details.account_type,
												receiver_name		: user_details.name,
												receiver_logo		: user_details.logo,
												feed_id: retrnResult.group_id.toString(),
												subject: retrnResult.subject,
												replay_status : feedStatus.toString(),
												terminate_status:terminate_status
											};

											return_result(head_details);
										});
									}
									else
									{return_result(null);}
								});
							}
							else
							{return_result(null);}
						});
					}
					else
					{return_result(null);}
				});
			}
			else
			{return_result(null);}	
		}
		catch(err)
		{
			return_result(null)
		}
	};

	/*
	*  Get Jaze Feed Detail List
	*/
	methods.GetFeedList = function(arrParams,return_result){

		try{

				 let query = " SELECT group_id FROM jaze_jazefeed_groups_meta WHERE  ";
				 query += "group_id IN (SELECT group_id FROM jaze_jazefeed_groups_meta WHERE meta_key = 'master_id'  AND meta_value ='"+arrParams.master_id+"' AND  ";
				 query += "group_id IN (SELECT group_id FROM jaze_jazefeed_groups_meta WHERE meta_key = 'status' AND meta_value ='1' )) ";
				 query += " group by group_id ORDER BY  group_id DESC ";

				 if(parseInt(arrParams.page_no) !== 1)
				 {            
					 query +=" LIMIT "+((parseInt(arrParams.page_no) - 1) * 10)+" , 10 ";                
				 }
				 else if(parseInt(arrParams.page_no) === 1)
				 {
					 query +=" LIMIT 0 , 10 ";
				 }

				 DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var group_id_list = "";

						data.forEach(function(row){
							group_id_list += (group_id_list  == "") ? row.group_id: ','+row.group_id;
							
						});

						query  = " SELECT * FROM `jaze_jazefeed_groups_meta` WHERE `group_id` IN ("+group_id_list+")";

						DB.query(query,null, function(data, error){

							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

								HelperRestriction.convertMultiMetaArray(data, function(retrnResult){

									if(retrnResult !== null)
									{
										var array_rows = [];

										var counter = retrnResult.length;						

										if(parseInt(counter) !== 0)
										{
											promiseForeach.each(retrnResult,[function (jsonItems) {

												return new Promise(function (resolve, reject) {

													methods.__getFeedMessageObjectResult(jsonItems, function(feed_details){

														array_rows.push(feed_details);
							
														counter -= 1;

														resolve(array_rows);

													});
												})
											}],
											function (result, current) {
												if (counter === 0)
												{return_result(result[0]); }
											});
										}
										else
										{return_result(null)}
									}
									else
									{return_result(null)}
								});
							}
							else
							{return_result(null)}
						});
					}
					else
					{return_result(null)}
				});
		}
		catch(error)
		{
			console.log(error);
			return_result(null);
		}

	};

	/*
	*  Get Count Jaze Feed Detail List
	*/
	methods.getCountJazeFeedList = function(master_id,return_result){

		try{

			if(master_id !== 0)
			{
				let query = " SELECT group_id FROM jaze_jazefeed_groups_meta WHERE  ";
				query += "group_id IN (SELECT group_id FROM jaze_jazefeed_groups_meta WHERE meta_key = 'master_id'  AND meta_value ='"+master_id+"' AND  ";
				query += "group_id IN (SELECT group_id FROM jaze_jazefeed_groups_meta WHERE meta_key = 'status' AND meta_value ='1' )) ";
				query += " group by group_id ";

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(Math.ceil(parseInt(data.length) / 10));
					}
					else
					{return_result("0")}
				});
			}
			else
			{
				return_result("0")
			}
		}
		catch(err)
		{
			return_result(null)
		}
	};

	/*
	 * Check attachment url 
	 */
	methods.GetFeedAttachmentList = function(master_id,return_result){

		try{
			let query = " SELECT group_concat(meta_value) as meta_value FROM jaze_jazefeed_groups_meta WHERE meta_key in ('attachment_url')  and ";
				query += "group_id IN (SELECT group_id FROM jaze_jazefeed_groups_meta WHERE meta_key = 'master_id'  AND meta_value ='"+master_id+"' AND  ";
				query += "group_id IN (SELECT group_id FROM jaze_jazefeed_groups_meta WHERE meta_key = 'status' AND meta_value ='1' )) and  meta_value != '' ";
			
				  DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var list = convert_array(data[0].meta_value);

						if(typeof list !== 'undefined' && list !== null && list !== "" && parseInt(list.length) > 0){

							var attachment_list = [];

							list.forEach(function(row) {

								var file_name = row.split("/")[1];

								var attachment_type =  getFileTypes('.'+file_name.substring(file_name.indexOf(".") + 1));

								var path = imagePath+row;

								var f_size = "0";

								if (fs.existsSync(path)) {
									f_size = fs.statSync(path);
									f_size = formatSizeUnits(f_size.size).toString();
								} 

								var objFiles = {
									attachment_name: file_name,
									attachment_url: urlJazeFeed+row,
									attachment_size: f_size,
									attachment_type: attachment_type.toString()
								}

								attachment_list.push(objFiles);
							});


							return_result(attachment_list);	
						}
						else
						{return_result(null);}	
					}
					else
					{return_result(null);}
				});
		}
		catch(error)
		{
			console.log(error);
			return_result(null);
		}

	};

	/*
	 * set Feed Respond Status
	 */
	methods.setFeedRespondStatus = function(arrParams,return_result){

		try{
			var query  = " SELECT meta_value  FROM jaze_jazefeed_groups WHERE meta_key ='feed_status' and meta_value ='0' and  group_id IN ";
				query += " (SELECT `group_id` FROM jaze_jazefeed_groups  WHERE  `meta_key` = 'company_account_id' AND `meta_value` = '"+arrParams.account_id+"'  ";
				query +=" AND `group_id` IN (SELECT `group_id` FROM jaze_jazefeed_groups WHERE `meta_key` = 'status' AND meta_value = '1' AND group_id = '"+arrParams.master_id+"' )) ";

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						query = " UPDATE jaze_jazefeed_groups SET meta_value = '1' WHERE group_id = '"+arrParams.master_id+"' AND meta_key ='feed_status'; ";

						DB.query(query,null, function(data, error){

							if(data !== null)
							{
								return_result(1);
							}
							else
							{ return_result(null); }
						});
					}
					else
					{ return_result(null); }
				});
		}
		catch(error)
		{
			console.log(error);
			return_result(null);
		}		
	};

	/*
	 * set Feed Respond Status
	 */
	methods.setFeedReadStatus = function(arrParams,return_result){

		try{

			var query  = " SELECT group_id  FROM jaze_jazefeed_groups_meta WHERE meta_key ='read_status' and meta_value ='0' and  group_id IN ";
				query += " (SELECT `group_id` FROM jaze_jazefeed_groups_meta  WHERE  `meta_key` = 'owner_account_id' AND `meta_value` != '"+arrParams.account_id+"'  ";
				query += " AND `group_id` IN (SELECT `group_id` FROM jaze_jazefeed_groups_meta  WHERE  `meta_key` = 'master_id' AND `meta_value` = '"+arrParams.master_id+"'  ";
				query += " AND `group_id` IN (SELECT `group_id` FROM jaze_jazefeed_groups_meta WHERE `meta_key` = 'status' AND meta_value = '1' ))) ";
				query += " order by group_id desc limit 1 ";

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						query = " UPDATE jaze_jazefeed_groups_meta SET meta_value = '1' WHERE group_id = '"+data[0].group_id+"' AND meta_key ='read_status'; ";

						DB.query(query,null, function(data, error){

							if(data !== null)
							{
								return_result(1);
							}
							else
							{ return_result(null); }
						});
					}
					else
					{ return_result(null); }
				});
		}
		catch(error)
		{
			console.log(error);
			return_result(null);
		}		
	};

	/*
	 * Feed Notification Read Status
	 */
	methods.setFeedNotificationReadStatus = function(arrParams,return_result){

		try{
			var query = " SELECT GROUP_CONCAT(group_id) as group_id  FROM jaze_notification_list WHERE ";
				query +=  " group_id IN ( SELECT group_id FROM jaze_notification_list WHERE meta_key = 'notify_category' AND meta_value = 'jazefeed' ";
				query +=  " AND group_id IN ( SELECT group_id FROM jaze_notification_list WHERE meta_key = 'read_status' AND meta_value = '0'  ";
				query +=  " AND group_id IN ( SELECT group_id FROM jaze_notification_list WHERE meta_key = 'receiver_account_id' AND meta_value = '"+arrParams.account_id+"'  ";
				query +=  " AND group_id IN ( SELECT group_id FROM jaze_notification_list WHERE meta_key = 'notify_id' AND meta_value = '"+arrParams.master_id+"' )))) group by group_id ";   
				
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						query = " UPDATE jaze_notification_list SET meta_value = '1' WHERE group_id IN ("+data[0].group_id+") AND meta_key ='read_status'; ";

						DB.query(query,null, function(data, error){

						});
					}

					return_result(1);

				});
		}
		catch(error)
		{
			console.log(error);
			return_result(null);
		}	
	};


	methods.getMultiple = function(arrFiles,account_id, return_result){

		var	file_type;
		var	extention;

		var newObj = {};
		var arrRows = [];



		if(typeof arrFiles !== 'undefined' && arrFiles !== null && parseInt(arrFiles.length) > 0){

			var array_rows = [];

			arrFiles.forEach(function (arrayItem) {

				file_type = arrayItem.mimetype;
				extention = arrayItem.filename;

				var trimed = extention.substring(extention.indexOf(".") + 1);

				var retYpe =  getFileTypes('.'+trimed);

					newObj = {
						file_name:extention,
						file_format:file_type,
						file_type:retYpe,
						extention:trimed,
						file_path:urlJazeFeed+account_id+'/'+arrayItem.filename
					};

					arrRows.push(newObj);

			});


			return_result(arrRows);


		}else{
			return_result(null);
		}


	};

//---------------------------------------------------------------------------   Feed Module End ------------------------------------------------------------------------------//

//------------------------------------------------------- General Functions Start -----------------------------------------------------------//

function convert_array(array_val) {

	if(typeof array_val !== 'undefined' && array_val !== null && array_val !== "" && parseInt(array_val.length) > 0){
		
		var list = [];

		if(array_val.toString().indexOf(',') != -1 ){
			list = array_val.split(',');
		}
		else {
			list.push(array_val);
		}

		return list;
	}
	else
	{ return ""; }
}

function getFileTypes(ext){

	var type;

	var arrImages = ['.jpeg','.jpg','.gif','.png','.tif'];
	var arrVideos = ['.mp4','.avi','.mov','.flv','.wmv'];
	var arrAudio = ['.pcm','.wav','.aiff','.mp3','.aac','.wma','.flac','.alac'];
	var arrDocs = ['.odt','.xls','.xlsx','.ods','.ppt','.pptx','.txt','.doc'];
	var arrPdf = ['.pdf'];

	if(arrImages.includes(ext.toLowerCase())){
		type = '2';
	}else if(arrAudio.includes(ext.toLowerCase())){
		type = '3';
	}else if(arrVideos.includes(ext.toLowerCase())){
		type = '4';
	}else if(arrDocs.includes(ext.toLowerCase())){
		type = '5';
	}else if(arrPdf.includes(ext.toLowerCase())){
		type = '6';
	}else{
		type = '7';
	}

	return type;
};


function formatSizeUnits(bytes){
	if      (bytes >= 1073741824) { bytes = (bytes / 1073741824).toFixed(2) + " GB"; }
	else if (bytes >= 1048576)    { bytes = (bytes / 1048576).toFixed(2) + " MB"; }
	else if (bytes >= 1024)       { bytes = Math.ceil((bytes / 1024)) + " KB"; }
	else if (bytes > 1)           { bytes = bytes + " bytes"; }
	else if (bytes == 1)          { bytes = bytes + " byte"; }
	else                          { bytes = "0 bytes"; }
	return bytes;
  }

//------------------------------------------------------- General Functions End -----------------------------------------------------------//

module.exports = methods;