const methods = {};
const DB = require('./../../helpers/db_base.js');
const HelperRestriction = require('../../helpers/HelperRestriction.js');
const HelperNotification = require('./HelperNotification.js');
const HelperGeneral = require('../../helpers/HelperGeneral.js');

const moment = require("moment");
const fast_sort = require("fast-sort");
const promiseForeach = require('promise-foreach');

var php = require('js-php-serialize');
var PHPUnserialize = require('php-unserialize');

//------------------------------------------------------------------- MAIL MODULE START -----------------------------------------------------------//

/*
* save send email
*/
methods.saveSendEmail = function(arrParams,return_result)
{
	try
	{		
		if(arrParams !== null && parseInt(arrParams.account_id) !== 0)
		{
			HelperRestriction.getNewGroupId('jaze_jazecom_jazeemail_details', function (group_id) {

				if(parseInt(group_id) !== 0){

					var flag = 1;

					//subject encrypt
					HelperRestriction.jazeEncode(arrParams.subject, flag, function(encrypt_subject){	

						//message encrypt
						HelperRestriction.jazeEncode(arrParams.message, flag, function(encrypt_msg){

							//attachment
							if(typeof arrParams.attachment !== undefined && arrParams.attachment !== null && arrParams.attachment !== '' && parseInt(arrParams.attachment.length) > 0 && arrParams.attachment.replace(/\s/g, "") !== '' ){
								
								var attachment_arr = php.serialize(arrParams.attachment);

							}else{
								var attachment_arr = '';
							}

							var arrInsert = {		
								account_id  	: arrParams.account_id,
								to_account_id  	: arrParams.to_account_id,
								cc_account_id  	: arrParams.cc_account_id,
								bcc_account_id  : arrParams.bcc_account_id,
								subject     	: encrypt_subject,
								message     	: encrypt_msg,
								attachment     	: attachment_arr,								
								create_date		: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
								status			: 1
							};
							
							HelperRestriction.insertTBQuery('jaze_jazecom_jazeemail_details',group_id,arrInsert, function (mailGrpId) {

								if(parseInt(mailGrpId) != 0){									
											
									//account ids to insert

									var account_ids = [];

									if(parseInt(arrParams.flag) == 1)
									{	
										account_ids = __getAccountidArray(account_ids, arrParams.account_id, 3);

										// to account id
										account_ids = __getAccountidArray(account_ids, arrParams.to_account_id, 1);

										// cc account id
										account_ids = __getAccountidArray(account_ids, arrParams.cc_account_id, 1);

										// bcc account id
										account_ids = __getAccountidArray(account_ids, arrParams.bcc_account_id, 1);	
									}
									else if(parseInt(arrParams.flag) == 2)
									{
										account_ids = __getAccountidArray(account_ids, arrParams.account_id, 2);
									}
									
									var counter = account_ids.length;

									var loop_no = 0;

									if(parseInt(counter) !== 0)
									{
										promiseForeach.each(account_ids,[function (accountid) {

											return new Promise(function (resolve, reject) {

												var acc_id = accountid.split('|')[0];

												var fold_id = accountid.split('|')[1];

												methods.__updateShowInList(acc_id, fold_id, arrParams.parent_id, 0, arrParams.flag, function(update_result){

													loop_no += 1;

													setTimeout(function(){
														
														var arrInsertList = {		
															account_id  	: acc_id.toString(),
															mail_group_id  	: mailGrpId.toString(),
															folder_id  		: fold_id.toString(),
															starred			: "0",
															parent_id		: arrParams.parent_id.toString(),
															status			: "1",
															show_in_list	: "1"
														};

														if(parseInt(fold_id) == 1){

															arrInsertList.read_status = "0";
														}

														HelperRestriction.getNewGroupId('jaze_jazecom_jazeemail_list', function (list_group_id) {										

															if(parseInt(list_group_id) !== 0){
					
																HelperRestriction.insertTBQuery('jaze_jazecom_jazeemail_list',list_group_id,arrInsertList, function (listGrpId) {

																	var receiver_id = acc_id;

																	if(parseInt(receiver_id) !== parseInt(arrParams.account_id) && parseInt(arrParams.flag) == 1){

																		var mail_push_notification = {
																			account_id           : arrParams.account_id.toString(),
																			request_account_id   : receiver_id.toString(),
																			group_id             : mailGrpId.toString(),
																			message              : arrParams.subject,
																			flag                 : "12"
																		};

																		HelperNotification.sendPushNotification(mail_push_notification, function(returnNoti){

																			counter -= 1;															

																			if (counter === 0){ 
																				resolve(listGrpId);
																			}
																		});																
																	}else{
																		counter -= 1;															

																		if (counter === 0){ 
																			resolve(listGrpId);
																		}
																	}																																												
																});
															}	
															else
															{ return_result(0); }											
														});	
													}, loop_no * 250);	
												});																					
											})
										}],
										function (result, current) {
											if (counter === 0){ 

												//if flag =2 then delete old email
												if(parseInt(arrParams.mail_id) != 0)
												{
													var arr = {
														account_id		:	arrParams.account_id,
														folder_id		:	"2",
														mail_group_id	:	arrParams.mail_id
													}
													
													methods.__deleteMailDetails(arr, 1, function(delete_mail){
														return_result(result[0]);
													});
												}
												else
												{
													return_result(result[0]);
												}												
											}
										});	
									}
									else
									{ return_result(0); }																
								}
								else
								{ return_result(0); }
							});
						});
					});
				}
				else
				{ return_result(0); }
			});
		}
		else
		{ return_result(0); }		
	}
	catch(err)
	{console.log("saveSendEmail",err); return_result(0)}
}

/*
* update folder list show in list
*/
methods.__updateShowInList = function(account_id, folder_id, parent_id, mail_group_id, flag, return_result)
{
	try
	{
		if((parseInt(parent_id) != 0 || parseInt(mail_group_id) != 0 ) && parseInt(flag) == 1)
		{			
			var query = "SELECT group_concat(group_id) as group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'show_in_list' ";

			query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'account_id' AND meta_value = '"+account_id+"')";

			query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'folder_id' AND meta_value = '"+folder_id+"')";	

			query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'show_in_list' AND meta_value = '1')";

			if(parseInt(parent_id) != 0)
			{
				query += " AND (group_id = '"+parent_id+"' OR group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'parent_id' AND meta_value = '"+parent_id+"'))";
			}
			
			if(parseInt(mail_group_id) != 0)
			{
				query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'mail_group_id' AND meta_value = '"+mail_group_id+"')"
			}
 
			DB.query(query,null, function(data, error)
			{		
				if (typeof data !== undefined && data !== null && parseInt(data.length) > 0 && data[0].group_id !== null && parseInt(data[0].group_id) !== 0)
				{
					var data_count = __convert_array(data[0].group_id);

					var counter = data_count.length;

					if(parseInt(counter) !== 0)
					{
						promiseForeach.each(data_count,[function (id) {

							return new Promise(function (resolve, reject) {

								//update
								var update_arr = { show_in_list : 0};

								HelperRestriction.updateQuery('jaze_jazecom_jazeemail_list',id,update_arr, function (update_id) {

									counter -= 1;															

									if (counter === 0){ 
										resolve(update_id);
									}
								});
							})
						}],
						function (result, current) {
							if (counter === 0){ 
								return_result(result[0]);
							}
						});
					}
					else
					{
						return_result(0);
					}
					
				}
				else
				{
					var query = "SELECT meta_value FROM `jaze_jazecom_jazeemail_list` WHERE group_id = '"+parent_id+"' AND meta_key = 'mail_group_id' ";
					
					DB.query(query,null, function(det, error){
				
						if (typeof det !== undefined && det !== null && parseInt(det.length) > 0)
						{
							methods.__updateShowInList(account_id, folder_id, 0, det[0]['meta_value'], flag, function(update_result){
								return_result(update_result);
							});
						}
						else
						{
							return_result(0);
						}
					});
				}
			});
		}
		else
		{
			return_result(0);
		}
	}
	catch(err)
	{console.log("__updateShowInList",err); return_result(0)}
}


/*
* get mail list
*/
methods.getMailList = function(arrParams, return_result)
{	
	var pagination_details = {
		total_page  	: "0",
		current_page    : "0",
		next_page   	: "0",
		total_result    : "0"
	};

	var email_list_arr = [];

	var mail_list_details = {
		pagination_details	:	pagination_details,
		email_list			:	email_list_arr
	}

	try
	{		
		if(arrParams !== null && parseInt(arrParams.account_id) !== 0)
		{
			//get mail group id list

			var query = " SELECT group_concat(group_id) as group_id  FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'mail_group_id' ";

			query += " AND group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'status' and  meta_value = '1' )";

			query += " AND group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'account_id' and  meta_value = '"+arrParams.account_id+"' )";

			query += " AND group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'folder_id' and  meta_value = '"+arrParams.folder+"' )";

			query += " AND group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'show_in_list' and  meta_value = '1' );";
			
			DB.query(query,null, function(data, error)
			{	
				if (typeof data !== undefined && data !== null && parseInt(data.length) > 0)
				{
					var mail_list = __convert_array(data[0].group_id);

					var total_page = Math.ceil(mail_list.length / 5);

					if(total_page > parseInt(arrParams.page)+1)
					{
						var next_page = parseInt(arrParams.page)+1;
					}
					else
					{
						var next_page = 0;
					}

					var pagination_details = {

						total_page  	: total_page.toString(),
						current_page    : arrParams.page.toString(),
						next_page   	: next_page.toString(),
						total_result    : mail_list.length.toString()
					}	
					
					mail_list.sort(function(a, b){return b-a});
														
					var group_ids = mail_list.slice( ((5 * parseInt(arrParams.page))), (5 * (parseInt(arrParams.page) + 1)) );

					if(typeof group_ids !== undefined && group_ids !== null && parseInt(group_ids.length) > 0){

						// get mail list 
						var query = " SELECT * FROM `jaze_jazecom_jazeemail_list` WHERE group_id IN ("+group_ids.join()+") ORDER BY group_id desc";

						DB.query(query,null, function(data, error){

							HelperRestriction.convertMultiMetaArray(data, function (mail_detail) {									

								if(mail_detail.length > 0)
								{
									var email_list_arr = [];

									var counter = mail_detail.length;

									if(parseInt(counter) !== 0)
									{
										promiseForeach.each(mail_detail,[function (mail) {
											
											return new Promise(function (resolve, reject) {
												
												if(parseInt(arrParams.folder) != 4 )
												{
													var mail_arr = {
														account_id 		: arrParams.account_id,
														mail_id			: mail.group_id,
														mail_group_id	: mail.mail_group_id,
														parent_id		: mail.parent_id,
														folder_id		: arrParams.folder

													}
													methods.__getLastMailByParentId(mail_arr, function(return_arr)
													{
														if (typeof return_arr !== undefined && return_arr !== null && Object.entries(return_arr).length !== 0 )
														{
															mail.mail_group_id = return_arr.mail_group_id;

															methods.__getMailDetailsArray(mail, function(mail_details)
															{
																if(mail_details !== null && mail_details !== ''){

																	email_list_arr.push(mail_details);			
																}

																counter -= 1;	
			
																if (counter === 0)
																{ resolve(email_list_arr); }																		
															});
														}
														else
														{
															methods.__getMailDetailsArray(mail, function(mail_details)
															{
																if(mail_details !== null && mail_details !== ''){

																	email_list_arr.push(mail_details);
			
																}

																counter -= 1;	
					
																if (counter === 0)
																{ resolve(email_list_arr); }
															});
														}
													});
												}
												else
												{
													methods.__getMailDetailsArray(mail, function(mail_details)
													{
														if(mail_details !== null && mail_details !== ''){

															email_list_arr.push(mail_details);
	
														}

														counter -= 1;	
			
														if (counter === 0)
														{ resolve(email_list_arr); }
													});
												}																							
											});
										}],
										function (result, current) 
										{									
											if (counter === 0)
											{ 
												var mail_list_details = {
													pagination_details	:	pagination_details,
													email_list			:	fast_sort(result[0]).desc('date')
												}
												
												return_result(mail_list_details);
											}
										});	
									}
									else
									{ return_result(mail_list_details); }
								}
								else
								{ return_result(mail_list_details); }
							});
						});
					}
					else
					{ return_result(mail_list_details); }	
				}
				else
				{ return_result(mail_list_details); }	
			});
		}
		else
		{ return_result(mail_list_details); }	
	}
	catch(err)
	{console.log("getMailList",err); return_result(mail_list_details);}
}

/*
* get mail details array
*/
methods.__getMailDetailsArray = function(arrParams, return_result)
{
	var return_obj = [];
	
	try
	{
		if (arrParams !== null && Object.entries(arrParams).length !== 0 )
		{
			var args = {
				account_id		:	arrParams.account_id,
				mail_group_id	:	arrParams.mail_group_id,
				flag			:	0,
				history			:	1
			}

			args.read_status = (parseInt(arrParams.folder_id) == 1) ? arrParams.read_status : '';
 
			//get mail details
			methods.__getMailDetailsByGroupId(args, function (mail_details) 
			{													
				//get reply count
				methods.__getMailReplyCount(arrParams.account_id, arrParams.parent_id, arrParams.mail_group_id, function(count)
				{	
					
					mail_details.reply_count = count.toString();

					if(mail_details !== null && mail_details !== '')
					{																																											
						return_result(mail_details);
					}
					else
					{ return_result(return_obj); }
				});
			});
		}
		else
		{ return_result(return_obj); }	
	}
	catch(err)
	{console.log("__getMailDetailsArray",err); return_result(return_obj);}
}

/*
* get last mail of this parent id
*/
methods.__getLastMailByParentId = function(arrParams, return_result)
{
	var return_obj = [];

	try
	{
		if(parseInt(arrParams.mail_id) != 0 )
		{
			if(parseInt(arrParams.parent_id) != 0 )
			{
				var query = "SELECT MAX(group_id) AS group_id FROM `jaze_jazecom_jazeemail_list` WHERE group_id > '"+arrParams.mail_id+"' AND meta_key = 'parent_id' AND ";

				query += " group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'parent_id' AND meta_value = '"+arrParams.parent_id+"')";

				query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'account_id' AND meta_value = '"+arrParams.account_id+"')";

				query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'show_in_list' AND meta_value = '1')";

				DB.query(query,null, function(data, error){

					if (typeof data !== undefined && data !== null && parseInt(data.length) > 0)
					{
						var query = " SELECT * FROM `jaze_jazecom_jazeemail_list` WHERE group_id = '"+data[0]['group_id']+"' ";

						DB.query(query,null, function(last_mail, error)
						{
							if (typeof last_mail !== undefined && last_mail !== null && parseInt(last_mail.length) > 0)
							{
								HelperRestriction.convertMetaArray(last_mail, function (last_mail_det) 
								{	
									return_result(last_mail_det);
								});
							}
							else
							{
								return_result(return_obj);
							}
						});
					}
					else
					{
						return_result(return_obj);
					}

				});
			}
			else
			{
				methods.__getMasterId(arrParams.account_id, arrParams.mail_group_id, function(master_id)
				{
					if(parseInt(master_id) != 0)
					{
						arrParams.parent_id = master_id;

						methods.__getLastMailByParentId(arrParams, function(return_arr)
						{
							return_result(return_arr);
						});
					}
					else
					{
						return_result(return_obj);
					}
				});
			}
		}
		else
		{
			return_result(return_obj);
		}
	}
	catch(err)
	{console.log("__getLastMailByParentId",err); return_result(return_obj);}
}

/*
* get mail details
*/
methods.__getMailDetailsByGroupId = function(arrParams, return_result)
{
	try
	{
		if(parseInt(arrParams.mail_group_id) != 0)
		{
			var query = " SELECT * FROM `jaze_jazecom_jazeemail_details` WHERE group_id = '"+arrParams.mail_group_id+"' ";

			DB.query(query,null, function(data, error){

				if (typeof data !== undefined && data !== null && parseInt(data.length) > 0){

					HelperRestriction.convertMetaArray(data, function (mail_details) {	

						//subject decrypt
						HelperRestriction.jazeEncode(mail_details.subject, 2, function(decrypt_subject){	

							//message decrypt
							HelperRestriction.jazeEncode(mail_details.message, 2, function(decrypt_msg){

								var all_account_ids = {
									sender		:	mail_details.account_id,
									receiver	:	mail_details.to_account_id,
									cc			:	mail_details.cc_account_id,
									bcc			:	mail_details.bcc_account_id									
								};

								//mail listing - flag =0																								
								if(parseInt(arrParams.flag) == 0)	
								{												
									methods.__getAccountDetailsByFolderId(arrParams.account_id, arrParams.mail_group_id, all_account_ids, function(account_details)
									{

										var attachment_status = (mail_details.attachment !== null && mail_details.attachment !== '') ? "1" : "0";

										var email_details = {
											id					: arrParams.mail_group_id.toString(),
											subject				: decrypt_subject,
											date				: mail_details.create_date.toString(),
											from_account_id		: mail_details.account_id.toString(),
											from_account_type	: account_details.account_type.toString(),
											from_name			: account_details.name,
											from_logo			: account_details.logo,  
											attachment_status	: attachment_status,
											message 			: decrypt_msg.substr(0, 20)+'...'
										}											

										if(arrParams.read_status !== ""){
											Object.assign(email_details, {read_status : arrParams.read_status});
										}
										
										return_result(email_details);
									});
								}
								else
								{
									//mail details - flag = 1

									//to account id details
									methods.__getAccountIdsDetails(mail_details.to_account_id, function (to_details) {

										//cc account id details
										methods.__getAccountIdsDetails(mail_details.cc_account_id, function (cc_details) {

											//bcc account id details
											methods.__getAccountIdsDetails(mail_details.bcc_account_id, function (bcc_details) {

												//get folder id
												methods.__getFolderIdByMailId(arrParams.mail_group_id, arrParams.account_id, function (folder_id) {
													
													//if folder is inbox then update read status
													var update_arr = {
														account_id		:	arrParams.account_id,
														mail_group_id	:	arrParams.mail_group_id,
														folder_id		:	folder_id
													}

													methods.__updateReadStatus(update_arr, function (update_status) {

														//attachment	
														var attachment_arr = [];

														if(mail_details.attachment !== null && mail_details.attachment !== "" )
														{
															Object.values(PHPUnserialize.unserialize(mail_details.attachment)).forEach(function(row) {
																
																if(row !== null && row !== "" && Object.keys(row).length > 0)
																{ 
																	var filename = row.file_name.split(/_(.+)/)[1];

																	var extension = row.file_name.split('.')[1];

																	var file_type = getFileTypes('.'+extension);																		
																	
																	var attach_arr = {
																				file_name	:	filename,
																				filepath	:	G_STATIC_IMG_URL+'uploads/jazenet/jazemail/attachments/'+row.file_name,
																				file_id		:	row.file_name,
																				filetype	:	file_type,
																				iv			:	row.iv
																			};
																	
																	if(parseInt(folder_id) == 2){
																		attach_arr.file_id = row.file_name;
																	}
																	
																	attachment_arr.push(attach_arr);
																}
															});

														}

														methods.__getMasterId(mail_details.account_id, arrParams.mail_group_id, function(mail_id)
														{
															//get history_details
															var hist_arr = {
																account_id		:	arrParams.account_id,
																mail_group_id	:	arrParams.mail_group_id,
																parent_id		:	mail_id,
																flag			:	arrParams.history,
																folder_id		:	folder_id
															};
															
															methods.__getHistoryMails(hist_arr, function(history_mails)
															{	//console.log('history_mails',history_mails);															
																HelperGeneral.getAccountBasicDetails(mail_details.account_id , function (account_details) {

																	var email_details = {
																		id					: arrParams.mail_group_id.toString(),
																		master_id			: mail_id.toString(),  //list group id || parent id
																		subject				: decrypt_subject,
																		date				: mail_details.create_date.toString(),
																		from_account_id		: mail_details.account_id.toString(),
																		from_account_type	: account_details.account_type.toString(),
																		from_name			: account_details.name,
																		from_logo			: account_details.logo,  
																		to_account_id		: to_details,
																		cc_account_id		: cc_details,
																		bcc_account_id		: bcc_details,
																		attachment			: attachment_arr,
																		message 			: decrypt_msg,
																		folder_id			: folder_id.toString()
																	}

																	if(parseInt(arrParams.history) == 1)
																	{
																		email_details.history_mails = history_mails;
																	}
																	
																	return_result(email_details);
																});
															});
														});
													});
												});
											});
										});												
									});
								}								
							});
						});
					});
				}
				else
				{ return_result(null); }
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log('__getMailDetailsByGroupId',err);
		return_result(null);
	}
}

/*
* get history details of mail details 
*/
methods.__getHistoryMails = function(arrParams, return_result)
{
	var return_obj = [];
	
	try
	{
		if(arrParams !== null && parseInt(arrParams.account_id) !== 0 && parseInt(arrParams.parent_id) !== 0 && parseInt(arrParams.mail_group_id) !== 0 && parseInt(arrParams.flag) !== 0)
		{
			//to get folder ids of master mail to check if master is in trash
			var query = "SELECT meta_value FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'mail_group_id' AND group_id = '"+arrParams.parent_id+"' ";

			//query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'status' AND meta_value = '1') ";

			DB.query(query,null, function(parent_mail_group_id, error)
			{
				if(typeof parent_mail_group_id !== 'undefined' && parent_mail_group_id !== null && parseInt(parent_mail_group_id.length) > 0 && parseInt(parent_mail_group_id[0]['meta_value']) != 0)
				{
					var query = "SELECT * FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'folder_id' AND group_id IN ";

					query += " (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'mail_group_id' AND meta_value = '"+parent_mail_group_id[0]['meta_value']+"' )";

					query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'account_id' AND meta_value = '"+arrParams.account_id+"')";		
										
					DB.query(query,null, function(parent_folder, error)
					{	
						if(typeof parent_folder !== 'undefined' && parent_folder !== null && parseInt(parent_folder.length) > 0 )
						{
							var parent_folder_id = parent_folder[0]['meta_value'];

							var parent_mail_id = parent_folder[0]['group_id'];
						}
						else
						{
							var parent_folder_id = 0;

							var parent_mail_id = 0;
						}	
						var query = "SELECT meta_value FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'mail_group_id' AND meta_value != '"+arrParams.mail_group_id+"' AND ";

						query += " group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'parent_id' AND meta_value = '"+arrParams.parent_id+"')";

						query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'account_id' AND meta_value = '"+arrParams.account_id+"')";

						query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'status' AND meta_value = '1') ";

						if(parseInt(arrParams.folder_id) == 4)
						{
							query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'folder_id' AND meta_value = '4')";
						}
						else
						{
							query += " AND group_id NOT IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'folder_id' AND meta_value = '4')";
						}
						
						if(parseInt(arrParams.folder_id) == 4 || (parseInt(parent_folder_id) !== 4 && parseInt(parent_folder_id) !== 0 && parent_folder_id !== null ))
						{
							query += " UNION (SELECT meta_value FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'mail_group_id' AND group_id = '"+parent_mail_id+"' AND meta_value != '"+arrParams.mail_group_id+"' ";

							query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'status' AND meta_value = '1') )";
						}
						
						DB.query(query,null, function(history, error)
						{
							if(typeof history !== 'undefined' && history !== null && parseInt(history.length) > 0 )
							{
								var counter = history.length;

								if(parseInt(counter) !== 0)
								{
									promiseForeach.each(history,[function (group_id) {

										return new Promise(function (resolve, reject) {

											var args = {
												account_id		:	arrParams.account_id,
												mail_group_id	:	group_id['meta_value'],
												flag			:	1,
												history			:	0,
												read_status		: 	""														
											}
											
											//get mail details
											methods.__getMailDetailsByGroupId(args, function (mail_details) 
											{	
												return_obj.push(mail_details);
												
												counter -= 1;	
																		
												if (counter === 0)
												{	
													resolve(return_obj);
												}
											});

										});
									}],
									function (result, current) 
									{											
										if (counter === 0)
										{ return_result(fast_sort(result[0]).desc('id')); }
									});	
								}
								else
								{ return_result(return_obj); }					
							}
							else
							{
								return_result(return_obj);
							}
						});
					});
				}
				else{return_result(return_obj);}
			});
		}
		else{return_result(return_obj);}
	}
	catch(err)
	{
		console.log('__getHistoryMails',err);
		return_result(return_obj)
	}
}
/*
* get account details by folder id basis using mail group id and account id
*/
methods.__getAccountDetailsByFolderId = function(account_id, mail_group_id, all_account_ids, return_result)
{
	var return_obj = {
		account_id	: '',
		account_type: '',
		name		: '',
		logo		: ''
	};

	try
	{
		if(parseInt(account_id) != 0 && parseInt(mail_group_id) != 0 )
		{
			methods.__getFolderIdByMailId(mail_group_id, account_id, function (folder_id) 
			{
				//draft and send folders
				if(parseInt(folder_id) == 2 ||  parseInt(folder_id) == 3)
				{	

					methods.__getAccountDetails(0, all_account_ids, function (account_details) 
					{
						return_result(account_details);
					});
				}
				//trash and customised folders - contains both sent and received mails
				else if(parseInt(folder_id) > 3 )
				{
					var query = "SELECT meta_value FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'read_status' AND group_id IN "

					query += " (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'account_id' AND meta_value = '"+account_id+"' )";

					query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'mail_group_id' AND meta_value = '"+mail_group_id+"' )";

					DB.query(query,null, function(result, error)
					{	
						if(typeof result !== 'undefined' && result !== null && parseInt(result.length) > 0)
						{
							methods.__getAccountDetails(all_account_ids.sender, null, function (account_details) 
							{
								return_result(account_details);
							});
						}
						else
						{
							methods.__getAccountDetails(0, all_account_ids, function (account_details) 
							{
								return_result(account_details);
							});
						}
					});
				}
				else
				{
					methods.__getAccountDetails(all_account_ids.sender, null, function (account_details) 
					{
						return_result(account_details);
					});
				}
			});
		}
		else
		{
			return_result(return_obj);
		}
	}
	catch(err)
	{
		console.log('__getAccountDetailsByFolderId',err);
		return_result(return_obj);
	}
}

methods.__getAccountDetails = function(sender, receiver_arr, return_result)
{
	var return_obj = {
		account_id	: '',
		account_type: '',
		name		: '',
		logo		: ''
	};

	try
	{
		if(parseInt(sender) !== 0)
		{
			HelperGeneral.getAccountBasicDetails(sender , function (account_details) {

				if (account_details !== null)
				{									
					return_result(account_details);
				}
				else
				{ return_result(return_obj); }
			});
		}
		else
		{
			if(receiver_arr !== null)
			{
				var account_id_sel = (parseInt(receiver_arr.receiver) !== 0 && receiver_arr.receiver !== '') ? receiver_arr.receiver : ((parseInt(receiver_arr.cc) !== 0 && receiver_arr.cc !== '') ? receiver_arr.cc : ((parseInt(receiver_arr.bcc) !== 0 && receiver_arr.bcc !== '') ? receiver_arr.bcc : '0'));
					
				if(parseInt(account_id_sel) !== 0)
				{
					HelperGeneral.getAccountBasicDetails(account_id_sel , function (account_details) {

						if (account_details !== null)
						{
							return_result(account_details);
						}
						else
						{ return_result(return_obj); }
					});
				}
				else
				{ return_result(return_obj); }
			}
			else
			{ return_result(return_obj); }
		}
	}
	catch(err)
	{
		console.log('__getAccountDetails',err);
		return_result(return_obj);
	}
}

/*
* get master id
*/
methods.__getMasterId = function(account_id, mail_group_id, return_result)
{
	var group_id = 0;
	
	try
	{
		if(parseInt(account_id) != 0 && parseInt(mail_group_id) != 0 )
		{
			var query = " SELECT * FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'parent_id' ";

			query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'account_id' AND meta_value = '"+account_id+"') ";

			query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'mail_group_id' AND meta_value = '"+mail_group_id+"')";

			//query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'status' AND meta_value = '1')";
			
			DB.query(query,null, function(result, error)
			{			
				if(typeof result !== 'undefined' && result !== null && parseInt(result.length) > 0)
				{
					if(parseInt(result[0]['meta_value']) !== 0)
					{
						return_result(result[0]['meta_value']);
					}
					else
					{
						var query = " SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'parent_id' ";

						query += " AND group_id NOT IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'read_status' AND meta_value IN (0,1)) ";

						query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'mail_group_id' AND meta_value = '"+mail_group_id+"')";

						//query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'status' AND meta_value = '1')";
						
						DB.query(query,null, function(data, error)
						{
							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
							{
								return_result(data[0]['group_id']);
							}
							else
							{
								return_result(group_id);
							}
						});
					}
				}
				else
				{
					return_result(group_id);
				}
			});
		}
		else
		{
			return_result(group_id);
		}
	}
	catch(err)
	{
		console.log('__getMasterId',err);
		return_result(group_id);
	}
}

/*
* get master id
*/
methods.__getMailReplyCount = function(account_id, parent_id, mail_group_id, return_result)
{
	var group_id = 0;
	
	try
	{
		if(parseInt(parent_id) != 0)
		{
			var query = "SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'parent_id' AND ";
			
			query += " group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'parent_id' AND FIND_IN_SET('"+parent_id+"', meta_value) > 0 )";

			query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'account_id' AND meta_value = '"+account_id+"')";

			DB.query(query,null, function(reply, error){

				var query = " SELECT meta_value FROM `jaze_jazecom_jazeemail_list` WHERE group_id = '"+parent_id+"' AND meta_key = 'mail_group_id'  ";

				DB.query(query,null, function(master_details, error){

					if (typeof master_details !== undefined && master_details !== null && parseInt(master_details.length) > 0){

						var query = " SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'parent_id' AND  ";

						query += " group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'mail_group_id' AND meta_value = '"+master_details[0]['meta_value']+"')";

						query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'account_id' AND meta_value = '"+account_id+"')";

						DB.query(query,null, function(master_count, error){
							
							var reply_count = parseInt(reply.length) + parseInt(master_count.length);

							if(parseInt(reply_count) <= 1)
							{
								return_result(group_id);
							}
							else
							{
								return_result(reply_count);
							}							
						});
					}
					else
					{
						return_result(reply.length);
					}
				});								
			});
		}
		else
		{
			methods.__getMasterId(account_id, mail_group_id, function(parent)
			{
				methods.__getMailReplyCount(account_id, parent, mail_group_id, function(data_count)
				{
					return_result(data_count);
				});
			});
		}
	}
	catch(err)
	{
		console.log('__getMailReplyCount',err);
		return_result(group_id);
	}
}

/*
* get receiver account ids details
*/
methods.__getAccountIdsDetails = function(account_ids, return_result)
{
	try
	{
		var account_ids_arr = [];

		account_ids_arr = __getAccountidArray(account_ids_arr, account_ids, 0);

		if(account_ids_arr !== null && account_ids_arr !== '' && parseInt(account_ids_arr.length) > 0){

			var account_ids_details = [];

			var counter = account_ids_arr.length;

			if(parseInt(counter) !== 0)
			{
				promiseForeach.each(account_ids_arr,[function (account_id) {

					return new Promise(function (resolve, reject) {

						HelperGeneral.getAccountBasicDetails(account_id , function (account_details) {

							if (account_details !== null)
							{	
								var details = {									
									account_id		: account_id.toString(),
									account_type	: account_details.account_type.toString(),
									name			: account_details.name,
									logo			: account_details.logo,  
									title			: account_details.title
								}

								account_ids_details.push(details);

								counter -= 1;	
															
								if (counter === 0)
								{	
									resolve(account_ids_details);
								}
							}
							else
							{ return_result([]); }
						});
					});
				}],
				function (result, current) 
				{											
					if (counter === 0)
					{ return_result(result[0]); }
				});	
			}
			else
			{ return_result([]); }
		}
		else
		{ return_result([]); }
	}
	catch(err)
	{
		console.log('__getAccountIdsDetails',err);
		return_result([]);
	}
}

/*
* get folder id by mail group id
*/
methods.__getFolderIdByMailId = function(mail_group_id, account_id, return_result)
{
	try
	{
		if(parseInt(account_id) !== 0 && parseInt(mail_group_id) !== 0)
		{
			var query = " SELECT * FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'folder_id' ";

			query += " AND group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'status' and  meta_value = '1' )";

			query += " AND group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'account_id' and  meta_value = '"+account_id+"' )";

			query += " AND group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'mail_group_id' and  meta_value = '"+mail_group_id+"' );";

			DB.query(query,null, function(folder_details, error){

				if (typeof folder_details !== undefined && folder_details !== null && parseInt(folder_details.length) > 0){

					var folder_id = folder_details[0]['meta_value'];
					
					return_result(folder_id); 
				}
				else
				{ return_result(0); }
			});
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{
		console.log('__getFolderIdByMailId',err);
		return_result(0);
	}
}

/*
* update read status
*/
methods.__updateReadStatus = function(arrParams, return_result)
{
	try
	{
		if(parseInt(arrParams.account_id) !== 0 && parseInt(arrParams.mail_group_id) !== 0 && parseInt(arrParams.folder_id) == 1)
		{
			var query = " SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'status' ";

			query += " AND group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'status' and  meta_value = '1' )";

			query += " AND group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'account_id' and  meta_value = '"+arrParams.account_id+"' )";

			query += " AND group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'mail_group_id' and  meta_value = '"+arrParams.mail_group_id+"' )";

			query += " AND group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'folder_id' and  meta_value = '1' )";

			query += " AND group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'read_status' and  meta_value = '0' );";

			DB.query(query,null, function(groupid, error){

				if (typeof groupid !== undefined && groupid !== null && parseInt(groupid.length) > 0){

					var group_id = groupid[0]['group_id'];
					
					//update
					var update_arr = { read_status : 1};

					HelperRestriction.updateQuery('jaze_jazecom_jazeemail_list',group_id,update_arr, function (update_id) {

						if(parseInt(update_id) !== 0)
						{
							return_result(update_id); 
						}
						else
						{ return_result(0); }
					});
				}
				else
				{ return_result(0); }
			});
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{
		console.log('__updateReadStatus',err);
		return_result(0);
	}
}

/*
* get unread mail count
*/
methods.getAllEmailCount = function(account_id, folder_ids = null, return_result)
{
	try
	{
		if(folder_ids === null)
		{			
			folder_ids = ['1','2','4'];
		}		

		var count_arr = {};
		
		if(parseInt(account_id) != 0 ){

			folder_ids.forEach(function(id) {

				var query = " SELECT count(DISTINCT(group_id)) AS count FROM `jaze_jazecom_jazeemail_list` WHERE ";

				query += " group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'status' and  meta_value = '1' )";

				query += " AND group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'account_id' and  meta_value = '"+account_id+"' )";

				query += " AND group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'folder_id' and  meta_value = '"+id+"' )";

				if(parseInt(id) == 1)
				{
					query += " AND group_id IN ( SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'read_status' and  meta_value = '0' );";
				}				

				DB.query(query,null, function(count, error){

					if(parseInt(id) == 1)
					{
						count_arr.inbox_count = count[0].count.toString();
					}
					else if(parseInt(id) == 2)
					{
						count_arr.drafts_count = count[0].count.toString();
					}
					else if(parseInt(id) == 3)
					{
						count_arr.sent_count = count[0].count.toString();
					}
					else if(parseInt(id) == 4)
					{
						count_arr.trash_count = count[0].count.toString();
					}
					else
					{
						count_arr.mail_count = count[0].count.toString();
					}
					
					if(parseInt(folder_ids.length) == parseInt(Object.keys(count_arr).length)){

						return_result(count_arr);
					}					
				});
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log('getAllEmailCount',err);
		return_result(null);
	}
}

/*
* delete mail
*/
methods.deleteMail = function(arrParams,return_result)
{
	try
	{		
		if(arrParams !== null && parseInt(arrParams.account_id) !== 0 && arrParams.mail_group_id !== null && parseInt(arrParams.folder_id) !== 0)
		{

			if (arrParams.mail_group_id.indexOf(',') > -1) 
			{
				var mail_group_ids = arrParams.mail_group_id.split(',');
			}
			else
			{
				var mail_group_ids = arrParams.mail_group_id.split();
			}

			var counter = mail_group_ids.length;

			if(parseInt(counter) !== 0)
			{
				promiseForeach.each(mail_group_ids,[function (mail_id) {
					
					return new Promise(function (resolve, reject) {	

						var arr = {
							account_id		:	arrParams.account_id,
							folder_id		:	arrParams.folder_id,
							mail_group_id	:	mail_id
						}

						counter -= 1;	

						methods.__deleteMailDetails(arr, 0, function(delete_mail){
							if (counter === 0){ 
								resolve(delete_mail);
							}
						});						
					});
				}],
				function (result, current) 
				{									
					if (counter === 0){ 
						return_result(result[0]);
					}
				});
			}
			else
			{
				return_result(0); 
			}
		}
		else
		{ return_result(0); }		
	}
	catch(err)
	{console.log("deleteMail",err); return_result(0)}
}

/*
* delete mail by group id
*/
methods.__deleteMailDetails = function(arrParams, flag, return_result)
{
	try
	{
		if(arrParams !== null && parseInt(arrParams.account_id) !== 0 && parseInt(arrParams.mail_group_id) !== 0)
		{
			//draft mail
			if(parseInt(flag) == 1)
			{
				//get mail id of this mail group id
				var query = "SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='mail_group_id' AND group_id IN ";

				query += " (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='mail_group_id' AND meta_value = '"+arrParams.mail_group_id+"' )";

				query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='account_id' AND meta_value = '"+arrParams.account_id+"' )";

				query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='folder_id' AND meta_value = '"+arrParams.folder_id+"' )";
				
				DB.query(query,null, function(mail_id, error){
					
					if (typeof mail_id !== undefined && mail_id !== null && parseInt(mail_id.length) > 0){

						var query = "DELETE FROM `jaze_jazecom_jazeemail_list` WHERE group_id = '"+mail_id[0]['group_id']+"' ";

						HelperRestriction.deleteQuery(query, function (delete_mail_list){

							// var query = "SELECT count(group_id) as count FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='mail_group_id' AND group_id IN ";

							// query += " (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='mail_group_id' AND meta_value = '"+arrParams.mail_group_id+"' )";

							// DB.query(query,null, function(exist_ids, error){

							// 	if(parseInt(exist_ids[0]['count']) == 0)
							// 	{
									var query = "DELETE FROM `jaze_jazecom_jazeemail_details` WHERE group_id = '"+arrParams.mail_group_id+"' ";

									HelperRestriction.deleteQuery(query, function (delete_mail)
									{
										return_result(delete_mail);
									});
								// }
								// else
								// {
								// 	return_result(delete_mail_list);
								// }
							// });
						});
					}
					else
					{return_result(1);}	
				});
			}
			//deleting mails from trash by changing status
			else
			{
				var update_status = {status : "0"};

				methods.__getSubMailListIds(arrParams.mail_group_id, arrParams.account_id, 4, function (group_ids) 
				{
					if(group_ids !== null && parseInt(group_ids) !== 0)
					{
						if (group_ids.indexOf(',') > -1) 
						{
							var mail_ids = group_ids.split(',');
						}
						else
						{
							var mail_ids = group_ids.split();
						}

						mail_ids.forEach(function(id)
						{
							HelperRestriction.updateQuery('jaze_jazecom_jazeemail_list',id,update_status, function (update_result) 
							{
								//check this mail group_id exists for another accounts
								var query = "SELECT count(group_id) as count FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='mail_group_id' AND group_id IN ";

								query += " (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='mail_group_id' AND meta_value = '"+arrParams.mail_group_id+"' )";

								query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='status' AND meta_value = '1' )";

								DB.query(query,null, function(exist_ids, error){

									if(parseInt(exist_ids[0]['count']) == 0)
									{
										HelperRestriction.updateQuery('jaze_jazecom_jazeemail_details',arrParams.mail_group_id,update_status, function (update_mail_det) 
										{
											var ret = (parseInt(update_mail_det) == 0) ? '0' : '1';
											return_result(ret);
										});
									}
									else
									{
										var ret = (parseInt(update_result) == 0) ? '0' : '1';
										return_result(ret);
									}
								});								
							});
						});
					}
					else
					{
						//check this mail group_id exists for another accounts
						var query = "SELECT count(group_id) as count FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='mail_group_id' AND group_id IN ";

						query += " (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='mail_group_id' AND meta_value = '"+arrParams.mail_group_id+"' )";

						query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='status' AND meta_value = '1' )";

						DB.query(query,null, function(exist_ids, error){

							if(parseInt(exist_ids[0]['count']) == 0)
							{
								HelperRestriction.updateQuery('jaze_jazecom_jazeemail_details',arrParams.mail_group_id,update_status, function (update_mail_det) 
								{
									return_result(1);
								});
							}
							else
							{return_result(1);}
						});
					}
				});
			}
		}
		else{return_result(0);}				
	}
	catch(err)
	{console.log("__deleteMailDetails",err); return_result(0);}
}

/*
* move mail
*/
methods.moveMailOld = function(arrParams,return_result)
{
	try
	{	
		if(arrParams !== null && parseInt(arrParams.account_id) !== 0 && arrParams.mail_group_id !== null && parseInt(arrParams.move_folder_id) !== 0)
		{
			if (arrParams.mail_group_id.indexOf(',') > -1) 
			{
				var mail_group_ids = arrParams.mail_group_id.split(',');
			}
			else
			{
				var mail_group_ids = arrParams.mail_group_id.split();
			}

			var counter = mail_group_ids.length;

			if(parseInt(counter) !== 0)
			{
				promiseForeach.each(mail_group_ids,[function (mail_id) {
					
					return new Promise(function (resolve, reject) {	

						var group_ids = [];

						var query = "SELECT * FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='parent_id' AND group_id IN ";

						query += " (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='mail_group_id' AND meta_value = '"+mail_id+"' )";

						query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='account_id' AND meta_value = '"+arrParams.account_id+"' )";
						
						DB.query(query,null, function(mail, error){

							if (typeof mail !== undefined && mail !== null && parseInt(mail.length) > 0)
							{								
								if(parseInt(mail[0]['meta_value']) !== 0)
								{
									//get all mails with this parent id and same account id
									var query = "SELECT group_concat(group_id) as group_id FROM `jaze_jazecom_jazeemail_list` WHERE group_id != '"+mail[0]['group_id']+"' AND meta_key ='parent_id' AND ";

									query += " group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='parent_id' AND meta_value = '"+mail[0]['meta_value']+"' )";

									query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='account_id' AND meta_value = '"+arrParams.account_id+"' )";

									DB.query(query,null, function(all_mails, error)
									{
										if (all_mails[0]['group_id'] !== null && parseInt(all_mails[0]['group_id']) != 0)
										{
											group_ids = all_mails[0]['group_id']+','+mail[0]['group_id'];
										}
										else
										{
											group_ids = mail[0]['group_id'].toString();;
										}
										
										//get parent mail 
										var query = "SELECT meta_value FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'mail_group_id' AND group_id = '"+mail[0]['meta_value']+"' ";

										DB.query(query,null, function(parent_mail, error)
										{
											if (typeof parent_mail !== undefined && parent_mail !== null && parseInt(parent_mail.length) > 0)
											{
												var query = "SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='parent_id' AND group_id IN ";

												query += " (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='mail_group_id' AND meta_value = '"+parent_mail[0]['meta_value']+"' )";

												query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='account_id' AND meta_value = '"+arrParams.account_id+"' )";

												DB.query(query,null, function(parent_id, error)
												{
													if (typeof parent_id !== undefined && parent_id !== null && parseInt(parent_id.length) > 0)
													{
														group_ids += ','+parent_id[0]['group_id'];
													}

													methods.__updateMailFolder(group_ids, arrParams.move_folder_id, function(update_folder)
													{
														counter -= 1;	

														if (counter === 0){ 
															resolve(1);
														}
													});
												});
											}
											else
											{
												methods.__updateMailFolder(group_ids, arrParams.move_folder_id, function(update_folder)
												{
													counter -= 1;	

													if (counter === 0){ 
														resolve(1);
													}
												});
											}
										});
									});
								}
								else
								{
									group_ids = mail[0]['group_id'].toString();

									methods.__updateMailFolder(group_ids, arrParams.move_folder_id, function(update_folder)
									{
										counter -= 1;	

										if (counter === 0){ 
											resolve(1);
										}
									});
								}
							}
							else
							{
								counter -= 1;	

								if (counter === 0){ 
									resolve(1);
								}
							}
						});
					});
				}],
				function (result, current) 
				{									
					if (counter === 0){ 
						return_result(result[0]);
					}
				});
			}
			else
			{
				return_result(0); 
			}
		}
		else
		{ return_result(0); }	
	}
	catch(err)
	{console.log("moveMail",err); return_result(0)}
}

/*
* move mail
*/
methods.moveMail = function(arrParams,return_result)
{
	try
	{	
		if(arrParams !== null && parseInt(arrParams.account_id) !== 0 && arrParams.mail_group_id !== null && parseInt(arrParams.move_folder_id) !== 0)
		{
			if (arrParams.mail_group_id.indexOf(',') > -1) 
			{
				var mail_group_ids = arrParams.mail_group_id.split(',');
			}
			else
			{
				var mail_group_ids = arrParams.mail_group_id.split();
			}
			
			var counter = mail_group_ids.length;

			if(parseInt(counter) !== 0)
			{
				promiseForeach.each(mail_group_ids,[function (mail_id) {
					
					return new Promise(function (resolve, reject) {	

						methods.__getSubMailListIds(mail_id, arrParams.account_id, 0, function (group_ids) 
						{
							if(group_ids !== null)
							{
								methods.__updateMailFolder(group_ids, arrParams.move_folder_id, function(update_folder)
								{
									counter -= 1;	

									if (counter === 0){ 
										resolve(1);
									}
								});
							}
							else
							{
								counter -= 1;	

								if (counter === 0){ 
									resolve(1);
								}
							}
						});
					});
				}],
				function (result, current) 
				{									
					if (counter === 0){ 
						return_result(result[0]);
					}
				});
			}
			else
			{
				return_result(0); 
			}
		}
		else
		{ return_result(0); }	
	}
	catch(err)
	{console.log("moveMail",err); return_result(0)}
}

methods.__updateMailFolder = function(group_ids, move_folder_id, return_result)
{
	try
	{
		if(group_ids !== null && parseInt(group_ids) !== 0)
		{
			if (group_ids.indexOf(',') > -1) 
			{
				var mail_ids = group_ids.split(',');
			}
			else
			{
				var mail_ids = group_ids.split();
			}

			var max_group_id = mail_ids.reduce(function(a, b) {
				return Math.max(a, b);
			});
				
			var update_arr = {folder_id : move_folder_id};

			var counter = mail_ids.length;

			mail_ids.forEach(function(id){

				if(parseInt(id) === parseInt(max_group_id))
				{
					update_arr.show_in_list = 1;
				}
				else
				{
					update_arr.show_in_list = 0;
				}
									
				HelperRestriction.updateQuery('jaze_jazecom_jazeemail_list',id,update_arr, function (update_result) 
				{														
					counter -= 1;	

					if (counter === 0){ 
						return_result(1);
					}
				});
			});			
		}
		else
		{
			return_result(0);
		}
	}
	catch(err)
	{console.log("__updateMailFolder",err); return_result(0);}
}

/*
* get all sub mail list ids of given mail list id
*/
methods.__getSubMailListIds = function(mail_group_id, account_id, folder_id, return_result)
{
	var group_ids = '';

	try
	{
		if(parseInt(mail_group_id) != 0 && parseInt(account_id) != 0 )
		{
			//get parent details
			var query = "SELECT * FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='parent_id' AND group_id IN ";

			query += " (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='mail_group_id' AND meta_value = '"+mail_group_id+"' )";

			query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='account_id' AND meta_value = '"+account_id+"' )";
						
			DB.query(query,null, function(mail, error){

				if (typeof mail !== undefined && mail !== null && parseInt(mail.length) > 0)
				{	
					//parent_id
					if(parseInt(mail[0]['meta_value']) !== 0)	
					{
						//get all mails with this parent id and same account id
						var query = "SELECT group_concat(group_id) as group_id FROM `jaze_jazecom_jazeemail_list` WHERE group_id != '"+mail[0]['group_id']+"' AND meta_key ='parent_id' AND ";

						query += " group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='parent_id' AND meta_value = '"+mail[0]['meta_value']+"' )";

						query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='account_id' AND meta_value = '"+account_id+"' )";

						if(parseInt(folder_id) == 4)
						{
							query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='folder_id' AND meta_value = '"+folder_id+"' )";
						}

						DB.query(query,null, function(all_mails, error)
						{
							if (all_mails[0]['group_id'] !== null && parseInt(all_mails[0]['group_id']) != 0)
							{
								group_ids = all_mails[0]['group_id']+','+mail[0]['group_id'];
							}
							else
							{
								group_ids = mail[0]['group_id'].toString();
							}

							//get parent mail 
							var query = "SELECT meta_value FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'mail_group_id' AND group_id = '"+mail[0]['meta_value']+"' ";

							DB.query(query,null, function(parent_mail, error)
							{
								if (typeof parent_mail !== undefined && parent_mail !== null && parseInt(parent_mail.length) > 0)
								{
									var query = "SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='parent_id' AND group_id IN ";

									query += " (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='mail_group_id' AND meta_value = '"+parent_mail[0]['meta_value']+"' )";

									query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='account_id' AND meta_value = '"+account_id+"' )";

									if(parseInt(folder_id) == 4)
									{
										query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='folder_id' AND meta_value = '"+folder_id+"' )";
									}

									DB.query(query,null, function(parent_id, error)
									{
										if (typeof parent_id !== undefined && parent_id !== null && parseInt(parent_id.length) > 0)
										{
											group_ids += ','+parent_id[0]['group_id'];
											return_result(group_ids);
										}
										else{return_result(group_ids);}
									});
								}
								else{return_result(group_ids);}
							});										
						});
					}
					else
					{
						group_ids = mail[0]['group_id'].toString();
						return_result(group_ids);
					}
				}
				else{return_result(group_ids);}
			});											
		}
		else{return_result(group_ids);}
	}
	catch(err)
	{
		console.log('__getSubMailListIds',err);
		return_result(group_ids);
	}
}

//------------------------------------------------------------------- MAIL MODULE END -----------------------------------------------------------//

//------------------------------------------------------------------- FOLDER MODULE START -------------------------------------------------------//

/*
* get folder list
*/
methods.__getFolderList = function(account_id, parent_id = 0, return_result)
{
	var return_obj = [];

	try
	{
		if(parseInt(account_id) != 0 )
		{
			methods.__getMasterFolderDetails(account_id, parent_id, function (master_mail_count) {		

				var query = " SELECT * FROM `jaze_jazecom_jazeemail_folder_list` WHERE group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_folder_list` WHERE meta_key = 'account_id' AND meta_value = '"+account_id+"')";

				query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_folder_list` WHERE meta_key = 'parent_id' AND meta_value = '"+parent_id+"')";

				query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_folder_list` WHERE meta_key = 'status' AND meta_value = '1')";
				

				DB.query(query,null, function(result, error){

					HelperRestriction.convertMultiMetaArray(result, function (folder_list) {	
						
						if(typeof folder_list !== 'undefined' && folder_list !== null && parseInt(folder_list.length) > 0)
						{
							var counter = folder_list.length;

							var folder_list_arr = [];

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(folder_list,[function (folder) {
									
									return new Promise(function (resolve, reject) {		

										methods.__getFolderList(account_id,folder.group_id, function(child_list){
										
											methods.__getFolderDetailsByGroupId(folder.parent_id, function (parent_details) {
												
												var parent_name = (parent_details !== null) ? parent_details.folder_name : "";

												var folder_ids = [];

												folder_ids.push(folder.group_id);
												
												methods.getAllEmailCount(account_id, folder_ids, function (mail_count) {
													
													var objRres = {
														folder_id		:	folder.group_id.toString(),
														folder_name		:	folder.folder_name,
														parent_name		:	parent_name,
														parent_id		:	parent_id.toString(),
														count			:	mail_count.mail_count.toString(),
														is_master		:	"0",
														child_list     	: 	child_list
													};
													
													folder_list_arr.push(objRres);
													
													counter -= 1;

													resolve(folder_list_arr);												

												});
											});
										});
									});
								}],
								function (result, current) 
								{									
									if (counter === 0)
									{ 
										if(parseInt(parent_id) == 0)
										{
											const folder_arrays = master_mail_count.concat(result[0]);

											return_result(folder_arrays);

										}
										else
										{
											return_result(result[0]);
										}
									}
								});
							}
							else
							{
								return_result(master_mail_count); 
							}	
						}
						else
						{
							return_result(master_mail_count); 
						}
					});
				});
			});
		}
		else
		{
			return_result(return_obj);
		}
	}
	catch(err)
	{
		console.log('__getFolderList',err);
		return_result(return_obj);
	}
}

/*
* get master folder list details
*/
methods.__getMasterFolderDetails = function(account_id, parent_id, return_result)
{
	var return_obj = [];

	try
	{
		if(parseInt(account_id) != 0 && parseInt(parent_id) == 0 )
		{
			var folder_ids = ['1','2','3','4'];

			methods.getAllEmailCount(account_id, folder_ids, function (master_mail_count) {

				for(var i=1; i <= 4; i++)
				{
					var count = (parseInt(i) == 1) ? master_mail_count.inbox_count : ((parseInt(i) == 2) ? master_mail_count.drafts_count : ((parseInt(i) == 3) ? master_mail_count.sent_count : master_mail_count.trash_count));

					var is_master = (parseInt(i) == 1 || parseInt(i) == 2) ? "1" : ((parseInt(i) == 3 || parseInt(i) == 4) ? "2" : "0");
					
					var folder_arr = {
						folder_id		:	i.toString(),
						folder_name		:	__getFolderName(i),
						parent_name		:	"",
						parent_id		:	parent_id.toString(),
						count			:	count.toString(),
						is_master		:	is_master.toString(),
						child_list     	: 	[]
					};
					
					return_obj.push(folder_arr);
				}

				return_result(return_obj);
			
			});
		}
		else
		{
			return_result(return_obj);
		}
	}
	catch(err)
	{
		console.log('__getMasterFolderDetails',err);
		return_result(return_obj);
	}
}

/*
* get folder details by group_id
*/
methods.__getFolderDetailsByGroupId = function(folder_id, return_result)
{
	try
	{
		if(parseInt(folder_id) != 0 )
		{
			var query = " SELECT * FROM `jaze_jazecom_jazeemail_folder_list` WHERE group_id = '"+folder_id+"'";

			DB.query(query,null, function(result, error){

				HelperRestriction.convertMetaArray(result, function (folder_details) {	
					
					return_result(folder_details);
				});
			});
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{
		console.log('__getFolderDetailsByGroupId',err);
		return_result(null);
	}
}

/*
* save send email
*/
methods.saveCreateUpdateFolder = function(arrParams,return_result)
{
	var return_obj = [];

	try
	{		
		if(arrParams !== null && parseInt(arrParams.account_id) !== 0 && arrParams.folder_name !== null)
		{
			
			//check folder name exists
			var query = "SELECT * FROM `jaze_jazecom_jazeemail_folder_list` WHERE group_id != '"+arrParams.folder_id+"' ";

			query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_folder_list` WHERE meta_key ='account_id' AND meta_value = '"+arrParams.account_id+"' )";

			query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_folder_list` WHERE meta_key ='folder_name' AND meta_value = '"+arrParams.folder_name+"' )";

			DB.query(query,null, function(data, error){

				if (typeof data !== undefined && data !== null && parseInt(data.length) > 0)
				{
					return_result({error : 'folder name exists'});
				}
				else
				{
					var folder_details = {
						account_id	:	arrParams.account_id,
						folder_name	:	arrParams.folder_name,
						parent_id	:	arrParams.parent_id,
						status		:	'1'
					};
				
					//insert
					if(parseInt(arrParams.folder_id) == 0)
					{
						HelperRestriction.getNewGroupId('jaze_jazecom_jazeemail_folder_list', function (group_id) {

							if(parseInt(group_id) !== 0)
							{	
								if(parseInt(group_id) < 100)
								{
									group_id = parseInt(group_id) + 100;
								}

								HelperRestriction.insertTBQuery('jaze_jazecom_jazeemail_folder_list',group_id,folder_details, function (insert_result) {

									methods.__getFolderList(arrParams.account_id,0, function(folder_list){
										return_result(folder_list);
									});
								});
							}
							else
							{ return_result(return_obj); }
						});
					}
					//update
					else
					{
						HelperRestriction.updateQuery('jaze_jazecom_jazeemail_folder_list',arrParams.folder_id,folder_details, function (update_result) {

							if(parseInt(update_result) !== 0)
							{
								methods.__getFolderList(arrParams.account_id,0, function(folder_list){
									return_result(folder_list);
								});
							}
							else
							{ return_result(return_obj); }
						});
					}
				}
			});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{console.log("saveCreateUpdateFolder",err); return_result(return_obj)}
}

/*
* delete folder details
*/
methods.deleteFolderDetails = function(arrParams,return_result)
{
	try
	{		
		if(arrParams !== null && parseInt(arrParams.account_id) !== 0 && parseInt(arrParams.folder_id) !== 0)
		{
			//get child list
			var query = "SELECT group_id FROM `jaze_jazecom_jazeemail_folder_list` WHERE meta_key = 'folder_name' AND group_id IN ";

			query += " (SELECT group_id FROM `jaze_jazecom_jazeemail_folder_list` WHERE meta_key ='parent_id' AND meta_value = '"+arrParams.folder_id+"' )";

			query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_folder_list` WHERE meta_key ='account_id' AND meta_value = '"+arrParams.account_id+"' )";

			DB.query(query,null, function(data, error){

				//move to new folder and delete
				if(parseInt(arrParams.new_folder_id) !== 0)
				{
					
					if (typeof data !== undefined && data !== null && parseInt(data.length) > 0)
					{
										
						var update_folder_details = {parent_id : arrParams.new_folder_id};

						Object.keys(data).forEach(function(k)
						{
							HelperRestriction.updateQuery('jaze_jazecom_jazeemail_folder_list',data[k]['group_id'],update_folder_details, function (update_result) {});
						});	
					}			

					//get mail list of folder id
					var query = "SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'folder_id' AND group_id IN ";

					query += " (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='folder_id' AND meta_value = '"+arrParams.folder_id+"' )";

					query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='account_id' AND meta_value = '"+arrParams.account_id+"' )";

					DB.query(query,null, function(mail_list, error){

						if (typeof mail_list !== undefined && mail_list !== null && parseInt(mail_list.length) > 0)
						{
							var update_folder_details = {folder_id : arrParams.new_folder_id};

							Object.keys(mail_list).forEach(function(k)
							{
								HelperRestriction.updateQuery('jaze_jazecom_jazeemail_list',mail_list[k]['group_id'],update_folder_details, function (update_result) {});
							});					
						}														
					});	
					
					//delete parent folder from folder list
					var query = "DELETE FROM `jaze_jazecom_jazeemail_folder_list` WHERE group_id = '"+arrParams.folder_id+"' ";

					HelperRestriction.deleteQuery(query, function (delete_result)
					{
						return_result(delete_result);
					});
				}
					
				//delete all details
				else
				{
					if (typeof data !== undefined && data !== null && parseInt(data.length) > 0)
					{
						data = data.concat({ group_id : arrParams.folder_id.toString()});
					}
					else
					{
						data= [{ group_id : arrParams.folder_id.toString()}];
					}

					var counter = data.length;

					if(parseInt(counter) !== 0)
					{
						promiseForeach.each(data,[function (folderid) {

							return new Promise(function (resolve, reject) {

								//delete parent folder from folder list
								var query = "DELETE FROM `jaze_jazecom_jazeemail_folder_list` WHERE group_id = '"+folderid.group_id+"' ";

								HelperRestriction.deleteQuery(query, function (delete_folder) {							

									var query = "SELECT meta_value FROM `jaze_jazecom_jazeemail_list` WHERE meta_key = 'mail_group_id' AND group_id IN ";

									query += " (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='folder_id' AND meta_value = '"+folderid.group_id+"' )";

									query += " AND group_id IN (SELECT group_id FROM `jaze_jazecom_jazeemail_list` WHERE meta_key ='account_id' AND meta_value = '"+arrParams.account_id+"' )";

									DB.query(query,null, function(mail_ids, error){
										
										counter -= 1;															
																		
										if (typeof mail_ids !== undefined && mail_ids !== null && parseInt(mail_ids.length) > 0)
										{
											Object.keys(mail_ids).forEach(function(k)
											{
												var arr = {
													account_id	:	arrParams.account_id,
													folder_id	:	folderid.group_id,
													mail_group_id	:	mail_ids[k]['meta_value']
												}

												methods.__deleteMailDetails(arr, 0, function(delete_mail){
													if (counter === 0){ 
														resolve(delete_mail);
													}
												});
											});					
										}
										else
										{ 
											if (counter === 0){ 
												resolve(delete_folder);
											}
										}		
									});
								});
							})
						}],
						function (result, current) {
							if (counter === 0){ 
								return_result(result[0]);
							}
						});	
					}
					else
					{ return_result(0); }										
				}													
			});				
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{console.log("deleteFolderDetails",err); return_result(0);}
}

/*
* move folder
*/
methods.moveFolder = function(arrParams,return_result)
{
	try
	{		
		if(arrParams !== null && parseInt(arrParams.account_id) !== 0 && parseInt(arrParams.folder_id) !== 0 && parseInt(arrParams.new_folder_id) !== 0)
		{
					
			var update_folder_details = {parent_id : arrParams.new_folder_id};
				
			HelperRestriction.updateQuery('jaze_jazecom_jazeemail_folder_list',arrParams.folder_id, update_folder_details, function (update_result) {
				
				if(parseInt(update_result) !== 0)
				{
					methods.__getFolderList(arrParams.account_id,0, function(folder_list)
					{
						return_result(folder_list);
					});
				}
				else
				{ return_result(null); }
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{console.log("moveFolder",err); return_result(null);}
}
//----------------------------------------------------------------------- FOLDER MODULE END --------------------------------------------------------------//

//------------------------------------------------------------------- General Module Start ---------------------------------------------------------------------------//

/*
* get folder name
*/
function __getFolderName(type) {

	var name = '';

	if(parseInt(type) !== 0 && type !== null && type !== "")
	{
		if(parseInt(type) == 1)
		{
			name = 'inbox';
		}
		else if(parseInt(type) == 2)
		{
			name = 'drafts';
		}
		else if(parseInt(type) == 3)
		{
			name = 'sent';
		}	
		else if(parseInt(type) == 4)
		{
			name = 'trash';
		}	
	}

	return name;
}

/*
* get account id array
*/
function __getAccountidArray(account_id_arr, account_details, folder_id) {

	if(account_details !== '' && account_details !== null && account_details !== undefined && parseInt(account_details) != 0){

		if (account_details.indexOf(',') > -1) 
		{
			account_details = account_details.split(',');
		}
		else
		{
			account_details = account_details.split();
		}

		account_details.forEach(function(row){

			if(folder_id !== null && folder_id !== '' && parseInt(folder_id) !== 0)
			{
				account_id_arr.push(row+'|'+folder_id);
			}
			else
			{
				account_id_arr.push(row);
			}
		});

		// for(var i = 0; i < account_details.length; i++){

		// 	if(folder_id !== null && folder_id !== '' && parseInt(folder_id) !== 0)
		// 	{
		// 		account_id_arr.push(account_details[i]+'|'+folder_id);
		// 	}
		// 	else
		// 	{
		// 		account_id_arr.push(account_details[i]);
		// 	}
			
		// }  
	}	

	return account_id_arr;
}

/*
* get file type
*/
function getFileTypes(ext){

	var type;

	var arrImages = ['.jpeg','.jpg','.gif','.png','.tif'];
	var arrVideos = ['.mp4','.avi','.mov','.flv','.wmv'];
	var arrAudio = ['.pcm','.wav','.aiff','.mp3','.aac','.wma','.flac','.alac'];
	var arrDocs = ['.odt','.xls','.xlsx','.ods','.ppt','.pptx','.txt','.doc'];
	var arrPdf = ['.pdf'];

	if(arrImages.includes(ext.toLowerCase())){
		type = '2';
	}else if(arrAudio.includes(ext.toLowerCase())){
		type = '3';
	}else if(arrVideos.includes(ext.toLowerCase())){
		type = '4';
	}else if(arrDocs.includes(ext.toLowerCase())){
		type = '5';
	}else if(arrPdf.includes(ext.toLowerCase())){
		type = '6';
	}else{
		type = '7';
	}

	return type;
};

function __convert_array(array_val) {

	var list = [];

	if(typeof array_val !== 'undefined' && array_val !== null && array_val !== "")
	{
		if(array_val.toString().indexOf(',') != -1 ){
			list = array_val.split(',');
			list = list.map(v => v.toLowerCase());
		}
		else {
			list.push(array_val.toLowerCase());
		}
	}	
	return list;
}

//------------------------------------------------------------------- General Module End ---------------------------------------------------------------------------//

module.exports = methods;
