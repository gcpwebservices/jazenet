const methods = {};
const DB = require('./../../helpers/db_base.js');
const HelperRestriction = require('../../helpers/HelperRestriction.js');
const HelperGeneral = require('../../helpers/HelperGeneral.js');
const HelperNotification = require('./HelperNotification.js');
const HelperJazecom = require('./HelperJazecom.js');
const promiseForeach = require('promise-foreach');
const moment = require("moment");

//------------------------------------------------------ Get Child Accounts Module Start  ------------------------------------------------------//

methods.getDeviceAccountList = function(arrParams,return_result){

	if(arrParams !== null)
	{
		var query = "SELECT * FROM jaze_app_device_details WHERE  app_type = '2' AND api_token = '"+arrParams.api_token+"' AND (id ='"+arrParams.app_id+"' OR parent_id ='"+arrParams.app_id+"') AND  status = '1' ";

		DB.query(query,null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				var counter = data.length;
						
				var array_rows = [];

				var account_id_list = "";
				
				if(parseInt(counter) !== 0)
				{
					promiseForeach.each(data,[function (account_list) {
				
						return new Promise(function (resolve, reject) {	
									 
							methods.__getChildAccountList(arrParams.account_id,account_list.account_id, function(accRres){  

								if(accRres !== null)
								{ 
									if(array_rows.length !== 0)
									{
										Object.assign({},array_rows[0]).account_list.push(accRres);
								
										array_rows[0].account_id_list =  array_rows[0].account_id_list +','+  accRres.account_id;
									}
									else
									{ 
										var obgRet = {
											account_list: [accRres],
											account_id_list : accRres.account_id
										};   

										array_rows.push(obgRet);
									}
								}

								counter -= 1;

								resolve(array_rows);
							});
						})
					}],
					function (result, current) {
						
						if (counter === 0)
						{ return_result(result[0]); }
					});	
				}
				else
				{ return_result(null); }
			}
			else
			{ return_result(null); }
		});
	}
	else
	{ return_result(null); }
};

methods.__getChildAccountList = function(master_account_id,account_id,return_result)
{
	try
	{
		if(parseInt(master_account_id) !== 0 && parseInt(account_id) !== 0)
		{			
			HelperGeneral.getAccountTypeDetails(account_id, function(account_type){
				
				if(parseInt(account_type) !== 0)
				{ 
					var arrParams = {account_id:account_id,account_type:account_type};

					HelperJazecom.___getJazecomAccountStatus(master_account_id,arrParams, function(statusRes){	

						if(statusRes !== null)
						{
							if(parseInt(statusRes.jaze_diary) === 1)
							{ 
								var objAccRres = {
										account_id	: arrParams.account_id,
										account_type: arrParams.account_type
									};
								
								return_result(objAccRres); 
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }
					});
				}
				else
				{ return_result(null); }
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("__getChildAccountList",err); return_result(null); }	
};

//------------------------------------------------------ Get Child Accounts Module End  ------------------------------------------------------//


//------------------------------------------------------ Own Appointment Module Start  ------------------------------------------------------//

methods.getOwnAppointments = function(arrLinkAccounts,arrParams,return_result){
	try
	{
		var array_rows = [];

		if(arrLinkAccounts !== "" && arrParams !== null)
		{
			var query = " SELECT * from jaze_dairy_myappointments where  group_id IN (SELECT group_id FROM  jaze_dairy_myappointments  WHERE meta_key = 'account_id' AND meta_value IN ("+arrLinkAccounts+" )  ";
				query += " AND group_id IN (SELECT group_id FROM  jaze_dairy_myappointments  WHERE meta_key = 'appointment_date' AND (DATE(meta_value) BETWEEN '"+arrParams.date_from+"' AND '"+arrParams.date_to+"')  ";
				query += " AND group_id IN (SELECT group_id FROM  jaze_dairy_myappointments  WHERE meta_key = 'status' AND meta_value = '1' ))) ";

				DB.query(query, null, function(data, error){
											
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(retrnResult){

							if(retrnResult !== null)
							{
								var counter = retrnResult.length;

								if(parseInt(counter) !== 0)
								{
									promiseForeach.each(retrnResult,[function (data) {
			
										return new Promise(function (resolve, reject) {
			
											var arrAccountParams = { account_id:data.account_id };
			
											HelperGeneral.getAccountDetails(arrAccountParams, function(accRres){	
												
												if(accRres !== null)
												{
													var newObj = { 
															id					 : data.group_id.toString(),
															owner_account_id	 : accRres.account_id.toString(),
															owner_account_type   : accRres.account_type.toString(),
															company_id			 : accRres.account_id.toString(),
															company_account_type : accRres.account_type.toString(),
															name				 : accRres.name,
															logo				 : accRres.logo,
															activity		 	 : data.activity,
															subject			 	 : data.subject,
															appointment_date 	 : data.appointment_date,
															start_time	     	 : data.start_time,
															end_time		 	 : data.end_time,
															reminder_mins	 	 : data.reminder_mins,
															repetition_type  	 : data.repetition_type,
															venue			 	 : data.venues,
															remarks			 	 : data.remarks,
															no_people			 : '',
															flag				 : '3',
															filter_date			 : new Date(data.appointment_date + ' ' + data.start_time)
														};
														
														array_rows.push(newObj);

														counter -= 1;
			
														resolve(array_rows);
												}
												else
												{counter -= 1; resolve(array_rows);}
											});
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ return_result(result[0]);}
									});
								}
								else
								{ return_result(array_rows); }
							}
							else
							{ return_result(array_rows); }
						});
					}
					else
					{ return_result(array_rows); }
				});
		}
		else
		{ return_result(array_rows); }
	}
	catch(err)
	{ console.log("getOwnAppointments",err); return_result([]); }	
};

methods.createUpdateOwnAppointmentDetails = function(arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{
			if(parseInt(arrParams.flag) === 1)
			{
				//create
				HelperRestriction.getNewGroupId('jaze_dairy_myappointments', function (group_id) {

					if(parseInt(group_id) !== 0){

						var	arrInsert = {
							account_id		 : arrParams.account_id,
							activity		 : arrParams.activity,
							subject			 : arrParams.subject,
							appointment_date : arrParams.date,
							start_time	     : arrParams.start_time,
							end_time		 : arrParams.end_time,
							reminder_mins	 : arrParams.reminder_mins,
							repetition_type  : arrParams.repetition_type,
							venues			 : arrParams.venue,
							remarks			 : arrParams.remarks,
							create_date   	 : moment().utc().format('YYYY-MM-DD HH:mm:ss'),
							update_date   	 : moment().utc().format('YYYY-MM-DD HH:mm:ss'),
							status 		   	 : '1'
						};

						HelperRestriction.insertTBQuery('jaze_dairy_myappointments',group_id,arrInsert, function (queGrpId) {

							return_result(queGrpId);
						});
					}
				});
			}
			else if(parseInt(arrParams.flag) === 2 && parseInt(arrParams.group_id) !== 0)
			{
				//update
				var	arrUpdate = {
					activity		 : arrParams.activity,
					subject			 : arrParams.subject,
					appointment_date : arrParams.date,
					start_time	     : arrParams.start_time,
					end_time		 : arrParams.end_time,
					reminder_mins	 : arrParams.reminder_mins,
					repetition_type  : arrParams.repetition_type,
					venues			 : arrParams.venue,
					remarks			 : arrParams.remarks,
					update_date    	 : moment().utc().format('YYYY-MM-DD HH:mm:ss')
				};

				HelperRestriction.updateQuery('jaze_dairy_myappointments',arrParams.group_id,arrUpdate, function (queGrpId) {

					return_result(queGrpId);
				});
			}
			else if(parseInt(arrParams.flag) === 0 && parseInt(arrParams.group_id) !== 0)
			{
				//delete
				var query = "delete from jaze_dairy_myappointments where group_id = '"+arrParams.group_id+"' ";

				DB.query(query,null, function(data, error){
					return_result(1);
				});
			}
			else
			{ return_result(0); }
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{ console.log("createUpdateOwnAppointmentDetails",err); return_result(null); }
};

//------------------------------------------------------ Own Appointment Module End  ------------------------------------------------------//

//------------------------------------------------------ Request Appointment Module Start  ------------------------------------------------------//

methods.getRequestAppointments = function(arrLinkAccounts,arrParams,return_result){

	try
	{
		var array_rows = [];

		if(arrLinkAccounts !== "" && arrParams !== null)
		{
			var query = " SELECT * from jaze_dairy_appointment_info where  group_id IN (SELECT group_id FROM  jaze_dairy_appointment_info  WHERE (meta_key = 'account_id' OR meta_key = 'company_account_id') AND meta_value IN ("+arrLinkAccounts+" )  ";
				query += " AND group_id IN (SELECT group_id FROM  jaze_dairy_appointment_info  WHERE meta_key = 'appointment_date' AND (DATE(meta_value) BETWEEN '"+arrParams.date_from+"' AND '"+arrParams.date_to+"')   ";
				query += " AND group_id IN (SELECT group_id FROM  jaze_dairy_appointment_info  WHERE meta_key = 'appointment_status' AND meta_value = '1' ))) ";

				DB.query(query, null, function(data, error){
											
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(retrnResult){

							if(retrnResult !== null)
							{
								var counter = retrnResult.length;

								if(parseInt(counter) !== 0)
								{
									promiseForeach.each(retrnResult,[function (data) {
			
										return new Promise(function (resolve, reject) {
			
											var arrAccountParams = {};
											var owner_account_id = 0;

											if(arrLinkAccounts.toString().indexOf(data.account_id) !== -1)
											{ arrAccountParams = { account_id:data.company_account_id }; owner_account_id = data.account_id; }
											else
											{ arrAccountParams = { account_id:data.account_id }; owner_account_id = data.company_account_id; }

											HelperGeneral.getAccountDetails(arrAccountParams, function(accRres){	
												
												if(accRres !== null)
												{
													HelperGeneral.getAccountTypeDetails(data.account_id, function(owner_account_type){

														var no_pepole = "";

														if(typeof data.no_people !== 'undefined' && data.no_people !== null && data.no_people !== "")
														{ no_pepole = data.no_people; }

														var newObj = { 
																id						: data.group_id.toString(),
																owner_account_id		: owner_account_id.toString(),
																owner_account_type 		: owner_account_type.toString(),
																company_id				: accRres.account_id.toString(),
																company_account_type	: accRres.account_type.toString(),
																name					: accRres.name,
																logo					: accRres.logo,
																activity		 		: data.activity,
																subject			 		: data.subject,
																venue					: data.venues,
																no_people				: no_pepole.toString(),
																remarks					: data.remarks,
																repetition_type			: data.repetition_type.toString(),
																reminder_mins			: data.reminder_mins,
																appointment_date		: data.appointment_date, 
																start_time				: data.start_time,
																end_time				: data.end_time,
																flag					: data.appointment_type.toString(),
																filter_date				: new Date(data.appointment_date + ' ' + data.start_time)
															};
															
															array_rows.push(newObj);

															counter -= 1;
				
															resolve(array_rows);
													});
												}
												else
												{counter -= 1; resolve(array_rows);}
											});
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ return_result(result[0]);}
									});
								}
								else
								{ return_result(array_rows); }
							}
							else
							{ return_result(array_rows); }
						});
					}
					else
					{ return_result(array_rows); }
				});
		}
		else
		{ return_result(array_rows); }
	}
	catch(err)
	{ console.log("getRequestAppointments",err); return_result([]); }	
};

methods.createUpdateRequestAppointmentDetails = function(arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{
			if(parseInt(arrParams.flag) === 1)
			{
				//create jazecom contact
				methods.__saveJazecomContactList(arrParams.account_id,arrParams.company_account_id, function(returnJazecom){

					methods.__saveJazecomContactList(arrParams.company_account_id,arrParams.account_id, function(returnJazecom){

						//create
						HelperRestriction.getNewGroupId('jaze_dairy_appointment_info', function (group_id) {

							if(parseInt(group_id) !== 0){

								var	arrInsert = {
									account_id		 : arrParams.account_id,
									company_account_id:arrParams.company_account_id,
									activity		 : arrParams.activity,
									subject			 : arrParams.subject,
									no_people		 : arrParams.no_people,
									appointment_date : arrParams.date,
									start_time	     : arrParams.start_time,
									end_time		 : arrParams.end_time,
									reminder_mins	 : arrParams.reminder_mins,
									repetition_type  : arrParams.repetition_type,
									venues			 : arrParams.venue,
									remarks			 : arrParams.remarks,
									appointment_type : arrParams.appointment_type,
									appointment_status :'0',
									create_date   	 : moment().utc().format('YYYY-MM-DD HH:mm:ss'),
									update_date   	 : moment().utc().format('YYYY-MM-DD HH:mm:ss'),
									status 		   	 : '1'
								};

								HelperRestriction.insertTBQuery('jaze_dairy_appointment_info',group_id,arrInsert, function (queGrpId) {

									//send push notification
									var notiParams = {	
											group_id			: group_id.toString(),
											account_id			: arrParams.account_id,
											account_type		: arrParams.account_type,
											request_account_id	: arrParams.company_account_id,
											request_account_type: arrParams.company_account_type,
											message				: "",
											flag				: '7'
										};

									HelperNotification.sendPushNotification(notiParams, function(retNoti){		

										return_result(group_id);
									});
								});
							}
							else
							{ return_result(0); }
						});
					});
				});
			}
			else if(parseInt(arrParams.flag) === 0 && parseInt(arrParams.group_id) !== 0)
			{
				//delete
				var query = "delete from jaze_dairy_appointment_info where group_id = '"+arrParams.group_id+"' ";

				DB.query(query,null, function(data, error){
					return_result(1);
				});
			}
			else
			{
				return_result(0);
			}
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{ console.log("createUpdateOwnAppointmentDetails",err); return_result(null); }
};

methods.acceptRejectRequestAppointment = function(arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{
			var query = " SELECT meta_value  FROM jaze_dairy_appointment_info WHERE meta_key in ('account_id') ";
				query +=  " AND group_id IN ( SELECT group_id FROM jaze_dairy_appointment_info WHERE meta_key = 'appointment_status' AND meta_value = '0' ";
				query +=  " AND group_id IN ( SELECT group_id FROM jaze_dairy_appointment_info WHERE meta_key = 'company_account_id' AND meta_value = '"+arrParams.account_id+"'))  ";  
				query +=  " AND group_id = '"+arrParams.group_id+"' ";

				DB.query(query,null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						if(parseInt(arrParams.flag) === 1)
						{
							//accept
							var	arrUpdate = {
								appointment_status 	: '1',
								update_date   	    : moment().utc().format('YYYY-MM-DD HH:mm:ss')
							};
			
							HelperRestriction.updateQuery('jaze_dairy_appointment_info',arrParams.group_id,arrUpdate, function (queGrpId) {
			
								if(parseInt(queGrpId) !== 0)
								{
									//push notification
									HelperGeneral.getAccountTypeDetails(arrParams.account_id, function(acc_account_type){	

										if (acc_account_type !== null)
										{ 
											//get reciver details
											HelperGeneral.getAccountTypeDetails(data[0].meta_value, function(reciv_account_type){	
												
												if (reciv_account_type !== null)
												{
													var notiParams = {	
														group_id			: arrParams.group_id.toString(),
														account_id			: arrParams.account_id,
														account_type		: acc_account_type,
														request_account_id	: data[0].meta_value,
														request_account_type: reciv_account_type,
														message				: "",
														flag				:'8'
													}; 
													
													//delete old notification
													methods.__deleteacceptRejectRequestAppointmentNotification(arrParams, function(retNoti){							
													
														//send notification
														HelperNotification.sendPushNotification(notiParams, function(retNoti){		
															
															return_result('1');
														});
													});
												}
												else
												{ return_result(null); }
											});
										}
										else
										{ return_result(null); }
									});
								}
								else
								{ return_result(null); }
							});
						}
						else if(parseInt(arrParams.flag) === 2)
						{
							//reject
							var	arrUpdate = {
								appointment_status 	: '2',
								update_date   	    : moment().utc().format('YYYY-MM-DD HH:mm:ss')
							};
			
							HelperRestriction.updateQuery('jaze_dairy_appointment_info',arrParams.group_id,arrUpdate, function (queGrpId) {
			
								if(parseInt(queGrpId) !== 0)
								{
									var	arrInsert = {
										reason 	: arrParams.reason
									};

									HelperRestriction.insertTBQuery('jaze_dairy_appointment_info',arrParams.group_id,arrInsert, function (queGrpId) {

										if(parseInt(queGrpId) !== 0)
										{
											//push notification
											HelperGeneral.getAccountTypeDetails(arrParams.account_id, function(acc_account_type){	

												if (acc_account_type !== null)
												{
													//get reciver details
													HelperGeneral.getAccountTypeDetails(data[0].meta_value, function(reciv_account_type){	

														if (reciv_account_type !== null)
														{
															var notiParams = {	
																group_id			: arrParams.group_id.toString(),
																account_id			: arrParams.account_id,
																account_type		: acc_account_type,
																request_account_id	: data[0].meta_value,
																request_account_type: reciv_account_type,
																message				: arrParams.reason,
																flag				:'9'
															}; 

															//delete old notification
															methods.__deleteacceptRejectRequestAppointmentNotification(arrParams, function(retNoti){					
																	
																//send notification
																HelperNotification.sendPushNotification(notiParams, function(retNoti){		
																
																	return_result('1');
																});
															});
														}
														else
														{ return_result(null); }
													});
												}
												else
												{ return_result(null); }
											});
										}
										else
										{ return_result(null); }
									});
								}
								else
								{ return_result(null); }
							});
						}
						else
						{ return_result(null); }
					}
					else
					{ return_result(null); }
				});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("createUpdateOwnAppointmentDetails",err); return_result(null); }
};


methods.__deleteacceptRejectRequestAppointmentNotification =  function(arrParams,return_result){

	try
	{
		var query =   " SELECT group_id  FROM jaze_notification_list WHERE  ";
			query +=  " group_id in (SELECT group_id  FROM jaze_notification_list WHERE  meta_key = 'notify_id' and meta_value = '"+arrParams.group_id+"' ";
			query +=  " and group_id in (SELECT group_id  FROM jaze_notification_list WHERE  meta_key = 'receiver_account_id' and meta_value = '"+arrParams.account_id+"' ";
			query +=  " and group_id in (SELECT group_id  FROM jaze_notification_list WHERE  meta_key = 'notify_type' and meta_value = '107' ";
			query +=  " and group_id in (SELECT group_id  FROM jaze_notification_list WHERE  meta_key = 'read_status' and meta_value = '1')))) ";
			query +=  " group by group_id ";
															
			DB.query(query,null, function(data, error){	

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var query =   " DELETE FROM `jaze_notification_list` WHERE `group_id`=  '"+data[0].group_id+"' ";

						DB.query(query,null, function(data, error){	

							return_result('1');

						});
				}
				else
				{ return_result('1'); }
			});
	}
	catch(err)
	{
		console.log("__deleteacceptRejectRequestAppointmentNotification",err);
		return_result(null);
	}
};

/*
* save or Reject to jazecome contact table
*/
methods.__saveJazecomContactList =  function(account_id,request_account_id,return_result){

	try
	{
		//first check user have details
		var query = " SELECT group_id,meta_value FROM jaze_jazecom_contact_list WHERE meta_key in ('diary_account_id') and  group_id  in ( SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value ='"+account_id+"'  ) ";
			
			DB.query(query,null, function(data, error){
				
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					HelperRestriction.convertMetaArray(data, function(retrnResult){

						if(retrnResult !== null)
						{
							var list = convert_array(retrnResult.meta_value);

							if(typeof list !== 'undefined' && list !== null && parseInt(list.length) > 0){

								if(list.indexOf(request_account_id) === -1)
								{
									list.push(request_account_id);

									var query = "  UPDATE jaze_jazecom_contact_list SET meta_value = '"+list.join()+"' where meta_key = 'diary_account_id' and group_id = '"+retrnResult.group_id+"'; ";

									DB.query(query,null, function(data_check, error){

										return_result("1");

									});
								}
								else
								{
									return_result("1");
								}
							}
							else
							{
								var query = "  UPDATE jaze_jazecom_contact_list SET meta_value = '"+request_account_id+"' where meta_key = 'diary_account_id' and group_id = '"+retrnResult.group_id+"'; ";

									DB.query(query,null, function(data_check, error){

										return_result("1");

									});
							}
						}
					});
				}
				else
				{
					//insert new 
					HelperRestriction.getNewGroupId('jaze_jazecom_contact_list', function (group_id) {

						if(parseInt(group_id) !== 0){
						
							var arrConvInsert = {
								account_id: account_id,
								mail_account_id: "",
								mail_blocked_account_id: "",
								chat_account_id: "",
								chat_blocked_account_id: "",
								call_account_id: "",
								call_blocked_account_id: "",
								feed_account_id: "",
								feed_blocked_account_id: "",
								diary_account_id: request_account_id,
								diary_blocked_account_id: "",
								create_date: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
								status: '1'
							};

							HelperRestriction.insertTBQuery('jaze_jazecom_contact_list',group_id, arrConvInsert, function (queResGrpId) {

								return_result(queResGrpId);
							});
						}
						else
						{ return_result(null); }
					});
				}
			});
	}
	catch(err)
	{
		console.log("__saveJazecomContactList",err);
		return_result(null);
	}
}

/*
 * save or Reject to jazecome contact table
 */
methods.checkAccountJazecomDiaryStatus =  function(arrParams,return_result){

	try
	{
		var account_id = arrParams.account_id;

		var request_account_id = arrParams.company_account_id;

		methods.__checkJazeDiaryActiveStatus(account_id, function (ownerStatus) {

			if(parseInt(ownerStatus) === 1)
			{
				methods.__checkJazeDiaryActiveStatus(request_account_id, function (recivStatus) {

					if(parseInt(recivStatus) === 1)
					{
						methods.__checkJazeDiaryBlockStatus(account_id,request_account_id, function (ownerBlockStatus) {

							if(parseInt(ownerBlockStatus) === 1)
							{
								methods.__checkJazeDiaryBlockStatus(request_account_id,account_id, function (recivBlockStatus) {

									if(parseInt(recivBlockStatus) === 1)
									{ return_result("1"); }
									else
									{ return_result("0"); }
								});
							}
							else
							{ return_result("0"); }
						});
					}
					else
					{ return_result("0"); }
				});
			}
			else
			{ return_result("0"); }
		});
	}
	catch(err)
	{
		console.log("checkAccountJazecomDiaryStatus",err);
		return_result(null);
	}
}

methods.__checkJazeDiaryActiveStatus =  function(account_id,return_result){

	try
	{
		var query ="  SELECT meta_value FROM jaze_user_jazecome_status WHERE meta_key in ('jaze_diary') and  group_id  in ( SELECT group_id FROM jaze_user_jazecome_status WHERE meta_key = 'account_id' AND meta_value ='"+account_id+"'  ) ";

		DB.query(query,null, function(data, error){
								
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				return_result(data[0].meta_value);
			}
			else
			{ return_result("1"); }
		});
	}
	catch(err)
	{
		console.log("__checkJazeDiaryActiveStatus",err);
		return_result("0");
	}
}

methods.__checkJazeDiaryBlockStatus =  function(account_id,request_account_id,return_result){

	try
	{
		var query =" SELECT meta_value FROM jaze_jazecom_contact_list WHERE meta_key != '' and meta_key in ('diary_blocked_account_id') and  group_id  in ( SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value ='"+account_id+"'  ) ";

		DB.query(query,null, function(data, error){
								
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				if(data[0].meta_value !== ""){
				
					var list = convert_array(data[0].meta_value);

					if(list.indexOf(request_account_id) !== -1)
					{ 
						return_result("0");
					}
					else
					{ return_result("1"); }
				}
				else
				{ return_result("1"); }
				
			}
			else
			{ return_result("1"); }
		});
	}
	catch(err)
	{
		console.log("__checkJazeDiaryBlockStatus",err);
		return_result("0");
	}
}

function convert_array(array_val) {

	
	if(typeof array_val !== 'undefined' && array_val !== null && array_val !== "")
	{
		var list = [];

		if(array_val.toString().indexOf(',') != -1 ){
			list = array_val.split(',');
		}
		else {
			list.push(array_val);
		}

		return list;
	}
	else
	{ return ""; }
}

//------------------------------------------------------ Request Appointment Module End  ------------------------------------------------------//

//------------------------------------------------------ Task Module Start  ------------------------------------------------------//

methods.getTaskList = function(arrLinkAccounts,arrParams,return_result){

	try
	{
		var array_rows = [];

		if(arrLinkAccounts !== "" && arrParams !== null)
		{
			var query = " SELECT * from jaze_dairy_task_details where  group_id IN (SELECT group_id FROM  jaze_dairy_task_details  WHERE meta_key = 'account_id' AND meta_value IN ("+arrLinkAccounts+" )  ";
				query += " AND group_id IN (SELECT group_id FROM  jaze_dairy_task_details  WHERE meta_key = 'task_date' AND (DATE(meta_value) BETWEEN '"+arrParams.date_from+"' AND '"+arrParams.date_to+"') )) ";

				DB.query(query, null, function(data, error){
											
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(retrnResult){

							if(retrnResult !== null)
							{
								var counter = retrnResult.length;

								if(parseInt(counter) !== 0)
								{
									promiseForeach.each(retrnResult,[function (data) {
			
										return new Promise(function (resolve, reject) {

											HelperGeneral.getAccountTypeDetails(data.account_id, function(owner_account_type){
		
												var newObj = { 
													id					: data.group_id.toString(),
													owner_account_id	: data.account_id.toString(),
													owner_account_type	: owner_account_type.toString(),
													task_date 	  		: data.task_date,
													task_time 	  		: data.task_time,
													remarks		  		: data.remarks,
													reminder_mins 		: data.reminder_mins,
													appointment_date	: data.task_date, 
													status				: data.status,
													flag				: '4'
												};
															
												array_rows.push(newObj);

												counter -= 1;
				
												resolve(array_rows);
											});
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ return_result(result[0]);}
									});
								}
								else
								{ return_result(array_rows); }
							}
							else
							{ return_result(array_rows); }
						});
					}
					else
					{ return_result(array_rows); }
				});
		}
		else
		{ return_result(array_rows); }
	}
	catch(err)
	{ console.log("getTaskList",err); return_result([]); }	
};

methods.createUpdateTaskDetails = function(arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{
			if(parseInt(arrParams.flag) === 1)
			{
				//create
				HelperRestriction.getNewGroupId('jaze_dairy_task_details', function (group_id) {

					if(parseInt(group_id) !== 0){

						var	arrInsert = {
							account_id	  : arrParams.account_id,
							task_date 	  : arrParams.task_date,
							task_time 	  : arrParams.task_time,
							remarks		  : arrParams.remarks,
							reminder_mins : arrParams.reminder_mins,
							create_date   : moment().utc().format('YYYY-MM-DD HH:mm:ss'),
							update_date   : moment().utc().format('YYYY-MM-DD HH:mm:ss'),
							status 		  : '0'
						};

						HelperRestriction.insertTBQuery('jaze_dairy_task_details',group_id,arrInsert, function (queGrpId) {

							return_result(queGrpId);
						});
					}
					else
					{ return_result(0); }
				});
			}
			else if(parseInt(arrParams.flag) === 2 && parseInt(arrParams.group_id) !== 0)
			{
				//update
				var	arrUpdate = {
					task_date 	  : arrParams.task_date,
					task_time 	  : arrParams.task_time,
					remarks		  : arrParams.remarks,
					reminder_mins : arrParams.reminder_mins,
					update_date   : moment().utc().format('YYYY-MM-DD HH:mm:ss')
				};

				HelperRestriction.updateQuery('jaze_dairy_task_details',arrParams.group_id,arrUpdate, function (queGrpId) {

					return_result(queGrpId);
				});
			}
			else if(parseInt(arrParams.flag) === 3 && parseInt(arrParams.group_id) !== 0) 
			{
				//change status
				var	arrUpdate = {
					update_date   : moment().utc().format('YYYY-MM-DD HH:mm:ss'),
					status 		  : '1'
				};

				HelperRestriction.updateQuery('jaze_dairy_task_details',arrParams.group_id,arrUpdate, function (queGrpId) {

					return_result(queGrpId);
				});
			}
			else if(parseInt(arrParams.flag) === 4 && parseInt(arrParams.group_id) !== 0) 
			{
				//change status
				var	arrUpdate = {
					update_date   : moment().utc().format('YYYY-MM-DD HH:mm:ss'),
					status 		  : '0'
				};

				HelperRestriction.updateQuery('jaze_dairy_task_details',arrParams.group_id,arrUpdate, function (queGrpId) {

					return_result(queGrpId);
				});
			}
			else if(parseInt(arrParams.flag) === 0 && parseInt(arrParams.group_id) !== 0)
			{
				//delete
				var query = "delete from jaze_dairy_task_details where group_id = '"+arrParams.group_id+"' ";

				DB.query(query,null, function(data, error){
					return_result(1);
				});
			}
			else
			{ return_result(0); }
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{ console.log("createUpdateTaskDetails",err); return_result(null); }
};

//------------------------------------------------------ Task Module End  ------------------------------------------------------//

//------------------------------------------------------ Incoming/Outgoing Module Start  ------------------------------------------------------//

methods.getIncomingCount = function(arrLinkAccounts,arrParams,return_result){

	try
	{
		if(arrLinkAccounts !== "" && arrParams !== null)
		{
			var query = " SELECT group_id from jaze_dairy_appointment_info where  group_id IN (SELECT group_id FROM  jaze_dairy_appointment_info  WHERE meta_key = 'company_account_id' AND meta_value IN ("+arrLinkAccounts+" )  ";
				query += " AND group_id IN (SELECT group_id FROM  jaze_dairy_appointment_info  WHERE meta_key = 'appointment_status' AND meta_value = '0'  ";
				query += " AND group_id IN (SELECT group_id FROM  jaze_dairy_appointment_info  WHERE meta_key = 'appointment_date' AND YEAR(meta_value) = '"+arrParams.year+"' ))) GROUP BY group_id ";
		
				DB.query(query, null, function(data, error){
											
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						return_result(data.length);
					}
					else
					{ return_result(0); }
				});
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{ console.log("getIncomingCount",err); return_result(0); }		
};

methods.getIncOutList = function(arrLinkAccounts,type,return_result){

	try
	{
		var array_rows = [];

		// type : 1 = inbox , 2 = outbox

		if(arrLinkAccounts !== "" && type !== "")
		{
			var meta_key = 'account_id';

			if(parseInt(type) === 1){
				meta_key = 'company_account_id';
			}

			var query = " SELECT * from jaze_dairy_appointment_info where  group_id IN (SELECT group_id FROM  jaze_dairy_appointment_info  WHERE meta_key = '"+meta_key+"' AND meta_value IN ("+arrLinkAccounts+" )  ";
				query += " AND group_id IN (SELECT group_id FROM  jaze_dairy_appointment_info  WHERE meta_key = 'appointment_status' AND meta_value = '0' )) ";
				
				DB.query(query, null, function(data, error){
											
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(retrnResult){
							
							if(retrnResult !== null)
							{
								var counter = retrnResult.length;
								
								if(parseInt(counter) !== 0)
								{
									promiseForeach.each(retrnResult,[function (data) {
			
										return new Promise(function (resolve, reject) {
			
											var arrAccountParams = {};
											var owner_account_id = 0;

											if(parseInt(type) === 1){
												//inbox
												arrAccountParams = { account_id:data.account_id }; 
												owner_account_id = data.company_account_id;
											}
											else if(parseInt(type) === 2){
												//outbox
												arrAccountParams = { account_id:data.company_account_id }; 
												owner_account_id = data.account_id;
											}

												HelperGeneral.getAccountDetails(arrAccountParams, function(accRres){	
													
													if(accRres !== null)
													{
														HelperGeneral.getAccountCategoryName(owner_account_id, function(ownerRres){

															var remarks = "";

															if(typeof data.remarks !== 'undefined' && data.remarks !== "" &&  data.remarks !== null)
															{ remarks = data.remarks; }

															var newObj = { 
																	id					 : data.group_id.toString(),
																	owner_account_id	 : owner_account_id.toString(),
																	owner_account_type   : ownerRres.account_type.toString(),
																	owner_category_name  : ownerRres.category_name,
																	company_id			 : accRres.account_id.toString(),
																	company_account_type : accRres.account_type.toString(),
																	name				 : accRres.name,
																	logo				 : accRres.logo,
																	activity		 		: data.activity,
																	subject			 		: data.subject,
																	venue					: data.venues,
																	no_people				: data.no_people,
																	remarks					: data.remarks,
																	repetition_type			: data.repetition_type.toString(),
																	reminder_mins			: data.reminder_mins,
																	appointment_date		: data.appointment_date, 
																	start_time				: data.start_time,
																	end_time				: data.end_time,
																	flag					: data.appointment_type.toString(),
																	order_by			 	: data.appointment_date + " "+data.start_time
																};

																array_rows.push(newObj);

																counter -= 1;
					
																resolve(array_rows);
														});
													}
													else
													{counter -= 1; resolve(array_rows);}
												});
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ return_result(result[0]);}
									});
								}
								else
								{ return_result(array_rows); }
							}
							else
							{ return_result(array_rows); }
						});
					}
					else
					{ return_result(array_rows); }
				});
		}
		else
		{ return_result(array_rows); }
	}
	catch(err)
	{ console.log("getIncOutList",err); return_result([]); }	
};

/*
 * Diary Notification Read Status
 */
methods.setDiaryNotificationReadStatus = function(arrLinkAccounts,return_result){

	try{
		if(arrLinkAccounts !== "")
		{
			var query = " SELECT GROUP_CONCAT(group_id) as group_id  FROM jaze_notification_list WHERE ";
				query +=  " group_id IN ( SELECT group_id FROM jaze_notification_list WHERE meta_key = 'notify_category' AND meta_value = 'diary' ";
				query +=  " AND group_id IN ( SELECT group_id FROM jaze_notification_list WHERE meta_key = 'read_status' AND meta_value = '0'  ";
				query +=  " AND group_id IN ( SELECT group_id FROM jaze_notification_list WHERE meta_key = 'receiver_account_id' AND meta_value IN ("+arrLinkAccounts+" )   ";
				query +=  " AND group_id IN ( SELECT group_id FROM jaze_notification_list WHERE meta_key = 'notify_type' AND meta_value = '107' )))) group by group_id ";   
				
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						query = " UPDATE jaze_notification_list SET meta_value = '1' WHERE group_id IN ("+data[0].group_id+") AND meta_key ='read_status'; ";

						DB.query(query,null, function(data, error){
							return_result(1);
						});
					}
					else
					{ return_result(null); }
				});
		}
		else
		{ return_result(null); }
	}
	catch(error)
	{
		console.log(error);
		return_result(null);
	}	
};

//------------------------------------------------------ Incoming/Outgoing Module End  ------------------------------------------------------//

//------------------------------------------------------- General Functions Start -----------------------------------------------------------//


methods.getDiaryYearDatekList = function(arrLinkAccounts,arrParams,return_result){

	try
	{
		var array_rows = [];

		if(arrLinkAccounts !== null && arrParams !== null)
		{
			var query  = "	SELECT t1.* FROM ( ";
				query += "	SELECT meta_value FROM jaze_dairy_task_details WHERE meta_key = 'task_date' ";
				query += " 	and group_id IN (SELECT group_id FROM  jaze_dairy_task_details  WHERE meta_key = 'account_id' AND meta_value  IN ("+arrLinkAccounts+" )  ";
				query += " 	AND group_id IN (SELECT group_id FROM  jaze_dairy_task_details  WHERE meta_key = 'task_date' AND YEAR(meta_value) = '"+arrParams.year+"' )) ";
				query += " 	union all ";
				query += " 	SELECT meta_value from jaze_dairy_appointment_info where meta_key = 'appointment_date' and group_id IN (SELECT group_id FROM  jaze_dairy_appointment_info  WHERE (meta_key = 'account_id' OR meta_key = 'company_account_id') AND meta_value IN ("+arrLinkAccounts+" ) ";
				query += " 	AND group_id IN (SELECT group_id FROM  jaze_dairy_appointment_info  WHERE meta_key = 'appointment_date' AND YEAR(meta_value) = '"+arrParams.year+"'  ";
				query += " 	AND group_id IN (SELECT group_id FROM  jaze_dairy_appointment_info  WHERE meta_key = 'appointment_status' AND meta_value = '1' ))) ";
				query += " 	union all ";
				query += " 	SELECT meta_value from jaze_dairy_myappointments where meta_key = 'appointment_date' and group_id IN (SELECT group_id FROM  jaze_dairy_myappointments  WHERE meta_key = 'account_id'  AND meta_value IN ("+arrLinkAccounts+" ) ";
				query += " 	AND group_id IN (SELECT group_id FROM  jaze_dairy_myappointments  WHERE meta_key = 'appointment_date' AND YEAR(meta_value) = '"+arrParams.year+"'  ";
				query += " 	AND group_id IN (SELECT group_id FROM  jaze_dairy_myappointments  WHERE meta_key = 'status' AND meta_value = '1' ))) ";
				query += " 	) AS t1 group by meta_value order by meta_value asc  ";
			
				DB.query(query, null, function(data, error){
											
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						data.forEach(function (row) {
							var newObj = {date:row.meta_value};
							array_rows.push(newObj);
						});

						return_result(array_rows);
					}
					else
					{ return_result(array_rows); }
				});
		}
		else
		{ return_result(array_rows); }
	}
	catch(err)
	{ console.log("getDiaryYearDatekList",err); return_result([]); }	
};

methods.getDiaryDetails = function(arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{
			var query = "";

			if(parseInt(arrParams.flag) === 1 || parseInt(arrParams.flag) === 2)
			{	
				query  = " SELECT * FROM jaze_dairy_appointment_info WHERE ";
				query += " group_id IN (SELECT group_id FROM  jaze_dairy_appointment_info  WHERE (meta_key = 'account_id' OR meta_key = 'company_account_id') AND meta_value = '"+arrParams.account_id+"'  ";
				query += " AND group_id IN (SELECT group_id FROM  jaze_dairy_appointment_info  WHERE meta_key = 'appointment_type' AND meta_value = '"+arrParams.flag+"' )) and group_id = '"+arrParams.group_id+"' ";
			}
			else if(parseInt(arrParams.flag) === 3)
			{
				query  = " SELECT * FROM jaze_dairy_myappointments WHERE ";
				query += " group_id IN (SELECT group_id FROM  jaze_dairy_myappointments  WHERE meta_key = 'account_id' AND meta_value = '"+arrParams.account_id+"' ) and group_id = '"+arrParams.group_id+"' ";
			}
			else if(parseInt(arrParams.flag) === 4)
			{
				query  = " SELECT * FROM jaze_dairy_task_details WHERE ";
				query += " group_id IN (SELECT group_id FROM  jaze_dairy_task_details  WHERE meta_key = 'account_id' AND meta_value = '"+arrParams.account_id+"' ) and group_id = '"+arrParams.group_id+"' ";
			}

			DB.query(query,null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					
					HelperRestriction.convertMetaArray(data, function(retrnResult){
						
						if(retrnResult !== null)
						{ 
							var arrAccountParams = {};
							var owner_account_id = 0;

							if(parseInt(arrParams.flag) === 1 || parseInt(arrParams.flag) === 2)
							{
								if(parseInt(retrnResult.account_id) === parseInt(arrParams.account_id))
								{ arrAccountParams = { account_id:retrnResult.company_account_id }; owner_account_id = retrnResult.account_id; }
								else
								{ arrAccountParams = { account_id:retrnResult.account_id }; owner_account_id = retrnResult.company_account_id; }
							}
							else if(parseInt(arrParams.flag) === 3 || parseInt(arrParams.flag) === 4)
							{ arrAccountParams = { account_id:retrnResult.account_id }; owner_account_id = retrnResult.account_id; }

							HelperGeneral.getAccountDetails(arrAccountParams, function(accRres){	
											
								if(accRres !== null)
								{
									HelperGeneral.getAccountTypeDetails(owner_account_id, function(owner_account_type){

										var newObj = {};

										if(parseInt(arrParams.flag) !== 4)
										{
											var no_pepole = "";
											var reason = "";

											if(parseInt(arrParams.flag) !== 3)
											{ no_pepole = retrnResult.no_people; }

											if(parseInt(arrParams.flag) !== 3 && parseInt(retrnResult.appointment_status) === 2)
											{ reason = retrnResult.reason; }

											newObj = { 
												id					 : retrnResult.group_id.toString(),
												owner_account_id	 : owner_account_id,
												owner_account_type   : owner_account_type.toString(),
												company_id			 : accRres.account_id,
												company_account_type : accRres.account_type,
												name				 : accRres.name,
												logo				 : accRres.logo,
												activity		 	 : retrnResult.activity,
												subject			 	 : retrnResult.subject,
												venue				 : retrnResult.venues,
												no_pepole			 : no_pepole.toString(),
												remarks				 : retrnResult.remarks,
												reason				 : reason.toString(),
												repetition_type		 : retrnResult.repetition_type.toString(),
												reminder_mins		 : retrnResult.reminder_mins,
												appointment_date	 : retrnResult.appointment_date, 
												start_time			 : retrnResult.start_time,
												end_time			 : retrnResult.end_time,
												flag				 : arrParams.flag.toString()
											};
										}
										else if(parseInt(arrParams.flag) === 4)
										{
											newObj = { 
												id					: retrnResult.group_id.toString(),
												owner_account_id	: owner_account_id,
												owner_account_type	: owner_account_type.toString(),
												remarks				: retrnResult.remarks,
												reminder_mins		: retrnResult.reminder_mins,
												task_date			: retrnResult.task_date, 
												task_time 	  		: data.task_time,
												status				: retrnResult.status,
												flag				: arrParams.flag.toString()
											};
										}
										else
										{ newObj = null; }

										return_result(newObj);
									});
								}
								else
								{return_result(null); }
							});
						}
						else
						{return_result(null); }
					});
				}
				else
				{return_result(null); }
			});
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{ console.log("getDiaryDetails",err); return_result(null); }
};

//------------------------------------------------------- General Functions End -----------------------------------------------------------//


module.exports = methods;