const methods = {};
const DB = require('../../helpers/db_base.js');

const dateFormat = require('dateformat');
const moment = require("moment");
const promiseForeach = require('promise-foreach');
const HelperGeneral = require('../../helpers/HelperGeneral.js');
const HelperRestriction = require('../../helpers/HelperRestriction.js');
//const HelperWebNotification = require('../../jazenet/helpers/HelperWebNotification.js');
var fcm = require('fcm-notification');
const array_unique = require('array-unique');
const serviceAccount = require('../../helpers/service_accounts/jazechat-firebase-adminsdk-xey63-949986c811.json');
const FCM = new fcm(serviceAccount);



	//checking users have notification mute
	methods.checkMuteNotification = function(arrParams,return_result){
		
		try
		{
			if(parseInt(arrParams.flag) === 4)
			{
				//chat messages
					//check user have mute notification
					var query = " SELECT ";
					query +=" (SELECT a.meta_value   FROM jaze_jazechat_conversation_setting a  WHERE a.`meta_key` = 'mute_notification_start' AND a.group_id = jaze_jazechat_conversation_setting.group_id ) as mute_notification_start,";
					query +=" (SELECT a.meta_value   FROM jaze_jazechat_conversation_setting a  WHERE a.`meta_key` = 'mute_notification_end' AND a.group_id = jaze_jazechat_conversation_setting.group_id ) as mute_duration";
					query +=" FROM jaze_jazechat_conversation_setting WHERE ";
					query +=" `group_id` IN (SELECT `group_id`   FROM jaze_jazechat_conversation_setting  WHERE `meta_key` = 'mute_notification' AND `meta_value` = '1'  ";
					query +=" AND   `group_id` IN (SELECT `group_id`   FROM jaze_jazechat_conversation_setting  WHERE `meta_key` = 'account_id' AND `meta_value` = '"+arrParams.request_account_id+"'  ";
					query +=" AND   `group_id` IN (SELECT `group_id` FROM jaze_jazechat_conversation_setting WHERE `meta_key` = 'convers_id' AND meta_value = '"+arrParams.group_id+"')))";
					query +=" group by  group_id";

					
				DB.query(query, null, function(data, error){

					if (typeof data[0] !== 'undefined' && data[0] !== null)
					{
						//check time 
						//if mute return '1' else '0'
						var mute_duration = data[0]['mute_duration'];

						const dateUTC = moment().utc().format('YYYY-MM-DD hh:mm:ss');

						if(parseInt(mute_duration) !== 0)
						{
							var start_date = new Date(data[0]['mute_notification_start']);

							var expire_date =  start_date.setTime(start_date.getTime() + mute_duration * 60000);

							var current_datetime = new Date(dateUTC).getTime();

							if(current_datetime > expire_date)
							{
								return_result('0');
							}
							else
							{
								return_result('1');
							}
						}
						else
						{ return_result('1'); }
					}
					else
					{ return_result('0'); }
				});
			}
			else
			{ return_result('0'); }
		}	
		catch(err)
		{
		console.log(err);
		return_result('0');
		}
	};

	// get users fcm tockn 
	methods.getFCMTokenList = function(reqParams,return_result){
		
		try
		{
			
			methods.checkMuteNotification(reqParams, function(mute_result){	
			
				if(parseInt(mute_result) === 0)
				{
					var main_flag = [5,6,7,8,9,12];

					var jazenet_flag = [14,15,16,19,20,21,22];

					var jazecom_flag = [1,2,3,4,10,11,17];

					//get fcm array
					var query = " select fcm_token,device_type from jaze_app_device_details where account_id = '"+reqParams.request_account_id+"' and status= '1' and fcm_token != '' ";

					if(jazenet_flag.indexOf(parseInt(reqParams.flag)) !== -1 ){
						query += " and app_type = '1' ";
					}
					else if(jazecom_flag.indexOf(parseInt(reqParams.flag)) !== -1 ){
						query += " and app_type = '2' ";
					}

					query += " order by id desc  ";
//console.log('query',query);
					DB.query(query, null, function(data, error){

						if (typeof data[0] !== 'undefined' && data[0] !== null)
						{
							var fcm_array = {};

							var counter = data.length;

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data,[function (jsonItems) {
					
									return new Promise(function (resolve, reject) {		
//console.log('jsonItems',jsonItems);									
										if(typeof fcm_array[jsonItems.device_type] !== 'undefined' && fcm_array[jsonItems.device_type] !== null)
										{
											fcm_array[jsonItems.device_type] = fcm_array[jsonItems.device_type].concat([jsonItems.fcm_token]);
										}
										else
										{
											fcm_array[jsonItems.device_type]= [jsonItems.fcm_token];
										}	
										
										counter -= 1;

										if (counter === 0)
										{ 
											resolve(fcm_array);
										}
									})
								}],
								function (result, current) { 
									if (counter === 0)
									{ return_result(result[0]); }
								});	
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }
					});
				}
				else
				{ return_result(null); }
			});	
		}	
		catch(err)
		{
			console.log(err);
			return_result(null);
		}

	};

	// get notification template
	methods.getNotificationMobTemplate = function(reqParams,return_result){

		// device_type = 1: ios 2 : android
		try
		{
			var title = body = message = type = message_type = group_id = sender_account_id = sender_name = sender_logo = receiver_account_id = fcm_token = '';

			message_type = '1';

			var master_fild_name = 'group_id';
			//check flag
			// flag = 1: new contact request, 2:chat request accepted, 3:chat request rejected, 4: send new message
			if(parseInt(reqParams.notifi_flag) === 1)
			{
				title = 'new contact request received';
				body = 'new contact request received';
				message = 'new contact request received';
				type = 'new_contact_request';
			}
			else if(parseInt(reqParams.notifi_flag) === 2)
			{
				title = 'your request has been approved';
				body = 'your request has been approved';
				message = 'your request has been approved';
				type = 'chat_request_accepted';
			}
			else if(parseInt(reqParams.notifi_flag) === 3)
			{
				title = 'your request has been rejected';
				body = 'your request has been rejected';
				message = 'your request has been rejected';
				type = 'new_contact_rejected';
			}
			else if(parseInt(reqParams.notifi_flag) === 4)
			{
				title = 'message received';
				body = 'message : '+reqParams.msg;
				message = reqParams.msg;
				message_type = reqParams.msg_type;
				type = 'new_text_chat';
			}
			else if(parseInt(reqParams.notifi_flag) === 5)
			{
				title = 'feed received';
				body = 'feed received';
				message = 'feed received';
				type = 'feed_received';
				master_fild_name = 'feed_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 6)
			{
				title = 'feed replay received';
				body = 'feed replay received';
				message = 'feed replay received';
				type = 'feed_received';
				master_fild_name = 'feed_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 7)
			{
				title = 'request appointment received';
				body = 'request appointment received';
				message = 'request appointment received';
				type = 'request_appointment_received';
				master_fild_name = 'appointment_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 8)
			{
				title = 'appointment accepted';
				body = 'appointment accepted';
				message = 'appointment accepted';
				type = 'appointment_accepted';
				master_fild_name = 'appointment_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 9)
			{
				title = 'appointment rejected';
				body = 'appointment rejected';
				message = 'appointment rejected';
				type = 'appointment_rejected';
				master_fild_name = 'appointment_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 10)
			{
				title = 'block contact';
				body = 'block contact';
				message = 'block contact';
				type = 'block_contact';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 11)
			{
				title = 'unblock contact';
				body = 'unblock contact';
				message = 'unblock contact';
				type = 'unblock_contact';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 12)
			{
				title = 'mail received';
				body = 'mail received from '+reqParams.sender_name;
				message  = 'mail received';
				type = 'new_email_received';
				master_fild_name = 'mail_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 13)
			{
				
			}
			else if(parseInt(reqParams.notifi_flag) === 14)
			{
				title = 'team invitation received';
				body = 'team invitation received';
				message  = 'team invitation received';
				type = 'receive_team_invitation';
				master_fild_name = 'team_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 15)
			{
				title = 'team invitation approved';
				body = 'team invitation approved';
				message  = 'team invitation approved';
				type = 'accept_team_invitation';
				master_fild_name = 'team_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 16)
			{
				title = 'team invitation rejected';
				body = 'team invitation rejected';
				message  = 'team invitation rejected';
				type = 'reject_team_invitation';
				master_fild_name = 'team_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 17)
			{
				title = 'contact request has been deleted';
				body = 'contact request has been deleted';
				message  = 'contact request has been deleted';
				type = 'new_contact_remove';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 18)
			{
				title = 'team invitation removed';
				body = 'team invitation removed';
				message  = 'team invitation removed';
				type = 'removed_team_invitation';
				master_fild_name = 'team_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 19)
			{
				title 	 = 'notice has been received';
				body 	 = 'notice has been received';
				message  = 'notice has been received';
				type 	 = 'send_followers_notice';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 20)
			{
				title 	 = 'cv request has been received';
				body 	 = 'cv request has been received';
				message  = 'cv request has been received';
				type 	 = 'cv_request_received';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 21)
			{
				title 	 = 'cv request has been accepted';
				body 	 = 'cv request has been accepted';
				message  = 'cv request has been accepted';
				type 	 = 'cv_request_accepted';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 22)
			{
				title 	 = 'cv request has been rejected';
				body 	 = 'cv request has been rejected';
				message  = 'cv request has been rejected';
				type 	 = 'cv_request_rejected';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 23)
			{
				title = 'team member removed';
				body = 'team member removed';
				message  = 'team member removed';
				type = 'removed_team_member';
				master_fild_name = 'team_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 24)
			{
				title = 'added follow groups';
				body = 'added follow groups';
				message  = 'added follow groups';
				type = 'added_follow_groups';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 25)
			{
				title = 'new contact request received';
				body = 'new contact request received';
				message  = 'new contact request received';
				type = 'jazemail_new_contact_request';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 26)
			{
				title = 'new contact request approved';
				body = 'new contact request approved';
				message  = 'new contact request approved';
				type = 'jazemail_request_accepted';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 27)
			{
				title = 'your request has been rejected';
				title = 'new contact request rejected';
				body = 'new contact request rejected';
				message  = 'new contact request rejected';
				type = 'jazemail_request_rejected';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 50)
			{
				title = 'fund received';
				body = 'fund received';
				message  = 'fund received';
				type = 'jazepay_fund_received';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 51)
			{
				title = 'quotations request received';
				body = 'quotations request received';
				message  = 'quotations request received';
				type = 'jazepay_quotations_request_received';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 52)
			{
				title = 'quotations request declined';
				body = 'quotations request declined';
				message  = 'quotations request declined';
				type = 'jazepay_quotations_request_declined';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 53)
			{
				title = 'quotations received';
				body = 'quotations received';
				message  = 'quotations received';
				type = 'jazepay_quotations_received';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 54)
			{
				title = 'quotations declined';
				body = 'quotations declined';
				message  = 'quotations declined';
				type = 'jazepay_quotations_declined';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 55)
			{
				title = 'order received';
				body = 'order received';
				message  = 'order received';
				type = 'jazepay_order_received';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 56)
			{
				title = 'order declined';
				body = 'order declined';
				message  = 'order declined';
				type = 'jazepay_order_declined';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 57)
			{
				title = 'invoice received';
				body = 'invoice received';
				message  = 'invoice received';
				type = 'jazepay_invoice_received';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 58)
			{
				title = 'invoice declined';
				body = 'invoice declined';
				message  = 'invoice declined';
				type = 'jazepay_invoice_declined';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 59)
			{
				title = 'payment received';
				body = 'payment received';
				message  = 'payment received';
				type = 'jazepay_invoice_payment_received';
				master_fild_name = 'group_id';
			}

				var template = {};

				if(parseInt(reqParams.device_type) === 1){

					 template = {
							data: {   
								message: message,
								msg_type: message_type,
								type: type,
								[master_fild_name]:reqParams.group_id,
								sender_account_id:reqParams.sender_account_id,
								sender_name:reqParams.sender_name,
								sender_logo:reqParams.sender_logo,
								receiver_account_id:reqParams.receiver_account_id,
								receiver_name		:reqParams.receiver_name,
								timestamp: moment().utc().format('YYYY-MM-DD hh:mm:ss').toString(),
								sound: 'default',
								priority:'high',
								delay_while_idle: 'false',
								content_available: 'true'
							},
							notification:{
								title : title,
								body : body,
							}
						};
				}
				else if(parseInt(reqParams.device_type) === 2){

					template = {
							data: {   
								title : title,
								body : body,
								message: message,
								msg_type: message_type,
								type: type,
								[master_fild_name]:reqParams.group_id,
								sender_account_id:reqParams.sender_account_id,
								sender_name:reqParams.sender_name,
								sender_logo:reqParams.sender_logo,
								receiver_account_id:reqParams.receiver_account_id,
								receiver_name		:reqParams.receiver_name,
								timestamp: moment().utc().format('YYYY-MM-DD hh:mm:ss').toString(),
								sound: 'default',
								priority:'high',
								delay_while_idle: 'false',
								content_available: 'true'
							}
						};				
				}


				//console.log(template);
				return_result(template);
		}
		catch(err)
		{ 
		
			console.log("getNotificationMobTemplate: " + err);
			return_result(null);
		}
	}


		// get notification web template
	methods.getNotificationWebPushTemplate = function(arrParams,reqParams,return_result){

		
		try
		{
			var title = body = message = type = message_type = group_id = sender_account_id = sender_name = sender_logo = receiver_account_id = fcm_token = '';

			message_type = '1';

			var master_fild_name = 'group_id';
			//check flag
			// flag = 1: new contact request, 2:chat request accepted, 3:chat request rejected, 4: send new message
			if(parseInt(reqParams.notifi_flag) === 1)
			{
				title = 'new contact request received';
				body = 'new contact request received';
				message = 'new contact request received';
				type = 'new_contact_request';
			}
			else if(parseInt(reqParams.notifi_flag) === 2)
			{
				title = 'your request has been approved';
				body = 'your request has been approved';
				message = 'your request has been approved';
				type = 'chat_request_accepted';
			}
			else if(parseInt(reqParams.notifi_flag) === 3)
			{
				title = 'your request has been rejected';
				body = 'your request has been rejected';
				message = 'your request has been rejected';
				type = 'new_contact_rejected';
			}
			else if(parseInt(reqParams.notifi_flag) === 4)
			{
				title = 'message received';
				body = 'message : '+reqParams.msg;
				message = reqParams.msg;
				message_type = reqParams.msg_type;
				type = 'new_text_chat';
			}
			else if(parseInt(reqParams.notifi_flag) === 5)
			{
				title = 'feed received';
				body = 'feed received';
				message = 'feed received';
				type = 'feed_received';
				master_fild_name = 'feed_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 6)
			{
				title = 'feed replay received';
				body = 'feed replay received';
				message = 'feed replay received';
				type = 'feed_received';
				master_fild_name = 'feed_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 7)
			{
				title = 'request appointment received';
				body = 'request appointment received';
				message = 'request appointment received';
				type = 'request_appointment_received';
				master_fild_name = 'appointment_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 8)
			{
				title = 'appointment accepted';
				body = 'appointment accepted';
				message = 'appointment accepted';
				type = 'appointment_accepted';
				master_fild_name = 'appointment_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 9)
			{
				title = 'appointment rejected';
				body = 'appointment rejected';
				message = 'appointment rejected';
				type = 'appointment_rejected';
				master_fild_name = 'appointment_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 10)
			{
				title = 'block contact';
				body = 'block contact';
				message = 'block contact';
				type = 'block_contact';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 11)
			{
				title = 'unblock contact';
				body = 'unblock contact';
				message = 'unblock contact';
				type = 'unblock_contact';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 12)
			{
				title = 'mail received';
				body = 'mail received from '+reqParams.sender_name;
				message  = 'mail received';
				type = 'new_email_received';
				master_fild_name = 'mail_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 13)
			{
				
			}
			else if(parseInt(reqParams.notifi_flag) === 14)
			{
				title = 'team invitation received';
				body = 'team invitation received';
				message  = 'team invitation received';
				type = 'receive_team_invitation';
				master_fild_name = 'team_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 15)
			{
				title = 'team invitation approved';
				body = 'team invitation approved';
				message  = 'team invitation approved';
				type = 'accept_team_invitation';
				master_fild_name = 'team_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 16)
			{
				title = 'team invitation rejected';
				body = 'team invitation rejected';
				message  = 'team invitation rejected';
				type = 'reject_team_invitation';
				master_fild_name = 'team_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 17)
			{
				title = 'contact request has been deleted';
				body = 'contact request has been deleted';
				message  = 'contact request has been deleted';
				type = 'new_contact_remove';
			}
			else if(parseInt(reqParams.notifi_flag) === 18)
			{
				title = 'team invitation removed';
				body = 'team invitation removed';
				message  = 'team invitation removed';
				type = 'removed_team_invitation';
				master_fild_name = 'team_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 19)
			{
				title 	 = 'notice has been received';
				body 	 = 'notice has been received';
				message  = 'notice has been received';
				type 	 = 'send_followers_notice';
			}
			else if(parseInt(reqParams.notifi_flag) === 20)
			{
				title 	 = 'cv request has been received';
				body 	 = 'cv request has been received';
				message  = 'cv request has been received';
				type 	 = 'cv_request_received';
			}
			else if(parseInt(reqParams.notifi_flag) === 21)
			{
				title 	 = 'cv request has been accepted';
				body 	 = 'cv request has been accepted';
				message  = 'cv request has been accepted';
				type 	 = 'cv_request_accepted';
			}
			else if(parseInt(reqParams.notifi_flag) === 22)
			{
				title 	 = 'cv request has been rejected';
				body 	 = 'cv request has been rejected';
				message  = 'cv request has been rejected';
				type 	 = 'cv_request_rejected';
			}
			else if(parseInt(reqParams.notifi_flag) === 23)
			{
				title = 'team member removed';
				body = 'team member removed';
				message  = 'team member removed';
				type = 'removed_team_member';
				master_fild_name = 'team_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 24)
			{
				title = 'added follow groups';
				body = 'added follow groups';
				message  = 'added follow groups';
				type = 'added_follow_groups';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 25)
			{
				title = 'new contact request received';
				body = 'new contact request received';
				message  = 'new contact request received';
				type = 'jazemail_new_contact_request';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 26)
			{
				title = 'new contact request approved';
				body = 'new contact request approved';
				message  = 'new contact request approved';
				type = 'jazemail_request_accepted';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 27)
			{
				title = 'your request has been rejected';
				title = 'new contact request rejected';
				body = 'new contact request rejected';
				message  = 'new contact request rejected';
				type = 'jazemail_request_rejected';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 50)
			{
				title = 'fund received';
				body = 'fund received';
				message  = 'fund received';
				type = 'jazepay_fund_received';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 51)
			{
				title = 'quotations request received';
				body = 'quotations request received';
				message  = 'quotations request received';
				type = 'jazepay_quotations_request_received';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 52)
			{
				title = 'quotations request declined';
				body = 'quotations request declined';
				message  = 'quotations request declined';
				type = 'jazepay_quotations_request_declined';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 53)
			{
				title = 'quotations received';
				body = 'quotations received';
				message  = 'quotations received';
				type = 'jazepay_quotations_received';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 54)
			{
				title = 'quotations declined';
				body = 'quotations declined';
				message  = 'quotations declined';
				type = 'jazepay_quotations_declined';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 55)
			{
				title = 'order received';
				body = 'order received';
				message  = 'order received';
				type = 'jazepay_order_received';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 56)
			{
				title = 'order declined';
				body = 'order declined';
				message  = 'order declined';
				type = 'jazepay_order_declined';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 57)
			{
				title = 'invoice received';
				body = 'invoice received';
				message  = 'invoice received';
				type = 'jazepay_invoice_received';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 58)
			{
				title = 'invoice declined';
				body = 'invoice declined';
				message  = 'invoice declined';
				type = 'jazepay_invoice_declined';
				master_fild_name = 'group_id';
			}
			else if(parseInt(reqParams.notifi_flag) === 59)
			{
				title = 'payment received';
				body = 'payment received';
				message  = 'payment received';
				type = 'jazepay_invoice_payment_received';
				master_fild_name = 'group_id';
			}


			var	template = {
				data: {   
					title : title,
					body : body,
					message: message,
					msg_type: message_type,
					type: type,
					[master_fild_name]:reqParams.group_id,
					sender_account_id:reqParams.sender_account_id,
					sender_name:reqParams.sender_name,
					sender_logo:reqParams.sender_logo,
					receiver_account_id:reqParams.receiver_account_id,
					receiver_name		:reqParams.receiver_name,
					timestamp: moment().utc().format('YYYY-MM-DD hh:mm:ss').toString(),
					sound: 'default',
					priority:'high',
					delay_while_idle: 'false',
					content_available: 'true'
				}
			};
	
			

		//	HelperWebNotification.getFcmTokenWeb(template,arrParams);

			return_result(template);
		}
		catch(err)
		{ 
		
			console.log("getNotificationWebPushTemplate: " + err);
			return_result(null);
		}
	}

	// get notification template
	methods.getNotificationWebTemplate = function(reqParams,return_result){

		try
		{
			var title = category  = '';

			var notifi_status = 1;

			//check flag
			if(parseInt(reqParams.flag) === 1)
			{
				title = 'new contact request received';
				category = 'jazechat';
			}
			else if(parseInt(reqParams.flag) === 2)
			{
				title = 'your request has been approved';
				category = 'jazechat';
			}
			else if(parseInt(reqParams.flag) === 3)
			{
				title = 'your request has been rejected';
				category = 'jazechat';
			}
			else if(parseInt(reqParams.flag) === 4)
			{
				title = 'new message received';
				category = 'jazechat';
				notifi_status = 0;
			}
			else if(parseInt(reqParams.flag) === 5)
			{
				title = 'feed received';
				category = 'jazefeed';
			}
			else if(parseInt(reqParams.flag) === 6)
			{
				title = 'feed replay received';
				category = 'jazefeed';
			}
			else if(parseInt(reqParams.flag) === 7)
			{
				title = 'request appointment received';
				category = 'diary';
			}
			else if(parseInt(reqParams.flag) === 8)
			{
				title = 'appointment accepted';
				category = 'diary';
			}
			else if(parseInt(reqParams.flag) === 9)
			{
				title = 'appointment rejected';
				category = 'diary';
			}
			else if(parseInt(reqParams.flag) === 10)
			{
				title = 'block contact';
				category = 'jazechat';
				notifi_status = 0;
			}
			else if(parseInt(reqParams.flag) === 11)
			{
				title = 'unblock contact';
				category = 'jazechat';
				notifi_status = 0;
			}
			else if(parseInt(reqParams.flag) === 12)
			{
				title = reqParams.message;
				category = 'jazemail';
			}
			else if(parseInt(reqParams.flag) === 13)
			{
				
			}
			else if(parseInt(reqParams.flag) === 14)
			{
				title = 'team invitation received';
				category = 'team';
			}
			else if(parseInt(reqParams.flag) === 15)
			{
				title = 'team invitation approved';
				category = 'team';
			}
			else if(parseInt(reqParams.flag) === 16)
			{
				title = 'team invitation rejected';
				category = 'team';
			}
			else if(parseInt(reqParams.flag) === 17)
			{
				title = 'contact request has been deleted';
				category = 'jazechat';
			}
			else if(parseInt(reqParams.flag) === 18)
			{
				title = 'team invitation removed';
				category = 'team';
			}
			else if(parseInt(reqParams.flag) === 19)
			{
				title = 'notice has been received';
				category = 'notice';
			}
			else if(parseInt(reqParams.flag) === 20)
			{
				title = 'cv request';
				category = 'jazejob';
			}
			else if(parseInt(reqParams.flag) === 21)
			{
				title = 'cv request has been accepted';
				category = 'jazejob';
			}
			else if(parseInt(reqParams.flag) === 22)
			{
				title = 'cv request has been rejected';
				category = 'jazejob';
			}
			else if(parseInt(reqParams.flag) === 23)
			{
				title = 'team member removed';
				category = 'team';
			}
			else if(parseInt(reqParams.flag) === 24)
			{
				title = 'added follow groups';
				category = 'notice';
			}
			else if(parseInt(reqParams.flag) === 25)
			{
				title = 'new contact request received';
				category = 'jazemail';
			}
			else if(parseInt(reqParams.flag) === 26)
			{
				title = 'your request has been approved';
				category = 'jazemail';
			}
			else if(parseInt(reqParams.flag) === 27)
			{
				title = 'your request has been rejected';
				category = 'jazemail';
			}
			else if(parseInt(reqParams.flag) === 50)
			{
				title = 'fund received';
				category = 'jazepay';
			}
			else if(parseInt(reqParams.flag) === 51)
			{
				title = 'quotations request received';
				category = 'jazepay';
			}
			else if(parseInt(reqParams.flag) === 52)
			{
				title = 'quotations request declined';
				category = 'jazepay';
			}
			else if(parseInt(reqParams.flag) === 53)
			{
				title = 'quotations received';
				category = 'jazepay';
			}
			else if(parseInt(reqParams.flag) === 54)
			{
				title = 'quotations declined';
				category = 'jazepay';
			}
			else if(parseInt(reqParams.flag) === 55)
			{
				title = 'order received';
				category = 'jazepay';
			}
			else if(parseInt(reqParams.flag) === 56)
			{
				title = 'order declined';
				category = 'jazepay';
			}
			else if(parseInt(reqParams.flag) === 57)
			{
				title = 'invoice received';
				category = 'jazepay';
			}
			else if(parseInt(reqParams.flag) === 58)
			{
				title = 'invoice declined';
				category = 'jazepay';
			}
			else if(parseInt(reqParams.flag) === 59)
			{
				title = 'payment received';
				category = 'jazepay';
			}



			if(parseInt(notifi_status) === 1)
			{
				var template = {
						account_id: reqParams.account_id,
						receiver_account_id: reqParams.request_account_id,
						notify_category: category,
						notify_type: (parseInt(reqParams.flag) + 100).toString(),
						notify_id: reqParams.group_id,
						notify_text: title,
						read_status: "0",
						notify_response: "0",
						create_date: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
						status: '1'
					};

				return_result(template);
			}
			else
			{return_result(null);}
		}
		catch(err)
		{ 
			console.log("getNotificationWebTemplate: " + err);
			return_result(null);
		}
	}

	// Send Push Notification
	methods.sendPushNotification = function(arrParams,return_result){
	
		try{ 
			var i = 0;
			//save to notification table
			methods.saveJazeNotificationTable(arrParams, function (return_notifi) {
				
					if(return_notifi !== null && parseInt(i) === 0){
						i++;
						//get owner details
						var ownerParams = { account_id : arrParams.request_account_id };
						HelperGeneral.getAccountDetails(ownerParams, function (ownerdetails) {
							
							if(ownerdetails !== null ){	

								//get sender fcm tocken
								methods.___getSenderAccountDetails(arrParams, function (userdetails) {
									
									if(userdetails !== null ){

										// web notification start
										var webTemplateParam = {
										
											device_type 		:  '',
											notifi_flag			:  arrParams.flag,
											group_id			:  arrParams.group_id,
											sender_account_id	:  userdetails.account_id,
											sender_account_type	:  userdetails.account_type,
											sender_name			:  userdetails.name,
											sender_logo			:  userdetails.logo,
											receiver_account_id :  arrParams.request_account_id,
											receiver_name 		:  ownerdetails.name,
											msg_type			:  arrParams.msg_type,
											msg					:  arrParams.msg,
										};
										
										methods.getNotificationWebPushTemplate(arrParams,webTemplateParam, function (return_web_template) {
											
										// web notification end
		
											//get reciver fcm tocken
											methods.getFCMTokenList(arrParams, function (fcmToken) {
												
												if(fcmToken !== null ){
													//foreach
													var obj_key = Object.keys(fcmToken);

													var counter = obj_key.length;

													if(parseInt(counter) !== 0)
													{
														promiseForeach.each(obj_key,[function (obj_key) {

															return new Promise(function (resolve, reject) {

																// get templates
																var template_para = {};
																
																if(parseInt(arrParams.flag) === 4 || parseInt(arrParams.flag) === 7 || parseInt(arrParams.flag) === 8 || parseInt(arrParams.flag) === 9)
																{
																	template_para = {
																		device_type 		:  obj_key,
																		notifi_flag			:  arrParams.flag,
																		group_id			:  arrParams.group_id,
																		sender_account_id	:  userdetails.account_id,
																		sender_account_type	:  userdetails.account_type,
																		sender_name			:  userdetails.name,
																		sender_logo			:  userdetails.logo,
																		receiver_account_id :  arrParams.request_account_id,
																		receiver_name 		:  ownerdetails.name,
																		msg_type			:  arrParams.msg_type,
																		msg					:  arrParams.msg,
																		fcm_token			:  fcmToken[obj_key], 
																	};
																}
																else
																{
																	template_para = {
																		device_type 		:  obj_key,
																		notifi_flag			:  arrParams.flag,
																		group_id			:  arrParams.group_id,
																		sender_account_id	:  userdetails.account_id,
																		sender_account_type	:  userdetails.account_type,
																		sender_name			:  userdetails.name,
																		sender_logo			:  userdetails.logo,
																		receiver_account_id :  arrParams.request_account_id,
																		receiver_name 		:  ownerdetails.name,
																		fcm_token			:  fcmToken[obj_key]
																	};
																}
																
																methods.getNotificationMobTemplate(template_para, function (return_template) {
																	
																	if(return_template !== null)
																	{  
																		
																			// send Notification
																			FCM.sendToMultipleToken(return_template, array_unique(fcmToken[obj_key]), function(err, response) {
																			//	console.log('response-- ',moment().utc().format('YYYY-MM-DD hh:mm:ss').toString());	
																				if(err){
																					console.log('err-- ', err);
																				}
																				else if(response[0].response === "Error sending message:")
																				{
																						methods.setDeactivateDevice(response[0].token,function(response) {});												
																				}
																					
																				counter -= 1;

																				resolve(obj_key);
																			});
																	}
																});
															});
															
														}],
														function (result, current) {
															if (counter === 0)
															{  return_result('1'); }
														});
													}
													else
													{ return_result('1'); }
												}
												else
												{ return_result('1'); }
											});

										});
									}
									else
									{ return_result('0'); }
								});
							}
							else
							{ return_result('0'); }
						});
					}
					else
					{ return_result('1'); }
			});
		}
		catch(err)
		{
			console.log('err: ', err);
			return_result('0');
		}
	};

	// deactivate  Device
	methods.setDeactivateDevice = function(fcmToken,return_result){
		
		try
		{
			if(fcmToken !== "")
			{
				//update query
				DB.query(" UPDATE `jaze_app_device_details` SET `fcm_token`='' WHERE `fcm_token` = '"+fcmToken+"' and status = '1' ", null, function(data, error){

				});
			
				return_result("1");
			}
		}	
		catch(err)
		{
			console.log(err);
			return_result("0");
		}

	};

	//insert 
	methods.saveJazeNotificationTable = function(template_para,return_result){

		methods.getNotificationWebTemplate(template_para, function (return_template) {
			
			if(return_template !== null)
			{ 
				//get group id
				HelperRestriction.getNewGroupId('jaze_notification_list', function (group_id) {
					
					//check group id exist 
					methods.___checkGroupidExist('jaze_notification_list',group_id, function (new_group_id) {

						//insert table
						HelperRestriction.insertTBQuery('jaze_notification_list',new_group_id,return_template, function (queGrpId) {
		
							if(queGrpId !== null)
							{  if(parseInt(template_para.flag) === 105 || parseInt(template_para.flag) === 106)
								{ console.log("feed notification",queGrpId); }
								return_result(queGrpId);
							}
							else
							{ return_result(null); }
						});
					});
				});
			}
			else
			{ return_result(template_para); }
		});
	};


	methods.___getSenderAccountDetails= function(arrParams,return_result)
	{ 
		var objResult = { 
			account_id : "-1",
			account_type : "3",
			name : "jazenet",
			logo : G_STATIC_WEB_URL+"images/jazenet/jazenet_softdev_dmcc.png",
		};

		if(parseInt(arrParams.account_id) !== -1)
		{
			HelperGeneral.getAccountDetails(arrParams, function (userdetails) {
				return_result(userdetails);
			});
		}
		else
		{ return_result(objResult); }	
	}

	methods.___checkGroupidExist= function(tbName,group_id,return_result)
	{ 
		try{
			if(tbName !== "" && parseInt(group_id) !== 0)
			{
				var query = " SELECT  group_id FROM  "+tbName+ "  where `group_id` = '"+group_id+ "' group by group_id ";

				DB.query(query,null, function(data, error){ 

					if(typeof data !== 'undefined' && data !== null  && parseInt(data.length) > 0){
							
						HelperRestriction.getNewGroupId(tbName, function (new_group_id) {
	
							return_result(new_group_id);
						});				
					}
					else
					{ return_result(group_id); }
				});
			}
			else
			{ return_result(group_id); }
		}
		catch(err)
		{
			console.log(err);
			return_result(group_id);
		}
	}

	//------------------------------------------------------------------- broadcasting Module Start ---------------------------------------------------------------------------//

	/*
	* Update Conversation Last Message Date
	*/
	methods.updateConversationChatDate =  function(receiver_account_id,conversa_id,upd_date,return_result){

			try
			{
				var query = "  select group_id from jaze_jazechat_conversation_list WHERE meta_key = 'conversa_id' and meta_value = '"+conversa_id+"' ";

				DB.query(query,null, function(queGrpId, error){

					if(typeof queGrpId !== 'undefined' && queGrpId !== null && parseInt(queGrpId.length) > 0){
						
						var group_id = queGrpId[0].group_id;

						var update_date =dateFormat(new Date(parseInt(upd_date)), 'yyyy-mm-dd HH:MM:ss');




					var query = "  UPDATE jaze_jazechat_conversation_list SET meta_value = '"+update_date+"' where meta_key = 'update_date' and group_id = '"+group_id+"'; ";
					    query += "  UPDATE jaze_jazechat_conversation_list SET meta_value = (meta_value + 1) where meta_key = 'unread_count_"+receiver_account_id+"' and group_id = '"+group_id+"'; ";

						DB.query(query,null, function(dataUpdate, error){

						});	

						return_result(group_id);
					}
				});
				
			}
			catch(err)
			{
				console.log(err);
				return_result(0);
			}
	};

	/*
	* get Conversation Receiver Details
	*/
	methods.getConversationAccountDetails = function(conversation_id,account_id,return_result){

		try
		{
			if(parseInt(conversation_id) !== 0 && parseInt(account_id) !== 0)
			{
				let query  = " SELECT  ";
						query += " (SELECT a.meta_value FROM jaze_jazechat_conversation_list a WHERE a.meta_key = 'contact_account_id' AND a.group_id = jaze_jazechat_conversation_list.group_id) AS contact_account_id, ";
						query += " (SELECT g.meta_value FROM jaze_jazechat_conversation_list g WHERE g.meta_key = 'account_id' 		AND g.group_id = jaze_jazechat_conversation_list.group_id) AS account_id ";
						query += " FROM  jaze_jazechat_conversation_list  ";
						query += " WHERE group_id IN(SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'conversa_id' AND meta_value = '"+conversation_id+"')  group by group_id;  ";		

						
					DB.query(query,null, function(data, error){
					
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							let arrAccountParams = {};

							if(parseInt(account_id) === parseInt(data[0].contact_account_id))
							{ arrAccountParams = { account_id:data[0].account_id }; }
							else
							{ arrAccountParams = { account_id:data[0].contact_account_id }; }
	
							HelperGeneral.getAccountDetails(arrAccountParams, function(accDetails){	

								return_result(accDetails);
							});
						}
					});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("__getConversationAccountDetails",err); return_result(null);}
	}; 
	//------------------------------------------------------------------- broadcasting Module End ---------------------------------------------------------------------------//




module.exports = methods;

