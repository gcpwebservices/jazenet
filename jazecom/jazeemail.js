require('events').EventEmitter.prototype._maxListeners = 1000;
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const express = require('express');
const router = express.Router();

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperJazeEMail	= require('./helpers/HelperJazeEMail.js');
const HelperEncrypt	= require('../helpers/HelperEncrypt.js');

const { check, validationResult } = require('express-validator/check');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};
var arrEmails = [];

const btoa = require('btoa');
const atob = require('atob');
const fs = require('fs');
const path = require('path');

const multer = require('multer');

const crypto = require("crypto");
const CryptoAlgorithm = "aes-256-cbc";

let key = crypto.createHash('sha256').update(String(MAILKEY)).digest('base64').substr(0, 32);

var randomBytes = require('randombytes');

//-- random number generator --//
var rn = require('random-number');
var options = { min:  100, max:  100000, integer: true };

process.on('uncaughtException', function (err) {
  	console.error((new Date).toUTCString() + ' uncaughtException:', err.message)
  	console.error(err.stack)
  	process.exit(1)
});

//------------------------------------------------------------------- MAIL MODULE START -----------------------------------------------------------//

/*
*  Send Email New
*/
router.post('/jazeemail/send_email', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('reply_id','Invalid reply id').isNumeric(),
	check('flag','Invalid flag').isNumeric(),
	check('mail_id','Invalid mail id').isNumeric(),
	check('return_folder_id','Invalid return folder id').isNumeric(),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
	}

	var arrParams = {		
		api_token   	: req.body.api_token,
		account_id  	: req.body.account_id,
		to_account_id  	: req.body.to_account_id,
		cc_account_id  	: req.body.cc_account_id,
		bcc_account_id  : req.body.bcc_account_id,
		subject     	: req.body.subject,
		message     	: req.body.message,
		attachment     	: req.body.attachment,
		parent_id		: req.body.reply_id,
		flag			: req.body.flag,
		mail_id			: req.body.mail_id,
		folder			: req.body.return_folder_id,
		page     	    : '0' 
	};
	
	//flag 1 = send email | 2 = save email
	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		
		if(queParam.length)
		{	
			HelperJazeEMail.saveSendEmail(arrParams, function(retGroupId){

				if(parseInt(retGroupId) != 0){

					if(parseInt(arrParams.folder) != 0)
					{					
						HelperJazeEMail.getMailList(arrParams, function(mail_details){				

							HelperJazeEMail.getAllEmailCount(arrParams.account_id, null, function (mail_count) {		
			
								var email_arr = [];	
			
								mail_details.message_count = mail_count;
			
								email_arr.push(mail_details);
											
								jsonRes = {
									success: '1',
									result : {email_object : email_arr},
									successMessage: 'Email Sent Successfully...',
									errorMessage: errorMessage
								};
					
								res.status(201).json(jsonRes);
								res.end();
							});
						});
					}
					else
					{
						jsonRes = {
							success: '1',
							result : {},
							successMessage: 'Email Sent Successfully...',
							errorMessage: errorMessage
						};
			
						res.status(201).json(jsonRes);
						res.end();
					}
				}
				else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};
		
					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});
});	


/*
*  Mail List
*/
router.post('/jazeemail/mail_list', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('folder','Invalid folder id').isNumeric(),
	check('page','Invalid page no').isNumeric(),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
	}

	var arrParams = {		
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		folder  	: req.body.folder,
		page     	: req.body.page
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		
		if(queParam.length)
		{	
			HelperJazeEMail.getMailList(arrParams, function(mail_details){				

				HelperJazeEMail.getAllEmailCount(arrParams.account_id, null, function (mail_count) {		

					var email_arr = [];	

					mail_details.message_count = mail_count;

					email_arr.push(mail_details);
								
					jsonRes = {
						success: '1',
						result : {email_object : email_arr},
						successMessage: 'Mail List',
						errorMessage: errorMessage
					};
		
					res.status(201).json(jsonRes);
					res.end();
				});
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


/**
 * attachment upload
 */

const storage = multer.memoryStorage()
const upload = multer({ storage });

router.post('/jazeemail/attachment_uploads', function(req, res) {
	
	var uploadMulti = multer({
		storage: storage,
	}).array('file',10);

	uploadMulti(req, res, function(err) {
		
		HelperEncrypt.fileEncrypt(req.files, 1, function(encrypted_file){

			if(encrypted_file !== null){

				jsonRes = {
					success: '1',
					result : { encrypted_details : encrypted_file},
					successMessage: 'Encrypted File',
					errorMessage: errorMessage
				};

				res.status(201).json(jsonRes);
				res.end();
			}
			else{

				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Unable to process request. Please try again." 
				};

				res.status(201).json(jsonRes);
				res.end();
			}
		});
	});
});

/**
 * single mail details
 */

router.post('/jazeemail/mail_details', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('mail_id','Invalid mail id').isNumeric(),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
	}

	var arrParams = {		
		api_token   	: req.body.api_token,
		account_id  	: req.body.account_id,
		mail_group_id	: req.body.mail_id,
		flag			: 1,
		history			: 1,
		read_status		: 	""	
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		
		if(queParam.length)
		{	
			HelperJazeEMail.__getMailDetailsByGroupId(arrParams, function(mail_result){//console.log('mail_result',mail_result);
								
				if(mail_result !== null){

					jsonRes = {
						success: '1',
						result : {email_details : mail_result},
						successMessage: 'Mail Details',
						errorMessage: errorMessage
					};
		
					res.status(201).json(jsonRes);
					res.end();
				}
				else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};
		
					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

/**
 * delete mail
 */
router.post('/jazeemail/delete_mail', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('mail_group_id','Invalid mail group id').isLength({ min: 1 }),
	check('folder_id','Invalid folder id').isNumeric(),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
	}

	var arrParams = {		
		api_token   	: req.body.api_token,
		account_id  	: req.body.account_id,
		mail_group_id	: req.body.mail_group_id,
		folder_id		: req.body.folder_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		
		if(queParam.length)
		{	
			HelperJazeEMail.deleteMail(arrParams, function(delete_result){

				if(parseInt(delete_result) == 1)
				{
					jsonRes = {
						success: '1',
						result : {},
						successMessage: 'Mail Deleted',
						errorMessage: errorMessage
					};
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Mail not found." 
					};
				}				
	
				res.status(201).json(jsonRes);
				res.end();	
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Mail not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

/*
* move mail
*/
router.post('/jazeemail/move_mail', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('mail_group_id','Invalid mail group id').isLength({ min: 1 }),
	check('move_folder_id','Invalid folder id').isNumeric(),
	check('return_folder_id','Invalid return folder id').isNumeric(),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
		if(sent) return;
		_send.bind(res)(data);
		sent = true;
	};
	
	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
		success: success,
		result: {},
		successMessage: successMessage,
		errorMessage: errors.array()[0].msg
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var arrParams = {		
		api_token   	: req.body.api_token,
		account_id  	: req.body.account_id,
		mail_group_id	: req.body.mail_group_id,
		move_folder_id	: req.body.move_folder_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		
		if(queParam.length)
		{	
			HelperJazeEMail.moveMail(arrParams, function(move_result)
			{
				if(move_result)
				{
					var listParams = {		
						api_token   : req.body.api_token,
						account_id  : req.body.account_id,
						folder  	: req.body.return_folder_id,
						page     	: '0'
					};

					HelperJazeEMail.getMailList(listParams, function(mail_details){		
						
						HelperJazeEMail.getAllEmailCount(arrParams.account_id, null, function (mail_count) {		

							var email_arr = [];	
		
							mail_details.message_count = mail_count;
		
							email_arr.push(mail_details);
										
							jsonRes = {
								success: '1',
								result : {email_object : email_arr},
								successMessage: 'Mail List',
								errorMessage: errorMessage
							};
				
							res.status(201).json(jsonRes);
							res.end();
						});
					});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Updation Failed" 
					};
		
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

/*
* encrypt given file and upload
*/
router.post('/jazeemail/file_encrypt_upload', [
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('file_name','Invalid file').isLength({ min: 1 }),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
		if(sent) return;
		_send.bind(res)(data);
		sent = true;
	};
	
	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
		success: success,
		result: {},
		successMessage: successMessage,
		errorMessage: errors.array()[0].msg
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var arrParams = {		
		api_token   : req.body.api_token,
		file_name	: req.body.file_name
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		
		if(queParam.length)
		{	
			HelperEncrypt.fileCopyEncrypt(arrParams.file_name, function(result)
			{
				if(result !== null && parseInt(result.length) > 0){

					jsonRes = {
						success: '1',
						result : { encrypted_details : result},
						successMessage: 'Encrypted File',
						errorMessage: errorMessage
					};
	
					res.status(201).json(jsonRes);
					res.end();
				}
				else{
	
					jsonRes = {
						success: success,
						result: [],
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};
	
					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: [],
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

//------------------------------------------------------------------- MAIL MODULE END -----------------------------------------------------------//

//------------------------------------------------------------------- FOLDER MODULE START -------------------------------------------------------//

/*
*  create or update folder
*/
router.post('/jazeemail/create_update_folder', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('folder_name','Invalid folder name').isLength({ min: 1 }),
	check('parent_id','Invalid parent id').isNumeric(),
	check('folder_id','Invalid folder id').isNumeric(),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
	}

	var arrParams = {		
		api_token	: req.body.api_token,
		account_id	: req.body.account_id,
		folder_name	: req.body.folder_name,
		parent_id	: req.body.parent_id,
		folder_id	: req.body.folder_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		
		if(queParam.length)
		{	
			HelperJazeEMail.saveCreateUpdateFolder(arrParams, function(result){

				if(result !== null && result['error'] === undefined)
				{
					jsonRes = {
						success: '1',
						result : {folder_list : result},
						successMessage: 'Folder Details',
						errorMessage: errorMessage
					};
		
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: result 
					};
		
					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Error in saving." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

/*
*  folder list
*/
router.post('/jazeemail/folder_list', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
	}

	var arrParams = {		
		api_token	: req.body.api_token,
		account_id	: req.body.account_id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		
		if(queParam.length)
		{	
			HelperJazeEMail.__getFolderList(arrParams.account_id, 0, function(list_result){

				jsonRes = {
					success: '1',
					result : {folder_list : list_result},
					successMessage: 'Folder Details',
					errorMessage: errorMessage
				};
	
				res.status(201).json(jsonRes);
				res.end();
				
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "No Data found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

/*
*  delete folder
*/
router.post('/jazeemail/delete_folder', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('folder_id','Invalid folder id').isNumeric(),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
	}

	var arrParams = {		
		api_token		: req.body.api_token,
		account_id		: req.body.account_id,
		folder_id		: req.body.folder_id,
		new_folder_id	: req.body.new_folder_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		
		if(queParam.length)
		{	
			HelperJazeEMail.deleteFolderDetails(arrParams, function(result){

				if(parseInt(result) == 1)
				{
					jsonRes = {
						success: '1',
						result : {},
						successMessage: 'Folder Deleted',
						errorMessage: errorMessage
					};
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Folder not found." 
					};
				}				
	
				res.status(201).json(jsonRes);
				res.end();				
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Error while deleting folder" 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

/*
* move folder
*/
router.post('/jazeemail/move_folder', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('folder_id','Invalid folder id').isNumeric(),
	check('new_folder_id','Invalid folder id').isNumeric(),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
	}

	var arrParams = {		
		api_token		: req.body.api_token,
		account_id		: req.body.account_id,
		folder_id		: req.body.folder_id,
		new_folder_id	: req.body.new_folder_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		
		if(queParam.length)
		{	
			HelperJazeEMail.moveFolder(arrParams, function(list_result){

				if(list_result !== null)
				{
					jsonRes = {
						success: '1',
						result : {folder_list : list_result},
						successMessage: 'Folder Details',
						errorMessage: errorMessage
					};
		
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Error while moving folder" 
					};
		
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else	
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Error while moving folder" 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

//------------------------------------------------------------------- FOLDER MODULE START -----------------------------------------------------------//

module.exports = router;

