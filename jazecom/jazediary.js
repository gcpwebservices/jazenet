require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperDiary = require('./helpers/HelperDiary.js');
const fast_sort = require("fast-sort");
const { check, validationResult } = require('express-validator');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};


/*
 * Diary Day List
 */
router.post('/jazediary/diary_day_list',[
	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('app_id').isNumeric(),
	check('date').isISO8601('yyyy-mm-dd')

	], (req,res,next) => {


	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token	 		:req.body.api_token,
		app_id				:req.body.app_id,
		account_id	 		:req.body.account_id,
		date_from			:req.body.date,
		date_to				:req.body.date
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperDiary.getDeviceAccountList(arrParams, function(retChildAccounts){

				if(retChildAccounts !== null)
				{
					var child_account_id_list = retChildAccounts[0].account_id_list;

					HelperDiary.getRequestAppointments(child_account_id_list,arrParams, function(queAppoint){

						HelperDiary.getOwnAppointments(child_account_id_list,arrParams, function(queOwnAppoint){

							HelperDiary.getTaskList(child_account_id_list,arrParams, function(task_list){

								var appointment_list = queAppoint.concat(queOwnAppoint);
									
								jsonRes = {
									success: '1',
									result: {day_list: {appointment_list,task_list:task_list}},
									successMessage: 'Day List',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();
							});
						});
					});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

/*
 * Diary Week List
 */
router.post('/jazediary/diary_week_list',[
	check('api_token').isAlphanumeric(),
	check('app_id').isNumeric(),
	check('account_id').isNumeric(),
	check('date_from').isISO8601('yyyy-mm-dd'),
	check('date_to').isISO8601('yyyy-mm-dd')
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token	 		:req.body.api_token,
		account_id	 		:req.body.account_id,
		app_id	 	 		:req.body.app_id,
		date_from			:req.body.date_from,
		date_to				:req.body.date_to
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperDiary.getDeviceAccountList(arrParams, function(retChildAccounts){

				if(retChildAccounts !== null)
				{
					var child_account_id_list = retChildAccounts[0].account_id_list;

						HelperDiary.getRequestAppointments(child_account_id_list,arrParams, function(queAppoint){

							HelperDiary.getOwnAppointments(child_account_id_list,arrParams, function(queOwnAppoint){

								HelperDiary.getTaskList(child_account_id_list,arrParams, function(task_list){

										if(typeof queAppoint !== 'undefined' && queAppoint !== null && parseInt(queAppoint.length) > 0)
										{ queAppoint = fast_sort(queAppoint).asc('filter_date'); }
										
										if(typeof queOwnAppoint !== 'undefined' && queOwnAppoint !== null && parseInt(queOwnAppoint.length) > 0)
										{ queOwnAppoint = fast_sort(queOwnAppoint).asc('filter_date'); }

										if(typeof task_list !== 'undefined' && task_list !== null && parseInt(task_list.length) > 0)
										{ task_list = fast_sort(task_list).asc('task_date'); }

										var appointment_list = queAppoint.concat(queOwnAppoint);

										jsonRes = {
											success: '1',
											result: { week_list: {appointment_list,task_list} },
											successMessage: 'Week List',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();

								});
							});
						});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "some error occurred, please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}	
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

/*
 * Diary Year List
 */
router.post('/jazediary/diary_year_list',[
	check('api_token').isAlphanumeric(),
	check('app_id').isNumeric(),
	check('account_id').isNumeric(),
	check('year').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token	 		:req.body.api_token,
		account_id	 		:req.body.account_id,
		app_id	 	 		:req.body.app_id,
		year				:req.body.year
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperDiary.getDeviceAccountList(arrParams, function(retChildAccounts){

				if(retChildAccounts !== null)
				{
					var child_account_id_list = retChildAccounts[0].account_id_list;

						HelperDiary.getIncomingCount(child_account_id_list,arrParams, function(inbox_count){

							HelperDiary.getDiaryYearDatekList(child_account_id_list,arrParams, function(reternYear){

										var inner_object = {
											year_list: reternYear,
											inbox_count: inbox_count.toString()
										};

										jsonRes = {
											success: '1',
											result: inner_object,
											successMessage: 'Year List',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();
								});
							});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "some error occurred, please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}	
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

/*
 * Diary Inbox/Outbox List
 */
router.post('/jazediary/diary_inbox',[
	check('api_token').isAlphanumeric(),
	check('app_id').isNumeric(),
	check('account_id').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	  var arrParams = {
		api_token	 		:req.body.api_token,
		account_id	 		:req.body.account_id,
		app_id	 	 		:req.body.app_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{ 
			HelperDiary.getDeviceAccountList(arrParams, function(retChildAccounts){

				if(retChildAccounts !== null)
				{
					var child_account_id_list = retChildAccounts[0].account_id_list;

					HelperDiary.setDiaryNotificationReadStatus(child_account_id_list, function(retUrnUpdate){});  

					HelperDiary.getIncOutList(child_account_id_list,'1', function(queIncRes){
								
						HelperDiary.getIncOutList(child_account_id_list,'2', function(queOutRes){

							if(typeof queIncRes !== 'undefined' && queIncRes !== null && parseInt(queIncRes.length) > 0)
							{ queIncRes = fast_sort(queIncRes).asc('order_by'); }

							if(typeof queOutRes !== 'undefined' && queOutRes !== null && parseInt(queOutRes.length) > 0)
							{ queOutRes = fast_sort(queOutRes).asc('order_by'); }
							
							jsonRes = {
								success: '1',
								result: { diary_incoming:queIncRes, diary_outgoing:queOutRes },
								successMessage: 'Diary Inbox',
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();
						});
					});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "some error occurred, please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

/*
 * Diary Details
 */
router.post('/jazediary/diary_details',[
	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('id').isNumeric(),
	check('flag').isNumeric()
	], (req,res,next) => {
	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		account_id	 : req.body.account_id,
		api_token	 : req.body.api_token,
		group_id 	 : req.body.id,
		flag 		 : req.body.flag
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperDiary.getDiaryDetails(arrParams, function(queDiaryRes){

				if(queDiaryRes!=null)
				{
					var inner_object = {};

					if(parseInt(arrParams.flag) !== 4)
					{ inner_object = { appointment_details:queDiaryRes, task_details:{} }; }
					else
					{ inner_object = { appointment_details:{}, task_details:queDiaryRes }; }

					jsonRes = {
						success: '1',
						result: inner_object,
						successMessage: 'Diary Details',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result founds" 
					};

					  res.status(statRes).json(jsonRes);
					 res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});


/*
 * Create update delete Task
 */
router.post('/jazediary/create_task_details',[
	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('flag').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
			account_id		: req.body.account_id,
			api_token		: req.body.api_token,
			flag 			: req.body.flag,
			group_id 		: req.body.task_id,
			task_date 		: req.body.date,
			task_time 		: req.body.task_time,
			remarks 		: req.body.remarks,
			reminder_mins 	: req.body.reminder_mins
		};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(queParam.length)
		{
			HelperDiary.createUpdateTaskDetails(arrParams, function(retrnResult){

				if(parseInt(retrnResult) !== 0)
				{
					var success_message = "";

					if(parseInt(arrParams.flag) === 1)
					{ success_message = 'New task is successfully created.'; }
					else if(parseInt(arrParams.flag) === 2 || parseInt(arrParams.flag) === 3 || parseInt(arrParams.flag) === 4)
					{ success_message = 'Task is successfully updated.'; }
					else if(parseInt(arrParams.flag) === 0)
					{ success_message = 'Task is successfully deleted.'; }

					jsonRes = {
						success: '1',
						result: {},
						successMessage: success_message,
						errorMessage: errorMessage 
					  };

					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});


/*
 * Create update delete own appointment
 */
router.post('/jazediary/create_ownappointment_details',[
	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		account_id		: req.body.account_id,
		api_token		: req.body.api_token,
		group_id 		: req.body.appointment_id,
		flag 			: req.body.flag,
		activity		: req.body.activity,
		subject			: req.body.subject,
		date 			: req.body.date,
		start_time 		: req.body.start_time,
		end_time 		: req.body.end_time,
		reminder_mins 	: req.body.reminder_mins,
		repetition_type : req.body.repetition_type,
		venue 			: req.body.venue,
		remarks 		: req.body.remarks
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(queParam.length)
		{
			HelperDiary.createUpdateOwnAppointmentDetails(arrParams, function(retrnResult){

				if(parseInt(retrnResult) !== 0)
				{
					var success_message = "";

					if(parseInt(arrParams.flag) === 1)
					{ success_message = 'New Appointment is successfully saved'; }
					else if(parseInt(arrParams.flag) === 2)
					{ success_message = 'Appointment is successfully updated.'; }
					else if(parseInt(arrParams.flag) === 0)
					{ success_message = 'Appointment is successfully deleted.'; }

					jsonRes = {
						success: '1',
						result: {},
						successMessage: success_message,
						errorMessage: errorMessage 
					  };

					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});


/*
 * Create update delete Task
 */
router.post('/jazediary/create_request_appointment_details',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('company_account_id').isNumeric(),
	check('company_account_type').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
			account_id			 : req.body.account_id,
			account_type		 : req.body.account_type,
			company_account_id 	 : req.body.company_account_id,
			company_account_type : req.body.company_account_type,
			api_token			 : req.body.api_token,
			group_id 			 : req.body.appointment_id,
			flag 				 : req.body.flag,
			activity 		 	 : req.body.activity,
			subject 		 	 : req.body.subject,
			no_people 			 : req.body.no_people,
			date 				 : req.body.date,
			start_time 			 : req.body.start_time,
			end_time 			 : req.body.end_time,
			reminder_mins 		 : req.body.reminder_mins,
			repetition_type 	 : req.body.repetition_type,
			venue 				 : req.body.venue,
			remarks 			 : req.body.remarks,
			appointment_type 	 : req.body.appointment_type
		};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(queParam.length)
		{
			HelperDiary.checkAccountJazecomDiaryStatus(arrParams, function(retAccount){
						
				if(parseInt(retAccount) === 0)
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Sorry, the service isn't available right now. Please try again later." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
				else
				{
					if(parseInt(arrParams.account_id) !== parseInt(arrParams.company_account_id))
					{
						HelperDiary.createUpdateRequestAppointmentDetails(arrParams, function(retrnResult){

							if(parseInt(retrnResult) !== 0)
							{
								if(parseInt(arrParams.flag) === 1)
								{
									jsonRes = {
										success: '1',
										result: {},
										successMessage: 'Appointment request is successfully sent.',
										errorMessage: errorMessage 
									};

									res.status(201).json(jsonRes);
									res.end();	
								}
								else if(parseInt(arrParams.flag) === 0)
								{
									jsonRes = {
										success: '1',
										result: {},
										successMessage: 'Appointment is successfully deleted.',
										errorMessage: errorMessage 
									};

									res.status(201).json(jsonRes);
									res.end();
								}
								else
								{
									jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "Unable to process request. Please try again." 
									};

									res.status(statRes).json(jsonRes);
									res.end();
								}
							}
							else
							{
									jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "No result found." 
									};

									res.status(statRes).json(jsonRes);
									res.end();
							}
						});
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process. accounts are be same." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});


router.post('/jazediary/diary_appointment_acceptreject',[
	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('appointment_id').isNumeric(),
	check('status').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
			account_id  : req.body.account_id,
			api_token   : req.body.api_token,
			group_id  	: req.body.appointment_id,
			reason 		: req.body.reason,
			flag 		: req.body.status
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(queParam.length)
		{
			HelperDiary.acceptRejectRequestAppointment(arrParams, function(returnResult){

				if(returnResult !== null)
				{	
					if(parseInt(arrParams.flag) === 1)
					{
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Appointment request is accepted.',
							errorMessage: errorMessage 
						};

						res.status(201).json(jsonRes);
						res.end();	
					}
					else if(parseInt(arrParams.flag) === 2)
					{
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Appointment request is rejected.',
							errorMessage: errorMessage 
						};

						res.status(201).json(jsonRes);
						res.end();	
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process. Please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Account not found." 
					};

				  	res.status(statRes).json(jsonRes);
		 			res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});


module.exports = router;
