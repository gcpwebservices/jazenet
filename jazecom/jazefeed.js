'use strict';
require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();
const fast_sort = require("fast-sort");
const fs = require('fs');
const multer = require('multer');

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperJazefeed = require('./helpers/HelperJazefeed.js');
const { check, validationResult } = require('express-validator');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};


/*
*  Create New Feed Details
*/
router.post('/jazefeed/post_feed', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('company_account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	  var arrParams = {		
			account_id: req.body.account_id,
			company_account_id : req.body.company_account_id,
			subject : req.body.subject,
			message : req.body.body_message,
			file_name : req.body.file_name,
			api_token : req.body.api_token,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(queParam.length)
		{	
			HelperJazefeed.checkAccountJazecomFeedStatus(arrParams, function(retAccount){
				
				if(parseInt(retAccount) === 0)
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Sorry, the service isn't available right now. Please try again later." 
					};

					res.status(statRes).json(jsonRes);
					res.end();

				}else{

					HelperJazefeed.createFeedDetails(arrParams, function(notiParams){
										
						if(notiParams !== null)
						{ 
								jsonRes = {
									success: '1',
									result: {},
									successMessage: 'New feed has been posted.',
									errorMessage: errorMessage 
								};

								res.status(201).json(jsonRes);
								res.end();
						}
						else
						{
							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Unable to precess request. Please try again." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}
					});	
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

/*
*  Replay Feed
*/
router.post('/jazefeed/reply_feed',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('company_id').isNumeric(),
	check('feed_id').isNumeric()

	],(req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	  var arrParams = {		
		account_id: req.body.account_id,
		company_id: req.body.company_id,
		master_id : req.body.feed_id,
		message : req.body.body_message,
		file_name : req.body.file_name,
		api_token : req.body.api_token,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(queParam.length)
		{
			HelperJazefeed.saveReplayFeedDetails(arrParams, function(sendBack){

				if(sendBack !== null)
				{
					//update read status notification
					HelperJazefeed.setFeedRespondStatus(arrParams, function(retUrnUpdate){});  
					
						// sendback last details
							jsonRes = {
								success: '1',
								result: {reply_message:sendBack},
								successMessage: 'Successfully posted a reply from the feed.',
								errorMessage: errorMessage 
							};

							res.status(201).json(jsonRes);
							res.end();
				}else{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to reply on the feed. Please try again." 
					  };

					  res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});


router.post('/jazefeed/feed_list',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('page').isNumeric(),
	check('total_count').isNumeric()

	],(req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		account_id: req.body.account_id,
		api_token: req.body.api_token,
		page_no : req.body.page,
		total_count : req.body.total_count,
	};

	
	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(queParam.length)
		{
			HelperJazefeed.getJazeFeedGroupList(arrParams, function(retJazeFeed){

				if(retJazeFeed!=null)
				{
					HelperJazefeed.getCountJazeFeedGroupList(arrParams, function(totalcount){

						var next_page = 0;

						var current_page = arrParams.page_no;

						if(totalcount > current_page)
						{
							next_page =  parseInt(current_page) + 1;
						}

						var pagination_details  = {
							total_page 	 : totalcount.toString(),
							current_page : current_page.toString(),
							next_page    : next_page.toString()
						};

						jsonRes = {
							success: '1',
							result: {pagination_details:pagination_details,feed_list:fast_sort(retJazeFeed).asc('date') },
							successMessage: 'Feed List',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();

					});
				}
				else
				{
					jsonRes = {
						success: "1",
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

				  	res.status(statRes).json(jsonRes);
		 			res.end();
				}
			});	
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();

		}

	});
});


router.post('/jazefeed/feed_details',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('feed_id').isNumeric(),
	check('page').isNumeric()

	],(req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
	  }
	  
	var arrParams = {
		account_id: req.body.account_id,
		api_token: req.body.api_token,
		master_id:req.body.feed_id,
		page_no	:req.body.page
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{
			// get head Details
			HelperJazefeed.getJazeFeedGroupDetails(arrParams, function(head_details){

				if(head_details !== null)
				{
					//Get Detaile list
					HelperJazefeed.GetFeedList(arrParams, function(feedList){

						if(feedList !== null)
						{ 
							HelperJazefeed.getCountJazeFeedList(arrParams.master_id, function(totalcount){

										var next_page = 0;

										var current_page = arrParams.page_no;

										if(totalcount > current_page)
										{
											next_page =  parseInt(current_page) + 1;
										}

										var pagination_details  = {
											total_page 	 : totalcount.toString(),
											current_page : current_page.toString(),
											next_page    : next_page.toString()
										};

										jsonRes = {
											success: '1',
											result: {feed_details: { head_details:head_details, pagination_details : pagination_details, body_details:fast_sort(feedList).asc('date')}},
											successMessage: 'Feed Details',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();
							});
						}
						else
						{
							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "No result found." 
							  };
		
							 res.status(statRes).json(jsonRes);
							res.end();
						}
					});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found.." 
			  		};

		 			res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();

		}

	});
});

router.post('/jazefeed/read_feed_details',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('feed_id').isNumeric()

	],(req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
	  }
	  
	var arrParams = {
		account_id: req.body.account_id,
		api_token: req.body.api_token,
		master_id:req.body.feed_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{
			// get head Details
			HelperJazefeed.setFeedReadStatus(arrParams, function(feedStatus){

				if(feedStatus !== null)
				{
					//update read status notification 
					HelperJazefeed.setFeedNotificationReadStatus(arrParams, function(retUrnUpdate){});  
					
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Feed Updated',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found.." 
			  		};

		 			res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();

		}

	});
});



router.post('/jazefeed/feed_attachment_list',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('feed_id').isNumeric()

	],(req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
	  }
	  
	var arrParams = {
		account_id: req.body.account_id,
		api_token: req.body.api_token,
		master_id:req.body.feed_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{
			// get head Details
			HelperJazefeed.GetFeedAttachmentList(arrParams.master_id, function(list){

				if(list !== null)
				{
					jsonRes = {
						success: '1',
						result: {attachment_list: list},
						successMessage: 'Feed Attachment List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found.." 
			  		};

		 			res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();

		}

	});
});


router.post('/jazefeed/delete_feed',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('feed_id').isLength({ min: 1 })

	],(req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
	  }
	  
	var arrParams = {
		account_id: req.body.account_id,
		api_token: req.body.api_token,
		feed_id:req.body.feed_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{
			// get head Details
			HelperJazefeed.deleteFeed(arrParams, function(feedStatus){

				if(feedStatus !== null)
				{
					var arrParamS = {
						account_id	: arrParams.account_id,
						api_token	: arrParams.api_token,
						page_no 	: "1",
						total_count : "0",
					};

					HelperJazefeed.getJazeFeedGroupList(arrParamS, function(retJazeFeed){

						if(retJazeFeed!=null)
						{
							HelperJazefeed.getCountJazeFeedGroupList(arrParamS, function(totalcount){
		
								var next_page = 0;
		
								var current_page = arrParamS.page_no;
		
								if(totalcount > current_page)
								{
									next_page =  parseInt(current_page) + 1;
								}
		
								var pagination_details  = {
									total_page 	 : totalcount.toString(),
									current_page : current_page.toString(),
									next_page    : next_page.toString()
								};
		
								jsonRes = {
									success: '1',
									result: {pagination_details:pagination_details,feed_list:fast_sort(retJazeFeed).asc('date') },
									successMessage: 'Feed List',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();
		
							});
						}
						else
						{
							jsonRes = {
								success: "1",
								result: {},
								successMessage: successMessage,
								errorMessage: "No result found." 
							};
		
							  res.status(statRes).json(jsonRes);
							 res.end();
						}
					});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found.." 
			  		};

		 			res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});




router.post('/jazefeed/block_account',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('block_account_id').isNumeric(),
	check('flag').isNumeric()
	],(req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	  var arrParams = {		
		account_id: req.body.account_id,
		block_account_id: req.body.block_account_id,
		flag 		: req.body.flag,
		api_token : req.body.api_token,
	};

	// flag : 2 = block, 3 = unblock
	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(queParam.length)
		{
			HelperJazefeed.__saveReomveJazecomContactList(arrParams.flag,arrParams.account_id,arrParams.block_account_id, function(returnStatus){

				if(parseInt(returnStatus) !== 0)
				{
					if(parseInt(arrParams.flag) === 2)
					{
						jsonRes = {
							success: '1',
							result: {feed_block_status : '1'},
							successMessage: 'Account is blocked.',
							errorMessage: errorMessage 
						};

						res.status(201).json(jsonRes);
						res.end();
					}
					else if(parseInt(arrParams.flag) === 3)
					{
						jsonRes = {
							success: '1',
							result: {feed_block_status : '0'},
							successMessage: 'Account is unblocked.',
							errorMessage: errorMessage 
						};

						res.status(201).json(jsonRes);
						res.end();
					}

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					  };

					  res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});


var storageMulti = multer.diskStorage({
	destination: function (req, file, callback) {
	
	function asyncFunction (item, cb) {
	  	setTimeout(() => {
			
		 	var chat_path = G_STATIC_IMG_PATH+'jazefeed/'+item;
		 	
			if (!fs.existsSync(chat_path)){
		          fs.mkdirSync(chat_path,'0757', true); 
			}

			callback(null, chat_path);

			cb();
	  	}, 100);
	}

	let requests = 0;

		if(req.body.api_token !=''){

			return new Promise((resolve) => {
				requests = req.body.account_id;
			
		  		asyncFunction(requests, resolve);

		    });

		Promise.all(requests);

		}
  },
  	filename: function (req, file, callback) {
    	callback(null, file.originalname);
  	}
});


router.post('/jazefeed/multi_uploads', function(req, res) {

	var arrFiles = [];
	var arrObjects = [];
	var result = {};
	var uploadMulti = multer({
		storage: storageMulti,
	}).array('file',10);


	uploadMulti(req, res, function(err) {

	arrFiles.push(req.files);

		HelperJazefeed.getMultiple(req.files,req.body.account_id, function(returnResult){
	
          	jsonRes = {
                success: '1',
                result: {feed_media : returnResult},
                successMessage: 'File is successfully uploaded.',
                errorMessage: errorMessage 
            };
            res.status(201).json(jsonRes);
            res.end();

	  	});	

	})
});




module.exports = router;