'use strict';
// content of fcm_node.js

const bodyParser = require('body-parser');
const express = require('express');
const router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
const app = express();

const { check, validationResult } = require('express-validator');

const HelperGeneral = require('./helpers/HelperGeneral.js');
const HelperJazenetVoice = require('./helpers/HelperJazenetVoice.js');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};
 
var statRes = 422;
var jsonRes = {};
 

// ------------------------------------------------------ jazenet Voice Module Start  --------------------------------------------------------------//   

  //test.......
router.post('/jazenet_voice/getVoiceSearchResult', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
    check('account_type').isNumeric(),
    check('country').isNumeric(),
    check('state').isNumeric(),
    check('city').isNumeric()
    
], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
        api_token       : req.body.api_token,
		account_id      : req.body.account_id,
        account_type    : req.body.account_type,
        country         : req.body.country,
        state           : req.body.state,
        city            : req.body.city,
        keyword         : req.body.keyword
	};

    HelperGeneral.checkGeneralParametersApiToken(reqParams, function (queParamRes) {

        if (typeof queParamRes !== 'undefined' && queParamRes !== null && parseInt(queParamRes.length) !== 0)
        {  
            HelperJazenetVoice.getVoiceSearchResult(reqParams,function(voice_results) {

                if (voice_results !== null)
                {
                    jsonRes = {
                        success: '1',
                        result: voice_results,
                        successMessage: "voice search result",
                        errorMessage: "" 
                    };

                    res.status(201).json(jsonRes);
                    res.end();

                }
                else{
                    jsonRes = {
                        success: '0',
                        result: {},
                        successMessage: successMessage,
                        errorMessage: "no result found" 
                    };
    
                    res.status(201).json(jsonRes);
                    res.end();
                }
            });
        }
        else
        {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };
            res.status(statRes).json(jsonRes);
            res.end();
        }
   });
});

// ------------------------------------------------------ jazenet Voice Module End  --------------------------------------------------------------//   









//------------------------------------------------------- Private Functions -----------------------------------------------------------//



module.exports = router;