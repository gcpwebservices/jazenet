const methods = {};
const DB = require('./db_base.js');


methods.getAccountDetails = function(arrParams,return_result){

	var queAccountDetails = '';
	if(arrParams.account_type == '1' || arrParams.account_type == '2'){

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type, 
			CONCAT(MAX(CASE WHEN meta_key = 'fname' THEN meta_value END),' ',MAX(CASE WHEN meta_key = 'lname' THEN meta_value END)) AS fullname,
			MAX(CASE WHEN meta_key = 'profile_logo' THEN meta_value END) AS logo 
			FROM jaze_user_basic_details WHERE account_id=?`;

	}else{

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type,
			MAX(CASE WHEN meta_key = 'company_name' THEN meta_value END) AS fullname, 
			MAX(CASE WHEN meta_key = 'company_logo' THEN meta_value END) AS  logo  
			FROM jaze_user_basic_details WHERE account_id =?`;
	}

	DB.query(queAccountDetails,[arrParams.account_id], function(data, error){

	   	if(data != null){

			var newObj = {};
			var logo = '';
			var fullnmae = '';

		  	data.forEach(function(val,key,arr) {

		   		if(val.logo){
			  		if(val.logo != ''){
			  			if(val.account_type == '1'){
			  				logo = G_STATIC_IMG_URL+'uploads/jazenet/individual_account/members_thumb/thumb_'+val.logo;
			  			}else{
			  				logo = G_STATIC_IMG_URL+'uploads/companylogo/'+val.logo;
			  			}
			  		}
		  		}

		  		if(val.fullname){
	  				if(val.fullname !=''){
						fullnmae = val.fullname;
		  			}
		  		}
		  	
				newObj = { 
					account_id: val.account_id.toString(),
					account_type: val.account_type.toString(),
					name: fullnmae,
					logo: logo,
				};
		  	});

	 		return_result(newObj);
	
 	   	}else{
 	   		return_result(null);
 	   	}

	});
};

methods.getAccountDetailsFull = function(arrParams,return_result){

	var queAccountDetails = '';
	if(arrParams.account_type == '1' || arrParams.account_type == '2'){

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type, 
			CONCAT(MAX(CASE WHEN meta_key = 'fname' THEN meta_value END),' ',MAX(CASE WHEN meta_key = 'lname' THEN meta_value END)) AS fullname,
			MAX(CASE WHEN meta_key = 'profile_logo' THEN meta_value END) AS logo,
			MAX(CASE WHEN meta_key = 'email' THEN meta_value END) AS email,
			MAX(CASE WHEN meta_key = 'mobile' THEN meta_value END) AS mobile,
			MAX(CASE WHEN meta_key = 'mobile_country_code_id' THEN meta_value END) AS country_code
			FROM jaze_user_basic_details WHERE account_id=?`;

	}else{

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type,
			MAX(CASE WHEN meta_key = 'company_name' THEN meta_value END) AS fullname, 
			MAX(CASE WHEN meta_key = 'company_logo' THEN meta_value END) AS  logo,
			MAX(CASE WHEN meta_key = 'company_email' THEN meta_value END) AS email,
			MAX(CASE WHEN meta_key = 'company_phone' THEN meta_value END) AS mobile,
			MAX(CASE WHEN meta_key = 'company_phone_code' THEN meta_value END) AS country_code
			FROM jaze_user_basic_details WHERE account_id =?`;
	}

	DB.query(queAccountDetails,[arrParams.account_id], function(data, error){

	   	if(data != null){

			var newObj = {};
			var logo = G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png';
			var newPhoneCode;
			var fullname = '';
			
		  	data.forEach(function(val,key,arr) {

		  		if(val.logo != ''){
		  			if(val.account_type == '1'){
		  				logo = G_STATIC_IMG_URL+'uploads/jazenet/individual_account/members_thumb/thumb_'+val.logo;
		  			}else{
		  				logo = G_STATIC_IMG_URL+'uploads/companylogo/'+val.logo;
		  			}
		  		}

		  		if(val.fullname!=''){
		  			fullname = val.fullname;
		  		}

  				methods.getMobileCountryCode(val.country_code, function(phone_code){

  					if(phone_code!=null){
  						newPhoneCode = '+'+phone_code+val.mobile;
  					}else{
  						newPhoneCode = val.mobile;
  					}

					newObj = { 
						app_id: arrParams.new_app_id.toString(),
						api_token: arrParams.api_token,
						account_id: val.account_id.toString(),
						account_type: val.account_type.toString(),
						name: fullname,
						logo: logo,
						email: val.email,
						mobile: newPhoneCode
					};

					return_result(newObj);
		  		});

 	
		  	});

 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


methods.getAccountTypes = function(arrParams,return_result){


	var arrRows = [];
	var newObj = {};
	var arrAccountParams = {};
	var queLinkAccoType = `SELECT account_id,account_type FROM jaze_user_credential_details WHERE account_id =? `;
	DB.query(queLinkAccoType,[arrParams.account_id], function(data, error){

	   	if(data != null){

	   		function asyncFunction (item, cb) {
			  setTimeout(() => {
					return_result(item);
				cb();
			  }, 100);
			}

			let requests = data.map((val, key, array) => {
				arrAccountParams = { account_id:val.account_id, account_type:val.account_type};
				methods.getAccountDetails(arrAccountParams, function(returned){		

			    	return new Promise((resolve) => {
			      		asyncFunction(returned, resolve);
				    });

				});
			})

			Promise.all(requests);
	
 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


methods.getFollowerGroupsBusiness = function(arrParams,return_result){

	var checkFav = `SELECT * FROM jaze_follower_groups WHERE account_id =? AND STATUS ='1'  ORDER BY id ASC`;

	var newObj = {};
	var arrRows = [];

	DB.query(checkFav,[arrParams.account_id], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){

					methods.getFollowersGroupCount(val.id,function(returned){	

						if(val.group_type == '1'){

							newObj = {
								id:val.id.toString(),
								group_name:val.group_name,
								followers:returned.toString()
							}

						}
					
						arrRows.push(newObj);

					});	

				   	const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
		   		return_result(arrRows);
			})

 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


methods.getFollowerGroupsSpecial = function(arrParams,return_result){

	var checkFav = `SELECT * FROM jaze_follower_groups WHERE account_id =? AND STATUS ='1' ORDER BY id ASC`;

	var newObj = {};
	var arrRows = [];

	DB.query(checkFav,[arrParams.account_id], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){

					methods.getFollowersGroupCount(val.id,function(retCount){	
						methods.getFollowerGroupSpecialDetails(val.id,function(retDetails){	

							if(val.group_type != '1'){

								newObj = {
									id:val.id.toString(),
									group_name:val.group_name,
									followers:retCount.toString(),
									company:retDetails
								}

								arrRows.push(newObj);
							}
					
						});	
					});	

				   	const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
		   		return_result(arrRows);
			})

 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


methods.getFollowerGroupSpecialDetails = function(arrParams,return_result){

	var quer = `SELECT t1.* FROM(
				SELECT group_id,
				(CASE WHEN meta_key = 'follower_account_id' THEN meta_value END) AS follower_account_id
				FROM  jaze_follower_groups_meta
				WHERE group_id IN (SELECT group_id FROM  jaze_follower_groups_meta  WHERE meta_key = 'follower_group_id' AND meta_value =? )
				) AS t1 WHERE follower_account_id != ""`;

	var newObj = {};
	var arrRows = [];

	DB.query(quer,[arrParams], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){

			  		var accParam = {account_id:val.follower_account_id};
					methods.getAccountTypes(accParam,function(returned){	

						newObj = {
							account_id:returned.account_id,
							name:returned.name,
							logo:returned.logo
						}

						arrRows.push(newObj);
				
				
					});	

				   	const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
		   		return_result(arrRows);
			})

 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


methods.getFollowersGroupCount = function(arrParams,return_result){


	var quer = `SELECT COUNT(id) AS total FROM jaze_follower_groups_meta WHERE meta_key = 'follower_group_id' AND meta_value =?`;

	DB.query(quer,[arrParams], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){     	       
          	return_result(data[0].total); 
        }else{ 
        	return_result(null);
        }

	});
};



methods.getFollowerList = function(arrParams,return_result){

	var checkFav = `SELECT * FROM jaze_follower_groups WHERE account_id =? AND STATUS ='1'  ORDER BY id ASC`;

	var newObj = {};
	var arrRows = [];

	DB.query(checkFav,[arrParams.account_id], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			data.forEach(function(row) {
				arrRows.push(row.id);

		  	});

			methods.getFollowersGroupDetails(arrRows,function(returned){	

				return_result(returned);

			});	
	 

 	   	}else{
 	   		return_result(null);
 	   	}

	});
};




methods.getFollowerList = function(arrParams,return_result){

	var checkFav = `SELECT * FROM jaze_follower_groups WHERE account_id =? AND STATUS ='1'  ORDER BY id ASC`;

	var newObj = {};
	var arrRows = [];

	DB.query(checkFav,[arrParams.account_id], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			data.forEach(function(row) {
				arrRows.push(row.id);

		  	});

			methods.getFollowersGroupDetails(arrRows,function(returned){	

				return_result(returned);

			});	
	 

 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


methods.getFollowersGroupDetails = function(arrParams,return_result){

	var quer = `SELECT t1.* FROM(
				SELECT group_id,
				(CASE WHEN meta_key = 'follower_account_id' THEN meta_value END) AS follower_account_id
				FROM  jaze_follower_groups_meta
				WHERE group_id IN (SELECT group_id FROM  jaze_follower_groups_meta  WHERE meta_key = 'follower_group_id' AND meta_value IN (?) )
				) AS t1 WHERE follower_account_id != ""`;

	var newObj = {};
	var arrRows = [];
	var arrAccountId = [];

	DB.query(quer,[arrParams], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){

					arrAccountId.push(val.follower_account_id);
			   		const result = await returnData(arrAccountId);
			  	}
			}

			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
	
				var uniqueArray = arrAccountId.filter(function(item, pos, self) {
			    	return self.indexOf(item) == pos;
				});

				methods.getAllFollowersData(uniqueArray, function(retputangina){	
		   			return_result(retputangina);
				});
			})

 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};


methods.getAllFollowersData = function(arrParams,return_result){


	var newObj = {};
	var arrRows = [];

	const forEach = async () => {
	  	const values = arrParams;
	  	for (const val of values){

	  		var gagoParams = {account_id:val};
	  		methods.getAccountTypes(gagoParams, function(retAccount){	

  				methods.getAllFollowersFollowingMeta(val,function(retFollowing){	

  					methods.getFollowersFollowingStatus(retFollowing,val,function(retStatus){

  						 methods.getFollowersFollowingDates(val,function(retDates){	

							newObj = {
								account_id:retAccount.account_id.toString(),
								name:retAccount.name,
								logo:retAccount.logo,
								date:retDates.dates,
								status:retStatus,
								sections:retFollowing
							};

							arrRows.push(newObj);

						});
					});
				});

			});

	   		const result = await returnData();
	  	}
	}


	const returnData = x => {
	  return new Promise((resolve, reject) => {
	    setTimeout(() => {
	      resolve(x);
	    }, 1000);
	  });
	}

	forEach().then(() => {
			return_result(arrRows);

	})

};


methods.getAllFollowersFollowingMeta = function(arrParams,return_result){

	var quer = `SELECT t1.* FROM(
				SELECT group_id,
				(CASE WHEN meta_key = 'follower_account_id' THEN meta_value END) AS follower_account_id,
				(SELECT c.meta_value FROM jaze_follower_groups_meta c WHERE c.meta_key = 'follower_group_id' AND c.group_id = jaze_follower_groups_meta.group_id) AS follower_group_id
				FROM  jaze_follower_groups_meta
				WHERE group_id IN (SELECT group_id FROM  jaze_follower_groups_meta  WHERE meta_key = 'follower_account_id' AND meta_value =? )
				) AS t1 WHERE follower_account_id != ""`;

	var newObj = {};
	var arrRows = [];
	var arrGroupIDS = [];


	DB.query(quer,[arrParams], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{


			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){

					arrGroupIDS.push(val.follower_group_id);
			   		const result = await returnData(arrGroupIDS);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 10);
			  });
			}
			 
			forEach().then(() => {
	   			methods.getAllFollowersFollowingGroups(arrGroupIDS,function(retGroups){	
					return_result(retGroups);
				});
			})

 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};



methods.getAllFollowersFollowingGroups = function(arrParams,return_result){


	var quer = `SELECT * FROM jaze_follower_groups WHERE id IN (?) AND STATUS = '1' ORDER BY id ASC`;

	var newObj = {};
	var arrRows = [];


	DB.query(quer,[arrParams], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){

					newObj = {
						id:val.id.toString(),
						name:val.group_name,
	
					}

					arrRows.push(newObj);

			   		const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 10);
			  });
			}
			 
			forEach().then(() => {
		   		return_result(arrRows);
			})

 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};


methods.getFollowersFollowingDates = function(arrParams,return_result){


	var quer = `SELECT t1.* FROM(
		SELECT group_id,
		(CASE WHEN meta_key = 'follower_account_id' THEN meta_value END) AS follower_account_id,
		(SELECT c.meta_value FROM jaze_follower_groups_meta c WHERE c.meta_key = 'joined_date' AND c.group_id = jaze_follower_groups_meta.group_id) AS joined_date
		FROM  jaze_follower_groups_meta
		WHERE group_id IN (SELECT group_id FROM  jaze_follower_groups_meta  WHERE meta_key = 'follower_account_id' AND meta_value =? )
		) AS t1 WHERE follower_account_id != "" ORDER BY group_id ASC`;

	var newObj = {};
	var arrRows = [];


	DB.query(quer,[arrParams], function(data, error){



		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){

					newObj = {
						group_id:val.group_id,
						dates:val.joined_date
					}

					arrRows.push(newObj);

			   		const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 10);
			  });
			}
			 
			forEach().then(() => {

				if(arrRows.length > 0 ){

					arrRows.sort(function (a, b) {
						    return a.group_id - b.group_id
					})
				}

				var min = arrRows[0];

		   		return_result(min);
			})


 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};


methods.getFollowersFollowingStatus = function(arrParams,account_id, return_result){

	var arrGroups = [];

	arrParams.forEach(function(row) {
		arrGroups.push(row.id);
  	});	

	var quer = `SELECT t1.* FROM(
		SELECT group_id,
		(CASE WHEN meta_key = 'follower_account_id' THEN meta_value END) AS follower_account_id,
		(SELECT c.meta_value FROM jaze_follower_groups_meta c WHERE c.meta_key = 'status' AND c.group_id = jaze_follower_groups_meta.group_id) AS status
		FROM  jaze_follower_groups_meta
		WHERE group_id IN (SELECT group_id FROM  jaze_follower_groups_meta  WHERE meta_key = 'follower_account_id' AND meta_value =? )
		AND  group_id IN (SELECT group_id FROM  jaze_follower_groups_meta  WHERE meta_key = 'follower_group_id' AND meta_value IN(?) )
		) AS t1 WHERE follower_account_id != "" ORDER BY group_id ASC`;

	var newObj = {};
	var arrRows = [];

	DB.query(quer,[account_id,arrGroups], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
					arrRows.push(val.status);
			   		const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 10);
			  });
			}
			 
			forEach().then(() => {

				var status = '1';
				if(arrRows.includes('2')){
					status = '2';
				}

		   		return_result(status);
			})

 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};



methods.getGroupById = function(arrParams, return_result){

	var quer = `SELECT * FROM jaze_follower_groups WHERE id =? AND STATUS = '1'`;

	var newObj = {};
	var arrRows = [];

	DB.query(quer,[arrParams.id], function(data, error){

		console.log(data);

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			return_result(data[0]);
 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};



methods.getGroupByIds = function(arrParams, return_result){

	var quer = `SELECT * FROM jaze_follower_groups WHERE id IN (?) AND account_id =? AND STATUS = '1'`;

	var newObj = {};
	var arrRows = [];

	DB.query(quer,[arrParams.arr_ids,arrParams.account_id], function(data, error){


		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
					arrRows.push(val.id);
			   		const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 50);
			  });
			}
			 
			forEach().then(() => {

		   		return_result(arrRows);
			})
 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};


methods.getOwnGroups = function(arrParams, return_result){

	var quer = `SELECT * FROM jaze_follower_groups WHERE account_id =? AND STATUS = '1'`;

	var newObj = {};
	var arrRows = [];

	DB.query(quer,[arrParams.account_id], function(data, error){


		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
					arrRows.push(val.id);
			   		const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 50);
			  });
			}
			 
			forEach().then(() => {

				methods.getOwnFollowers(arrRows,arrParams,function(retGroups){	
					return_result(retGroups);
				});
			})
 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};


methods.getOwnFollowers = function(arrGroupIDS,arrParams, return_result){


	var quer = `SELECT t1.* FROM(
		SELECT group_id,
		(CASE WHEN meta_key = 'follower_account_id' THEN meta_value END) AS follower_account_id
		FROM  jaze_follower_groups_meta
		WHERE group_id IN (SELECT group_id FROM  jaze_follower_groups_meta  WHERE meta_key = 'follower_group_id' AND meta_value IN (?) )
		AND group_id IN (SELECT group_id FROM  jaze_follower_groups_meta  WHERE meta_key = 'follower_account_id' AND meta_value =? )
		) AS t1 WHERE follower_account_id != ""`;

	var newObj = {};
	var arrRows = [];

	DB.query(quer,[arrGroupIDS,arrParams.follower_id], function(data, error){


		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
					arrRows.push(val.group_id);
			   		const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 50);
			  });
			}
			 
			forEach().then(() => {
		   		return_result(arrRows);
			})
 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};


methods.getOwnGroupsAndId = function(arrParams, return_result){

	var quer = `SELECT * FROM jaze_follower_groups WHERE account_id =? AND id IN(?) AND STATUS = '1'`;

	var newObj = {};
	var arrRows = [];

	DB.query(quer,[arrParams.account_id,arrParams.arr_ids], function(data, error){


		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
					arrRows.push(val.id);
			   		const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 50);
			  });
			}
			 
			forEach().then(() => {
				return_result(arrRows);
			})
 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};





methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
}

methods.insertResult = function(arrInsert,querY,grpId,count,return_result){

	var insertedGrpid;
	insertedGrpid=grpId;


	if(count > 1)
	{
	  	arrInsert.forEach(function(row,indx) {
	  		insertedGrpid += 1;

		 	for (var key in row) 
		 	{
				DB.query(querY,[insertedGrpid,key,row[key]], function(data, error){
		 		 	if(data == null){
			 	   		return_result(0);
			 	   	}else{
		   				return_result(data.affectedRows);
			 	   	}
		 	 	});
		  	};

		});
	}
	else
	{

	 	for (var key in arrInsert) 
	 	{
			DB.query(querY,[insertedGrpid,key,arrInsert[key]], function(data, error){
	 		 	if(data == null){
		 	   		return_result(0);
		 	   	}else{
	   				return_result(data.affectedRows);
		 	   	}
	 	 	});
	  	};

	}


}




methods.updateQuery = function(querY,arrUpdate,group_id,return_result){

	for (var key in arrUpdate) 
 	{	
 		DB.query(querY, [arrUpdate[key],key,group_id], function(data, error){
 			if(data == null){
	 	   		return_result(0);
	 	   	}else{
	  			return_result(data.affectedRows);
	 	   	}
		});
  	};
}


methods.deleteQuery = function(qry,arrDelete,return_result){

	DB.query(qry, [arrDelete], function(data, error){
 	   	if(data != null){
			return_result(data.affectedRows);
 	   	}else{
 	   		return_result(0);
 	   	}
	});
}



methods.updateGroup = function(arrParams, return_result){

	var updateQue = `UPDATE jaze_follower_groups SET group_name =? WHERE id =?`;

	return_result(1);
	DB.query(updateQue,[arrParams.group_name,arrParams.id], function(data, error){

		if(data == null){
 	   		return_result(0);
 	   	}

	});
};


methods.updateBlockUnblock = function(arrParams,arrGroupIDs, return_result){

	var updateQue = `UPDATE jaze_follower_groups_meta SET meta_value =? WHERE meta_key = 'status' AND group_id IN (?)`;

	return_result(1);
	DB.query(updateQue,[arrParams.status,arrGroupIDs], function(data, error){

		if(data == null){
 	   		return_result(0);
 	   	}

	});
};

methods.delGroup = function(arrParams, return_result){

	var updateQue = `DELETE FROM jaze_follower_groups WHERE id =?`;

	return_result(1);
	DB.query(updateQue,[arrParams.id], function(data, error){

		if(data == null){
 	   		return_result(0);
 	   	}

	});
};


methods.delGroupsMetaFollowers = function(arrParams, return_result){

	var updateQue = `DELETE FROM jaze_follower_groups_meta WHERE group_id IN(?)`;

	return_result(1);
	DB.query(updateQue,[arrParams], function(data, error){

		if(data == null){
 	   		return_result(0);
 	   	}

	});
};



module.exports = methods;