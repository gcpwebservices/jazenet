const methods = {};
const DB = require('./db_base.js');
const HelperRestriction = require('./HelperRestriction.js');
const HelperGeneral = require('../helpers/HelperGeneral.js');
const fs = require('fs');
const path = require('path');
var promiseForeach = require('promise-foreach');
const fast_sort = require("fast-sort");
var str_replace = require('str_replace');
var rtrim = require('rtrim');
var ltrim = require('ltrim');


methods.checkGeneralParametersApiToken = function(arrParams,return_result){
	try
	{
		if(arrParams.api_token === "jazenet")
		{
			var return_data = [];

			return_data.push(arrParams.account_id);

			return_result(return_data);
		}
		else
		{
			DB.query('select * from jaze_app_device_details where  status = 1 and account_id=? and api_token=?', [arrParams.account_id,arrParams.api_token], function(data, error){
				if(data != null){
					return_result(data);
				}else{
					return_result(null);
				}

			});
		}
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


 	/*
    * get Specific Account Details
    */
methods.getAccountDetails = function(arrParams, return_result){

		if(parseInt(arrParams.account_id) !== 0)
		{

			var queAccountDetails = " SELECT *  FROM jaze_user_basic_details WHERE  account_id = '" + arrParams.account_id + "' and account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key in ('status','team_status') and meta_value = '1') ";

					DB.query(queAccountDetails, null, function(data, error){

							if (typeof data[0] !== 'undefined' && data[0] !== null)
							{

								var list = [];

								Object.assign(list, {account_id : data[0].account_id});
								Object.assign(list, {account_type : data[0].account_type});

								for (var i in data)
								{
									Object.assign(list, {[data[i].meta_key]:  data[i].meta_value});
								}

								if(parseInt(list.account_type) === 4)
								{
									var accParam = {account_id: list.team_company_id};

									methods.getAccountDetails(accParam, function(compAccRres){

										methods.__getMobileCode(list, function(mobile_code){

											var newObj = {
												account_id	    : list.account_id.toString(),
												account_type    : list.account_type.toString(),
												name		    : list.team_fname+' '+list.team_lname,
												logo		    : G_STATIC_IMG_URL + 'uploads/jazenet/'+list.team_display_pic,
												category   		: compAccRres.name,
												title		    : "work",
												email 		 	: list.team_email,
												company_id 	  	: compAccRres.account_id,
												company_name 	: compAccRres.name,
												jazenet_id 	  	: compAccRres.jazenet_id,
												mobile_code   	: mobile_code.toString(),
												mobile_no     	: list.team_mobile_no,
												area_id		    : compAccRres.area_id,
												area_name		: compAccRres.area_name,
												city_id		    : compAccRres.city_id,
												city_name		: compAccRres.city_name,
												state_id		: compAccRres.state_id,
												state_name	 	: compAccRres.state_name,
												country_id		: compAccRres.country_id,
												country_name   	: compAccRres.country_name,
											};

											return_result(newObj);

										});
									});
								}
								else
								{
									methods.__getLocationDetails(list.area_id,list.city_id, function(location_details){

										methods.__getAccountEmailId(list, function(email_id){

											methods.__getAccountJazenetId(list.account_id,list.account_type, function(jazenet_id){

												methods.__getMobileCode(list, function(mobile_code){

													methods.__getCompanyCategoryName(list, function(category){

														var default_logo = '' + G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png'+'';

														var newObj = {};

														if(parseInt(list.account_type) === 1)
														{
															if(list.profile_logo !== "" && list.profile_logo !== null)
															{
																default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + list.profile_logo + '';
															}

															newObj = {
																account_id	  : list.account_id.toString(),
																account_type  : list.account_type.toString(),
																name		  : (list.fname +" "+list.lname).toString(),
																logo		  : default_logo.toString(),
																title	  	  : category.title_name.toString(),
																category	  : category.category_name.toString(),
																email 		  : email_id.toString(),
																jazenet_id 	  : jazenet_id.toString(),
																mobile_code   : mobile_code.toString(),
																mobile_no     : list.mobile.toString(),
																area_id		    : list.area_id,
																area_name		: location_details.area_name,
																city_id		    : list.city_id,
																city_name		: location_details.city_name,
																state_id		: list.state_id,
																state_name	 	: location_details.state_name,
																country_id		: list.country_id,
																country_name   	: location_details.country_name,
															};
														}
														else if(parseInt(list.account_type) === 2)
														{
															if(list.professional_logo !== "" && list.professional_logo !== null)
															{
																default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/professional_account/'+list.account_id+'/logo_thumb/thumb_' + list.professional_logo + '';
															}

															newObj = {
																account_id	  : list.account_id.toString(),
																account_type  : list.account_type.toString(),
																name		  : (list.fname +" "+list.lname).toString(),
																logo		  : default_logo.toString(),
																title	  	  : category.title_name.toString(),
																category	  : category.category_name.toString(),
																email 		  : email_id.toString(),
																jazenet_id 	  : jazenet_id.toString(),
																mobile_code   : mobile_code.toString(),
																mobile_no     : list.mobile.toString(),
																area_id		    : list.area_id,
																area_name		: location_details.area_name,
																city_id		    : list.city_id,
																city_name		: location_details.city_name,
																state_id		: list.state_id,
																state_name	 	: location_details.state_name,
																country_id		: list.country_id,
																country_name   	: location_details.country_name,
															};
														}
														else if(parseInt(list.account_type) === 3 || parseInt(list.account_type) === 5)
														{

															if(list.company_logo !== "" && list.company_logo !== null)
															{
																default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+list.account_id+'/logo/' + list.company_logo + '';
															}

															newObj = {
																account_id	  : list.account_id.toString(),
																account_type  : list.account_type.toString(),
																name		  : list.company_name.toString(),
																logo		  : default_logo.toString(),
																title	  	  : category.title_name.toString(),
																category	  : category.category_name.toString(),
																email 		  : email_id.toString(),
																jazenet_id 	  : jazenet_id.toString(),
																mobile_code   : mobile_code.toString(),
																mobile_no     : list.company_phone.toString(),
																area_id		    : list.area_id,
																area_name		: location_details.area_name,
																city_id		    : list.city_id,
																city_name		: location_details.city_name,
																state_id		: list.state_id,
																state_name	 	: location_details.state_name,
																country_id		: list.country_id,
																country_name   	: location_details.country_name,
															};
														}



														return_result(newObj);

													});
												});
											});
										});
									});
								}

							} else{
							return_result(null);
							}
					});
		}
		else{
			return_result(null);
		}
};

methods.getAccountTypeDetails = function(account_id, return_result){

	if(parseInt(account_id) !== 0)
	{
		//get Account type
		var queryAccountType = "SELECT `account_type` FROM `jaze_user_basic_details` WHERE `account_id` = '" + account_id + "' ";

		DB.query(queryAccountType, null, function(dataType, error){

			if (typeof dataType[0] !== 'undefined' && dataType[0] !== null)
			{
				return_result(parseInt(dataType[0].account_type));
			}
		});
	}
	else
	{ return_result(0); }
};

methods.getAccountBasicDetails = function(account_id, return_result){

	if(parseInt(account_id) !== 0)
	{
		var query = " SELECT *  FROM jaze_user_basic_details WHERE  account_id = '"+account_id+"' and meta_key in ('fname','lname','profile_logo','professional_logo','team_fname','team_lname','team_display_pic','company_name','company_logo') ";
			query += "and account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key in ('status','team_status') and meta_value = '1')";

				DB.query(query, null, function(data, error){

					if (typeof data[0] !== 'undefined' && data[0] !== null)
					{
						var default_logo = '' + G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png'+'';

						var list = [];

						Object.assign(list, {account_id : data[0].account_id});
						Object.assign(list, {account_type : data[0].account_type});

						for (var i in data)
						{
							Object.assign(list, {[data[i].meta_key]:  data[i].meta_value});
						}

						if(parseInt(list.account_type) === 1)
						{
							if(list.profile_logo !== "" && list.profile_logo !== null)
							{
								default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + list.profile_logo + '';
							}

							var newObj = {
									account_id	  : list.account_id.toString(),
									account_type  : list.account_type.toString(),
									name		  : (list.fname +" "+list.lname).toString(),
									logo		  : default_logo.toString(),
								};

								return_result(newObj);
						}
						else if(parseInt(list.account_type) === 2)
						{
							if(list.professional_logo !== "" && list.professional_logo !== null)
							{
								default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/professional_account/'+list.account_id+'/logo_thumb/thumb_' + list.professional_logo + '';
							}

							var newObj = {
									account_id	  : list.account_id.toString(),
									account_type  : list.account_type.toString(),
									name		  : (list.fname +" "+list.lname).toString(),
									logo		  : default_logo.toString(),
								};

								return_result(newObj);
						}
						else if(parseInt(list.account_type) === 4)
						{
							if(list.company_logo !== "" && list.company_logo !== null)
							{
								default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/'+list.team_display_pic + '';
							}

							var newObj = {
								account_id	  : list.account_id.toString(),
								account_type  : list.account_type.toString(),
								name		  : (list.team_fname+' '+list.team_lname).toString(),
								logo		  : default_logo.toString(),
							};

							return_result(newObj);
						}
						else if(parseInt(list.account_type) === 3 || parseInt(list.account_type) === 5)
						{
							if(list.company_logo !== "" && list.company_logo !== null)
							{
								default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+list.account_id+'/logo/' + list.company_logo + '';
							}

							var newObj = {
								account_id	  : list.account_id.toString(),
								account_type  : list.account_type.toString(),
								name		  : list.company_name.toString(),
								logo		  : default_logo.toString(),
							};

							return_result(newObj);
						}
						else{
							return_result(null);
						}
					}
					else{
						return_result(null);
					}
				});
	}
	else
	{ return_result(default_logo); }
};

methods.getAccountLogoDetails = function(account_id, return_result){

	var default_logo = '' + G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png'+'';

	if(parseInt(account_id) !== 0)
	{
		//get Account type
		var query = " SELECT *  FROM jaze_user_basic_details WHERE  account_id = '" + account_id + "' and meta_key in ('profile_logo','professional_logo','company_logo','team_display_pic') ";
			query += " and account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key = 'status' and meta_value = '1') ";

			DB.query(query, null, function(data, error){

				if (typeof data[0] !== 'undefined' && data[0] !== null)
				{

					if(parseInt(data[0].account_type) === 1)
					{
						if(data[0].meta_value !== "" && data[0].meta_value !== null)
						{
							default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + data[0].meta_value + '';
						}
					}
					else if(parseInt(data[0].account_type) === 2)
					{
						if(data[0].meta_value !== "" && data[0].meta_value !== null)
						{
							default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/professional_account/'+data[0].account_id+'/logo_thumb/thumb_' + data[0].meta_value + '';
						}
					}
					else if(parseInt(data[0].account_type) === 4)
					{
						if(data[0].meta_value !== "" && data[0].meta_value !== null)
						{
							default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/'+ data[0].meta_value + '';
						}
					}
					else if(parseInt(data[0].account_type) === 3 || parseInt(data[0].account_type) === 5)
					{
						if(data[0].meta_value !== "" && data[0].meta_value !== null)
						{
							default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+data[0].account_id+'/logo/' + data[0].meta_value + '';
						}
					}

					return_result(default_logo);
				}
				else
				{ return_result(default_logo); }
			});
	}
	else
	{ return_result(default_logo); }
};

methods.getAccountCategoryDetails = function(account_id, return_result){

	var return_obj = {
		title_name 	  : "",
		category_name : ""
	};

	try
	{
		if(parseInt(account_id) !== 0)
		{
			//get Account type
			var queryAccountType = "SELECT account_id,account_type,meta_key,meta_value from jaze_user_basic_details where meta_key in ('status','category_id') and   account_id =  '" + account_id + "' ";

			DB.query(queryAccountType, null, function(dataType, error){

				if (typeof dataType[0] !== 'undefined' && dataType[0] !== null)
				{
					methods.convertMetaArray(dataType, function(retrnResult){

						 methods.__getCompanyCategoryName(retrnResult, function(category){
							return_result(category);
						});
					});
				}
				else
				{ return_result(return_obj); }
			});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{ console.log("getAccountCategoryDetails",err); return_result(return_obj); }
};

methods.getApplications = function(arrParams,return_result){


	var arrApp = [];
	if(arrParams.flag == 1){
		subpath = 'jazechat/conversation';
	}else if(arrParams.flag == 2){
		subpath = 'jazechat/groups';
	}else if(arrParams.flag == 4){
		subpath = 'jazefeed';
	}

	var rootPathConv = G_STATIC_IMG_PATH+subpath+'/'+arrParams.conversation_id+'/';

	if(arrParams.item != ''){
		if(arrParams.item.includes('application')){

			arrParams.item.forEach(function (items) {

				if(items == 'application')
				{

					function asyncFunction (returnItem, cb) {
					  	setTimeout(() => {

							return_result(returnItem);
						cb();
					  	}, 100);
					}
					rootPathConv = path.join(G_STATIC_IMG_PATH, subpath+'/'+arrParams.conversation_id+'/'+items);

					fs.readdir(rootPathConv, function (err, files) {

						let requests = files.map((val, key, array) => {

							var ext = path.extname(rootPathConv+'/'+val);

							return new Promise((resolve) => {
								methods.getFileTypes(ext, function(reType){
									var objApp = {
				 		   				url: G_STATIC_IMG_URL+'uploads/jazenet/'+subpath+'/'+arrParams.conversation_id+'/'+items+'/'+val,
			 		   					ext: ext.replace('.',''),
	 		  							type: reType
				 		  			};
				 		  			arrApp.push(objApp);
						      		asyncFunction(arrApp, resolve);
								});
							});
						})

						Promise.all(requests);
					});
				}

			});

		}else{
			return_result(null);
		}
	}else{
		return_result(null);
	}
};




methods.getMedia = function(arrParams,return_result){


	var arrApp = [];
	var subpath = '';
	if(arrParams.flag == 1){
		subpath = 'jazechat/conversation';
	}else if(arrParams.flag == 2){
		subpath = 'jazechat/groups';
	}else if(arrParams.flag == 4){
		subpath = 'jazefeed';
	}

	if(arrParams.item != '')
	{
		var filteredAry = arrParams.item.filter(e => e !== 'application');

		var rootPathConv = G_STATIC_IMG_PATH+subpath+'/'+arrParams.conversation_id+'/';

		if(filteredAry != ''){

			filteredAry.forEach(function (items, index, arr) {

				function asyncFunction (returnItem, cb) {
				  	setTimeout(() => {
						return_result(returnItem);
					cb();
				  	}, 100);
				}

				rootPathConv = path.join(G_STATIC_IMG_PATH, subpath+'/'+arrParams.conversation_id+'/'+items);


				fs.readdir(rootPathConv, function (err, files) {


					if (typeof files !== 'undefined' && files !== null)
					{

						let requests = files.map((val, key, array) => {
							var ext = path.extname(rootPathConv+'/'+val);
							return new Promise((resolve) => {
								methods.getFileTypes(ext, function(reType){
									var objApp = {
										   url: G_STATIC_IMG_URL+'uploads/jazenet/'+subpath+'/'+arrParams.conversation_id+'/'+items+'/'+val,
										   ext: ext.replace('.',''),
										  type: reType
									   };
									   arrApp.push(objApp);
									  asyncFunction(arrApp, resolve);
								});
							});
						})
						Promise.all(requests);
					}
					else
					{	return_result(null);}

				});

			});
		}else{
			return_result(null);
		}
	}else{
		return_result(null);
	}
};


methods.getFileTypes = function(ext,return_result){

	var type;

	var arrImages = ['.jpeg','.jpg','.gif','.png','.tif'];
	var arrVideos = ['.mp4','.avi','.mov','.flv','.wmv'];
	var arrAudio = ['.pcm','.wav','.aiff','.mp3','.aac','.wma','.flac','.alac'];
	var arrDocs = ['.odt','.xls','.xlsx','.ods','.ppt','.pptx','.txt','.doc'];
	var arrPdf = ['.pdf'];

	if(arrImages.includes(ext.toLowerCase())){
		type = '2';
	}else if(arrAudio.includes(ext.toLowerCase())){
		type = '3';
	}else if(arrVideos.includes(ext.toLowerCase())){
		type = '4';
	}else if(arrDocs.includes(ext.toLowerCase())){
		type = '5';
	}else if(arrPdf.includes(ext.toLowerCase())){
		type = '6';
	}else{
		type = '7';
	}

	return_result(type);
};

//Get Mobile Code
methods.__getMobileCode= function (account_list,return_result){
	try
	{
		if(account_list !== null)
		{
			if(parseInt(account_list.account_type) === 1 || parseInt(account_list.account_type) === 2)
			{
				var query = " SELECT `phonecode` FROM `country` WHERE `id` = '"+account_list.mobile_country_code_id+"'; ";

				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						return_result("+"+data[0].phonecode);
					}
					else
					{ return_result(''); }
				});
			}
			else if(parseInt(account_list.account_type) === 3)
			{
				if(parseInt(account_list.company_phone_code) !== 0)
				{
					var query = " SELECT `phonecode` FROM `country` WHERE `id` = '"+account_list.company_phone_code+"'; ";

					DB.query(query, null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_result("+"+data[0].phonecode);
						}
						else
						{ return_result(''); }
					});
				}
				else
				{
					return_result('');
				}
			}
			else if(parseInt(account_list.account_type) === 4)
			{
				if(parseInt(account_list.team_mobile_code) !== 0)
				{
					var query = " SELECT `phonecode` FROM `country` WHERE `id` = '"+account_list.team_mobile_code+"'; ";

					DB.query(query, null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_result("+"+data[0].phonecode);
						}
						else
						{ return_result(''); }
					});
				}
				else
				{
					return_result('');
				}
			}
			else
			{
				return_result('');
			}

		}
		else
		{
			return_result('');
		}
	}
	catch(err)
	{ console.log("getMobileCode",err); return_result(''); }
};

//Get Email id
methods.__getAccountEmailId = function (account_list,return_result){
	try
	{
		if(parseInt(account_list.account_type) === 4)
		{
			var query = " SELECT meta_value FROM `jaze_user_basic_details` WHERE `meta_key` = 'team_email' and `account_id` = '"+account_list.account_id+"'  ";

			DB.query(query, null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result(data[0].meta_value);
				}
				else
				{ return_result(''); }
			});
		}
		else
		{
			var query = " SELECT `jazemail_id` FROM `jaze_user_credential_details` WHERE `account_id` = '"+account_list.account_id+"' and status = '1'; ";

			DB.query(query, null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result(data[0].jazemail_id);
				}
				else
				{ return_result(''); }
			});
		}
	}
	catch(err)
	{ console.log("getAccountEmailId",err); return_result(''); }
};

//Get Jazenet id
methods.__getAccountJazenetId = function (account_id,account_type,return_result){
	try
	{
		if(parseInt(account_id) !== 0 && parseInt(account_type) !== 5)
		{
			var query = " SELECT concat(`jazenet_id`,`unique_id`) as id FROM `jaze_user_jazenet_id` WHERE `account_id` = '"+account_id+"' and status = '1'; ";

			DB.query(query, null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result(data[0].id);
				}
				else
				{ return_result(''); }
			});
		}
		else
		{
			return_result('');
		}
	}
	catch(err)
	{ console.log("getAccountEmailId",err); return_result(''); }
};

//Get Company id
methods.__getCompanyCategoryName =  function (account_list,return_result){

	var return_obj = {
			account_type  : "",
			title_name 	  : "",
			category_name : ""
		};

	try
	{
		if(account_list !== null)
		{

			if(parseInt(account_list.account_type) === 1)
			{
				return_obj = {
					account_type  : "1",
					title_name 	  : "personal",
					category_name : "personal"
				};

				return_result(return_obj);
			}
			else if(parseInt(account_list.account_type) === 2)
			{
				var query = "  SELECT meta_value  FROM jaze_user_basic_details WHERE  meta_key = 'profession' and account_id = '"+account_list.account_id+"'; ";

					DB.query(query, null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_obj = {
								account_type  : "2",
								title_name 	  : "professional",
								category_name : data[0].meta_value
							};

							return_result(return_obj);
						}
						else
						{
							return_obj = {
								account_type  : "2",
								title_name 	  : "professional",
								category_name : "professional"
							};

							return_result(return_obj);
						}
					});
			}
			else if(parseInt(account_list.account_type) === 4)
			{
				var query = "  SELECT meta_value FROM `jaze_user_basic_details` WHERE `meta_key` = 'company_name' ";
					query += " and account_id in (SELECT company_id FROM `jaze_user_account_details` WHERE `user_id` = '"+account_list.account_id+"'  and status = '1') ";

					DB.query(query, null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_obj = {
								account_type  : "4",
								title_name 	  : "work",
								category_name : data[0].meta_value
							};

							return_result(return_obj);
						}
						else
						{
							return_obj = {
								account_type  : "4",
								title_name 	  : "work",
								category_name : "work"
							};

							return_result(return_obj);
						}
					});
			}
			else if(parseInt(account_list.account_type) === 3)
			{
				if((Object.keys(account_list)).indexOf("category_id") !== -1)
				{
					var query = "  SELECT meta_value  FROM jaze_category_list WHERE  meta_key = 'cate_name' and group_id = '"+account_list.category_id+"'; ";

					DB.query(query, null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_obj = {
								account_type  : "3",
								title_name 	  : "company",
								category_name : data[0].meta_value
							};

							return_result(return_obj);
						}
						else
						{
							return_obj = {
								account_type  : "3",
								title_name 	  : "company",
								category_name : "company"
							};

							return_result(return_obj);
						}
					});
				}
				else
				{
					var query = " SELECT meta_value  FROM jaze_category_list WHERE  meta_key = 'cate_name' and group_id in  ";
						query += " (SELECT meta_value FROM jaze_user_basic_details WHERE meta_key = 'category_id'  AND account_id = '"+account_list.account_id+"') ";

						DB.query(query, null, function(data, error){

							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
								return_obj = {
									account_type  : "3",
									title_name 	  : "company",
									category_name : data[0].meta_value
								};

								return_result(return_obj);
							}
							else
							{
								return_obj = {
									account_type  : "3",
									title_name 	  : "company",
									category_name : "company"
								};

								return_result(return_obj);
							}
						});
				}
			}
			else if(parseInt(account_list.account_type) === 5)
			{
				return_obj = {
					account_type  : "5",
					title_name 	  : "government",
					category_name : "government"
				};

				return_result(return_obj);
			}
		}
		else
		{
			return_result(return_obj);
		}
	}
	catch(err)
	{ console.log("getAccountEmailId",err); return_result(return_obj); }
};

//Get Company id
methods.getAccountCategoryName = function (account_id,return_result){

	var return_obj = {
			account_type  : "",
			title_name 	  : "",
			category_name : ""
		};

	try
	{
		if(parseInt(account_id) !== 0)
		{

			//get Account type

			var queryAccountType  = " SELECT * FROM jaze_user_basic_details WHERE meta_key IN ('category_id','fname','company_name') ";
				queryAccountType += " and account_id IN (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status' AND meta_value = '1' and account_id = '" + account_id + "' ) ";

			DB.query(queryAccountType, null, function(dataType, error){

				if (typeof dataType[0] !== 'undefined' && dataType[0] !== null)
				{
					var data_result  = dataType[0];

					if(parseInt(data_result.account_type) === 1)
					{
						return_obj = {
							account_type  : data_result.account_type.toString(),
							title_name 	  : "personal",
							category_name : "personal"
						};

						return_result(return_obj);
					}
					else if(parseInt(data_result.account_type) === 2)
					{
						return_obj = {
							account_type  : data_result.account_type.toString(),
							title_name 	  : "professional",
							category_name : "professional"
						};

						return_result(return_obj);
					}
					else if(parseInt(data_result.account_type) === 3)
					{

						if((Object.keys(data_result)).indexOf("category_id") !== -1)
						{
							var query = "  SELECT meta_value  FROM jaze_category_list WHERE  meta_key = 'cate_name' and group_id = '"+data_result.category_id+"'; ";

							DB.query(query, null, function(data, error){

								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
									return_obj = {
										account_type  : data_result.account_type.toString(),
										title_name 	  : "company",
										category_name : data[0].meta_value
									};

									return_result(return_obj);
								}
							});
						}
						else
						{
							return_obj = {
								account_type  : data_result.account_type.toString(),
								title_name 	  : "company",
								category_name : "company"
							};

							return_result(return_obj);
						}
					}
				}
			});
		}
		else
		{
			return_result(return_obj);
		}
	}
	catch(err)
	{ console.log("getAccountCategoryName",err); return_result(return_obj); }
};



// ------------------------------------------------------ General Search Module Start  --------------------------------------------------------------//

/*
 *  Company  Module Start
 */
methods.getAutofillCompanyResults = function(arrParams,return_result)
{
	var return_obj = {
		result : null,
		tot_count : "0",
		tot_page : "0"
	};

	var company_id_list = "";

	try
	{
		if(arrParams !== null)
		{
			var query  = " SELECT account_id,account_type FROM jaze_user_basic_details WHERE  ( account_type = 3 or account_type = 5)  ";

				if(arrParams.block_accounts !== '')
				{
					query += " and account_id not in ("+arrParams.block_accounts+") " ;
				}

				query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";
				query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('company_name')  AND LOWER(meta_value) like '%"+arrParams.keyword.toLowerCase()+"%') ";
				query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
				query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state+"') ";
				query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";
				query += " ) ";
				query += " group by account_id  ";

				DB.query(query, null, function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{
						data.forEach(function(row){
							company_id_list += (company_id_list  == "") ? row.account_id: ','+row.account_id;
						});

						var tot_count = data.length;

						if(parseInt(arrParams.result_flag) === 1)
						{
							var data_result;

							if(parseInt(arrParams.limit) !== 0)
							{
								data_result = data.splice(parseInt(arrParams.limit) * 30, 30);
							}
							else
							{
								data_result = data.splice(0, 30);
							}

							var counter = data_result.length;

							var array_rows = [];

								if(parseInt(counter) !== 0)
								{
										promiseForeach.each(data_result,[function (list) {

											return new Promise(function (resolve, reject) {

												methods.__getAccountDetails(arrParams,list, function(comp_details){

													if(comp_details !== null)
													{
														array_rows.push(comp_details);
													}

													counter -= 1;

													resolve(array_rows);
												});
											});

										}],
										function (result, current) {

											if (counter === 0)
											{
												var obj = {
														result 	  : result[0],
														tot_count : tot_count.toString(),
														tot_page  : Math.ceil(parseInt(tot_count) / 30)
												}

												return_result(obj,company_id_list);

											}
										});
								}
								else
								{ return_result(return_obj,company_id_list); }
						}
						else
						{
							return_obj = {
								result : null,
								tot_count : data.length.toString(),
								tot_page : "0"
							};

							return_result(return_obj,company_id_list);
						}
					}
					else
					{ return_result(return_obj,company_id_list); }
				});
		}
		else
		{ return_result(return_obj,company_id_list); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj,company_id_list);
	}
}

/*
 * Profile  Module Start
 */
methods.getAutofillProfileResults = function(arrParams,company_id_list,return_result)
{
	var return_obj = {
		result : null,
		tot_count : "0",
		tot_page : "0"
	};

	try
	{
		if(arrParams !== null)
		{
			var query  = "";

				if(parseInt(arrParams.flag) !== 7 && parseInt(arrParams.flag) !== 8)
				{
					query += " SELECT account_id,account_type FROM jaze_user_basic_details WHERE  account_type = 2  ";

					if(arrParams.block_accounts !== '')
					{
						query += " and account_id not in ("+arrParams.block_accounts+") " ;
					}

					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('fname','lname')  AND LOWER(meta_value) like '%"+rtrim(ltrim(arrParams.keyword.toLowerCase()))+"%' ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state+"') ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";
					query += " )) ";
					query += " group by account_id  ";

					query += " union  ";

				}
					query += " SELECT account_id,account_type FROM jaze_user_basic_details WHERE account_type = 4 ";
					if(arrParams.block_accounts !== '')
					{
						query += " and account_id not in ("+arrParams.block_accounts+") " ;
					}
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'team_status'  AND meta_value = '1' ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'team_company_id'  AND meta_value in ";
					query += " (SELECT account_id FROM jaze_user_basic_details WHERE  (account_type = 3 or account_type = 5) ";
					if(arrParams.block_accounts !== '')
					{
						query += " and account_id not in ("+arrParams.block_accounts+") " ;
					}
					query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
					query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state+"') ";
					query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"')";
					query += " ))";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('team_fname','team_lname')  AND LOWER(meta_value) like '%"+rtrim(ltrim(arrParams.keyword.toLowerCase()))+"%' ";
					query += " )) group by account_id;";




					// query += " SELECT account_id,account_type FROM jaze_user_basic_details WHERE account_type = 4  ";

					// if(arrParams.block_accounts !== '')
					// {
					// 	query += " and account_id not in ("+arrParams.block_accounts+") " ;
					// }
					// query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'team_status'  AND meta_value = '1' ";

					// if(company_id_list !== "")
					//  { query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'team_company_id'  AND meta_value in ("+company_id_list+")) "; }

					// query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('team_fname','team_lname')  AND LOWER(meta_value) like '%"+rtrim(ltrim(arrParams.keyword.toLowerCase()))+"%' ";
					// query += " )) ";
					// query += " group by account_id  ";

					DB.query(query, null, function (data, error) {

						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
						{
							var tot_count = data.length;

							if(parseInt(arrParams.result_flag) === 2)
							{
								var data_result;

								if(parseInt(arrParams.limit) !== 0)
								{
									data_result = data.splice(parseInt(arrParams.limit) * 30, 30);
								}
								else
								{
									data_result = data.splice(0, 30);
								}

								var counter = data_result.length;

								var array_rows = [];

								if(parseInt(counter) !== 0)
								{
										promiseForeach.each(data_result,[function (list) {

											return new Promise(function (resolve, reject) {

												methods.__getAccountDetails(arrParams,list, function(user_details){

													if(user_details !== null)
													{
														array_rows.push(user_details);
													}

													counter -= 1;

													resolve(array_rows);
												});
											});
										}],
										function (result, current) {

											if (counter === 0)
											{
												var obj = {
													result : result[0],
													tot_count : tot_count.toString(),
													tot_page  : Math.ceil(parseInt(tot_count) / 30)
												}

												return_result(obj);
											}
										});
								}
								else
								{ return_result(return_obj); }
							}
							else
							{
								return_obj = {
									result : null,
									tot_count : data.length.toString(),
									tot_page : "0"
								};

								return_result(return_obj,company_id_list);
							}
						}
						else
						{ return_result(return_obj); }
					});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

/*
 * User  Module Start
 */
methods.getAutofillUserResults = function(arrParams,return_result)
{
	var return_obj = {
		result : null,
		tot_count : "0",
		tot_page : "0"
	};

	try
	{
		if(arrParams !== null)
		{
			var query  = " SELECT account_id,account_type FROM jaze_user_basic_details WHERE  account_type = 1  ";

			if(arrParams.block_accounts !== '')
			{
				query += " and account_id not in ("+arrParams.block_accounts+") " ;
			}

			query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";

			query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('fname','lname')  AND LOWER(meta_value) like '%"+arrParams.keyword.toLowerCase()+"%' ";

			query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
			query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state+"') ";
			query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";

			query += " )) ";

			query += " group by account_id  ";


			DB.query(query,null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					if((parseInt(arrParams.account_type) !== 1 && parseInt(arrParams.result_flag) !== 1))
					{
						var data_result;

						var tot_count = data.length;

						if(parseInt(arrParams.limit) !== 0)
						{
							data_result = data.splice(parseInt(arrParams.limit) * 30, 30);
						}
						else
						{
							data_result = data.splice(0, 30);
						}

						var counter = data_result.length;

						var array_rows = [];

						if(parseInt(counter) !== 0)
						{
								promiseForeach.each(data_result,[function (list) {

									return new Promise(function (resolve, reject) {

										methods.__getAccountDetails(arrParams,list, function(user_details){

											if(user_details !== null)
											{
												array_rows.push(user_details);
											}

											counter -= 1;

											resolve(array_rows);
										});
									});
								}],
								function (result, current) {

									if (counter === 0)
									{
										var obj = {
											result : result[0],
											tot_count : tot_count.toString(),
											tot_page  : Math.ceil(parseInt(tot_count) / 30)
										}

										return_result(obj);
									}
								});
						}
						else
						{ return_result(return_obj); }
					}
					else
					{
						return_obj = {
							result : null,
							tot_count : data.length.toString(),
							tot_page : "0"
						};
						return_result(return_obj);
					}
				}
				else
				{ return_result(return_obj); }
			});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}


/*
 * General  Module
 */
methods.__getAccountDetails = function(arrParams,list,return_result)
{
	try
	{
		if(arrParams !== null)
		{
			methods.getAccountBasicDetails(list.account_id, function(user_details){

				methods.__getCompanyCategoryName(list, function(category){

					methods.__getAccountEmailId(list, function(email_id){

						methods.___getJazecomAccountStatus(arrParams.flag,list, function(module_status){

								var chat_request_status = "0";
								var mail_request_status = "0";
								var diary_request_status = "0";

								if(arrParams.request_status !== null)
								{
									if(parseInt(arrParams.flag) === 1 || parseInt(arrParams.flag) === 7)
									{
										if(arrParams.request_status.chat_account_list.indexOf(list.account_id.toString()) != -1 ){
											chat_request_status	 = "1";
										}
									}
								}

								methods.__getJazechatStatus(arrParams.flag,chat_request_status,arrParams.account_id,list.account_id, function(chatRequestStatus){

									var newObj = {
											account_id	  		: list.account_id.toString(),
											account_type  		: list.account_type.toString(),
											name		  		: user_details.name.toString(),
											logo		  		: user_details.logo.toString(),
											email		  		: email_id.toString(),
											title	  	  		: category.title_name,
											category	  		: category.category_name,
											chat_request_status : chatRequestStatus,
											mail_request_status : mail_request_status,
											diary_request_status : diary_request_status,
											module_status 		: module_status.toString()
										};

									return_result(newObj);

								});
							});
						});
					});
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log('__getAccountDetails',err);
		return_result('');
	}
}

methods.___getCompanyAccountArray = function(arrParams,return_result)
{
	try
	{
		if(arrParams !== null)
		{

			var query  = " SELECT account_id,account_type FROM jaze_user_basic_details WHERE  ( account_type = 3 or account_type = 5)  ";

				if(arrParams.block_accounts !== '')
				{
					query += " and account_id not in ("+arrParams.block_accounts+") " ;
				}

				query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";
				query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
				query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state+"') ";
				query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";
				query += " ) ";
				query += " group by account_id  ";

				DB.query(query, null, function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{
						var company_id_list = "";

						data.forEach(function(row){
							company_id_list += (company_id_list  == "") ? row.account_id: ','+row.account_id;
						});

						return_result(company_id_list);
					}
					else
					{ return_result(''); }
				});
		}
		else
		{ return_result(''); }
	}
	catch(err)
	{ console.log("___getJazecomAccountStatus",err); return_result('1'); }
}

methods.getBlockAccountIdList = function(arrParams,return_result)
{
	try
	{
		if(arrParams !== null)
		{
			var condit_query = "";

			if(parseInt(arrParams.flag) === 1)
			{
				//chat
				condit_query = " and meta_key IN ('chat_account_id','chat_blocked_account_id') ";
			}
			else if(parseInt(arrParams.flag) === 2)
			{
				//mail
				condit_query = " and meta_key IN ('mail_blocked_account_id') ";
			}
			else if(parseInt(arrParams.flag) === 3)
			{
				//call
				condit_query = " and meta_key IN ('call_blocked_account_id') ";
			}
			else if(parseInt(arrParams.flag) === 4)
			{
				//feed
				condit_query = " and meta_key IN ('feed_blocked_account_id') ";
			}
			else if(parseInt(arrParams.flag) === 5)
			{
				//diary
				condit_query = " and meta_key IN ('diary_blocked_account_id') ";
			}

			if(condit_query !== "")
			{
				var query = " SELECT group_concat(meta_value) as account FROM jaze_jazecom_contact_list WHERE meta_value != ''  ";
					query += condit_query;
					query += " and  group_id IN  (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id'  AND meta_value = '"+arrParams.account_id+"' ) ";

				DB.query(query, null, function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0) {

						if (typeof data[0].account !== 'undefined' && data[0].account !== null && data[0].account !== "")
						{
							return_result(data[0].account);
						}
						else
						{ return_result(arrParams.account_id); }
					}
					else
					{ return_result(arrParams.account_id); }
				});
			}
			else
			{ return_result(arrParams.account_id); }
		}
		else
		{ return_result(arrParams.account_id); }
	}
	catch(err)
	{
		console.log('getBlockAccountIdList',err);
		return_result('');
	}
}

methods.___getJazecomAccountStatus= function(flag,arrParams,return_result)
{
	try
	{
		if(arrParams !== null &&  parseInt(flag) !== 0)
		{
			if(parseInt(flag) === 6)
			{
				return_result('1');
			}
			else
			{
				if(parseInt(arrParams.account_type) === 4)
				{
					var meta_key = { '1' : 'team_chat_status','2' : 'team_mail_status','3' : 'team_call_status', '4' : 'team_feed_status', '5' : 'team_diary_status','7' : 'team_chat_status','8' : 'team_mail_status'};

					var query  = " SELECT meta_value FROM jaze_user_basic_details WHERE meta_key in ('"+meta_key[flag]+"') and  `account_id` IN (SELECT `account_id`  FROM jaze_user_basic_details WHERE `meta_key` = 'team_status' AND meta_value = '1' ) ";
						query += " and account_id = '"+arrParams.account_id+"' and account_type = '4' ";

						DB.query(query, null, function(data, error){

							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

								return_result(data[0].meta_value);
							}
							else
							{ return_result('0'); }
						});
				}
				else
				{
					var meta_key = { '1' : 'jaze_chat','2' : 'jaze_mail','3' : 'jaze_call', '4' : 'jaze_feed', '5' : 'jaze_diary','7' : 'jaze_chat','8' : 'jaze_mail'};

					var query = "SELECT meta_value FROM jaze_user_jazecome_status WHERE  meta_key in ('"+meta_key[flag]+"') and `group_id` IN (SELECT `group_id`  FROM jaze_user_jazecome_status WHERE `meta_key` = 'account_id' AND meta_value = '"+arrParams.account_id+"' ) ";

					DB.query(query, null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							return_result(data[0].meta_value);
						}
						else
						{ return_result('0'); }
					});
				}
			}
		}
		else
		{ return_result('1'); }
	}
	catch(err)
	{ console.log("___getJazecomAccountStatus",err); return_result('1'); }
}

methods.getAccountContactList= function(arrParams,return_result)
{
	try
	{
		if(arrParams !== null)
		{
			var request_status = {
					chat_account_list : null,
					mail_account_list : null,
					diary_account_list : null
				};

			if(parseInt(arrParams.flag) === 1 || parseInt(arrParams.flag) === 7)
			{
				//Chat
				var query  = " SELECT group_concat(meta_value) as account FROM jaze_jazechat_conversation_list WHERE  meta_value != '' and  meta_key IN ('account_id','contact_account_id') ";
					query += " and  group_id IN  (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'status'  AND meta_value = '0' ";
					query += " and  group_id IN  (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key in ('contact_account_id','account_id')  AND meta_value = '"+arrParams.account_id+"' )) ";

					DB.query(query, null, function (data, error) {

						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0) {

							request_status = {
								chat_account_list : convert_array(data[0].account),
								mail_account_list : "",
								diary_account_list : "",
							};

							return_result(request_status);
						}
						else
						{ return_result(request_status); }
					});
			}
			else if(parseInt(arrParams.flag) === 2 || parseInt(arrParams.flag) === 8)
			{
				// mail

				return_result(request_status);

				// var query = " SELECT group_concat(meta_value) as account FROM jaze_jazecom_contact_list WHERE meta_value != ''  ";
				// 	query += " and meta_key IN ('mail_account_id') ";
				// 	query += " and  group_id IN  (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id'  AND meta_value = '"+arrParams.account_id+"' ) ";

				// DB.query(query, null, function (data, error) {

				// 	if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0) {

				// 		if (typeof data[0].account !== 'undefined' && data[0].account !== null && data[0].account !== "")
				// 		{
				// 			request_status = {
				// 				chat_account_list : "",
				// 				mail_account_list : convert_array(data[0].account),
				// 				diary_account_list : "",
				// 			};

				// 			return_result(request_status);
				// 		}
				// 		else
				// 		{ return_result(request_status); }
				// 	}
				// 	else
				// 	{ return_result(request_status); }
				// });
			}
			else if(parseInt(arrParams.flag) === 3)
			{
				//call
				return_result(request_status);
			}
			else if(parseInt(arrParams.flag) === 4)
			{
				// feed
				return_result(request_status);
			}
			else if(parseInt(arrParams.flag) === 5)
			{
				// dairy

				return_result(request_status);

				// var query = " SELECT group_concat(meta_value) as account FROM jaze_jazecom_contact_list WHERE meta_value != ''  ";
				// 	query += " and meta_key IN ('diary_account_id') ";
				// 	query += " and  group_id IN  (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id'  AND meta_value = '"+arrParams.account_id+"' ) ";

				// DB.query(query, null, function (data, error) {

				// 	if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0) {

				// 		if (typeof data[0].account !== 'undefined' && data[0].account !== null && data[0].account !== "")
				// 		{
				// 			request_status = {
				// 				chat_account_list : "",
				// 				mail_account_list : "",
				// 				diary_account_list : convert_array(data[0].account),
				// 			};

				// 			return_result(request_status);
				// 		}
				// 		else
				// 		{ return_result(request_status); }
				// 	}
				// 	else
				// 	{ return_result(request_status); }
				// });
			}
			else if(parseInt(arrParams.flag) === 6)
			{
				return_result(request_status);
			}

		}
		else
		{ return_result(request_status); }
	}
	catch(err)
	{ console.log("getAccountContactList",err); return_result(request_status); }
}

methods.__getJazechatStatus= function(flag,flag_status,account_id,list_account_id,return_result)
{
	try
	{
		if(parseInt(flag) === 7 && parseInt(account_id) !== 0 && parseInt(list_account_id) !== 0)
		{
			var query  =" SELECT meta_value FROM jaze_jazechat_conversation_list WHERE meta_key in ('conversa_id') ";
				query += " and  group_id IN  (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'status'  AND meta_value = '1' and";
				query +=" (group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'contact_account_id' AND meta_value ='"+account_id+"' and group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'account_id' AND meta_value ='"+list_account_id+"')) ";
				query +=" or ";
				query +=" group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'contact_account_id' AND meta_value ='"+list_account_id+"' and group_id IN (SELECT group_id FROM  jaze_jazechat_conversation_list  WHERE meta_key = 'account_id' AND meta_value ='"+account_id+"'))";
				query +=")) ";

				DB.query(query, null, function(data, error){

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0) {

						if(parseInt(data[0].meta_value) !== 0)
						{ return_result('2'); }
						else
						{ return_result(flag_status); }
					}
					else
					{ return_result(flag_status); }
				});
		}
		else
		{ return_result(flag_status); }
	}
	catch(err)
	{ console.log("__getJazechatStatus",err); return_result(flag_status); }
}

/*
	 * split values push to array
	 */
	function convert_array(array_val) {

		var list = [];

		if(typeof array_val !== 'undefined' && array_val !== null && array_val !== "")
		{
			if(array_val.toString().indexOf(',') != -1 ){
				list = array_val.split(',');
			}
			else {
				list.push(array_val);
			}
		}

		return list;
	}

// ------------------------------------------------------ General Search Module End  --------------------------------------------------------------//


// ------------------------------------------------------ Geo Search Module Start  --------------------------------------------------------------//

methods.getAutofillCountryResults = function(arrParams,count,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(count) === 2 && (parseInt(arrParams.more_result_flag) === 0 || parseInt(arrParams.more_result_flag) === 1))
		{

				var query = " SELECT *,(select app_img_url from geo_bg_img_details where geo_bg_img_details.flag = 1 and geo_bg_img_details.status = 1 and geo_bg_img_details.geo_id = country.id ) as country_bg_image ";
					query += " FROM country WHERE status = 1 AND country_name != ''  ";

					if(parseInt(arrParams.country) !== 0)
					{
						query += " and  id = '"+arrParams.country+"' ";
					}

					if(parseInt(arrParams.flag) === 4)
					{
						if(arrParams.country_key != "" && arrParams.country_code != "")
						{
							query += " AND  (LOWER(country_name) REGEXP '[[:<:]]" +arrParams.country_key.toLowerCase()+ "' or LOWER(country_short_code) = '" +arrParams.country_code.toLowerCase()+ "') ";
						}
						else if(arrParams.country_key != "")
						{
							query += " AND  (LOWER(country_name) REGEXP '[[:<:]]" +arrParams.country_key.toLowerCase()+ "') ";
						}
						else if(arrParams.country_code != "")
						{
							query += " AND  (LOWER(country_short_code) = '" +arrParams.country_code.toLowerCase()+ "') ";
						}
						else
						{ return_result(null); }
					}
					else if(parseInt(arrParams.flag) === 5)
					{
						query += "  ORDER BY  country_name asc  ";
					}
					else
					{
						if(arrParams.keyword != '')
						{
							query += " AND   LOWER(country_name) REGEXP '[[:<:]]" +arrParams.keyword.toLowerCase()+ "' ";

							query += "  ORDER BY '" +arrParams.keyword+  "', country_name DESC  ";
						}
						else
						{
							query += "  ORDER BY  country_name DESC  ";
						}
					}




					if(parseInt(arrParams.flag) === 1)
					{
						if(parseInt(arrParams.limit) === 0)
						{
							query += "  LIMIT  0 , 50 ";
						}
						else
						{
							query += "  LIMIT  "+ parseInt(arrParams.limit) * 50 +" , 50 ";
						}
					}
					else if(parseInt(arrParams.flag) === 4)
					{
							query += "  LIMIT  1";
					}
					else if(parseInt(arrParams.flag) === 5)
					{ }
					else
					{
						if(parseInt(arrParams.limit) === 0)
						{
							query += "  LIMIT  0 , 5 ";
						}
						else if(parseInt(arrParams.limit) === 1)
						{
							query += "  LIMIT  5 , 50 ";
						}
						else
						{
							var start = parseInt(arrParams.limit - 1) * 50;

							var end = parseInt(arrParams.limit) * 50;

							query += "  LIMIT  "+start+" , "+end;
						}
					}


					DB.query(query, null, function (data, error) {

						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
						{
							var counter = data.length;

							var array_rows = [];

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data,[function (list) {

									return new Promise(function (resolve, reject) {

										var newObj = { };

											var country_bg_img = "";

											if(list.country_bg_image !== "" && list.country_bg_image !== null)
											{ country_bg_img = G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/'+list.country_bg_image.toString(); }


										if(parseInt(arrParams.flag) === 5)
										{
											var country_short_code = "";

											if(list.country_short_code !== "" && list.country_short_code !== null)
											{ country_short_code = list.country_short_code.toString(); }

											var phonecode = "";

											if(list.phonecode !== "" && list.phonecode !== null)
											{ phonecode = "+"+list.phonecode.toString(); }

											newObj = {
												country_id	  		: list.id.toString(),
												country_name  		: list.country_name.toString(),
												country_bg_img  : country_bg_img,
												country_short_code	: country_short_code,
												country_phonecode   : phonecode,
												state_id	  		: "",
												state_name    		: "",
												city_id	  			: "",
												city_name    		: "",
												area_id	  			: "",
												area_name    		: "",
												result_flag    		: "1"

											};
										}
										else
										{
											newObj = {
												country_id	  	: list.id.toString(),
												country_name  	: list.country_name.toString(),
												country_bg_img  : country_bg_img,
												state_id	  	: "",
												state_name    	: "",
												city_id	  		: "",
												city_name    	: "",
												city_bg_img  	: "",
												area_id	  		: "",
												area_name    	: "",
												result_flag    	: "1"

											};
										}

										array_rows.push(newObj);

										counter -= 1;

										resolve(array_rows);
									})
								}],
								function (result, current) {

									if (counter === 0)
									{
										if(parseInt(arrParams.flag) === 4 || parseInt(arrParams.flag) === 5)
										{
											return_result(result[0]);
										}
										else
										{
											methods.getCountCountryResults(arrParams, function(returnTotalCount){

												var obj = {
													result : result[0],
													tot_count : returnTotalCount.toString(),
													tot_page  : Math.ceil((parseInt(returnTotalCount) - 5) / 50)
												}
												return_result(obj);
											});
										}
									}
								});
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }
					});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

methods.getCountCountryResults = function(arrParams,return_result){

	try{
			var query = " SELECT id FROM country WHERE status = 1 AND country_name != ''  ";
				query += " AND   LOWER(country_name) REGEXP '[[:<:]]" +arrParams.keyword.toLowerCase()+ "' ";

				if(parseInt(arrParams.country) !== 0)
				{
					query += " and  id = '"+arrParams.country+"') ";
				}

			DB.query(query,null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data.length);
				}
				else
				{return_result(0)}
			});
	}
	catch(err)
	{
		return_result(null)
	}
};

methods.getAutofillStateResults = function(arrParams,count,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(count) === 3 && (parseInt(arrParams.more_result_flag) === 0 || parseInt(arrParams.more_result_flag) === 2))
		{

				var query  = " SELECT  con.id as country_id,con.country_name,con.country_short_code,con.phonecode,st.id as state_id, st.state_name , ";
					query += " (select app_img_url from geo_bg_img_details where geo_bg_img_details.flag = 1 and geo_bg_img_details.status = 1 and geo_bg_img_details.geo_id = con.id ) as country_bg_image  ";
					query += " FROM states st  ";
					query += " INNER JOIN country con ON con.id = st.country_id  ";
					query += " WHERE st.status = 1 AND con.status = 1 ";


					if(parseInt(arrParams.country) !== 0)
					{
						query += " and  con.id = '"+arrParams.country+"' ";
					}

					if(parseInt(arrParams.state) !== 0)
					{
						query += " and  st.id = '"+arrParams.state+"' ";
					}

					if(parseInt(arrParams.flag) === 4)
					{
						if(arrParams.state_key != "")
						{
							query += " AND  (LOWER(st.state_name) REGEXP '[[:<:]]" +arrParams.state_key.toLowerCase()+ "') ";
						}
						else
						{ return_result(null); }
					}
					else if(parseInt(arrParams.flag) === 5)
					{
						query += "  ORDER BY  st.state_name asc  ";
					}
					else
					{
						if(arrParams.keyword != '')
						{
							query += " AND   LOWER(st.state_name) REGEXP '[[:<:]]" +arrParams.keyword.toLowerCase()+ "' ";

							query += "  ORDER BY '" +arrParams.keyword+  "', st.state_name DESC  ";
						}
						else
						{
							query += "  ORDER BY  st.state_name DESC  ";
						}
					}

					if(parseInt(arrParams.flag) === 2)
					{
						if(parseInt(arrParams.limit) === 0)
						{
							query += "  LIMIT  0 , 50 ";
						}
						else
						{
							query += "  LIMIT  "+ parseInt(arrParams.limit) * 50 +" , 50 ";
						}
					}
					else if(parseInt(arrParams.flag) === 4)
					{
							query += "  LIMIT  1";
					}
					else if(parseInt(arrParams.flag) === 5)
					{ }
					else
					{
						if(parseInt(arrParams.limit) === 0)
						{
							query += "  LIMIT  0 , 5 ";
						}
						else if(parseInt(arrParams.limit) === 1)
						{
							query += "  LIMIT  5 , 50 ";
						}
						else
						{
							var start = parseInt(arrParams.limit - 1) * 50;

							var end = parseInt(arrParams.limit) * 50;

							query += "  LIMIT  "+start+" , "+end;
						}
					}

					DB.query(query, null, function (data, error) {

						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
						{
							var counter = data.length;

							var array_rows = [];

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data,[function (list) {

									return new Promise(function (resolve, reject) {

										var newObj = {};

										var country_bg_img = "";

										if(list.country_bg_image !== "" && list.country_bg_image !== null)
										{ country_bg_img =  G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/'+list.country_bg_image.toString(); }

										if(parseInt(arrParams.flag) === 5)
										{
											var country_short_code = "";

											if(list.country_short_code !== "" && list.country_short_code !== null)
											{ country_short_code = list.country_short_code.toString(); }

											var phonecode = "";

											if(list.phonecode !== "" && list.phonecode !== null)
											{ phonecode = "+"+list.phonecode.toString(); }

											newObj = {
												country_id	  		: list.country_id.toString(),
												country_name  		: list.country_name.toString(),
												country_bg_img  	: country_bg_img,
												country_short_code	: country_short_code,
												country_phonecode   : phonecode,
												state_id	  		: list.state_id.toString(),
												state_name    		: list.state_name.toString(),
												city_id				: "",
												city_name    		: "",
												area_id	  			: "",
												area_name    		: "",
												result_flag    		: "2"
											};
										}
										else
										{
											newObj = {
												country_id	  	: list.country_id.toString(),
												country_name  	: list.country_name.toString(),
												country_bg_img  : country_bg_img,
												state_id	  	: list.state_id.toString(),
												state_name    	: list.state_name.toString(),
												city_id	  		: "",
												city_name    	: "",
												city_bg_img  	: "",
												area_id	  		: "",
												area_name    	: "",
												result_flag    	: "2"

											};
										}

										array_rows.push(newObj);

										counter -= 1;

										resolve(array_rows);
									})
								}],
								function (result, current) {

									if (counter === 0)
									{
										if(parseInt(arrParams.flag) === 4 || parseInt(arrParams.flag) === 5)
										{
											return_result(result[0]);
										}
										else
										{
											methods.getCountStateResults(arrParams, function(returnTotalCount){

												var obj = {
													result : result[0],
													tot_count : returnTotalCount.toString(),
													tot_page  : Math.ceil((parseInt(returnTotalCount) - 5) / 50)
												}

												return_result(obj);

											});
										}
									}
								});
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }
					});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

methods.getCountStateResults = function(arrParams,return_result){

	try{
			var query  = " SELECT  st.id  ";
			query += " FROM states st  ";
			query += " INNER JOIN country con ON con.id = st.country_id  ";
			query += " WHERE st.status = 1 AND con.status = 1 ";
			query += " AND   LOWER(st.state_name) REGEXP '[[:<:]]" +arrParams.keyword.toLowerCase()+ "' ";

			if(parseInt(arrParams.country) !== 0)
			{
				query += " and  con.id = '"+arrParams.country+"' ";
			}

			if(parseInt(arrParams.state) !== 0)
			{
				query += " and  st.id = '"+arrParams.state+"' ";
			}

			DB.query(query,null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data.length);
				}
				else
				{return_result(0)}
			});
	}
	catch(err)
	{
		return_result(null)
	}
};

methods.getAutofillCityResults = function(arrParams,count,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(count) === 4 && (parseInt(arrParams.more_result_flag) === 0 || parseInt(arrParams.more_result_flag) === 3))
		{

				var query  = " SELECT  con.id as country_id,con.country_name ,con.country_short_code,con.phonecode, st.id as state_id,st.state_name,city.id as city_id,city.city_name , ";
					query += " (select app_img_url from geo_bg_img_details where geo_bg_img_details.flag = 1 and geo_bg_img_details.status = 1 and geo_bg_img_details.geo_id = con.id ) as country_bg_image , ";
					query += " (select app_img_url from geo_bg_img_details where geo_bg_img_details.flag = 3 and geo_bg_img_details.status = 1 and geo_bg_img_details.geo_id = city.id ) as city_bg_image  ";
					query += " FROM cities city  ";
					query += " INNER JOIN states st ON  st.id = city.`state_id`  ";
					query += " INNER JOIN country con ON con.id = st.country_id  ";
					query += " WHERE city.status = 1 AND st.status = 1 AND con.status = 1 ";


					if(parseInt(arrParams.country) !== 0)
					{
						query += " and  con.id = '"+arrParams.country+"' ";
					}

					if(parseInt(arrParams.state) !== 0)
					{
						query += " and  st.id = '"+arrParams.state+"' ";
					}

					if(parseInt(arrParams.city) !== 0)
					{
						query += " and  city.id = '"+arrParams.city+"' ";
					}


					if(parseInt(arrParams.flag) === 4)
					{
						if(arrParams.city_key != "")
						{
							query += " AND  (LOWER(city.city_name) REGEXP '[[:<:]]" +arrParams.city_key.toLowerCase()+ "') ";
						}
						else
						{ return_result(null); }
					}
					else if(parseInt(arrParams.flag) === 5 || parseInt(arrParams.flag) === 6)
					{
						query += "  ORDER BY  city.city_name asc  ";
					}
					else
					{
						if(arrParams.keyword != '')
						{
							query += " AND   LOWER(city.city_name) REGEXP '[[:<:]]" +arrParams.keyword.toLowerCase()+ "' ";

							query += "  ORDER BY '" +arrParams.keyword+  "', city.city_name DESC  ";
						}
						else
						{
							query += "  ORDER BY  city.city_name DESC  ";
						}
					}


					if(parseInt(arrParams.flag) === 3)
					{
						if(parseInt(arrParams.limit) === 0)
						{
							query += "  LIMIT  0 , 50 ";
						}
						else
						{
							query += "  LIMIT  "+ parseInt(arrParams.limit) * 50 +" , 50 ";
						}
					}
					else if(parseInt(arrParams.flag) === 4)
					{
							query += "  LIMIT  1";
					}
					else if(parseInt(arrParams.flag) === 5 || parseInt(arrParams.flag) === 6)
					{ }
					else
					{
						if(parseInt(arrParams.limit) === 0)
						{
							query += "  LIMIT  0 , 5 ";
						}
						else if(parseInt(arrParams.limit) === 1)
						{
							query += "  LIMIT  5 , 50 ";
						}
						else
						{
							var start = parseInt(arrParams.limit - 1) * 50;

							var end = parseInt(arrParams.limit) * 50;

							query += "  LIMIT  "+start+" , "+end;
						}
					}

					DB.query(query, null, function (data, error) {

						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
						{
							var counter = data.length;

							var array_rows = [];

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data,[function (list) {

									return new Promise(function (resolve, reject) {

										var newObj = {};

										var country_bg_img = "";

										if(list.country_bg_image !== "" && list.country_bg_image !== null)
										{ country_bg_img =  G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/'+list.country_bg_image.toString(); }

										var city_bg_image = "";

										if(list.city_bg_image !== "" && list.city_bg_image !== null)
										{ city_bg_image =  G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/'+list.city_bg_image.toString(); }

										if(parseInt(arrParams.flag) === 5)
										{
											var country_short_code = "";

											if(list.country_short_code !== "" && list.country_short_code !== null)
											{ country_short_code = list.country_short_code.toString(); }

											var phonecode = "";

											if(list.phonecode !== "" && list.phonecode !== null)
											{ phonecode = "+"+list.phonecode.toString(); }

											newObj = {
												country_id	  		: list.country_id.toString(),
												country_name  		: list.country_name.toString(),
												country_bg_img  	: country_bg_img,
												country_short_code	: country_short_code,
												country_phonecode   : phonecode,
												state_id	  		: list.state_id.toString(),
												state_name    		: list.state_name.toString(),
												city_id	  			: list.city_id.toString(),
												city_name    		: list.city_name.toString(),
												city_bg_img  		: city_bg_image,
												area_id	  			: "",
												area_name    		: "",
												result_flag    		: "3"
											};
										}
										else
										{
											newObj = {
												country_id	  	: list.country_id.toString(),
												country_name  	: list.country_name.toString(),
												country_bg_img  : country_bg_img,
												state_id	  	: list.state_id.toString(),
												state_name    	: list.state_name.toString(),
												city_id	  		: list.city_id.toString(),
												city_name    	: list.city_name.toString(),
												city_bg_img  	: city_bg_image,
												area_id	  		: "",
												area_name    	: "",
												result_flag    	: "3"

											};
										}

										array_rows.push(newObj);

										counter -= 1;

										resolve(array_rows);
									})
								}],
								function (result, current) {

									if (counter === 0)
									{
										if(parseInt(arrParams.flag) === 4 || parseInt(arrParams.flag) === 5 || parseInt(arrParams.flag) === 6)
										{
											return_result(result[0]);
										}
										else
										{
											methods.getCountCityResults(arrParams, function(returnTotalCount){

												var obj = {
													result : result[0],
													tot_count : returnTotalCount.toString(),
													tot_page  : Math.ceil((parseInt(returnTotalCount) - 5) / 50)
												}
												return_result(obj);
											});
										}
									}
								});
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }
					});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

methods.getCountCityResults = function(arrParams,return_result){

	try{
		var query  = " SELECT  city.id ";
					query += " FROM cities city  ";
					query += " INNER JOIN states st ON  st.id = city.`state_id`  ";
					query += " INNER JOIN country con ON con.id = st.country_id  ";
					query += " WHERE city.status = 1 AND st.status = 1 AND con.status = 1 ";
					query += " AND   LOWER(city.city_name) REGEXP '[[:<:]]" +arrParams.keyword.toLowerCase()+ "' ";

					if(parseInt(arrParams.country) !== 0)
					{
						query += " and  con.id = '"+arrParams.country+"' ";
					}

					if(parseInt(arrParams.state) !== 0)
					{
						query += " and  st.id = '"+arrParams.state+"' ";
					}

					if(parseInt(arrParams.city) !== 0)
					{
						query += " and  city.id = '"+arrParams.city+"' ";
					}

			DB.query(query,null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data.length);
				}
				else
				{return_result(0)}
			});
	}
	catch(err)
	{
		return_result(null)
	}
};

methods.getAutofillAreaResults = function(arrParams,count,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(count) === 5 && (parseInt(arrParams.more_result_flag) === 0 || parseInt(arrParams.more_result_flag) === 4))
		{

				var query  = " SELECT  con.id as country_id,con.country_name ,con.country_short_code,con.phonecode, st.id as state_id,st.state_name,city.id as city_id,city.city_name,area.area_id,area.area_name, ";
					query += " (select app_img_url from geo_bg_img_details where geo_bg_img_details.flag = 1 and geo_bg_img_details.status = 1 and geo_bg_img_details.geo_id = con.id ) as country_bg_image , ";
					query += " (select app_img_url from geo_bg_img_details where geo_bg_img_details.flag = 3 and geo_bg_img_details.status = 1 and geo_bg_img_details.geo_id = city.id ) as city_bg_image  ";
					query += " FROM area   ";
					query += " INNER JOIN cities city ON  city.id = area.city_id  ";
					query += " INNER JOIN states st ON  st.id = city.`state_id`  ";
					query += " INNER JOIN country con ON con.id = st.country_id  ";
					query += " WHERE area.status = 1 AND city.status = 1 AND st.status = 1 AND con.status = 1 ";


					if(parseInt(arrParams.country) !== 0)
					{
						query += " and  con.id = '"+arrParams.country+"' ";
					}

					if(parseInt(arrParams.state) !== 0)
					{
						query += " and  st.id = '"+arrParams.state+"' ";
					}

					if(parseInt(arrParams.city) !== 0)
					{
						query += " and  city.id = '"+arrParams.city+"' ";
					}

					if(typeof arrParams.area !== 'undefined' && arrParams.area !== null && parseInt(arrParams.area) !== 0)
					{
						query += " and area.area_id = '"+arrParams.area+"') ";
					}


					if(parseInt(arrParams.flag) === 4)
					{
						if(arrParams.city_key != "")
						{
							query += " AND  (LOWER(area.area_name) REGEXP '[[:<:]]" +arrParams.area_key.toLowerCase()+ "') ";
						}
						else
						{ return_result(null); }
					}
					else if(parseInt(arrParams.flag) === 5)
					{
						query += "  ORDER BY  area.area_name asc  ";
					}
					else
					{
						if(arrParams.keyword != '')
						{
							query += " AND   LOWER(area.area_name) REGEXP '[[:<:]]" +arrParams.keyword.toLowerCase()+ "' ";

							query += "  ORDER BY '" +arrParams.keyword+  "', area.area_name DESC  ";
						}
						else
						{
							query += "  ORDER BY  area.area_name DESC  ";
						}
					}


					if(parseInt(arrParams.flag) === 3)
					{
						if(parseInt(arrParams.limit) === 0)
						{
							query += "  LIMIT  0 , 50 ";
						}
						else
						{
							query += "  LIMIT  "+ parseInt(arrParams.limit) * 50 +" , 50 ";
						}
					}
					else if(parseInt(arrParams.flag) === 4)
					{
							query += "  LIMIT  1";
					}
					else if(parseInt(arrParams.flag) === 5)
					{ }
					else
					{
						if(parseInt(arrParams.limit) === 0)
						{
							query += "  LIMIT  0 , 5 ";
						}
						else if(parseInt(arrParams.limit) === 1)
						{
							query += "  LIMIT  5 , 50 ";
						}
						else
						{
							var start = parseInt(arrParams.limit - 1) * 50;

							var end = parseInt(arrParams.limit) * 50;

							query += "  LIMIT  "+start+" , "+end;
						}
					}


					DB.query(query, null, function (data, error) {

						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
						{
							var counter = data.length;

							var array_rows = [];

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data,[function (list) {

									return new Promise(function (resolve, reject) {

										var newObj = {};

										var country_bg_img = "";

										if(list.country_bg_image !== "" && list.country_bg_image !== null)
										{ country_bg_img =  G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/'+list.country_bg_image.toString(); }


										if(parseInt(arrParams.flag) === 5)
										{
											var country_short_code = "";

											if(list.country_short_code !== "" && list.country_short_code !== null)
											{ country_short_code = list.country_short_code.toString(); }

											var phonecode = "";

											if(list.phonecode !== "" && list.phonecode !== null)
											{ phonecode = "+"+list.phonecode.toString(); }

											newObj = {
												country_id	  		: list.country_id.toString(),
												country_name  		: list.country_name.toString(),
												country_bg_img  	: country_bg_img,
												country_short_code	: country_short_code,
												country_phonecode   : phonecode,
												state_id	  		: list.state_id.toString(),
												state_name    		: list.state_name.toString(),
												city_id	  			: list.city_id.toString(),
												city_name    		: list.city_name.toString(),
												area_id	  			: list.area_id.toString(),
												area_name    		: list.area_name.toString(),
												result_flag    		: "4"
											};
										}
										else
										{
											var city_bg_image = "";

											if(list.city_bg_image !== "" && list.city_bg_image !== null)
											{ city_bg_image =  G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/'+list.city_bg_image.toString(); }

											newObj = {
												country_id	  	: list.country_id.toString(),
												country_name  	: list.country_name.toString(),
												country_bg_img  : country_bg_img,
												state_id	  	: list.state_id.toString(),
												state_name    	: list.state_name.toString(),
												city_id	  		: list.city_id.toString(),
												city_name    	: list.city_name.toString(),
												city_bg_img  	: city_bg_image,
												area_id	  		: list.area_id.toString(),
												area_name    	: list.area_name.toString(),
												result_flag    	: "4"
											};
										}

										array_rows.push(newObj);

										counter -= 1;

										resolve(array_rows);
									})
								}],
								function (result, current) {

									if (counter === 0)
									{return_result(result[0]); }
								});
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }
					});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

// ------------------------------------------------------ Geo Search Module End  --------------------------------------------------------------//

// ------------------------------------------------------ Geo Phone Code Module Start  --------------------------------------------------------------//

methods.getPhoneCodeList = function(arrParams,return_result)
{
	try
	{
		var query = " SELECT *  FROM country WHERE status = 1 AND country_name != '' and phonecode != '0' ";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{
					var counter = data.length;

					var array_rows = [];

					if(parseInt(counter) !== 0)
					{
						promiseForeach.each(data,[function (list) {

							return new Promise(function (resolve, reject) {

								var country_short_code = "";

									if(list.country_short_code !== "" && list.country_short_code !== null)
									{ country_short_code = list.country_short_code.toString(); }

								var newObj = {
									country_id	  		: list.id.toString(),
									country_name  		: list.country_name.toString(),
									country_short_code	: country_short_code,
									country_phonecode   : "+"+list.phonecode.toString(),
								};


								array_rows.push(newObj);

								counter -= 1;

								resolve(array_rows);
							})
						}],
						function (result, current) {

							if (counter === 0)
							{ return_result(fast_sort(result[0]).asc('country_name')); }
						});
					}
					else
					{ return_result(null); }
				}
				else
				{ return_result(null); }
			});
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}
// ------------------------------------------------------ Geo Phone Code Module End  --------------------------------------------------------------//


// ------------------------------------------------------ General Currency Module Start  --------------------------------------------------------------//

methods.getCurrencyList = function(arrParams,return_result)
{
	try
	{
		var query  = "  SELECT `group_id`, `meta_key`, `meta_value` FROM jaze_currency_master WHERE  ";
			query += " group_id IN  (SELECT group_id FROM jaze_currency_master WHERE meta_key = 'status'  AND meta_value = '1')  ";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{
					HelperRestriction.convertMultiMetaArray(data, function(data_result){

						var counter = data_result.length;

						var array_rows = [];

						if(parseInt(counter) !== 0)
						{
							promiseForeach.each(data_result,[function (list) {

								return new Promise(function (resolve, reject) {

									var newObj = {
										currency_id	  	: list.group_id.toString(),
										currency_name  	: list.currency_name.toString(),
										currency_code   : list.currency_code.toString()
									};

									array_rows.push(newObj);

									counter -= 1;

									resolve(array_rows);
								})
							}],
							function (result, current) {

								if (counter === 0)
								{return_result(result[0]); }
							});
						}
						else
						{ return_result(null); }
					});
				}
				else
				{ return_result(null); }
			});
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

// ------------------------------------------------------ General Currency Module End  --------------------------------------------------------------//


//------------------------------------------------------------------- Location Module Start ---------------------------------------------------------------------------//

methods.__getLocationDetails = function(area_id,city_id,return_result){

	try{
			var obj_return = {
				country_id : "0",
				country_name : "",
				state_id : "0",
				state_name : "",
				city_id : "0",
				city_name : "",
				area_id : "0",
				area_name : "",
			};

			if(parseInt(area_id) !== 0)
			{
				var query  =" SELECT  con.id as country_id,con.country_name ,st.id as state_id,st.state_name,city.id as city_id,city.city_name,ar.area_id,ar.area_name ";
					query +=" FROM area ar  ";
					query +=" INNER JOIN cities city ON city.id = ar.city_id ";
                    query +=" INNER JOIN states st ON st.id = city.state_id ";
                    query +=" INNER JOIN country con ON con.id=st.country_id ";
					query +=" WHERE ar.status = '1' and city.status = '1' and st.status = '1' and con.status = '1' and ar.area_id = '"+area_id+"' " ;

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0]);
					}
					else
					{return_result(obj_return)}
				});
			}
			else if(parseInt(city_id) !== 0)
			{
				var query  =" SELECT  con.id as country_id,con.country_name ,st.id as state_id,st.state_name,city.id as city_id,city.city_name, '0' as area_id, '' as area_name ";
					query +=" FROM cities city  ";
                    query +=" INNER JOIN states st ON st.id = city.state_id ";
                    query +=" INNER JOIN country con ON con.id=st.country_id ";
					query +=" WHERE city.status = '1' and st.status = '1' and con.status = '1' and city.id = '"+city_id+"' " ;

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0]);
					}
					else
					{return_result(obj_return)}
				});
			}
			else
			{ return_result(obj_return); }


	}
	catch(err)
	{
		return_result(obj_return)
	}
};

//------------------------------------------------------------------- Location Module End ---------------------------------------------------------------------------//


methods.convertMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			Object.assign(list, {account_id : arrvalues[0].account_id});
			Object.assign(list, {account_type : arrvalues[0].account_type});

			for (var i in arrvalues)
			{
				Object.assign(list, {[arrvalues[i].meta_key]:  arrvalues[i].meta_value});
			}

			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};

methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues)
			{
				var index = __getGroupIndex(list,arrvalues[i].account_id);

				if(index != ""){

					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							account_type : arrvalues[i].account_type,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}
			}

			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};

/*
* split values push to array
*/
function __convert_array(array_val) {

	var list = [];

	if(typeof array_val !== 'undefined' && array_val !== null && array_val !== "")
	{
		if(array_val.toString().indexOf(',') != -1 ){
			list = array_val.split(',');
		}
		else {
			list.push(array_val);
		}
	}
	return list;
}

function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

		if(list_array !== null)
		{
			Object.keys(list_array).forEach(function(prop) {

				var value = list_array[prop].account_id;

				if(parseInt(value) === parseInt(index_value))
				{
					retern_index = prop;
				}
			  });
		}

		return retern_index;
}

module.exports = methods;
