const methods = {};
const DB = require('./db_base.js');
const HelperRestriction = require('./HelperRestriction.js');
const HelperGeneral = require('./HelperGeneral.js');
const fast_sort = require("fast-sort");


// ------------------------------------------------------ jazenet Voice Module Start  --------------------------------------------------------------//   

methods.getVoiceSearchResult = function(arrParams,return_result)
{
	var return_obj = {
		type : "0",
		result_count : "0",
		result : "My apologies... i don't understand",
		result_link : ""
	};

	// type : 0 = text, 1 = link, 2 = search, 3 = popup, jq Fn(), 4 = language
	// category :  1 = location change, 2 = Q/A,  3 = switch user, 4 = time, 5 = dtae, 6= weather, 7 = company, 8 = prof profiles, 9 = products category, 10 product details, 11 = category 12 = keywords

	try
	{
		if(arrParams !== null)
		{
			var flag = 0;

			methods.___getVoiceSearchIgnoreList(0, function(ignoreList){

				methods.__getLocationDetails(arrParams.city, function(locationDetails){

					methods.__getVoiceSearchLocation(flag,ignoreList,arrParams, function(switchlocationDetails){

						if(switchlocationDetails !== null) { flag = 1; return_obj = switchlocationDetails; }

						methods.__getVoiceSearchQuestionAnswer(flag,arrParams, function(qaDetails){

							if(qaDetails !== null) { flag = 1; return_obj = qaDetails; }

							methods.__getVoiceSearchTime(flag,ignoreList,arrParams, function(timeDetails){

								if(timeDetails !== null) { flag = 1; return_obj = timeDetails; }

								methods.__getVoiceSearchDate(flag,ignoreList,arrParams, function(dateDetails){

									if(dateDetails !== null) { flag = 1; return_obj = dateDetails; }
								
									methods.__getVoiceSearchWeather(flag,ignoreList,arrParams, function(weatherDetails){

										if(weatherDetails !== null) { flag = 1;  return_obj = weatherDetails; }

										methods.__getVoiceSearchServiceCategorylList(flag,ignoreList,arrParams,locationDetails, function(servCatList){

											if(servCatList !== null) { flag = 1; return_obj = servCatList; }

											methods.__getVoiceSearchCompanyList(flag,ignoreList,arrParams,locationDetails, function(compList){

												if(compList !== null) { flag = 1; return_obj = compList; }

												methods.__getVoiceSearchProfessionalList(flag,ignoreList,arrParams,locationDetails, function(profList){

													if(profList !== null) { flag = 1; return_obj = profList; }

													return_result(return_obj); 

												});
											});
										});
									});
								});
							});
						});
					});
				});
			});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.__getVoiceSearchLocation = function(flag,ignoreList,arrParams,return_result)
{
	try
	{
		if(parseInt(flag) === 0 && arrParams !== null)
		{
			var key_location_list = ["change location","change","location","change country","change state","change city"];

			var keywordList = __convert_arraySpace(arrParams.keyword.toLowerCase());

			var key = intersect_arrays(key_location_list, keywordList);

			if(parseInt(key.length) !== 0)
			{
				var keyword = outersect_arrays(ignoreList, keywordList);	

					methods.__checkCityKey(keyword.trim(), function(locationDetails){

						if(locationDetails !== null)
						{
							if(LOCATION_CITY_LIST.indexOf(locationDetails.city_id) !== -1){

								var link = G_STATIC_WEB_URL+'/geo/set_country/'+locationDetails.country_id+'/'+locationDetails.state_id+'/'+locationDetails.city_id+'/0/1';

								var return_obj = {
										category 	 : 1,
										type 	 	 : 1,
										result_count : 1,
										result 		 : "",
										result_link  : link
									};

								return_result(return_obj);
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }
					});	
			}
			else
			{ return_result(null); }
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}		
}

methods.__getVoiceSearchQuestionAnswer = function(flag,arrParams,return_result)
{
	try
	{
		if(parseInt(flag) === 0 && arrParams !== null)
		{ 
			methods.___getVoiceSearchIgnoreList(2, function(ignoreList){

				var keyword =  arrParams.keyword;

				for (var i in ignoreList) 
				{
					keyword = keyword.replace(ignoreList[i], "");
				}

				var query  = " SELECT * FROM `jaze_voice_greeting_questions` WHERE `status` = '1' and `type` in (0,5,6,7) ";

				if(keyword !== null && keyword !== "")
				{
					query  += " and (lower(question) ='"+keyword.toLowerCase()+"' or lower(question) = '"+keyword.toLowerCase()+"s') ";
				}

				DB.query(query, null, function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{
						var query  = " SELECT  * FROM    ( ";
							query += " SELECT a.answer FROM `jaze_voice_greetings_answers` a ";
							query += " inner join jaze_voice_greetings_map_question_answers map on map.answer_id = a.id ";
							query += " WHERE map.`status`= '1'  and a.`status`= '1'  and map.question_id = '"+data[0].id+"'  ";
							query += " ORDER BY `answer` DESC ";
							query += " ) AS temp ORDER BY RAND() LIMIT 1 ";

							DB.query(query, null, function (dataAns, error) {
								
								if (typeof dataAns !== 'undefined' && dataAns !== null && parseInt(dataAns.length) !== 0)
								{
									var return_obj = {};

									if(parseInt(data[0].type) === 5 || parseInt(data[0].type) === 6)
									{
										var answer = dataAns[0].answer.split('|');

										var popup_name = "" ,popup_function = "";

										if(parseInt(answer.length) > 1)
										{
											popup_name = answer[0]; 
											popup_function = answer[1]; 
										}
										else
										{  popup_function = dataAns[0].answer;  }

										return_obj = {
											category 	 	: 2,
											type 	 	 	: 3,
											result_count 	: 1,
											result 		 	: "",
											popup_name  	: popup_name,
											jq_function  : popup_function
										};
									}
									else if(parseInt(data[0].type) === 7)
									{
										return_obj = {
											category 	 	: 2,
											type 	 	 	: 4,
											result_count 	: 1,
											result 		 	: "",
											result_link  	: dataAns[0].answer
										};
									}
									else
									{
										return_obj = {
											category 	 : 2,
											type 	 	 : 0,
											result_count : 1,
											result 		 : dataAns[0].answer,
											result_link  : ""
										};
									}

									return_result(return_obj);
								}
								else
								{ return_result (null); }
							});
					}
					else
					{ return_result (null); }
				});
			});
		}
		else 
		{ return_result(null); }
	}
	catch(err)
	{ console.log("__getVoiceSearchQuestionAnswer",err); return_result(null);}
};

methods.__getVoiceSearchTime = function(flag,ignoreList,arrParams,return_result)
{
	try
	{
		if(parseInt(flag) === 0 && arrParams !== null)
		{
			methods.___getVoiceSearchQuestionList(2, function(timeKey){

				var keywordList = __convert_arraySpace(arrParams.keyword.toLowerCase());

				var key = intersect_arrays(timeKey, keywordList);
				
				if(parseInt(key.length) !== 0)
				{
					var ignore_list = 	timeKey.concat(ignoreList);

					var keyword = outersect_arrays(ignore_list, keywordList);		

					methods.__checkCityKey(keyword.trim(), function(locationDetails){
							
						if(locationDetails !== null)
						{
							var link = G_STATIC_WEB_URL+'weather/timeInfo/'+locationDetails.city_id+'/'+encodeURI(locationDetails.city_name.toLowerCase());
						}
						else
						{ 
							//passing current weather
							var link = G_STATIC_WEB_URL+'weather/timeInfo/'+arrParams.city;
						}

							var return_obj = {
									category 	 : 4,
									type 	 	 : 1,
									result_count : 1,
									result 		 : "",
									result_link  : link
								};

							return_result(return_obj);
					});	
				}
				else
				{ return_result(null); }
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}		
};

methods.__getVoiceSearchDate = function(flag,ignoreList,arrParams,return_result)
{
	try
	{
		if(parseInt(flag) === 0 && arrParams !== null)
		{
			methods.___getVoiceSearchQuestionList(3, function(timeKey){

				var keywordList = __convert_arraySpace(arrParams.keyword.toLowerCase());

				var key = intersect_arrays(timeKey, keywordList);
				
				if(parseInt(key.length) !== 0)
				{
					var ignore_list = 	key.concat(ignoreList);

					var keyword = outersect_arrays(ignore_list, keywordList);	
					
						methods.__checkCityKey(keyword.trim(), function(locationDetails){
							
							if(locationDetails !== null)
							{
								var link = G_STATIC_WEB_URL+'weather/timeInfo/'+locationDetails.city_id+'/'+encodeURI(locationDetails.city_name.toLowerCase());
							}
							else
							{ 
								//passing current weather
								var link = G_STATIC_WEB_URL+'weather/timeInfo/'+arrParams.city;
							}

							var return_obj = {
									category 	 : 5,
									type 	 	 : 1,
									result_count : 1,
									result 		 : "",
									result_link  : link
								};

							return_result(return_obj);
						});	
				}
				else
				{ return_result(null); }
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}		
};

methods.__getVoiceSearchWeather = function(flag,ignoreList,arrParams,return_result)
{
	try
	{
		if(parseInt(flag) === 0 && arrParams !== null)
		{
			methods.___getVoiceSearchQuestionList(1, function(weatherKey){

				var keywordList = __convert_arraySpace(arrParams.keyword.toLowerCase());

				var key = intersect_arrays(weatherKey, keywordList);
			
				if(parseInt(key.length) !== 0)
				{
					var ignore_list = 	weatherKey.concat(ignoreList);

					var keyword = outersect_arrays(ignore_list, keywordList);				
						
					methods.__checkCityKey(keyword.trim(), function(locationDetails){

							if(locationDetails !== null)
							{
								var link = G_STATIC_WEB_URL+'weather/weatherInfo/'+locationDetails.city_id+'/'+encodeURI(locationDetails.city_name.toLowerCase());
							}
							else
							{ 
								//passing current weather
								var link = G_STATIC_WEB_URL+'weather/weatherInfo/'+arrParams.city;
							}

							var return_obj = {
									category 	 : 6,
									type 	 	 : 1,
									result_count : 1,
									result 		 : "",
									result_link  : link
								};

							return_result(return_obj);
					});	
				}
				else
				{ return_result(null); }
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}		
};

methods.__getVoiceSearchCompanyList = function(flag,ignoreList,arrParams,parms_locationDetails,return_result)
{
	try
	{
		if(parseInt(flag) === 0 && arrParams !== null)
		{ 
			ignoreList.splice(ignoreList.indexOf('in'), 1);

			var keywordList =__convert_arraySpace(arrParams.keyword.toLowerCase());

			var keyword = outersect_arrays(ignoreList, keywordList);

			if(keyword !== "" && keyword !== null)
			{
				var city_key = "";

				if(parseInt((intersect_arrays(['in'], __convert_arraySpace(keyword))).length) !== 0)
				{
					city_key = (keywordList.slice(keywordList.indexOf('in') + 1, keywordList.length)).join(" ").trim();

					keyword  = (keywordList.slice(0,keywordList.indexOf('in'))).join(" ");
				}
				
				//check city name exisit
				methods.__checkCityKey(city_key, function(citylocationDetails){


					var locationDetails = [];

					if(citylocationDetails != null)
					{ locationDetails =  citylocationDetails; }
					else 
					{ locationDetails =  parms_locationDetails; }

 
					var query  = " SELECT * FROM jaze_user_basic_details WHERE account_type in (3,5) and meta_key IN ('company_name')  ";
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('company_name')  AND LOWER(meta_value) REGEXP '[[:<:]]"+keyword.trim()+"' ";
							
						if(locationDetails != null)
						{
							query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+locationDetails.city_id+"') ";
						}

						query += " )) ";

						query += " group by account_id  ";

						DB.query(query,null, function(data, error){
							
							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							var return_obj = {};
							
								if(parseInt(data.length) === 1)
								{
									var link = G_STATIC_WEB_URL;

									if(parseInt(data[0].account_type) === 3)
									{ link += 'jazecompany/view/'; }
									else if(parseInt(data[0].account_type) === 5)
									{ link += 'jazegovernment/view/'; }

									link += data[0].account_id+'/'+data[0].meta_value.replace(/\s+/g, '-').toLowerCase();

									return_obj = {
										category 	 : 7,
										type 	 	 : 1,
										result_count : 1,
										result 		 : data[0].meta_value,
										result_link  : link
									};
								}
								else
								{
									return_obj = {
										category 	 : 7,
										type 	 	 : 2,
										result_count : data.length,
										result 		 : keyword.trim(),
										result_link  : ""
									};

									Object.assign(return_obj, locationDetails); 
								}

								return_result (return_obj);
							}
							else
							{ return_result (null); }
						});
				});
			}
			else
			{ return_result (null); }
		}
		else 
		{ return_result(null); }
	}
	catch(err)
	{ console.log("__getVoiceSearchCompanyList",err); return_result(null);}
};

methods.__getVoiceSearchProfessionalList = function(flag,ignoreList,arrParams,parms_locationDetails,return_result)
{
	try
	{
		if(parseInt(flag) === 0 && arrParams !== null)
		{ 
			ignoreList.splice(ignoreList.indexOf('in'), 1);

			var keywordList =__convert_arraySpace(arrParams.keyword.toLowerCase());

			var keyword = outersect_arrays(ignoreList, keywordList);

			if(keyword !== "" && keyword !== null)
			{
				var city_key = "";

				if(parseInt((intersect_arrays(['in'], __convert_arraySpace(keyword))).length) !== 0)
				{
					city_key = (keywordList.slice(keywordList.indexOf('in') + 1, keywordList.length)).join(" ").trim();

					keyword  = (keywordList.slice(0,keywordList.indexOf('in'))).join(" ");
				}
				
				//check city name exisit
				methods.__checkCityKey(city_key, function(citylocationDetails){

					var locationDetails = [];

					if(citylocationDetails != null)
					{ locationDetails =  citylocationDetails; }
					else 
					{ locationDetails =  parms_locationDetails; }

					var query  = " SELECT * FROM jaze_user_basic_details WHERE account_type = 2 and meta_key IN ('fname','lname') ";
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('fname','lname')  AND LOWER(meta_value) REGEXP '[[:<:]]"+keyword.trim()+"' ";
							
						if(locationDetails != null)
						{
							query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+locationDetails.city_id+"') ";
						}
						query += " )) ";

						DB.query(query,null, function(data, error){
							
							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
								
								HelperGeneral.convertMultiMetaArray(data, function(retConverted){	
									
									var return_obj = {};
								
									if(parseInt(retConverted.length) === 1)
									{
										var link = G_STATIC_WEB_URL+'jazeprofessional/view/'+retConverted[0].account_id+'/'+retConverted[0].fname.replace(/\s+/g, '-').toLowerCase()+'-'+retConverted[0].lname.replace(/\s+/g, '-').toLowerCase();

										return_obj = {
											category 	 : 8,
											type 	 	 : 1,
											result_count : 1,
											result 		 : retConverted[0].fname+' '+retConverted[0].lname,
											result_link  : link
										};
									}
									else
									{
										return_obj = {
											category 	 : 8,
											type 	 	 : 2,
											result_count : retConverted.length,
											result 		 : keyword.trim(),
											result_link  : ""
										};

										Object.assign(return_obj, locationDetails); 
									}

									return_result (return_obj);
								});
							}
							else
							{ return_result (null); }
						});
				});
			}
			else
			{ return_result (null); }
		}
		else 
		{ return_result(null); }
	}
	catch(err)
	{ console.log("__getVoiceSearchProfessionalList",err); return_result(null);}
};


methods.__getVoiceSearchServiceCategorylList = function(flag,ignoreList,arrParams,parms_locationDetails,return_result)
{
	try
	{
		if(parseInt(flag) === 0 && arrParams !== null)
		{ 
			ignoreList.splice(ignoreList.indexOf('in'), 1);

			var keywordList =__convert_arraySpace(arrParams.keyword.toLowerCase());

			var keyword = outersect_arrays(ignoreList, keywordList);

			if(keyword !== "" && keyword !== null)
			{
				var city_key = "";

				if(parseInt((intersect_arrays(['in'], __convert_arraySpace(keyword))).length) !== 0)
				{
					city_key = (keywordList.slice(keywordList.indexOf('in') + 1, keywordList.length)).join(" ").trim();

					keyword  = (keywordList.slice(0,keywordList.indexOf('in'))).join(" ");
				}
				
				//check city name exisit
				methods.__checkCityKey(city_key, function(citylocationDetails){

					var query  = " SELECT * FROM jaze_category_list WHERE  meta_key IN ('parent_id','cate_name','is_portal','url_key')  ";
						query += " and  group_id IN  (SELECT group_id FROM jaze_category_list WHERE meta_key = 'status'  AND meta_value = '1') ";
						query += " and  group_id NOT IN  (SELECT group_id FROM jaze_category_list WHERE meta_key = 'parent_id'  AND meta_value IN (2,4,6,10,11,1834,1833)) ";
						query += " and  group_id IN  (SELECT group_id FROM jaze_category_list WHERE meta_key IN ('cate_name')  AND LOWER(meta_value) REGEXP '[[:<:]]"+keyword.trim()+"') ";

						DB.query(query,null, function(data, error){
							
							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
								
								HelperRestriction.convertMultiMetaArray(data, function(retConverted){	

										fast_sort(retConverted).desc('is_portal');

											var return_obj = {};

											var link = G_STATIC_WEB_URL;

											var locationDetails = [];

											if(citylocationDetails != null)
											{ locationDetails =  citylocationDetails; }
											else 
											{ locationDetails =  parms_locationDetails; }

											var city_url = "/"+locationDetails.city_id+"/"+locationDetails.city_name.replace(/\s+/g, '-').toLowerCase(); 

											//checking portal exists
											if(parseInt(retConverted[0].is_portal) === 1)
											{
												return_obj = {
													category 	 : 11,
													type 	 	 : 1,
													result_count : 1,
													result 		 : '',
													result_link  : link+retConverted[0].url_key
												};
 
												return_result (return_obj); 
											}
											else
											{ 
												if(parseInt(retConverted.length) === 1)
												{ 
													methods.___getCategoryParentName(retConverted[0].parent_id, function(parent_cat_list){
												
														var parent_cat_array = [2,4,6,10,11,1834,1833];

														if(parent_cat_list !== null)
														{  
															parent_cat_list = parent_cat_list.reverse(); 
															
															if(parent_cat_array.indexOf(parseInt(parent_cat_list[0].cate_id)) === -1)
															{    
																var parent_cat_id = parent_cat_list[0].cate_id;
																
																if(parseInt(parent_cat_id) === 0 || parseInt(parent_cat_id) === 1)
																{
																	//products and services
																	methods.___checkCategoryIsChild(retConverted[0].group_id, function(childCount){

																		if(parseInt(childCount) !== 0)
																		{
																			link += 'jazecategory/child/'+retConverted[0].group_id+'/'+retConverted[0].cate_name.replace(/\s+/g, '-').toLowerCase()+city_url;
																		}
																		else
																		{
																			var parent_id =  parent_cat_list[parent_cat_list.length - 1].cate_id;
																			link += 'jazecategory/listing/'+retConverted[0].group_id+'/'+retConverted[0].cate_name.replace(/\s+/g, '-').toLowerCase()+"/mcategory-"+parent_id;
																			link += ';:country-'+locationDetails.country_id+';state-'+locationDetails.state_id+';city-'+locationDetails.city_id+';area-0';
																		}
																			return_obj = {
																				category 	 : 11,
																				type 	 	 : 1,
																				result_count : childCount,
																				result 		 : keyword.trim(),
																				result_link  : link
																			};
																			
																			return_result (return_obj); 
																	});
																}
																else if(parseInt(parent_cat_id) === 3)
																{
																	//city information

																	var tourist_info_status = 0;

																	if(parseInt(retConverted[0].group_id) === 947)
																	{ tourist_info_status = 1; }
																	else if(parent_cat_list.length > 1)
																	{ if(parseInt(parent_cat_list[1].cate_id) === 947)
																	{ tourist_info_status = 1; }
																	}

																	// tourist info
																	if(parseInt(tourist_info_status) === 1)
																	{
																		if(parseInt(retConverted[0].group_id) !== 947)
																		{
																			//check tourist info is specific city
																			methods.___checkTouristInfoCategoryCity(retConverted[0].group_id,locationDetails.city_id, function(cat_list){

																				if(cat_list !== null)
																				{
																					link += 'jazetouristinfo/tourist_info/'+cat_list[0].group_id+'/'+cat_list[0].category_type+'/'+retConverted[0].cate_name.replace(/\s+/g, '-').toLowerCase()+city_url;

																					return_obj = {
																						category 	 : 11,
																						type 	 	 : 1,
																						result_count : 1,
																						result 		 : '',
																						result_link  : link
																					};
					
																					return_result (return_obj); 
																				}
																				else
																				{ return_result (null); }
																			});
																		}
																		else
																		{
																			link += '/jazetouristinfo/home/'+retConverted[0].group_id+city_url;

																			return_obj = {
																				category 	 : 11,
																				type 	 	 : 1,
																				result_count : 1,
																				result 		 : '',
																				result_link  : link
																			};
			
																			return_result (return_obj); 
																		}
																	}
																	else
																	{
																		//check have sub cat
																		methods.___checkCityInfoSubCategory(retConverted[0].group_id, function(city_child_cat_list){

																			if(parseInt(city_child_cat_list) !== 0)
																			{
																				link += 'jazecityinfo/sub_category_list/'+retConverted[0].group_id+city_url;
																			}
																			else
																			{
																				link += 'jazecityinfo/static_company_lists/'+retConverted[0].group_id+'/'+retConverted[0].cate_name.replace(/\s+/g, '-').toLowerCase()+'/0/0'+city_url+'/1'
																			}

																			return_obj = {
																				category 	 : 11,
																				type 	 	 : 1,
																				result_count : 1,
																				result 		 : '',
																				result_link  : link
																			};
				
																			return_result (return_obj); 
																		});
																	}
																}
																else
																{ return_result (null); }
															}
															else
															{ return_result (null); }
														}
														else
														{ return_result (null); }
													});
												}
												else
												{
													return_obj = {
														category 	 : 11,
														type 	 	 : 2,
														result_count : retConverted.length,
														result 		 : keyword.trim(),
														result_link  : ""
													};
			
													Object.assign(return_obj, locationDetails);

													return_result (return_obj);
												}
											}
									
								});
							}
							else
							{ return_result (null); }
						});
				});
			}
			else
			{ return_result (null); }
		}
		else 
		{ return_result(null); }
	}
	catch(err)
	{ console.log("__getVoiceSearchServiceCategorylList",err); return_result(null);}
};


//Get Category is child
methods.___checkCategoryIsChild = function(category_id,return_result)
{
	try
	{
		if (parseInt(category_id) !== 0)
		{
			var query = " SELECT group_id FROM `jaze_category_list` WHERE `meta_key` = 'parent_id' AND `meta_value` = '"+category_id+"' ";

			DB.query(query, null, function (data, error) {
			
				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{
					return_result(data.length);
				}
				else
				{ return_result(0); }
			});
		}
		else
		{ return_result (0); }
	}
	catch(err)
	{ console.log("___checkCategoryIsChild",err); return_result(0);}
};

//Get Category Parent Name 
methods.___getCategoryParentName =  function (category_id,return_result){

	try
	{
		var result_array = [];

		if(parseInt(category_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_category_list WHERE  meta_key IN ('cate_name','parent_id')  ";
				query += " and group_id IN  (SELECT group_id FROM jaze_category_list WHERE meta_key = 'status'  AND meta_value = '1') ";
				query += " and group_id = '"+category_id+"' ";
				
				DB.query(query, null, function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{
						HelperRestriction.convertMultiMetaArray(data, function(data_result){

							if (typeof data_result !== 'undefined' && data_result !== null && parseInt(data_result.length) !== 0)
							{
								var obj_return = {
									cate_id 	:  data_result[0].group_id,
									cate_name   :  data_result[0].cate_name,
									parent_id  :  data_result[0].parent_id
								};

								result_array.push(obj_return);
									
								if(parseInt(data_result[0].parent_id) !== 0)
								{  
									methods.___getCategoryParentName(data_result[0].parent_id, function(parent_result){	

										if(parent_result !== null)
										{ result_array = result_array.concat(parent_result); }

										return_result(result_array);
									});
								}
								else
								{ return_result(result_array); }
							}
							else
							{ return_result(null); }
						});
					}
					else
					{ return_result(null); }
				});
		}  
		else if(parseInt(category_id) === 0)
		{
			var obj_return = {
				cate_id    :  0,
				cate_name  : 'products and services',
				parent_id  : '0'
			};

			result_array.push(obj_return);

			return_result(result_array);
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("___getCategoryParentName",err); return_result(null); }	
};

//check cityinfo sub cat
methods.___checkCityInfoSubCategory =  function (category_id,return_result){

	try
	{
		if (parseInt(category_id) !== 0)
		{
			var query = " SELECT group_id FROM `jaze_cityinfo_subcat_details` WHERE `meta_key` = 'parent_id' AND `meta_value` = '"+category_id+"' ";

			DB.query(query, null, function (data, error) {
			
				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{
					return_result(data.length);
				}
				else
				{ return_result(0); }
			});
		}
		else
		{ return_result (0); }
	}
	catch(err)
	{ console.log("___checkCityInfoSubCategory",err); return_result(0); }	
};

//check tourist info sub cat
methods.___checkTouristInfoCategoryCity =  function (category_id,city_id,return_result){

	try
	{
		if (parseInt(category_id) !== 0 && parseInt(city_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_touristinfo_page_groups WHERE  meta_key in ('category_type') ";
				query += "and group_id IN  (SELECT group_id FROM jaze_touristinfo_page_groups WHERE meta_key = 'status'  AND meta_value = '1' ";
				query += "and group_id IN  (SELECT group_id FROM jaze_touristinfo_page_groups WHERE meta_key = 'category_id'  AND meta_value = '"+category_id+"' ";
				query += "and group_id IN  (SELECT group_id FROM jaze_touristinfo_page_groups WHERE meta_key = 'city_id'  AND meta_value = '"+city_id+"'))) ";

			DB.query(query, null, function (data, error) {
			
				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{
					HelperRestriction.convertMultiMetaArray(data, function(retConverted){	

						return_result(retConverted);
					});
				}
				else
				{ return_result(null); }
			});
		}
		else
		{ return_result (null); }
	}
	catch(err)
	{ console.log("___checkTouristInfoCategoryCity",err); return_result(null); }	
};

//get Voice Ignore keyword List
methods.___getVoiceSearchIgnoreList = function(keyword_type,return_result)
{
	try
	{
		var query = " SELECT group_concat(keywords) as keywords FROM jaze_voice_ignore_list WHERE `status` = '1'  ";

		if(parseInt(keyword_type) !== 0)
		{ 
			query += " and `type` = '"+keyword_type+"' ";
		}

		DB.query(query, null, function (data, error) {
	
			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{
				return_result (__convert_arrayComma(data[0].keywords));
			}
			else
			{ return_result (null); }
		});
	}
	catch(err)
	{ console.log("___getVoiceSearchIgnoreList",err); return_result(null);}
};

//get Voice Question List
methods.___getVoiceSearchQuestionList = function(type,return_result)
{
	try
	{
		var query = " SELECT group_concat(question) as question FROM jaze_voice_greeting_questions WHERE `status` = '1'  ";

		if(type !== '')
		{ 
			query += " and `type` = '"+type+"' ";
		}

		DB.query(query, null, function (data, error) {
	
			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{
				return_result (__convert_arrayComma(data[0].question));
			}
			else
			{ return_result (null); }
		});
	}
	catch(err)
	{ console.log("___getVoiceSearchQuestionList",err); return_result(null);}
};

// ------------------------------------------------------ jazenet Voice Module End  --------------------------------------------------------------//   

//------------------------------------------------------------------- General Module Start ---------------------------------------------------------------------------//


methods.__checkCityKey = function(city_key,return_result){

	try{

		if(city_key !== "" && city_key !== null)
		{
			var query  =" SELECT  con.id as country_id,con.country_name ,st.id as state_id,st.state_name,city.id as city_id,city.city_name ";
				query +=" FROM cities city  ";
                query +=" INNER JOIN states st ON st.id = city.state_id ";
            	query +=" INNER JOIN country con ON con.id=st.country_id ";
				query +=" WHERE city.status = '1' and st.status = '1' and con.status = '1' and  MATCH(city.city_name) AGAINST('"+city_key+"' IN BOOLEAN MODE) limit 1" ;

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0]);
					}
					else
					{return_result(null)}
				});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ return_result(null); }
};

methods.__getLocationDetails = function(city_id,return_result){

	try{
			var obj_return = {
				country_id : "0",
				country_name : "",
				state_id : "0",
				state_name : "",
				city_id : "0",
				city_name : ""
			};

			if(parseInt(city_id) !== 0)
			{
				var query  =" SELECT  con.id as country_id,con.country_name ,st.id as state_id,st.state_name,city.id as city_id,city.city_name ";
					query +=" FROM cities city  ";
                    query +=" INNER JOIN states st ON st.id = city.state_id ";
                    query +=" INNER JOIN country con ON con.id=st.country_id ";
					query +=" WHERE city.status = '1' and st.status = '1' and con.status = '1' and city.id = '"+city_id+"' " ;
 
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0]);
					}
					else
					{return_result(obj_return)}
				});
			}
			else
			{ return_result(obj_return); }
	}
	catch(err)
	{
		return_result(obj_return)
	}
};


function __convert_arraySpace(array_val) {

	var list = [];

	if(typeof array_val !== 'undefined' && array_val !== null && array_val !== "")
	{
		if(array_val.toString().indexOf(' ') != -1 ){
			list = array_val.split(' ');
			list = list.map(v => v.toLowerCase());
		}
		else {
			list.push(array_val.toLowerCase());
		}
	}	
	return list;
}

function __convert_arrayComma(array_val) {

	var list = [];

	if(typeof array_val !== 'undefined' && array_val !== null && array_val !== "")
	{
		if(array_val.toString().indexOf(',') != -1 ){
			list = array_val.split(',');
			list = list.map(v => v.toLowerCase());
		}
		else {
			list.push(array_val.toLowerCase());
		}
	}	
	return list;
}

function intersect_arrays(a, b) {
    var sorted_a = a.concat().sort();
    var sorted_b = b.concat().sort();
    var common = [];
    var a_i = 0;
    var b_i = 0;

    while (a_i < a.length
           && b_i < b.length)
    {
        if (sorted_a[a_i] === sorted_b[b_i]) {
            common.push(sorted_a[a_i]);
            a_i++;
            b_i++;
        }
        else if(sorted_a[a_i] < sorted_b[b_i]) {
            a_i++;
        }
        else {
            b_i++;
        }
    }
    return common;
}

function outersect_arrays(a, b) {
	//a : ignoreList
	//b : keywordList
    var sorted_a = a.concat().sort();
    var sorted_b = b.concat().sort();
	var common = [];
	var keywordList = b;
    var a_i = 0;
    var b_i = 0;

	while (a_i < a.length && b_i < b.length)
	{
		if (sorted_a[a_i] === sorted_b[b_i]) {
			common.push(sorted_a[a_i]);
			a_i++;
			b_i++;
		}
		else if(sorted_a[a_i] < sorted_b[b_i]) {
			a_i++;
		}
		else {
			b_i++;
		}
	}

	if(parseInt(common.length) !== 0)
	{ 
		for (var i in common) 
		{ 
			keywordList.splice(keywordList.indexOf(common[i]), 1);
		}
	}

    return keywordList.join(' ');
}


//------------------------------------------------------------------- General Module End ---------------------------------------------------------------------------//


module.exports = methods;

