const methods = {};
const DB = require('./db_base.js');


methods.getAccountDetails = function(arrParams,return_result){

	var queAccountDetails = '';
	if(arrParams.account_type == '1' || arrParams.account_type == '2'){

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type, 
			CONCAT(MAX(CASE WHEN meta_key = 'fname' THEN meta_value END),' ',MAX(CASE WHEN meta_key = 'lname' THEN meta_value END)) AS fullname,
			MAX(CASE WHEN meta_key = 'profile_logo' THEN meta_value END) AS logo 
			FROM jaze_user_basic_details WHERE account_id=?`;

	}else{

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type,
			MAX(CASE WHEN meta_key = 'company_name' THEN meta_value END) AS fullname, 
			MAX(CASE WHEN meta_key = 'company_logo' THEN meta_value END) AS  logo  
			FROM jaze_user_basic_details WHERE account_id =?`;
	}

	DB.query(queAccountDetails,[arrParams.account_id], function(data, error){

	   	if(data != null){

			var newObj = {};
			var logo = '';
			var fullnmae = '';

		  	data.forEach(function(val,key,arr) {

		   		if(val.logo){
			  		if(val.logo != ''){
			  			if(val.account_type == '1'){
			  				logo = G_STATIC_IMG_URL+'uploads/jazenet/individual_account/members_thumb/thumb_'+val.logo;
			  			}else{
			  				logo = G_STATIC_IMG_URL+'uploads/companylogo/'+val.logo;
			  			}
			  		}
		  		}

		  		if(val.fullname){
	  				if(val.fullname !=''){
						fullnmae = val.fullname;
		  			}
		  		}
		  	
				newObj = { 
					account_id: val.account_id.toString(),
					account_type: val.account_type.toString(),
					name: fullnmae,
					logo: logo,
				};
		  	});

	 		return_result(newObj);
	
 	   	}else{
 	   		return_result(null);
 	   	}

	});
};

methods.getAccountDetailsFull = function(arrParams,return_result){

	var queAccountDetails = '';
	if(arrParams.account_type == '1' || arrParams.account_type == '2'){

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type, 
			CONCAT(MAX(CASE WHEN meta_key = 'fname' THEN meta_value END),' ',MAX(CASE WHEN meta_key = 'lname' THEN meta_value END)) AS fullname,
			MAX(CASE WHEN meta_key = 'profile_logo' THEN meta_value END) AS logo,
			MAX(CASE WHEN meta_key = 'email' THEN meta_value END) AS email,
			MAX(CASE WHEN meta_key = 'mobile' THEN meta_value END) AS mobile,
			MAX(CASE WHEN meta_key = 'mobile_country_code_id' THEN meta_value END) AS country_code
			FROM jaze_user_basic_details WHERE account_id=?`;

	}else{

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type,
			MAX(CASE WHEN meta_key = 'company_name' THEN meta_value END) AS fullname, 
			MAX(CASE WHEN meta_key = 'company_logo' THEN meta_value END) AS  logo,
			MAX(CASE WHEN meta_key = 'company_email' THEN meta_value END) AS email,
			MAX(CASE WHEN meta_key = 'company_phone' THEN meta_value END) AS mobile,
			MAX(CASE WHEN meta_key = 'company_phone_code' THEN meta_value END) AS country_code
			FROM jaze_user_basic_details WHERE account_id =?`;
	}

	DB.query(queAccountDetails,[arrParams.account_id], function(data, error){

	   	if(data != null){

			var newObj = {};
			var logo = G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png';
			var newPhoneCode;
			var fullname = '';
			
		  	data.forEach(function(val,key,arr) {

		  		if(val.logo != ''){
		  			if(val.account_type == '1'){
		  				logo = G_STATIC_IMG_URL+'uploads/jazenet/individual_account/members_thumb/thumb_'+val.logo;
		  			}else{
		  				logo = G_STATIC_IMG_URL+'uploads/companylogo/'+val.logo;
		  			}
		  		}

		  		if(val.fullname!=''){
		  			fullname = val.fullname;
		  		}

  				methods.getMobileCountryCode(val.country_code, function(phone_code){

  					if(phone_code!=null){
  						newPhoneCode = '+'+phone_code+val.mobile;
  					}else{
  						newPhoneCode = val.mobile;
  					}

					newObj = { 
						app_id: arrParams.new_app_id.toString(),
						api_token: arrParams.api_token,
						account_id: val.account_id.toString(),
						account_type: val.account_type.toString(),
						name: fullname,
						logo: logo,
						email: val.email,
						mobile: newPhoneCode
					};

					return_result(newObj);
		  		});

 	
		  	});

 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


methods.getAccountTypes = function(arrParams,return_result){


	var arrRows = [];
	var newObj = {};
	var arrAccountParams = {};
	var queLinkAccoType = `SELECT account_id,account_type FROM jaze_user_credential_details WHERE account_id =? `;
	DB.query(queLinkAccoType,[arrParams.account_id], function(data, error){

	   	if(data != null){

	   		function asyncFunction (item, cb) {
			  setTimeout(() => {
					return_result(item);
				cb();
			  }, 100);
			}

			let requests = data.map((val, key, array) => {
				arrAccountParams = { account_id:val.account_id, account_type:val.account_type};
				methods.getAccountDetails(arrAccountParams, function(returned){		

			    	return new Promise((resolve) => {
			      		asyncFunction(returned, resolve);
				    });

				});
			})

			Promise.all(requests);
	
 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


methods.getMyFollowingMeta = function(arrParams, return_result){


	var quer = `SELECT t1.* FROM(
		SELECT group_id,
		(CASE WHEN meta_key = 'follower_account_id' THEN meta_value END) AS follower_account_id,
		(SELECT c.meta_value FROM jaze_follower_groups_meta c WHERE c.meta_key = 'follower_group_id' AND c.group_id = jaze_follower_groups_meta.group_id) AS follower_group_id
		FROM  jaze_follower_groups_meta
		WHERE group_id IN (SELECT group_id FROM  jaze_follower_groups_meta  WHERE meta_key = 'follower_account_id' AND meta_value =? )
		) AS t1 WHERE follower_account_id != ""`;

	var newObj = {};
	var arrRows = [];
	var mainObj = {};

	DB.query(quer,[arrParams.account_id], function(data, error){


		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
					arrRows.push(val.follower_group_id);
			   		const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 50);
			  });
			}
			 
			forEach().then(() => {

				methods.getMyFollowingGroups(arrRows, function(retGroups){	

					var groups = {};
					if(retGroups!=null){
						groups = retGroups;
					}	

					methods.getMyLatestFollowingDate(arrParams, function(retDate){	

						methods.getAccountTypes(arrParams, function(retAccount){	

							mainObj = {
								account_id:arrParams.account_id,
								name:retAccount.name,
								logo:retAccount.logo,
								date:retDate.dates,
								groups:groups

							}

			   				return_result(mainObj);

		   				});

	   				});

	   			});

			})
 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};


methods.getMyFollowingGroups = function(arrParams, return_result){

	var quer = `SELECT * FROM jaze_follower_groups WHERE id IN (?) AND STATUS = '1' order by id asc`;

	var newObj = {};
	var arrRows = [];

	DB.query(quer,[arrParams], function(data, error){


		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){

			  		newObj = {
			  			group_id:val.id.toString(),
			  			group_name:val.group_name
			  		}

					arrRows.push(newObj);
			   		const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 50);
			  });
			}
			 
			forEach().then(() => {
		   		return_result(arrRows);
			})
 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};



methods.getMyLatestFollowingDate = function(arrParams,return_result){


	var quer = `SELECT t1.* FROM(
		SELECT group_id,
		(CASE WHEN meta_key = 'follower_account_id' THEN meta_value END) AS follower_account_id,
		(SELECT c.meta_value FROM jaze_follower_groups_meta c WHERE c.meta_key = 'joined_date' AND c.group_id = jaze_follower_groups_meta.group_id) AS joined_date
		FROM  jaze_follower_groups_meta
		WHERE group_id IN (SELECT group_id FROM  jaze_follower_groups_meta  WHERE meta_key = 'follower_account_id' AND meta_value =? )
		) AS t1 WHERE follower_account_id != "" ORDER BY group_id ASC`;

	var newObj = {};
	var arrRows = [];


	DB.query(quer,[arrParams.account_id], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){

					newObj = {
						group_id:val.group_id,
						dates:val.joined_date
					}

					arrRows.push(newObj);

			   		const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 50);
			  });
			}
			 
			forEach().then(() => {

				if(arrRows.length > 0 ){
					arrRows.sort(function (a, b) {
					    return a.group_id - b.group_id
					})
				}

				var min = arrRows[0];
		   		return_result(min);
			})

 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};



methods.checkMyFollowingMeta = function(arrParams, return_result){


	var quer = `SELECT t1.* FROM(
		SELECT group_id,
		(CASE WHEN meta_key = 'follower_account_id' THEN meta_value END) AS follower_account_id,
		(SELECT c.meta_value FROM jaze_follower_groups_meta c WHERE c.meta_key = 'follower_group_id' AND c.group_id = jaze_follower_groups_meta.group_id) AS follower_group_id
		FROM  jaze_follower_groups_meta
		WHERE group_id IN (SELECT group_id FROM  jaze_follower_groups_meta  WHERE meta_key = 'follower_account_id' AND meta_value =? )
		) AS t1 WHERE follower_account_id != ""`;

	var newObj = {};
	var arrRows = [];
	var mainObj = {};

	DB.query(quer,[arrParams.account_id], function(data, error){


		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
					arrRows.push(val.follower_group_id);
			   		const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 50);
			  });
			}
			 
			forEach().then(() => {

   				return_result(arrRows);
			})
 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};



methods.getAvailableNotAvailableGroups = function(arrGroupIds,arrParams, return_result){


	var quer = `SELECT * FROM jaze_follower_groups WHERE id NOT IN (?) and account_id =? ORDER BY id ASC`;

	var newObj = {};
	var arrRows = [];
	var mainObj = {};
	var newArr;

	DB.query(quer,[arrGroupIds,arrParams.following_id], function(data, error){


		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){

		  			newObj = {
						group_id:val.id.toString(),
						group_name:val.group_name,
						status:'0'
		  			}

					arrRows.push(newObj);
				
			   		const result = await returnData(arrRows);
			  	}

	
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
				var accParams = {account_id:arrParams.following_id};
		 		methods.getAccountTypes(accParams, function(retAccount){	

		 			methods.getAlreadyFollowed(arrGroupIds,arrParams, function(retAlready){	

		 				if(retAlready!=null){
							newArr = arrRows.concat(retAlready);
		 				}else{
		 					newArr = arrRows;
		 				}
	
		 		
				  		mainObj = {
				  			account_id:arrParams.following_id.toString(),
				  			name:retAccount.name,
				  			logo:retAccount.logo,
				  			groups:newArr
				  		}

						return_result(mainObj);

	  		 		});
	  		 	});

   			
			})
 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};


methods.getAlreadyFollowed = function(arrGroupIds,arrParams, return_result){


	var quer = `SELECT * FROM jaze_follower_groups WHERE id IN (?) and account_id =? ORDER BY id ASC`;

	var newObj = {};
	var arrRows = [];
	var mainObj = {};

	DB.query(quer,[arrGroupIds,arrParams.following_id], function(data, error){


		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){

		  			newObj = {
						group_id:val.id.toString(),
						group_name:val.group_name,
						status:'1'
		  			}

					arrRows.push(newObj);
				
			   		const result = await returnData(arrRows);
			  	}
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
				return_result(arrRows);
			})
 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});
};


methods.getMyFollowingGroupsPerFollowingID = function(arrParams, return_result){

	var quer;

	if(arrParams.id !='' && arrParams.id != '0'){

		quer = `SELECT * FROM jaze_follower_groups WHERE id IN `+'('+arrParams.arr_id+')';

	}else{
		quer = `SELECT * FROM jaze_follower_groups WHERE account_id =?`;
	}


	var newObj = {};
	var arrRows = [];
	var mainObj = {};

	DB.query(quer,[arrParams.following_id], function(data, error){


		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
					arrRows.push(val.id.toString());
			  	}

		  		const result = await returnData(arrRows);
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
				return_result(arrRows);
			})
 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});


};


methods.getMyFollowingGroupsMetaPerID = function(arrParams,arrGroupIDS, return_result){

	var quer = `SELECT t1.* FROM(
		SELECT group_id,
		(CASE WHEN meta_key = 'follower_account_id' THEN meta_value END) AS follower_account_id,
		(SELECT c.meta_value FROM jaze_follower_groups_meta c WHERE c.meta_key = 'follower_group_id' AND c.group_id = jaze_follower_groups_meta.group_id) AS follower_group_id
		FROM  jaze_follower_groups_meta
		WHERE group_id IN (SELECT group_id FROM  jaze_follower_groups_meta  WHERE meta_key = 'follower_account_id' AND meta_value =? )
		AND group_id IN (SELECT group_id FROM  jaze_follower_groups_meta  WHERE meta_key = 'follower_group_id' AND meta_value IN (?) )
		) AS t1 WHERE follower_account_id != ""`;

	var newObj = {};
	var arrRows = [];
	var mainObj = {};

	DB.query(quer,[arrParams.account_id,arrGroupIDS], function(data, error){


		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
					arrRows.push(val.group_id.toString());
			  	}

		  		const result = await returnData(arrRows);
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
				return_result(arrRows);
			})
 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});


};


methods.getGroupsToFollow = function(arrParams, return_result){

	var quer = `SELECT * FROM jaze_follower_groups WHERE id IN(?)`;

	var newObj = {};
	var arrRows = [];
	var mainObj = {};

	DB.query(quer,[arrParams.arr_id], function(data, error){


		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
					arrRows.push(val.id);
			  	}
			  	
		  		const result = await returnData(arrRows);
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
				return_result(arrRows);
			})
 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});


};


methods.delGroupsMetaFollowing = function(arrParams, return_result){


	var updateQue = `DELETE FROM jaze_follower_groups_meta WHERE group_id IN(?)`;

	return_result(1);
	DB.query(updateQue,[arrParams], function(data, error){

		if(data == null){
 	   		return_result(0);
 	   	}

	});
};


methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
}


methods.insertResult = function(arrInsert,querY,grpId,count,return_result){

	var insertedGrpid;
	insertedGrpid=grpId;

	console.log(arrInsert);

	if(count > 1)
	{
	  	arrInsert.forEach(function(row,indx) {
	  		insertedGrpid += 1;

		 	for (var key in row) 
		 	{
				DB.query(querY,[insertedGrpid,key,row[key]], function(data, error){
		 		 	if(data == null){
			 	   		return_result(0);
			 	   	}else{
		   				return_result(data.affectedRows);
			 	   	}
		 	 	});
		  	};

		});
	}
	else
	{

	 	for (var key in arrInsert) 
	 	{
			DB.query(querY,[insertedGrpid,key,arrInsert[key]], function(data, error){
	 		 	if(data == null){
		 	   		return_result(0);
		 	   	}else{
	   				return_result(data.affectedRows);
		 	   	}
	 	 	});
	  	};

	}


}

module.exports = methods;