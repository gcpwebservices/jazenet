const methods = {};
const DB = require('./db_base.js');

var randomBytes = require('randombytes');

var crypto = require('crypto'),
    algorithm = 'aes-256-ctr';

let key = crypto.createHash('sha256').update(String('jazenet123')).digest('base64').substr(0, 32);

const inputEncoding = 'utf8';
const outputEncoding = 'hex';

methods.checkParametersApiToken = function(arrParams,return_result){
	try
	{
		if(arrParams.api_token === "jazenet")
		{
			var return_data = [];

			return_data.push(arrParams.account_id);

			return_result(return_data);
		}
		else
		{
			DB.query('select * from jaze_app_device_details where ( app_type = 1 or app_type = 2) and  status = 1 and account_id=? and api_token=?', [arrParams.account_id,arrParams.api_token], function(data, error){
				if(data != null){
					return_result(data);
				}else{
					return_result(null);
				}

			});
		}
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};

methods.checkAccountCredentials = function(arrParams,return_result){
	try
	{
		DB.query('select * from jaze_user_credential_details where account_id=? and status=?', [arrParams.account_id,1], function(data, error){
			if(data != null){
				return_result(data);
			}else{
				return_result(null);
			}

		});
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};

methods.convertMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			Object.assign(list, {group_id : arrvalues[0].group_id});

			for (var i in arrvalues) 
			{
				Object.assign(list, {[arrvalues[i].meta_key]:  arrvalues[i].meta_value});
			}

			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray : ",err); return_result(null);}
};


methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};

function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

		if(list_array !== null)
		{
			Object.keys(list_array).forEach(function(prop) {

				var value = list_array[prop].group_id;
 
				if(parseInt(value) === parseInt(index_value))
				{
					retern_index = prop;
				}
			  });
		}
		
		return retern_index;
}

methods.convertAccountMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getAccountGroupIndex(list,arrvalues[i].account_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							account_type : arrvalues[i].account_type,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};

function __getAccountGroupIndex(list_array,index_value)
{
	var retern_index = "";

		if(list_array !== null)
		{
			Object.keys(list_array).forEach(function(prop) {

				var value = list_array[prop].account_id;
 
				if(parseInt(value) === parseInt(index_value))
				{
					retern_index = prop;
				}
			  });
		}
		
		return retern_index;
}

//-------------------------------------------------------------------  General TB Module Start ---------------------------------------------------------------------------//

methods.getNewGroupId = function(tbName,return_result){

	try{
		if(tbName !== "")
		{
			var query = " SELECT  CASE COALESCE(group_id, 0) WHEN  '0' THEN '1'  ELSE MAX(group_id) + 1   END as group_id FROM  "+tbName+ " ";

			DB.query(query,null, function(dataG, error){ 

				if(typeof dataG !== 'undefined' && dataG !== null  && parseInt(dataG.length) > 0){
						
					return_result(dataG[0].group_id); 						
				}
				else
				{ return_result(1); }
			});
		}
		else
		{ return_result(1); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

methods.selectQuery = function(query,return_result){

	try
	{
		if(query !== "" && query !== null)
		{
			DB.query(query, null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result(data);
				}
				else
				{ return_result(null); }
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log('selectQuery',err);
		return_result(null);
	}
}

methods.insertTBQuery = function(tbName,grpId,arrInsert,return_result){
	
	try{
		if(arrInsert !== null && tbName !== "" && grpId !== null)
		{			
			//check group_id exiisit 
			var query = " SELECT  *  FROM  "+tbName+ " where group_id = '"+grpId+"' limit 1";

			DB.query(query,null, function(dataG, error){ 
				
				if (typeof dataG !== 'undefined' && dataG !== null && parseInt(dataG.length) > 0){

					methods.getNewGroupId(tbName, function (group_id) {

						methods.insertTBQuery(tbName,group_id,arrInsert, function (queGrpId) {

							return_result(queGrpId);
						});
					});
				}
				else
				{
					var value_query = "";

					for (var meta_keys in arrInsert) 
					{
						value_query += (value_query  !== "") ? ',': '';	

						value_query += "('" + grpId + "','" + meta_keys + "','" + arrInsert[meta_keys] + "')";
					}
					

					if (value_query !== "") {

						var query = " INSERT INTO  "+tbName;
							query += " (`group_id`, `meta_key`, `meta_value`)";
							query += " VALUES ";
							query += value_query;

							// DB.query(query,null).then(response=>{
							// 	return_result(grpId);
							// }).catch(function(err){
							// 	return_result(null);
							// });

							DB.query(query, null, function (error,data) { 

								if(error)
								{
									return_result(grpId);
								}
								else
								{ return_result(null); }
							});
					}
					else
					{ return_result(null); }
				}
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

methods.updateQuery = function(tbName,grpId,arrInsert,return_result){

		try{
			if(tbName !== "" && grpId !== "" && arrInsert !== null)
			{
				var i = 0;

				for (var meta_key in arrInsert) 
				{
					var quer = " UPDATE  "+tbName+"  SET    meta_value = '"+arrInsert[meta_key]+"' where meta_key = '"+meta_key+"' and  group_id =  '"+grpId+"';";
						DB.query(quer,null, function(data, error){
					});
				};

				return_result(grpId);
			}	
			else
			{ return_result(0); }
		}
		catch(err)
		{
			console.log(err);
			return_result(0);
		}
}

methods.deleteQuery = function(query,return_result){

		try
		{
			if(query !== "" && query !== null)
			{
				DB.query(query, null, function(data, error){
	
					if(data !== null){
						return_result('1');
					}
					else
					{ return_result('0'); }
				});
			}
			else
			{ return_result('0'); }
		}
		catch(err)
		{
			console.log('deleteQuery',err);
			return_result('0');
		}
	}

//-------------------------------------------------------------------  General TB Module End ---------------------------------------------------------------------------//

methods.checkParametersApiTokenForAppID = function(arrParams,return_result){

	DB.query(`SELECT * FROM jaze_app_device_details WHERE  STATUS = '1' AND  app_type = '2' AND  api_token =? AND (id =? OR parent_id =?)`, [arrParams.api_token,arrParams.app_id,arrParams.app_id], function(data, error){
		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
			return_result(data[0]);
 	   	}else{
 	   		return_result(null);
 	   	}

	});
};

//-------------------------------------------------------------- Encryption & Decryption Start-------------------------------------------------------------//

// flag = 1 : Encryption || flag = 2 : Decryption

methods.jazeEncode = function(message,flag,return_result)
{
	try
	{
		if(parseInt(flag) == 1)
		{
			//generate IV
			const iv = Buffer.from(randomBytes(16));

			//encrypting message
			const cipher = crypto.createCipheriv(algorithm, key, iv);
			let crypted = cipher.update(message, inputEncoding, outputEncoding);
			crypted += cipher.final(outputEncoding);
			var result = `${iv.toString('hex')}:${crypted.toString()}`;
		}
		else
		{
			const textParts = message.split(':');

			//extract the IV from the first half of the value
			const IV = Buffer.from(textParts.shift(), outputEncoding);
		
			//extract the encrypted text without the IV
			const encryptedText = Buffer.from(textParts.join(':'), outputEncoding);
		
			//decipher the string
			const decipher = crypto.createDecipheriv(algorithm,key, IV);
			let decrypted = decipher.update(encryptedText,  outputEncoding, inputEncoding);
			decrypted += decipher.final(inputEncoding);
			var result = decrypted.toString();
		}

		return_result(result);
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

//-------------------------------------------------------------- Encryption & Decryption Start-------------------------------------------------------------//

//------------------------------------------------------------------- System Mailer Module Start ---------------------------------------------------------------------------//

methods.sendSystemMailer = function(arrParams,return_result)
{ 
	try
	{
		if(arrParams !== null)
		{
			const mailer = require('nodemailer-promise');

			var sendEmail = mailer.config({
				host: 'smtp.gmail.com',
			port: 465,
			secure: true, // true for 465, false for other ports
			auth: {
					user: 'donotreplyjazenet@gmail.com', // generated ethereal user
					pass: 'pv85@fnd'
					// generated ethereal password
			}
			});

			var message_arr = {
				from: 'do-not-reply@jazenet.com', 
				to: arrParams.to_email,
				cc: '',
				bcc: '',
				subject: arrParams.subject, 
				html: arrParams.message, 
				attachments: ''
			};

			sendEmail(message_arr).then(response=>{
				return_result(1);
				//console.log('info',response)
			}).catch(function(err){
				return_result(null);
			});
		}
		else
		{ return_result(null);  }
	}
	catch(err)
	{ console.log("sendSystemMailer",err); return_result(null); }	
}


//------------------------------------------------------------------- System Mailer Module Start ---------------------------------------------------------------------------//  


module.exports = methods;
