var mysql   = require("mysql2");

var pool = mysql.createPool({
    connectionLimit : 10,
    host: "104.247.79.227",
    user: "jazenet_root",
    password: "eIxm6}RZ6]bL",
    database: "jazenet_jazenetDB",
    waitForConnections: true,
    queueLimit: 0,
   // debug : true
});

// var pool = mysql.createPool({
//     connectionLimit: 10,
//     host: "localhost",
//     user: "root",
//     password: "",
//     database: "final_jazenetdb",
//     port: 3306,
//     waitForConnections: true,
//     queueLimit: 0
// });



var DB = (function () {

    function _query(query, params, callback) {
        pool.getConnection(function (err, connection) {
            if (err) {
                connection.release();
                callback(null, err);
                throw err;
            }

            connection.query(query, params, function (err, rows) {
                connection.release();
                if (!err) {
                    callback(rows);
                }
                else {
                    callback(null, err);
                }

            });

            connection.on('error', function (err) {
                connection.release();
                callback(null, err);
                throw err;
            });
        });
    };
    return {
        query: _query
    };
})();

module.exports = DB;