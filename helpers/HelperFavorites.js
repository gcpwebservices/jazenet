const methods = {};
const DB = require('./db_base.js');

const dateFormat = require('dateformat');
const now = new Date();


methods.getAccountDetails = function(arrParams,return_result){

	var queAccountDetails = '';
	if(arrParams.account_type == '1' || arrParams.account_type == '2'){

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type, 
			CONCAT(MAX(CASE WHEN meta_key = 'fname' THEN meta_value END),' ',MAX(CASE WHEN meta_key = 'lname' THEN meta_value END)) AS fullname,
			MAX(CASE WHEN meta_key = 'profile_logo' THEN meta_value END) AS logo 
			FROM jaze_user_basic_details WHERE account_id=?`;

	}else{

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type,
			MAX(CASE WHEN meta_key = 'company_name' THEN meta_value END) AS fullname, 
			MAX(CASE WHEN meta_key = 'company_logo' THEN meta_value END) AS  logo  
			FROM jaze_user_basic_details WHERE account_id =?`;
	}

	DB.query(queAccountDetails,[arrParams.account_id], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			var newObj = {};
			var logo = '';
			var fullnmae = '';

		  	data.forEach(function(val,key,arr) {

		   		if(val.logo){
			  		if(val.logo != ''){
			  			if(val.account_type == '1'){
			  				logo = G_STATIC_IMG_URL+'uploads/jazenet/individual_account/members_thumb/thumb_'+val.logo;
			  			}else{
			  				logo = G_STATIC_IMG_URL+'uploads/companylogo/'+val.logo;
			  			}
			  		}
		  		}

		  		if(val.fullname){
	  				if(val.fullname !=''){
						fullnmae = val.fullname;
		  			}
		  		}
		  	
				newObj = { 
					account_id: val.account_id.toString(),
					account_type: val.account_type.toString(),
					name: fullnmae,
					logo: logo,
				};
		  	});

	 		return_result(newObj);
	
 	   	}else{
 	   		return_result(null);
 	   	}

	});
};

methods.getAccountDetailsFull = function(arrParams,return_result){

	var queAccountDetails = '';
	if(arrParams.account_type == '1' || arrParams.account_type == '2'){

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type, 
			CONCAT(MAX(CASE WHEN meta_key = 'fname' THEN meta_value END),' ',MAX(CASE WHEN meta_key = 'lname' THEN meta_value END)) AS fullname,
			MAX(CASE WHEN meta_key = 'profile_logo' THEN meta_value END) AS logo,
			MAX(CASE WHEN meta_key = 'email' THEN meta_value END) AS email,
			MAX(CASE WHEN meta_key = 'mobile' THEN meta_value END) AS mobile,
			MAX(CASE WHEN meta_key = 'mobile_country_code_id' THEN meta_value END) AS country_code
			FROM jaze_user_basic_details WHERE account_id=?`;

	}else{

		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 
			MAX(CASE WHEN account_type THEN account_type END) AS account_type,
			MAX(CASE WHEN meta_key = 'company_name' THEN meta_value END) AS fullname, 
			MAX(CASE WHEN meta_key = 'company_logo' THEN meta_value END) AS  logo,
			MAX(CASE WHEN meta_key = 'company_email' THEN meta_value END) AS email,
			MAX(CASE WHEN meta_key = 'company_phone' THEN meta_value END) AS mobile,
			MAX(CASE WHEN meta_key = 'company_phone_code' THEN meta_value END) AS country_code
			FROM jaze_user_basic_details WHERE account_id =?`;
	}

	DB.query(queAccountDetails,[arrParams.account_id], function(data, error){

	   	if(data != null){

			var newObj = {};
			var logo = G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png';
			var newPhoneCode;
			var fullname = '';
			
		  	data.forEach(function(val,key,arr) {

		  		if(val.logo != ''){
		  			if(val.account_type == '1'){
		  				logo = G_STATIC_IMG_URL+'uploads/jazenet/individual_account/members_thumb/thumb_'+val.logo;
		  			}else{
		  				logo = G_STATIC_IMG_URL+'uploads/companylogo/'+val.logo;
		  			}
		  		}

		  		if(val.fullname!=''){
		  			fullname = val.fullname;
		  		}

  				methods.getMobileCountryCode(val.country_code, function(phone_code){

  					if(phone_code!=null){
  						newPhoneCode = '+'+phone_code+val.mobile;
  					}else{
  						newPhoneCode = val.mobile;
  					}

					newObj = { 
						app_id: arrParams.new_app_id.toString(),
						api_token: arrParams.api_token,
						account_id: val.account_id.toString(),
						account_type: val.account_type.toString(),
						name: fullname,
						logo: logo,
						email: val.email,
						mobile: newPhoneCode
					};

					return_result(newObj);
		  		});

 	
		  	});

 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


methods.getAccountTypes = function(arrParams,return_result){


	var arrRows = [];
	var newObj = {};
	var arrAccountParams = {};
	var queLinkAccoType = `SELECT account_id,account_type FROM jaze_user_credential_details WHERE account_id =? `;
	DB.query(queLinkAccoType,[arrParams.account_id], function(data, error){

	   	if(data != null){

	   		function asyncFunction (item, cb) {
			  setTimeout(() => {
					return_result(item);
				cb();
			  }, 100);
			}

			let requests = data.map((val, key, array) => {
				arrAccountParams = { account_id:val.account_id, account_type:val.account_type};
				methods.getAccountDetails(arrAccountParams, function(returned){		

			    	return new Promise((resolve) => {
			      		asyncFunction(returned, resolve);
				    });

				});
			})

			Promise.all(requests);
	
 	   	}else{
 	   		return_result(null);
 	   	}

	});
};


methods.checkFavorites = function(arrParams,return_result){

	var checkFav = `SELECT t1.* FROM(
	SELECT id,group_id
		FROM  jaze_user_favorite_list
	WHERE group_id IN (SELECT group_id FROM  jaze_user_favorite_list  WHERE meta_key = 'account_id' AND meta_value =? )
	AND group_id IN (SELECT group_id FROM  jaze_user_favorite_list  WHERE meta_key = 'fav_account_id' AND meta_value =? )
	AND group_id IN (SELECT group_id FROM  jaze_user_favorite_list  WHERE meta_key = 'fav_account_type' AND meta_value =? )
	) AS t1 LIMIT 1`;

	DB.query(checkFav,[arrParams.account_id,arrParams.fav_account_id,arrParams.fav_account_type], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
			return_result(data[0].group_id);
 	   	}else{
 	   		return_result(null);
 	   	}

	});
};

methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
}

methods.insertResult = function(arrInsert,querY,grpId,return_result){

	var insertedGrpid;
	insertedGrpid=grpId;

 	for (var key in arrInsert) 
 	{
		DB.query(querY,[grpId,key,arrInsert[key]], function(data, error){
 		 	if(data == null){
	 	   		return_result(0);
	 	   	}else{
   				return_result(data.affectedRows);
	 	   	}
 	 	});
  	};
}

methods.updateQuery = function(querY,arrUpdate,group_id,return_result){

	for (var key in arrUpdate) 
 	{	
 		DB.query(querY, [arrUpdate[key],key,group_id], function(data, error){
 			if(data == null){
	 	   		return_result(0);
	 	   	}else{
	  			return_result(data.affectedRows);
	 	   	}
		});
  	};
}


methods.deleteQuery = function(qry,arrDelete,return_result){

	DB.query(qry, [arrDelete], function(data, error){
 	   	if(data != null){
			return_result(data.affectedRows);
 	   	}else{
 	   		return_result(0);
 	   	}
	});
}


methods.getAppID = function(arrParams,return_result){

	var queLinkAccoType = `SELECT account_id,account_type,parent_id FROM jaze_app_device_details WHERE id =? AND api_token=?`;
	DB.query(queLinkAccoType,[arrParams.app_id,arrParams.api_token], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			var arrMyDetails = { account_id:data[0].account_id};

			methods.getAccountTypes(arrMyDetails, function(retMyAccount){ 

				var category;
				if(retMyAccount.account_type == '1'){
					category = 'User';
				}else if(retMyAccount.account_type == '2'){
					categoryretMyAccount = 'Professional';
				}else if(arrParams.account_type == '3'){
					category = 'Company';
				}

				var newObj = {
					account_id:retMyAccount.account_id,
					name:retMyAccount.name,
					logo:retMyAccount.logo,
					category:category,
					account_type:retMyAccount.account_type.toString()

				}

				return_result(newObj);

			});

 	   	}else{
 	   		return_result(null);
 	   	}

	});
};



methods.getFavoritesListAllDetails = function(arrParams,return_result){

	var arrRows = [];
	var newObj = {};

	var quer = `SELECT t1.* FROM(
			SELECT id,group_id,
			(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
			(SELECT b.meta_value FROM jaze_user_favorite_list b WHERE b.meta_key = 'fav_account_id' AND b.group_id = jaze_user_favorite_list.group_id) AS fav_account_id,
			(SELECT c.meta_value FROM jaze_user_favorite_list c WHERE c.meta_key = 'fav_account_type' AND c.group_id = jaze_user_favorite_list.group_id) AS fav_account_type,
			(SELECT d.meta_value FROM jaze_user_favorite_list d WHERE d.meta_key = 'fav_type' AND d.group_id = jaze_user_favorite_list.group_id) AS reminder_mins,
			(SELECT f.meta_value FROM jaze_user_favorite_list f WHERE f.meta_key = 'create_date' AND f.group_id = jaze_user_favorite_list.group_id) AS create_date
			FROM  jaze_user_favorite_list
	WHERE group_id IN (SELECT group_id FROM  jaze_user_favorite_list  WHERE meta_key = 'account_id' AND meta_value =? )
	) AS t1 WHERE account_id != ""`;

	DB.query(quer,[arrParams.account_id], function(data, error){

	if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
                            
			var newObj = {};
			var arrRows = [];

			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
					var arrAccountParams = { account_id:val.fav_account_id, account_type:val.fav_account_type};
					methods.getAccountDetails(arrAccountParams, function(returned){ 

						newObj = {
							account_id:arrParams.account_id,
							acccount_name:arrParams.name,
							account_type:arrParams.account_type,
							account_category:arrParams.category,
							fav_id:val.group_id.toString(),
							name:returned.name,
							logo:returned.logo,
							email_address:val.jazemail_id,
							fav_account_id:val.fav_account_id,
							fav_account_type:val.fav_account_type,
							fav_type:val.fav_type,
							create_date:val.create_date

						};
						
						arrRows.push(newObj);

					});
			  	}

		  		const result = await returnData(arrRows);
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
				return_result(arrRows);
			})

                
        }
        else
        { 
        	return_result(null);
        }

	});
};

module.exports = methods;