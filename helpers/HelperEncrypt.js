const methods = {};
const DB = require('../helpers/db_base.js');

const promiseForeach = require('promise-foreach');
	
const crypto = require("crypto");
const CryptoAlgorithm = "AES-256-CBC"; //"aes-256-cbc";

//let key = crypto.createHash('sha256').update(String(MAILKEY)).digest('base64').substr(0, 32);
let key = crypto.createHash('sha1').update(String(MAILKEY)).digest('hex').substr(0, 32);

var randomBytes = require('randombytes');

const fs = require('fs');
const path = require('path');
const multer = require('multer');

//-- random number generator --//
var rn = require('random-number');
var options = { min:  100, max:  100000, integer: true };

//------------------------------------------------------------------- Encryption & Decryption Start --------------------------------------------------------------------//

/*
* file encrypt new
*/
methods.fileEncrypt = function(files, path_type, return_result)
{
	if(typeof files !== undefined && files !== null && parseInt(files.length) > 0 && parseInt(path_type) !== 0){

		if(parseInt(path_type) == 1){

			//var file_path = "C:/xampp/htdocs/img/node/";

			var file_path = G_STATIC_IMG_PATH+'/jazemail/attachments/';

			if (!fs.existsSync(path.dirname(file_path))) 
			{
				fs.mkdirSync(path.dirname(file_path));
			}
		}
		else
		{
			var file_path = '';
		}

		var counter = files.length;

		if(parseInt(counter) !== 0 && file_path !== '')
		{
			var return_files = [];

			promiseForeach.each(files,[function (each_file) {

				return new Promise(function (resolve, reject) {

					//generate IV
					const iv = Buffer.from(randomBytes(16)).toString('hex').toString().substr(0,16); 

					//encryption
					const encrypted = encrypt(each_file.buffer, CryptoAlgorithm, key, iv);

					//file name	
					each_file.originalname = each_file.originalname.replace(/ /g,'');	

					filePath = __setNewFileName(each_file.originalname, file_path);

					if(filePath !== '' && filePath !== 'undefined')
					{
						fs.writeFileSync(filePath.file_Path, encrypted);						

						var file_details = {
							file_id 	: filePath.file_name,
							file_name	: each_file.originalname,
							iv 			: iv
						}					  
										
					}					

					counter -= 1;

					return_files.push(file_details);

					if (counter === 0)
					{ 
						resolve(return_files);
					}		
				})
			}],
			function (result, current) {
				if (counter === 0){ 
					return_result(result[0]);
				}
			});
		}
		else
		{ return_result(null); }			
	}
	else
	{ return_result(null); }
};

/**
* copy and encrypt given file 
*/
methods.fileCopyEncrypt = function(files, return_result)
{
	if (files.indexOf(',') > -1) 
	{
		var files_arr = files.split(',');
	}
	else
	{
		var files_arr = files.split();
	}

	var counter = files_arr.length;

	if(parseInt(counter) !== 0)
	{
		var return_files = [];

		

		var source_file_path = './uploads/pdf/';

		var destination_file_path = './uploads/others/mail/';

		// var source_file_path = G_STATIC_IMG_PATH+'/jazemail/attachments/others/';

		// var destination_file_path = G_STATIC_IMG_PATH+'/jazemail/attachments/';

		promiseForeach.each(files_arr,[function (each_file) {

			return new Promise(function (resolve, reject) {

				if (fs.existsSync(source_file_path+each_file)) 
				{
					fs.readFile(source_file_path+each_file, function (err, data) {
						
						//generate IV
						const iv = Buffer.from(randomBytes(16)).toString('hex').toString().substr(0,16); 

						//encryption
						const encrypted = encrypt(data, CryptoAlgorithm, key, iv);

						//file name	
						each_file.originalname = each_file.replace(/ /g,'');	

						filePath = __setNewFileName(each_file, destination_file_path);

						if(filePath !== '' && filePath !== 'undefined')
						{
							fs.writeFileSync(filePath.file_Path, encrypted);	

							fs.unlinkSync(source_file_path+each_file);

							var file_details = {
								file_id 	: filePath.file_name,
								file_name	: each_file,
								iv 			: iv
							}					  
											
						}					

						counter -= 1;

						return_files.push(file_details);

						if (counter === 0)
						{ 
							resolve(return_files);
						}	
					});
				}
				else
				{
					counter -= 1;

					if (counter === 0)
					{ 
						resolve(return_files);
					}	
				}
			})
		}],
		function (result, current) {
			if (counter === 0){ 
				return_result(result[0]);
			}
		});
	}
	else
	{ return_result(null); }	
};

/*
* encryption method 
*/
function encrypt (plain_text, encryptionMethod, secret, iv) {
	var encryptor = crypto.createCipheriv(encryptionMethod, secret, iv);
	return encryptor.update(plain_text, 'utf8', 'base64') + encryptor.final('base64');
};

/*
* get new file name if exists
*/
function __setNewFileName(filename, file_path){

	var return_result = null;

	if(filename !== '' && file_path !== ''){

		fileName = rn(options)+Date.now()+'_'+filename;

		filePath = path.join(file_path, fileName);

		filePath = path.join(path.dirname(filePath), path.basename(filePath, path.extname(filePath)) + path.extname(filePath));

		if (fs.existsSync(filePath)) 
		{				
			__setNewFileName(filename, file_path);
		} 
		else 
		{
			return_result = { file_name : fileName,
							  file_Path	: filePath
							};
		}
	}
	else
	{
		return_result = null;		
	}	

	return return_result;
}

//-------------------------------------------------------------- Encryption & Decryption End-------------------------------------------------------------//

module.exports = methods;
