module.exports = {
  apps : [{
    name: 'API',
    script: 'app.js',
     args: 'one two',
    instances: 6,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    "production" : {
      user : "node",
      host : "212.83.163.1",
      repo : "git@github.com:repo.git",
      ref  : "origin/master",
      path : "/var/www/production",
      'post-deploy' : "pm2 startOrRestart ecosystem.config.js --env production"
    },
    dev : {
      user : "node",
      host : "212.83.163.1",
      repo : "git@github.com:repo.git",
      ref  : "origin/master",
      path : "/var/www/development",
      'post-deploy' : "pm2 startOrRestart ecosystem.config.js --env production"
    }
  }
};

