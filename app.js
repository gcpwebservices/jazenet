'use strict';
// content of app.js
require('events').EventEmitter.defaultMaxListeners = 100;
require('events').EventEmitter.prototype._maxListeners = 100;

 const fs = require('fs');
 const privateKey  = '';
 const certificate = '';
// const privateKey  = fs.readFileSync('/home/chaaby/ssl/keys/c4585_d4e4d_7ef9e4ffaf24af200da07830683ff0c1.key', 'utf8');
// const certificate = fs.readFileSync('/home/chaaby/ssl/certs/chaaby_com_c4585_d4e4d_1614643199_bce2f0aa8277a12e5055cc98d0e83bee.crt', 'utf8');

 const credentials = {key: privateKey, cert: certificate};


 const   express = require('express');
 const   bodyParser = require('body-parser');
 const   app = express();

 app.use(bodyParser.urlencoded({extended: false}));


  // var server = require('https').createServer(credentials,app),
  //     io = require('socket.io').listen(server),
  //     app_socket_io = require('./app_socket_io')(io);



 var admin_firebase = require('firebase');

 //initialize firebase
  const firebaseConfig = {
     apiKey: "AIzaSyBfHt55NN9EhIjZmSz3etCHDN7zxUKYsqo",
     authDomain: "jazechat.firebaseapp.com",
     databaseURL: "https://jazechat.firebaseio.com",
     projectId: "jazechat",
     storageBucket: "jazechat.appspot.com",
     messagingSenderId: "118107995157",
     appId: "1:118107995157:web:9fd5f473c4a507d9"
  };
 
  var firebase_app =  admin_firebase.initializeApp(firebaseConfig);
   
  module.exports.firebase_app = firebase_app;

  
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


//Global Static Link
global.LOCATION_CITY_LIST = [34917,30611,32870,34250,31654,34732,32618,33437,34639,34952];
global.G_STATIC_WEB_URL = "https://www.staging.jazenet.com/";
global.G_STATIC_IMG_URL = "https://www.img.jazenet.com/images/master.resources/";
global.G_STATIC_IMG_PATH = "/home/imgjazenet/public_html/images/master.resources/uploads/jazenet/";



global.MAILKEY = "jazenet1986JazeEmail";


// //include files
const   jazenet_general = require('./jazenet_general.js');
const   jazenet_voice = require('./jazenet_voice.js');

const   fcm_pushnotification = require('./fcm_push_notification.js');



const   jazecom = require('./jazecom/jazecom.js');
const   jazechat_api = require('./jazecom/jazechat.js');
const   jazediary_api = require('./jazecom/jazediary.js');
const   jazefeed_api = require('./jazecom/jazefeed.js');
const   jazemail_api = require('./jazecom/jazeemail.js');

const   jazenet = require('./jazenet/app');


app.use(jazenet_general);
app.use(jazenet_voice);

app.use(fcm_pushnotification);

app.use(jazecom);
app.use(jazechat_api);
app.use(jazediary_api);
app.use(jazefeed_api);
app.use(jazemail_api);


// jazenet web start
app.use(jazenet);
// jazenet web end
const http = require('http');
const port = process.env.PORT || 8080;

const server = http.createServer(app);

server.listen(port);

// app.set('port', (3000));

// app.listen(app.get('port'), () => {
//     console.log('Server lancé sur le port ' + app.get('port'));
// });


app.get('/', function (req, res) {
  res.send('Hello World!');
});



