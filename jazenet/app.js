const express = require('express');

const app = express();



const bodyParser = require('body-parser');
global.MAILKEY = "jazenet1986JazeEmail";

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

const jazenet = require('./routes/jazenet');
const general = require('./routes/general');
const account = require('./routes/account');
const notification = require('./routes/notification');
const jazecategory = require('./routes/jazecategory');
const jazeproduct = require('./routes/jazeproduct');
const webnotification = require('./routes/pushnotification');
const documents = require('./routes/documents');
const company = require('./routes/company');
const professional = require('./routes/professional');
const jobs = require('./routes/jazejobs');
const jazeevents = require('./routes/events');
const following = require('./routes/following');
const jazepost = require('./routes/jazepost');
const jazeteam = require('./routes/jazeteam');
const jazestore = require('./routes/jazestore');
const jazepay = require('./routes/jazepay');
const jazereceivable = require('./routes/jazereceivable');

app.use('/jazenet/', webnotification);
app.use('/jazenet/', jazenet);
app.use('/jazenet/', general);
app.use('/jazenet/', account);
app.use('/jazenet/', notification);
app.use('/jazenet/', jazecategory);
app.use('/jazenet/', jazeproduct);
app.use('/jazenet/', documents);
app.use('/jazenet/', company);
app.use('/jazenet/', professional);
app.use('/jazenet/', jobs);
app.use('/jazenet/', jazeevents);
app.use('/jazenet/', following);
app.use('/jazenet/', jazepost);
app.use('/jazenet/', jazeteam);
app.use('/jazenet/', jazestore);
app.use('/jazenet/', jazepay);
app.use('/jazenet/', jazereceivable);



module.exports = app;





