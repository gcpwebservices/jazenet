require('events').EventEmitter.prototype._maxListeners = 1000;
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const express = require('express');
const router = express.Router();

const HelperRestriction = require('./helpers/HelperRestriction.js');
const HelperFollowers = require('./helpers/HelperFollowers.js');

const { check, validationResult } = require('express-validator');

const dateFormat = require('dateformat');
const now = new Date();
const standardDT = dateFormat(now,'yyyy-mm-dd HH:MM:ss');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};


router.post('/followers/follower_groups',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var  account_id = req.body.account_id;
	var  api_token = req.body.api_token;



	var arrParams = {
		account_id:account_id,
		api_token:api_token
	};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{

			HelperFollowers.getFollowerGroupsBusiness(arrParams, function(retGroupBus){

				HelperFollowers.getFollowerGroupsSpecial(arrParams, function(retGroupSpe){


					var retBus;
					var retSpe;

					if(retGroupBus!=null && retGroupSpe!=null){

						retBus = retGroupBus;
						retSpe = retGroupSpe;

					}else if(retGroupBus!=null && retGroupSpe==null){

						retBus = retGroupBus;
						retSpe = {};


					}else if(retGroupBus==null && retGroupSpe!=null){

						retBus = {};
						retSpe = retGroupSpe;
					}


					jsonRes = {
						success: '1',
						result: {business: retBus,special:retSpe},
						successMessage: "Groups",
						errorMessage: "" 
			  		};

		  			res.status(201).json(jsonRes);
		 			res.end();

				});
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

});


router.post('/followers/followers_list',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var  account_id = req.body.account_id;
	var  api_token = req.body.api_token;



	var arrParams = {
		account_id:account_id,
		api_token:api_token
	};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{

			HelperFollowers.getFollowerList(arrParams, function(retGroups){

					
				if(retGroups!=null){

					jsonRes = {
						success: '1',
						result: {followers:retGroups},
						successMessage: "Followers",
						errorMessage: "" 
			  		};

		  			res.status(201).json(jsonRes);
		 			res.end();

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
			  		};

		  			res.status(statRes).json(jsonRes);
		 			res.end();
		
				}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

});


router.post('/followers/edit_groups',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('id').isNumeric(),
	check('group_name').isLength({ min: 1 }),
	check('flag').isLength({ min: 1 })


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  account_id = req.body.account_id;
	var  api_token = req.body.api_token;
	var  id = req.body.id;
	var  group_name = req.body.group_name;
	var  flag = req.body.flag;

	var arrParams = {
		account_id:account_id,
		api_token:api_token,
		id:id,
		group_name:group_name,
		flag:flag
	};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{
			HelperFollowers.getGroupById(arrParams, function(retGroup){

				if(retGroup!=null)
				{
					if(flag == '1')
					{

						HelperFollowers.updateGroup(arrParams, function(retGroup){

							if(retGroup!=0){

								setTimeout(function(){ 
									HelperFollowers.getGroupById(arrParams, function(retGroupDone){

										var obj = {}
										var main_obj = {};

										obj = {
											id:retGroupDone.id.toString(),
											group_name:retGroupDone.group_name
										}

										if(retGroupDone.group_type == '1'){

											main_obj = {business:obj};
											successMessage = 'Business group is successfully edited.';

										}else{

											main_obj = {special:obj};
											successMessage = 'Special group is successfully edited.';

										}

										jsonRes = {
											success: '1',
											result: main_obj,
											successMessage: successMessage,
											errorMessage: "" 
								  		};

							  			res.status(201).json(jsonRes);
							 			res.end();

									});

						  		}, 500);

							}else{

								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
						  		};

					  			res.status(statRes).json(jsonRes);
					 			res.end();
			
							}

						});

					}
					else
					{

						var retStatus = retGroup.group_type;

						HelperFollowers.delGroup(arrParams, function(retDel){

							if(retDel!=0){

								if(retStatus == '1'){
									successMessage = 'Business group is successfully deleted.';
								}else{
									successMessage = 'Special group is successfully deleted.';
								}

								jsonRes = {
									success: '1',
									result: {},
									successMessage: successMessage,
									errorMessage: "" 
						  		};

					  			res.status(201).json(jsonRes);
					 			res.end();

							}else{

								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
						  		};

					  			res.status(statRes).json(jsonRes);
					 			res.end();
		
							}

						});
					}

				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
			  		};

		  			res.status(statRes).json(jsonRes);
		 			res.end();
		
				}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

});



router.post('/followers/edit_followers',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('id').isLength({ min: 1 }),
	check('follower_id').isNumeric(),
	check('flag').isLength({ min: 1 })


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  account_id = req.body.account_id;
	var  api_token = req.body.api_token;
	var  id = req.body.id;
	var  follower_id = req.body.follower_id;
	var  flag = req.body.flag;
	var  status = req.body.status;

	var arrID = id.split(",");

	var arrParams = {
		account_id:account_id,
		api_token:api_token,
		arr_ids:arrID,
		follower_id:follower_id,
		flag:flag,
		status:status
	};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{
		
			if(flag == '0')
			{	

				HelperFollowers.getOwnGroups(arrParams, function(retGroup){

					if(retGroup!=null)
					{	
			
						HelperFollowers.delGroupsMetaFollowers(retGroup, function(retDelStatus){

							if(retDelStatus!=0){

								jsonRes = {
									success: '1',
									result: {},
									successMessage: 'Account is successfully removed to the groups.',
									errorMessage: "" 
						  		};

					  			res.status(201).json(jsonRes);
					 			res.end();

							}else{

								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
						  		};

					  			res.status(statRes).json(jsonRes);
					 			res.end();

							}

						});

					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
				  		};

			  			res.status(statRes).json(jsonRes);
			 			res.end();
			
					}

				});


			}
			else if(flag == '1')
			{

				HelperFollowers.getOwnGroupsAndId(arrParams, function(retGroups){

					if(retGroups!=null)
					{

						var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_follower_groups_meta";
				  		var lastGrpId = 0;

						HelperFollowers.getLastGroupId(queGroupMax, function(queGrpRes){
		
						  	queGrpRes.forEach(function(row) {
								lastGrpId = row.lastGrpId;
						  	});

				  			var plusOneGroupID;
				  			var arrInsert;
				  			var arrToInsert = [];

	  				  		if(arrID.length > 1){
		  				
					  		  	arrID.forEach(function(row) {

									arrInsert = {
										follower_group_id:row,
										follower_account_id:follower_id,
										joined_date:standardDT,
										status:1
									};

									arrToInsert.push(arrInsert);

								});

		  		  			}else{

	  							arrInsert = {
									follower_group_id:id,
									follower_account_id:follower_id,
									joined_date:standardDT,
									status:1
								};

								arrToInsert = arrInsert;
								lastGrpId = lastGrpId + 1;
		  		  			}

					  		var querInsert = `insert into jaze_follower_groups_meta (group_id, meta_key, meta_value) VALUES (?,?,?)`;
							HelperFollowers.insertResult(arrToInsert,querInsert,lastGrpId,arrID.length, function(insertRes){	

								jsonRes = {
									success: '1',
									result: {},
									successMessage: 'Account is successfully added to the groups.',
									errorMessage: "" 
						  		};

					  			res.status(201).json(jsonRes);
					 			res.end();


							});

						});

					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
				  		};

			  			res.status(statRes).json(jsonRes);
			 			res.end();



					}

				});

			}
			else if(flag == '2')
			{

				if(status == '1' || status == '2')
				{

					HelperFollowers.getOwnGroups(arrParams, function(retGroups){

						if(retGroups!=null)
						{

							HelperFollowers.updateBlockUnblock(arrParams,retGroups, function(retUpdate){

								if(retUpdate!=0){

									if(status == '1'){
										successMessage = 'Follower is successfully unblocked.';
									}else if(status == '2'){
										successMessage = 'Follower is successfully blocked.';
									}else{
										successMessage = '';
									}

									jsonRes = {
										success: '1',
										result: {},
										successMessage: successMessage,
										errorMessage: "" 
							  		};

						  			res.status(201).json(jsonRes);
						 			res.end();

								}else{

									jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "Unable to process request. Please try again." 
							  		};

						  			res.status(statRes).json(jsonRes);
						 			res.end();


								}

			 				});
						}
						else
						{
							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "No result found." 
					  		};

				  			res.status(statRes).json(jsonRes);
				 			res.end();

						}

					});

				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid status provided." 
			  		};

		  			res.status(statRes).json(jsonRes);
		 			res.end();
				}

			}
			else
			{

				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
		  		};

	  			res.status(statRes).json(jsonRes);
	 			res.end();
			}

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

});



module.exports = router;