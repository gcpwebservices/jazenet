const methods = {};
const DB = require('../../helpers/db_base.js');
const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperGeneral = require('../../helpers/HelperGeneral.js');
const HelperNotification = require('../../jazecom/helpers/HelperNotification.js');
const promiseForeach = require('promise-foreach');
const array_unique = require('array-unique');
const fast_sort = require("fast-sort");
const moment = require("moment");

//------------------------------------------------------------------- Follow Module Start ---------------------------------------------------------------------------//

methods.getFollowingList  =  function (reqParams,return_result){
	
	try
	{
		if(reqParams !== null)
		{
			var query  = " SELECT * FROM jaze_follower_groups_meta WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'account_id'  AND meta_value = "+reqParams.account_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'status' AND meta_value in (1,2))) ";

				if(typeof reqParams.follow_account_id !== 'undefined' && parseInt(reqParams.follow_account_id) > 0)
				{
					query += " AND group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'follow_account_id'  AND meta_value = "+reqParams.follow_account_id+")  ";
				}

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(retConverted){	
							
							var array_rows = [];

							var counter = retConverted.length;
							
							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(retConverted,[function (list) {

										return new Promise(function (resolve, reject) {

											var sendParam = { account_id: list.follow_account_id,groups:list.follow_groups}; 
											
											methods.__getCompanyGroupList(sendParam, function(groupList,count){	

												var arrParams = { account_id :list.follow_account_id };
												HelperGeneral.getAccountDetails(arrParams, function(accRet){	

													methods.___getAccountJazecomStatus(accRet, function(jazecom_status){
													
														var arrObj = {
															follow_id 		: list.group_id.toString(),
															account_id 		: accRet.account_id,
															account_type 	: accRet.account_type,
															name       		: accRet.name,
															logo       		: accRet.logo,
															title   		: accRet.category,
															date       		:  list.create_date.split(" ")[0],
															following_count : count.toString(),
															jaze_chat		: jazecom_status.jaze_chat.toString(),
															jaze_mail 		: jazecom_status.jaze_mail.toString(),
															jaze_call 		: jazecom_status.jaze_call.toString(),
															jaze_feed 		: jazecom_status.jaze_feed.toString(),
															groups     		: groupList
														}
														
														array_rows.push(arrObj);
														
														counter -= 1;

														resolve(array_rows);
														
													});
												});
											});
										});
									}],
									function (result, current) {
										
										if (counter === 0)
										{ return_result(fast_sort(result[0]).asc('name')); }
									});	
							}
							else
							{return_result(null)}
						});
					}
					else
					{return_result(null)}
				});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("getFollowingList",err); return_result(null); }	
};


methods.__getCompanyGroupList = function(reqParams,return_result){

	var objReturn = [{
		id					: "0",
		title		  		: "General",
		following_status 	: "0"
	}];
	
	try{
			if(reqParams !== null)
			{
				var query  = " SELECT * FROM jaze_follower_groups WHERE status = 1 and account_id = "+reqParams.account_id+" and  group_type = '1' ";
				
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var array_rows = [];

						var group_count = 0;

						var group_split = reqParams.groups.split(',');

						for (var i in data) 
						{
							var follow_status = "0";

							if(reqParams.groups.indexOf(data[i].id) !== -1)
							{ follow_status = "1"; group_count += 1; }


							var objAccRres = {
								id					: data[i].id.toString(),
								title		  		: data[i].group_name.toString(),
								following_status 	: follow_status.toString()
							};	
		
							array_rows.push(objAccRres);
						}
						
						if(group_split.includes("0"))
						{ 
							var objAccRres = {
								id					: "0",
								title		  		: "General",
								following_status 	: "1"
							};

							group_count += 1;

							array_rows.push(objAccRres);
						}

						return_result(array_rows,group_count);
					}
					else
					{
						if(typeof reqParams.groups !== 'undefined' && reqParams.groups !== null)
						{
							if(parseInt(reqParams.groups) === 0 || reqParams.groups === "0")
							{
								objReturn = [{
									id					: "0",
									title		  		: "General",
									following_status 	: "1"
								}];

								return_result(objReturn,1);
								
							}
							else 
							{ return_result(objReturn,0); }
						}
						else 
						{ return_result(objReturn,0); }
					}
				});
			}
			else
			{ return_result(objReturn,0); }		
	}
	catch(err)
	{console.log("__getCompanyGroupList",err); return_result(null,0)}
};

methods.___getAccountJazecomStatus= function(arrParams,return_result)
{ 
	var objResult = { 
		jaze_diary : '0',
		jaze_feed  : '0',
		jaze_call  : '0',
		jaze_mail  : '0',
		jaze_chat  : '0',
	};

	try
	{ 
		if(arrParams !== null)
		{
			if(parseInt(arrParams.account_type) === 4)
			{
				var query  = " SELECT * FROM jaze_user_basic_details WHERE meta_key in ('team_diary_status','team_mail_status','team_chat_status','team_call_status') and  `account_id` IN (SELECT `account_id`  FROM jaze_user_basic_details WHERE `meta_key` = 'team_status' AND meta_value = '1' ) ";
					query += " and account_id = '"+arrParams.account_id+"' and account_type = '4' ";

					DB.query(query, null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							var list = [];

							Object.assign(list, {account_id : data[0].account_id});
							Object.assign(list, {account_type : data[0].account_type});

							for (var i in data) 
							{
								Object.assign(list, {[data[i].meta_key]:  data[i].meta_value});
							}

							objResult = { 
									jaze_diary : list.team_diary_status.toString(),
									jaze_feed  : '0',
									jaze_call  : list.team_call_status.toString(),
									jaze_mail  : list.team_mail_status.toString(),
									jaze_chat  : list.team_chat_status.toString(),
								};

							return_result(objResult);
						}
						else
						{ return_result(objResult); }
					});
			}
			else
			{
				var query = "SELECT * FROM jaze_user_jazecome_status WHERE `meta_key` not in ('account_id') and `group_id` IN (SELECT `group_id`  FROM jaze_user_jazecome_status WHERE `meta_key` = 'account_id' AND meta_value = '"+arrParams.account_id+"' ) ";
				
				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
						HelperRestriction.convertMetaArray(data, function(statusRes){
							if(statusRes !== null && parseInt(statusRes.length) > 0)
							{ return_result(statusRes); }
							else
							{ return_result(objResult); }
						});
					}
					else
					{ return_result(objResult); }
				});
			}
		}
		else
		{ return_result(objResult); }
	}
	catch(err)
	{ console.log("___getAccountJazecomStatus",err); return_result(objResult); }	
}
//------------------------------------------------------------------- Follow Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Followers Module Start ---------------------------------------------------------------------------//

methods.getFollowersList  =  function (reqParams,return_result){
	
	try
	{
		if(reqParams !== null)
		{
			var query  = " SELECT * FROM jaze_follower_groups_meta WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'follow_account_id'  AND meta_value = "+reqParams.account_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'status' AND meta_value in (1,2))) ";

				if(typeof reqParams.follow_account_id !== 'undefined' && parseInt(reqParams.follow_account_id) > 0)
				{
					query += " AND group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'account_id'  AND meta_value = "+reqParams.follow_account_id+")  ";
				}

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(retConverted){	
							
							var array_rows = [];

							var counter = retConverted.length;
							
							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(retConverted,[function (list) {

										return new Promise(function (resolve, reject) {

											var sendParam = { account_id: list.follow_account_id,groups:list.follow_groups}; 
											
											methods.__getCompanyGroupList(sendParam, function(groupList,count){	
												
												var arrParams = { account_id :list.account_id };
												HelperGeneral.getAccountDetails(arrParams, function(accRet){	

													methods.___getAccountJazecomStatus(accRet, function(jazecom_status){	
													
														var block_status = "0";

														if(parseInt(list.status) === 2)
														{ block_status = "1"; }

														var arrObj = {
															follow_id 		: list.group_id.toString(),
															account_id 		: accRet.account_id,
															account_type 	: accRet.account_type,
															name       		: accRet.name,
															logo       		: accRet.logo,
															title   		: accRet.category,
															date       		:  list.create_date.split(" ")[0],
															following_count : count.toString(),
															block_status    : block_status.toString(),
															jaze_chat		: jazecom_status.jaze_chat.toString(),
                											jaze_mail 		: jazecom_status.jaze_mail.toString(),
                											jaze_call 		: jazecom_status.jaze_call.toString(),
                											jaze_feed 		: jazecom_status.jaze_feed.toString(),
															groups     		: groupList
														}
														
														array_rows.push(arrObj);
														
														counter -= 1;

														resolve(array_rows);
													});
												});
											});
										});
									}],
									function (result, current) {
										
										if (counter === 0)
										{ return_result(fast_sort(result[0]).asc('name')); }
									});	
							}
							else
							{return_result(null)}
						});
					}
					else
					{return_result(null)}
				});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("getFollowersList",err); return_result(null); }		
};

methods.getFollowersGroupList  =  function (reqParams,return_result){
	
	try
	{
		if(reqParams !== null)
		{
			var query  = " SELECT * FROM jaze_follower_groups WHERE status = '1' and  account_id = '"+reqParams.account_id+"' ";

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var array_rows = {
							business_group : [],
							special_group : []
						};

						var counter = data.length;
							
						if(parseInt(counter) !== 0)
						{
							promiseForeach.each(data,[function (list) {

								return new Promise(function (resolve, reject) {

									methods.__getCountGroupFollowers(0,reqParams.follow_account_id,list.id, function(count){	

										var status = '0';

										if(parseInt(count) !== 0)
										{ status = '1'; }

										var arrObj = {
											id 		: list.id.toString(),
											name    : list.group_name,
											status   : status.toString()
										}

										if(parseInt(list.group_type) === 1)
										{
											array_rows.business_group.push(arrObj);
										}
										else 
										{
											array_rows.special_group.push(arrObj);
										}
			
										counter -= 1;

										resolve(array_rows);

									});
								});
							}],
							function (result, current) {
										
								if (counter === 0)
								{
									fast_sort(result[0].business_group).asc('name');

									fast_sort(result[0].special_group).asc('name')

									return_result(result[0]); 
								}
							});	
						}
						else
						{return_result(null)}
					}
					else
					{return_result(null)}
				});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("getFollowerDetailGroupList",err); return_result(null); }	
};

methods.updateFollowersDetails  =  function (reqParams,return_result){
	
	try
	{
		if(reqParams !== null)
		{
			if(parseInt(reqParams.flag) === 0)
			{
				//remove follower
				var query = "DELETE FROM `jaze_follower_groups_meta` WHERE  group_id = '"+reqParams.follow_id+"' ";

				DB.query(query,null, function(data, error){
					return_result(1)
				});
			}
			else if(parseInt(reqParams.flag) === 1)
			{
				//get existing groups 

				var query = "SELECT meta_value FROM `jaze_follower_groups_meta` WHERE meta_key = 'follow_groups' and  group_id = '"+reqParams.follow_id+"' ";

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var old_groups = convert_array(data[0].meta_value);

						//update follower groups
						var follow_groups = convert_array(reqParams.group_id);

						if(parseInt(follow_groups.length) !== 0)
						{
							var query = "UPDATE `jaze_follower_groups_meta` SET meta_value = '"+follow_groups.join()+"' WHERE meta_key = 'follow_groups' and  group_id = '"+reqParams.follow_id+"' ";

							DB.query(query,null, function(data, error){

								//send notification
								follow_groups.forEach(function(new_group_id){ 
		 	
									if(old_groups.indexOf(new_group_id) === -1)
									 {
										methods.__sendGroupNotification(reqParams.account_id,reqParams.follow_id,new_group_id, function(returnNotification){

										});
									 }
							    });

								return_result(1)
							});
						}
						else
						{ return_result(null); }
					}
					else
					{ return_result(null); }
				});	
			}
			else if(parseInt(reqParams.flag) === 2)
			{
				//block follower
				var query = "UPDATE `jaze_follower_groups_meta` SET meta_value = '2' WHERE meta_key = 'status' and  group_id = '"+reqParams.follow_id+"' ";

				DB.query(query,null, function(data, error){

					{return_result(1)}
				});
			}
			else if(parseInt(reqParams.flag) === 3)
			{
				//unblock follower
				var query = "UPDATE `jaze_follower_groups_meta` SET meta_value = '1' WHERE meta_key = 'status' and  group_id = '"+reqParams.follow_id+"' ";

				DB.query(query,null, function(data, error){

					{return_result(1)}
				});
			}
			else
			{ return_result(null); }
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("updateFollowersDetails",err); return_result(null); }	
};

methods.getFollowId = function(reqParams,return_result){
	
	try{
			if(reqParams !== null)
			{
				var query  = " SELECT group_id FROM jaze_follower_groups_meta WHERE ";
					query += " group_id IN (SELECT group_id  FROM jaze_follower_groups_meta WHERE meta_key= 'follow_account_id' AND meta_value = '"+reqParams.account_id+"') ";
					query += " AND group_id IN (SELECT group_id  FROM jaze_follower_groups_meta WHERE meta_key= 'account_id' AND meta_value = '"+reqParams.follow_account_id+"') ";
					query += " AND group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'status' AND meta_value  = 1 ) GROUP BY group_id "; 

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0].group_id);
					}
					else
					{ 
						//create entry
						HelperRestriction.getNewGroupId('jaze_follower_groups_meta', function (group_id) {

							if(parseInt(group_id) !== 0){
	
								var arrInsert = {
									account_id          : reqParams.follow_account_id,
									follow_groups       : "",
									follow_account_id   : reqParams.account_id,
									create_date         : moment().utc().format('YYYY-MM-DD HH:mm:ss'),
									status 				: 1
								};
	
								HelperRestriction.insertTBQuery('jaze_follower_groups_meta',group_id,arrInsert, function (queGrpId) {
	
									return_result(queGrpId);
								});
							}
						});
					}
				});
			}
			else
			{ return_result(0); }		
	}
	catch(err)
	{console.log("getFollowId",err); return_result(0)}
};

methods.__getCountGroupFollowers = function(follow_account_id,account_id,group_id,return_result){
	
	try{
			if(parseInt(group_id) !== 0)
			{
				var query  = " SELECT COUNT(id) AS total FROM jaze_follower_groups_meta WHERE ";

					if(parseInt(follow_account_id) !== 0)
					{
						query += " group_id IN (SELECT group_id  FROM jaze_follower_groups_meta WHERE meta_key= 'follow_account_id' AND meta_value = '"+follow_account_id+"') ";
					}

					if(parseInt(account_id) !== 0)
					{
						query += " group_id IN (SELECT group_id  FROM jaze_follower_groups_meta WHERE meta_key= 'account_id' AND meta_value = '"+account_id+"') ";
					}
					
					query += " AND group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'status' AND meta_value  = 1 ) "; 
					query += " AND group_id IN  (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'follow_groups' AND FIND_IN_SET("+group_id+",meta_value)>0 ) GROUP BY group_id ";
				
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data.length);
					}
					else
					{ return_result(0); }
				});
			}
			else
			{ return_result(objReturn,0); }		
	}
	catch(err)
	{console.log("__getCountGroupFollowers",err); return_result(null,0)}
};
//------------------------------------------------------------------- Followers Module Start ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Followers Group Module Start ---------------------------------------------------------------------------//

methods.getGroupList  =  function (reqParams,return_result){
	
	try
	{
		if(reqParams !== null)
		{
			var query  = " SELECT * FROM jaze_follower_groups WHERE status = '1' and  account_id = '"+reqParams.account_id+"' ";

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var array_rows = {
							business_group : [],
							special_group : []
						};

						var counter = data.length;
							
						if(parseInt(counter) !== 0)
						{
							promiseForeach.each(data,[function (list) {

								return new Promise(function (resolve, reject) {

									methods.__getCountGroupFollowers(list.account_id,0,list.id, function(count){	

										var arrObj = {
											id 		: list.id.toString(),
											name    : list.group_name,
											count   : count.toString()
										}

										if(parseInt(list.group_type) === 1)
										{
											array_rows.business_group.push(arrObj);
										}
										else 
										{
											array_rows.special_group.push(arrObj);
										}
			
										counter -= 1;

										resolve(array_rows);

									});
								});
							}],
							function (result, current) {
										
								if (counter === 0)
								{
									fast_sort(result[0].business_group).asc('name');

									fast_sort(result[0].special_group).asc('name')

									return_result(result[0]); 
								}
							});	
						}
						else
						{return_result(null)}
					}
					else
					{return_result(null)}
				});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("getFollowersGroupList",err); return_result(null); }	
};

methods.getGroupDetails  =  function (reqParams,return_result){
	
	try
	{
		if(reqParams !== null)
		{
			var query  = " SELECT * FROM jaze_follower_groups WHERE status = '1' and  account_id = '"+reqParams.account_id+"' and id = '"+reqParams.group_id+"' ";

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						methods.__getCountGroupFollowers(0,reqParams.account_id,data[0].id, function(count){	

							var arrObj = {
								id 		: data[0].id.toString(),
								name    : data[0].group_name,
								type    : data[0].group_type,
								count   : count.toString()
							}

							return_result(arrObj)

						});
					}
					else
					{return_result({})}
				});
		}
		else
		{ return_result({}); }
	}
	catch(err)
	{ console.log("getGroupDetails",err); return_result({}); }	
};

methods.createFollowersGroup  =  function (reqParams,return_result){
	
	try
	{
		if(reqParams.title !== "" && parseInt(reqParams.group_type) !== 0)
		{
			var query = "";

			if(parseInt(reqParams.group_id) !== 0)
			{
				//update 
				query  = " UPDATE `jaze_follower_groups` set ";
				query += " group_name = '"+reqParams.title+"' ";
				query += " where  account_id = '"+reqParams.account_id+"' and  id = '"+reqParams.group_id+"' ";

				DB.query(query,null, function(data, error){ 

					return_result(reqParams.group_id);
				});
			}
			else if(parseInt(reqParams.group_id) === 0)
			{
				//create 
				query = " INSERT INTO `jaze_follower_groups` set ";
				query += " account_id = '"+reqParams.account_id+"', ";
				query += " group_name = '"+reqParams.title+"', ";
				query += " group_type = '"+reqParams.group_type+"', ";
				query += " status     = '1' ";

				DB.query(query,null, function(data, error){ 

					if(data !== null){
						return_result(data.insertId);
					}
					else
					{ return_result(0); }
				});
			}
			else
			{ return_result(0); }
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{ console.log("createFollowersGroup",err); return_result(0); }	
};

methods.deleteFollowersGroup  =  function (reqParams,return_result){
	
	try
	{
		if(parseInt(reqParams.group_id) !== 0)
		{
			//delete group 
			var query = "DELETE FROM `jaze_follower_groups` WHERE  id = '"+reqParams.group_id+"' ";

			DB.query(query,null, function(data, error){

				if(parseInt(reqParams.new_group_id) !== 0)
				{
					//move group
					methods.__moveGroupmembers(reqParams, function(count){	});
				}

				return_result(1);
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("deleteFollowersGroup",err); return_result(null); }	
};

methods.__moveGroupmembers = function(reqParams,return_result){
	
	try{
			if(parseInt(reqParams.new_group_id) !== 0)
			{
				var query  = " SELECT * FROM jaze_follower_groups_meta WHERE meta_key in ('follow_groups') ";
					query += " AND group_id IN (SELECT group_id  FROM jaze_follower_groups_meta WHERE meta_key= 'follow_account_id' AND meta_value = '"+reqParams.account_id+"') ";
					query += " AND group_id IN  (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'follow_groups' AND FIND_IN_SET("+reqParams.group_id+",meta_value)>0 ) GROUP BY group_id ";
				
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						data.forEach(function(row){

							var follow_groups = convert_array(row.meta_value);

							//remove id
							follow_groups.splice(follow_groups.indexOf(reqParams.group_id), 1);

							//add new id
							follow_groups.push(reqParams.new_group_id);

							var query  = " UPDATE jaze_follower_groups_meta SET meta_value = '"+(array_unique(follow_groups)).join()+"' where meta_key = 'follow_groups' and group_id = '"+row.group_id+"';"
					
							DB.query(query,null, function(udata, error){ 

								//send notification
								methods.__sendGroupNotification(reqParams.account_id,row.group_id,reqParams.new_group_id, function(returnNotification){

								});
							});
						});

						return_result(1);
					}
					else
					{ return_result(0); }
				});
			}
			else
			{ return_result(objReturn,0); }		
	}
	catch(err)
	{console.log("__moveGroupmembers",err); return_result(null,0)}
};

methods.getFollowersGroupMembersList = function(reqParams,return_result){
	
	try{
			if(reqParams !== null)
			{
				var query  = " SELECT * FROM jaze_follower_groups_meta WHERE ";
					query += " group_id IN (SELECT group_id  FROM jaze_follower_groups_meta WHERE meta_key= 'follow_account_id' AND meta_value = '"+reqParams.account_id+"') ";
					query += " AND group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'status' AND meta_value  = 1 ) "; 

					if(parseInt(reqParams.group_id) !== 0)
					{
						query += " AND group_id IN  (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'follow_groups' AND FIND_IN_SET("+reqParams.group_id+",meta_value)>0 )";
					}

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(retConverted){	
							
							var array_rows = [];

							var counter = retConverted.length;
							
							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(retConverted,[function (list) {

										return new Promise(function (resolve, reject) {

											var arrParams = { account_id :list.account_id };
											
												HelperGeneral.getAccountDetails(arrParams, function(accRet){	

													methods.___getAccountJazecomStatus(accRet, function(jazecom_status){	
													
														var block_status = "0";

														if(parseInt(list.status) === 2)
														{ block_status = "1"; }

														var arrObj = {
															follow_id 		: list.group_id.toString(),
															account_id 		: accRet.account_id,
															account_type 	: accRet.account_type,
															name       		: accRet.name,
															logo       		: accRet.logo,
															title   		: accRet.category,
															date       		:  list.create_date.split(" ")[0],
															block_status    : block_status.toString(),
															jaze_chat		: jazecom_status.jaze_chat.toString(),
                											jaze_mail 		: jazecom_status.jaze_mail.toString(),
                											jaze_call 		: jazecom_status.jaze_call.toString(),
                											jaze_feed 		: jazecom_status.jaze_feed.toString(),
														}
														
														array_rows.push(arrObj);
														
														counter -= 1;

														resolve(array_rows);
													});
												});
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ return_result(fast_sort(result[0]).asc('name')); }
									});	
							}
							else
							{return_result([])}
						});
					}
					else
					{return_result([])}
				});
			}
			else
			{return_result([])}
	}
	catch(err)
	{console.log("getFollowersGroupMembersList",err); return_result([])}
};

methods.setFollowersGroupMembers =  function (flag,reqParams,return_result){
	
	try
	{
		// flag : 1 = create 0 = remove
		if(reqParams !== null && parseInt(reqParams.group_id) !== 0 && reqParams.follow_ids !== null && reqParams.follow_ids !== "0")
		{
			var follow_id = reqParams.follow_ids.split(",");

			var counter = follow_id.length;

			if(parseInt(counter) !== 0)
			{
				follow_id.forEach(function(id){

					//get existing groups
					var query = " SELECT meta_value FROM `jaze_follower_groups_meta` WHERE meta_key = 'follow_groups' and  group_id = '"+id+"' ";

					DB.query(query,null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							var follow_groups = convert_array(data[0].meta_value);

							if(parseInt(flag) === 1)
							{
								//add new id
								follow_groups.push(reqParams.group_id);
							}
							else
							{
								//remove id
								follow_groups.splice(follow_groups.indexOf(reqParams.group_id), 1);
							}

							var query  = " UPDATE jaze_follower_groups_meta SET meta_value = '"+(array_unique(follow_groups)).join()+"' where meta_key = 'follow_groups' and group_id = '"+id+"';"
					
							DB.query(query,null, function(udata, error){ 

								if(parseInt(flag) === 1)
								{
									//send notification
									methods.__sendGroupNotification(reqParams.account_id,id,reqParams.group_id, function(returnNotification){

									});
								}
							});
						}
					});
				});

				return_result(1); 
			}
			else
			{return_result(0)}
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{ console.log("setFollowersGroupMembers",err); return_result(0); }	
};

methods.__sendGroupNotification = function(account_id,follow_group_id,group_id,return_result){
	
	try{

		var query = " SELECT meta_value FROM `jaze_follower_groups_meta` WHERE meta_key = 'account_id' and  group_id = '"+follow_group_id+"' ";

		DB.query(query,null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				var request_account_id = data[0].meta_value;

				HelperGeneral.getAccountTypeDetails(account_id, function(account_type){

					HelperGeneral.getAccountTypeDetails(request_account_id, function(request_account_type){

						var notiParams = {	
							group_id			 : group_id.toString(),
							account_id			 : account_id.toString(),
							account_type		 : account_type.toString(),
							request_account_id	 : request_account_id.toString(),
							request_account_type : request_account_type.toString(),
							message				 : '',
							flag				 : '24'
						}; 

						HelperNotification.sendPushNotification(notiParams, function(retNoti){						
							
							return_result(1);
						});
					});
				});
			}
			else
			{ return_result(0); }		
		});
	}
	catch(err)
	{console.log("__sendGroupNotification",err); return_result(0)}
};


function convert_array(array_val) {

	var list = [];

	if(typeof array_val !== 'undefined' && array_val !== null && array_val !== "")
	{
		if(array_val.toString().indexOf(',') != -1 ){
			list = array_val.split(',');
		}
		else {
			list.push(array_val);
		}
	}
	
	return list;
}
//------------------------------------------------------------------- Followers Group Module End ---------------------------------------------------------------------------//



module.exports = methods;
