const methods = {};
const DB = require('../../helpers/db_base.js');
const HelperRestriction = require('../../helpers/HelperRestriction.js');
const HelperGeneral = require('../../helpers/HelperGeneral.js');
const HelperGeneralJazenet = require('../helpers/HelperGeneralJazenet.js');
const promiseForeach = require('promise-foreach');
const fast_sort = require("fast-sort");

const STATIC_IMG_URL = G_STATIC_IMG_URL+"uploads/emergency_category"; 
const STATIC_IMG_URL_CAT_NEW = G_STATIC_IMG_URL+"uploads/jazenet/category/services/"; 
const STATIC_IMG_COMPANY = G_STATIC_IMG_URL+"uploads/jazenet/company_city_static/";


// const express = require('express');
// const router = express.Router();

methods.getJazefoodHome = function(arrParams,return_result){

	try
	{
		
		var arrRows = [];
		var newObj = {};

		var quer = `SELECT * FROM jaze_category_list 
			WHERE group_id IN (SELECT group_id FROM jaze_category_list WHERE meta_key = 'parent_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM  jaze_category_list  WHERE meta_key = 'status' AND meta_value = '1' )`;

		DB.query(quer,[arrParams.category_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){				


					var array_order =  { '1838' : '1','1839' : '2','1331' : '3','1354' : '4','1355' : '5'};
					
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	
	
									
							newObj = {
								category_id  : jsonItems.group_id.toString(),
								title        : jsonItems.cate_name,
								image        : STATIC_IMG_URL_CAT_NEW+'mobile/'+jsonItems.group_id+'.png',
								sort_order   : array_order[jsonItems.group_id]
							};

							arrRows.push(newObj);										

							counter -= 1;
							if (counter === 0){ 
								resolve(arrRows);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(fast_sort(result[0]).asc('sort_order'));
						}
					});

				});
			}
		});
	}
	catch(err)
	{ console.log("getJazefoodHome",err); return_result(null); }	

};


methods.getFeaturedCompany = function(arrParams,return_result){

	try
	{
		var arrRows = [];
		var newObj = {};

		var quer = " SELECT * FROM jaze_jazefood_premium_companies WHERE group_id IN (SELECT group_id FROM jaze_jazefood_premium_companies WHERE meta_key = 'category_id' AND meta_value ='"+arrParams.category_id+"') ";
			quer += " AND group_id IN (SELECT group_id FROM jaze_jazefood_premium_companies WHERE meta_key = 'status' AND meta_value = '1') ";
			
			if(parseInt(arrParams.country_id) !== 0){
				quer += " AND group_id IN (SELECT group_id FROM jaze_jazefood_premium_companies WHERE meta_key = 'country_id' AND meta_value ='"+arrParams.country_id+"') ";
			}

			if(parseInt(arrParams.state_id) !== 0){
				quer += " AND group_id IN (SELECT group_id FROM jaze_jazefood_premium_companies WHERE meta_key = 'state_id' AND meta_value ='"+arrParams.state_id+"') ";
			}

			if(parseInt(arrParams.city_id) !== 0){
				quer += " AND group_id IN (SELECT group_id FROM jaze_jazefood_premium_companies WHERE meta_key = 'city_id' AND meta_value ='"+arrParams.city_id+"') ";
			}


		DB.query(quer,null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var arrAccounts = jsonItems.account_id.split(',');

							arrRows.push(arrAccounts);										

							counter -= 1;
							if (counter === 0){ 
								resolve(arrRows);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							var arrResults = [];
							var account_details = {};

							var filteredArray = removeDuplicates(result[0]);

							var query = "SELECT * FROM jaze_user_basic_details WHERE account_id IN("+filteredArray[0]+")";
						
							DB.query(query,null, function(indata, error){

			
								if(typeof indata !== 'undefined' && indata !== null && parseInt(indata.length) > 0)
								{

									methods.convertMultiMetaArrayCompany(indata, function(retConverted){	


										var counterIn = retConverted.length;		
										var jsonItemsIn = Object.values(retConverted);

										promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
											return new Promise(function (resolveIn, reject) {	

									  			var accParams = {account_id:jsonItemsIn.account_id};
			
									  			methods.getCompanyProfile(accParams, function(retAccountDetails){	

									  				if(retAccountDetails!=null){

					  									account_details = {

								  							account_id:retAccountDetails.account_id,
															name:retAccountDetails.name,
															image:retAccountDetails.logo

										  				};

									  					arrResults.push(account_details)

							  							counterIn -= 1;
														if (counterIn === 0){ 
															resolveIn(arrResults);
														}
									  				}

												});												
										
											})
										}],
										function (resultIn, current) {
											if (counterIn === 0){ 
												
												var sliced = resultIn[0].slice(0,20); 

												var sortedArray = sliced.sort(function(a, b) {
												    return a.name > b.name ? 1 : a.name < b.name ? -1 : 0;
												});																

												return_result(sortedArray);
											}
										});

									});

								}

							});

						}
					});

				});
	
	        }
	        else
	        { 
	        	return_result([]);
	        }

		});

	}
	catch(err)
	{ console.log("getFavoritesListAllDetails",err); return_result([]); }	

};


methods.getAttributeParent = function(arrParams,return_result){

	try
	{
		var arrRows = [];
		var newObj = {};
		var status = '1';
		var category_id = '1331';

		if(parseInt(arrParams.category_id) !== 0){
			category_id = arrParams.category_id;
		}

		var quer = `SELECT t1.* FROM(
					SELECT group_id,
						(CASE WHEN meta_key = 'parent_id' THEN meta_value END) AS parent_id,
						(SELECT c.meta_value FROM jaze_category_list c WHERE c.meta_key = 'sort_order' AND c.group_id = jaze_category_list.group_id) AS sort_order
					FROM  jaze_category_list
					WHERE group_id =? ) AS t1 WHERE parent_id != ''`;


		DB.query(quer,[category_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				var querParent = `SELECT * FROM jaze_category_list WHERE group_id IN (SELECT group_id FROM jaze_category_list WHERE meta_key = 'parent_id' AND meta_value =? ) `;
				DB.query(querParent,[data[0].parent_id], function(pdata, error){

					if(typeof pdata !== 'undefined' && pdata !== null && parseInt(pdata.length) > 0){

						methods.convertMultiMetaArray(pdata, function(retConverted){

							const forEach = async () => {
							  	const values = retConverted;
							  	for (const val of values){
									var status = '1';
									if(val.group_id !== parseInt(category_id) ){
										status = '0';
									}

									newObj = {
										title: val.cate_name,
										active_status: status,
										category_id: val.group_id.toString()
									};
						
									arrRows.push(newObj);
							  	}

						  		const result = await returnData(arrRows);
							}
							 
							const returnData = x => {
							  return new Promise((resolve, reject) => {
							    setTimeout(() => {
							      resolve(x);
							    }, 500);
							  });
							}
							 
							forEach().then(() => {
								return_result(arrRows);
							})
	
						});
					}

				});
	
	        }
	        else
	        { 
	        	return_result(null);
	        }

		});

	}
	catch(err)
	{ console.log("getFavoritesListAllDetails",err); return_result(null); }	

};


methods.getCategoryAttributes = function(arrParams,return_result){


	try
	{

		var mainArr = [];
		var objController = {};
		var category_id = '1331';
		var sort_order;
		var trigger = 0;
		var object_name;

		if(parseInt(arrParams.category_id) !== 0){
			category_id = arrParams.category_id;
		}


		var quer = `SELECT * FROM jaze_category_attributes_groups WHERE group_id IN (SELECT group_id FROM jaze_category_attributes_groups WHERE meta_key = 'cat_id' AND( meta_value =? or meta_value ='10') ) `;
			quer +=	` AND group_id IN (SELECT group_id FROM  jaze_category_attributes_groups  WHERE meta_key = 'status' AND meta_value = '1' )`;

		DB.query(quer,[category_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){


				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (val) {
						return new Promise(function (resolve, reject) {	
					
							methods.getCategoryAttributeValue(val.group_id,val.input_type, function(retMainArr){ 


		  						if(val.input_type === "multiselect_image_button"){

								 	objController = {
										controller_type:'1',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'all',
									 	items_value_list:retMainArr
									};
					
								}else if(val.input_type === "multiselect_dropdown"){

								 	objController = {
										controller_type:'2',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'all',
									 	items_value_list:retMainArr
									};

						
								}else if(val.input_type === "color_picker"){

								 	objController = {
										controller_type:'3',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'all',
									 	items_color_list:retMainArr
									};
					
								}else if(val.input_type === "image_button"){

								 	objController = {
										controller_type:'4',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'all',
									 	items_value_list:retMainArr
									};

						
								}else if(val.input_type === "dropdown"){

								 	objController = {
										controller_type:'5',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'all',
									 	items_value_list:retMainArr
									};
						
								}else if(val.input_type === "radio_button"){
							
								 	objController = {
										controller_type:'6',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'',
									 	items_sing_value_list:retMainArr
									};
				
								}else if(val.input_type === "select_range"){
									
								 	objController = {
										controller_type:'7',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'',
									 	items_select_range_value:retMainArr
									};
				
								}else if(val.input_type === "input_range"){
					
								 	objController = {
										controller_type:'8',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'all',
									 	items_input_range_value:retMainArr
									};
							
								}else if(val.input_type === "text"){
									
								 	objController = {
										controller_type:'9',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'',
									 	items_edittext_value:retMainArr
									};
						
								}else if(val.input_type === "button"){
						
								 	objController = {
										controller_type:'10',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'',
									 	items_sing_value_list:retMainArr
									};
						
								}

								counter -= 1;
								mainArr.push(objController);


								if (counter === 0){ 
									resolve(mainArr);
								}

							});

						})
					}],
					function (result, current) {
						if (counter === 0){ 

							var sortedArray = mainArr.sort(function(a, b) {
							    return a.sort_order > b.sort_order ? 1 : a.sort_order < b.sort_order ? -1 : 0;
							});
						
							return_result(sortedArray);		
						}
					});

				});

	        }
	        else
	        { 
	        	return_result([]);
	        }

		});

	}
	catch(err)
	{ console.log("getCategoryAttributes",err); return_result(null); }	
};


methods.getCategoryAttributeValue = function(group_id,input_type,return_result){

	try
	{
		var arrValues = [];
		var obj_values = {};
		var controller_type;
		var objController = {};
		var value;
		var logo;

		if(input_type == 'color_picker'){
			obj_values = {
				id:'0',
				title:'all',
				color_code:''
			};
			arrValues.push(obj_values);
		}
		
	 	var arrSameInputs = ['multiselect_image_button','multiselect_dropdown','image_button','dropdown'];
		var singleObj = ['text','input_range','select_range'];
	 			
		if(arrSameInputs.includes(input_type) || input_type == 'color_picker' || input_type == 'radio_button' || input_type == 'text'){

			var quer = `SELECT * FROM jaze_category_attributes_groups_meta 
				WHERE group_id IN (SELECT group_id FROM jaze_category_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
				AND group_id IN (SELECT group_id FROM  jaze_category_attributes_groups_meta  WHERE meta_key = 'status' AND meta_value = '1' )`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					methods.convertMultiMetaArray(data, function(retConverted){	

						var ctr = 0;
						retConverted.forEach(vala => { 
						
							if(arrSameInputs.includes(input_type)){

								if(vala.value != null){
									value = vala.value;
									logo  = '';
								}else{
									value = '';
									logo = vala.logo;
								}

								obj_values = {
									id:vala.group_id.toString(),
									title:vala.key_name,
									value:value,
									logo:logo
								};

							}else if(input_type == 'color_picker'){

							
								obj_values = {
									id:vala.group_id.toString(),
									title:vala.key_name,
									color_code:vala.value
								};

							}else if(input_type == 'radio_button'){

								obj_values = {
									id:vala.group_id.toString(),
									title:vala.key_name,
									value:vala.value
								};

							}else if(input_type == 'text'){
																
								obj_values = {
									text_hint_label :vala.key_name
								};
							}

							arrValues.push(obj_values);

						}); 

					});

		  		}

			});

	
		}else if(input_type == 'input_range'){
	

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_category_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_category_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_category_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_category_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_category_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_category_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var min_field_label;
					var max_field_label;
					var suffix_field_label;
					var separator_field_label;

					const forEach = async () => {
					  	const values = data;
					  	for (const vala of values){

							if(vala.key_name == 'separator label'){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == 'min label'){
								min_field_label = vala.key_value;
							}

							if(vala.key_name == 'max label'){
								max_field_label = vala.key_value;
							}

							if(vala.key_name == 'suffix'){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								min_field_label:min_field_label,
								max_field_label:max_field_label,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

					  	}
				  		const result = await returnData(obj_values);
					}
					 
					const returnData = x => {
					  return new Promise((resolve, reject) => {
					    setTimeout(() => {
					      resolve(x);
					    }, 300);
					  });
					}
					 
					forEach().then(() => {
						arrValues.push(obj_values);
					
					})
		  		}

			});
					
		}else if(input_type == 'select_range'){
	

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_category_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_category_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_category_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_category_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_category_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_category_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var min_field_label;
					var min_field_value;
					var max_field_label;
					var max_field_value;
					var interval_value;
					var suffix_field_label;
					var separator_field_label;

					const forEach = async () => {
					  	const values = data;
					  	for (const vala of values){

							if(vala.key_name == 'separator label'){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == 'min label'){
								min_field_label = vala.key_value;
							}


							if(vala.key_name == 'min value'){
								min_field_value = vala.key_value;
							}

							if(vala.key_name == 'max label'){
								max_field_label = vala.key_value;
							}


							if(vala.key_name == 'max value'){
								max_field_value = vala.key_value;
							}

							if(vala.key_name == 'interval value'){
								interval_value = vala.key_value;
							}


							if(vala.key_name == 'suffix'){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								id:'0',
								min_field_label:min_field_label,
								min_field_value:min_field_value,
								max_field_label:max_field_label,
								max_field_value:max_field_value,
								internal_value:interval_value,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

					  	}
				  		const result = await returnData(obj_values);
					}
					 
					const returnData = x => {
					  return new Promise((resolve, reject) => {
					    setTimeout(() => {
					      resolve(x);
					    }, 300);
					  });
					}
					 
					forEach().then(() => {
						arrValues.push(obj_values);
					
					})
		  		}

			});
					
		}

		setTimeout(function(){ 
			if(singleObj.includes(input_type)){
				return_result(obj_values);
			}else{
				return_result(arrValues.sort(sortingArrayObject('title')));
			}
		}, 300);

	}
	catch(err)
	{ console.log("getCategoryAttributeValue",err); return_result([]); }	
};


methods.searchEatingOut =  function(arrParams,return_result){
	try
	{			
		var arrValues = [];
		var accParams = {};
		var account_details = {};
		var arrResults = [];
		var arrAccounts = [];

		var	pagination_details  = {
			total_page   : '0',
            current_page : '0',
            next_page    : '0',
            total_result : '0',
        };

		var total_result;

		var delay = 1000;

		if(Object.entries(arrParams.attributes).length !== 0 )
		{
			if(parseInt(arrParams.attributes.length) > 10){
				delay = delay + 200 * 5;
			}
			  
			const forEach = async () => {
			  	const attributes = arrParams.attributes;
			  	for (const param of attributes){

					if(parseInt(param.type) >= 1 &&  parseInt(param.type) <= 6)
					{	
						var title = param.value.toLowerCase();
						if(param.value.toLowerCase() != ''){
							title = param.value.toLowerCase().split(",");
						}
		
						var arrTitle = title;
						var query = "SELECT * FROM jaze_company_attribute_values WHERE meta_key=? ";
					}

					else if(parseInt(param.type) == 7)
					{
						var query = "SELECT * FROM jaze_company_attribute_values where meta_key =? and ( meta_value >='"+param.min_value+"' and meta_value <='"+param.max_value+"')";
					}
					else if(parseInt(param.type) == 8)
					{
						var query = "SELECT * FROM jaze_company_attribute_values WHERE meta_key=? ";
					}
					else if(parseInt(param.type) == 9)
					{
						var query = "SELECT * FROM jaze_company_attribute_values WHERE meta_key =? AND meta_value LIKE '%" + param.value + "%' ";
					}

					DB.query(query,[param.attribute_id], function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							const forEach = async () => {
							  	const values = data;
							  	for (const val of values){

						  			if(parseInt(param.type) >= 1 &&  parseInt(param.type) <= 6)
									{	
										var arrValues = val.meta_value.split(",");
										if(arrValues.some(item => arrTitle.includes(item))){
											arrAccounts.push(val.account_id);
										}
									}
									else if(parseInt(param.type) == 7)
									{
										arrAccounts.push(val.account_id);
									}
									else if(parseInt(param.type) == 8)
									{
										var arrValues = val.meta_value.split(",");
								  		if( parseInt(param.min_value) >= parseInt(arrValues[0]) && parseInt(param.max_value) >= parseInt(arrValues[1]) ){
								  			arrAccounts.push(val.account_id);
								  		}
									}
									else if(parseInt(param.type) == 9)
									{
										arrAccounts.push(val.account_id);
									}
						
							  	}
						  		const result = await returnData(arrAccounts);
							}
							 
							const returnData = x => {
							  return new Promise((resolve, reject) => {
							    setTimeout(() => {
							      resolve(x);
							    }, 500);
							  });
							}
							 
							forEach();
			
						}
					});

			  	}

		  		const result = await returnData(arrAccounts);
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, delay);
			  });
			}
			 
			forEach().then(() => {


				var filteredArray = removeDuplicates(arrAccounts);

				if( filteredArray.length > 0){

					var total_page = 0;
					var next_page = 0;
					var total_result = filteredArray.length.toString();

					var newArray = filteredArray;

					if(parseInt(filteredArray.length) > 50){
						newArray   = filteredArray.slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
						total_page = Math.ceil((parseInt(filteredArray.length) - 50 ) / 50);
					}

					if( parseInt(arrParams.page) <= parseInt(total_page) )
					{
						
						if(total_page !== 0){
							next_page  = parseInt(arrParams.page)+1;
						}
					
						pagination_details  = {
							total_page   : total_page.toString(),
			                current_page : arrParams.page.toString(),
			                next_page    : next_page.toString(),
                         	total_result : total_result
			            };
				

						if(parseInt(arrParams.city_id)  !=0  || parseInt(arrParams.state_id)  !=0 || parseInt(arrParams.country_id) !=0 )
						{


							if(parseInt(arrParams.lat) !==0  && parseInt(arrParams.lon) !==0)
							{

								methods.processwithLocCompany(newArray,arrParams,  function(retAccountDetails,pagination_details){	

									return_result(retAccountDetails,pagination_details);

								});	

							}
							else
							{

		
								methods.checkIfInGeoCompany(newArray,arrParams, function(retAccs){

									if(retAccs.length > 0){

										const forEach = async () => {
										  	const values = retAccs;
										  	for (const val of values){
										  		accParams = {account_id:val};
									  			methods.getCompanyProfile(accParams, function(retAccountDetails){	
									  				if(retAccountDetails!=null){

					  									account_details = {
								  							company_id:retAccountDetails.account_id,
															company_name:retAccountDetails.name,
															company_image:retAccountDetails.logo
										  				};
									  					arrResults.push(account_details)
									  				}

												});
										  	}
									  		const result = await returnData(arrResults);
										}
										 
										const returnData = x => {
										  return new Promise((resolve, reject) => {
										    setTimeout(() => {
										      resolve(x);
										    }, 500);
										  });
										}

										forEach().then(() => {
											return_result(arrResults.sort(sortingArrayObject('company_name')),pagination_details);
										})

										return_result(retAccountDetails,pagination_details);
									}else{

										pagination_details  = {
											total_page   : '0',
								            current_page : '0',
								            next_page    : '0',
								            total_result : '0',
								        };

										return_result(null,pagination_details);
									}
								});

							}

						}
						else
						{

							if(parseInt(arrParams.lat) !==0  && parseInt(arrParams.lon) !==0)
							{

								methods.processwithLocCompany(newArray,arrParams,  function(retAccountDetails,pagination_details){	

									return_result(retAccountDetails,pagination_details);

								});	

							}
							else
							{	


								const forEach = async () => {
								  	const values = newArray;
								  	for (const val of values){
								  		accParams = {account_id:val};
							  			methods.getCompanyProfile(accParams, function(retAccountDetails){	
							  				if(retAccountDetails!=null){

			  									account_details = {
						  							company_id:retAccountDetails.account_id,
													company_name:retAccountDetails.name,
													company_image:retAccountDetails.logo
								  				};
							  					arrResults.push(account_details)
							  				}

										});
								  	}
							  		const result = await returnData(arrResults);
								}
								 
								const returnData = x => {
								  return new Promise((resolve, reject) => {
								    setTimeout(() => {
								      resolve(x);
								    }, 500);
								  });
								}

								forEach().then(() => {
									return_result(arrResults.sort(sortingArrayObject('company_name')),pagination_details);
								})

							}
							

						}

					}
					else
					{
						
						return_result(null,pagination_details);
					}
				 
				}else{
					return_result(null,pagination_details);
				}

			})

		}
		else
		{
			if(parseInt(arrParams.category_id) !==0)
			{
				var group_ids = [];
				var arrGroupds = [];

				var quer = `SELECT t1.* FROM(
					SELECT group_id,
						(CASE WHEN meta_key = 'parent_id' THEN meta_value END) AS parent_id,
						(SELECT c.meta_value FROM jaze_category_list c WHERE c.meta_key = 'sort_order' AND c.group_id = jaze_category_list.group_id) AS sort_order
					FROM  jaze_category_list
					WHERE group_id =? ) AS t1 WHERE parent_id != ''`;

				DB.query(quer,[arrParams.category_id], function(data, error){	

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var query = "SELECT * FROM jaze_category_list WHERE meta_key = 'parent_id' AND meta_value =? ";
						DB.query(query,[data[0].parent_id], function(datax, error){
							if(typeof datax !== 'undefined' && datax !== null && parseInt(datax.length) > 0){

							  	group_ids.push(data[0].parent_id);

								var counterMain = datax.length;		
								var jsonItemsMain = Object.values(datax);

								promiseForeach.each(jsonItemsMain,[function (jsonItemsMain) {
									return new Promise(function (resolveMain, reject) {		
						
										group_ids.push(jsonItemsMain.group_id.toString());
										counterMain -= 1;
										if (counterMain === 0){ 
											resolveMain(group_ids);
										}
									})
								}],
								function (resultMain, current) {
									if (counterMain === 0){ 

										DB.query("SELECT * FROM jaze_category_attributes_groups WHERE meta_key = 'cat_id' AND meta_value IN(?)",[resultMain[0]], function(datay, error){

											if(typeof datay !== 'undefined' && datay !== null && parseInt(datay.length) > 0){


												var counter = datay.length;		
												var jsonItems = Object.values(datay);

												promiseForeach.each(jsonItems,[function (jsonItems) {
													return new Promise(function (resolve, reject) {		
											
														arrGroupds.push(jsonItems.group_id.toString());
												
														counter -= 1;
														if (counter === 0){ 
															resolve(arrGroupds);
														}
													})
												}],
												function (result, current) {
													if (counter === 0){ 
														
														DB.query("SELECT * FROM jaze_company_attribute_values WHERE meta_key IN(?)",[result[0]], function(dataz, error){ 


															var counterIn = dataz.length;		
															var jsonItemsIn = Object.values(dataz);

															promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
																return new Promise(function (resolve, reject) {		
														
																	arrAccounts.push(jsonItemsIn.account_id);

																	counterIn -= 1;
																	if (counterIn === 0){ 
																		resolve(arrAccounts);
																	}
																})
															}],
															function (resultIn, current) {
																if (counterIn === 0){ 
																	
																	DB.query("SELECT * FROM jaze_user_basic_details WHERE meta_key ='category_id' AND meta_value =?",[arrParams.category_id], function(datazz, error){  

																		if(typeof datazz !== 'undefined' && datazz !== null && parseInt(datazz.length) > 0)
																		{

																			var counterInn = datazz.length;		
																			var jsonItemsInn = Object.values(datazz);

																			promiseForeach.each(jsonItemsInn,[function (jsonItemsInn) {
																				return new Promise(function (resolve, reject) {		
																		
																					arrAccounts.push(jsonItemsInn.account_id);
																					counterInn -= 1;
																					if (counterInn === 0){ 
																						resolve(arrAccounts);
																					}
																				})
																			}],
																			function (resultInn, current) {
																				if (counterInn === 0){ 
																	
																					var filteredArray = removeDuplicates(arrAccounts);

																					if( parseInt(filteredArray.length) > 0){

																						var total_page = 0;
																						var next_page = 0;
																						var total_result = filteredArray.length.toString();

																						var newArray = filteredArray;

																						if(parseInt(filteredArray.length) > 50){
																							newArray   = filteredArray.slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
																							total_page = Math.ceil((parseInt(filteredArray.length) - 50 ) / 50);
																						}

															
																						if( parseInt(arrParams.page) <= parseInt(total_page) )
																						{

																					
																							if(arrParams.page < total_page){
																								next_page  = parseInt(arrParams.page)+1;
																							}
																						
																							pagination_details  = {
																								total_page   : total_page.toString(),
																				                current_page : arrParams.page.toString(),
																				                next_page    : next_page.toString(),
																				                total_result : total_result
																				            };
																					
																							if(parseInt(arrParams.city_id)  !=0  || parseInt(arrParams.state_id)  !=0 || parseInt(arrParams.country_id) !=0 )
																							{
																							
																								methods.checkIfInGeo(newArray,arrParams, function(retAccountDetails){
																									if(retAccountDetails.length > 0){
																										return_result(retAccountDetails,pagination_details);
																									}else{
																										return_result(null,pagination_details);
																									}
																								});
																							}
																							else
																							{

																								var counterInnn = newArray.length;		
																								var jsonItemsInnn = Object.values(newArray);

																								promiseForeach.each(jsonItemsInnn,[function (jsonItemsInnn) {
																									return new Promise(function (resolve, reject) {		
																										
																										var accParams = {account_id:jsonItemsInnn};
																										HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

																											account_details = {
																							   					company_id:retAccountDetails.account_id,
																							   					company_name:retAccountDetails.name,
																							   					company_image:retAccountDetails.logo
																							   				};
																			
																											arrResults.push(account_details);
																											counterInnn -= 1;
																											if (counterInnn === 0){ 
																												resolve(arrResults);
																											}

																										});
																									})
																								}],
																								function (resultInnn, current) {
																									if (counterInnn === 0){ 
																										return_result(resultInnn[0].sort(sortingArrayObject('company_name')),pagination_details);
																									}
																								});

																
																							}

																						}else{
																							return_result(null,pagination_details);
																						}

																					}else{
																						return_result(null,pagination_details);
																					}

																				}
																			});

																		}
																		else
																		{
																			

																			var filteredArray = removeDuplicates(arrAccounts);

																			if( parseInt(filteredArray.length) > 0){

																				var total_page = 0;
																				var next_page = 0;
																				var total_result = filteredArray.length.toString();

																				var newArray = filteredArray;

																				if(parseInt(filteredArray.length) > 50){
																					newArray   = filteredArray.slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
																					total_page = Math.ceil((parseInt(filteredArray.length) - 50 ) / 50);
																				}

													
																				if( parseInt(arrParams.page) <= parseInt(total_page) )
																				{

																			
																					if(arrParams.page < total_page){
																						next_page  = parseInt(arrParams.page)+1;
																					}
																				
																					pagination_details  = {
																						total_page   : total_page.toString(),
																		                current_page : arrParams.page.toString(),
																		                next_page    : next_page.toString(),
																		                total_result : total_result
																		            };
																			
																					if(parseInt(arrParams.city_id)  !=0  || parseInt(arrParams.state_id)  !=0 || parseInt(arrParams.country_id) !=0 )
																					{
																					
																						methods.checkIfInGeo(newArray,arrParams, function(retAccountDetails){
																							if(retAccountDetails.length > 0){
																								return_result(retAccountDetails,pagination_details);
																							}else{
																								return_result(null,pagination_details);
																							}
																						});
																					}
																					else
																					{

																						var counterInnn = newArray.length;		
																						var jsonItemsInnn = Object.values(newArray);

																						promiseForeach.each(jsonItemsInnn,[function (jsonItemsInnn) {
																							return new Promise(function (resolve, reject) {		
																								
																								var accParams = {account_id:jsonItemsInnn};
																								HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

																									account_details = {
																					   					company_id:retAccountDetails.account_id,
																					   					company_name:retAccountDetails.name,
																					   					company_image:retAccountDetails.logo
																					   				};
																	
																									arrResults.push(account_details);
																									counterInnn -= 1;
																									if (counterInnn === 0){ 
																										resolve(arrResults);
																									}

																								});
																							})
																						}],
																						function (resultInnn, current) {
																							if (counterInnn === 0){ 
																								return_result(resultInnn[0].sort(sortingArrayObject('company_name')),pagination_details);
																							}
																						});

														
																					}

																				}else{
																					return_result(null,pagination_details);
																				}

																			}else{
																				return_result(null,pagination_details);
																			}

																		}

																	});

																}
															});

														});

													}
												});


											}else{
												return_result(null,pagination_details);	
											}

								  		});

									}
								});
						
							}else{
								return_result(null,pagination_details);
							}

						});

					}else{
						return_result(null,pagination_details);	
					}

				});

			}
			else
			{
				return_result(null,pagination_details);	
			}
		}


	}
	catch(err)
	{ console.log("searchEatingOut",err); return_result(null);}
};



methods.checkIfInGeo =  function(arrAccounts,arrParams,return_result){
	try
	{

		var accParams = {};
		var account_details = {};
		var arrResults = [];

		const forEach = async () => {
		  	const values = arrAccounts;
		  	for (const val of values){
		  		accParams = {account_id:val};
	  			HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	
					
					if(retAccountDetails !== null)
					{
					  if(parseInt(arrParams.country_id) == parseInt(retAccountDetails.country_id)  || parseInt(arrParams.state_id) == parseInt(retAccountDetails.state_id)  || parseInt(arrParams.city_id) == parseInt(retAccountDetails.city_id))
					  {

						account_details = {
		  					company_id:retAccountDetails.account_id,
							company_name:retAccountDetails.name,
							company_image:retAccountDetails.logo
		  				};
		  				arrResults.push(account_details)
						}
					}
				});
		  	}
	  		const result = await returnData(arrResults);
		}
		 
		const returnData = x => {
		  return new Promise((resolve, reject) => {
		    setTimeout(() => {
		      resolve(x);
		    }, 500);
		  });
		}

		forEach().then(() => {
			return_result(arrResults);
		})

	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};





methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMultiMetaArray",err); return_result(null);}
};


function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].group_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}


function sortingArrayObject(key, order='asc') {

	return function(a, b) {
	  if(!a.hasOwnProperty(key) || 
		 !b.hasOwnProperty(key)) {
		  return 0; 
	  }
	  
	  const varA = (typeof a[key] === 'string') ? 
		a[key].toUpperCase() : a[key];
	  const varB = (typeof b[key] === 'string') ? 
		b[key].toUpperCase() : b[key];
		
	  let comparison = 0;
	  if (varA > varB) {
		comparison = 1;
	  } else if (varA < varB) {
		comparison = -1;
	  }
	  return (
		(order == 'desc') ? 
		(comparison * -1) : comparison
	  );
	};
}

function removeDuplicates(array) {
  return array.filter((a, b) => array.indexOf(a) === b)
};


//jaze category company start //


methods.searchCompanyCategory = function(arrParams,return_result)
{
		

	try
	{
		var arrAccounts = [];
		var arrAccIds = [];
		var newObj = {};
		var object_account = {};


		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };


		var query = `SELECT account_id FROM jaze_user_basic_details 
			WHERE account_id IN (SELECT account_id FROM jaze_user_basic_details 
			WHERE meta_key = 'category_id' AND meta_value =?) GROUP BY account_id`

		DB.query(query, [arrParams.category_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var counter = data.length;
									
				var jsonItemsArr = Object.values(data);
	
				promiseForeach.each(jsonItemsArr,[function (jsonItems) {


					return new Promise(function (resolve, reject) {

						arrAccounts.push(jsonItems.account_id.toString());
						counter -= 1;

						if (counter === 0)
						{ 		
							resolve(arrAccounts);
						}
					})
				}],
				function (result, current) { 
					if (counter === 0)
					{ 
						var filteredArray = removeDuplicates(result[0]);
						methods.processInnerResultCompany(filteredArray,arrParams, function(retProcessed,pagination_details){
								return_result(retProcessed,pagination_details);
						});


					}
				});	
			

			}else{
				return_result(null,pagination_details);
			}
		});	


	}
	catch(err)
	{ 
		console.log("searchCompanyCategoryNew",err); 
		return_result(null,pagination_details); 
	}	

};


methods.processInnerResultCompany  =  function(arrvalues,arrParams,return_result){
	try
	{
		var arrResults = [];
		var newArray = [];
		var account_details = {};

		if(parseInt(arrParams.city_id)  !==0  || parseInt(arrParams.state_id)  !==0 || parseInt(arrParams.country_id) !==0 )
		{
		
			methods.checkIfInGeoCompany(arrvalues,arrParams, function(retCoGeo){

				if(retCoGeo!=null)
				{

					if(parseInt(arrParams.lat) !==0 && parseInt(arrParams.lon) !==0)
					{	
						var filteredArray = removeDuplicates(retCoGeo);
						methods.processwithLocCompany(filteredArray,arrParams,  function(retAccountDetails,pagination_details){	
						 	retAccountDetails.filter((v,i,a)=>a.findIndex(t=>(t.account_id === v.account_id && t.account_id===v.account_id))===i);
							return_result(retAccountDetails,pagination_details);

						});	
					}
					else
					{

						var filteredArray = removeDuplicates(retCoGeo);
						var counter = filteredArray.length;		
						var jsonItems = Object.values(filteredArray);

						promiseForeach.each(jsonItems,[function (jsonItems) {


							return new Promise(function (resolve, reject) {		
					
								var accParams = {account_id:jsonItems};
							
					  			methods.getCompanyProfile(accParams, function(retAccountDetails){	
									
									if(retAccountDetails!=null){

									
						  				account_details = {	
						  					company_id:retAccountDetails.account_id,
											company_name:retAccountDetails.name,
											company_image:retAccountDetails.logo,
											distance:''
						  				};

						  				
										arrResults.push(account_details);
				  
										counter -= 1;
										if (counter === 0)
										{ 		
											resolve(arrResults);
										}
								
									
									}
			
								});

							})
						}],
						function (result, current) { 

							if (counter === 0){ 

								var geo_total_result = result[0].length.toString();

								if(parseInt(result[0].length) > 100){
									newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 100 ), (parseInt(arrParams.page) + 1) * 100);
									total_page = Math.ceil((parseInt(result[0].length) - 100 ) / 100);
								}else{
									newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 100 ), (parseInt(arrParams.page) + 1) * 100);
									total_page = 0;
								}
								var geo_next_page = 0;;
								if(parseInt(total_page) !== 0){
									geo_next_page  = parseInt(arrParams.page)+1;
								}


								pagination_details  = {
									total_page   : total_page.toString(),
					                current_page : arrParams.page.toString(),
					                next_page    : geo_next_page.toString(),
					                total_result : geo_total_result
					            };

            				 	newArray.filter((v,i,a)=>a.findIndex(t=>(t.account_id === v.account_id && t.account_id===v.account_id))===i);
					            return_result(newArray.sort(sortingArrayObject('company_name')),pagination_details);
								
							}

						});	
					}
				

				}
				else
				{
					return_result(null);
				}

			});

		}
		else
		{

			if(parseInt(arrParams.lat) !==0 && parseInt(arrParams.lon) !==0)
			{
				var filteredArray = removeDuplicates(arrvalues);
				methods.processwithLocCompany(filteredArray,arrParams,  function(retAccountDetails,pagination_details){	

				 	retAccountDetails.filter((v,i,a)=>a.findIndex(t=>(t.account_id === v.account_id && t.account_id===v.account_id))===i);
					return_result(retAccountDetails,pagination_details);

				});	
			}
			else
			{

				var filteredArray = removeDuplicates(arrvalues);
				var counter = filteredArray.length;		
				var jsonItems = Object.values(filteredArray);

				promiseForeach.each(jsonItems,[function (jsonItems) {


					return new Promise(function (resolve, reject) {		
			
						var accParams = {account_id:jsonItems};
					
			  			methods.getCompanyProfile(accParams, function(retAccountDetails){	
							
							if(retAccountDetails!=null){

							
				  				account_details = {	
				  					company_id:retAccountDetails.account_id,
									company_name:retAccountDetails.name,
									company_image:retAccountDetails.logo,
									distance:''
				  				};

				  				
								arrResults.push(account_details);
		  
								counter -= 1;
								if (counter === 0)
								{ 		
									resolve(arrResults);
								}
							
							}
	
						});

					})
				}],
				function (result, current) { 

					if (counter === 0){ 


						var geo_total_result = result[0].length.toString();

						if(parseInt(result[0].length) > 100){
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 100 ), (parseInt(arrParams.page) + 1) * 100);
							total_page = Math.ceil((parseInt(result[0].length) - 100 ) / 100);
						}else{
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 100 ), (parseInt(arrParams.page) + 1) * 100);
							total_page = 0;
						}
						var geo_next_page = 0;;
						if(parseInt(total_page) !== 0){
							geo_next_page  = parseInt(arrParams.page)+1;
						}


						pagination_details  = {
							total_page   : total_page.toString(),
			                current_page : arrParams.page.toString(),
			                next_page    : geo_next_page.toString(),
			                total_result : geo_total_result
			            };

			            newArray.filter((v,i,a)=>a.findIndex(t=>(t.account_id === v.account_id && t.account_id===v.account_id))===i);

			            return_result(newArray.sort(sortingArrayObject('company_name')),pagination_details);
						
					}

				});	
			}

		}
	}
	catch(err)
	{ console.log("processInnerResultCompany",err); return_result(null);}
};


methods.checkIfInGeoCompany =  function(arrAccounts,arrParams,return_result){
	try
	{

		var account_details = {};
		var arrResults = [];

		var filteredArray = removeDuplicates(arrAccounts);
			
		var counter = filteredArray.length;		
		var jsonItems = Object.values(filteredArray);

		promiseForeach.each(jsonItems,[function (jsonItems) {

			return new Promise(function (resolve, reject) {		
	
				var accParams = {account_id:jsonItems};

	  			methods.getCompanyProfile(accParams, function(retAccountDetails){	

	 				
					if(parseInt(arrParams.country_id) == parseInt(retAccountDetails.country_id) 
	  					|| parseInt(arrParams.state_id) == parseInt(retAccountDetails.state_id) 
	  					|| parseInt(arrParams.city_id) == parseInt(retAccountDetails.city_id)  )
					{
						arrResults.push(retAccountDetails.account_id);		

		  			
					}
				
					counter -= 1;
					if (counter === 0)
					{ 		
						resolve(arrResults);
					}
	  		
				});

			})
		}],
		function (result, current) {

			if (counter === 0){ 
				
				return_result(result[0]);
			}
		});

	}
	catch(err)
	{ console.log("checkIfInGeo",err); return_result(null);}
};


methods.processwithLocCompany =  function(arrvalues,arrParams,return_result){
	try
	{	


		var total_page = 0;
		var next_page = 0;
		var total_result = 0;
		var newArray = [];

		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };

		var arrResults = [];
		var account_details = {};

		var query = `SELECT account_id, GROUP_CONCAT(meta_value ORDER BY meta_value ASC SEPARATOR ':') AS coordinates
				FROM jaze_user_basic_details 
			WHERE meta_key IN ('latitude','longitude') AND account_id IN(?) AND meta_value != ''  GROUP BY account_id`;

		var filteredArray = removeDuplicates(arrvalues);

		DB.query(query, [filteredArray], function(data, error){

	

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				//console.log(data);
				var counter = data.length;		
				var jsonItems = Object.values(data);

				promiseForeach.each(jsonItems,[function (jsonItems) {

					return new Promise(function (resolve, reject) {		
			
						var accParams = {account_id:jsonItems.account_id};
								
			  			methods.getCompanyProfile(accParams, function(retAccountDetails){	
							
							if(retAccountDetails!=null){

								var geo = jsonItems.coordinates.split(":");

								var geo_distance = distance(parseFloat(arrParams.lat), parseFloat(arrParams.lon), parseFloat(geo[0]), parseFloat(geo[1]),"K");

				  				account_details = {	
				  					company_id:retAccountDetails.account_id,
									company_name:retAccountDetails.name,
									company_image:retAccountDetails.logo,
									distance:Math.ceil(geo_distance).toString()
				  				};

				  				
								arrResults.push(account_details);
		  
								counter -= 1;
								if (counter === 0)
								{ 		
									resolve(arrResults);
								}
								
							
							}
	
						});

					})
				}],
				function (result, current) { 

					if (counter === 0){ 
				
						var geo_total_result = result[0].length.toString();

						if(parseInt(result[0].length) > 100){
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 100 ), (parseInt(arrParams.page) + 1) * 100);
							total_page = Math.ceil((parseInt(result[0].length) - 100 ) / 100);

						}else{
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 100 ), (parseInt(arrParams.page) + 1) * 100);
							total_page = 0;
						}

	
						var geo_next_page = 0;;
						if(parseInt(total_page) !== 0){
							geo_next_page  = parseInt(arrParams.page)+1;
						}

		
						if(parseInt(arrParams.page) <= parseInt(total_page)){

							pagination_details  = {
								total_page   : total_page.toString(),
				                current_page : arrParams.page.toString(),
				                next_page    : geo_next_page.toString(),
				                total_result : geo_total_result
				            };



				            newArray.sort(function(a, b) {
							    return parseFloat(a.distance) - parseFloat(b.distance);
							});

							return_result(newArray,pagination_details);

						}else{
							return_result(null,pagination_details);
						}
	
					}

				});	

			
			}
			else
			{ return_result(null,pagination_details); }

		});
	}
	catch(err)
	{ console.log("processwithLocCompany",err); return_result(null,pagination_details);}
};



methods.getCompanyProfile =  function(arrParams,return_result){
	try
	{

		var newObj = {};

 		var default_logo = "";

		var queryAccountType = `SELECT * FROM jaze_user_basic_details WHERE account_id =? and account_type = '3'`;

		DB.query(queryAccountType,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArrayCompany(data, function(retConverted){	

					const forEach = async () => {
					  	const values = retConverted;
					  	for (const val of values){


		  					if(val.listing_images)
							{
								default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/listing_images/' + val.listing_images + '';
							}
							else if(val.company_logo)
							{
								default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/logo/' + val.company_logo + '';
							}
							else
							{
								default_logo = "";
							}
					

							newObj = {
								account_id	  : val.account_id.toString(),
								name		  : val.company_name,
								logo		  : default_logo,
								country_id    : val.country_id.toString(),
								city_id       : val.city_id.toString(),
								state_id      : val.state_id.toString()
							}	
					  	}

				  		const result = await returnData(newObj);
					}
					 
					const returnData = x => {
					  return new Promise((resolve, reject) => {
					    setTimeout(() => {
					      resolve(x);
					    }, 150);
					  });
					}
					 
					forEach().then(() => {
						return_result(newObj);
					})

				});
			}
			else
			{
				return_result(null)
			}
		});

	}
	catch(err)
	{ console.log("getCompanyProfile",err); return_result(null);}
};



//company attributes //
methods.getCompanyCategoryAttributes = function(arrParams,return_result){


	try
	{

		var mainArr = [];
		var objController = {};

		if(parseInt(arrParams.page) === 0)
		{

			var quer = `SELECT * FROM jaze_category_attributes_groups WHERE group_id IN (SELECT group_id FROM jaze_category_attributes_groups WHERE meta_key = 'cat_id' AND meta_value =? ) 
			AND group_id IN (SELECT group_id FROM  jaze_category_attributes_groups  WHERE meta_key = 'status' AND meta_value = '1' )`;

			DB.query(quer,[arrParams.category_id], function(data, error){


				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					methods.convertMultiMetaArray(data, function(retConverted){

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);


						promiseForeach.each(jsonItems,[function (val) {
							return new Promise(function (resolve, reject) {	
								
								methods.getCompanyCategoryAttributeValue(val.group_id,val.input_type, function(retMainArr){ 

									
			  						if(val.input_type === "multiselect_image_button"){

									 	objController = {
											controller_type:'1',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_value_list:retMainArr
										};
						
									}else if(val.input_type === "multiselect_dropdown"){

									 	objController = {
											controller_type:'2',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_value_list:retMainArr
										};

							
									}else if(val.input_type === "color_picker"){

									 	objController = {
											controller_type:'3',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_color_list:retMainArr
										};
						
									}else if(val.input_type === "image_button"){

									 	objController = {
											controller_type:'4',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_value_list:retMainArr
										};

							
									}else if(val.input_type === "dropdown"){

									 	objController = {
											controller_type:'5',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_value_list:retMainArr
										};
							
									}else if(val.input_type === "radio_button"){
								
									 	objController = {
											controller_type:'6',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'',
										 	items_sing_value_list:retMainArr
										};
					
									}else if(val.input_type === "select_range"){
										
									 	objController = {
											controller_type:'7',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'',
										 	items_select_range_value:retMainArr
										};
					
									}else if(val.input_type === "input_range"){
						
									 	objController = {
											controller_type:'8',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_input_range_value:retMainArr
										};
								
									}else if(val.input_type === "text"){
										
									 	objController = {
											controller_type:'9',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'',
										 	items_edittext_value:retMainArr
										};
							
									}else if(val.input_type === "button"){
							
									 	objController = {
											controller_type:'10',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'',
										 	items_sing_value_list:retMainArr
										};
							
									}

									mainArr.push(objController);
									counter -= 1;
									if (counter === 0){ 
										resolve(mainArr);
									}
								});
							
							})
						}],
						function (result, current) {
							if (counter === 0){ 

								var sortedArray = result[0].sort(function(a, b) {
								    return a.home_sort_order > b.home_sort_order ? 1 : a.home_sort_order < b.home_sort_order ? -1 : 0;
								});

								return_result(sortedArray);
							}
						});


					});
		        }
		        else
		        { 
		        	return_result([]);
		        }

			});
		}
		else
		{
			return_result([]);
		}

	}
	catch(err)
	{ console.log("getCompanyCategoryAttributes",err); return_result(null); }	
};


methods.getCompanyCategoryAttributeValue = function(group_id,input_type,return_result){

	try
	{
		var arrValues = [];
		var obj_values = {};
		var controller_type;
		var objController = {};
		var value;
		var logo;

		if(input_type == 'color_picker'){
			obj_values = {
				id:'0',
				title:'all',
				color_code:''
			};
			arrValues.push(obj_values);
		}

		var singleObj = ['text','input_range','select_range'];
	 	var arrSameInputs = ['multiselect_image_button','multiselect_dropdown','image_button','dropdown'];

		if(arrSameInputs.includes(input_type) || input_type == 'color_picker' || input_type == 'radio_button' || input_type == 'text'  || input_type == 'button'){

			var quer = `SELECT * FROM jaze_category_attributes_groups_meta 
				WHERE group_id IN (SELECT group_id FROM jaze_category_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
				AND group_id IN (SELECT group_id FROM  jaze_category_attributes_groups_meta  WHERE meta_key = 'status' AND meta_value = '1' )`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					methods.convertMultiMetaArray(data, function(retConverted){	


						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (vala) {
							return new Promise(function (resolve, reject) {		
					
													
								if(arrSameInputs.includes(input_type)){

									if(vala.value != null){
										value = vala.value;
										logo  = '';
									}else{
										value = '';
										logo = vala.logo;
									}

									if(input_type == 'multiselect_image_button'){

										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value:'',
											logo:STATIC_IMG_URL+vala.value
										};

									}else if(input_type == 'image_button'){

										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value:value,
											logo:logo
										};

									}else{
										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value:value,
											logo:logo
										};
									}

								}else{

									if(input_type == 'color_picker'){

										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											color_code:vala.value
										};

									}else if(input_type == 'radio_button'){


										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value: vala.value,
								
										};

									}else if(input_type == 'button'){


										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value: vala.value,
								
										};

									}else if(input_type == 'text'){

										obj_values = {
											text_hint_label :vala.group_id.toString()
										};
									}

								}

								counter -= 1;
								resolve(obj_values);
							
							})
						}],
						function (result, current) {
							if (counter === 0){ 
								arrValues.push(result[0]);
							
							}
						});

					});

		  		}

			});

		
		}else if(input_type == 'input_range'){
	

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_jazestore_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var counter = data.length;		
					var jsonItems = Object.values(data);

					var min_field_label;
					var max_field_label;
					var suffix_field_label;
					var separator_field_label;

					promiseForeach.each(jsonItems,[function (vala) {
						return new Promise(function (resolve, reject) {		
							
			

							if(vala.key_name == 'separator label'){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == 'min label'){
								min_field_label = vala.key_value;
							}

							if(vala.key_name == 'max label'){
								max_field_label = vala.key_value;
							}

							if(vala.key_name == 'suffix'){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								min_field_label:min_field_label,
								max_field_label:max_field_label,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

							counter -= 1;
							if (counter === 0){ 
								resolve(obj_values);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							arrResults.push(result[0]);
						}
					});

		  		}

			});
					
		}else if(input_type == 'select_range'){
	
		

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_jazestore_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){


					var counter = data.length;		
					var jsonItems = Object.values(data);

					var min_field_label;
					var min_field_value;
					var max_field_label;
					var max_field_value;
					var interval_value;
					var suffix_field_label;
					var separator_field_label;

					promiseForeach.each(jsonItems,[function (vala) {
						return new Promise(function (resolve, reject) {		
								
							if(vala.key_name == "separator label"){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == "min label"){
								min_field_label = vala.key_value;
							}


							if(vala.key_name == "min value"){
								min_field_value = vala.key_value;
							}

							if(vala.key_name == "max label"){
								max_field_label = vala.key_value;
							}


							if(vala.key_name == "max value"){
								max_field_value = vala.key_value;
							}

							if(vala.key_name == "interval value"){
								interval_value = vala.key_value;
							}


							if(vala.key_name == "suffix"){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								id:'0',
								min_field_label:min_field_label,
								min_field_value:min_field_value,
								max_field_label:max_field_label,
								max_field_value:max_field_value,
								internal_value:interval_value,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

							counter -= 1;
							if (counter === 0){ 
								resolve(obj_values);
							}
							
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							arrValues.push(result[0]);
						}
					});

		  		}

			});
					
		}

		setTimeout(function(){ 

			if(singleObj.includes(input_type)){
				return_result(obj_values);
			}else{
				return_result(arrValues.sort(sortingArrayObject('title')));


			}
			
		}, 500);

	}
	catch(err)
	{ console.log("getCompanyCategoryAttributeValue",err); return_result([]); }	
};

// ------ company attributes ----------- //

// ------ city information -----------

methods.getCityInformation =  function(arrParams,return_result){
	try
	{	

		var arrResults = [];
		var arrObjects = {};


		if(parseInt(arrParams.category_id) == 0){
			var category_id = '3';
			var query = `SELECT * FROM jaze_category_list WHERE group_id IN (SELECT group_id FROM jaze_category_list WHERE meta_key = 'parent_id' AND meta_value = ?) 
			AND  group_id IN (SELECT group_id FROM  jaze_category_list  WHERE meta_key = 'status' AND meta_value ='1' )`;
		}
		else if( parseInt(arrParams.category_id) == 947 )
		{
			var category_id = '947';
			var query = `SELECT * FROM jaze_category_list WHERE group_id IN (SELECT group_id FROM jaze_category_list WHERE meta_key = 'parent_id' AND meta_value = ?) 
			AND  group_id IN (SELECT group_id FROM  jaze_category_list  WHERE meta_key = 'status' AND meta_value ='1' )`;
		}
		else
		{
			var category_id = arrParams.category_id;
			var query = `SELECT * FROM jaze_cityinfo_subcat_details  WHERE group_id IN (SELECT group_id FROM jaze_cityinfo_subcat_details WHERE meta_key = 'parent_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_cityinfo_subcat_details WHERE meta_key = 'country_id' AND (meta_value =? OR meta_value = '0')) 
			AND group_id IN (SELECT group_id FROM jaze_cityinfo_subcat_details WHERE meta_key = 'state_id' AND (meta_value =? OR meta_value = '0')) 
			AND group_id IN (SELECT group_id FROM jaze_cityinfo_subcat_details WHERE meta_key = 'city_id' AND (meta_value =? OR meta_value = '0')) 
			AND group_id IN (SELECT group_id FROM jaze_cityinfo_subcat_details WHERE meta_key = 'status' AND meta_value = '1')`;

		}


		DB.query(query, [category_id,arrParams.country_id,arrParams.state_id,arrParams.city_id], function(data, error){

	
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {

						return new Promise(function (resolve, reject) {	

							var checkParam = {
								id:jsonItems.group_id,
								country_id:arrParams.country_id,
								state_id:arrParams.state_id,
								city_id:arrParams.city_id
							};

							methods.checkCategoryStatus(checkParam, function(retStat){	

								var img = '';
								if(parseInt(arrParams.category_id) == 0 || parseInt(arrParams.category_id) == 947){

					
							  		if(jsonItems.image_url){
										img = STATIC_IMG_URL_CAT_NEW+jsonItems.image_url;
							  		}

							  		arrObjects = {
							  			id           :jsonItems.group_id.toString(),
							  			name         :jsonItems.cate_name,
							  			logo         :img,
							  			child_status :retStat

							  		};

						  		}else{

					
							  		if(jsonItems.logo){
										img = STATIC_IMG_URL_CAT_NEW+jsonItems.logo;
							  		}

							  		arrObjects = {
							  			id           :jsonItems.group_id.toString(),
							  			name         :jsonItems.title,
							  			logo         :img,
							  			child_status :retStat

							  		};

						  		}

								arrResults.push(arrObjects);
								counter -= 1;
								resolve(arrResults);
							});

						})
					}],
					function (result, current) { 
			
						if (counter === 0){ 
							return_result(arrResults.sort(sortingArrayObject('name')));
	
						}

					});	

				});	

			
			}
			else{ return_result(null); }

		});




	}
	catch(err)
	{ console.log("getCityInformation",err); return_result(null,pagination_details);}
};


methods.checkCategoryStatus = function(arrParams,return_result)
{ 

	try
	{

		if(parseInt(arrParams.id) == 947 )
		{
			return_result('3');
		}
		else
		{
			var query = `SELECT COUNT(*) AS count FROM jaze_cityinfo_subcat_details WHERE group_id IN (SELECT group_id FROM jaze_cityinfo_subcat_details WHERE meta_key = 'parent_id' AND meta_value =?)
					AND group_id IN (SELECT group_id FROM jaze_cityinfo_subcat_details WHERE meta_key = 'country_id' AND (meta_value =? OR meta_value = '0')) 
					AND group_id IN (SELECT group_id FROM jaze_cityinfo_subcat_details WHERE meta_key = 'state_id' AND (meta_value =? OR meta_value = '0')) 
					AND group_id IN (SELECT group_id FROM jaze_cityinfo_subcat_details WHERE meta_key = 'city_id' AND (meta_value =? OR meta_value = '0'))  
					AND group_id IN (SELECT group_id FROM jaze_cityinfo_subcat_details WHERE meta_key = 'status' AND meta_value = '1')`;

			DB.query(query, [arrParams.id,arrParams.country_id,arrParams.state_id,arrParams.city_id], function(data, error){

				if( parseInt(data[0].count) !== 0)
				{	
					var quer = `SELECT COUNT(*) AS count FROM jaze_cityinfo_static_company WHERE group_id IN 
						(SELECT group_id FROM jaze_cityinfo_static_company WHERE meta_key = 'sub_category_id' OR meta_key ='parent_category_id' AND meta_value =?)
						AND group_id IN (SELECT group_id FROM jaze_cityinfo_static_company WHERE meta_key = 'country_id' AND (meta_value =? OR meta_value = '0')) 
						AND group_id IN (SELECT group_id FROM jaze_cityinfo_static_company WHERE meta_key = 'state_id' AND (meta_value =? OR meta_value = '0')) 
						AND group_id IN (SELECT group_id FROM jaze_cityinfo_static_company WHERE meta_key = 'city_id' AND (meta_value =?' OR meta_value = '0')) 
						AND group_id IN (SELECT group_id FROM jaze_cityinfo_static_company WHERE meta_key = 'status' AND meta_value = '1')`;

					DB.query(quer, [arrParams.id,arrParams.country_id,arrParams.state_id,arrParams.city_id], function(cdata, error){

						if( parseInt(data[0].count) == 1 && parseInt(cdata[0].count) == 1)
						{
							return_result('2'); 
						}
						else
						{ 
							return_result('0'); 
						}
					});
				}
				else
				{ 
					return_result('1'); 
				}
			});

		}

	}
	catch(err)
	{ console.log("checkCategoryStatus",err); return_result("0"); }	
}


// --------------- city information ---------


// --------------- tourist information ---------
methods.getTouristInformation =  function(arrParams,return_result){
	try
	{	

		var arrResults = [];
		var arrObjects = {};

		var query = `SELECT * FROM jaze_touristinfo_page_groups WHERE group_id IN (SELECT group_id FROM jaze_touristinfo_page_groups WHERE meta_key = 'city_id' AND meta_value =?)
				AND group_id IN (SELECT group_id FROM jaze_touristinfo_page_groups WHERE meta_key = 'category_id' AND meta_value =?)`;

		var flag;

		DB.query(query, [arrParams.city_id,arrParams.category_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){

					const forEach = async () => {
					  	const values = retConverted;
					  	for (const val of values){

							var checkParam = {id:val.group_id, type:val.category_type};
				  			methods.getTouristInformatioNDetails(checkParam, function(retDetails){			

	  							if(parseInt(val.category_type) == 0)
								{
									arrObjects = {
				  						details :retDetails,
				  						photos  :[],
				  						map     :[]
				  					};

				  					flag = '1';
								}
								else if(parseInt(val.category_type) == 2)
			  					{
	  								arrObjects = {
				  						details :[],
				  						photos  :retDetails,
				  						map     :[]
				  					};

			  						flag = '2';
			  					}
			  					else if(parseInt(val.category_type) == 1)
			  					{
	  								arrObjects = {
				  						details :[],
				  						photos  :[],
				  						map     :retDetails
				  					};

			  						flag = '3';
			  					}

		  						arrResults.push(arrObjects);
				  			});	

					  	}

				  		const result = await returnData(arrResults);
					}
					 
					const returnData = x => {
					  return new Promise((resolve, reject) => {
					    setTimeout(() => {
					      resolve(x);
					    }, 300);
					  });
					}
					 
					forEach().then(() => {
						return_result(arrResults[0],flag);
					})

				});	

			
			}
			else
			{ return_result(null,null); }

		});




	}
	catch(err)
	{ console.log("getTouristInformation",err); return_result(null,null);}
};



methods.getTouristInformatioNDetails = function(checkParam,return_result)
{ 

	try
	{
		var arrResults = [];
		var arrObjects = {};

		var query = `SELECT * FROM jaze_touristinfo_page_values WHERE group_id IN (SELECT group_id FROM jaze_touristinfo_page_values WHERE meta_key = 'page_group_id' AND meta_value =?)`;
		DB.query(query, [checkParam.id], function(data, error){
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){


				var counter = retConverted.length;		
				var jsonItems = Object.values(retConverted);

				promiseForeach.each(jsonItems,[function (jsonItems) {

					return new Promise(function (resolve, reject) {	

							
							var img = '';
					  		if(jsonItems.image_url){
								img = G_STATIC_IMG_URL+"uploads/jazenet/category/touristinfo/photos/"+jsonItems.image_url;
					  		}

  							if(parseInt(checkParam.type) == 0)
							{
						  		arrObjects = {
						  			description  :jsonItems.description,
					  			};
							}
							else if(parseInt(checkParam.type) == 2)
		  					{
  								arrObjects = {
			  						title  :jsonItems.description,
			  						image  :img
			  					}

		  					}
		  					else if(parseInt(checkParam.type) == 1)
		  					{
  								arrObjects = {
  									title  :jsonItems.description,
			  						image  :img
			  					}

		  					}

							arrResults.push(arrObjects);
							counter -= 1;
							resolve(arrResults);

						})
					}],
					function (result, current) { 
			
						if (counter === 0){ 

							return_result(result[0]);
	
						}

					});	

				});	

			}else{
				return_result(null);
			}

		});
	
	}
	catch(err)
	{ console.log("getTouristInformatioNDetails",err); return_result(null); }	
}


// --------------- tourist information ---------

//---------------- city information company ------------------------


methods.getCityInformationCompany = function(arrParams,return_result)
{ 

	try
	{

		var arrResults = [];
		var arrObjects = {};

		var query = `SELECT * FROM jaze_cityinfo_static_company WHERE group_id IN (SELECT group_id FROM jaze_cityinfo_static_company WHERE meta_key = 'sub_category_id' AND meta_value =?) `;

		if(parseInt(arrParams.country_id) !==0 ){
			query += " AND group_id IN (SELECT group_id FROM jaze_cityinfo_static_company WHERE meta_key = 'country_id' AND meta_value ='"+arrParams.country_id+"') ";
		}

		if(parseInt(arrParams.state_id) !==0 ){
			query += " AND group_id IN (SELECT group_id FROM jaze_cityinfo_static_company WHERE meta_key = 'state_id' AND meta_value ='"+arrParams.state_id+"') ";
		}

		if(parseInt(arrParams.city_id) !==0 ){
			query += " AND group_id IN (SELECT group_id FROM jaze_cityinfo_static_company WHERE meta_key = 'city_id' AND meta_value ='"+arrParams.city_id+"') ";	
		}

		
		DB.query(query, [arrParams.category_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){

				var counter = retConverted.length;		
				var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {

						return new Promise(function (resolve, reject) {	

							var phoneParam = {account_type:3, company_phone_code:jsonItems.company_phone_code};
							methods.__getMobileCode(phoneParam, function (retPhoneCode) {

								var geo_distance = '0';
								if(parseInt(arrParams.latitude) !==0 && parseInt(arrParams.longitude) !==0){
								  geo_distance = distance(parseFloat(arrParams.latitude), parseFloat(arrParams.longitude), parseFloat(jsonItems.latitude), parseFloat(jsonItems.longitude),"K");
								}

								var logo = '';
								if(jsonItems.listing_img){
									logo = G_STATIC_IMG_URL+"uploads/jazenet/company_city_static/"+jsonItems.listing_img.substr(0, jsonItems.listing_img.indexOf('_'))+'/listing_images/'+jsonItems.listing_img;
								}else if(jsonItems.company_logo){
									logo = STATIC_IMG_COMPANY+jsonItems.company_logo.substr(0, jsonItems.company_logo.indexOf('_'))+'/logo/'+jsonItems.company_logo;
								}
				
								arrObjects = {
									id:jsonItems.group_id.toString(),
									name:jsonItems.company_name,
									logo:logo,
									distance:geo_distance.toString()
								}

								arrResults.push(arrObjects);
								counter -= 1;
								resolve(arrResults);

							});	
						
						})
					}],
					function (result, current) { 
						
						if (counter === 0){ 

							if(parseInt(arrParams.latitude) !==0 && parseInt(arrParams.longitude) !==0){

							   	result[0].sort(function(a, b) {
								    return parseFloat(a.distance) - parseFloat(b.distance);
								});

								return_result(result[0]);

							}else{
								return_result(result[0].sort(sortingArrayObject('name')));
							}

						}

					});	

				});	

			}
			else
			{


	
				var query = `SELECT * FROM jaze_cityinfo_static_company  WHERE group_id IN (SELECT group_id FROM jaze_cityinfo_static_company WHERE meta_key ='parent_category_id' AND meta_value =?)
					AND group_id IN (SELECT group_id FROM jaze_cityinfo_static_company WHERE meta_key = 'status' AND meta_value = '1') `;

					if(parseInt(arrParams.country_id) !==0 ){
						query += " AND group_id IN (SELECT group_id FROM jaze_cityinfo_static_company WHERE meta_key = 'country_id' AND meta_value ='"+arrParams.country_id+"') ";
					}

					if(parseInt(arrParams.state_id) !==0 ){
						query += " AND group_id IN (SELECT group_id FROM jaze_cityinfo_static_company WHERE meta_key = 'state_id' AND meta_value ='"+arrParams.state_id+"') ";
					}

					if(parseInt(arrParams.city_id) !==0 ){
						query += " AND group_id IN (SELECT group_id FROM jaze_cityinfo_static_company WHERE meta_key = 'city_id' AND meta_value ='"+arrParams.city_id+"') ";	
					}

				DB.query(query, [arrParams.category_id], function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{

						methods.convertMultiMetaArray(data, function(retConverted){

							var counter = retConverted.length;		
							var jsonItems = Object.values(retConverted);

							promiseForeach.each(jsonItems,[function (jsonItems) {

								return new Promise(function (resolve, reject) {	

									var phoneParam = {account_type:3, company_phone_code:jsonItems.company_phone_code};
									methods.__getMobileCode(phoneParam, function (retPhoneCode) {

										var geo_distance = '0';
										if(parseInt(arrParams.latitude) !==0 && parseInt(arrParams.longitude) !==0){
										  geo_distance = distance(parseFloat(arrParams.latitude), parseFloat(arrParams.longitude), parseFloat(jsonItems.latitude), parseFloat(jsonItems.longitude),"K");
										}

										var logo = '';
										if(jsonItems.listing_img){
											var check_logo = jsonItems.listing_img.split(",");
											logo = STATIC_IMG_COMPANY+'/'+jsonItems.group_id+'/listing_images/'+check_logo[0];
										}else if(jsonItems.company_logo){
											logo = STATIC_IMG_COMPANY+jsonItems.company_logo.substr(0, jsonItems.company_logo.indexOf('_'))+'/logo/'+jsonItems.company_logo;
										}
						
										arrObjects = {
											id:jsonItems.group_id.toString(),
											name:jsonItems.company_name,
											logo:logo,
											distance:geo_distance.toString()
										}

										arrResults.push(arrObjects);
										counter -= 1;
										resolve(arrResults);

									});

								})
							}],
							function (result, current) { 
					
								if (counter === 0){ 

									if(parseInt(arrParams.latitude) !==0 && parseInt(arrParams.longitude) !==0){

									   	result[0].sort(function(a, b) {
										    return parseFloat(a.distance) - parseFloat(b.distance);
										});

										return_result(result[0]);

									}else{
										return_result(result[0].sort(sortingArrayObject('name')));
									}

								}
							});	

						});	

					
					}else{
						return_result(null); 
					}
				});
	

				//return_result(null);
			}

		});
	
	}
	catch(err)
	{ console.log("getCityInformationCompany",err); return_result(null); }	
}


methods.getCityInformationCompanyDetails = function(arrParams,return_result)
{ 

	try
	{

		var arrResults = [];
		var arrObjects = {};

		var query = `SELECT * FROM jaze_cityinfo_static_company WHERE group_id =? AND group_id IN (SELECT group_id FROM jaze_cityinfo_static_company WHERE meta_key = 'status' AND meta_value ='1') `;

		DB.query(query, [arrParams.category_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){

				var counter = retConverted.length;		
				var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {

						return new Promise(function (resolve, reject) {	

							var phoneParam = {account_type:3, company_phone_code:jsonItems.company_phone_code};

							methods.__getMobileCode(phoneParam, function (retPhoneCode) {

								methods.__getCountryDetails(jsonItems.country_id, function (country) {

									methods.__getStateDetails(jsonItems.state_id, function (state) {

										methods.__getCityDetails(jsonItems.city_id, function (city) {

											methods.__getAreaDetails(jsonItems.area_id, function (area) {

												methods.__getParentDetails(jsonItems.parent_category_id, function (parentD) {
								
													methods.__getSubDetails(jsonItems.sub_category_id, function (subCat) {


														var geo_distance = '0';
														if(parseInt(arrParams.latitude) !==0 && parseInt(arrParams.longitude) !==0){
														  geo_distance = distance(parseFloat(arrParams.latitude), parseFloat(arrParams.longitude), parseFloat(jsonItems.latitude), parseFloat(jsonItems.longitude),"K");
														}
														var img_num_listing = '';
														if(jsonItems.listing_img){
															img_num_listing = jsonItems.listing_img.substr(0, jsonItems.listing_img.indexOf('_')); 
														}

														var img_num_logo = '';
														if(jsonItems.company_logo){
															img_num_logo = jsonItems.company_logo.substr(0, jsonItems.company_logo.indexOf('_')); 
														}
			

														arrObjects = {													
															parent_category_id  : jsonItems.parent_category_id.toString(),
															parent_cateory_name : parentD,
															sub_category_id     : jsonItems.sub_category_id.toString(),
															sub_category_name   : subCat,
															id                  : jsonItems.group_id.toString(),
															name                : jsonItems.company_name,
															image               : STATIC_IMG_COMPANY+img_num_listing+'/listing_images/'+jsonItems.listing_img,
															logo                : STATIC_IMG_COMPANY+img_num_logo+'/logo/'+jsonItems.company_logo,
															phone_code          : retPhoneCode,
															phone_number        : jsonItems.company_phone,
															email               : jsonItems.company_email,
															street_name         : jsonItems.street_name,
															street_no           : jsonItems.street_no,
															building_name       : jsonItems.building_name,
															building_no         : jsonItems.building_no,
															country_id          : jsonItems.country_id,
															country_name        : country,
															state_id            : jsonItems.state_id,
															state_name          : state,
															city_id             : jsonItems.city_id,
															city_name           : city,
															area_id             : jsonItems.area_id,
															area_name           : area,
															website             : jsonItems.company_website,
															about               : jsonItems.company_about,
															latitude            : jsonItems.latitude,
															longitude           : jsonItems.longitude,
															distance            : geo_distance.toString(),
															link_account_id     : '0'
														};

														arrResults.push(arrObjects);
														counter -= 1;
														resolve(arrObjects);

													});	

												});	

											});	

										});	

									});	

								});	

							});	
						
						})
					}],
					function (result, current) { 
						if (counter === 0){ 
							return_result(result[0]);
						}
					});	

				});	

			}else{
				return_result(null);
			}

		});
	
	}
	catch(err)
	{ console.log("getCityInformationCompanyDetails",err); return_result(null); }	
}


methods.__getParentDetails = function(parent_id,return_result){

	try{

		var querParent = `SELECT t1.* FROM(
			SELECT (CASE WHEN meta_key = 'cate_name' THEN meta_value END) AS cate_name
			FROM  jaze_category_list
			WHERE group_id =? ) AS t1 WHERE cate_name != ''`;

		DB.query(querParent, [parent_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				return_result(data[0].cate_name);
			}
			else
			{return_result('')}

		});

	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getSubDetails = function(sub_category_id,return_result){

	try{

		var querSub = `SELECT t1.* FROM(
			SELECT (CASE WHEN meta_key = 'title' THEN meta_value END) AS title
			FROM  jaze_cityinfo_subcat_details
			WHERE group_id =? ) AS t1 WHERE title != ''`;

		DB.query(querSub, [sub_category_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				return_result(data[0].title);
			}
			else
			{return_result('')}

		});
	}
	catch(err)
	{
		return_result('')
	}
};

// ------------------------------------ city information company ----------------------


// ---------------------  government ----------------------------


methods.getCityInformationGovernment = function(arrParams,return_result)
{ 

	try
	{
		var arrRows = [];
		var arrResults = [];
		var arrObjects = {};

		methods.getLocalGovernment(arrParams, function (retCityDetails) {
			if(retCityDetails!=null){
				arrRows.push(retCityDetails[0]);
			}
			methods.getStateGovernment(arrParams, function (retStateDetails) {
				if(retStateDetails!=null){
					arrRows.push(retStateDetails[0]);
				}
				methods.getNationalGovernment(arrParams, function (retCountryDetails) {
					if(retCountryDetails!=null){
						arrRows.push(retCountryDetails[0]);
					}
					
					if(arrRows.length > 0 )
					{
						var counter = arrRows.length;		
						var jsonItems = Object.values(arrRows);

						promiseForeach.each(jsonItems,[function (jsonItems) {

							return new Promise(function (resolve, reject) {	

								var title = '';
								if(parseInt(jsonItems.type) == 3){
									title = 'Local';
								}else if(parseInt(jsonItems.type) == 2){
									title = 'State';
								}else if(parseInt(jsonItems.type) == 1){
									title = 'National';
								}
						
								arrObjects = {
									id:jsonItems.group_id.toString(),
									geo_id:jsonItems.geo_id.toString(),
									title:title,
									logo:G_STATIC_IMG_URL+"uploads/jazenet/category/government_services/"+jsonItems.geo_logo
								}

								arrResults.push(arrObjects);
								counter -= 1;
								resolve(arrResults);		

							})
						}],
						function (result, current) { 
							
							if (counter === 0){ 
								return_result(result[0]);
							}

						});	
					}
					else
					{
						return_result(null);
					}

				});	
			});	
		});	

	}
	catch(err)
	{ console.log("getCityInformationGovernment",err); return_result(null); }	
}



methods.getLocalGovernment = function(arrParams,return_result)
{ 

	try
	{

		var arrResults = [];

		var queryCity = `SELECT * FROM jaze_government_list WHERE group_id IN (SELECT group_id FROM jaze_government_list WHERE meta_key = 'type' AND meta_value = '3' ) 
			AND group_id IN (SELECT group_id FROM jaze_government_list WHERE meta_key = 'geo_id' AND meta_value =? )  
			AND group_id IN (SELECT group_id FROM jaze_government_list WHERE meta_key = 'status' AND meta_value ='1' )`;



		DB.query(queryCity, [arrParams.city_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {

						return new Promise(function (resolve, reject) {	

							arrResults.push(jsonItems);
							counter -= 1;
							resolve(arrResults);		

						})
					}],
					function (result, current) { 
						
						if (counter === 0){ 
							return_result(result[0]);
						}

					});	
				});	

			}else{
				return_result(null);
			}

		});
	
	}
	catch(err)
	{ console.log("getLocalGovernment",err); return_result(null); }	
}


methods.getStateGovernment = function(arrParams,return_result)
{ 

	try
	{
		var arrResults = [];

		var query = `SELECT * FROM jaze_government_list WHERE group_id IN (SELECT group_id FROM jaze_government_list WHERE meta_key = 'type' AND meta_value = '2' ) 
			AND group_id IN (SELECT group_id FROM jaze_government_list WHERE meta_key = 'geo_id' AND meta_value =? ) 
			AND group_id IN (SELECT group_id FROM jaze_government_list WHERE meta_key = 'status' AND meta_value ='1' )`;

		DB.query(query, [arrParams.state_id], function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {

						return new Promise(function (resolve, reject) {	

							arrResults.push(jsonItems);
							counter -= 1;
							resolve(arrResults);		

						})
					}],
					function (result, current) { 
						
						if (counter === 0){ 
							return_result(result[0]);
						}

					});	
				});	

			}else{
				return_result(null);
			}

		});
	
	}
	catch(err)
	{ console.log("getStateGovernment",err); return_result(null); }	
}

methods.getNationalGovernment = function(arrParams,return_result)
{ 

	try
	{

		var arrResults = [];

		var query = `SELECT * FROM jaze_government_list WHERE group_id IN (SELECT group_id FROM jaze_government_list WHERE meta_key = 'type' AND meta_value = '1' ) 
			AND group_id IN (SELECT group_id FROM jaze_government_list WHERE meta_key = 'geo_id' AND meta_value =? ) 
			AND group_id IN (SELECT group_id FROM jaze_government_list WHERE meta_key = 'status' AND meta_value ='1' )`;

		DB.query(query, [arrParams.country_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {

						return new Promise(function (resolve, reject) {	


							arrResults.push(jsonItems);
							counter -= 1;
							resolve(arrResults);		

						})
					}],
					function (result, current) { 
						
						if (counter === 0){ 
							return_result(result[0]);
						}

					});	
				});	

			}else{
				return_result(null);
			}

		});
	
	}
	catch(err)
	{ console.log("getNationalGovernment",err); return_result(null); }	
}


methods.getGeoIDList = function(arrParams,return_result)
{ 

	try
	{

		var arrResults = [];
		var arrObjects = {};

		var query = `SELECT * FROM jaze_government_company_list WHERE group_id IN (SELECT group_id FROM jaze_government_company_list WHERE meta_key = 'status' AND meta_value = '1' ) `;

		if(parseInt(arrParams.country_id) !== 0){
			query += " AND group_id IN (SELECT group_id FROM jaze_government_company_list WHERE meta_key = 'jaze_country_id' AND meta_value = '"+arrParams.country_id+"' )";
		}

		if(parseInt(arrParams.state_id) !== 0){
			query += " AND group_id IN (SELECT group_id FROM jaze_government_company_list WHERE meta_key = 'jaze_state_id' AND meta_value = '"+arrParams.state_id+"' )";
		}

		if(parseInt(arrParams.city_id) !== 0){
			query += " AND group_id IN (SELECT group_id FROM jaze_government_company_list WHERE meta_key = 'jaze_city_id' AND meta_value = '"+arrParams.city_id+"' )";
		}

		DB.query(query, null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {

						return new Promise(function (resolve, reject) {	

							var accParams = {
								account_id:jsonItems.account_id,
								lat:arrParams.latitude,
								lon:arrParams.longitude
							};

			  				methods.getCompanyProfileGovernment(accParams, function(retAccountDetails){
			  				
			  					if(retAccountDetails!=null){
									arrObjects = {
				  						account_id:jsonItems.account_id.toString(),
				  						name:retAccountDetails.name,
				  						logo:retAccountDetails.logo,
				  						distance:retAccountDetails.distance.toString()
				  					};

									arrResults.push(arrObjects);

			  					}	

								counter -= 1;

								if (counter === 0){ 
									resolve(arrResults);
								}
			
							});	

						})
					}],
					function (result, current) { 
						
						if (counter === 0){ 

							if(parseInt(arrParams.latitude) !==0 && parseInt(arrParams.longitude) !==0){

							   	result[0].sort(function(a, b) {
								    return parseFloat(a.distance) - parseFloat(b.distance);
								});

							   	setTimeout(function(){
										return_result(result[0]);
								}, 100);

							}else{
								return_result(result[0].sort(sortingArrayObject('name')));
							}
	
						}

					});	
				});	

			}else{
				return_result(null);
			}

		});
	
	}
	catch(err)
	{ console.log("getLocalGovernment",err); return_result(null); }	
}



methods.getCompanyProfileGovernment =  function(arrParams,return_result){
	try
	{
			
		var newObj = {};
	
		var query = `SELECT * FROM jaze_user_basic_details WHERE account_id =? and account_type ='5'`;

		DB.query(query,[arrParams.account_id], function(data, error){
	
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	
				methods.convertMultiMetaArrayCompany(data, function(retConverted){	



					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

				  			var default_logo = '';
			  				var geo_distance = '0'
		  					var company_name = '';

  							if(jsonItems.listing_img){
								default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+jsonItems.account_id+'/listing_images/' + jsonItems.listing_img + '';
							}else if(jsonItems.cover_image){
								default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+jsonItems.account_id+'/cover_image/' + jsonItems.cover_image + '';
							}else if(jsonItems.company_logo){
								default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+jsonItems.account_id+'/logo/' + jsonItems.company_logo + '';
							}

							if(parseInt(arrParams.lat) !==0 && parseInt(arrParams.lon) !==0){
								geo_distance = distance(parseFloat(arrParams.lat), parseFloat(arrParams.lon), parseFloat(jsonItems.latitude), parseFloat(jsonItems.longitude),"K");
							}

							if(jsonItems.company_name){
								company_name = jsonItems.company_name;
							}
						
							newObj = {
								ccount_id	  : jsonItems.account_id.toString(),
								name		  : company_name,
								logo		  : default_logo,
								distance      : geo_distance
			 				}	

							counter -= 1;
							if (counter === 0){ 
								resolve(newObj);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});

				});

			}
			else
			{
				return_result(null);
			}
		});

	}
	catch(err)
	{ console.log("getCompanyProfile",err); return_result(null);}
};


methods.getGovernmentNotice = function(arrParams,return_result)
{ 

	try
	{
		var arrRows = [];
		var arrResults = [];
		var arrObjects = {};
		var arrPaginate = [];

		var pagination_details  = {
			total_page   : '0',
        	current_page : '0',
        	next_page    : '0',
        	total_result : '0'
        };

        if(parseInt(arrParams.country_id) !=0 || parseInt(arrParams.state_id) !==0 || parseInt(arrParams.city_id) !==0 )
    	{
    
			methods.getLocalGovernmentNotice(arrParams, function (retCityDetails) {
				if(parseInt(arrParams.city_id) !==0 ){
					if(retCityDetails!=null){
						arrRows.push(retCityDetails);
					}
				}
				methods.getStateGovernmentNotice(arrParams, function (retStateDetails) {
					if(parseInt(arrParams.state_id) !==0 ){
						if(retStateDetails!=null){
							arrRows.push(retStateDetails);
						}
					}
					methods.getNationalGovernmentNotice(arrParams, function (retCountryDetails) {
						if(parseInt(arrParams.country_id) !==0 ){
							if(retCountryDetails!=null){
								arrRows.push(retCountryDetails);
							}
						}
						var filteredArray = removeDuplicates(arrRows[0]);
						if(arrRows[0].length > 0 )
						{
							methods.getCompanyNotice(filteredArray,arrParams, function (retNotice,retPaginate) {
								if(retNotice!=null){
									return_result(retNotice,retPaginate);
								}else{
									return_result(null,pagination_details);
								}
							});
						}
						else
						{
							return_result(null,pagination_details);
						}
					});	
				});	
			});	
		}
		else
		{
			return_result(null,pagination_details);
		}

	}
	catch(err)
	{ console.log("getGovernmentNotice",err); return_result(null); }	
}

methods.getLocalGovernmentNotice = function(arrParams,return_result)
{ 

	try
	{

		var arrResults = [];

		var queryCity = `SELECT * FROM jaze_government_company_list WHERE group_id IN ( SELECT group_id FROM jaze_government_company_list WHERE meta_key = 'jaze_city_id' AND meta_value =? )`;

		DB.query(queryCity, [arrParams.city_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {

						return new Promise(function (resolve, reject) {	

							arrResults.push(jsonItems.account_id.toString());

							counter -= 1;
							if (counter === 0){
								resolve(arrResults);		
							}
						})
					}],
					function (result, current) { 
						
						if (counter === 0){ 
							return_result(result[0]);
						}

					});	
				});	

			}else{
				return_result(null);
			}

		});
	
	}
	catch(err)
	{ console.log("getLocalGovernmentNotice",err); return_result(null); }	
}

methods.getStateGovernmentNotice = function(arrParams,return_result)
{ 

	try
	{
		var arrResults = [];

		var query = `SELECT * FROM jaze_government_company_list WHERE group_id IN ( SELECT group_id FROM jaze_government_company_list WHERE meta_key = 'jaze_state_id' AND meta_value =?)`;

		DB.query(query, [arrParams.state_id], function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {

						return new Promise(function (resolve, reject) {	

							arrResults.push(jsonItems.account_id.toString());

							counter -= 1;
							if (counter === 0){ 
								resolve(arrResults);		
							}

						})
					}],
					function (result, current) { 
						
						if (counter === 0){ 
							return_result(result[0]);
						}

					});	
				});	

			}else{
				return_result(null);
			}

		});
	
	}
	catch(err)
	{ console.log("getStateGovernmentNotice",err); return_result(null); }	
}

methods.getNationalGovernmentNotice = function(arrParams,return_result)
{ 

	try
	{

		var arrResults = [];

		var query = `SELECT * FROM jaze_government_company_list WHERE group_id IN ( SELECT group_id FROM jaze_government_company_list WHERE meta_key = 'jaze_country_id' AND meta_value =?)`;

		DB.query(query, [arrParams.country_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {

						return new Promise(function (resolve, reject) {	

							arrResults.push(jsonItems.account_id.toString());

							counter -= 1;
							if (counter === 0){ 
								resolve(arrResults);		
							}	

						})
					}],
					function (result, current) { 
						
						if (counter === 0){ 
							return_result(result[0]);
						}

					});	
				});	

			}else{
				return_result(null);
			}

		});
	
	}
	catch(err)
	{ console.log("getNationalGovernmentNotice",err); return_result(null); }	
}

methods.getCompanyNotice = function(arrAccounts,arrParams,return_result)
{ 

	try
	{
		var pagination_details  = {
			total_page   : '0',
        	current_page : '0',
        	next_page    : '0',
        	total_result : '0'
        };

		var arrResults = [];
		var arrObj = {};

		var query = `SELECT * FROM jaze_company_notice WHERE group_id IN (SELECT group_id FROM jaze_company_notice WHERE meta_key = 'account_id' AND meta_value IN(?)) 
		AND group_id IN (SELECT group_id FROM jaze_company_notice WHERE meta_key = 'created_date' AND meta_value >= DATE_SUB(NOW(),INTERVAL 1 YEAR) )`;

		DB.query(query, [arrAccounts], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {

						return new Promise(function (resolve, reject) {	

							var accParams = {account_id:jsonItems.account_id};
							methods.getCompanyProfileNotice(accParams, function(retProfile){

								if(retProfile!=null){

									arrObj = {
										notice_id    : jsonItems.group_id.toString(),
										account_id   : jsonItems.account_id.toString(),
										company_name : retProfile.name,
										company_logo : retProfile.logo,
										title        : jsonItems.title,
										description  : jsonItems.description,
										image_url    : G_STATIC_IMG_URL+"uploads/jazenet/company/"+jsonItems.account_id+'/jazenotice/'+jsonItems.image_url,
										date         : jsonItems.created_date
									}

									arrResults.push(arrObj);
								}

								counter -= 1;
								if (counter === 0){ 
									resolve(arrResults);		
								}	
			
							});

						})
					}],
					function (result, current) {

						if (counter === 0){ 


							var sortedArray = result[0].sort(function(a, b) {
							    return a.date < b.date ? 1 : a.date > b.date ? -1 : 0;
							});

							var newArray = [];
							var total_page = 0;
							var next_page = 0;

							var total_result = sortedArray.length.toString();

							if(parseInt(sortedArray.length) > 50){
								newArray   = sortedArray.slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
								total_page = Math.ceil((parseInt(sortedArray.length) - 50 ) / 50);
							}else{
								newArray   = sortedArray.slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
								total_page = 0;
							}

					
							if(parseInt(total_page) !== 0){
								next_page  = parseInt(arrParams.page)+1;
							}		

						   	if(parseInt(next_page) > parseInt(total_page)){
								next_page = 0;
					   		}

							if( parseInt(arrParams.page) <= parseInt(total_page) )
							{
								
								pagination_details  = {
									total_page   : total_page.toString(),
					                current_page : arrParams.page.toString(),
					                next_page    : next_page.toString(),
					                total_result : total_result
					            };
						
								return_result(newArray,pagination_details);

				            }else{
			            		return_result(null,pagination_details);
				            }
						}

					});	
				});	

			}else{
				return_result(null,pagination_details);
			}

		});
	
	}
	catch(err)
	{ console.log("getCompanyNotice",err); return_result(null); }	
}

methods.getCompanyProfileNotice =  function(arrParams,return_result){
	try
	{
		
		var newObj = {};
		var queryAccountType = `SELECT * FROM jaze_user_basic_details WHERE account_id =? and account_type = '5'`;
		DB.query(queryAccountType,[arrParams.account_id], function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArrayCompany(data, function(retConverted){	



					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (val) {
						return new Promise(function (resolve, reject) {		
				
						
		  					if(val.company_logo !== "" && val.company_logo !== null)
							{
								default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/logo/' + val.company_logo + '';
							}
							else
							{
								default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/logo/' + val.listing_images + '';
							}

							newObj = {
								account_id	  : val.account_id.toString(),
								name		  : val.company_name,
								logo		  : default_logo,
								country_id    : val.country_id.toString(),
								city_id       : val.city_id.toString(),
								state_id      : val.state_id.toString()
							}

							counter -= 1;
							if (counter === 0){ 
								resolve(newObj);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});

				});
			}
			else
			{
				return_result(null);
			}
		});

	}
	catch(err)
	{ console.log("getCompanyProfileNotice",err); return_result(null);}
};


// --------------------  government --------------------


// -------------------category list---------------------- //

methods.getCategoryListFromJazenet = function(arrParams,return_result)
{ 

	var arrRows = [];
	var newObj = {};

	try
	{	

		var category_id = '1';

		if(parseInt(arrParams.category_id) !== 0 ){
			category_id = arrParams.category_id;
		}

		var query = `SELECT t1.* FROM(
			SELECT group_id,
			(CASE WHEN meta_key = 'parent_id' THEN meta_value END) AS parent_id,
			(SELECT c.meta_value FROM jaze_category_list c WHERE c.meta_key = 'cate_name' AND c.group_id = jaze_category_list.group_id) AS cate_name,
			(SELECT c.meta_value FROM jaze_category_list c WHERE c.meta_key = 'url_key' AND c.group_id = jaze_category_list.group_id) AS url_key,
			(SELECT c.meta_value FROM jaze_category_list c WHERE c.meta_key = 'image_url' AND c.group_id = jaze_category_list.group_id) AS image_url
			FROM  jaze_category_list
			WHERE group_id IN (SELECT group_id FROM  jaze_category_list  WHERE meta_key = 'parent_id' AND meta_value =? )
			AND  group_id IN (SELECT group_id FROM  jaze_category_list  WHERE meta_key = 'status' AND meta_value ='1' )
		) AS t1 WHERE parent_id != "" ORDER BY cate_name ASC `;

		DB.query(query, [category_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				const forEach = async () => {
				  	const values = data;
				  	for (const val of values){
							
					

						var checkParam = {id:val.group_id,category_id:arrParams.category_id};
						methods.checkCategoryStatusFromJazenet(checkParam, function(retStat){	

							var img = '';
					  		if(val.image_url){
								img = G_STATIC_IMG_URL+"uploads/services_cat_images"+'/'+val.image_url;
					  		}
					  		
					  		newObj = {
					  			id: val.group_id.toString(),
					  			name:val.cate_name,
					  			logo:img,
					  			child_status:retStat

					  		};

					  	
							arrRows.push(newObj);
						});
					
				  	}

			  		const result = await returnData(arrRows);
				}
				 
				const returnData = x => {
				  return new Promise((resolve, reject) => {
				    setTimeout(() => {
				      resolve(x);
				    }, 500);
				  });
				}
				 
				forEach().then(() => {

					return_result(arrRows.sort(sortingArrayObject('name')));
				})

			}
			else
			{
				return_result("0"); 
			}
		
		});


	}
	catch(err)
	{ console.log("getCategoryListFromJazenet",err); return_result("0"); }	
}


methods.checkCategoryStatusFromJazenet = function(arrParams,return_result)
{ 

	try
	{
		if(parseInt(arrParams.id) == 57 || parseInt(arrParams.id) == 208 || parseInt(arrParams.id) == 696)
		{
		
			return_result('3');
		}
		else
		{
			var query = `SELECT COUNT(*) AS count FROM jaze_category_list WHERE group_id IN (SELECT group_id FROM jaze_category_list WHERE meta_key = 'parent_id' AND meta_value =?)
								AND group_id IN (SELECT group_id FROM jaze_category_list WHERE meta_key = 'status' AND meta_value = '1') `;

			DB.query(query, [arrParams.id], function(data, error){

				
				if( parseInt(data[0].count) != 0)
				{
					var quer = `SELECT COUNT(*) AS count FROM jaze_user_basic_details WHERE meta_key = 'category_id' AND meta_value =?`;

					DB.query(quer, [arrParams.id], function(cdata, error){
	
						if( parseInt(data[0].count) == 1 && parseInt(cdata[0].count) == 1)
						{
							return_result('2'); 
						}
						else
						{ 
							return_result('0'); 
						}
					});
				}
				else
				{ 
					return_result('1'); 
				}
			});

		}


	}
	catch(err)
	{ console.log("checkCategoryStatusFromJazenet",err); return_result("0"); }	
}


// -------------------category list---------------------- //



// ----- search category start ----- //

methods.searchCategory = function(arrParams,return_result)
{

	try
	{
		var arrCategory = [];
		var arrAccounts = [];
		var arrAttributes = [];
		var objCategory = {};
		var objController = {};

		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };

	    let category_id;


	  	if(arrParams.attributes.length > 0)
	  	{

  			var query = `SELECT * FROM jaze_category_attributes_groups WHERE group_id IN (SELECT group_id FROM jaze_category_attributes_groups WHERE meta_key = 'cat_id' AND meta_value =?)`;

			DB.query(query, [arrParams.category_id], function(data, error){


				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{	

					const forEach = async () => {
					  	const values = arrParams.attributes;
					  	for (const val of values){

					  		var parameter = '';
					  		var query = '';
				  		
							if(parseInt(val.type) >= 1 &&  parseInt(val.type) <= 6)
							{	
								var title = val.value.toLowerCase();
								if(val.value.toLowerCase() != ''){
									title = val.value.toLowerCase().split(",");
								}
				
								parameter = title;

								query = "SELECT * FROM jaze_category_attributes_groups_meta ";
								query += " WHERE group_id IN (SELECT group_id FROM jaze_category_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value ='"+data[0].group_id+"') ";
								query += " AND group_id IN (SELECT group_id FROM  jaze_category_attributes_groups_meta  WHERE meta_key = 'status' AND meta_value = '1' ) ";

								query += " AND group_id IN (SELECT group_id FROM  jaze_category_attributes_groups_meta  WHERE meta_key = 'key_name' AND LOWER(meta_value) IN (?) )";
						
							}
							else if(parseInt(val.type) == 7)
							{	
								query = "SELECT * FROM jaze_category_attributes_groups_meta ";
								query += " WHERE group_id IN (SELECT group_id FROM jaze_category_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value ='"+data[0].group_id+"') ";
								query += " AND group_id IN (SELECT group_id FROM  jaze_category_attributes_groups_meta  WHERE meta_key = 'status' AND meta_value = '1' ) ";
								query += " AND group_id IN (SELECT group_id FROM jaze_category_attributes_groups_meta where meta_key ='value' and ( meta_value >='"+val.min_value+"' and meta_value <='"+val.max_value+"') )";
							}
							else if(parseInt(val.type) == 8)
							{
								query += "SELECT * FROM jaze_company_attribute_values WHERE meta_key='"+data[0].group_id+"' AND meta_value =? ";
								parameter = val.value;
							}
							else if(parseInt(val.type) == 9 || parseInt(val.type) == 10)
							{
								query += "SELECT * FROM jaze_company_attribute_values WHERE meta_key='"+data[0].group_id+"' AND LOWER(meta_value) LIKE '%" +val.value.toLowerCase()+ "%' ";
							}

							DB.query(query,[parameter], function(catGrp, error){

								if(typeof catGrp !== 'undefined' && catGrp !== null && parseInt(catGrp.length) > 0)
								{	

									methods.convertMultiMetaArray(catGrp, function(retConverted){
										arrAttributes.push(retConverted[0].attribute_id);

									});
								}

							});

					  	}
				  		const result = await returnData(arrAttributes);
					}
					 
					const returnData = x => {
					  return new Promise((resolve, reject) => {
					    setTimeout(() => {
					      resolve(x);
					    }, 300);
					  });
					}

					forEach().then(() => {

						if(arrAttributes.length > 0 )
						{

							var filteredArray = removeDuplicates(arrAttributes);
							
							var quer = "SELECT account_id FROM jaze_company_attribute_values WHERE meta_key IN(?)";

							DB.query(quer, [filteredArray], function(data, error){
	
								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
								{	

									const forEach = async () => {
									  	const values = data;
									  	for (const val of values){

									  		arrAccounts.push(val.account_id.toString());

									  	}
								  		const result = await returnData(arrAccounts);
									}
									 
									const returnData = x => {
									  return new Promise((resolve, reject) => {
									    setTimeout(() => {
									      resolve(x);
									    }, 300);
									  });
									}

									forEach().then(() => {	

										var filteredArrayAccounts = removeDuplicates(arrAccounts);
										methods.processInnerResultCompanyForCategory(filteredArrayAccounts,arrParams, function(retProcessed,pagination_details){
											return_result([],retProcessed,pagination_details);
										});

									})

								}
								else
								{
									return_result([],null,pagination_details);
								}

							});

						}
						else
						{
							return_result([],null,pagination_details);
						}

					})

				}
				else
				{
					return_result([],null,pagination_details);
				}

			});
		}
		else
		{

			var query = `SELECT * FROM jaze_category_attributes_groups WHERE group_id IN (SELECT group_id FROM jaze_category_attributes_groups WHERE meta_key = 'cat_id' AND meta_value =?)`;

			DB.query(query, [arrParams.category_id], function(catGrp, error){

				if(typeof catGrp !== 'undefined' && catGrp !== null && parseInt(catGrp.length) > 0)
				{	

					methods.convertMultiMetaArray(catGrp, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (val) {
							return new Promise(function (resolve, reject) {		
								
								methods.getCategoryAttributeValueCategory(val.group_id,val.input_type, function(retMainArr){	

									var controller_type;

	 		  						if(val.input_type == 'multiselect_image_button'){
										controller_type = '1';
										objController = {
											controller_type:'1',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
											home_sort_order:val.details_sort_order.toString(),
											adv_status:val.is_listing_page.toString(),
											adv_sort_order:val.listing_sort_order.toString(),
											attribute_option_label:'all',
											items_value_list:retMainArr
										};
									}else if(val.input_type == 'multiselect_dropdown'){
										controller_type = '2';
										objController = {
											controller_type:'2',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
											home_sort_order:val.details_sort_order.toString(),
											adv_status:val.is_listing_page.toString(),
											adv_sort_order:val.listing_sort_order.toString(),
											attribute_option_label:'all',
											items_value_list:retMainArr
										};
									}else if(val.input_type == 'color_picker'){
										controller_type = '3';
										objController = {
											controller_type:'3',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
											home_sort_order:val.details_sort_order.toString(),
											adv_status:val.is_listing_page.toString(),
											adv_sort_order:val.listing_sort_order.toString(),
											attribute_option_label:'all',
											items_color_list:retMainArr
										};
									}else if(val.input_type == 'image_button'){
										controller_type = '4';
										objController = {
											controller_type:'4',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
											home_sort_order:val.details_sort_order.toString(),
											adv_status:val.is_listing_page.toString(),
											adv_sort_order:val.listing_sort_order.toString(),
											attribute_option_label:'all',
											items_value_list:retMainArr
										};
									}else if(val.input_type == 'dropdown'){
										controller_type = '5';
										objController = {
											controller_type:'5',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
											home_sort_order:val.details_sort_order.toString(),
											adv_status:val.is_listing_page.toString(),
											adv_sort_order:val.listing_sort_order.toString(),
											attribute_option_label:'all',
											items_value_list:retMainArr
										};
									}else if(val.input_type == 'radio_button'){
										controller_type = '6';
										objController = {
											controller_type:'6',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
											home_sort_order:val.details_sort_order.toString(),
											adv_status:val.is_listing_page.toString(),
											adv_sort_order:val.listing_sort_order.toString(),
											attribute_option_label:'',
											items_sing_value_list:retMainArr
										};
									}else if(val.input_type == 'select_range'){
										controller_type = '7';
										objController = {
											controller_type:'7',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
											home_sort_order:val.details_sort_order.toString(),
											adv_status:val.is_listing_page.toString(),
											adv_sort_order:val.listing_sort_order.toString(),
											attribute_option_label:'',
											items_select_range_value:retMainArr
										};
									}else if(val.input_type == 'input_range'){
										controller_type = '8';
										objController = {
											controller_type:'8',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
											home_sort_order:val.details_sort_order.toString(),
											adv_status:val.is_listing_page.toString(),
											adv_sort_order:val.listing_sort_order.toString(),
											attribute_option_label:'all',
											items_input_range_value:retMainArr
										};
									}else if(val.input_type == 'text'){
										controller_type = '9';
										objController = {
											controller_type:'9',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
											home_sort_order:val.details_sort_order.toString(),
											adv_status:val.is_listing_page.toString(),
											adv_sort_order:val.listing_sort_order.toString(),
											attribute_option_label:'',
											items_edittext_value:retMainArr
										};
									}
									
									arrCategory.push(objController);
							
									counter -= 1;
									resolve(arrCategory);

								});

							})
						}],
						function (result, current) {
							if (counter === 0){ 	

								methods.searchCompanyForCategory(arrParams.category_id,arrParams, function(retCompany,pagination_details){	

									if(retCompany!=null){
										if(arrParams.page > 0){
											return_result([],retCompany,pagination_details);
										}else{
											return_result(result[0],retCompany,pagination_details);
										}
										
									}else{
										return_result(result[0],null,pagination_details);
									}
			
								});
							}
						});

					});

				}else{



					methods.searchCompanyForCategory(arrParams.category_id,arrParams, function(retCompany,pagination_details){	


						if(retCompany!=null){
							if(arrParams.page > 0){
								return_result([],retCompany,pagination_details);
							}else{
								return_result([],retCompany,pagination_details);
							}
							
						}else{
							return_result([],null,pagination_details);
						}

					});

					
				}

			});	

		}

	}
	catch(err)
	{ 
		console.log("searchCategory",err); 
		return_result(null,null,pagination_details); 
	}	

};

methods.searchCompanyForCategory = function(category_id,arrParams, return_result)
{
		
	try
	{
		var arrAccounts = [];
		var arrAccIds = [];
		var newObj = {};
		var object_account = {};

		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };

		var query = `SELECT account_id FROM jaze_user_basic_details 
			WHERE account_id IN (SELECT account_id FROM jaze_user_basic_details 
			WHERE meta_key = 'category_id' AND meta_value =?) GROUP BY account_id`

		DB.query(query, [category_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var counter = data.length;			
				var jsonItemsArr = Object.values(data);
	
				promiseForeach.each(jsonItemsArr,[function (jsonItems) {

					return new Promise(function (resolve, reject) {

						arrAccounts.push(jsonItems.account_id);
						counter -= 1;

						if (counter === 0)
						{ 		
							resolve(arrAccounts);
						}
					})
				}],
				function (result, current) { 
					if (counter === 0)
					{ 
	
						var filteredArray = removeDuplicates(result[0]);

						methods.processInnerResultCompanyForCategory(filteredArray,arrParams, function(retProcessed,pagination_details){
							return_result(retProcessed,pagination_details);
						});

					}
				});	
			

			}else{
				return_result(null,pagination_details);
			}
		});	

	}
	catch(err)
	{ 
		console.log("searchCompanyForCategory",err); 
		return_result(null,pagination_details); 
	}	

};

methods.processInnerResultCompanyForCategory  =  function(arrvalues,arrParams,return_result){
	try
	{
		var arrResults = [];

		var account_details = {};


		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };

		if(parseInt(arrParams.city_id)  !==0  || parseInt(arrParams.state_id)  !==0 || parseInt(arrParams.country_id) !==0 )
		{


		
			methods.checkIfInGeoCompanyForCategory(arrvalues,arrParams, function(retCoGeo){

				if(retCoGeo!=null  )
				{

	
					if(parseInt(arrParams.lat) !==0 && parseInt(arrParams.lon) !==0)
					{	
						var filteredArray = removeDuplicates(retCoGeo);
						methods.processwithLocCompanyForCategory(filteredArray,arrParams,  function(retAccountDetails,pagination_details){	
							return_result(retAccountDetails,pagination_details);

						});	
					}
					else
					{


						var filteredArray = removeDuplicates(retCoGeo);
						var counter = filteredArray.length;		

						var jsonItems = Object.values(filteredArray);

						promiseForeach.each(jsonItems,[function (jsonItems) {


							return new Promise(function (resolve, reject) {		
					
								var accParams = {account_id:jsonItems};
							
					  			methods.getCompanyProfileForCategory(accParams, function(retAccountDetails){	
									
									if(retAccountDetails!=null){

									
						  				account_details = {	
						  					company_id:retAccountDetails.account_id,
											company_name:retAccountDetails.name,
											company_image:retAccountDetails.logo,
											distance:''
						  				};

						  				counter -= 1;
										arrResults.push(account_details);
										if (counter === 0){ 
											resolve(arrResults);
										}
									
									}
			
								});

							})
						}],
						function (result, current) { 

				
							var newArray = [];

							if (counter === 0){ 

								var geo_total_result = result[0].length.toString();
								var total_page = 0;


								if(parseInt(result[0].length) > 50){
									newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
									total_page = Math.ceil((parseInt(result[0].length) - 50 ) / 50);
								}else{
									newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
									total_page = 0;
								}

								var next_page = 0;
								if(parseInt(total_page) !== 0){
									next_page  = parseInt(arrParams.page)+1;
								}

            				

				                 if(parseInt(total_page) >= parseInt(arrParams.page)){

									pagination_details  = {
										total_page   : total_page.toString(),
						                current_page : arrParams.page.toString(),
						                next_page    : next_page.toString(),
						                total_result : geo_total_result
						            };

								  	return_result(newArray.sort(sortingArrayObject('company_name')),pagination_details);
			
					            }else{

				            		return_result(null,pagination_details);

					            }			          
								
							}

						});	
					}

				}
				else
				{
			
					return_result(null,pagination_details);
				}

			});

		}
		else
		{

			if(parseInt(arrParams.lat) !==0 && parseInt(arrParams.lon) !==0)
			{
				var filteredArray = removeDuplicates(arrvalues);
				methods.processwithLocCompanyForCategory(filteredArray,arrParams,  function(retAccountDetails,pagination_details){	
					return_result(retAccountDetails,pagination_details);
				});	
			}
			else
			{

				var filteredArray = removeDuplicates(arrvalues);
				var counter = filteredArray.length;		
				var jsonItems = Object.values(filteredArray);

				promiseForeach.each(jsonItems,[function (jsonItems) {


					return new Promise(function (resolve, reject) {		
			
						var accParams = {account_id:jsonItems};
					
			  			methods.getCompanyProfileForCategory(accParams, function(retAccountDetails){	
							
							if(retAccountDetails!=null){

							
				  				account_details = {	
				  					company_id:retAccountDetails.account_id,
									company_name:retAccountDetails.name,
									company_image:retAccountDetails.logo,
									distance:''
				  				};

				  				
								arrResults.push(account_details);
		  		
								counter -= 1;
								if (counter === 0){ 
									resolve(arrResults);
								}
							
							}
	
						});

					})
				}],
				function (result, current) { 

					if (counter === 0){ 

						var geo_total_result = result[0].length.toString();
						var total_page = 0;

						if(parseInt(result[0].length) > 50){
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
							total_page = Math.ceil((parseInt(result[0].length) - 50 ) / 50);
						}else{
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
							total_page = 0;
						}

						var next_page = 0;
						if(parseInt(total_page) !== 0){
							next_page  = parseInt(arrParams.page)+1;
						}


		                 if(parseInt(total_page) >= parseInt(arrParams.page)){

							pagination_details  = {
								total_page   : total_page.toString(),
				                current_page : arrParams.page.toString(),
				                next_page    : next_page.toString(),
				                total_result : geo_total_result
				            };

						  	return_result(newArray.sort(sortingArrayObject('company_name')),pagination_details);
	
			            }else{

		            		return_result(null,pagination_details);

			            }	
				
					}

				});	
			}

		}
	}
	catch(err)
	{ console.log("processInnerResultCompanyForCategory",err); return_result(null);}
};

methods.checkIfInGeoCompanyForCategory =  function(arrAccounts,arrParams,return_result){
	try
	{

		var account_details = {};
		var arrResults = [];

		var filteredArray = removeDuplicates(arrAccounts);
			
		var counter = filteredArray.length;		
		var jsonItems = Object.values(filteredArray);

		promiseForeach.each(jsonItems,[function (jsonItems) {

			return new Promise(function (resolve, reject) {		
	
				var accParams = {account_id:jsonItems};

	  			methods.getCompanyProfileForCategory(accParams, function(retAccountDetails){	

	 				
					if(parseInt(arrParams.country_id) == parseInt(retAccountDetails.country_id) 
	  					|| parseInt(arrParams.state_id) == parseInt(retAccountDetails.state_id) 
	  					|| parseInt(arrParams.city_id) == parseInt(retAccountDetails.city_id)  )
					{
						arrResults.push(retAccountDetails.account_id);		

		  			
					}
				
					counter -= 1;

					if (counter === 0){ 
						resolve(arrResults);
					}
				
	  		
				});

			})
		}],
		function (result, current) {

			if (counter === 0){ 



				if(parseInt(result[0].length) > 0)
				{
					return_result(result[0]);
				}
				else
				{
					return_result(null);
				}
				
			}
		});

	}
	catch(err)
	{ console.log("checkIfInGeoCompanyForCategory",err); return_result(null);}
};

methods.processwithLocCompanyForCategory =  function(arrvalues,arrParams,return_result){
	try
	{	


		var total_page = 0;
		var next_page = 0;
		var total_result = 0;
		var newArray = [];

		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };

		var arrResults = [];
		var account_details = {};

		var query = `SELECT account_id, GROUP_CONCAT(meta_value ORDER BY meta_value ASC SEPARATOR ':') AS coordinates
				FROM jaze_user_basic_details 
			WHERE meta_key IN ('latitude','longitude') AND account_id IN(?) AND meta_value != ''  GROUP BY account_id`;

		var filteredArray = removeDuplicates(arrvalues);

		DB.query(query, [filteredArray], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var counter = data.length;		
				var jsonItems = Object.values(data);

				promiseForeach.each(jsonItems,[function (jsonItems) {

					return new Promise(function (resolve, reject) {		
			
						var accParams = {account_id:jsonItems.account_id};
								
			  			methods.getCompanyProfileForCategory(accParams, function(retAccountDetails){	
							
							if(retAccountDetails!=null){

								var geo = jsonItems.coordinates.split(":");

								var geo_distance = distance(parseFloat(arrParams.lat), parseFloat(arrParams.lon), parseFloat(geo[0]), parseFloat(geo[1]),"K");

				  				account_details = {	
				  					company_id:retAccountDetails.account_id,
									company_name:retAccountDetails.name,
									company_image:retAccountDetails.logo,
									distance:Math.ceil(geo_distance).toString()
				  				};

				  				
								arrResults.push(account_details);
		  
								counter -= 1;
								if (counter === 0){ 
									resolve(arrResults);
								}
							
							
							}
	
						});

					})
				}],
				function (result, current) { 

					if (counter === 0){ 


						var geo_total_result = result[0].length.toString();
						var total_page = 0;

						if(parseInt(result[0].length) > 50){
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
							total_page = Math.ceil((parseInt(result[0].length) - 50 ) / 50);
						}else{
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
							total_page = 0;
						}

						var next_page = 0;
						if(parseInt(total_page) !== 0){
							next_page  = parseInt(arrParams.page)+1;
						}


		                 if(parseInt(total_page) >= parseInt(arrParams.page)){

							pagination_details  = {
								total_page   : total_page.toString(),
				                current_page : arrParams.page.toString(),
				                next_page    : next_page.toString(),
				                total_result : geo_total_result
				            };

		                  	newArray.sort(function(a, b) {
							    return parseFloat(a.distance) - parseFloat(b.distance);
							});


						  	return_result(newArray,pagination_details);
	
			            }else{

		            		return_result(null,pagination_details);

			            }	
	
					}

				});	

			
			}
			else
			{ return_result(null,pagination_details); }

		});
	}
	catch(err)
	{ console.log("processwithLocCompanyForCategory",err); return_result(null,pagination_details);}
};

methods.getCompanyProfileForCategory =  function(arrParams,return_result){
	try
	{

		var newObj = {};

		var queryAccountType = `SELECT * FROM jaze_user_basic_details WHERE account_id =?`;

		DB.query(queryAccountType,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArrayCompany(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (val) {
						return new Promise(function (resolve, reject) {		
				
					 		var default_logo = "";
		  			
							 if(val.listing_images)
							{
								default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/listing_images/' + val.listing_images + '';
							}
							else if(val.company_logo)
							{
								default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/logo/' + val.company_logo + '';
							}
							else
							{
								default_logo = "";
							}
					
							newObj = {
								account_id	  : val.account_id.toString(),
								name		  : val.company_name,
								logo		  : default_logo,
								country_id    : val.country_id.toString(),
								city_id       : val.city_id.toString(),
								state_id      : val.state_id.toString()
							}	

							counter -= 1;
							if (counter === 0){ 
								resolve(newObj);
							}
		
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});

				});
			}
			else
			{
				return_result(null)
			}
		});

	}
	catch(err)
	{ console.log("getCompanyProfileForCategory",err); return_result(null);}
};

methods.getCategoryAttributeValueCategory = function(group_id,input_type,return_result){

	try
	{
		var arrValues = [];
		var obj_values = {};
		var controller_type;
		var objController = {};
		var value;
		var logo;

		if(input_type == 'color_picker'){
			obj_values = {
				id:'0',
				title:'all',
				color_code:''
			};
			arrValues.push(obj_values);
		}

		var singleObj = ['text','input_range','select_range'];
	 	var arrSameInputs = ['multiselect_image_button','multiselect_dropdown','image_button','dropdown'];

		if(arrSameInputs.includes(input_type) || input_type == 'color_picker' || input_type == 'radio_button' || input_type == 'text'  || input_type == 'button'){

			var quer = `SELECT * FROM jaze_category_attributes_groups_meta 
				WHERE group_id IN (SELECT group_id FROM jaze_category_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
				AND group_id IN (SELECT group_id FROM  jaze_category_attributes_groups_meta  WHERE meta_key = 'status' AND meta_value = '1' )`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					methods.convertMultiMetaArray(data, function(retConverted){	


						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (vala) {
							return new Promise(function (resolve, reject) {		
					
													
								if(arrSameInputs.includes(input_type)){

									if(vala.value != null){
										value = vala.value;
										logo  = '';
									}else{
										value = '';
										logo = vala.logo;
									}

									if(input_type == 'multiselect_image_button'){

										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value:'',
											logo:STATIC_IMG_URL+vala.value
										};

									}else if(input_type == 'image_button'){

										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value:STATIC_IMG_URL+value,
											logo:logo
										};

									}else{
										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value:value,
											logo:logo
										};
									}

								}else{

									if(input_type == 'color_picker'){

										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											color_code:vala.value
										};

									}else if(input_type == 'radio_button'){


										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value: vala.value,
								
										};

									}else if(input_type == 'button'){


										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value: vala.value,
								
										};

									}else if(input_type == 'text'){
										
										obj_values = {
											text_hint_label :vala.group_id.toString()
										};
									}

								}

								
							
								counter -= 1;
								resolve(obj_values);

					
							
							})
						}],
						function (result, current) {
							if (counter === 0){ 
								arrValues.push(result[0]);
							
							}
						});

					});

		  		}

			});

		
		}else if(input_type == 'input_range'){
	

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_jazestore_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var counter = data.length;		
					var jsonItems = Object.values(data);

					var min_field_label;
					var max_field_label;
					var suffix_field_label;
					var separator_field_label;

					promiseForeach.each(jsonItems,[function (vala) {
						return new Promise(function (resolve, reject) {		
							
							if(vala.key_name == 'separator label'){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == 'min label'){
								min_field_label = vala.key_value;
							}

							if(vala.key_name == 'max label'){
								max_field_label = vala.key_value;
							}

							if(vala.key_name == 'suffix'){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								min_field_label:min_field_label,
								max_field_label:max_field_label,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

							counter -= 1;
							if (counter === 0){ 
								resolve(obj_values);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							arrResults.push(result[0]);
						}
					});

		  		}

			});
					
		}else if(input_type == 'select_range'){
	

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_jazestore_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){


					var counter = data.length;		
					var jsonItems = Object.values(data);

					var min_field_label;
					var min_field_value;
					var max_field_label;
					var max_field_value;
					var interval_value;
					var suffix_field_label;
					var separator_field_label;

					promiseForeach.each(jsonItems,[function (vala) {
						return new Promise(function (resolve, reject) {		
								
							if(vala.key_name == 'separator label'){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == 'min label'){
								min_field_label = vala.key_value;
							}


							if(vala.key_name == 'min value'){
								min_field_value = vala.key_value;
							}

							if(vala.key_name == 'max label'){
								max_field_label = vala.key_value;
							}


							if(vala.key_name == 'max value'){
								max_field_value = vala.key_value;
							}

							if(vala.key_name == 'interval value'){
								interval_value = vala.key_value;
							}


							if(vala.key_name == 'suffix'){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								id:'0',
								min_field_label:min_field_label,
								min_field_value:min_field_value,
								max_field_label:max_field_label,
								max_field_value:max_field_value,
								internal_value:interval_value,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

							counter -= 1;
							if (counter === 0){ 
								resolve(obj_values);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							arrValues.push(result[0]);
						}
					});

		  		}

			});
					
		}

		setTimeout(function(){ 

			if(singleObj.includes(input_type)){
				return_result(obj_values);
			}else{
				return_result(arrValues.sort(sortingArrayObject('title')));
			}
			
		}, 500);

	}
	catch(err)
	{ console.log("getCategoryAttributeValueCategory",err); return_result([]); }	
};


// ----- search category end ----- //


//------------------------------------------------------------------- jazefood Search Module Start ---------------------------------------------------------------------------//

methods.searchJazefood = function(arrParams,return_result)
{
	try
	{
		if(arrParams !== null)
		{
			var location_query  = " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'category_id'  AND meta_value in (1838,1839,1331,1354,1355) ";
				location_query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country_id+"'  ";
				location_query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state_id+"'  ";
				location_query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city_id+"'))))  ";



			// company_name
			var query  = "(SELECT  account_id, '1' as type,meta_value as keyword FROM jaze_user_basic_details WHERE  account_type = 3 and meta_key IN ('company_name')";
				query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1') ";
				query += location_query;
				query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('company_name')  AND LOWER(meta_value) like '%"+arrParams.keyword.toLowerCase()+"%')  ";
				query += " group by account_id)  ";

				query += " union  ";	

				// keyword
				query += " (SELECT '0' as account_id, '2' as type,meta_value as keyword ";
				query += " FROM jaze_company_keywords WHERE  meta_key in ('keywords') ";
				query += " and group_id IN  (SELECT group_id FROM jaze_company_keywords WHERE meta_key = 'status'  AND meta_value = '1'  ";
				query += "  and group_id IN  (SELECT group_id FROM jaze_company_keywords WHERE meta_key = 'account_id'  AND meta_value in( ";
				query += " (SELECT account_id  FROM jaze_user_basic_details WHERE  account_type = 3 ";
				query += location_query;
				query += " ))) ";
				query += " and group_id in (SELECT group_id FROM jaze_company_keywords WHERE meta_key IN ('keywords') AND LOWER(meta_value) like '%"+arrParams.keyword.toLowerCase()+"%')))";
				//query = " )  ";
				

				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
						var counter = data.length;
						
						var array_rows = {
							keyword_result : [],
							comp_result : []
						};

						if(parseInt(counter) !== 0)
						{
							promiseForeach.each(data,[function (list) {
							
								return new Promise(function (resolve, reject) {
									
									methods.__searchJazefoodCompanyDetails(arrParams,list, function(ret_result){
										
										if(ret_result !== null)
										{
											if(parseInt(list.type) === 2)
											{ 
												var keyword_result = array_rows.keyword_result;

												if(keyword_result !== null && parseInt(keyword_result.length) > 0)
												{ array_rows.keyword_result = array_rows.keyword_result.concat(ret_result); }
												else
												{ array_rows.keyword_result = ret_result; }
											}
											else
											{ array_rows.comp_result.push(ret_result); }	
										}																												
										counter -= 1;
																																	
										resolve(array_rows);
									});
								});
							}],
							function (result, current) {
																	
								if (counter === 0)
								{ 
									var return_rows = {
										keyword_result : [],
										comp_result : []
									};

									if(result[0].keyword_result !== null && parseInt(result[0].keyword_result.length) > 0)
									{
										return_rows.keyword_result = sort_keyword((result[0].keyword_result).slice(0, 4),arrParams.keyword);
									}

									if(result[0].comp_result !== null && parseInt(result[0].comp_result.length) > 0)
									{
										return_rows.comp_result = sort_name((result[0].comp_result).slice(0, 50),arrParams.keyword);
									}

									return_result(return_rows);  }
							});
						}
						else
						{ return_result(null); }
					}
					else
					{ return_result(null); }
				});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log('searchJazefood',err);
		return_result('');
	}
}

methods.__searchJazefoodCompanyDetails = function(arrParams,list_result,return_result)
{
	try
	{
		if(arrParams !== null && list_result !== null)
		{
			if(parseInt(list_result.type) === 2)
			{
				var keyword = convert_array(list_result.keyword);

				var keyword_array = [];

				keyword.forEach(function (row) {

					if(row.includes(arrParams.keyword.toLowerCase()))
					{
						var obj = {
							keyword : row
						};

						keyword_array.push(obj) 
					}
				});

				return_result(keyword_array);
			}
			else
			{
				HelperGeneral.getAccountLogoDetails(list_result.account_id, function(company_logo){ 

					methods.___getJazefoodCategoryDetails(list_result.account_id, function(company_category){ 

						var newObj = {
							company_id	  	  : list_result.account_id.toString(),
							company_name	  : list_result.keyword.toString(),
							company_logo	  : company_logo.toString()
						};

						Object.assign(newObj, company_category);

						return_result(newObj);

					});
				});
			}
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log('__searchJazefoodCompanyDetails',err);
		return_result(null);
	}
}

methods.___getJazefoodCategoryDetails = function(account_id, return_result){

	var return_obj = {
			company_category  : '',
			online_status	  : "0",
			chat_status	  	  : "0",
			mail_status	  	  : "0",
			call_status	  	  : "0",
			feed_status	  	  : "0",
			diary_status	  : "0",
		};

	try
	{
		if(parseInt(account_id) !== 0)
		{
			//get category_name
			var query = "SELECT * FROM `jaze_user_basic_details` WHERE `account_id` = '" + account_id + "'  AND `meta_key` in ('category_id','is_merchant_type') ";

			DB.query(query, null, function(data, error){

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					var data_result = [];

					for (var i in data)
					{
						Object.assign(data_result, {[data[i].meta_key]:  data[i].meta_value});
					}

					var array_order =  { '1838' : 'grocery online','1839' : 'food online','1331' : 'restaurants','1354' : 'coffee shops','1355' : 'fast food'};

					if(parseInt(data_result.category_id) !== 0)
					{ return_obj.company_category = array_order[data_result.category_id]; }

					if(parseInt(data_result.is_merchant_type) === 2 || parseInt(data_result.is_merchant_type) === 3)
					{ return_obj.online_status = "1"; }

						methods.___getJazecomStatus(account_id, function(jazecom_status){ 

							if (typeof jazecom_status !== 'undefined' && jazecom_status !== null)
							{
								Object.assign(return_obj, {
									chat_status	  	  : jazecom_status.jaze_chat,
									mail_status	  	  : jazecom_status.jaze_mail,
									call_status	  	  : jazecom_status.jaze_call,
									feed_status	  	  : jazecom_status.jaze_feed,
									diary_status	  : jazecom_status.jaze_diary,
								});
							}

							return_result(return_obj);
						});
				}
				else
				{ return_result(return_obj); }
			});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{ console.log("___getJazefoodCategoryDetails",err); return_result(return_obj); }
};


methods.___getJazecomStatus= function(account_id,return_result)
{ 
	try
	{
		if(parseInt(account_id) !== 0)
		{
			var query = "SELECT * FROM jaze_user_jazecome_status WHERE  meta_key not in ('jaze_pay','account_id')";
				query += " and `group_id` IN (SELECT `group_id`  FROM jaze_user_jazecome_status WHERE `meta_key` = 'account_id' AND meta_value = '"+account_id+"' ) ";
					
				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
						HelperRestriction.convertMetaArray(data, function(statusRes){

							return_result(statusRes);
						});
					}
					else
					{ return_result(null); }
				});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("___getAccountJazecomStatus",err); return_result(null); }	
}
	/*
	 * split values push to array
	 */
	function convert_array(array_val) {

		var list = [];

		if(typeof array_val !== 'undefined' && array_val !== null && array_val !== "")
		{
			if(array_val.toString().indexOf(',') != -1 ){
				list = array_val.split(',');
			}
			else {
				list.push(array_val);
			}
		}
		
		return list;
	}

	function sort_keyword(array,keyword)
	{
		return array.filter(o => o.keyword.toLowerCase().includes(keyword.toLowerCase()))
					.sort((a, b) => a.keyword.toLowerCase().indexOf(keyword.toLowerCase()) - b.keyword.toLowerCase().indexOf(keyword.toLowerCase()));
	}

	function sort_name(array,keyword)
	{
	return array.filter(o => o.company_name.toLowerCase().includes(keyword.toLowerCase()))
					.sort((a, b) => a.company_name.toLowerCase().indexOf(keyword.toLowerCase()) - b.company_name.toLowerCase().indexOf(keyword.toLowerCase()));
	}
//------------------------------------------------------------------- jazefood Search Module End ---------------------------------------------------------------------------//



// ----------------------------------- concierge -------------------------------- //


methods.getConcierge = function(arrParams,return_result)
{ 

	var arrRows = [];
	var newObj = {};

	try
	{	

		var query = `SELECT * FROM jaze_category_list WHERE group_id IN (SELECT group_id FROM jaze_category_list WHERE meta_key = 'parent_id' AND meta_value ='5')`;

		DB.query(query, null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
								
							var logo = "";
							if(jsonItems.image_url){
								logo = STATIC_IMG_URL_CAT_NEW+jsonItems.image_url;
							}

							newObj = {

								id         : jsonItems.group_id.toString(),
								name       : jsonItems.cate_name,
								image_url  : logo,
								sort_order : jsonItems.sort_order

							}
							
							arrRows.push(newObj);
							counter -= 1;
							if (counter === 0){ 
								resolve(arrRows);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							return_result(result[0].sort(sortingArrayObject('sort_order')));
									
						}
					});

				});

			}
			else
			{
				return_result(null); 
			}
		
		});


	}
	catch(err)
	{ console.log("getConcierge",err); return_result(null); }	
}


// ----------------------------------- concierge -------------------------------- //


function distance(lat1, lon1, lat2, lon2, unit) {

	if ((lat1 == lat2) && (lon1 == lon2)) {
		return 0;
	}
	else 
	{
		var radlat1 = Math.PI * lat1/180;
		var radlat2 = Math.PI * lat2/180;
		var theta = lon1-lon2;
		var radtheta = Math.PI * theta/180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist);
		dist = dist * 180/Math.PI;
		dist = dist * 60 * 1.1515;
		if (unit.toString()=="K") { dist = dist * 1.609344 }
		if (unit.toString()=="N") { dist = dist * 0.8684 }
		return dist;
	}
}

methods.convertMultiMetaArrayCompany =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndexCompany(list,arrvalues[i].account_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndexCompany(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].account_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}

//Get Mobile Code
methods.__getMobileCode = function (account_list,return_result){
	try
	{
		if(account_list !== null)
		{
			if(parseInt(account_list.account_type) === 1 || parseInt(account_list.account_type) === 2)
			{
				var query = " SELECT `phonecode` FROM `country` WHERE `id` = '"+account_list.mobile_country_code_id+"'; ";
		
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						return_result("+"+data[0].phonecode);
					}
					else
					{ return_result(''); }
				});
			}
			else if(parseInt(account_list.account_type) === 3)
			{
				if(parseInt(account_list.company_phone_code) !== 0)
				{
					var query = " SELECT `phonecode` FROM `country` WHERE `id` = '"+account_list.company_phone_code+"'; ";
				
					DB.query(query, null, function(data, error){
						
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_result("+"+data[0].phonecode);
						}
						else
						{ return_result(''); }
					});
				}
				else
				{
					return_result('');
				}
			}
			else
			{
				return_result('');
			}
			
		}
		else
		{
			return_result('');
		}
	}
	catch(err)
	{ console.log("getMobileCode",err); return_result(''); }	
};


methods.__getCountryDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT country_name FROM country  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getStateDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT state_name FROM states  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].state_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getCityDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT city_name FROM cities  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].city_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getAreaDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT area_name FROM area  WHERE area_id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].area_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};


module.exports = methods;
