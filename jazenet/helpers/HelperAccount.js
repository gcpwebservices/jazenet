const methods = {};
const DB = require('../../helpers/db_base.js');
const HelperGeneral = require('../../helpers/HelperGeneral.js');
const HelperGeneralJazenet = require('../helpers/HelperGeneralJazenet.js');
const HelperRestriction = require('../helpers/HelperRestriction.js');
const promiseForeach = require('promise-foreach');
const fs = require('fs');
const fast_sort = require("fast-sort");

const dateFormat = require('dateformat');
const now = new Date();
const standardDT = dateFormat(now,'yyyy-mm-dd HH:MM:ss');
const moment = require("moment-timezone");
const express = require('express');
//const router = express.Router();

methods.checkAccountLogin = function(arrParams,return_result){

	try
	{
		var jazemail_id = arrParams.jazemail_id;

		if(!emailIsValid(arrParams.jazemail_id))
		{
			jazemail_id = jazemail_id+"@jazenet.com";
		}

		DB.query("select account_id,account_type from jaze_user_credential_details where jazemail_id=? and password=? and status = '1'", [jazemail_id,arrParams.password], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				return_result(data[0]);
			}else{
				return_result(null);
			}
		});

	}
	catch(err)
	{ 
		console.log("checkCredentialDetails",err); 
		return_result(null); 
	}	
};

methods.getMeAccountDetails = function(arrParams,return_result)
{ 

	var newObj = {};
	var essen_obj = {};
	var billing_shipping = {};
	var payment_info = [];

	var arrRows = [];

	try
	{
		HelperGeneralJazenet.getAccountDetails(arrParams, function(accRres){	

			if(accRres !=null)
			{
				methods.getJazecomMenus(arrParams, function(retJazeMenu){	
					
					methods.getBankDetails(arrParams, function(retBank){
						
						methods.getCreditCardDetails(arrParams, function(retCard){	

							methods.getShippingDetails(arrParams, function(retShip){	
								
								methods.__jobProfileStatus(arrParams, function(jobStatus){	

									var bank = [];
									var card = [];
									var ship = [];

									if(retBank!=null){
										bank = retBank;
									}


									if(retCard!=null){
										card = retCard;
									}

									
									if(retShip!=null){
										ship = retShip;
									}

									essen_obj = {

										account_id:arrParams.account_id.toString(),
										category:accRres.category,
										title:accRres.title,
										fname:accRres.fname,
										lname:accRres.lname,
										logo:accRres.logo,
										jazenet_id:accRres.jazenet_id,
										dob:accRres.dob,
										email:accRres.email,
										mobile:(accRres.mobile_code+" "+accRres.mobile_no).toString(),
										nationality_id  : accRres.nationality_id,
										nationality  : accRres.nationality,
										country_id   : accRres.country_id,
										country_name : accRres.country,
										state_id     : accRres.state_id,
										state_name   : accRres.state,
										city_id      : accRres.city_id,
										city_name    : accRres.city,
										area_id      : accRres.area_id,
										area_name    : accRres.area,
										currency_id  : accRres.currency_id,
										currency     :accRres.currency


									};

									newObj = {

										essential_info: essen_obj,
										jobs_status:{
											basic_job_profile_status:jobStatus.profile,
											curriculum_status:jobStatus.cv
										},
										communication_info:retJazeMenu,
										billing_shipping:ship,
										payment_info:{
											bank:bank,
											card:card
										}

									};

									return_result(newObj);

								});		
							});		

						});		
					});		

				});		
			}
			else
			{ return_result(null); }
		});			

	}
	catch(err)
	{ 
		console.log("getMeAccountDetails",err); 
		return_result(null); 
	}	
}

methods.checkAccountType = function(account_id,return_result){

	try
	{

		if(parseInt(account_id) !==0)
		{
			DB.query("select account_type from jaze_user_credential_details where account_id=? and status = '1'", [account_id], function(data, error){
			
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result(data[0].account_type);
				}else{
					return_result(null);
				}
			});
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ 
		console.log("checkCredentialDetails",err); 
		return_result(null); 
	}	
};


methods.__jobProfileStatus = function(arrParams,return_result){

	var obj_status = {
			profile:'0',
			cv:'0'
	};

	try{

		DB.query("SELECT * FROM jaze_job_basic_profile WHERE meta_key = 'account_id' AND meta_value =?",[arrParams.account_id], function(datap, error){

			if(typeof datap !== 'undefined' && datap !== null && parseInt(datap.length) > 0){
			

				obj_status.profile = '1';
			}

			DB.query("SELECT * FROM jaze_job_cv_basic_details WHERE meta_key = 'account_id' AND meta_value =?",[arrParams.account_id], function(datac, error){

				if(typeof datac !== 'undefined' && datac !== null && parseInt(datac.length) > 0){
					obj_status.cv = '1';
				}

				return_result(obj_status);

			});
	
		});
	}
	catch(err)
	{
		return_result(obj_status)
	}
};


//------------------------------------------------------------------- Essential Info Module Start ---------------------------------------------------------------------------//

methods.getEssentialDetails = function(arrParams,return_result)
{ 

	var essen_obj = {};

	try
	{
		HelperGeneralJazenet.getAccountDetails(arrParams, function(accRres){	

			essen_obj = {

				logo:accRres.logo,
				name:accRres.name,
				jazenet_id:accRres.jazenet_id,
				dob:accRres.dob,
				email:accRres.email,
				mobile:(accRres.mobile_code+" "+accRres.mobile_no).toString(),
				country_id:accRres.country_id,
				country:accRres.country,
				state:accRres.state,
				city:accRres.city,
				area:accRres.area,
				currency:accRres.currency

			};

			return_result(essen_obj);
		
		});			

	}
	catch(err)
	{ 
		console.log("getEssentialDetails",err); 
		return_result(null); 
	}	
}

methods.updateEssentials =  function(arrvalues,return_result){
	try
	{
		var kupal = 0;
		var	arrUpdate = {
			fname   : arrvalues.first_name,
			lname   : arrvalues.last_name,
			nationality_id  : arrvalues.nationality_id,
			country_id      : arrvalues.country_id,
			state_id        : arrvalues.state_id,
			city_id         : arrvalues.city_id,
			area_id         : arrvalues.area_id,
			currency_id     : arrvalues.currency_id,
			dob				:arrvalues.dob
		};

		var updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key =? AND account_id =?`;
		
		for (var key in arrUpdate) 
	 	{	
		     if(arrUpdate[key] !== null && arrUpdate[key] != ""){
				DB.query(updateQue, [arrUpdate[key],key,arrvalues.account_id], function(data, error){
			 		if(data !== null){
			 			kupal += parseInt(data.affectedRows);
		 				return_result(kupal);
			 	   	}else{
		 	   			return_result(kupal);
			 	   	}
				});
			}
		}


	}
	catch(err)
	{ console.log("updateEssentials",err); return_result(null);}
};

//------------------------------------------------------------------- Essential Info Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Profile Pic Module Start ---------------------------------------------------------------------------//

methods.updateProfilePic = function (arrParams,return_result){

	try
	{
		if( arrParams !== null )
		{
			var query = " SELECT `meta_value` FROM `jaze_user_basic_details` WHERE `meta_key` = 'profile_logo' and `account_id` = '"+arrParams.account_id+"' ";

				DB.query(query, null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					var update_query = "UPDATE jaze_user_basic_details SET meta_value = '"+arrParams.filename+"' WHERE meta_key ='profile_logo' AND account_id ='"+arrParams.account_id+"' ";

					DB.query(update_query, null, function(datas, error){

						if(error == null){

							const urlPath = G_STATIC_IMG_PATH+"individual_account/";

							fs.unlinkSync(urlPath+'members/'+data[0].meta_value);

							fs.unlinkSync(urlPath+'members_thumb/thumb_'+data[0].meta_value);

							return_result(1);
							
						 }else{
							 return_result(2);
						 }
					});
				}
			});
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{ 
		console.log("updateProfilePic",err); 
		return_result(0); 
	}	
};

//------------------------------------------------------------------- Profile Pic Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Jazecom Menus Module Start ---------------------------------------------------------------------------//

methods.getJazecomMenus = function(arrParams,return_result)
{ 
	try
	{
		var query = " SELECT *  FROM jaze_user_jazecome_status WHERE meta_key not in ('account_id') and  group_id in (SELECT group_id  FROM jaze_user_jazecome_status WHERE  meta_key = 'account_id' and meta_value = '"+arrParams.account_id+"') ";
		   
		DB.query(query,null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				HelperRestriction.convertMultiMetaArray(data, function(data_result){	
							
					if(data_result !== null)
					{
						var result = data_result[0];

						delete result.group_id;

						return_result(result);
					}
				});
			}
			else
			{ return_result(null); }
		});
	}
	catch(err)
	{ 
		console.log("getJazecomMenus1",err); 
		return_result(arrRows); 
	}	
}

//------------------------------------------------------------------- Jazecom Menus Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- billing & shipping address Module Start ---------------------------------------------------------------------------//

methods.getShippingDetails = function(arrParams,return_result)
{ 
	try
	{
		var query = " SELECT *  FROM jaze_user_billing_address WHERE group_id in (SELECT group_id  FROM jaze_user_billing_address WHERE  meta_key = 'account_id' and meta_value = '"+arrParams.account_id+"' and group_id in ";
		    query += " (SELECT group_id  FROM jaze_user_billing_address WHERE  meta_key = 'status' and meta_value = '1')) ";

		DB.query(query,null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				HelperRestriction.convertMultiMetaArray(data, function(data_result){	
							
					if(data_result !== null)
					{
						var counter = data_result.length;

						var array_rows = [];

						if(parseInt(counter) !== 0)
						{
							promiseForeach.each(data_result,[function (list) {
				
								return new Promise(function (resolve, reject) {

									var country_id = '';
									var state_id = '';
									var city_id = '';
									var area_id = '';

									methods.__getCountryDetails(list.country, function(country){	

										methods.__getStateDetails(list.state, function(state){	
			
											methods.__getCityDetails(list.city, function(city){	
			
												methods.__getAreaDetails(list.area, function(area){	
													
													methods.__getMobileCode(list.mobile_country_code, function(mobcode){	
			
														methods.__getMobileCode(list.landline_country_code, function(phocode){	
			
															if(list.landline_no!=""){
																landline = (phocode+" "+list.landline_no).toString();
															}	
															
															var location_type = "";

															if(parseInt(list.location_type) == 1){
																location_type = 'home';
															}else if(parseInt(list.location_type) == 2){
																location_type = 'office';
															}

															if(list.country){

																country_id = list.country;
															}

															if(list.state){
																state_id = list.state;
															}

															if(list.city){
																city_id = list.city;
															}

															if(list.area){
																area_id = list.area;
															}

															  var newObj = {
																  id:list.group_id.toString(),
																  first_name	 : list.first_name.toString(),
																  last_name		 : list.last_name.toString(),
																  country_id	 : country_id,
																  country_name   : country,
																  state_id		 : state_id,
																  state_name	 : state,
																  city_id		 : city_id,
																  city_name		 : city,
																  area_id		 : area_id,
																  area_name		 : area,
																  street_name	 :list.street_name,
																  street_no	 	 :list.street_no,
																  building_name  :list.building_name,
																  building_no	 :list.building_no,
																  landmark       :list.landmark,
																  location_type  :list.location_type,
																  mobile_code_id : list.mobile_country_code.toString(),
																  mobile_code    : mobcode.toString(),
																  mobile_no 	 : list.mobile_no.toString(),
																  landline_code_id : list.landline_country_code.toString(),
																  landline_code    : phocode,
																  landline_no 	   : list.landline_no.toString(),
																  shipping_note	   :list.shipping_note,
																  default		   :list.is_default 
																  
															  };
			
															  array_rows.push(newObj);
					
															  resolve(array_rows);

															counter -= 1;
			
														});
													});
												});
											});
										});
									});
								})
							}],
							function (result, current) {
												
								if (counter === 0)
								{  return_result(result[0]); }
							});
						}
						else
						{ return_result(null); }
					}
					else
					{ return_result(null); }

				});
			}
			else
			{
				return_result(null); 
			}
		});
	}
	catch(err)
	{ 
		console.log("getShippingDetails",err); 
		return_result(null); 
	}	
}

methods.addBillingAddress = function (arrParams,return_result){

	try
	{


		if(parseInt(arrParams.is_default) == 1 )
		{

			var query = `SELECT group_id FROM jaze_user_billing_address WHERE group_id IN 
				(SELECT group_id FROM jaze_user_billing_address WHERE meta_key = 'account_id' AND meta_value =?) GROUP BY group_id`;

				DB.query(query, [arrParams.account_id], function(data, error){
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					var	arrUpdate = {is_default   : 0};
					var updateQue = `update jaze_user_billing_address SET meta_value =? WHERE meta_key =? AND group_id =?`;
					for (let i in data) { 
				  		for (var key in arrUpdate) 
					 	{	
					 		DB.query(updateQue, [arrUpdate[key],key,data[i].group_id], function(data, error){});
					  	};
					}
				}
			
			});

		}

			HelperRestriction.getNewGroupId('jaze_user_billing_address', function(lastGrpId){

			var arrInsert = {
				account_id            : arrParams.account_id,
				first_name            : arrParams.first_name,
				last_name             : arrParams.last_name,
				mobile_country_code   : arrParams.mobile_country_code,
				mobile_no             : arrParams.mobile_no,
				landline_country_code : arrParams.landline_country_code,
				landline_no           : arrParams.landline_no,
				country            : arrParams.country,
				state              : arrParams.state,
				city               : arrParams.city,
				area               : arrParams.area,
				street_name           : arrParams.street_name,
				street_no             : arrParams.street_no,
				building_name         : arrParams.building_name,
				building_no           : arrParams.building_no,
				shipping_note         : arrParams.shipping_note,
				landmark              : arrParams.landmark,
				location_type         : arrParams.location_type,
				is_default            : arrParams.is_default,
				create_date           : standardDT,
				status                : 1
			};

	  		var querInsert = `insert into jaze_user_billing_address (group_id, meta_key, meta_value) VALUES (?,?,?)`;
	  		var arrRows = [];
		 	for (var key in arrInsert) 
		 	{
				DB.query(querInsert,[lastGrpId,key,arrInsert[key]], function(data, error){

		 		 	if(parseInt(data.affectedRows) > 0){
						arrRows.push(data.affectedRows);
			 	   	}

		 	 	});
		  	};

  			setTimeout(function(){ 		
				if(arrRows.length > 0){
					return_result(1);
				}else{
					return_result(0);
				}
  			}, 500);

		});


	}
	catch(err)
	{ 
		console.log("addBillingAddress",err); 
		return_result(0); 
	}	

};

methods.updateBillingAddress = function (arrParams,return_result){

	try
	{

		if(parseInt(arrParams.id) !== 0 )
		{

			if(parseInt(arrParams.is_default) == 1 )
			{

				var query = `SELECT group_id FROM jaze_user_billing_address WHERE group_id IN 
					(SELECT group_id FROM jaze_user_billing_address WHERE meta_key = 'account_id' AND meta_value =?) GROUP BY group_id`;

					DB.query(query, [arrParams.account_id], function(data, error){
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						var	arrUpdate = {is_default   : 0};
						var updateQue = `update jaze_user_billing_address SET meta_value =? WHERE meta_key =? AND group_id =? and group_id !=?`;
						for (let i in data) { 
					  		for (var key in arrUpdate) 
						 	{	
						 		DB.query(updateQue, [arrUpdate[key],key,data[i].group_id,arrParams.id], function(data, error){});
						  	};
						}
					}
				
				});


			}

			var query = `SELECT group_id FROM jaze_user_billing_address WHERE group_id =? LIMIT 1`;
			DB.query(query, [arrParams.id], function(data, error){
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
						
					var arrUpdate = {
						account_id            : arrParams.account_id,
						first_name            : arrParams.first_name,
						last_name             : arrParams.last_name,
						mobile_country_code   : arrParams.mobile_country_code,
						mobile_no             : arrParams.mobile_no,
						landline_country_code : arrParams.landline_country_code,
						landline_no           : arrParams.landline_no,
						country            : arrParams.country,
						state              : arrParams.state,
						city               : arrParams.city,
						area               : arrParams.area,
						street_name           : arrParams.street_name,
						street_no             : arrParams.street_no,
						building_name         : arrParams.building_name,
						building_no           : arrParams.building_no,
						shipping_note         : arrParams.shipping_note,
						landmark              : arrParams.landmark,
						location_type         : arrParams.location_type,
						is_default            : arrParams.is_default,
						create_date           : standardDT,
						status                : 1
					};

					var updateQue = `update jaze_user_billing_address SET meta_value =? WHERE meta_key =? AND group_id =?`;
					for (let i in data) { 
				  		for (var key in arrUpdate) 
					 	{	
					 		DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(data, error){
					 			if(data!=null){
					 				return_result(1);
					 			}
					 		});
					  	};
					}
				}
				else
				{
					return_result(2);
				}
			
			});

		}
		else
		{
			return_result(0);
		}


	}
	catch(err)
	{ 
		console.log("addBillingAddress",err); 
		return_result(0); 
	}	

};

methods.delBillingAddress = function(group_ids,arrParams, return_result){


	try
	{

		if(group_ids.length > 0 )
		{

			var query = `SELECT * FROM jaze_user_billing_address WHERE group_id IN(?)`;
			DB.query(query,[group_ids], function(qdata, error){

				if(typeof qdata !== 'undefined' && qdata !== null && parseInt(qdata.length) > 0)
				{	

					var queDel = `DELETE FROM jaze_user_billing_address WHERE group_id IN(?)`;
					DB.query(queDel,[group_ids], function(rdata, error){

						if(rdata == null){
				 	   		return_result(0);
				 	   	}else{
				 	   		

		 	   				var query = `SELECT * FROM jaze_user_billing_address WHERE 
		 	   				group_id IN (SELECT group_id FROM jaze_user_billing_address WHERE meta_key = 'account_id' AND meta_value ='41623') ORDER BY group_id DESC LIMIT 1`;

								DB.query(query, [arrParams.account_id], function(data, error){
						
								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
								{	
									var	arrUpdate = {is_default   : 1};
									var updateQue = `update jaze_user_billing_address SET meta_value =? WHERE meta_key =? AND group_id =?`;
							
							  		for (var key in arrUpdate) 
								 	{	
								 		DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(udata, error){});
								  	};
									
								}
							
							});
							return_result(1);
				 	   	}

					});
				}
				else
				{
					return_result(2);
				}

			});
		}
		else
		{
			return_result(0);
		}

	}
	catch(err)
	{ 
		console.log("delBillingAddress",err); 
		return_result(0); 
	}	
};

//------------------------------------------------------------------- billing & shipping address Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Bank Module Start ---------------------------------------------------------------------------//

methods.getBankDetails = function (arrParams,return_result){

	try
	{
		var query = " SELECT *  FROM jaze_user_bank_details WHERE group_id in (SELECT group_id  FROM jaze_user_bank_details WHERE  meta_key = 'account_id' and meta_value = '"+arrParams.account_id+"' and group_id in ";
		    query += " (SELECT group_id  FROM jaze_user_bank_details WHERE  meta_key = 'status' and meta_value = '1')) ";

			DB.query(query,null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
					HelperRestriction.convertMultiMetaArray(data, function(data_result){	
								
						if(data_result !== null)
						{
							var counter = data_result.length;
							
							var array_rows = [];
	
							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data_result,[function (list) {
					
									return new Promise(function (resolve, reject) {
										
										if(list.account_no !== "" && list.account_no !== null)
										{
											var newObj = {
												id			 :list.group_id.toString(),
												is_default	 	: list.is_default.toString(),
												holder_name		: list.holder_name.toString(),
												account_no	 	: list.account_no.toString(),
												bank_name	 	:list.bank_name.toString(),
												branch_name	 	:list.branch_name.toString(),
												iban_no  		:list.iban_no.toString(),
												swift_code	 	:list.swift_code.toString()											
											};
											
											array_rows.push(newObj);

											resolve(array_rows);
										}

									     counter -= 1;
									})
								}],
								function (result, current) {
													
									if (counter === 0)
									{  return_result(result[0]); }
								});
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }
	
					});
				}
				else
				{
					return_result(null); 
				}
			});
	}
	catch(err)
	{ 
		console.log("getBankDetails",err); 
		return_result(null); 
	}	

};

methods.addBankDetails = function (arrParams,return_result){

	try
	{

		if(parseInt(arrParams.is_default) == 1 )
		{

			var query = `SELECT group_id FROM jaze_user_bank_details WHERE group_id IN 
				(SELECT group_id FROM jaze_user_bank_details WHERE meta_key = 'account_id' AND meta_value =?) GROUP BY group_id`;

				DB.query(query, [arrParams.account_id], function(data, error){
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					var	arrUpdate = {is_default   : 0};
					var updateQue = `update jaze_user_bank_details SET meta_value =? WHERE meta_key =? AND group_id =?`;
					for (let i in data) { 
				  		for (var key in arrUpdate) 
					 	{	
					 		DB.query(updateQue, [arrUpdate[key],key,data[i].group_id], function(data, error){});
					  	};
					}
				}
			
			});

		}

			HelperRestriction.getNewGroupId('jaze_user_bank_details', function(lastGrpId){

			var arrInsert = {
				account_id            : arrParams.account_id,
				holder_name           : arrParams.holder_name,
				account_no            : arrParams.account_no,
				bank_name             : arrParams.bank_name,
				branch_name           : arrParams.branch_name,
				iban_no               : arrParams.iban_no,
				swift_code            : arrParams.swift_code,
				is_default            : arrParams.is_default,
				create_date           : standardDT,
				status                : 1
			};
		
	  		var querInsert = `insert into jaze_user_bank_details (group_id, meta_key, meta_value) VALUES (?,?,?)`;

		 	for (var key in arrInsert) 
		 	{
				DB.query(querInsert,[lastGrpId,key,arrInsert[key]], function(data, error){

		 		 	if(parseInt(data.affectedRows) > 0){
						return_result(data.affectedRows)
			 	   	}else{
		   				return_result(0);
			 	   	}
		 	 	});
		  	};

		});


	}
	catch(err)
	{ 
		console.log("addBank",err); 
		return_result(0); 
	}	

};

methods.updateBankDetails = function (arrParams,return_result){

	try
	{

		if(parseInt(arrParams.id) !== 0 )
		{

			if(parseInt(arrParams.is_default) == 1 )
			{

				var query = `SELECT group_id FROM jaze_user_bank_details WHERE group_id IN 
					(SELECT group_id FROM jaze_user_bank_details WHERE meta_key = 'account_id' AND meta_value =?) GROUP BY group_id`;

					DB.query(query, [arrParams.account_id], function(data, error){
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						var	arrUpdate = {is_default   : 0};
						var updateQue = `update jaze_user_bank_details SET meta_value =? WHERE meta_key =? AND group_id =? and group_id !=?`;
						for (let i in data) { 
					  		for (var key in arrUpdate) 
						 	{	
						 		DB.query(updateQue, [arrUpdate[key],key,data[i].group_id,arrParams.id], function(data, error){});
						  	};
						}
					}
				
				});


			}

			var query = `SELECT group_id FROM jaze_user_bank_details WHERE group_id =? LIMIT 1`;
			DB.query(query, [arrParams.id], function(data, error){
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
						
					var arrUpdate = {
						account_id            : arrParams.account_id,
						holder_name           : arrParams.holder_name,
						account_no            : arrParams.account_no,
						bank_name             : arrParams.bank_name,
						branch_name           : arrParams.branch_name,
						iban_no               : arrParams.iban_no,
						swift_code            : arrParams.swift_code,
						is_default            : arrParams.is_default,
						create_date           : standardDT,
						status                : 1
					};

					var updateQue = `update jaze_user_bank_details SET meta_value =? WHERE meta_key =? AND group_id =?`;
					for (let i in data) { 
				  		for (var key in arrUpdate) 
					 	{	
					 		DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(data, error){
					 			if(data!=null){
					 				return_result(1);
					 			}
					 		});
					  	};
					}
				}
				else
				{
					return_result(2);
				}
			
			});

		}
		else
		{
			return_result(0);
		}


	}
	catch(err)
	{ 
		console.log("updateBankDetails",err); 
		return_result(0); 
	}	

};

methods.delBankDetails = function(group_ids,arrParams, return_result){


	try
	{

		if(group_ids.length > 0 )
		{

			var query = `SELECT * FROM jaze_user_bank_details WHERE group_id IN(?)`;
			DB.query(query,[group_ids], function(qdata, error){

				if(typeof qdata !== 'undefined' && qdata !== null && parseInt(qdata.length) > 0)
				{	

					var queDel = `DELETE FROM jaze_user_bank_details WHERE group_id IN(?)`;
					DB.query(queDel,[group_ids], function(rdata, error){

						if(rdata == null){
				 	   		return_result(0);
				 	   	}else{
				 	   		

		 	   				var query = `SELECT * FROM jaze_user_bank_details WHERE 
		 	   				group_id IN (SELECT group_id FROM jaze_user_bank_details WHERE meta_key = 'account_id' AND meta_value ='41623') ORDER BY group_id DESC LIMIT 1`;

								DB.query(query, [arrParams.account_id], function(data, error){
						
								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
								{	
									var	arrUpdate = {is_default   : 1};
									var updateQue = `update jaze_user_bank_details SET meta_value =? WHERE meta_key =? AND group_id =?`;
							
							  		for (var key in arrUpdate) 
								 	{	
								 		DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(udata, error){});
								  	};
									
								}
							
							});
							return_result(1);
				 	   	}

					});
				}
				else
				{
					return_result(2);
				}

			});
		}
		else
		{
			return_result(0);
		}

	}
	catch(err)
	{ 
		console.log("delBankDetails",err); 
		return_result(0); 
	}	
};

//------------------------------------------------------------------- Bank Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Card Module Start ---------------------------------------------------------------------------//

methods.getCreditCardDetails = function (arrParams,return_result){

	try
	{
		var query = " SELECT *  FROM jaze_user_credit_card WHERE group_id in (SELECT group_id  FROM jaze_user_credit_card WHERE  meta_key = 'account_id' and meta_value = '"+arrParams.account_id+"' and group_id in ";
		    query += " (SELECT group_id  FROM jaze_user_credit_card WHERE  meta_key = 'status' and meta_value = '1')) ";

			DB.query(query,null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
					HelperRestriction.convertMultiMetaArray(data, function(data_result){	
								
						if(data_result !== null)
						{
							var counter = data_result.length;
	
							var array_rows = [];
	
							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data_result,[function (list) {
					
									return new Promise(function (resolve, reject) {
	
										var newObj = {
											id			 :list.group_id.toString(),
											is_default	 	: list.is_default.toString(),
											card_name		: list.card_name.toString(),
											bank_name	 	: list.bank_name.toString(),
											card_no	 		:list.card_no.toString(),
											exp_month	 	:list.exp_month.toString(),
											exp_year  		:list.exp_year.toString(),
											csv	 			:""											
										};

										array_rows.push(newObj);

										resolve(array_rows);

									  counter -= 1;
									})
								}],
								function (result, current) {
													
									if (counter === 0)
									{  return_result(result[0]); }
								});
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }
	
					});
				}
				else
				{
					return_result(null); 
				}
			});
	}
	catch(err)
	{ 
		console.log("getCreditCardDetails",err); 
		return_result(null); 
	}	
};

methods.addCardDetails = function (arrParams,return_result){

	try
	{

		if(parseInt(arrParams.is_default) == 1 )
		{

			var query = `SELECT group_id FROM jaze_user_credit_card WHERE group_id IN 
				(SELECT group_id FROM jaze_user_credit_card WHERE meta_key = 'account_id' AND meta_value =?) GROUP BY group_id`;

				DB.query(query, [arrParams.account_id], function(data, error){
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					var	arrUpdate = {is_default   : 0};
					var updateQue = `update jaze_user_credit_card SET meta_value =? WHERE meta_key =? AND group_id =?`;
					for (let i in data) { 
				  		for (var key in arrUpdate) 
					 	{	
					 		DB.query(updateQue, [arrUpdate[key],key,data[i].group_id], function(data, error){});
					  	};
					}
				}
			
			});

		}

			HelperRestriction.getNewGroupId('jaze_user_credit_card', function(lastGrpId){


			var arrInsert = {
				account_id     : arrParams.account_id,
				card_name      : arrParams.card_name,
				bank_name      : arrParams.bank_name,
				bank_name      : arrParams.bank_name,
				card_no        : arrParams.card_no,
				exp_month      : arrParams.exp_month,
				exp_year       : arrParams.exp_year,
				csv            : arrParams.csv,
				is_default     : arrParams.is_default,
				create_date    : standardDT,
				status         : 1
			};
		
	  		var querInsert = `insert into jaze_user_credit_card (group_id, meta_key, meta_value) VALUES (?,?,?)`;

		 	for (var key in arrInsert) 
		 	{
				DB.query(querInsert,[lastGrpId,key,arrInsert[key]], function(data, error){

		 		 	if(parseInt(data.affectedRows) > 0){
						return_result(data.affectedRows)
			 	   	}else{
		   				return_result(0);
			 	   	}
		 	 	});
		  	};

		});


	}
	catch(err)
	{ 
		console.log("addCardDetails",err); 
		return_result(0); 
	}	

};

methods.updateCardDetails = function (arrParams,return_result){

	try
	{

		if(parseInt(arrParams.id) !== 0 )
		{

			if(parseInt(arrParams.is_default) == 1 )
			{

				var query = `SELECT group_id FROM jaze_user_credit_card WHERE group_id IN 
					(SELECT group_id FROM jaze_user_credit_card WHERE meta_key = 'account_id' AND meta_value =?) GROUP BY group_id`;

					DB.query(query, [arrParams.account_id], function(data, error){
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						var	arrUpdate = {is_default   : 0};
						var updateQue = `update jaze_user_credit_card SET meta_value =? WHERE meta_key =? AND group_id =? and group_id !=?`;
						for (let i in data) { 
					  		for (var key in arrUpdate) 
						 	{	
						 		DB.query(updateQue, [arrUpdate[key],key,data[i].group_id,arrParams.id], function(data, error){});
						  	};
						}
					}
				
				});


			}

			var query = `SELECT group_id FROM jaze_user_credit_card WHERE group_id =? LIMIT 1`;
			DB.query(query, [arrParams.id], function(data, error){
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
						
					var arrUpdate = {
						account_id     : arrParams.account_id,
						card_name      : arrParams.card_name,
						bank_name      : arrParams.bank_name,
						bank_name      : arrParams.bank_name,
						card_no        : arrParams.card_no,
						exp_month      : arrParams.exp_month,
						exp_year       : arrParams.exp_year,
						csv            : arrParams.csv,
						is_default     : arrParams.is_default,
						create_date    : standardDT,
						status         : 1
					};

					var updateQue = `update jaze_user_credit_card SET meta_value =? WHERE meta_key =? AND group_id =?`;
					for (let i in data) { 
				  		for (var key in arrUpdate) 
					 	{	
					 		DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(data, error){
					 			if(data!=null){
					 				return_result(1);
					 			}
					 		});
					  	};
					}
				}
				else
				{
					return_result(2);
				}
			
			});

		}
		else
		{
			return_result(0);
		}


	}
	catch(err)
	{ 
		console.log("updateCardDetails",err); 
		return_result(0); 
	}	

};

methods.delCardDetails = function(group_ids,arrParams, return_result){


	try
	{

		if(group_ids.length > 0 )
		{

			var query = `SELECT * FROM jaze_user_credit_card WHERE group_id IN(?)`;
			DB.query(query,[group_ids], function(qdata, error){

				if(typeof qdata !== 'undefined' && qdata !== null && parseInt(qdata.length) > 0)
				{	

					var queDel = `DELETE FROM jaze_user_credit_card WHERE group_id IN(?)`;
					DB.query(queDel,[group_ids], function(rdata, error){

						if(rdata == null){
				 	   		return_result(0);
				 	   	}else{
				 	   		

		 	   				var query = `SELECT * FROM jaze_user_credit_card WHERE 
		 	   				group_id IN (SELECT group_id FROM jaze_user_credit_card WHERE meta_key = 'account_id' AND meta_value ='41623') ORDER BY group_id DESC LIMIT 1`;

								DB.query(query, [arrParams.account_id], function(data, error){
						
								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
								{	
									var	arrUpdate = {is_default   : 1};
									var updateQue = `update jaze_user_credit_card SET meta_value =? WHERE meta_key =? AND group_id =?`;
							
							  		for (var key in arrUpdate) 
								 	{	
								 		DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(udata, error){});
								  	};
									
								}
							
							});
							return_result(1);
				 	   	}

					});
				}
				else
				{
					return_result(2);
				}

			});
		}
		else
		{
			return_result(0);
		}

	}
	catch(err)
	{ 
		console.log("delCardDetails",err); 
		return_result(0); 
	}	
};

//------------------------------------------------------------------- Card Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Link Account Module Start ---------------------------------------------------------------------------//


methods.getMasterChildLinkAccountList = function(arrParams,return_result)
{
	if(arrParams !== null)
	{
		var obj = {};

		//get master account details

		var reqParams = {		
			account_id 		  : arrParams.master_account_id,
			active_account_id : arrParams.account_id,
			active_account_type : arrParams.account_type,
			group_id : "0",
			master_status : "1"
		};

		methods.___getAccountList(reqParams, function(masterList){

			obj = { master_account_details : masterList };

			methods.__getMasterChildAccountList(arrParams, function(childList){
				
				var childaccount = arrParams.master_account_id;

				if(childList !== null)
				{
					childaccount = arrParams.master_account_id+","+childList.account_id;

					Object.assign(obj, {child_account_details: childList.account_list});
				}
				else
				{
					Object.assign(obj, {child_account_details: []});
				}

				methods.__getMasterLinkAccountList(arrParams,childaccount, function(LinkList){
					
					if(LinkList !== null)
					{
						Object.assign(obj, {link_account_details: LinkList});
					}
					else
					{
						Object.assign(obj, {link_account_details: []});
					}

					return_result(obj);

				});
			});
		});
	}
	else
	{ return_result(null); }
}

methods.saveLinkAccountDetails = function (arrParams,return_result){

	try
	{
		// checking credential details
		var query = " SELECT account_id,account_type FROM jaze_user_credential_details WHERE jazemail_id = '" +arrParams.jazemail_id.toLowerCase()+ "' and password = '" +arrParams.password+ "' and status = '1' ";

		DB.query(query, null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				// all ready account exists
				methods.__checkLinkAccountDetails(arrParams.master_account_id,data[0].account_id, function(checkAccount){
					
					if(parseInt(checkAccount) === 1)
					{
						//insert tb jaze_app_device_details
						var reqParams = {		
							master_account_id : arrParams.master_account_id,
							account_id 		  : data[0].account_id,
							account_type 	  : data[0].account_type,
							app_id			  : arrParams.app_id
						};

						methods.__saveDeviceAccountDetails(reqParams, function(returnDevice){
							
							//insert tb jaze_user_link_account_details	
							methods.__saveLinkAccountDetails(reqParams, function(checkAccount){

								//insert tb jaze_jazecom_details	
								methods.__saveJazecomLinkAccountDetails(reqParams, function(checkAccount){

									return_result(1);
	
								});

							});
						});
					}
					else
					{ return_result(2); }
				});
			}
			else
			{ return_result(3); }
		});
	}
	catch(err)
	{ 
		console.log("saveLinkAccountDetails",err); 
		return_result(0); 
	}	

};

methods.removeLinkAccountDetails = function (arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{
			methods.__removeDeviceAccountDetails(arrParams, function(returnDevice){

				if(parseInt(returnDevice) === 1)
				{
					//delete tb jaze_user_link_account_details	
					methods.__removeLinkAccountDetails(arrParams, function(returnlink){

						if(parseInt(returnlink) === 1)
						{
							//insert tb jaze_jazecom_details
							methods.__removeJazecomLinkAccountDetails(arrParams, function(returnjazecom){

								if(parseInt(returnjazecom) === 1)
								{
									return_result(1);
								}
								else
								{ return_result(0);  }
							});
						}
						else
						{ return_result(0);  }

					});
				}
				else
				{ return_result(0);  }
			});
		}
		else
		{ return_result(0);  }

	}
	catch(err)
	{ 
		console.log("removeLinkAccountDetails",err); 
		return_result(0); 
	}	

};


methods.__getMasterChildAccountList = function(arrParams,return_result)
{
	try
		{
			if(parseInt(arrParams.master_account_id) !== 0)
			{
				var query = "SELECT `prof_id`,`company_id`,`work_id`  FROM `jaze_user_account_details` WHERE `status` = '1' and `user_id` = '" + arrParams.master_account_id + "' ";
				
				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var reqProf = {};

							if(parseInt(data[0].prof_id) !== 0)
							{
								reqProf = {		
									account_id 		  : data[0].prof_id,
									active_account_id : arrParams.account_id,
									active_account_type : arrParams.account_type,
									group_id : "0",
									master_status : "0"
								};
							}
							else
							{
								reqProf = null;
							}

						methods.___getAccountList(reqProf, function(profAccRres){
		
							var reqComp = {};

							if(parseInt(data[0].company_id) !== 0)
							{
								reqComp = {		
									account_id 		    : data[0].company_id,
									active_account_id   : arrParams.account_id,
									active_account_type : arrParams.account_type,
									group_id : "0",
									master_status : "0"
								};
							}
							else
							{
								reqComp = null;
							}
															
							methods.___getAccountList(reqComp, function(companyAccRres){	
																		
								var reqWork = {};

								if(parseInt(data[0].work_id) !== 0)
								{
									reqWork = {		

										account_id 		    : data[0].work_id,
										active_account_id   : arrParams.account_id,
										active_account_type : arrParams.account_type,
										group_id : "0",
										master_status : "0"
									};
								}
								else
								{
									reqWork = null;
								}
																														
								methods.___getWorkAccountDetails(reqWork, function(workAccRres){

									var account_id = "";

									var account_list = [];

									if(profAccRres != null)
									{
										account_id = profAccRres.account_id;

										account_list.push(profAccRres);
									}

									if(workAccRres != null)
									{
										account_id = (account_id  == "") ? workAccRres.account_id: account_id+","+workAccRres.account_id;

										account_list.push(workAccRres);
									}

									if(companyAccRres != null)
									{
										if(workAccRres != null)
										{
											if(parseInt(companyAccRres.account_id) !== parseInt(workAccRres.company_id))
											{
												account_list.push(companyAccRres);
											}
										}
										else
										{
											account_list.push(companyAccRres);
										}

										account_id = (account_id  == "") ? companyAccRres.account_id: account_id+","+companyAccRres.account_id;
									}
									
																		
									if(account_list != null)
									{
										var obj  = {
											account_id : account_id,
											account_list : fast_sort(account_list).asc('account_type')
										};

										return_result(obj);
									}
									else
									{ return_result(null); }

								});
							});
						});
					}
					else
					{ return_result(null); }
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("getMasterChildAccounts",err); return_result(null); }	
}

methods.__getMasterLinkAccountList = function(arrParams,child_account_id,return_result)
{
	try
		{
			if(parseInt(arrParams.master_account_id) !== 0)
			{
				
				var query = " SELECT * FROM jaze_user_link_account_details WHERE `group_id` in (SELECT group_id FROM jaze_user_link_account_details WHERE  `meta_key` = 'account_id' and `meta_value` = '" +arrParams.master_account_id+ "' and  `group_id` in (SELECT group_id FROM jaze_user_link_account_details WHERE  `meta_key` = 'status' and `meta_value` = '1' ";
					query += " and  `group_id` in (SELECT group_id FROM jaze_user_link_account_details WHERE  `meta_key` = 'link_account_id' and `meta_value` not in ("+child_account_id+"))  ";
					query += " ))";
				
				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){	
							
							if(data_result !== null)
							{
								var counter = data_result.length;

								var array_rows = [];

								if(parseInt(counter) !== 0)
								{
									promiseForeach.each(data_result,[function (list) {
						
										return new Promise(function (resolve, reject) {

											var reqlink = {		
												account_id 		  : list.link_account_id,
												active_account_id : arrParams.account_id,
												group_id : list.group_id,
												master_status : "0"
											};

											methods.___getAccountList(reqlink, function(masterList){

												if(masterList !== null)
												{
													array_rows.push(masterList);
							
													resolve(array_rows);
												}
												
												counter -= 1;

											});
										})
									}],
									function (result, current) {
														
										if (counter === 0)
										{  return_result(result[0]); }
									});
								}
								else
								{ return_result(null); }
							}
							else
							{ return_result(null); }

						});
					}
					else
					{ return_result(null); }
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("getMasterChildAccounts",err); return_result(null); }	
}

methods.___getAccountList = function(reqParams,return_result)
{
	try
		{
			if(reqParams !== null)
			{				
					HelperGeneral.getAccountDetails(reqParams, function(accRres){	

						var active_status = "0";

						var flag = "0";

						if(parseInt(reqParams.active_account_id) === parseInt(accRres.account_id) && parseInt(reqParams.active_account_type) === parseInt(accRres.account_type))
						{ active_status = "1"; }

						if(parseInt(reqParams.master_status) === 1)
						{ flag = "1"; }
						else if(parseInt(reqParams.group_id) === 0)
						{ flag = "2"; }
						else if(parseInt(reqParams.group_id) !== 0)
						{ flag = "3"; }

						var newObj = {
							account_id	  : accRres.account_id,
							account_type  : accRres.account_type,
							name		  : accRres.name,
							logo		  : accRres.logo,
							category_name : accRres.category,
							title		  : accRres.title,
							group_id	  : reqParams.group_id.toString(),
							active_status : active_status,
							master_status : reqParams.master_status,
							flag 		  : flag
						};

						return_result(newObj);

					});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("getMasterChildAccounts",err); return_result(null); }	
}

methods.__checkLinkAccountDetails = function (master_account_id,link_account_id,return_result){

	try
	{
		if(parseInt(master_account_id) !== 0 && parseInt(link_account_id) !== 0)
		{
			var query = " SELECT group_concat(meta_value) as id  FROM jaze_user_link_account_details WHERE  meta_key = 'link_account_id' and ";
				query += "group_id in (SELECT group_id  FROM jaze_user_link_account_details WHERE  meta_key = 'account_id' and meta_value = '"+master_account_id+"') ";

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						if(data[0].id.indexOf(link_account_id)!= -1)
						{ return_result(0); }
						else
						{ return_result(1); }
					}
					else
					{  return_result(0); }
				});
		}
		else
		{  return_result(0); }
	}
	catch(err)
	{ 
		console.log("__checkLinkAccountDetails",err); 
		return_result(0); 
	}	

}

methods.__saveDeviceAccountDetails  = function(arrParams,return_result){

	try
		{
			if(arrParams !== null)
			{
				//checking already exists
				var query = "SELECT * FROM jaze_app_device_details WHERE status = '1' AND  app_type = '1' AND account_id = '"+arrParams.account_id+"' AND  parent_id = '"+arrParams.app_id+"' ";
				
				DB.query(query, null, function(data, error){
					
					if(data === undefined || data.length == 0){
					
						//get master app details
						var query = "SELECT * FROM jaze_app_device_details WHERE status = '1' AND  app_type = '1' AND parent_id = '0' AND  account_id = '"+arrParams.master_account_id+"' AND id = '"+arrParams.app_id+"' ";
					
						DB.query(query, null, function(data, error){

							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
		
								const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

								var query = " INSERT INTO jaze_app_device_details SET ";
								query += " account_id = '"+arrParams.account_id+"', ";
								query += " account_type = '"+arrParams.account_type+"', ";
								query += " app_type = '1', ";
								query += " parent_id = '"+arrParams.app_id+"', ";
								query += " device_id = '"+data[0].device_id+"', ";
								query += " device_name = '"+data[0].device_name+"', ";
								query += " device_type = '"+data[0].device_type+"', ";
								query += " fcm_token = '"+data[0].fcm_token+"', ";
								query += " api_token = '"+data[0].api_token+"', ";
								query += " create_date = '"+standardDTUTC+"', ";
								query += " last_update_date = '"+standardDTUTC+"', ";
								query += " status  = '1' ";

								DB.query(query,null, function(data, error){
									
									if(data !== null){
										return_result(data.insertId);
									}
								});
							}
							else
							{ return_result(null); }
						});
					}
					else
					{ return_result(null); }
				});
			}
			else
			{ return_result(null); }
		}
		catch(err)
		{ console.log("saveDeviceAccountDetails",err); return_result(null); }
};

methods.__removeDeviceAccountDetails  = function(arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{
			//checking exists
			var query = "SELECT id FROM jaze_app_device_details WHERE status = '1' AND  app_type = '1' AND account_id = '"+arrParams.link_account_id+"' AND  parent_id = '"+arrParams.app_id+"' ";
			
			DB.query(query, null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					//update status
					var query = "UPDATE  jaze_app_device_details SET  status = '0'  WHERE status = '1' AND  app_type = '1' AND account_id = '"+arrParams.link_account_id+"' AND  parent_id = '"+arrParams.app_id+"' ";

					DB.query(query, null, function(data, error){

						if(error == null){
							return_result(1);
						}
						else
						{ return_result(null); }
					});
				}
				else
				{ return_result(null); }
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("removeDeviceAccountDetails",err); return_result(null); }	
};

methods.__saveLinkAccountDetails =  function(reqParams,return_result){

	try
	{
		HelperRestriction.getNewGroupId('jaze_user_link_account_details', function (group_id) {

			if(parseInt(group_id) !== 0){

				var	arrInsert = {
					account_id		:	reqParams.master_account_id,
					switch_count	:	"0",
					link_account_id	:	reqParams.account_id,
					create_date		:	moment().utc().format('YYYY-MM-DD HH:mm:ss'),
					status			: '1'
				};

				HelperRestriction.insertTBQuery('jaze_user_link_account_details',group_id, arrInsert, function (queGrpId) {

					return_result(group_id);
					
				});
			}
		});
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

methods.__removeLinkAccountDetails  = function(arrParams,return_result){

	try
	{
		if(arrParams !== null && parseInt(arrParams.link_group_id) !== 0)
		{
			var query = "DELETE FROM jaze_user_link_account_details WHERE group_id = '"+arrParams.link_group_id+"' ";
		
			DB.query(query, null, function(data, error){

				if(error == null){
					return_result(1);
				}
				else
				{ return_result(null); }
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("__removeLinkAccountDetails",err); return_result(null); }	
};

methods.__saveJazecomLinkAccountDetails =  function(reqParams,return_result){

	try
	{
		HelperRestriction.getNewGroupId('jaze_jazecom_link_accounts', function (group_id) {

			if(parseInt(group_id) !== 0){

				var	arrInsert = {
					account_id		:	reqParams.master_account_id,
					link_account_id	:	reqParams.account_id,
					link_account_type	:	reqParams.account_type,
					master_type		:	"0",
					status			: '1'
				};

				HelperRestriction.insertTBQuery('jaze_jazecom_link_accounts',group_id, arrInsert, function (queGrpId) {

					return_result(group_id);
					
				});
			}
		});
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

methods.__removeJazecomLinkAccountDetails  = function(arrParams,return_result){

	try
	{
		if(arrParams !== null && parseInt(arrParams.link_account_id) !== 0)
		{
			var query  =" SELECT group_id FROM jaze_jazecom_link_accounts WHERE ";
				query  +=" `group_id` in (SELECT group_id FROM jaze_jazecom_link_accounts WHERE  `meta_key` = 'account_id' and `meta_value` = '"+arrParams.master_account_id+"' and ";
				query  +=" `group_id` in (SELECT group_id FROM jaze_jazecom_link_accounts WHERE  `meta_key` = 'link_account_id' and `meta_value` = '"+arrParams.link_account_id+"')) ";
		
		
				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
						//delete
						var query = "DELETE FROM jaze_jazecom_link_accounts WHERE group_id = '"+data[0].group_id+"' ";
	
						DB.query(query, null, function(data, error){
	
							if(error == null){
								return_result(1);
							}
							else
							{ return_result(null); }
						});
					}
					else
					{ return_result(null); }
				});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("__removeJazecomLinkAccountDetails",err); return_result(null); }	
};

methods.___getWorkAccountDetails = function(reqParams,return_result)
{
	try
			{
				if(reqParams !== null)
				{		
					var query = " SELECT *  FROM jaze_user_basic_details WHERE  meta_key in ('team_fname','team_lname','team_display_pic','team_position','team_company_id') and  account_type = '4'  and account_id = '" + reqParams.account_id + "' and account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key = 'team_status' and meta_value = '1') ";

					DB.query(query, null, function(data, error){
			
						if (typeof data[0] !== 'undefined' && data[0] !== null)
						{
							var list = [];
				
							Object.assign(list, {account_id : data[0].account_id});
							Object.assign(list, {account_type : data[0].account_type});
	
							for (var i in data) 
							{
								Object.assign(list, {[data[i].meta_key]:  data[i].meta_value});
							}

							var accParams = { account_id  : list.team_company_id };

							HelperGeneral.getAccountDetails(accParams, function(compAccRres){	

								HelperGeneral.__getMobileCode(list, function(mobile_code){	

									var active_status = "0";

									var flag = "0";

									if(parseInt(reqParams.active_account_id) === parseInt(list.account_id) && parseInt(reqParams.active_account_type) === 4)
									{ active_status = "1"; }

									if(parseInt(reqParams.master_status) === 1)
									{ flag = "1"; }
									else if(parseInt(reqParams.group_id) === 0)
									{ flag = "2"; }
									else if(parseInt(reqParams.group_id) !== 0)
									{ flag = "3"; }

									var newObj = {
										account_id	  : list.account_id.toString(),
										account_type  : list.account_type.toString(),
										name		  : list.team_fname+' '+list.team_lname,
										logo		  : G_STATIC_IMG_URL + 'uploads/jazenet/'+list.team_display_pic,
										category_name : list.team_position,
										title		  : "work",
										mobile_code   : mobile_code.toString(),
										mobile_no     : list.team_mobile_no,
										email         : list.team_email,
										company_id	  : compAccRres.account_id.toString(),
										country		  : compAccRres.country_name,
										state		  : compAccRres.state_name,
										city		  : compAccRres.city_name,
										area		  : compAccRres.area_name,
										group_id	  : reqParams.group_id.toString(),
										active_status : active_status,
										master_status : reqParams.master_status,
										flag 		  : flag
									};

									return_result(newObj);
								});
							});
						}
						else
						{ return_result(null); }
					});
				}
				else
				{ return_result(null); }
			}
			catch(err)
			{ console.log("___getWorkAccountDetails",err); return_result(null); }		
}
//------------------------------------------------------------------- Link Account Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Job Profile Module Start ---------------------------------------------------------------------------//
methods.addJobProfile = function(arrParams,return_result){

	try
	{
		if(parseInt(arrParams.flag) == 0)
		{

			var query = `SELECT * FROM jaze_job_basic_profile WHERE group_id IN (SELECT group_id FROM jaze_job_basic_profile WHERE meta_key = 'account_id' AND meta_value =?)`;

			DB.query(query, [arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{

					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
								
								methods.__getNationality(jsonItems.nationality,function(retNationality){

									var default_logo = "";

									if(jsonItems.profile_logo){
										default_logo = G_STATIC_IMG_URL+"uploads/jazenet/individual_account/jobs/"+jsonItems.profile_logo;
									}

									var arrInsert = {
										id                 : jsonItems.group_id.toString(),
										account_id         : jsonItems.account_id.toString(),
										first_name         : jsonItems.first_name,
										last_name          : jsonItems.last_name,
										profile_logo       : default_logo,
										professional_title : jsonItems.professional_title,
										gender             : jsonItems.gender,
										nationality        : retNationality,
										nationality_id     : jsonItems.nationality.toString(),
										work_experience    : jsonItems.work_experience,
										education_level    : jsonItems.education_level,
										location           : jsonItems.location,
										notice_period      : jsonItems.notice_period,
										commitment         : jsonItems.commitment,
										expectation        : jsonItems.expectation,
										skill              : jsonItems.skill,
										about              : jsonItems.about,
										comment            : jsonItems.comment,
										jaze_call_status   : jsonItems.jaze_call_status.toString(),
										jaze_chat_status   : jsonItems.jaze_chat_status.toString(),
										jaze_mail_status   : jsonItems.jaze_mail_status.toString(),
										update_date        : dateFormat(jsonItems.update_date,'yyyy-mm-dd HH:MM:ss'),
										create_date        : dateFormat(jsonItems.create_date,'yyyy-mm-dd HH:MM:ss'),
										status             : jsonItems.status.toString(),
									};

									counter -= 1;
									resolve(arrInsert);

								});
							
							})
						}],
						function (result, current) {
							if (counter === 0){ 

								return_result(result[0]);	
							}
						});

					});
				
				}
				else
				{
					return_result(3);
				}

		});


		}
		else if(parseInt(arrParams.flag) == 1)
		{	

			var query = `SELECT * FROM jaze_job_basic_profile WHERE group_id IN (SELECT group_id FROM jaze_job_basic_profile WHERE meta_key = 'account_id' AND meta_value =?)`;
			DB.query(query, [arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					return_result(4);
				
				}
				else
				{
					
					var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_job_basic_profile";
		  			var lastGrpId = 0;

					methods.getLastGroupId(queGroupMax, function(queGrpRes){

					  	queGrpRes.forEach(function(row) {
							lastGrpId = row.lastGrpId;
					  	});

					  	var default_logo = "";
				  		if(arrParams.profile_logo !=""){

	 						default_logo = arrParams.account_id+"/"+arrParams.profile_logo+"."+arrParams.profile_logo_ext;
		 				}

						var arrInsert = {
							account_id         : arrParams.account_id,
							first_name         : arrParams.first_name,
							last_name          : arrParams.last_name,
							profile_logo       : default_logo,
							professional_title : arrParams.professional_title,
							gender             : arrParams.gender,
							nationality        : arrParams.nationality,
							work_experience    : arrParams.work_experience,
							education_level    : arrParams.education_level,
							location           : arrParams.location,
							notice_period      : arrParams.notice_period,
							commitment         : arrParams.commitment,
							expectation        : arrParams.expectation,
							skill              : arrParams.skill,
							about              : arrParams.about,
							comment            : arrParams.comment,
							jaze_call_status   : arrParams.jaze_call_status,
							jaze_chat_status   : arrParams.jaze_chat_status,
							jaze_mail_status   : arrParams.jaze_mail_status,
							update_date        : standardDT,
							create_date        : standardDT,
							status             : 1
						};
					
						var plusOneGroupID = lastGrpId+1;
				  		var querInsert = `insert into jaze_job_basic_profile (group_id, meta_key, meta_value) VALUES (?,?,?)`;

					 	for (var key in arrInsert) 
					 	{
							DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
					 		 	if(parseInt(data.affectedRows) > 0){
			 						return_result(1);
						 	   	}else{
				   					return_result(0);
						 	   	}
					 	 	});
					  	};

					});

				}

			});

		}
		else if(parseInt(arrParams.flag) == 2)
		{ 
			var query = `SELECT * FROM jaze_job_basic_profile WHERE group_id IN (SELECT group_id FROM jaze_job_basic_profile WHERE meta_key = 'account_id' AND meta_value =?)`;
			DB.query(query, [arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
			
					methods.convertMultiMetaArray(data, function(retConverted){

					  	var default_logo = "";
		 				if(arrParams.profile_logo !=""){

	 						var storagePath = G_STATIC_IMG_PATH+"individual_account/jobs/"+arrParams.account_id+'/'+retConverted[0].profile_logo;
							fs.unlink(storagePath, function (err) {
								console.log(err);

							});

							default_logo = arrParams.account_id+"/"+arrParams.profile_logo+"."+arrParams.profile_logo_ext;
		 				}

						var arrUpdate = {
							account_id         : arrParams.account_id,
							first_name         : arrParams.first_name,
							last_name          : arrParams.last_name,
							profile_logo       : default_logo,
							professional_title : arrParams.professional_title,
							gender             : arrParams.gender,
							nationality        : arrParams.nationality,
							work_experience    : arrParams.work_experience,
							education_level    : arrParams.education_level,
							location           : arrParams.location,
							notice_period      : arrParams.notice_period,
							commitment         : arrParams.commitment,
							expectation        : arrParams.expectation,
							skill              : arrParams.skill.toString(),
							about              : arrParams.about,
							comment            : arrParams.comment,
							jaze_call_status   : arrParams.jaze_call_status,
							jaze_chat_status   : arrParams.jaze_chat_status,
							jaze_mail_status   : arrParams.jaze_mail_status,
							update_date        : standardDT,
							create_date        : standardDT,
							status             : 1
						};

						var updateQue = `UPDATE jaze_job_basic_profile SET meta_value =? WHERE meta_key =? AND group_id =?`;
						
						for (var key in arrUpdate) 
					 	{	
						     if(arrUpdate[key] !== null && arrUpdate[key] != ""){
								DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(data, error){


							 		if(data !== null){
						 				return_result(2);
							 	   	}else{
						 	   			return_result(0);
							 	   	}
								});
							}
						}

					});

				}
				else
				{
					return_result(3);
				}

			});
		}
	}
	catch(err)
	{ 
		console.log("addJobProfile",err); return_result(null); 
	}	
};

methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
};


//------------------------------------------------------------------- Job Profile Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Security Module Start ---------------------------------------------------------------------------//

methods.updateAccountEmailId = function (arrParams,return_result){

	try
	{
		// checking is valid details
		var query = " SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'email' and  meta_value = '" +arrParams.old_email.toLowerCase()+ "' and  account_id = '" +arrParams.account_id+ "' ";

		DB.query(query, null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				if(parseInt(data[0].account_id) !== 0)
				{
					var updateQue = " update jaze_user_basic_details SET meta_value = '" +arrParams.new_email.toLowerCase()+ "' WHERE meta_key = 'email' and  account_id = '" +data[0].account_id+ "' ";

					DB.query(updateQue, null, function(data, error){
	
						if(error == null){
							return_result(1);
						}
						else
						{ return_result(3); }
					});
				}
				else
				{ return_result(2); }
			}
			else
			{ return_result(3); }
		});
	}
	catch(err)
	{ 
		console.log("updateAccountEmailId",err); 
		return_result(0); 
	}	
};

methods.updateAccountMobileNumber = function (arrParams,return_result){

	try
	{
		// checking is valid details
		var query = " SELECT account_id,account_type, group_concat(meta_value) as mob FROM jaze_user_basic_details WHERE account_id = '" +arrParams.account_id+ "' and meta_key in('mobile_country_code_id','company_phone_code','mobile','company_phone') ";

		DB.query(query, null, function(acData, error){

			if(typeof acData !== 'undefined' && acData !== null && parseInt(acData.length) > 0){

				var mob = acData[0].mob.split(",");

				if(parseInt(mob[0]) === parseInt(arrParams.old_code_id) && parseInt(mob[1]) === parseInt(arrParams.old_number))
				{
					if(parseInt(acData[0].account_type) === 3 || parseInt(acData[0].account_type) === 5)
					{
						var updateQue = " update jaze_user_basic_details SET meta_value = '" +arrParams.new_code_id+ "' WHERE meta_key = 'company_phone_code' and  account_id = '" +acData[0].account_id+ "' ";

						DB.query(updateQue, null, function(data, error){
	
							if(error == null){
								
								var updateQue = " update jaze_user_basic_details SET meta_value = '" +arrParams.new_number+ "' WHERE meta_key = 'company_phone' and  account_id = '" +acData[0].account_id+ "' ";

								DB.query(updateQue, null, function(data, error){
			
									if(error == null){
										return_result(1);
									}
									else
									{ return_result(3); }
								});
							}
							else
							{ return_result(3); }
						});
					}
					else 
					{
						var updateQue = " update jaze_user_basic_details SET meta_value = '" +arrParams.new_code_id+ "' WHERE meta_key = 'mobile_country_code_id' and  account_id = '" +acData[0].account_id+ "' ";

						DB.query(updateQue, null, function(data, error){
	
							if(error == null){
								
								var updateQue = " update jaze_user_basic_details SET meta_value = '" +arrParams.new_number+ "' WHERE meta_key = 'mobile' and  account_id = '" +acData[0].account_id+ "' ";

								DB.query(updateQue, null, function(data, error){
			
									if(error == null){
										return_result(1);
									}
									else
									{ return_result(3); }
								});
							}
							else
							{ return_result(3); }
						});
					}
				}
				else
				{ return_result(2); }
			}
			else
			{ return_result(3); }
		});
	}
	catch(err)
	{ 
		console.log("updateAccountEmailId",err); 
		return_result(0); 
	}	
};

methods.updateAccountSecurityPin = function (arrParams,return_result){

	try
	{
		// checking is valid details
		var query = " SELECT account_id FROM jaze_user_credential_details WHERE  jaze_security_pin = '" +arrParams.old_pin+ "' and  account_id = '" +arrParams.account_id+ "' and status = '1' ";

		DB.query(query, null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				if(parseInt(data[0].account_id) !== 0)
				{
					var updateQue = " update jaze_user_credential_details SET jaze_security_pin = '" +arrParams.new_pin+ "' WHERE  account_id = '" +data[0].account_id+ "' ";

					DB.query(updateQue, null, function(data, error){
	
						if(error == null){
							return_result(1);
						}
						else
						{ return_result(3); }
					});
				}
				else
				{ return_result(2); }
			}
			else
			{ return_result(3); }
		});
	}
	catch(err)
	{ 
		console.log("updateAccountSecurityPin",err); 
		return_result(0); 
	}	
};

methods.updateAccountPassword = function (arrParams,return_result){

	try
	{
		// checking is valid details
		var query = " SELECT account_id FROM jaze_user_credential_details WHERE  password = '" +arrParams.old_password+ "' and  account_id = '" +arrParams.account_id+ "' and status = '1' ";

		DB.query(query, null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				if(parseInt(data[0].account_id) !== 0)
				{
					var updateQue = " update jaze_user_credential_details SET password = '" +arrParams.new_password+ "' WHERE  account_id = '" +data[0].account_id+ "' ";

					DB.query(updateQue, null, function(data, error){
	
						if(error == null){
							return_result(1);
						}
						else
						{ return_result(3); }
					});
				}
				else
				{ return_result(2); }
			}
			else
			{ return_result(3); }
		});
	}
	catch(err)
	{ 
		console.log("updateAccountPassword",err); 
		return_result(0); 
	}	
};

//------------------------------------------------------------------- Security Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- General Module Start ---------------------------------------------------------------------------//

methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].group_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}

methods.__getCountryDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT country_name FROM country  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getStateDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT state_name FROM states  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].state_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getCityDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT city_name FROM cities  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].city_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getAreaDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT area_name FROM area  WHERE area_id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].area_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getMobileCode= function (phone_code,return_result){
	try
	{
		if(parseInt(phone_code) !== 0)
		{
			var query = " SELECT `phonecode` FROM `country` WHERE `id` = '"+phone_code+"'; ";
			DB.query(query, null, function(data, error){
				
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result("+"+data[0].phonecode);
				}
				else
				{ return_result(''); }
			});

			
		}
		else
		{
			return_result('');
		}
	}
	catch(err)
	{ console.log("getMobileCode",err); return_result(''); }	
};




methods.__getNationality = function(arrParams,return_result){

	try{

		if(parseInt(arrParams) !==0){

			let query  = " SELECT country_name FROM country  WHERE id =? ";
			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
			
		}else{
			return_result('');
		}
	
	}
	catch(err)
	{
		return_result('')
	}
};



function formatDate(current_datetime) {
	var dt = new Date(current_datetime);
	const months = ["January", "February", "March","April", "May", "June", "July", "August", "September", "October", "Novemnber", "December"];
	let formatted_date = dt.getDate() + " " + months[dt.getMonth()] + " " + dt.getFullYear()
	return formatted_date;
}


	/*
	 * sorting Array Object
	 */
	function sortingArrayObject(key, order='asc') {
		return function(a, b) {
		  if(!a.hasOwnProperty(key) || 
			 !b.hasOwnProperty(key)) {
			  return 0; 
		  }
		  
		  const varA = (typeof a[key] === 'string') ? 
			a[key].toUpperCase() : a[key];
		  const varB = (typeof b[key] === 'string') ? 
			b[key].toUpperCase() : b[key];
			
		  let comparison = 0;
		  if (varA > varB) {
			comparison = 1;
		  } else if (varA < varB) {
			comparison = -1;
		  }
		  return (
			(order == 'desc') ? 
			(comparison * -1) : comparison
		  );
		};
	}
		
methods.getFileTypes = function(ext,return_result){

	var type;

	var arrImages = ['.jpeg','.jpg','.gif','.png','.tif'];
	var arrVideos = ['.mp4','.avi','.mov','.flv','.wmv'];
	var arrAudio = ['.pcm','.wav','.aiff','.mp3','.aac','.wma','.flac','.alac'];
	var arrDocs = ['.odt','.xls','.xlsx','.ods','.ppt','.pptx','.txt','.doc'];
	var arrPdf = ['.pdf'];

	if(arrImages.includes(ext.toLowerCase())){
		type = '2';
	}else if(arrAudio.includes(ext.toLowerCase())){
		type = '3';
	}else if(arrVideos.includes(ext.toLowerCase())){
		type = '4';
	}else if(arrDocs.includes(ext.toLowerCase())){
		type = '5';
	}else if(arrPdf.includes(ext.toLowerCase())){
		type = '6';
	}else{
		type = '7';
	}

	return_result(type);
};
//------------------------------------------------------------------- General Module End ---------------------------------------------------------------------------//






module.exports = methods;