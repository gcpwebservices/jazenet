const methods = {};
const DB = require('../../helpers/db_base.js');

const promiseForeach = require('promise-foreach');
const dateFormat = require('dateformat');
const now = new Date();
const standardDT = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
const newdateForm = dateFormat(new Date(standardDT), 'yyyy-mm-dd HH:MM:ss');
const moment = require("moment");
const fs = require('fs');
const download = require('download-file');
const url = require('url');
const path = require('path');
const mime = require('mime-types');
var Imap = require('imap');
var mimemessage = require('mimemessage');
const mailer = require('nodemailer-promise');
const _ = require('lodash');

const atob = require('atob');

const dateUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

const fs_conversation = require('../../jazecom/firestore.js');

const HelperJazeMail = require('../../jazecom/helpers/HelperJazeMail.js');
const HelperNotification = require('../../jazecom/helpers/HelperNotification.js');


methods.getCompanyProfile =  function(arrParams,return_result){
	try
	{
		
		var arrAccounts = [];

		var queryAccountType = `SELECT account_id,account_type FROM jaze_user_basic_details WHERE account_id =?`;

		DB.query(queryAccountType,[arrParams.account_id], function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var queryAccDetails = `SELECT * FROM jaze_user_basic_details WHERE account_id =? and account_type=?`;

				DB.query(queryAccDetails,[data[0].account_id,data[0].account_type], function(allData, error){
	
					if(typeof allData !== 'undefined' && allData !== null && parseInt(allData.length) > 0)
					{

						methods.convertMultiMetaArrayCompany(allData, function(retConverted){	

							var counter = retConverted.length;		
							var jsonItems = Object.values(retConverted);

							promiseForeach.each(jsonItems,[function (val) {
								return new Promise(function (resolve, reject) {		
						
							  		var default_logo = '';

									var newObj = {};

							  		if(parseInt(data[0].account_type) === 1)
							  		{
							  			if(val.profile_logo){
											default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + val.profile_logo + '';
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : (val.fname +" "+val.lname).toString(),
											logo		  : default_logo
										}	

							  		}
							  		else if(parseInt(data[0].account_type) === 2)
							  		{

				  						if(val.professional_logo){
											default_logo = G_STATIC_IMG_URL + "uploads/jazenet/professional_account/"+val.account_id+"/logo_thumb/thumb_" + val.professional_logo;
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : (val.fname +" "+val.lname).toString(),
											logo		  : default_logo
										}	

							  		}
						  			else if(parseInt(data[0].account_type) === 3)
					  				{

		  								if(val.listing_images){
				  							default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/listing_images/' + val.listing_images + '';
										
										}else if(val.company_logo){
											default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/logo/' + val.company_logo + '';
										}else{
											default_logo = "";
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : val.company_name,
											logo		  : default_logo
										}	

					  				}

									counter -= 1;
									if (counter === 0){ 
										resolve(newObj);
									}
									
								})
							}],
							function (result, current) {
								if (counter === 0){ 
									return_result(result[0]);
								}
							});

						});


					}
					else
					{
						return_result(null);
					}
				});

		
			}
			else
			{
				return_result(null);
			}
		});

	}
	catch(err)
	{ console.log("getCompanyProfile",err); return_result(null);}
};


methods.__getCompanyCategoryName =  function (account_list,return_result){
	
	var return_obj = {
			account_type  : "",
			title_name 	  : "",
			category_name : ""
		};

	try
	{
		if(account_list !== null)
		{
			
			if(parseInt(account_list.account_type) === 1)
			{
				return_obj = {
					account_type  : "1",
					title_name 	  : "personal",
					category_name : "personal"
				};

				return_result(return_obj);
			}
			else if(parseInt(account_list.account_type) === 2)
			{
				return_obj = {
					account_type  : "2",
					title_name 	  : "professional",
					category_name : "professional"
				};

				return_result(return_obj);
			}
			else if(parseInt(account_list.account_type) === 3)
			{
				if((Object.keys(account_list)).indexOf("category_id") !== -1)
				{
					var query = "  SELECT meta_value  FROM jaze_category_list_backoffice WHERE  meta_key = 'cate_name' and group_id = '"+account_list.category_id+"'; ";
					
					DB.query(query, null, function(data, error){
					
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_obj = {
								account_type  : "3",
								title_name 	  : "company",
								category_name : data[0].meta_value
							};

							return_result(return_obj);
						}
						else
						{
							return_obj = {
								account_type  : "3",
								title_name 	  : "company",
								category_name : "company"
							};

							return_result(return_obj);
						}
					});
				}
				else
				{
					return_obj = {
						account_type  : "3",
						title_name 	  : "company",
						category_name : "company"
					};

					return_result(return_obj);
				}
			}
		}
		else
		{
			return_result(return_obj);
		}
	}
	catch(err)
	{ console.log("getAccountEmailId",err); return_result(return_obj); }	
};

methods.getCompanyProfileIn =  function(arrAccountIDS,return_result){

	try
	{
			
		var arrAccounts = [];
		var newObj = {};

		arrAccountIDS.forEach(function(value) {

			var queryAccountType = "SELECT account_id,account_type FROM jaze_user_basic_details WHERE account_id=?";

			DB.query(queryAccountType,[value], function(data, error){
				
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{

					var queryAccDetails = `SELECT * FROM jaze_user_basic_details WHERE account_id =? and account_type=?`;
					DB.query(queryAccDetails,[data[0].account_id,data[0].account_type], function(allData, error){

						if(typeof allData !== 'undefined' && allData !== null && parseInt(allData.length) > 0)
						{

							methods.convertMultiMetaArrayCompany(allData, function(retConverted){	

								var counter = retConverted.length;		
								var jsonItems = Object.values(retConverted);

								promiseForeach.each(jsonItems,[function (val) {
									return new Promise(function (resolve, reject) {	

										var accParam = {account_type:data[0].account_type, category_id:val.category_id };
										methods.__getCompanyCategoryName(accParam, function(retCategory){	
	
									  		var default_logo = '';

					  			  			if(parseInt(data[0].account_type) === 1)
									  		{
									  			if(val.profile_logo){
													default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + val.profile_logo + '';
												}

												newObj = {
													account_id	  : val.account_id.toString(),
													account_type  : data[0].account_type.toString(),
													name		  : (val.fname +" "+val.lname).toString(),
													logo		  : default_logo
												}	

									  		}
									  		else if(parseInt(data[0].account_type) === 2)
									  		{

						  						if(val.professional_logo){
													default_logo = G_STATIC_IMG_URL + "uploads/jazenet/professional_account/"+val.account_id+"/logo_thumb/thumb_" + val.professional_logo;
												}

												newObj = {
													account_id	  : val.account_id.toString(),
													account_type  : data[0].account_type.toString(),
													name		  : (val.fname +" "+val.lname).toString(),
													logo		  : default_logo
												}	

									  		}
								  			else if(parseInt(data[0].account_type) === 3)
							  				{

				  								if(val.listing_images){
						  							default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/listing_images/' + val.listing_images + '';
												
												}else if(val.company_logo){
													default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/logo/' + val.company_logo + '';
												}else{
													default_logo = "";
												}

												newObj = {
													account_id	  : val.account_id.toString(),
													account_type  : data[0].account_type.toString(),
													name		  : val.company_name,
													logo		  : default_logo
												}	

							  				}

											counter -= 1;
											arrAccounts.push(newObj);
											if (counter === 0){ 
												resolve(arrAccounts);
											}

										});
										
									})
								}],
								function (result, current) {
									if (counter === 0){ 
										return_result(result[0]);
									}
								});

							});
						}
						else
						{
							return_result(null);
						}
					});

				}
				else
				{
					return_result(null);
				}
			});

		});

	}
	catch(err)
	{ console.log("getCompanyProfileIn",err); return_result(null);}
};


methods.getActivityList =  function(arrParams,return_result){
	try
	{
		var arrObjects    = {};
		var arrResults    = [];
		var arrAccountIDS = [];

		var arrGroupID   = [];

		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };

		var query = "SELECT m.* FROM jaze_documents_activity_meta AS m LEFT JOIN jaze_documents_activity AS a ON m.group_id = a.id WHERE a.account_id =?";
			if(arrParams.company_id !=""){
				query += " OR a.account_id ='"+arrParams.company_id+"'";
			}
		DB.query(query,[arrParams.account_id], function(pdata, error){

			if(typeof pdata !== 'undefined' && pdata !== null && parseInt(pdata.length) > 0)
			{
				
				methods.convertMultiMetaArray(pdata, function(retConverted){	
					const forEach = async () => {
					  	const values = retConverted;
					  	for (const val of values){
							arrGroupID.push(val.group_id);
					  	}

				  		const result = await returnData(arrGroupID);
					}
					 
					const returnData = x => {
					  return new Promise((resolve, reject) => {
					    setTimeout(() => {
					      resolve(x);
					    }, 300);
					  });
					}
					 
					forEach().then(() => {
						
						var queryMeta = "";
						var arrGroupIDMeta = [];

						if(arrParams.company_id !="" && parseInt(arrParams.category) == 0){

							queryMeta = "SELECT * FROM jaze_documents_activity_meta ";
							queryMeta += " WHERE group_id IN("+arrGroupID+") ";

							if(arrParams.subject !=""){
								queryMeta += " AND group_id IN ( SELECT group_id FROM jaze_documents_activity_meta WHERE meta_key = 'subject' AND meta_value LIKE '%"+arrParams.subject+"%')";
							}else{
								queryMeta += " AND group_id IN ( SELECT group_id FROM jaze_documents_activity_meta WHERE meta_key = 'to_account_id' AND (meta_value ='"+arrParams.company_id+"' OR meta_value ='"+arrParams.account_id+"') )"
							}
				
						}else if(arrParams.company_id =="" && parseInt(arrParams.category) == 0){

							queryMeta = "SELECT * FROM jaze_documents_activity_meta ";
							queryMeta += " WHERE group_id IN("+arrGroupID+") ";

							if(arrParams.subject !=""){
								queryMeta += " AND group_id IN ( SELECT group_id FROM jaze_documents_activity_meta WHERE meta_key = 'subject' AND meta_value LIKE '%"+arrParams.subject+"%')";
							}
					
						}else if(arrParams.company_id =="" && parseInt(arrParams.category) == 1){

							queryMeta = "SELECT * FROM jaze_documents_activity_meta ";
							queryMeta += " WHERE group_id IN ( SELECT group_id FROM jaze_documents_activity_meta WHERE meta_key = 'to_account_id' AND meta_value !='"+arrParams.account_id+"')";
							queryMeta += " AND group_id IN("+arrGroupID+") ";

							if(arrParams.subject !=""){
								queryMeta += " AND group_id IN ( SELECT group_id FROM jaze_documents_activity_meta WHERE meta_key = 'subject' AND meta_value LIKE '%"+arrParams.subject+"%')";
							}

						}else if(arrParams.company_id !="" && parseInt(arrParams.category) == 1){

							queryMeta = "SELECT * FROM jaze_documents_activity_meta ";
							queryMeta += " WHERE group_id IN ( SELECT group_id FROM jaze_documents_activity_meta WHERE meta_key = 'to_account_id' AND meta_value ='"+arrParams.company_id+"')";
							queryMeta += " AND group_id IN("+arrGroupID+") ";

							if(arrParams.subject !=""){
								queryMeta += " AND group_id IN ( SELECT group_id FROM jaze_documents_activity_meta WHERE meta_key = 'subject' AND meta_value LIKE '%"+arrParams.subject+"%')";
							}

						}else if(arrParams.company_id =="" && parseInt(arrParams.category) == 2){

							queryMeta = "SELECT * FROM jaze_documents_activity_meta ";
							queryMeta += " WHERE group_id IN ( SELECT group_id FROM jaze_documents_activity_meta WHERE meta_key = 'to_account_id' AND meta_value ='"+arrParams.account_id+"')";

							if(arrParams.subject !=""){
								queryMeta += " AND group_id IN ( SELECT group_id FROM jaze_documents_activity_meta WHERE meta_key = 'subject' AND meta_value LIKE '%"+arrParams.subject+"%')";
							}
			
						}else if(arrParams.company_id !="" && parseInt(arrParams.category) == 2){

							queryMeta = "SELECT * FROM jaze_documents_activity_meta ";
							queryMeta += " WHERE group_id IN ( SELECT group_id FROM jaze_documents_activity_meta WHERE meta_key = 'to_account_id' AND meta_value ='"+arrParams.company_id+"')";

							if(arrParams.subject !=""){
								queryMeta += " AND group_id IN ( SELECT group_id FROM jaze_documents_activity_meta WHERE meta_key = 'subject' AND meta_value LIKE '%"+arrParams.subject+"%')";
							}

						}else if(arrParams.company_id =="" && arrParams.date !=""){

							queryMeta = "SELECT * FROM jaze_documents_activity_meta WHERE group_id IN("+arrGroupID+")";

							if(arrParams.subject !=""){
								queryMeta += " AND group_id IN ( SELECT group_id FROM jaze_documents_activity_meta WHERE meta_key = 'subject' AND meta_value LIKE '%"+arrParams.subject+"%')";
							}

						}else{

							queryMeta = "SELECT * FROM jaze_documents_activity_meta WHERE group_id IN("+arrGroupID+")";
							queryMeta += " OR group_id IN ( SELECT group_id FROM jaze_documents_activity_meta WHERE meta_key = 'to_account_id' AND meta_value ='"+arrParams.account_id+"')";

							if(arrParams.subject !=""){
								queryMeta += " AND group_id IN ( SELECT group_id FROM jaze_documents_activity_meta WHERE meta_key = 'subject' AND meta_value LIKE '%"+arrParams.subject+"%')";
							}

						}

						if(arrParams.date !=""){
							queryMeta += " AND group_id IN ( SELECT group_id FROM jaze_documents_activity_meta WHERE meta_key = 'create_date' AND DATE(meta_value) ='"+arrParams.date+"')";
						}

						DB.query(queryMeta,null, function(data, error){
					
							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

								methods.convertMultiMetaArray(data, function(retConverted){	

									var counter = retConverted.length;		
									var jsonItems = Object.values(retConverted);

									promiseForeach.each(jsonItems,[function (jsonItems) {
										return new Promise(function (resolve, reject) {		
							
											arrGroupIDMeta.push(jsonItems.group_id);
											counter -= 1;
											if (counter === 0){ 
												resolve(arrGroupIDMeta);
											}
										})
									}],
									function (result, current) {
										if (counter === 0){ 

											var	filteredArray = removeDuplicates(result[0]);
											methods.getActivityListDetails(filteredArray,arrParams, function(company,documents,paginate){	
												return_result(company,documents,paginate);
											});
													
										}
									});

								});

							}else{
								return_result([],null,pagination_details);
							}

						})

					});
				});

			}
			else
			{
				return_result([],null,pagination_details);
			}

		});

	}
	catch(err)
	{ console.log("getLibraryList",err); return_result(null);}
};



methods.getActivityListDetails =  function(arrGroupIDS,arrParams,return_result){
	try
	{
		var arrObjects    = {};
		var arrResults    = [];
		var arrAccountIDS = [];

		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };

		var query = `SELECT * FROM jaze_documents_activity WHERE id IN(?)`;
		DB.query(query,[arrGroupIDS], function(pdata, error){

			if(typeof pdata !== 'undefined' && pdata !== null && parseInt(pdata.length) > 0){

				var counter = pdata.length;		
				var jsonItemsMain = Object.values(pdata);

				promiseForeach.each(jsonItemsMain,[function (jsonItemsMain) {
					return new Promise(function (resolve, reject) {

						DB.query("SELECT * FROM jaze_documents_activity_meta WHERE group_id=?",[jsonItemsMain.id], function(cdata, error){

							methods.convertMultiMetaArray(cdata, function(retConverted){	

								if(retConverted[0].to_account_id){
									if(parseInt(arrParams.account_id) !== parseInt(retConverted[0].to_account_id)){
										arrAccountIDS.push(retConverted[0].to_account_id.toString());
									}
								}

								if(jsonItemsMain.account_id){
									if(parseInt(arrParams.account_id) !== parseInt(jsonItemsMain.account_id)){
										arrAccountIDS.push(jsonItemsMain.account_id.toString());
									}
								}
	
								var gParam = {id:retConverted[0].group_id};
								methods.getSenderAccountID(gParam, function(retAccoutnID){	

									var accParam = {account_id:retAccoutnID};
									if(parseInt(arrParams.account_id) !== parseInt(retConverted[0].to_account_id) ){
										accParam = {account_id:retConverted[0].to_account_id};
									}

									methods.getCompanyProfile(accParam, function(accRet){	

										if(accRet!=null)
										{
									
											var action = 'Received';
											if(parseInt(arrParams.account_id) !== parseInt(retConverted[0].to_account_id) ){
												action = 'Sent';
											}

											var file_split = jsonItemsMain.doc_path.split('/');
											var file_name  = file_split[2].split('.');

											var title;
											var subject;

											if(retConverted[0].subject){
												subject = retConverted[0].subject;
											}

											if(retConverted[0].doc_title){
												title = retConverted[0].doc_title;
											}

											arrObjects = {
												document_id     : jsonItemsMain.id.toString(),
												account_id      : accRet.account_id.toString(),
												account_name    : accRet.name,
												account_logo    : accRet.logo,
												subject         : subject,
												title           : title,
												file_name       : file_name[0],
												ext             : jsonItemsMain.doc_exten,
												path            : G_STATIC_IMG_URL+"uploads/jazenet/documents/"+jsonItemsMain.doc_path,
												action          : action,
												date            : dateFormat(retConverted[0].create_date,'yyyy-mm-dd HH:MM:ss')
											};

											arrResults.push(arrObjects);
										}

										counter -= 1;
										if (counter === 0){ 

				            			    arrResults.sort(function(a, b){ 
						                   	 	return new Date(b.date) - new Date(a.date); 
						                	});

											resolve(arrResults);
										}
				
									});

								});
							});
						});
			
				
					})
				}],
				function (result, current) {
					if (counter === 0){ 

						var	filteredArray = removeDuplicates(arrAccountIDS);
			
						var newArray = [];
						var total_result = result[0].length.toString();
						var total_page = 0;
						var next_page = 0;

						if(parseInt(result[0].length) > 10){
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 10 ), (parseInt(arrParams.page) + 1) * 10);
							total_page = Math.ceil((parseInt(result[0].length) - 10 ) / 10);
						}else{
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 10 ), (parseInt(arrParams.page) + 1) * 10);
							total_page = 0;
						}

						next_page  = parseInt(arrParams.page)+1;
						if(next_page >  total_page ){
							next_page = 0;
						}

	                 	if(parseInt(total_page) >= parseInt(arrParams.page)){

							pagination_details  = {
								total_page   : total_page.toString(),
				                current_page : arrParams.page.toString(),
				                next_page    : next_page.toString(),
				                total_result : total_result
				            };

        					if(parseInt(arrParams.page) == 0){

    							methods.getCompanyProfileIn(filteredArray, function(accRet){
    								if(accRet!=null){
    									setTimeout(function(){ 
											return_result(accRet.sort(sortingArrayObject('name')),newArray,pagination_details);
    									}, 1000);
										
    								}
								});
        		
        					}else{
    							return_result([],newArray,pagination_details);
        					}

			            }else{
		            		return_result([],null,pagination_details);
			            }
					}
				});
			}
			else
			{
				return_result([],null,pagination_details);
			}

		});
	}
	catch(err)
	{ console.log("getActivityListDetails",err); return_result(null);}
};

methods.getSenderAccountID =  function(arrParams,return_result){
	try
	{
		var arrObjects = {};
		var arrResults = [];

		var query = `SELECT account_id FROM jaze_documents_activity WHERE id=?`;

		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				return_result(data[0].account_id);
			}
			else
			{
				return_result(0);
			}

		});

	}
	catch(err)
	{ console.log("getSenderAccountID",err); return_result(0);}
};


// methods.getActivityList =  function(arrParams,return_result){
// 	try
// 	{
// 		var arrObjects    = {};
// 		var arrResults    = [];
// 		var arrAccountIDS = [];

// 		var pagination_details  = {
// 			total_page   : '0',
// 	    	current_page : '0',
// 	    	next_page    : '0',
// 	    	total_result : '0'
// 	    };

// 		var query = `SELECT * FROM jaze_documents_activity WHERE account_id=?`;
// 		DB.query(query,[arrParams.account_id], function(pdata, error){

// 			if(typeof pdata !== 'undefined' && pdata !== null && parseInt(pdata.length) > 0){

// 				var counter = pdata.length;		
// 				var jsonItemsMain = Object.values(pdata);

// 				promiseForeach.each(jsonItemsMain,[function (jsonItemsMain) {
// 					return new Promise(function (resolve, reject) {

// 						DB.query("SELECT * FROM jaze_documents_activity_meta WHERE group_id=?",[jsonItemsMain.id], function(cdata, error){

// 							methods.convertMultiMetaArray(cdata, function(retConverted){	

// 								if(retConverted[0].to_account_id){
// 									if(parseInt(arrParams.account_id) !== parseInt(retConverted[0].to_account_id)){
// 										arrAccountIDS.push(retConverted[0].to_account_id.toString());
// 									}

// 								}

// 								if(retConverted[0].account_id){
// 									if(parseInt(arrParams.account_id) !== parseInt(retConverted[0].account_id)){
// 										arrAccountIDS.push(retConverted[0].account_id.toString());
// 									}

// 								}
					
// 								var accParam = {account_id:retConverted[0].to_account_id};
// 								methods.getCompanyProfile(accParam, function(accRet){	

								
// 									if(accRet!=null)
// 									{
								
// 										var action = 'Received';
// 										if(parseInt(arrParams.account_id) !== parseInt(retConverted[0].to_account_id) ){
// 											action = 'Sent';
// 										}

// 										arrObjects = {
// 											document_id     : jsonItemsMain.id.toString(),
// 											account_id      : retConverted[0].to_account_id.toString(),
// 											account_name    : accRet.name,
// 											account_logo    : accRet.logo,
// 											subject         : retConverted[0].subject,
// 											title           : jsonItemsMain.doc_title,
// 											ext             : jsonItemsMain.doc_exten,
// 											path            : STATIC_IMG_URL+jsonItemsMain.doc_path,
// 											action          : action,
// 											date            : dateFormat(retConverted[0].create_date,'yyyy-mm-dd HH:MM:ss')
// 										};

// 										arrResults.push(arrObjects);
									
// 									}

// 									counter -= 1;
// 									if (counter === 0){ 

// 			            			    arrResults.sort(function(a, b){ 
// 					                   	 	return new Date(b.date) - new Date(a.date); 
// 					                	});

// 										resolve(arrResults);
// 									}
			

// 								});
// 							});
// 						});
			
				
// 					})
// 				}],
// 				function (result, current) {
// 					if (counter === 0){ 

// 						var	filteredArray = removeDuplicates(arrAccountIDS);
			
// 						var newArray = [];
// 						var total_result = result[0].length.toString();
// 						var total_page = 0;
// 						var next_page = 0;

// 						if(parseInt(result[0].length) > 10){
// 							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 10 ), (parseInt(arrParams.page) + 1) * 10);
// 							total_page = Math.ceil((parseInt(result[0].length) - 10 ) / 10);
// 						}else{
// 							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 10 ), (parseInt(arrParams.page) + 1) * 10);
// 							total_page = 0;
// 						}

// 						next_page  = parseInt(arrParams.page)+1;
// 						if(next_page >  total_page ){
// 							next_page = 0;
// 						}

// 	                 	if(parseInt(total_page) >= parseInt(arrParams.page)){

// 							pagination_details  = {
// 								total_page   : total_page.toString(),
// 				                current_page : arrParams.page.toString(),
// 				                next_page    : next_page.toString(),
// 				                total_result : total_result
// 				            };


//         					if(parseInt(arrParams.page) == 0){

//     							methods.getCompanyProfileIn(filteredArray, function(accRet){
//     								if(accRet!=null){
//     									setTimeout(function(){ 
// 											return_result(accRet.sort(sortingArrayObject('name')),newArray,pagination_details);
//     									}, 1000);
										
//     								}
// 								});
        		
//         					}else{

//     							return_result([],newArray,pagination_details);
//         					}

						  	
	
// 			            }else{
// 		            		return_result([],null,pagination_details);
// 			            }
					
// 					}
// 				});

// 			}
// 			else
// 			{
// 				return_result([],null,pagination_details);
// 			}

// 		});

// 	}
// 	catch(err)
// 	{ console.log("getLibraryList",err); return_result(null);}
// };


methods.getLibraryList =  function(arrParams,return_result){
	try
	{
		var arrObjects = {};
		var arrResults = [];

		var query = `SELECT * FROM jaze_documents_library WHERE group_id IN (SELECT group_id FROM jaze_documents_library WHERE meta_key = 'account_id' AND meta_value =?)
		ORDER BY group_id DESC`;
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
				
							arrObjects = {
								id    :jsonItems.group_id.toString(),
								title :jsonItems.doc_title,
								ext   :jsonItems.doc_exten,
								path  :G_STATIC_IMG_URL+"uploads/jazenet/documents/"+jsonItems.doc_path
							};

							arrResults.push(arrObjects);
							counter -= 1;
							resolve(arrResults);
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getLibraryList",err); return_result(null);}
};


methods.addToLibrary = function(arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{	
						
			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_documents_library";
  			var lastGrpId = 0;

			methods.getLastGroupId(queGroupMax, function(queGrpRes){

			  	queGrpRes.forEach(function(row) {
					lastGrpId = row.lastGrpId;
			  	});

			  	var full_filename = arrParams.file_name+'.'+arrParams.file_ext;

				var arrInsert = {
					account_id  : arrParams.account_id,
					doc_exten   : arrParams.file_ext,
					doc_path    : arrParams.account_id+'/library/'+full_filename,
					doc_title   : arrParams.title,
					create_date : newdateForm,
					status:1
				};
			
				var plusOneGroupID = lastGrpId+1;
		  		var querInsert = `insert into jaze_documents_library (group_id, meta_key, meta_value) VALUES (?,?,?)`;

		  		var returnFunction;
			 	for (var key in arrInsert) 
			 	{
					DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
			 		 	if(parseInt(data.affectedRows) > 0){
	 						return_result(data.affectedRows);
				 	   	}else{
		   					return_result(0);
				 	   	}
			 	 	});
			  	};
			  	
			});
		}
		else
		{ 
			return_result(null); 
		}
	}
	catch(err)
	{ 
		console.log("addToLibrary",err); return_result(null); 
	}	
};

methods.updateLibrary = function(arrParams,return_result){

	try
	{
		if(parseInt(arrParams.document_id) !==0 )
		{			

			var queLib = `SELECT * FROM jaze_documents_library WHERE group_id=? `;

			DB.query(queLib,[arrParams.document_id], function(dataL, error){

				if(typeof dataL !== 'undefined' && dataL !== null && parseInt(dataL.length) > 0)
				{
	 				
					methods.convertMultiMetaArray(dataL, function(retConverted){

						var fullfile_name = retConverted[0].doc_path.split("/");
						var filename_parts = fullfile_name[2].split(".");

		 				if(arrParams.file_name !="" && arrParams.file_ext !=""){

		 					var storagePath = G_STATIC_IMG_PATH+'documents/'+arrParams.account_id+'/library/'+filename_parts[0]+'.'+filename_parts[1];
		 	
							fs.unlink(storagePath, function (err) {});


		 					var full_filename = arrParams.file_name+'.'+arrParams.file_ext;

							var	arrUpdate = {
								account_id  : arrParams.account_id,
								doc_exten   : arrParams.file_ext,
								doc_path    : arrParams.account_id+'/library/'+full_filename,
								doc_title   : arrParams.title,
								create_date : newdateForm,
								status:1
							};

		 				}else{

 							var	arrUpdate = {
								account_id  : arrParams.account_id,
								doc_title   : arrParams.title,
								create_date : newdateForm,
								status:1
							};

		 				}


						var updateQue = `update jaze_documents_library SET meta_value =? WHERE meta_key =? AND group_id =?`;
						
						for (var key in arrUpdate) 
					 	{	
						     if(arrUpdate[key] !== null && arrUpdate[key] != ""){
								DB.query(updateQue, [arrUpdate[key],key,arrParams.document_id], function(data, error){
							 		if(data !== null){
						 				return_result(1);
							 	   	}else{
						 	   			return_result(0);
							 	   	}
								});
							}
						}

				 	});
		 	   	}
		 	   	else
		 	   	{
		 	   		return_result(2);
		 	   	}

	 	 	});

		}
		else
		{ 
			return_result(0); 
		}
	}
	catch(err)
	{ 
		console.log("updateLibrary",err); return_result(0); 
	}	
};


methods.removeToLibrary = function(arrParams,return_result){

	try
	{

		if(parseInt(arrParams.document_id) !==0 )
		{	

			var queLib = `SELECT * FROM jaze_documents_library WHERE group_id=? `;
			DB.query(queLib,[arrParams.document_id], function(dataL, error){

				if(typeof dataL !== 'undefined' && dataL !== null && parseInt(dataL.length) > 0)
				{

					methods.convertMultiMetaArray(dataL, function(retConverted){

						var fullfile_name = retConverted[0].doc_path.split("/");
						var filename_parts = fullfile_name[2].split(".");
						var storagePath = G_STATIC_IMG_PATH+'documents/'+arrParams.account_id+'/library/'+filename_parts[0]+'.'+filename_parts[1];
						fs.unlink(storagePath, function (err) {
							if(err==null)
							{
								var queDel = `DELETE FROM jaze_documents_library where group_id =? `;
								DB.query(queDel,[arrParams.document_id], function(data, error){

								 	if(parseInt(data.affectedRows) > 0){
				 						return_result(1);
							 	   	}else{
					   					return_result(0);
							 	   	}

								});
							}

						});

					});

			   	}
		 	   	else
		 	   	{
		 	   		return_result(2);
		 	   	}

			});

	   	}
 	   	else
 	   	{
 	   		return_result(0);
 	   	}

	}
	catch(err)
	{ console.log("removeToLibrary",err); return_result(0); }	
};


methods.sendDocument = function(arrParams,return_result){

	try
	{
		if(parseInt(arrParams.to_account_id) !=0 && parseInt(arrParams.document_id) == 0 )
		{	

			//send
				
			var storagePath = arrParams.account_id+'/activity/'+arrParams.file_name+'.'+arrParams.file_ext;

	  		var mainQuer = `INSERT INTO jaze_documents_activity (account_id, doc_path, doc_exten, status) VALUES (?,?,?,'1')`;

  			DB.query(mainQuer,[arrParams.account_id,storagePath,arrParams.file_ext], function(data, error){

	 		 	if(parseInt(data.affectedRows) > 0){

	 		 		if(parseInt(arrParams.flag) == 2)
 		 			{
 		 				// done with send to chat
	 		 			var file_url = G_STATIC_IMG_URL+"uploads/jazenet/documents/"+arrParams.account_id+'/activity/'+arrParams.file_name+'.'+arrParams.file_ext;

		 		 		var conversParam = {

		 		 			account_id    : arrParams.account_id,
		 		 			to_account_id : arrParams.to_account_id,
		 		 			file_url      : file_url,
		 		 			file_name     : arrParams.file_name,
		 		 			file_ext      : arrParams.file_ext
		 		 		}

						methods.getConversationDetails(conversParam,function(retConversation){

						
							setTimeout(function(){
							
								var group_id = data.insertId;

							  	var full_filename = arrParams.file_name+'.'+arrParams.file_ext;

								var arrInsert = {
									to_account_id  : arrParams.to_account_id,
									doc_title      : arrParams.title,
									subject        : arrParams.subject,
									send_type      : arrParams.flag,
									create_date    : newdateForm,
									status         : 1
								};
							
				
						  		var querInsert = `insert into jaze_documents_activity_meta (group_id, meta_key, meta_value) VALUES (?,?,?)`;

						  		var returnFunction;
							 	for (var key in arrInsert) 
							 	{
									DB.query(querInsert,[group_id,key,arrInsert[key]], function(data, error){
							 		 	if(parseInt(data.affectedRows) > 0){
					 						return_result(1);
								 	   	}else{
						   					return_result(0);
								 	   	}
							 	 	});
							  	};

		  				 	}, 100);


						});

 		 			}
 		 			else if(parseInt(arrParams.flag) == 1)
 		 			{

 		 				methods.getEmailAccountDetails(arrParams, function(retSenderAccount){

 		 					if(retSenderAccount!=null)
 		 					{	
 		 						var recParam = {account_id:arrParams.to_account_id};
								methods.getEmailAccountDetails(recParam, function(retReceiverAccount){

									if(retReceiverAccount!=null)
 		 							{

										var atobconvert = atob(retSenderAccount.email_password);
										//var password = "'" + atobconvert + "'";

										var sendEmail = mailer.config({
									  	    host: retSenderAccount.smtp_host,
										    port: retSenderAccount.smtp_port,
										    secure: true, // true for 465, false for other ports
										    auth: {
										      	user: retSenderAccount.email_account, // generated ethereal user
										      	pass: atobconvert
										      	  // generated ethereal password
										    }
										});

										var full_filname = arrParams.file_name+'.'+arrParams.file_ext;
										var file_url = G_STATIC_IMG_URL+"uploads/jazenet/documents/"+arrParams.account_id+'/activity/'+arrParams.file_name+'.'+arrParams.file_ext;

										var obj_files = {filename:full_filname,path:file_url};
													


										var message_arr = {
									  	    from: retSenderAccount.email_account, // sender address
									  	    to:retReceiverAccount.email_account,
									  	    cc:'',
									  	    bcc:'',
									  	    replyTo:'',
									  	    inReplyTo: '',
									  	    references: '',
										    subject: arrParams.subject, // Subject line
										    //text: all_message, // plain text body
										    html: arrParams.remarks, // html body
									    	attachments:obj_files
										};

										var arrToEmail = [];

										sendEmail(message_arr)
									    .then(function(info){


									    	arrToEmail.push(retReceiverAccount.email_account);

						    				var toCheckEmails = JSON.stringify(arrToEmail).replace(/[\[\]]+/g,"");
											var arrAccountIdtoNoti = [];
											var mail_push_notification = {};
					 	                   	var today = new Date();
					 	                   	var dateNow = dateFormat(today,'yyyy-mm-dd');

											HelperJazeMail.checkIfinCredentials(toCheckEmails, function(retAccountIds){
		

												mail_push_notification = {
													group_id            : (retAccountIds[0].email_address+'|'+info.messageId+'|'+dateNow).toString(),
													account_id          : arrParams.account_id.toString(),
													request_account_id  : arrParams.to_account_id.toString(),
													flag                : "12"
												};


												HelperNotification.sendPushNotification(mail_push_notification, function(returnNoti){});

												HelperJazeMail.getReceiverList(arrParams, function(returnReceivers){

													var toInsertEmail;
													var toInsertGroupId;
													var newFiltered;
													var toRemove = '@jazenet.com';
													var filtered,ToFilterEmails;

													if(returnReceivers !=null)
													{
														if(returnReceivers[0].length > 0){

															arrToEmail.forEach(function(row) {
																if(!returnReceivers[0].includes(row)){
																	returnReceivers[0].push(row);
																}
														  	});

															var ArrClean = returnReceivers[0].filter(v=>v!='');
															toInsertEmail = JSON.stringify(ArrClean).replace(/[\[\]"]+/g,"");

														}else{
															toInsertEmail = JSON.stringify(retReceiverAccount.email_account).replace(/[\[\]"]+/g,"");
														}

													
														toInsertGroupId = returnReceivers[1];

														ToFilterEmails = toInsertEmail.split(",");

				
    													filtered = ToFilterEmails.filter(function (str) { return str.indexOf(toRemove) === -1; });

														newFiltered = JSON.stringify(filtered).replace(/[\[\]"]+/g,"");

											

														var arrUpdate = {
															group_id   : toInsertGroupId,
															account_id : arrParams.account_id,
															email_id   : newFiltered
														};

														//console.log('update', arrUpdate);

														HelperJazeMail.updateReceivers(arrUpdate, function(retUpdate){});

													}else{

														var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_email_accounts_ids";
														var lastGrpId = 0;

														HelperJazeMail.getLastGroupId(queGroupMax, function(queGrpRes){		

															queGrpRes.forEach(function(row) {
																lastGrpId = row.lastGrpId;
													  		});

															toInsertGroupId = lastGrpId+1;
															toInsertEmail = retReceiverAccount.email_account;

															ToFilterEmails = toInsertEmail.split(",");

														
    														filtered = ToFilterEmails.filter(function (str) { return str.indexOf(toRemove) === -1; });

															newFiltered = JSON.stringify(filtered).replace(/[\[\]"]+/g,"");

													

															var arrInsert = {
																group_id   : toInsertGroupId,
																account_id : arrParams.account_id,
																email_id   : newFiltered
															};

															//console.log('insert', arrInsert);

															HelperJazeMail.insertReceivers(arrInsert, function(retInsert){});

														});
															
													}


												});

											});


											jazeComContactFunc(arrParams,arrToEmail,function(){});



											var emailContent = {
										  	    from: retSenderAccount.email_account, // sender address
										    	to: retReceiverAccount.email_account,
									    		cc: '',
									    		bcc: '',
											    subject: arrParams.subject, // Subject line
											    body_mes: arrParams.remarks, // plain text body
										     	message_id:info.messageId
											};

											copyToSent(retSenderAccount,emailContent,full_filname,file_url, function(returnSent){});



 											setTimeout(function(){
								
												var group_id = data.insertId;

												var arrInsert = {
													to_account_id  : arrParams.to_account_id,
													doc_title      : arrParams.title,
													subject        : arrParams.subject,
													send_type      : arrParams.flag,
													create_date    : newdateForm,
													status         : 1
												};
												
								
										  		var querInsert = `insert into jaze_documents_activity_meta (group_id, meta_key, meta_value) VALUES (?,?,?)`;

										  		var returnFunction;
											 	for (var key in arrInsert) 
											 	{
													DB.query(querInsert,[group_id,key,arrInsert[key]], function(data, error){
											 		 	if(parseInt(data.affectedRows) > 0){
									 						return_result(1);
												 	   	}else{
										   					return_result(0);
												 	   	}
											 	 	});
											  	};

						  				 	}, 100);
											
										
									    }).catch(function(err){
											
					    					return_result(0);

									    });

	 								}

							 	});

 		 					}

  				 	 	});

 		 			}

		 	   	}else{
   					return_result(0);
		 	   	}
	 	 	});


		}
		else if(parseInt(arrParams.to_account_id) !=0 && parseInt(arrParams.document_id) != 0 )
		{ 

			var query = `SELECT * FROM jaze_documents_activity WHERE id=?`;

			DB.query(query,[arrParams.document_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){


					var queryM = `SELECT * FROM jaze_documents_activity_meta WHERE group_id=?`;

					DB.query(queryM,[data[0].id], function(dataM, error){

						if(typeof dataM !== 'undefined' && dataM !== null && parseInt(dataM.length) > 0){


							methods.convertMultiMetaArray(dataM, function(retConvert){

								
								var title = retConvert[0].doc_title;
								var subject = retConvert[0].subject;

								var fullfile_name = data[0].doc_path.split("/");

								var filename_parts = fullfile_name[2].split(".");

								var storagePathCopy = G_STATIC_IMG_URL+"uploads/jazenet/documents/"+data[0].doc_path;

								var parsed = url.parse(storagePathCopy);
								var file_name = path.basename(parsed.pathname);

								var storagePathPaste = G_STATIC_IMG_PATH+'documents/'+arrParams.to_account_id+'/activity/';
									var createPath1 = G_STATIC_IMG_PATH+'documents/'+arrParams.to_account_id+'/';
									

								var dochPathToInsert = arrParams.to_account_id+'/activity/'+fullfile_name[2];


						      	if (!fs.existsSync(storagePathPaste)){
					            	fs.mkdirSync(createPath1,'0757', true);  
				            	 	if (!fs.existsSync(storagePathPaste)){
						            	fs.mkdirSync(storagePathPaste,'0757', true);  
							      	}
						      	}

				      	      	var options = {
						          	directory: storagePathPaste,
						          	filename: file_name
					      		};


				      	      	download(storagePathCopy, options, function(err){
			     
				      	  			console.log('error copying the file', err);

				      	      		if(!err){

								  		var mainQuer = `INSERT INTO jaze_documents_activity (account_id, doc_path, doc_exten, status) VALUES (?,?,?,'1')`;
							  			DB.query(mainQuer,[arrParams.account_id,dochPathToInsert,filename_parts[1]], function(data, error){
								 		 	if(parseInt(data.affectedRows) > 0){

								 		 		if(parseInt(arrParams.flag) == 2)
							 		 			{

													setTimeout(function(){
													
														var group_id = data.insertId;

														var arrInsert = {
															to_account_id  : arrParams.to_account_id,
															doc_title      : title,
															subject        : subject,
															send_type      : arrParams.flag,
															create_date    : newdateForm,
															status         : 1
														};


														var file_url = G_STATIC_IMG_URL+"uploads/jazenet/documents/"+arrParams.account_id+'/activity/'+file_name;

														var file_name_parts = file_name.split('.');

												 		var conversParam = {

										 		 			account_id    : arrParams.account_id,
										 		 			to_account_id : arrParams.to_account_id,
										 		 			file_url      : file_url,
										 		 			file_name     : file_name_parts[0],
										 		 			file_ext      : file_name_parts[1]
										 		 		}

														methods.getConversationDetails(conversParam,function(retConversation){

															setTimeout(function(){
													
														  		var querInsert = `insert into jaze_documents_activity_meta (group_id, meta_key, meta_value) VALUES (?,?,?)`;
															 	for (var key in arrInsert) 
															 	{
																	DB.query(querInsert,[group_id,key,arrInsert[key]], function(data, error){
															 		 	if(parseInt(data.affectedRows) > 0){
													 						return_result(1);
																 	   	}else{
														   					return_result(0);
																 	   	}
															 	 	});
															  	};

											  			 	}, 100);

													  	});

								  				 	}, 100);

							  				 	}
							  				 	else if(parseInt(arrParams.flag) == 1)
							  				 	{
							  				 		

							  				 		var group_id = data.insertId;

													var arrInsert = {
														to_account_id  : arrParams.to_account_id,
														doc_title      : title,
														subject        : subject,
														send_type      : arrParams.flag,
														create_date    : newdateForm,
														status         : 1
													};

													var file_url = G_STATIC_IMG_URL+"uploads/jazenet/documents/"+arrParams.account_id+'/activity/'+file_name;

													var file_name_parts = file_name.split('.');


													setTimeout(function(){

														methods.getEmailAccountDetails(arrParams, function(retSenderAccount){

								 		 					if(retSenderAccount!=null)
								 		 					{	
								 		 						var recParam = {account_id:arrParams.to_account_id};
																methods.getEmailAccountDetails(recParam, function(retReceiverAccount){

																	if(retReceiverAccount!=null)
								 		 							{

																		var atobconvert = atob(retSenderAccount.email_password);
																		//var password = "'" + atobconvert + "'";

																		var sendEmail = mailer.config({
																	  	    host: retSenderAccount.smtp_host,
																		    port: retSenderAccount.smtp_port,
																		    secure: true, // true for 465, false for other ports
																		    auth: {
																		      	user: retSenderAccount.email_account, // generated ethereal user
																		      	pass: atobconvert
																		      	  // generated ethereal password
																		    }
																		});

																		var obj_files = {filename:file_name,path:file_url};
																					
																		var message_arr = {
																	  	    from: retSenderAccount.email_account, // sender address
																	  	    to:retReceiverAccount.email_account,
																	  	    cc:'',
																	  	    bcc:'',
																	  	    replyTo:'',
																	  	    inReplyTo: '',
																	  	    references: '',
																		    subject: arrParams.subject, // Subject line
																		    //text: all_message, // plain text body
																		    html: arrParams.remarks, // html body
																	    	attachments:obj_files
																		};

																		var arrToEmail = [];

																		sendEmail(message_arr)
																	    .then(function(info){


																	    	arrToEmail.push(retReceiverAccount.email_account);

														    				var toCheckEmails = JSON.stringify(arrToEmail).replace(/[\[\]]+/g,"");
																			var arrAccountIdtoNoti = [];
																			var mail_push_notification = {};
													 	                   	var today = new Date();
													 	                   	var dateNow = dateFormat(today,'yyyy-mm-dd');

																			HelperJazeMail.checkIfinCredentials(toCheckEmails, function(retAccountIds){
										

																				mail_push_notification = {
																					group_id            : (retAccountIds[0].email_address+'|'+info.messageId+'|'+dateNow).toString(),
																					account_id          : arrParams.account_id.toString(),
																					request_account_id  : arrParams.to_account_id.toString(),
																					flag                : "12"
																				};


																				HelperNotification.sendPushNotification(mail_push_notification, function(returnNoti){});

																				HelperJazeMail.getReceiverList(arrParams, function(returnReceivers){

																					var toInsertEmail;
																					var toInsertGroupId;
																					var newFiltered;
																					var toRemove = '@jazenet.com';
																					var filtered,ToFilterEmails;

																					if(returnReceivers !=null)
																					{
																						if(returnReceivers[0].length > 0){

																							arrToEmail.forEach(function(row) {
																								if(!returnReceivers[0].includes(row)){
																									returnReceivers[0].push(row);
																								}
																						  	});

																							var ArrClean = returnReceivers[0].filter(v=>v!='');
																							toInsertEmail = JSON.stringify(ArrClean).replace(/[\[\]"]+/g,"");

																						}else{
																							toInsertEmail = JSON.stringify(retReceiverAccount.email_account).replace(/[\[\]"]+/g,"");
																						}

																					
																						toInsertGroupId = returnReceivers[1];

																						ToFilterEmails = toInsertEmail.split(",");

												
								    													filtered = ToFilterEmails.filter(function (str) { return str.indexOf(toRemove) === -1; });

																						newFiltered = JSON.stringify(filtered).replace(/[\[\]"]+/g,"");

																			

																						var arrUpdate = {
																							group_id   : toInsertGroupId,
																							account_id : arrParams.account_id,
																							email_id   : newFiltered
																						};

																						//console.log('update', arrUpdate);

																						HelperJazeMail.updateReceivers(arrUpdate, function(retUpdate){});

																					}else{

																						var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_email_accounts_ids";
																						var lastGrpId = 0;

																						HelperJazeMail.getLastGroupId(queGroupMax, function(queGrpRes){		

																							queGrpRes.forEach(function(row) {
																								lastGrpId = row.lastGrpId;
																					  		});

																							toInsertGroupId = lastGrpId+1;
																							toInsertEmail = retReceiverAccount.email_account;

																							ToFilterEmails = toInsertEmail.split(",");

																						
								    														filtered = ToFilterEmails.filter(function (str) { return str.indexOf(toRemove) === -1; });

																							newFiltered = JSON.stringify(filtered).replace(/[\[\]"]+/g,"");

																					

																							var arrInsert = {
																								group_id   : toInsertGroupId,
																								account_id : arrParams.account_id,
																								email_id   : newFiltered
																							};

																							//console.log('insert', arrInsert);

																							HelperJazeMail.insertReceivers(arrInsert, function(retInsert){});

																						});
																							
																					}

																				});

																			});


																			jazeComContactFunc(arrParams,arrToEmail,function(){});


																			var emailContent = {
																		  	    from: retSenderAccount.email_account, // sender address
																		    	to: retReceiverAccount.email_account,
																	    		cc: '',
																	    		bcc: '',
																			    subject: arrParams.subject, // Subject line
																			    body_mes: arrParams.remarks, // plain text body
																		     	message_id:info.messageId
																			};

																			copyToSent(retSenderAccount,emailContent,file_name,file_url, function(returnSent){});



								 											setTimeout(function(){
																
																				var group_id = data.insertId;

																				var arrInsert = {
																					to_account_id  : arrParams.to_account_id,
																					doc_title      : arrParams.title,
																					subject        : arrParams.subject,
																					send_type      : arrParams.flag,
																					create_date    : newdateForm,
																					status         : 1
																				};
																				
																
																		  		var querInsert = `insert into jaze_documents_activity_meta (group_id, meta_key, meta_value) VALUES (?,?,?)`;

																		  		var returnFunction;
																			 	for (var key in arrInsert) 
																			 	{
																					DB.query(querInsert,[group_id,key,arrInsert[key]], function(data, error){
																			 		 	if(parseInt(data.affectedRows) > 0){
																	 						return_result(1);
																				 	   	}else{
																		   					return_result(0);
																				 	   	}
																			 	 	});
																			  	};

														  				 	}, 100);
																			
																		
																	    }).catch(function(err){
																			
													    					return_result(0);

																	    });

									 								}

															 	});

								 		 					}

								  				 	 	});

									  			 	}, 100);

							  				 	}
							  				 	else
							  				 	{
							  				 		return_result(0);
							  				 	}

									 	   	}
									 	   	else
									 	   	{
							   					return_result(0);
									 	   	}
								 	 	});

				      	      		}else{
										return_result(0); 
				      	      		}
									 	      		
						      	});

						    });
					    }
					    else
					    {
					    	return_result(2);
					    }

				    });

				}
				else
				{
					return_result(2);
				}

			});
		}
		else
		{
			return_result(0); 
		}

	}
	catch(err)
	{ 
		console.log("sendDocument",err); return_result(0); 
	}	
};


methods.saveActivity = function(arrParams,return_result){

	try
	{
		if(parseInt(arrParams.to_account_id) !==0 && parseInt(arrParams.document_id) !==0 )
		{	
				
  				var query = `SELECT * FROM jaze_documents_activity WHERE id=?`;

			DB.query(query,[arrParams.document_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){


					var query = `SELECT * FROM jaze_documents_activity_meta WHERE group_id=?`;

					DB.query(query,[data[0].id], function(dataM, error){
						
						if(typeof dataM !== 'undefined' && dataM !== null && parseInt(dataM.length) > 0){

							methods.convertMultiMetaArray(dataM, function(retConverted){

								if(retConverted !== null)
								{
					
								var fullfile_name = data[0].doc_path.split("/");

								var filename_parts = fullfile_name[2].split(".");

								var title = retConverted[0].doc_title;

								var storagePathCopy = G_STATIC_IMG_URL+"uploads/jazenet/documents/"+data[0].doc_path;

								var parsed = url.parse(storagePathCopy);
								var file_name = path.basename(parsed.pathname);

								var storagePathPaste = G_STATIC_IMG_PATH+'documents/'+arrParams.account_id+'/library/';
									var createPath1 = G_STATIC_IMG_PATH+'documents/'+arrParams.account_id+'/';
								var dochPathToInsert = arrParams.account_id+'/library/'+fullfile_name[2];

						      	if (!fs.existsSync(storagePathPaste)){
									fs.mkdirSync(storagePathPaste,'0757', true);  
								}

				      	      	var options = {
						          	directory: storagePathPaste,
						          	filename: file_name
					      		};

				      	      	download(storagePathCopy, options, function(err){
			     
				      	  			console.log('error copying the file', err);

				      	      		if(!err){

										var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_documents_library";
							  			var lastGrpId = 0;

										methods.getLastGroupId(queGroupMax, function(queGrpRes){

										  	queGrpRes.forEach(function(row) {
												lastGrpId = row.lastGrpId;
										  	});

										  	var full_filename = arrParams.file_name+'.'+arrParams.file_ext;

											var arrInsert = {
												account_id  : arrParams.account_id,
												doc_exten   : filename_parts[1],
												doc_path    : dochPathToInsert,
												doc_title   : title,
												create_date : newdateForm,
												status:1
											};
										
											var plusOneGroupID = lastGrpId+1;
									  		var querInsert = `insert into jaze_documents_library (group_id, meta_key, meta_value) VALUES (?,?,?)`;

									  		var returnFunction;
										 	for (var key in arrInsert) 
										 	{
												DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
										 		 	if(parseInt(data.affectedRows) > 0){
								 						return_result(1);
											 	   	}else{
									   					return_result(0);
											 	   	}
										 	 	});
										  	};

										});

				      	      		}else{
										return_result(0); 
				      	      		}
									 	      		
								  });

								}
								else
								{
									return_result(2);
								}

						    });
					    }
					    else
					    {
					    	return_result(2);
					    }

				    });

				}
				else
				{
					return_result(2);
				}

			});

		}
		else
		{ 
			return_result(0); 
		}
	}
	catch(err)
	{ 
		console.log("saveActivity",err); return_result(0); 
	}	
};



methods.sendLibrary = function(arrParams,return_result){

	try
	{
		if(parseInt(arrParams.to_account_id) !==0 && parseInt(arrParams.library_id) !==0 )
		{	
				
  			var queLib = `SELECT * FROM jaze_documents_library WHERE group_id=? `;
  			DB.query(queLib,[arrParams.library_id], function(dataL, error){

				if(typeof dataL !== 'undefined' && dataL !== null && parseInt(dataL.length) > 0)
				{
									
					methods.convertMultiMetaArray(dataL, function(retConverted){

						var fullfile_name = retConverted[0].doc_path.split("/");
						var filename_parts = fullfile_name[2].split(".");

						var doc_title = retConverted[0].doc_title;

						//use actual url for downloading
						var storagePathCopy = G_STATIC_IMG_URL+"uploads/jazenet/documents/"+arrParams.account_id+'/library/'+filename_parts[0]+'.'+filename_parts[1];

						var parsed = url.parse(storagePathCopy);
						var file_name = path.basename(parsed.pathname);

						// use path for saving
						var storagePathPaste = G_STATIC_IMG_PATH+'documents/'+arrParams.to_account_id+'/activity/';

						var dochPathToInsert = arrParams.to_account_id+'/activity/'+fullfile_name[2];

				      	if (!fs.existsSync(storagePathPaste)){
			            	fs.mkdirSync(storagePathPaste,'0757', true);  
				      	}

		      	      	var options = {
				          	directory: storagePathPaste,
				          	filename: file_name
			      		};

		      	      	download(storagePathCopy, options, function(err){
	     
		      	  			console.log('error copying the file', err);

		      	      		if(!err)
		      	      		{

		      	      			// 1 mail, 2 chat
		      	      			if(parseInt(arrParams.flag) == 1)
		      	      			{
      			

      								setTimeout(function(){

										methods.getEmailAccountDetails(arrParams, function(retSenderAccount){

				 		 					if(retSenderAccount!=null)
				 		 					{	
				 		 						var recParam = {account_id:arrParams.to_account_id};
												methods.getEmailAccountDetails(recParam, function(retReceiverAccount){

													if(retReceiverAccount!=null)
				 		 							{

														var atobconvert = atob(retSenderAccount.email_password);
														//var password = "'" + atobconvert + "'";

														var sendEmail = mailer.config({
													  	    host: retSenderAccount.smtp_host,
														    port: retSenderAccount.smtp_port,
														    secure: true, // true for 465, false for other ports
														    auth: {
														      	user: retSenderAccount.email_account, // generated ethereal user
														      	pass: atobconvert
														      	  // generated ethereal password
														    }
														});

														var obj_files = {filename:fullfile_name[2],path:storagePathCopy};
																	
														var message_arr = {
													  	    from: retSenderAccount.email_account, // sender address
													  	    to:retReceiverAccount.email_account,
													  	    cc:'',
													  	    bcc:'',
													  	    replyTo:'',
													  	    inReplyTo: '',
													  	    references: '',
														    subject: arrParams.subject, // Subject line
														    //text: all_message, // plain text body
														    html: arrParams.remarks, // html body
													    	attachments:obj_files
														};

														var arrToEmail = [];

														sendEmail(message_arr)
													    .then(function(info){


													    	arrToEmail.push(retReceiverAccount.email_account);

										    				var toCheckEmails = JSON.stringify(arrToEmail).replace(/[\[\]]+/g,"");
															var arrAccountIdtoNoti = [];
															var mail_push_notification = {};
									 	                   	var today = new Date();
									 	                   	var dateNow = dateFormat(today,'yyyy-mm-dd');

															HelperJazeMail.checkIfinCredentials(toCheckEmails, function(retAccountIds){
						

																mail_push_notification = {
																	group_id            : (retAccountIds[0].email_address+'|'+info.messageId+'|'+dateNow).toString(),
																	account_id          : arrParams.account_id.toString(),
																	request_account_id  : arrParams.to_account_id.toString(),
																	flag                : "12"
																};


																HelperNotification.sendPushNotification(mail_push_notification, function(returnNoti){});

																HelperJazeMail.getReceiverList(arrParams, function(returnReceivers){

																	var toInsertEmail;
																	var toInsertGroupId;
																	var newFiltered;
																	var toRemove = '@jazenet.com';
																	var filtered,ToFilterEmails;

																	if(returnReceivers !=null)
																	{
																		if(returnReceivers[0].length > 0){

																			arrToEmail.forEach(function(row) {
																				if(!returnReceivers[0].includes(row)){
																					returnReceivers[0].push(row);
																				}
																		  	});

																			var ArrClean = returnReceivers[0].filter(v=>v!='');
																			toInsertEmail = JSON.stringify(ArrClean).replace(/[\[\]"]+/g,"");

																		}else{
																			toInsertEmail = JSON.stringify(retReceiverAccount.email_account).replace(/[\[\]"]+/g,"");
																		}

																	
																		toInsertGroupId = returnReceivers[1];

																		ToFilterEmails = toInsertEmail.split(",");

								
				    													filtered = ToFilterEmails.filter(function (str) { return str.indexOf(toRemove) === -1; });

																		newFiltered = JSON.stringify(filtered).replace(/[\[\]"]+/g,"");

															

																		var arrUpdate = {
																			group_id   : toInsertGroupId,
																			account_id : arrParams.account_id,
																			email_id   : newFiltered
																		};

																		//console.log('update', arrUpdate);

																		HelperJazeMail.updateReceivers(arrUpdate, function(retUpdate){});

																	}else{

																		var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_email_accounts_ids";
																		var lastGrpId = 0;

																		HelperJazeMail.getLastGroupId(queGroupMax, function(queGrpRes){		

																			queGrpRes.forEach(function(row) {
																				lastGrpId = row.lastGrpId;
																	  		});

																			toInsertGroupId = lastGrpId+1;
																			toInsertEmail = retReceiverAccount.email_account;

																			ToFilterEmails = toInsertEmail.split(",");

																		
				    														filtered = ToFilterEmails.filter(function (str) { return str.indexOf(toRemove) === -1; });

																			newFiltered = JSON.stringify(filtered).replace(/[\[\]"]+/g,"");

																	

																			var arrInsert = {
																				group_id   : toInsertGroupId,
																				account_id : arrParams.account_id,
																				email_id   : newFiltered
																			};

																			//console.log('insert', arrInsert);

																			HelperJazeMail.insertReceivers(arrInsert, function(retInsert){});

																		});
																			
																	}

																});

															});


															jazeComContactFunc(arrParams,arrToEmail,function(){});


															var emailContent = {
														  	    from: retSenderAccount.email_account, // sender address
														    	to: retReceiverAccount.email_account,
													    		cc: '',
													    		bcc: '',
															    subject: arrParams.subject, // Subject line
															    body_mes: arrParams.remarks, // plain text body
														     	message_id:info.messageId
															};

			

															copyToSent(retSenderAccount,emailContent,fullfile_name[2],storagePathCopy, function(returnSent){});



				 											setTimeout(function(){
												
														  		var mainQuer = `INSERT INTO jaze_documents_activity (account_id, doc_path, doc_exten, status) VALUES (?,?,?,'1')`;
													  			DB.query(mainQuer,[arrParams.account_id,dochPathToInsert,filename_parts[1]], function(data, error){
														 		 	if(parseInt(data.affectedRows) > 0){

																		setTimeout(function(){
																		
																			var group_id = data.insertId;

																			var arrInsert = {
																				to_account_id  : arrParams.to_account_id,
																				doc_title      : doc_title,
																				subject        : arrParams.subject,
																				send_type      : arrParams.flag,
																				create_date    : newdateForm
																			};
																		
																	  		var querInsert = `insert into jaze_documents_activity_meta (group_id, meta_key, meta_value) VALUES (?,?,?)`;
																		 	for (var key in arrInsert) 
																		 	{
																				DB.query(querInsert,[group_id,key,arrInsert[key]], function(data, error){
																		 		 	if(parseInt(data.affectedRows) > 0){
																 						return_result(1);
																			 	   	}else{
																	   					return_result(0);
																			 	   	}
																		 	 	});
																		  	};

													  				 	}, 100);

															 	   	}else{
													   					return_result(0);
															 	   	}
														 	 	});

										  				 	}, 100);
															
														
													    }).catch(function(err){
															
									    					return_result(0);

													    });

					 								}

											 	});

				 		 					}

				  				 	 	});

					  			 	}, 100);


		      	      			}
		      	      			else if(parseInt(arrParams.flag) == 2)
		      	      			{

									setTimeout(function(){
									
								 		var conversParam = {
						 		 			account_id    : arrParams.account_id,
						 		 			to_account_id : arrParams.to_account_id,
						 		 			file_url      : storagePathCopy,
						 		 			file_name     : filename_parts[0],
						 		 			file_ext      : filename_parts[1]
						 		 		}


										methods.getConversationDetails(conversParam,function(retConversation){
	 		 		
											setTimeout(function(){
									
										  		var mainQuer = `INSERT INTO jaze_documents_activity (account_id, doc_path, doc_exten, status) VALUES (?,?,?,'1')`;
									  			DB.query(mainQuer,[arrParams.account_id,dochPathToInsert,filename_parts[1]], function(data, error){
										 		 	if(parseInt(data.affectedRows) > 0){

														setTimeout(function(){
														
															var group_id = data.insertId;

															var arrInsert = {
																to_account_id  : arrParams.to_account_id,
																doc_title      : doc_title,
																subject        : arrParams.subject,
																send_type      : arrParams.flag,
																create_date    : newdateForm
															};
														
													  		var querInsert = `insert into jaze_documents_activity_meta (group_id, meta_key, meta_value) VALUES (?,?,?)`;
														 	for (var key in arrInsert) 
														 	{
																DB.query(querInsert,[group_id,key,arrInsert[key]], function(data, error){
														 		 	if(parseInt(data.affectedRows) > 0){
												 						return_result(1);
															 	   	}else{
													   					return_result(0);
															 	   	}
														 	 	});
														  	};

									  				 	}, 100);

											 	   	}else{
									   					return_result(0);
											 	   	}
										 	 	});

							  			 	}, 100);

									  	});

				  				 	}, 100);

		      	      			}


		      	      		}
		      	      		else
		      	      		{
								return_result(0); 
		      	      		}
							 	      		
				      	});


					});

				}
				else
				{
					return_result(2); 
				}

	 	 	});

		}
		else
		{ 
			return_result(0); 
		}
	}
	catch(err)
	{ 
		console.log("sendLibrary",err); return_result(0); 
	}	
};


methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
};

methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMultiMetaArray",err); return_result(null);}
};



function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].group_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}


methods.convertMultiMetaArrayCompany =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndexCompany(list,arrvalues[i].account_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMultiMetaArrayCompany",err); return_result(null);}
};


function __getGroupIndexCompany(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].account_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}



methods.convertMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			Object.assign(list, {group_id : arrvalues[0].group_id});

			for (var i in arrvalues) 
			{
				Object.assign(list, {[arrvalues[i].meta_key]:  arrvalues[i].meta_value});
			}

			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray : ",err); return_result(null);}
};

function sortingArrayObject(key, order='asc') {

	return function(a, b) {
	  if(!a.hasOwnProperty(key) || 
		 !b.hasOwnProperty(key)) {
		  return 0; 
	  }
	  
	  const varA = (typeof a[key] === 'string') ? 
		a[key].toUpperCase() : a[key];
	  const varB = (typeof b[key] === 'string') ? 
		b[key].toUpperCase() : b[key];
		
	  let comparison = 0;
	  if (varA > varB) {
		comparison = 1;
	  } else if (varA < varB) {
		comparison = -1;
	  }
	  return (
		(order == 'desc') ? 
		(comparison * -1) : comparison
	  );
	};
}


methods.getFileTypes = function(ext,return_result){

	var type;

	var arrImages = ['.jpeg','.jpg','.gif','.png','.tif'];
	var arrVideos = ['.mp4','.avi','.mov','.flv','.wmv'];
	var arrAudio = ['.pcm','.wav','.aiff','.mp3','.aac','.wma','.flac','.alac'];
	var arrDocs = ['.odt','.xls','.xlsx','.ods','.ppt','.pptx','.txt','.doc'];
	var arrPdf = ['.pdf'];

	if(arrImages.includes(ext.toLowerCase())){
		type = 2;
	}else if(arrAudio.includes(ext.toLowerCase())){
		type = 3;
	}else if(arrVideos.includes(ext.toLowerCase())){
		type = 4;
	}else if(arrDocs.includes(ext.toLowerCase())){
		type = 5;
	}else if(arrPdf.includes(ext.toLowerCase())){
		type = 6;
	}else{
		type = 7;
	}

	return_result(type);
};

function removeDuplicates(array) {
  return array.filter((a, b) => array.indexOf(a) === b)
};



//firestore checking conversation



methods.checkConversationList = function(arrParams,return_result){

	try
	{

		var query = `SELECT * FROM jaze_jazechat_conversation_list WHERE group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'account_id' AND (meta_value =? OR meta_value =?))
					AND group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'contact_account_id' AND (meta_value =? OR meta_value =?))`;
		DB.query(query,[arrParams.account_id,arrParams.to_account_id,arrParams.to_account_id,arrParams.account_id], function(data, error){

	
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				return_result(1);

			}else{

				var accParam = {account_id:arrParams.account_id};
				methods.getCompanyProfile(accParam,function(retSender){

					if(retSender!=null){

						accParam = {account_id:arrParams.to_account_id};
						methods.getCompanyProfile(accParam,function(retReceiver){

							if(retReceiver!=null){

								if(parseInt(retSender.account_type) === 1 && parseInt(retReceiver.account_type) === 1)
								{
									return_result(0);
								}
								else if(parseInt(retSender.account_type) === 1 && parseInt(retReceiver.account_type) !== 1)
								{
									return_result(1);
								}
								else if(parseInt(retSender.account_type) !== 1 && parseInt(retReceiver.account_type) === 1)
								{
									return_result(0);
								}
								else if(parseInt(retSender.account_type) !== 1 && parseInt(retReceiver.account_type) !== 1)
								{
									return_result(1);
								}
							}

						});

					}

				});

			}


		});

	}
	catch(err)
	{ console.log("checkConversationList",err); return_result(null);}

};

methods.getConversationDetails = function(arrParams,return_result){

	try
	{

		var query = `SELECT * FROM jaze_jazechat_conversation_list WHERE group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'account_id' AND (meta_value =? OR meta_value =?))
					AND group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'contact_account_id' AND (meta_value =? OR meta_value =?))`;
		DB.query(query,[arrParams.account_id,arrParams.to_account_id,arrParams.to_account_id,arrParams.account_id], function(data, error){

		

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){


		
					var conversParam = {
						account_id           : retConverted[0].account_id,
						account_type         : retConverted[0].account_type,
						request_account_id   : retConverted[0].contact_account_id,
						request_account_type : retConverted[0].contact_account_type,
						file_url             : arrParams.file_url
					};
					
					methods.getFileTypes('.'+arrParams.file_ext, function(returnType){


						fs_conversation.createNewConversationDocument(conversParam,retConverted[0].conversa_id,returnType,function(retStatus){
							if(parseInt(retStatus) !==0){
								return_result(1);
							}
					
						});


					});

				});

			}
			else
			{	

				var accParam = {account_id:arrParams.account_id};
				methods.getCompanyProfile(accParam,function(retSender){

					if(retSender!=null){

						accParam = {account_id:arrParams.to_account_id};
						methods.getCompanyProfile(accParam,function(retReceiver){

							if(retReceiver!=null){

								var conversParam = {
									account_id           : retSender.account_id,
									account_type         : retSender.account_type,
									request_account_id   : retReceiver.account_id,
									request_account_type : retReceiver.account_type,
									file_url             : arrParams.file_url
								};


				
									
								methods.getFileTypes('.'+arrParams.file_ext, function(returnType){

									methods.getLatestConversId(function(conversa_id) {


										var owner_count = "unread_count_"+conversParam.account_id;
										var request_count = "unread_count_"+conversParam.request_account_id;

										var arrComPro = [2,3];

										var status = '1';

										if(parseInt(retSender.account_type) === 1 && parseInt(retReceiver.account_type) === 1)
										{
											status = '0';
										}
										else if(parseInt(retSender.account_type) === 1 && parseInt(retReceiver.account_type) !== 1)
										{
											status = '1';
										}
										else if(parseInt(retSender.account_type) !== 1 && parseInt(retReceiver.account_type) === 1)
										{
											status = '0';
										}
										else if(parseInt(retSender.account_type) !== 1 && parseInt(retReceiver.account_type) !== 1)
										{
											status = '1';
										}

										var	arrConvInsert = {
											account_id: conversParam.account_id,
											account_type: conversParam.account_type,
											contact_account_id: conversParam.request_account_id,
											contact_account_type: conversParam.request_account_type,
											create_date: dateUTC,
											update_date: dateUTC,
											[owner_count]: '0',
											[request_count]: '0',
											conversa_id: conversa_id,
											status: status
										};


										var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazechat_conversation_list";
							  			var lastGrpId = 0;

										methods.getLastGroupId(queGroupMax, function(queGrpRes){

										  	queGrpRes.forEach(function(row) {
												lastGrpId = row.lastGrpId;
										  	});
								
											var plusOneGroupID = lastGrpId+1;

											methods.insertTBQuery('jaze_jazechat_conversation_list',plusOneGroupID, arrConvInsert, function (queResGrpId) {



												if (parseInt(queResGrpId) !== 0) {

													setTimeout(function(){ 

														fs_conversation.createNewConversation(conversParam,conversa_id,function(retNewConvers){

															if(parseInt(retNewConvers) !==0){
																setTimeout(function(){ 
																	fs_conversation.createNewConversationDocument(conversParam,conversa_id,returnType,function(retStatus){
																		if(parseInt(retStatus) !==0){
																			return_result(1);
																		}
																
																	});
																}, 150);

															}

														});

													}, 150);

												}

											});

										});


									});
								});

							}

						});

					}

				});

			}

		});

	}
	catch(err)
	{ console.log("getConversationDetails",err); return_result(null);}

};


         
methods.getLatestConversId = function(return_result){

	//check filed is exisit or not
	var query = "SELECT meta_value FROM jaze_jazechat_contact_list WHERE meta_key = 'last_convers_id' and  group_id = '0'";

	DB.query(query, null, function(lastdata, error){

		if (typeof lastdata[0] !== 'undefined' && lastdata[0] !== null)
		{
			var new_id = parseInt(lastdata[0].meta_value) + 1;
			//update last id
			var query = "UPDATE jaze_jazechat_contact_list SET meta_value = '"+new_id+"' WHERE meta_key = 'last_convers_id' and  group_id = '0' ";

			DB.query(query, null, function(data, error){
				return_result(new_id);
			});
		}
		else
		{
			var arrInsert = {
				last_convers_id: '1'
			};

			methods.insertTBQuery('jaze_jazechat_contact_list','0', arrInsert, function (queResGrpId) {

				return_result(arrInsert.last_convers_id);
			});
		}
	});
};


methods.getEmailAccountDetails = function(arrParams,return_result){

	try
	{
		var query = `SELECT * FROM jaze_email_accounts WHERE  group_id IN (SELECT group_id  FROM jaze_email_accounts WHERE meta_key = 'account_id' AND meta_value =?  )
			AND group_id IN (SELECT group_id  FROM jaze_email_accounts WHERE meta_key = 'mail_account_type' AND meta_value = '1' )
			AND group_id IN (SELECT group_id  FROM jaze_email_accounts WHERE meta_key = 'is_default' AND meta_value = '1' )`;


		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMetaArray(data, function(returnResult){	
				
					if(returnResult !== null)
					{
						var newObj = {
								mail_id		 	 : returnResult.group_id,
								mail_type		 : returnResult.mail_type,
								mail_account_type: returnResult.mail_type, 
								email_account	 : returnResult.email_id,
								email_password	 : returnResult.email_password,
								imap_host		 : returnResult.imap_host,
								imap_port		 : returnResult.imap_port,
								smtp_host		 : returnResult.smtp_name,
								smtp_port		 : returnResult.smtp_port,
								is_ssl			 : returnResult.is_ssl,
								account_id       : returnResult.account_id
							}
						
						return_result(newObj);
					}
					else 
					{ return_result(null); }
				});

			}else{
				return_result(null);
			}

		});
	}
	catch(err)
	{ console.log("getEmailAccountDetails",err); return_result(null); }	
};



let jazeComContactFunc = function(arrParams,arrMails){

	var toCheckEmails = JSON.stringify(arrMails).replace(/[\[\]]+/g,"");

	HelperJazeMail.checkContactsMail(arrParams, function(retMailContact){ 

		var arrAccountsFromContacts;
		var ArrAccountsFromCred;

		if(retMailContact!=null)
		{

		 	arrAccountsFromContacts = retMailContact.mail_account_id.split(",");
			//update
			HelperJazeMail.getAccountIDperEmail(toCheckEmails, function(retAccountIDs){ 

				if(retAccountIDs!=null)
				{	

					retAccountIDs = retAccountIDs.filter(val => !arrAccountsFromContacts.includes(val));
					allAccounts = arrAccountsFromContacts.concat(retAccountIDs);

					function removeDups(accounts) {
					  let unique = {};
					  accounts.forEach(function(i) {
					    if(!unique[i]) {
					      unique[i] = true;
					    }
					  });
					  return Object.keys(unique);
					}

					var AllAccConvert = removeDups(allAccounts.toString().split(","));

					var remove_last_coma = AllAccConvert.toString().replace(/,\s*$/, "");
					var	arrUpdate = {
						mail_account_id:remove_last_coma,
						create_date:standardDT
					};

					var updateQue = `update jaze_jazecom_contact_list SET meta_value =? WHERE meta_key =? AND group_id =?`;
					HelperJazeMail.updateQuery(updateQue,arrUpdate,retMailContact.group_id, function(updateStat){});
				
				}

			});

		}
		else
		{
			//insert
			HelperJazeMail.getAccountIDperEmail(toCheckEmails, function(retAccountIDs){ 
	
				if(retAccountIDs!=null){

					var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazecom_contact_list";
					var lastGrpId = 0;

					HelperJazeMail.getLastGroupId(queGroupMax, function(queGrpRes){

				  		queGrpRes.forEach(function(row) {
							lastGrpId = row.lastGrpId;
					  	});


		  		  		var	arrInsert = {
							account_id:arrParams.account_id,
							mail_account_id:retAccountIDs.toString(),
							call_account_id:'',
							chat_account_id:'',
							mail_blocked_account_id:'',
							chat_blocked_account_id:'',
							call_blocked_account_id:'',
							diary_blocked_account_id:'',
							diary_account_id:'',
							feed_blocked_account_id:'',
							feed_account_id:'',
							create_date:standardDT,
							status:1,
						};

				  		var plusOneGroupID = lastGrpId+1;

						var querInsert = `insert into jaze_jazecom_contact_list (group_id, meta_key, meta_value) VALUES (?,?,?)`;
						HelperJazeMail.insertResult(arrInsert,querInsert,plusOneGroupID, function(insertRes){});

					});
					
				}

			});

		}

	});

}

methods.insertTBQuery = function(tbName,grpId,arrInsert,return_result){

	try
	{
		for (var meta_key in arrInsert) 
		{
			var quer = "insert into "+tbName+" (group_id, meta_key, meta_value) VALUES ('"+grpId+"','"+meta_key+"','"+arrInsert[meta_key]+"');";
				DB.query(quer,null, function(data, error){
				if(data == null){
					return_result(0);
				}
			});
		};
		return_result(grpId);
	}
	catch(err)
	{
		console.log(err);
		return_result(0);
	}
}


let copyToSent = function(arrParams,emailContent,filenames,fileurls, return_result){



	var imap = new Imap({
	  	user: arrParams.email_account,
	  	password: atob(arrParams.email_password),
	  	host: arrParams.imap_host,
	  	port: arrParams.imap_port,
	  	tls: true
	});

	var msg, alternateEntity, htmlEntity, plainEntity, pngEntity;

	var mailDetails = {folder:'sent',mail_type:arrParams.mail_type};

	HelperJazeMail.getMailFolder(mailDetails, function(retFolder){

		if(retFolder!=null)
		{
			function openInbox(cb) {
			  	imap.openBox(retFolder, true, cb);
			}


		    imap.once('ready', function () {
		        openInbox(function (err, box) {
		        	if(!err){

				        msg = mimemessage.factory({
				          contentType: 'multipart/alternate',
				          body: []
				        });
			
				        plainEntity = mimemessage.factory({
			        	  contentType: 'text/html;charset=utf-8',
				          body: emailContent.body_mes
				        });

				        msg.header('Message-ID', emailContent.message_id);
				        msg.header('from', arrParams.email_account);
				        msg.header('to', emailContent.to);
			           	msg.header('cc', emailContent.cc);
			           	msg.header('bcc', emailContent.bcc);
				        msg.header('Subject', emailContent.subject);

				        msg.body.push(plainEntity);


						var source = G_STATIC_IMG_PATH+'documents/'+arrParams.account_id+'/activity/'+filenames;
		        	 

						var file_convert = fs.readFileSync(source);
						var contents = new Buffer(file_convert).toString('base64');

						pngEntity = mimemessage.factory({
						    contentType: mime.lookup(source),
						    contentTransferEncoding: 'base64',
						    body: contents
						});

						pngEntity.header('Content-Disposition', 'attachment ;filename='+filenames);

						msg.body.push(pngEntity);
			
						
				        imap.append(msg.toString());
				        return_result(1);
 
		        	}else{
	        			return_result(0);
		        	}
			
				});
			});


		    imap.once('error', function(err) {
      	        console.log('connection ' +err);
		    });

		    imap.once('end', function() {
		    	imap.end();
		      //  console.log('finished!');
		    });

		    imap.connect();

		}
		else
		{
			return_result(0);
		}

   	});



}




module.exports = methods;
