const methods = {};
const DB = require('../../helpers/db_base.js');

const fs = require('fs');
const path = require('path');
var promiseForeach = require('promise-foreach');
var str_replace = require('str_replace');
var rtrim = require('rtrim');
var ltrim = require('ltrim');


 	/*
    * get Specific Account Details
    */
methods.getAccountDetails = function(arrParams, return_result){

	if(parseInt(arrParams.account_id) !== 0)
		{

				var queAccountDetails = " SELECT *  FROM jaze_user_basic_details WHERE account_id = '" + arrParams.account_id + "' and account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key in ('status','team_status') and meta_value = '1') ";

					DB.query(queAccountDetails, null, function(data, error){
			
					if (typeof data[0] !== 'undefined' && data[0] !== null)
					{

						var list = [];
			
						Object.assign(list, {account_id : data[0].account_id});
						Object.assign(list, {account_type : data[0].account_type});


						for (var i in data) 
						{
							Object.assign(list, {[data[i].meta_key]:  data[i].meta_value});
						}

						if(parseInt(list.account_type) === 4)
						{
							var accParam = {account_id: list.team_company_id};
				
							methods.getAccountDetails(accParam, function(compAccRres){

								var accParam = {account_id: list.team_user_link_account_id};
				
								methods.getAccountDetails(accParam, function(profAccRres){

									methods.__getMobileCode(list, function(mobile_code){	

										var newObj = {
											account_id	  : list.account_id.toString(),
											account_type  : list.account_type.toString(),
											name		  : list.team_fname+' '+list.team_lname,
											fname		  : list.team_fname,
											lname		  : list.team_lname,
											dob			  : profAccRres.dob,
											logo		  : G_STATIC_IMG_URL + 'uploads/jazenet/'+list.team_display_pic,
											title		  : "work",
											category	  : list.team_position,
											company_id    : compAccRres.account_id,
											company_type  : compAccRres.account_type,
											company_name  : compAccRres.name,
											company_logo  : compAccRres.logo,
											email 		  : list.team_email.toString(),
											jazenet_id 	  : compAccRres.jazenet_id,
											mobile_code   : mobile_code.toString(),
											mobile_no     : list.team_mobile_no.toString(),
											country_id    : compAccRres.country_id.toString(),
											state_id      : compAccRres.state_id.toString(),
											city_id       : compAccRres.city_id.toString(),
											area_id       : compAccRres.area_id.toString(),
											country   	  : compAccRres.country,
											state	 	  : compAccRres.state,
											city		  : compAccRres.city,
											area		  : compAccRres.area,
											currency      : compAccRres.currency,
											currency_id   : compAccRres.currency_id,
											nationality_id : profAccRres.nationality_id.toString(),
											nationality   : profAccRres.nationality

										};
  
										return_result(newObj);
									});
								});
							});
						}
						else
						{
							methods.__getAccountEmailId(list.account_id, function(email_id){	
								
								methods.__getAccountJazenetId(list.account_id, function(jazenet_id){	
									
									methods.__getMobileCode(list, function(mobile_code){	
										
										methods.__getCompanyCategoryName(list, function(category){	
											
											methods.__getLocationDetails(list.area_id,list.city_id, function(location_details){		
												
												var nat_id = 0;
												if(parseInt(list.account_type) !== 3){
													nat_id = list.nationality_id;
												}

												methods.__getNationality(nat_id, function(nationality){		

																var cur_id = 0;
																var currenct_id = '';
																if(list.currency_id){
																	cur_id = list.currency_id;
																	currenct_id = list.currency_id.toString();
																}

																methods.__getCurrenctDetails(cur_id, function(currency){	

																	var default_logo = '' + G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png'+'';

																	var newObj = {};
																	
																	if(parseInt(list.account_type) === 1)
																	{
																		if(list.profile_logo !== "" && list.profile_logo !== null)
																		{
																			default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + list.profile_logo + '';
																		}


																		var mobile = "0";

																		if(parseInt(list.mobile) !== 0 && list.mobile  !== ""  && list.mobile  !== null)
																		{ mobile = list.mobile.toString();}

																		
																		newObj = {
																			account_id	  : list.account_id.toString(),
																			account_type  : list.account_type.toString(),
																			name		  : (list.fname +" "+list.lname).toString(),
																			fname		  : list.fname,
																			lname		  : list.lname,
																			profession		  : '',
																			dob			  : list.dob,
																			logo		  : default_logo.toString(),
																			title	  	  : category.title_name.toString(),
																			category	  : category.category_name.toString(),
																			email 		  : email_id.toString(),
																			jazenet_id 	  : jazenet_id.toString(),
																			mobile_code   : mobile_code.toString(),
																			mobile_no     : mobile,
																			country_id    : list.country_id.toString(),
																			state_id      : list.state_id.toString(),
																			city_id       : list.city_id.toString(),
																			area_id       : list.area_id,
																			country   	  : location_details.country_name,
																			state	 	  : location_details.state_name,
																			city		  : location_details.city_name,
																			area		  : location_details.area_name,
																			currency      : currency,
																			currency_id   : currenct_id,
																			nationality_id : nat_id.toString(),
																			nationality   : nationality,
																			category_id   : '',
																			is_merchant   : ''
																		};
																	}
																	else if(parseInt(list.account_type) === 2)
																	{
																		if(list.professional_logo !== "" && list.professional_logo !== null)
																		{
																			default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/professional_account/'+list.account_id+'/logo_thumb/thumb_' + list.professional_logo + '';
																		}
																		

																		newObj = {
																			account_id	  : list.account_id.toString(),
																			account_type  : list.account_type.toString(),
																			name		  : (list.fname +" "+list.lname).toString(),
																			fname		  : list.fname,
																			lname		  : list.lname,
																			profession	  : list.profession,
																			dob			  : list.dob,
																			logo		  : default_logo.toString(),
																			title	  	  : category.title_name.toString(),
																			category	  : category.category_name.toString(),
																			email 		  : email_id.toString(),
																			jazenet_id 	  : jazenet_id.toString(),
																			mobile_code   : mobile_code.toString(),
																			mobile_no     : list.mobile.toString(),
																			country_id    : list.country_id.toString(),
																			state_id      : list.state_id.toString(),
																			city_id       : list.city_id.toString(),
																			area_id       : list.area_id,
																			country   	  : location_details.country_name,
																			state	 	  : location_details.state_name,
																			city		  : location_details.city_name,
																			area		  : location_details.area_name,
																			currency      : currency,
																			currency_id   : currenct_id,
																			nationality_id : nat_id.toString(),
																			nationality   : nationality,
																			category_id   : '',
																			is_merchant   : ''
																		};
																	}
																	else if(parseInt(list.account_type) === 3 || parseInt(list.account_type) === 5)
																	{
																		var default_logo = "";
																		
																		if(list.company_logo !== "" && list.company_logo !== null)
																		{
																			default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+list.account_id+'/logo/' + list.company_logo + '';
																		}
																		else if(typeof list.listing_images !== "undefined" && list.listing_images !== null)
																		{
																			default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+list.account_id+'/logo/' + list.listing_images + '';
																		}
																		var category_id = '';
																		if(list.category_id){
																			category_id = list.category_id;
																		}														

																		var is_merchant = '';
																		if(list.is_merchant_type){is_merchant=list.is_merchant_type;}	

																		newObj = {
																			account_id	  : list.account_id.toString(),
																			account_type  : list.account_type.toString(),
																			name		  : list.company_name.toString(),
																			fname		  : '',
																			lname		  : '',
																			profession    : '',
																			dob			  : '',
																			logo		  : default_logo,
																			title	  	  : category.title_name.toString(),
																			category	  : category.category_name.toString(),
																			email 		  : email_id.toString(),
																			jazenet_id 	  : jazenet_id.toString(),
																			mobile_code   : mobile_code.toString(),
																			mobile_no     : list.company_phone.toString(),
																			country_id    : list.country_id.toString(),
																			state_id      : list.state_id.toString(),
																			city_id       : list.city_id.toString(),
																			area_id       : list.area_id,
																			country   	  : location_details.country_name,
																			state	 	  : location_details.state_name,
																			city		  : location_details.city_name,
																			area		  : location_details.area_name,
																			currency      : currency,
																			currency_id   : currenct_id,
																			nationality_id : "0",
																			nationality   : nationality,
																			category_id   : category_id,
																			is_merchant   : is_merchant
																		};
																	}
														
																	return_result(newObj);				
																		
																});
												});
											});
										});
									});
								});
							});
						}

					} else{
					return_result(null);
					}
				});
	}
	else{
		return_result(null);
	}
};

methods.getAccountTypeDetails = function(account_id, return_result){

	if(parseInt(account_id) !== 0)
	{
		//get Account type
		var queryAccountType = "SELECT `account_type` FROM `jaze_user_basic_details` WHERE `account_id` = '" + account_id + "' ";
		console.log('getAccountTypeDetails',account_id);
		DB.query(queryAccountType, null, function(dataType, error){

			if (typeof dataType[0] !== 'undefined' && dataType[0] !== null)
			{
				return_result(dataType[0].account_type);
			}
			else
			{ return_result('0');}
		});
	}
	else
	{ return_result('0');}
};

methods.getAccountCategoryDetails = function(account_id, return_result){

	var return_obj = {
		title_name 	  : "",
		category_name : ""
	};

	try
	{
		if(parseInt(account_id) !== 0)
		{
			//get Account type
			var queryAccountType = "SELECT account_id,account_type,meta_key,meta_value from jaze_user_basic_details where meta_key in ('status','category_id') and   account_id =  '" + account_id + "' ";

			DB.query(queryAccountType, null, function(dataType, error){

				if (typeof dataType[0] !== 'undefined' && dataType[0] !== null)
				{
					methods.convertMetaArray(dataType, function(retrnResult){

						 methods.__getCompanyCategoryName(retrnResult, function(category){	
							return_result(category);
						});
					});
				}
				else
				{ return_result(return_obj); }
			});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{ console.log("getAccountCategoryDetails",err); return_result(return_obj); }	
};

methods.getApplications = function(arrParams,return_result){


	var arrApp = [];
	if(arrParams.flag == 1){
		subpath = 'jazechat/conversation';
	}else if(arrParams.flag == 2){
		subpath = 'jazechat/groups';
	}else if(arrParams.flag == 4){
		subpath = 'jazefeed';
	}

	var rootPathConv = G_STATIC_IMG_PATH+subpath+'/'+arrParams.conversation_id+'/';

	if(arrParams.item != ''){
		if(arrParams.item.includes('application')){

			arrParams.item.forEach(function (items) {

				if(items == 'application')
				{

					function asyncFunction (returnItem, cb) {
					  	setTimeout(() => {
					  	
							return_result(returnItem);
						cb();
					  	}, 100);
					}
					rootPathConv = path.join(G_STATIC_IMG_PATH, subpath+'/'+arrParams.conversation_id+'/'+items);
				
					fs.readdir(rootPathConv, function (err, files) {

						let requests = files.map((val, key, array) => {

							var ext = path.extname(rootPathConv+'/'+val);
	
							return new Promise((resolve) => {
								methods.getFileTypes(ext, function(reType){
									var objApp = {
				 		   				url: G_STATIC_IMG_URL+'uploads/jazenet/'+subpath+'/'+arrParams.conversation_id+'/'+items+'/'+val,
			 		   					ext: ext.replace('.',''),
	 		  							type: reType
				 		  			};
				 		  			arrApp.push(objApp);
						      		asyncFunction(arrApp, resolve);
								});
							});
						})

						Promise.all(requests);
					});
				}

			});
		
		}else{
			return_result(null);
		}
	}else{
		return_result(null);
	}
};




methods.getMedia = function(arrParams,return_result){


	var arrApp = [];
	var subpath = '';
	if(arrParams.flag == 1){
		subpath = 'jazechat/conversation';
	}else if(arrParams.flag == 2){
		subpath = 'jazechat/groups';
	}else if(arrParams.flag == 4){
		subpath = 'jazefeed';
	}	

	if(arrParams.item != '')
	{
		var filteredAry = arrParams.item.filter(e => e !== 'application');

		var rootPathConv = G_STATIC_IMG_PATH+subpath+'/'+arrParams.conversation_id+'/';

		if(filteredAry != ''){

			filteredAry.forEach(function (items, index, arr) {

				function asyncFunction (returnItem, cb) {
				  	setTimeout(() => {
						return_result(returnItem);
					cb();
				  	}, 100);
				}

				rootPathConv = path.join(G_STATIC_IMG_PATH, subpath+'/'+arrParams.conversation_id+'/'+items);
			
			
				fs.readdir(rootPathConv, function (err, files) {

					
					if (typeof files !== 'undefined' && files !== null)
					{

						let requests = files.map((val, key, array) => {
							var ext = path.extname(rootPathConv+'/'+val);
							return new Promise((resolve) => {
								methods.getFileTypes(ext, function(reType){
									var objApp = {
										   url: G_STATIC_IMG_URL+'uploads/jazenet/'+subpath+'/'+arrParams.conversation_id+'/'+items+'/'+val,
										   ext: ext.replace('.',''),
										  type: reType
									   };
									   arrApp.push(objApp);
									  asyncFunction(arrApp, resolve);
								});
							});
						})
						Promise.all(requests);
					}
					else
					{	return_result(null);}

				});
			
			});
		}else{
			return_result(null);
		}
	}else{
		return_result(null);
	}
};


methods.getFileTypes = function(ext,return_result){

	var type;

	var arrImages = ['.jpeg','.jpg','.gif','.png','.tif'];
	var arrVideos = ['.mp4','.avi','.mov','.flv','.wmv'];
	var arrAudio = ['.pcm','.wav','.aiff','.mp3','.aac','.wma','.flac','.alac'];
	var arrDocs = ['.odt','.xls','.xlsx','.ods','.ppt','.pptx','.txt','.doc'];
	var arrPdf = ['.pdf'];

	if(arrImages.includes(ext.toLowerCase())){
		type = '2';
	}else if(arrAudio.includes(ext.toLowerCase())){
		type = '3';
	}else if(arrVideos.includes(ext.toLowerCase())){
		type = '4';
	}else if(arrDocs.includes(ext.toLowerCase())){
		type = '5';
	}else if(arrPdf.includes(ext.toLowerCase())){
		type = '6';
	}else{
		type = '7';
	}

	return_result(type);
};

//Get Mobile Code
methods.__getMobileCode= function (account_list,return_result){
	try
	{
		if(account_list !== null)
		{
			if(parseInt(account_list.account_type) === 1 || parseInt(account_list.account_type) === 2)
			{
				var query = " SELECT `phonecode` FROM `country` WHERE `id` = '"+account_list.mobile_country_code_id+"'; ";
		
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						return_result("+"+data[0].phonecode);
					}
					else
					{ return_result(''); }
				});
			}
			else if(parseInt(account_list.account_type) === 3)
			{
				if(parseInt(account_list.company_phone_code) !== 0)
				{
					var query = " SELECT `phonecode` FROM `country` WHERE `id` = '"+account_list.company_phone_code+"'; ";
				
					DB.query(query, null, function(data, error){
						
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_result("+"+data[0].phonecode);
						}
						else
						{ return_result(''); }
					});
				}
				else
				{
					return_result('');
				}
			}
			else if(parseInt(account_list.account_type) === 4)
			{
				if(parseInt(account_list.team_mobile_code) !== 0)
				{
					var query = " SELECT `phonecode` FROM `country` WHERE `id` = '"+account_list.team_mobile_code+"'; ";
				
					DB.query(query, null, function(data, error){
						
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_result("+"+data[0].phonecode);
						}
						else
						{ return_result(''); }
					});
				}
				else
				{
					return_result('');
				}
			}
			else
			{
				return_result('');
			}
			
		}
		else
		{
			return_result('');
		}
	}
	catch(err)
	{ console.log("getMobileCode",err); return_result(''); }	
};

//Get Email id
methods.__getAccountEmailId = function (account_id,return_result){
	try
	{
		if(parseInt(account_id) !== 0)
		{
			var query = " SELECT `jazemail_id` FROM `jaze_user_credential_details` WHERE `account_id` = '"+account_id+"' and status = '1'; ";
		
			DB.query(query, null, function(data, error){
				
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result(data[0].jazemail_id);
				}
				else
				{ return_result(''); }
			});
		}
		else
		{
			return_result('');
		}
	}
	catch(err)
	{ console.log("getAccountEmailId",err); return_result(''); }	
};

//Get Jazenet id
methods.__getAccountJazenetId = function (account_id,return_result){
	try
	{
		if(parseInt(account_id) !== 0)
		{
			var query = " SELECT concat(`jazenet_id`,`unique_id`) as id FROM `jaze_user_jazenet_id` WHERE `account_id` = '"+account_id+"' and status = '1'; ";
		
			DB.query(query, null, function(data, error){
				
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result(data[0].id);
				}
				else
				{ return_result(''); }
			});
		}
		else
		{
			return_result('');
		}
	}
	catch(err)
	{ console.log("getAccountEmailId",err); return_result(''); }	
};

//Get Company id
methods.__getCompanyCategoryName =  function (account_list,return_result){
	
	var return_obj = {
			account_type  : "",
			title_name 	  : "",
			category_name : ""
		};

	try
	{
		if(account_list !== null)
		{
			
			if(parseInt(account_list.account_type) === 1)
			{
				return_obj = {
					account_type  : "1",
					title_name 	  : "personal",
					category_name : "personal"
				};

				return_result(return_obj);
			}
			else if(parseInt(account_list.account_type) === 2)
			{
				return_obj = {
					account_type  : "2",
					title_name 	  : "professional",
					category_name : "professional"
				};

				return_result(return_obj);
			}
			else if(parseInt(account_list.account_type) === 3)
			{
				if((Object.keys(account_list)).indexOf("category_id") !== -1)
				{
					var query = "  SELECT meta_value  FROM jaze_category_list WHERE  meta_key = 'cate_name' and group_id = '"+account_list.category_id+"'; ";
					
					DB.query(query, null, function(data, error){
					
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_obj = {
								account_type  : "3",
								title_name 	  : "company",
								category_name : data[0].meta_value
							};

							return_result(return_obj);
						}
						else
						{
							return_obj = {
								account_type  : "3",
								title_name 	  : "company",
								category_name : "company"
							};

							return_result(return_obj);
						}
					});
				}
				else
				{
					return_obj = {
						account_type  : "3",
						title_name 	  : "company",
						category_name : "company"
					};

					return_result(return_obj);
				}
			}
			else if(parseInt(account_list.account_type) === 5)
			{
				return_obj = {
					account_type  : "5",
					title_name 	  : "government",
					category_name : "government"
				};

				return_result(return_obj);
			}
		}
		else
		{
			return_result(return_obj);
		}
	}
	catch(err)
	{ console.log("getAccountEmailId",err); return_result(return_obj); }	
};

//Get Company id
methods.getAccountCategoryName = function (account_id,return_result){
	
	var return_obj = {
			account_type  : "",
			title_name 	  : "",
			category_name : ""
		};

	try
	{
		if(parseInt(account_id) !== 0)
		{
			
			//get Account type

			var queryAccountType  = " SELECT * FROM jaze_user_basic_details WHERE meta_key IN ('category_id','fname','company_name') ";
				queryAccountType += " and account_id IN (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status' AND meta_value = '1' and account_id = '" + account_id + "' ) ";

			DB.query(queryAccountType, null, function(dataType, error){

				if (typeof dataType[0] !== 'undefined' && dataType[0] !== null)
				{
					var data_result  = dataType[0];

					if(parseInt(data_result.account_type) === 1)
					{
						return_obj = {
							account_type  : data_result.account_type.toString(),
							title_name 	  : "personal",
							category_name : "personal"
						};

						return_result(return_obj);
					}
					else if(parseInt(data_result.account_type) === 2)
					{
						return_obj = {
							account_type  : data_result.account_type.toString(),	
							title_name 	  : "professional",
							category_name : "professional"
						};

						return_result(return_obj);
					}
					else if(parseInt(data_result.account_type) === 3)
					{

						if((Object.keys(data_result)).indexOf("category_id") !== -1)
						{
							var query = "  SELECT meta_value  FROM jaze_category_list WHERE  meta_key = 'cate_name' and group_id = '"+data_result.category_id+"'; ";
							
							DB.query(query, null, function(data, error){
								
								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
									return_obj = {
										account_type  : data_result.account_type.toString(),
										title_name 	  : "company",
										category_name : data[0].meta_value
									};

									return_result(return_obj);
								}
							});
						}
						else
						{
							return_obj = {
								account_type  : data_result.account_type.toString(),
								title_name 	  : "company",
								category_name : "company"
							};

							return_result(return_obj);
						}
					}
				}
			});
		}
		else
		{
			return_result(return_obj);
		}
	}
	catch(err)
	{ console.log("getAccountCategoryName",err); return_result(return_obj); }	
};



// ------------------------------------------------------ General Search Module Start  --------------------------------------------------------------//   

methods.getAutofillUserResults = function(arrParams,count,blockAccounts,return_result)
{
	var return_obj = {
		result : null,
		tot_count : "0",
		tot_page : "0"
	};

	try
	{
		if(arrParams !== null && parseInt(count) === 2 && (parseInt(arrParams.result_flag) === 0 || parseInt(arrParams.result_flag) === 1))
		{	 
				var block_list = arrParams.account_id;

				if(parseInt(arrParams.flag) !== 3)
				{
					if(blockAccounts !== null)
					{ if(blockAccounts.chat_bloack !== null)
						{ block_list = blockAccounts.chat_bloack }
					}
				}

				var query  = " SELECT * FROM jaze_user_basic_details WHERE account_id not in ("+block_list+") and account_type = 1 and  meta_key IN ('fname','lname','profile_logo') ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('fname','lname')  AND LOWER(meta_value) REGEXP '^("+str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())))+")' ";
					//query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('fname','lname')  AND LOWER(meta_value) REGEXP '[[:<:]]"+arrParams.keyword.toLowerCase()+"'  ";
					
					query += " )) ";
	
					query += " ORDER BY (meta_value AND meta_key = 'fname') asc "

					DB.query(query, null, function (data, error) {
	
						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
						{
							methods.convertMultiMetaArray(data, function(returnRes){
							
								if(returnRes !== null)
								{
									var data_result = [];
	
									var count_page = 5;

									if(arrParams.api_token === "jazenet")
									{ count_page = 10; }
	
									var limit = 50;
	
									if(parseInt(arrParams.limit) === 0)
									{
										data_result = returnRes.slice(0,count_page);
									}
									else if(parseInt(arrParams.limit) === 1)
									{
										data_result = returnRes.slice(count_page,limit)
									}
									else
									{
										var start = parseInt(arrParams.limit - 1) * parseInt(limit);
	
										var end = parseInt(arrParams.limit) * parseInt(limit);
	
										data_result = returnRes.slice(parseInt(start),parseInt(end));
									}
									
	
									if (typeof data_result !== 'undefined' && data_result !== null && parseInt(data_result.length) !== 0)
									{
										var counter = data_result.length;
										
										var array_rows = [];

										if(parseInt(counter) !== 0)
										{
											promiseForeach.each(data_result,[function (list) {
				
												return new Promise(function (resolve, reject) {
													
													methods.__getAccountEmailId(list.account_id, function(email_id){	
														
														var default_logo = '' + G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png'+'';

														if(list.profile_logo !== "" && list.profile_logo !== null)
														{
															default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + list.profile_logo + '';
														}
		
															var chat_request_status	 = "0";

															if(blockAccounts !== null)
															{
																if(blockAccounts.chat_pending !== null)
																{
																	if(blockAccounts.chat_pending.indexOf(list.account_id) != -1 ){
																	chat_request_status	 = "1";
																	}
																}
															}

															var newObj = {
																account_id	  : list.account_id.toString(),
																account_type  : list.account_type.toString(),
																name		  : (list.fname +" "+ list.lname).toString(),
																logo		  : default_logo.toString(),
																email		  : email_id.toString(),
																title	  	  : "personal",
																category	  : "personal",
																chat_request_status : chat_request_status
															};
															
															array_rows.push(newObj);
														
															counter -= 1;
														
															resolve(array_rows);
													});
												})
											}],
											function (result, current) {
												
												if (counter === 0)
												{  
													return_obj = {
														result 	  : result[0],
														tot_count : returnRes.length.toString(),
														tot_page  : Math.ceil((returnRes.length.toString() - 5) / 50)
													};
													return_result(return_obj);
												}
											});
										}
										else
										{ return_result(return_obj); }
									}
									else
									{ return_result(return_obj); }
								}
								else
								{ return_result(return_obj); }
							});
						}
						else
						{ return_result(return_obj); }
					});	
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getAutofillProfessionalResults = function(arrParams,count,blockAccounts,return_result)
{
	var return_obj = {
		result : null,
		tot_count : "0",
		tot_page : "0"
	};

	try
	{
		if(arrParams !== null && parseInt(count) === 3 && (parseInt(arrParams.result_flag) === 0 || parseInt(arrParams.result_flag) === 2))
		{	
				var block_list = arrParams.account_id;

				if(parseInt(arrParams.flag) !== 3)
				{
					if(blockAccounts !== null)
					{ if(blockAccounts.chat_bloack !== null)
						{ block_list = blockAccounts.chat_bloack }
					}
				}

			var query  = " SELECT account_id FROM jaze_user_basic_details WHERE account_id not in ("+block_list+") and account_type = 2 ";
				query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";
				//query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('fname','lname')  AND LOWER(meta_value) REGEXP '[[:<:]]"+arrParams.keyword.toLowerCase()+"'  ";
				query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('fname','lname')  AND LOWER(meta_value) REGEXP '^("+str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())))+")' ";
			

				if(parseInt(arrParams.country) !== 0)
				{
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
				}
				if(parseInt(arrParams.state) !== 0)
				{
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state+"') ";
				}
				if(parseInt(arrParams.city) !== 0)
				{
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";
				}

				query += " )) ";

				query += " group by account_id ORDER BY  CASE WHEN meta_key = 'fname' THEN meta_value END desc ";

				
				if(arrParams.api_token === "jazenet")
				{ 
					query += " limit 0,10  "
				}
				else
				{
					if(parseInt(arrParams.limit) === 0)
					{
						query += "  LIMIT  0 , 5 ";	
					}
					else if(parseInt(arrParams.limit) === 1)
					{
						query += "  LIMIT  5 , 50 ";
					}
					else
					{
						var start = parseInt(arrParams.limit - 1) * 50;

						var end = parseInt(arrParams.limit) * 50;

						query += "  LIMIT  "+start+" , "+end;
					}
				}
	
				DB.query(query, null, function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{

						var account_id_id_list = "";

						data.forEach(function(row){
							account_id_id_list += (account_id_id_list  == "") ? row.account_id: ','+row.account_id;	
						});

						query  = " SELECT * FROM jaze_user_basic_details WHERE account_id  in ("+account_id_id_list+") and  meta_key IN ('fname','lname','professional_logo') ";

						DB.query(query, null, function (data, error) {

							if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
							{
								methods.convertMultiMetaArray(data, function(data_result){

									if(data_result !== null)
									{
											var counter = data_result.length;
		
											var array_rows = [];
		
											if(parseInt(counter) !== 0)
											{
												promiseForeach.each(data_result,[function (list) {
					
													return new Promise(function (resolve, reject) {
		
														methods.__getAccountEmailId(list.account_id, function(email_id){	
		
															var default_logo = '' + G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png'+'';
		
															if(list.professional_logo !== "" && list.professional_logo !== null)
															{
																default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/professional_account/'+list.account_id+'/logo_thumb/thumb_' + list.professional_logo + '';
															}
		
																	var chat_request_status	 = "0";
		
																	if(blockAccounts !== null)
																	{
																		if(blockAccounts.chat_pending !== null)
																		{
																			if(blockAccounts.chat_pending.indexOf(list.account_id) != -1 ){
																			chat_request_status	 = "1";
																			}
																		}
																	}
		
																var newObj = {
																	account_id	  : list.account_id.toString(),
																	account_type  : list.account_type.toString(),
																	name		  : (list.fname +" "+ list.lname).toString(),
																	logo		  : default_logo.toString(),
																	email		  : email_id.toString(),
																	title	  	  : "professional",
																	category	  : "professional",
																	chat_request_status : chat_request_status
																};
		
																array_rows.push(newObj);
															
																counter -= 1;
															
																resolve(array_rows);
														});
													})
												}],
												function (result, current) {
													if (counter === 0)
													{  
														methods.getCountProfessionalResults(arrParams, blockAccounts,function(returnTotalCount){

															var obj = {
																result : result[0],
																tot_count : returnTotalCount.toString(),
																tot_page  : Math.ceil((parseInt(returnTotalCount) - 5) / 50)
															}
															return_result(obj); 
														});
													}
												});
											}
											else
											{ return_result(return_obj); }
									}
									else
									{ return_result(return_obj); }
								});
							}
						});
					}
					else
					{ return_result(return_obj); }
				});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getCountProfessionalResults = function(arrParams,blockAccounts,return_result){

	try{
		
		var block_list = arrParams.account_id;

			if(parseInt(arrParams.flag) !== 3)
			{
				if(blockAccounts !== null)
				{ if(blockAccounts.chat_bloack !== null)
					{ block_list = blockAccounts.chat_bloack }
				}
			}

					var query  = " SELECT account_id FROM jaze_user_basic_details WHERE account_id not in ("+block_list+") and account_type = 2 ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";
					//query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('fname','lname')  AND LOWER(meta_value) REGEXP '[[:<:]]"+arrParams.keyword.toLowerCase()+"'  ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('fname','lname')  AND LOWER(meta_value) REGEXP '^("+str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())))+")' ";

					if(parseInt(arrParams.country) !== 0)
					{
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
					}
					if(parseInt(arrParams.state) !== 0)
					{
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state+"') ";
					}
					if(parseInt(arrParams.city) !== 0)
					{
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";
					}

					query += " )) ";

					query += " group by account_id  ";

		
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data.length);
					}
					else
					{return_result(0)}
				});
	}
	catch(err)
	{
		return_result(null)
	}
};

methods.getAutofillCompanyResults = function(arrParams,count,blockAccounts,return_result)
{
	var return_obj = {
		result : null,
		tot_count : "0",
		tot_page : "0"
	};

	try
	{
		if(arrParams !== null && parseInt(count) === 4 && (parseInt(arrParams.result_flag) === 0 || parseInt(arrParams.result_flag) === 3))
		{	
				var block_list = arrParams.account_id;

				if(parseInt(arrParams.flag) !== 3)
				{
					if(blockAccounts !== null)
					{ if(blockAccounts.chat_bloack !== null)
						{ block_list = blockAccounts.chat_bloack }
					}
				}

				var query  = " SELECT account_id FROM jaze_user_basic_details WHERE account_id not in ("+block_list+") and account_type = 3  ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";
					//query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('company_name')  AND LOWER(meta_value) REGEXP '[[:<:]]"+arrParams.keyword.toLowerCase()+"' ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('company_name')  AND LOWER(meta_value) REGEXP '^("+str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())))+")' ";
					
					if(parseInt(arrParams.country) !== 0)
					{
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
					}
					if(parseInt(arrParams.state) !== 0)
					{
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state+"') ";
					}
					if(parseInt(arrParams.city) !== 0)
					{
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";
					}

					query += " )) ";
				//	query += " GROUP BY account_id ORDER BY instr(meta_value AND meta_key = 'company_name','"+arrParams.keyword+"') desc  "

					query += " group by account_id ORDER BY  CASE WHEN meta_key = 'company_name' THEN meta_value END desc ";

					if(arrParams.api_token === "jazenet")
					{ 
						query += " limit 0,10  "
					}
					else
					{
						if(parseInt(arrParams.limit) === 0)
						{
							query += "  LIMIT  0 , 5 ";	
						}
						else if(parseInt(arrParams.limit) === 1)
						{
							query += "  LIMIT  5 , 50 ";
						}
						else
						{
							var start = parseInt(arrParams.limit - 1) * 50;

							var end = parseInt(arrParams.limit) * 50;

							query += "  LIMIT  "+start+" , "+end;
						}
					}
					
					DB.query(query, null, function (data, error) {

						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
						{
							var account_id_id_list = "";

							data.forEach(function(row){
								account_id_id_list += (account_id_id_list  == "") ? row.account_id: ','+row.account_id;
								
							});

							query  = " SELECT * FROM jaze_user_basic_details WHERE account_id  in ("+account_id_id_list+") and  meta_key IN ('company_name','company_logo','category_id') ";

							DB.query(query, null, function (data, error) {

								if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
								{
									methods.convertMultiMetaArray(data, function(data_result){

										if(data_result !== null)
										{
											var counter = data_result.length;

												var array_rows = [];

												if(parseInt(counter) !== 0)
												{
													promiseForeach.each(data_result,[function (list) {
						
														return new Promise(function (resolve, reject) {

															methods.__getCompanyCategoryName(list, function(category){	

																methods.__getAccountEmailId(list.account_id, function(email_id){	

																var default_logo = '' + G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png'+'';

																if(list.company_logo !== "" && list.company_logo !== null)
																{
																	default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+list.account_id+'/logo/' + list.company_logo + '';
																}

																	var chat_request_status	 = "0";

																	if(blockAccounts !== null)
																	{
																		if(blockAccounts.chat_pending !== null)
																		{
																			if(blockAccounts.chat_pending.indexOf(list.account_id) != -1 ){
																			chat_request_status	 = "1";
																			}
																		}
																	}

																	var newObj = {
																		account_id	  : list.account_id.toString(),
																		account_type  : list.account_type.toString(),
																		name		  : list.company_name.toString(),
																		logo		  : default_logo.toString(),
																		email		  : email_id.toString(),
																		title	  	  : category.title_name.toString(),
																		category	  : category.category_name.toString(),
																		chat_request_status : chat_request_status
																	};

																	array_rows.push(newObj);
																
																	counter -= 1;
																
																	resolve(array_rows);
																});
															});
														})
													}],
													function (result, current) {
														if (counter === 0)
														{  
															methods.getCountCompanyResults(arrParams, blockAccounts,function(returnTotalCount){

																var obj = {
																	result : result[0],
																	tot_count : returnTotalCount.toString(),
																	tot_page  : Math.ceil((parseInt(returnTotalCount) - 5) / 50)
																}
																return_result(obj); 
															});
														}
													});
												}
												else
												{ return_result(return_obj); }
										}
										else
										{ return_result(return_obj); }
									});
								}
								else
								{ return_result(return_obj); }
							});
						}
						else
						{ return_result(return_obj); }
					});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getCountCompanyResults = function(arrParams,blockAccounts,return_result){

	try{
		
		var block_list = arrParams.account_id;

			if(parseInt(arrParams.flag) !== 3)
			{
				if(blockAccounts !== null)
				{ if(blockAccounts.chat_bloack !== null)
					{ block_list = blockAccounts.chat_bloack }
				}
			}

					var query  = " SELECT account_id FROM jaze_user_basic_details WHERE account_id not in ("+block_list+") and account_type = 3  ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";
					//query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('company_name')  AND LOWER(meta_value) REGEXP '[[:<:]]"+arrParams.keyword.toLowerCase()+"' ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('company_name')  AND LOWER(meta_value) REGEXP '^("+str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())))+")' ";
								
					if(parseInt(arrParams.country) !== 0)
					{
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country+"') ";
					}
					if(parseInt(arrParams.state) !== 0)
					{
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state+"') ";
					}
					if(parseInt(arrParams.city) !== 0)
					{
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city+"') ";
					}

					query += " )) ";

					query += " group by account_id  ";

			

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data.length);
					}
					else
					{return_result(0)}
				});
	}
	catch(err)
	{
		return_result(null)
	}
};

methods.getBlockJazechtAccountIdList = function(arrParams,return_result)
{
	var return_obj = {
		chat_bloack : null,
		chat_pending : null
	};

	try
	{
		if(arrParams !== null)
		{	
				var query  = "  select group_concat(t1.account) as account from ( ";
				    query += " SELECT group_concat(meta_value) as account FROM jaze_jazecom_contact_list WHERE meta_value != '' and meta_key IN ('chat_account_id','chat_blocked_account_id') "; 
					query += " and  group_id IN  (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id'  AND meta_value = '"+arrParams.account_id+"' ) ";
					query += " union all ";
					query += " SELECT group_concat(meta_value) as account FROM jaze_jazechat_conversation_list WHERE  meta_value != '' and  meta_key IN ('account_id') "; 
					query += " and  group_id IN  (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'status'  AND meta_value = '0' ";
					query += " and  group_id IN  (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'contact_account_id'  AND meta_value = '"+arrParams.account_id+"' )) ";
					query += " ) as t1 ";
			
					DB.query(query, null, function (data, error) {

						var chat_bloack_list = [];
						var chat_pending_list = [];

						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0) {

							var list = __convert_array(data[0].account);
								list.push(arrParams.account_id);

						 	chat_bloack_list = list.join(); 
						}
						else
						{ chat_bloack_list.push(arrParams.account_id); }

							
						  //pending conversation
						  var query  = "  SELECT group_concat(meta_value) as account FROM jaze_jazechat_conversation_list WHERE  meta_value != '' and  meta_key IN ('contact_account_id') "; 
						  	  query += "and  group_id IN  (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'status'  AND meta_value = '0' ";
							  query += "and  group_id IN  (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'account_id'  AND meta_value = '"+arrParams.account_id+"' )) ";

							  DB.query(query, null, function (data, error) {

								if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
								{ chat_pending_list = data[0].account; }


								var return_obj = {
									chat_bloack : chat_bloack_list,
									chat_pending : chat_pending_list
								};

								//console.log("getBlockJazechtAccountIdList",return_obj);
								return_result(return_obj);

							  });
					});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}



// ------------------------------------------------------ General Search Module End  --------------------------------------------------------------//   


// ------------------------------------------------------ Geo Search Module Start  --------------------------------------------------------------//   

methods.getAutofillCountryResults = function(arrParams,count,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(count) === 2 && (parseInt(arrParams.more_result_flag) === 0 || parseInt(arrParams.more_result_flag) === 1))
		{	 

				var query = " SELECT *,(select app_img_url from geo_bg_img_details where geo_bg_img_details.flag = 1 and geo_bg_img_details.status = 1 and geo_bg_img_details.geo_id = country.id ) as country_bg_image ";
					query += " FROM country WHERE status = 1 AND country_name != ''  ";

					if(parseInt(arrParams.country) !== 0)
					{
						query += " and  id = '"+arrParams.country+"') ";
					}

					if(arrParams.keyword != '')
					{
						query += " AND   LOWER(country_name)  REGEXP '^("+str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())))+")'  ";
			
						query += "  ORDER BY '" +arrParams.keyword+  "', country_name DESC  ";
					}
					else
					{
						query += "  ORDER BY  country_name DESC  ";
					}

					if(parseInt(arrParams.flag) === 1)
					{
						if(parseInt(arrParams.limit) === 0)
						{
							query += "  LIMIT  0 , 50 ";	
						}
						else
						{
							query += "  LIMIT  "+ parseInt(arrParams.limit) * 50 +" , 50 ";	
						}
					}
					else
					{
						if(parseInt(arrParams.limit) === 0)
						{
							query += "  LIMIT  0 , 5 ";	
						}
						else if(parseInt(arrParams.limit) === 1)
						{
							query += "  LIMIT  5 , 50 ";
						}
						else
						{
							var start = parseInt(arrParams.limit - 1) * 50;

							var end = parseInt(arrParams.limit) * 50;

							query += "  LIMIT  "+start+" , "+end;
						}
					}
					



					DB.query(query, null, function (data, error) {
	
						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
						{	
							var counter = data.length;
										
							var array_rows = [];
									
							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data,[function (list) {
				
									return new Promise(function (resolve, reject) {

										var country_bg_img = "";

										if(list.country_bg_image !== "" && list.country_bg_image !== null)
										{ country_bg_img = G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/'+list.country_bg_image.toString(); }
									

										var newObj = {
											country_id	  	: list.id.toString(),
											country_name  	: list.country_name.toString(),
											country_bg_img  : country_bg_img,
											state_id	  	: "",
											state_name    	: "",
											city_id	  		: "",
											city_name    	: "",
											city_bg_img  	: "",
											area_id	  		: "",
											area_name    	: "",
											result_flag    	: "1"
											
										};
										
										array_rows.push(newObj);
									
										counter -= 1;
									
										resolve(array_rows);
									})
								}],
								function (result, current) {
									
									if (counter === 0)
									{
										methods.getCountCountryResults(arrParams, function(returnTotalCount){
											
											var obj = {
												result : result[0],
												tot_count : returnTotalCount.toString(),
												tot_page  : Math.ceil((parseInt(returnTotalCount) - 5) / 50)
											}
											return_result(obj); 
										});
									}
								});
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }	
					});	
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

methods.getCountCountryResults = function(arrParams,return_result){

	try{
			let query  = " SELECT id FROM country  WHERE status = 1 AND country_name != '' AND   LOWER(country_name)  REGEXP '^("+str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())))+")'  group by id ";

			DB.query(query,null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data.length);
				}
				else
				{return_result(0)}
			});
	}
	catch(err)
	{
		return_result(null)
	}
};

methods.getAutofillStateResults = function(arrParams,count,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(count) === 3 && (parseInt(arrParams.more_result_flag) === 0 || parseInt(arrParams.more_result_flag) === 2))
		{	 

				var query  = " SELECT  con.id as country_id,con.country_name,st.id as state_id, st.state_name , ";
					query += " (select app_img_url from geo_bg_img_details where geo_bg_img_details.flag = 1 and geo_bg_img_details.status = 1 and geo_bg_img_details.geo_id = con.id ) as country_bg_image  ";
					query += " FROM states st  ";
					query += " INNER JOIN country con ON con.id = st.country_id  ";
					query += " WHERE st.status = 1 AND con.status = 1 ";


					if(parseInt(arrParams.country) !== 0)
					{
						query += " and  con.id = '"+arrParams.country+"') ";
					}

					if(parseInt(arrParams.state) !== 0)
					{
						query += " and  st.id = '"+arrParams.state+"') ";
					}

					if(arrParams.keyword != '')
					{
						query += " AND   LOWER(st.state_name)  REGEXP '^("+str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())))+")' ";
			
						query += "  ORDER BY '" +arrParams.keyword+  "', st.state_name DESC  ";
					}
					else
					{
						query += "  ORDER BY  st.state_name DESC  ";
					}

					if(parseInt(arrParams.flag) === 2)
					{
						if(parseInt(arrParams.limit) === 0)
						{
							query += "  LIMIT  0 , 50 ";	
						}
						else
						{
							query += "  LIMIT  "+ parseInt(arrParams.limit) * 50 +" , 50 ";	
						}
					}
					else
					{
						if(parseInt(arrParams.limit) === 0)
						{
							query += "  LIMIT  0 , 5 ";	
						}
						else if(parseInt(arrParams.limit) === 1)
						{
							query += "  LIMIT  5 , 50 ";
						}
						else
						{
							var start = parseInt(arrParams.limit - 1) * 50;

							var end = parseInt(arrParams.limit) * 50;

							query += "  LIMIT  "+start+" , "+end;
						}
					}

					DB.query(query, null, function (data, error) {
	
						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
						{	
							var counter = data.length;
										
							var array_rows = [];
									
							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data,[function (list) {
				
									return new Promise(function (resolve, reject) {

										var country_bg_img = "";

										if(list.country_bg_image !== "" && list.country_bg_image !== null)
										{ country_bg_img =  G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/'+list.country_bg_image.toString(); }


										var newObj = {
											country_id	  	: list.country_id.toString(),
											country_name  	: list.country_name.toString(),
											country_bg_img  : country_bg_img,
											state_id	  	: list.state_id.toString(),
											state_name    	: list.state_name.toString(),
											city_id	  		: "",
											city_name    	: "",
											city_bg_img  	: "",
											area_id	  		: "",
											area_name    	: "",
											result_flag    	: "2"
											
										};
																		
										array_rows.push(newObj);
									
										counter -= 1;
									
										resolve(array_rows);
									})
								}],
								function (result, current) {
									
									if (counter === 0)
									{
										methods.getCountStateResults(arrParams, function(returnTotalCount){
											
											var obj = {
												result : result[0],
												tot_count : returnTotalCount.toString(),
												tot_page  : Math.ceil((parseInt(returnTotalCount) - 5) / 50)
											}
											return_result(obj); 
										});
									}
								});
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }	
					});	
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

methods.getCountStateResults = function(arrParams,return_result){

	try{

			let query  = " SELECT id FROM states  WHERE status = 1 AND state_name != '' AND   LOWER(state_name)  REGEXP '^("+str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())))+")'  group by id ";

			DB.query(query,null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data.length);
				}
				else
				{return_result(0)}
			});
	}
	catch(err)
	{
		return_result(null)
	}
};

methods.getAutofillCityResults = function(arrParams,count,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(count) === 4 && (parseInt(arrParams.more_result_flag) === 0 || parseInt(arrParams.more_result_flag) === 3))
		{	 

				var query  = " SELECT  con.id as country_id,con.country_name , st.id as state_id,st.state_name,city.id as city_id,city.city_name , ";
					query += " (select app_img_url from geo_bg_img_details where geo_bg_img_details.flag = 1 and geo_bg_img_details.status = 1 and geo_bg_img_details.geo_id = con.id ) as country_bg_image , ";
					query += " (select app_img_url from geo_bg_img_details where geo_bg_img_details.flag = 3 and geo_bg_img_details.status = 1 and geo_bg_img_details.geo_id = city.id ) as city_bg_image  ";
					query += " FROM cities city  ";
					query += " INNER JOIN states st ON  st.id = city.`state_id`  ";
					query += " INNER JOIN country con ON con.id = st.country_id  ";
					query += " WHERE city.status = 1 AND st.status = 1 AND con.status = 1 ";


					if(parseInt(arrParams.country) !== 0)
					{
						query += " and  con.id = '"+arrParams.country+"') ";
					}

					if(parseInt(arrParams.state) !== 0)
					{
						query += " and  st.id = '"+arrParams.state+"') ";
					}

					if(parseInt(arrParams.city) !== 0)
					{
						query += " and  city.id = '"+arrParams.city+"') ";
					}

					if(arrParams.keyword != '')
					{
						query += " AND   LOWER(city.city_name)  REGEXP '^("+str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())))+")'  ";
			
						query += "  ORDER BY '" +arrParams.keyword+  "', city.city_name DESC  ";
					}
					else
					{
						query += "  ORDER BY  city.city_name DESC  ";
					}

					if(parseInt(arrParams.flag) === 0)
					{
						if(parseInt(arrParams.limit) === 0)
						{
							query += "  LIMIT  0 , 5 ";	
						}
						else if(parseInt(arrParams.limit) === 1)
						{
							query += "  LIMIT  5 , 50 ";
						}
						else
						{
							var start = parseInt(arrParams.limit - 1) * 50;

							var end = parseInt(arrParams.limit) * 50;

							query += "  LIMIT  "+start+" , "+end;
						}
					}

					DB.query(query, null, function (data, error) {
	
						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
						{	
							var counter = data.length;
										
							var array_rows = [];
									
							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data,[function (list) {
				
									return new Promise(function (resolve, reject) {

										var country_bg_img = "";

										if(list.country_bg_image !== "" && list.country_bg_image !== null)
										{ country_bg_img =  G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/'+list.country_bg_image.toString(); }

										var city_bg_image = "";

										if(list.city_bg_image !== "" && list.city_bg_image !== null)
										{ city_bg_image =  G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/'+list.city_bg_image.toString(); }

										var newObj = {
											country_id	  	: list.country_id.toString(),
											country_name  	: list.country_name.toString(),
											country_bg_img  : country_bg_img,
											state_id	  	: list.state_id.toString(),
											state_name    	: list.state_name.toString(),
											city_id	  		: list.city_id.toString(),
											city_name    	: list.city_name.toString(),
											city_bg_img  	: city_bg_image,
											area_id	  		: "",
											area_name    	: "",
											result_flag    	: "3"
											
										};
																		
										array_rows.push(newObj);
									
										counter -= 1;
									
										resolve(array_rows);
									})
								}],
								function (result, current) {
														
									if (counter === 0)
									{ 
										methods.getCountCityResults(arrParams, function(returnTotalCount){
											
											var obj = {
												result : result[0],
												tot_count : returnTotalCount.toString(),
												tot_page  : Math.ceil((parseInt(returnTotalCount) - 5) / 50)
											}
											return_result(obj); 
										});
									}
								});
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }	
					});	
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

methods.getCountCityResults = function(arrParams,return_result){

	try{
			let query  = " SELECT id FROM cities  WHERE status = 1 AND city_name != '' AND   LOWER(city_name)  REGEXP '^("+str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())))+")'  group by id ";

			DB.query(query,null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data.length);
				}
				else
				{return_result(0)}
			});
	}
	catch(err)
	{
		return_result(null)
	}
};

methods.getAutofillAreaResults = function(arrParams,count,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(count) === 5 && (parseInt(arrParams.more_result_flag) === 0 || parseInt(arrParams.more_result_flag) === 4))
		{	 
			
				var query  = " SELECT  con.id as country_id,con.country_name , st.id as state_id,st.state_name,city.id as city_id,city.city_name,area.area_id,area.area_name, ";
					query += " (select app_img_url from geo_bg_img_details where geo_bg_img_details.flag = 1 and geo_bg_img_details.status = 1 and geo_bg_img_details.geo_id = con.id ) as country_bg_image , ";
					query += " (select app_img_url from geo_bg_img_details where geo_bg_img_details.flag = 3 and geo_bg_img_details.status = 1 and geo_bg_img_details.geo_id = city.id ) as city_bg_image  ";
					query += " FROM area   ";
					query += " INNER JOIN cities city ON  city.id = area.city_id  ";
					query += " INNER JOIN states st ON  st.id = city.`state_id`  ";
					query += " INNER JOIN country con ON con.id = st.country_id  ";
					query += " WHERE area.status = 1 AND city.status = 1 AND st.status = 1 AND con.status = 1 ";


					if(parseInt(arrParams.country) !== 0)
					{
						query += " and  con.id = '"+arrParams.country+"') ";
					}

					if(parseInt(arrParams.state) !== 0)
					{
						query += " and  st.id = '"+arrParams.state+"') ";
					}

					if(parseInt(arrParams.city) !== 0)
					{
						query += " and  city.id = '"+arrParams.city+"') ";
					}

					if(typeof arrParams.area !== 'undefined' && arrParams.area !== null && parseInt(arrParams.area) !== 0)
					{
						query += " and area.area_id = '"+arrParams.area+"') ";
					}

					if(arrParams.keyword != '')
					{
						query += " AND   LOWER(area_name)  REGEXP '^("+str_replace(' ', '|', rtrim(ltrim(arrParams.keyword.toLowerCase())))+")'  ";
			
						query += "  ORDER BY '" +arrParams.keyword+  "', area_name DESC  ";
					}
					else
					{
						query += "  ORDER BY  area_name DESC  ";
					}

					if(parseInt(arrParams.limit) !== 0)
					{
						query += "  LIMIT  "+(parseInt(arrParams.limit) * 50)+" , 50 ";				
					}
					else
					{
						query += "  LIMIT  0 , 50 ";	
					}

					DB.query(query, null, function (data, error) {
	
						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
						{	
							var counter = data.length;
										
							var array_rows = [];
									
							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data,[function (list) {
				
									return new Promise(function (resolve, reject) {

										var country_bg_img = "";

										if(list.country_bg_image !== "" && list.country_bg_image !== null)
										{ country_bg_img =  G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/'+list.country_bg_image.toString(); }

										var city_bg_image = "";

										if(list.city_bg_image !== "" && list.city_bg_image !== null)
										{ city_bg_image =  G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/'+list.city_bg_image.toString(); }

										var newObj = {
											country_id	  	: list.country_id.toString(),
											country_name  	: list.country_name.toString(),
											country_bg_img  : country_bg_img,
											state_id	  	: list.state_id.toString(),
											state_name    	: list.state_name.toString(),
											city_id	  		: list.city_id.toString(),
											city_name    	: list.city_name.toString(),
											city_bg_img  	: city_bg_image,
											area_id	  		: list.area_id.toString(),
											area_name    	: list.area_name.toString(),
											result_flag    	: "4"
										};
																		
										array_rows.push(newObj);
									
										counter -= 1;
									
										resolve(array_rows);
									})
								}],
								function (result, current) {
														
									if (counter === 0)
									{return_result(result[0]); }
								});
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }	
					});	
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

// ------------------------------------------------------ Geo Search Module End  --------------------------------------------------------------//   


methods.convertMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			Object.assign(list, {account_id : arrvalues[0].account_id});
			Object.assign(list, {account_type : arrvalues[0].account_type});

			for (var i in arrvalues) 
			{
				Object.assign(list, {[arrvalues[i].meta_key]:  arrvalues[i].meta_value});
			}

			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};

methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].account_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							account_type : arrvalues[i].account_type,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};

/*
* split values push to array
*/
function __convert_array(array_val) {

	var list = [];

	if(typeof array_val !== 'undefined' && array_val !== null && array_val !== "")
	{
		if(array_val.toString().indexOf(',') != -1 ){
			list = array_val.split(',');
		}
		else {
			list.push(array_val);
		}
	}	
	return list;
}

function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

		if(list_array !== null)
		{
			Object.keys(list_array).forEach(function(prop) {

				var value = list_array[prop].account_id;
 
				if(parseInt(value) === parseInt(index_value))
				{
					retern_index = prop;
				}
			  });
		}
		
		return retern_index;
}



methods.__getNationality = function(arrParams,return_result){

	try{

		if(parseInt(arrParams) !==0){

			let query  = " SELECT country_name FROM country  WHERE id =? ";
			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
			
		}else{
			return_result('');
		}
	
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getCountryDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT country_name FROM country  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('');
	}
};

methods.__getStateDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT state_name FROM states  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].state_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('');
	}
};

methods.__getCityDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT city_name FROM cities  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].city_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('');
	}
};

methods.__getAreaDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT area_name FROM area  WHERE area_id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].area_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('');
	}
};

methods.__getCurrenctDetails = function(arrParams,return_result){

	try{
			if(parseInt(arrParams) !==0){
				let query  = `SELECT meta_value FROM jaze_currency_master WHERE meta_key = 'currency_name' and meta_value != "" and group_id =?`;

				DB.query(query,[arrParams], function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0].meta_value);
					}
					else
					{return_result('')}
				});
			}else{
				return_result('');
			}
	}
	catch(err)
	{
		return_result('');
	}
};


//------------------------------------------------------------------- Location Module Start ---------------------------------------------------------------------------//

methods.__getLocationDetails = function(area_id,city_id,return_result){

	try{
			var obj_return = {
				country_id : "0",
				country_name : "",
				state_id : "0",
				state_name : "",
				city_id : "0",
				city_name : "",
				area_id : "0",
				area_name : "",
			};

			if(parseInt(area_id) !== 0)
			{
				var query  =" SELECT  con.id as country_id,con.country_name ,st.id as state_id,st.state_name,city.id as city_id,city.city_name,ar.area_id,ar.area_name ";
					query +=" FROM area ar  ";
					query +=" INNER JOIN cities city ON city.id = ar.city_id ";
                    query +=" INNER JOIN states st ON st.id = city.state_id ";
                    query +=" INNER JOIN country con ON con.id=st.country_id ";
					query +=" WHERE ar.status = '1' and city.status = '1' and st.status = '1' and con.status = '1' and ar.area_id = '"+area_id+"' " ;
 
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0]);
					}
					else
					{return_result(obj_return)}
				});
			}
			else if(parseInt(city_id) !== 0)
			{
				var query  =" SELECT  con.id as country_id,con.country_name ,st.id as state_id,st.state_name,city.id as city_id,city.city_name, '0' as area_id, '' as area_name ";
					query +=" FROM cities city  ";
                    query +=" INNER JOIN states st ON st.id = city.state_id ";
                    query +=" INNER JOIN country con ON con.id=st.country_id ";
					query +=" WHERE city.status = '1' and st.status = '1' and con.status = '1' and city.id = '"+city_id+"' " ;
 
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0]);
					}
					else
					{return_result(obj_return)}
				});
			}
			else
			{ return_result(obj_return); }

			
	}
	catch(err)
	{
		return_result(obj_return)
	}
};
//------------------------------------------------------------------- Location Module End ---------------------------------------------------------------------------//


module.exports = methods;

