const methods = {};
const DB = require('../../helpers/db_base.js');

methods.checkParametersApiToken = function(arrParams,return_result){
	try
	{
		if(arrParams.api_token === "jazenet")
		{
			var return_data = [];

			return_data.push(arrParams.account_id);

			return_result(return_data);
		}
		else
		{
			DB.query("select * from jaze_app_device_details where status ='1' and app_type = '1' and account_id=? and api_token=?", [arrParams.account_id,arrParams.api_token], function(data, error){
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result(data);
				
				}else{
					return_result(null);
				}

			});
		}
	}
	catch(err)
	{
		console.log("convertMetaArray",err); 
		return_result(null);
	}
};

methods.checkParametersApiTokenJazenetJazecom = function(arrParams,return_result){
	try
	{
		if(arrParams.api_token === "jazenet")
		{
			var return_data = [];

			return_data.push(arrParams.account_id);

			return_result(return_data);
		}
		else
		{
			DB.query("select * from jaze_app_device_details where status ='1' and (app_type = '1' or app_type = '2') and account_id=? and api_token=?", [arrParams.account_id,arrParams.api_token], function(data, error){
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result(data);
				
				}else{
					return_result(null);
				}

			});
		}
	}
	catch(err)
	{
		console.log("checkParametersApiTokenJazenetJazecom",err); 
		return_result(null);
	}
};

methods.convertMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			Object.assign(list, {group_id : arrvalues[0].group_id});

			for (var i in arrvalues) 
			{
				Object.assign(list, {[arrvalues[i].meta_key]:  arrvalues[i].meta_value});
			}

			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray : ",err); return_result(null);}
};

methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};

function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

		if(list_array !== null)
		{
			Object.keys(list_array).forEach(function(prop) {

				var value = list_array[prop].group_id;
 
				if(parseInt(value) === parseInt(index_value))
				{
					retern_index = prop;
				}
			  });
		}
		
		return retern_index;
}

//-------------------------------------------------------------------  General TB Module Start ---------------------------------------------------------------------------//

methods.getNewGroupId = function(tbName,return_result){

	try{
		if(tbName !== "")
		{
			var query = " SELECT  CASE COALESCE(group_id, 0) WHEN  '0' THEN '1'  ELSE MAX(group_id) + 1   END as group_id FROM  "+tbName+ " ";

			DB.query(query,null, function(dataG, error){ 

				if(typeof dataG !== 'undefined' && dataG !== null  && parseInt(dataG.length) > 0){
						
					return_result(dataG[0].group_id); 						
				}
				else
				{ return_result(1); }
			});
		}
		else
		{ return_result(1); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

methods.insertTBQuery = function(tbName,grpId,arrInsert,return_result){

	try{
		if(arrInsert !== null && tbName !== "" && grpId !== null)
		{					
			for (var meta_keys in arrInsert) 
			{
				var post  = {group_id: grpId, meta_key: meta_keys,meta_value: arrInsert[meta_keys] };

				DB.query("  insert into "+tbName+" SET ?  ",post, function(result,error){});
			};

			return_result(grpId);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}


methods.updateQuery = function(tbName,grpId,arrInsert,return_result){

		try{
			if(tbName !== "" && grpId !== "" && arrInsert !== null)
			{
				var i = 0;

				for (var meta_key in arrInsert) 
				{
					var quer = " UPDATE  "+tbName+"  SET    meta_value = '"+arrInsert[meta_key]+"' where meta_key = '"+meta_key+"' and  group_id =  '"+grpId+"';";
						DB.query(quer,null, function(data, error){
					});
				};

				return_result(grpId);
			}	
			else
			{ return_result(0); }
		}
		catch(err)
		{
			console.log(err);
			return_result(0);
		}
	}

//-------------------------------------------------------------------  General TB Module End ---------------------------------------------------------------------------//


module.exports = methods;