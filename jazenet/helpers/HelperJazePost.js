const methods = {};
const DB = require('../../helpers/db_base.js');
const promiseForeach = require('promise-foreach');
const dateFormat = require('dateformat');
const now = new Date();
const standardDT = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
const newdateForm = dateFormat(new Date(standardDT), 'yyyy-mm-dd HH:MM:ss');

const STATIC_IMG_URL = G_STATIC_IMG_URL+"uploads/jazenet/"; 
var php = require('js-php-serialize');

methods.getCompanyProfile =  function(arrParams,return_result){
	try
	{
		
		var arrAccounts = [];

		var queryAccountType = `SELECT account_id,account_type FROM jaze_user_basic_details WHERE account_id =?`;

		DB.query(queryAccountType,[arrParams.account_id], function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var queryAccDetails = `SELECT * FROM jaze_user_basic_details WHERE account_id =? and account_type=?`;

				DB.query(queryAccDetails,[data[0].account_id,data[0].account_type], function(allData, error){
	
					if(typeof allData !== 'undefined' && allData !== null && parseInt(allData.length) > 0)
					{

						methods.convertMultiMetaArrayCompany(allData, function(retConverted){	

							var counter = retConverted.length;		
							var jsonItems = Object.values(retConverted);

							promiseForeach.each(jsonItems,[function (val) {
								return new Promise(function (resolve, reject) {		
						
							  		var default_logo = '';

									var newObj = {};

							  		if(parseInt(data[0].account_type) === 1)
							  		{
							  			if(val.profile_logo){
											default_logo = STATIC_IMG_URL + 'individual_account/members_thumb/thumb_' + val.profile_logo + '';
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : (val.fname +" "+val.lname).toString(),
											logo		  : default_logo,
											currency	  : val.currency_id,
											category_id   : ''
										}	

							  		}
							  		else if(parseInt(data[0].account_type) === 2)
							  		{

				  						if(val.professional_logo){
											default_logo = STATIC_IMG_URL + "professional_account/"+val.account_id+"/logo_thumb/thumb_" + val.professional_logo;
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : (val.fname +" "+val.lname).toString(),
											logo		  : default_logo,
											currency	  : val.currency_id,
											category_id   : ''
										}	

							  		}
						  			else if(parseInt(data[0].account_type) === 3)
					  				{

		  								if(val.listing_images){
				  							default_logo = STATIC_IMG_URL + 'company/'+val.account_id+'/listing_images/' + val.listing_images + '';
										
										}else if(val.company_logo){
											default_logo = STATIC_IMG_URL + 'company/'+val.account_id+'/logo/' + val.company_logo + '';
										}else{
											default_logo = "";
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : val.company_name,
											logo		  : default_logo,
											currency	  : val.currency_id,
											category_id   : val.category_id
										}	

					  				}

									counter -= 1;
									if (counter === 0){ 
										resolve(newObj);
									}
									
								})
							}],
							function (result, current) {
								if (counter === 0){ 
									return_result(result[0]);
								}
							});

						});


					}
					else
					{
						return_result(null);
					}
				});

		
			}
			else
			{
				return_result(null);
			}
		});

	}
	catch(err)
	{ console.log("getCompanyProfile",err); return_result(null);}
};


// --------------------------------------------------- Post Events Start  ---------------------------------------------- //

methods.postNewEvent = function(arrParams,return_result){

	try
	{	

		var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_events_details";
		var lastGrpId = 0;

		methods.getLastGroupId(queGroupMax, function(queGrpRes){

			lastGrpId = queGrpRes[0].lastGrpId;

		  	var full_filename = "";
		  	if(arrParams.file_name!="" && arrParams.file_ext !=""){
		  		full_filename = arrParams.file_name+'.'+arrParams.file_ext;
		  	}

			var arrInsert = {

				account_id     : arrParams.account_id,

				event_cat_id   : arrParams.category_id,
				title          : arrParams.title,
				event_date     : arrParams.start_date,
				end_date       : arrParams.end_date,
				start_time     : arrParams.start_time,
				end_time       : arrParams.end_time,
	
				contact_name   : arrParams.contact_name,
				contact_no     : arrParams.contact_no,
				contact_email  : arrParams.contact_email,

				venue          : arrParams.venue,
				event_link     : arrParams.event_link,
				
				country_id     : arrParams.country_id,
				state_id       : arrParams.state_id,
				city_id        : arrParams.city_id,
				area_id        : arrParams.area_id,
				
				street_name    : arrParams.street_name,
				street_no      : arrParams.street_no,
				building_name  : arrParams.building_name,
				building_no    : arrParams.building_no,

				content        : htmlEntities(arrParams.event_desc),

				featured_media : full_filename,
			
				latitude       : arrParams.latitude,
				longitude      : arrParams.longitude,
				created_date   : newdateForm
			};

			var plusOneGroupID = lastGrpId+1;
	  		var querInsert = `insert into jaze_events_details (group_id, meta_key, meta_value) VALUES (?,?,?)`;

	  		var returnFunction;
		 	for (var key in arrInsert) 
		 	{
				DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
		 		 	if(parseInt(data.affectedRows) > 0){
 						return_result(1);
			 	   	}else{
	   					return_result(0);
			 	   	}
		 	 	});
		  	};

		});

	}
	catch(err)
	{ console.log("postNewEvent",err); return_result(0);}

};


methods.AddNewEvent = function(arrParams,return_result){

	try{


		var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_events_details";
		var lastGrpId = 0;

		methods.getLastGroupId(queGroupMax,function(queGrpRes){

			console.log(queGrpRes[0].lastGrpId);

		});


	}
	catch(err){ console.log("AddNewEvent",err); return_result(0);}

};


// --------------------------------------------------- Post Events End  ---------------------------------------------- //


// --------------------------------------------------- Post Property Start  ------------------------------------------ //



methods.getCategoryAttributesProperty = function(arrParams,return_result){


	try
	{
		var mainArr = [];
		var objController = {};
		var sort_order;
		var trigger = 0;
		var object_name;

		
		if(parseInt(arrParams.flag) === 1){
			var quer = "SELECT * FROM jaze_jazestore_attributes_groups WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups WHERE meta_key = 'cat_id' AND (meta_value ='209' OR meta_value ='208')  ) ";
		}else{
			var quer = "SELECT * FROM jaze_jazestore_attributes_groups WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups WHERE meta_key = 'cat_id' AND (meta_value ='210' OR meta_value ='208') ) ";
		}

		quer +=	` AND group_id IN (SELECT group_id FROM  jaze_jazestore_attributes_groups  WHERE meta_key = 'status' AND meta_value = '1' )`;

		DB.query(quer,null, function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){	


					const forEach = async () => {
					  	const values = data;
					  	for (const val of retConverted){
								
		
				  			methods.getCategoryAttributeValueProperty(val.group_id,val.input_type, function(retMainArr){ 

		  						if(val.input_type == 'multiselect_image_button'){
									controller_type = '1';
									objController = {
										controller_type:'1',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'all',
										items_value_list:retMainArr
									};
								}else if(val.input_type == 'multiselect_dropdown'){
									controller_type = '2';
									objController = {
										controller_type:'2',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'all',
										items_value_list:retMainArr
									};
								}else if(val.input_type == 'color_picker'){
									controller_type = '3';
									objController = {
										controller_type:'3',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'all',
										items_color_list:retMainArr
									};
								}else if(val.input_type == 'image_button'){
									controller_type = '4';
									objController = {
										controller_type:'4',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'all',
										items_value_list:retMainArr
									};
								}else if(val.input_type == 'dropdown'){
									controller_type = '5';
									objController = {
										controller_type:'5',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'all',
										items_value_list:retMainArr
									};
								}else if(val.input_type == 'radio_button'){
									controller_type = '6';
									objController = {
										controller_type:'6',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'',
										items_sing_value_list:retMainArr
									};
								}else if(val.input_type == 'select_range'){
									controller_type = '7';
									objController = {
										controller_type:'7',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'',
										items_select_range_value:retMainArr
									};
								}else if(val.input_type == 'input_range'){
									controller_type = '8';
									objController = {
										controller_type:'8',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'all',
										items_input_range_value:retMainArr
									};
								}else if(val.input_type == 'text'){
									controller_type = '9';
									objController = {
										controller_type:'9',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'',
										items_edittext_value:retMainArr
									};
								}else if(val.input_type == 'button'){
									controller_type = '10';
									objController = {
										controller_type:'10',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'',
										items_sing_value_list:retMainArr
									};
								}

								mainArr.push(objController);

							});
					  	}
				  		const result = await returnData(mainArr);
					}
					 
					const returnData = x => {
					  return new Promise((resolve, reject) => {
					    setTimeout(() => {
					      resolve(x);
					    }, 500);
					  });
					}
					 
					forEach().then(() => {
				
						
						var sortedArray = mainArr.sort(function(a, b) {
						    return a.sort_order > b.sort_order ? 1 : a.sort_order < b.sort_order ? -1 : 0;
						});

						return_result(sortedArray);
			
					})

				});
	        }
	        else
	        { 
	        	return_result([]);
	        }

		});

	}
	catch(err)
	{ console.log("getCategoryAttributesProperty",err); return_result(null); }	
};


methods.getCategoryAttributeValueProperty = function(group_id,input_type,return_result){

	try
	{
		var arrValues = [];
		var obj_values = {};
		var controller_type;
		var objController = {};
		var value;
		var logo;

		if(input_type == 'color_picker'){
			obj_values = {
				id:'0',
				title:'all',
				color_code:''
			};
			arrValues.push(obj_values);
		}

		var singleObj = ['text','input_range','select_range'];
	 	var arrSameInputs = ['multiselect_image_button','multiselect_dropdown','image_button','dropdown'];

		if(arrSameInputs.includes(input_type) || input_type == 'color_picker' || input_type == 'radio_button' || input_type == 'text' || input_type == 'button'){

			var quer = `SELECT * FROM jaze_jazestore_attributes_groups_meta 
				WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
				AND group_id IN (SELECT group_id FROM  jaze_jazestore_attributes_groups_meta  WHERE meta_key = 'status' AND meta_value = '1' )`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					methods.convertMultiMetaArray(data, function(retConverted){	


						retConverted.forEach(vala => { 
						
							if(arrSameInputs.includes(input_type)){

								if(vala.value != null){
									value = vala.value;
									logo  = '';
								}else{
									value = '';
									logo = vala.logo;
								}

								if(input_type == 'multiselect_image_button'){

									obj_values = {
										id:vala.group_id.toString(),
										title:vala.key_name,
										value:'',
										logo:STATIC_IMG_URL+vala.value
									};

								}else if(input_type == 'image_button'){

									obj_values = {
										id:vala.group_id.toString(),
										title:vala.key_name,
										value:STATIC_IMG_URL+vala.value,
										logo:''
									};


								}else{

									obj_values = {
										id:vala.group_id.toString(),
										title:vala.key_name,
										value:value,
										logo:logo
									};

								}

							}else if(input_type == 'color_picker'){

								obj_values = {
									id:vala.group_id.toString(),
									title:vala.key_name,
									color_code:vala.value
								};

							}else if(input_type == 'radio_button'){


								obj_values = {
									id:vala.group_id.toString(),
									title:vala.key_name,
									value: vala.value,
						
								};

							}else if(input_type == 'button'){


								obj_values = {
									id:vala.group_id.toString(),
									title:vala.key_name,
									value: vala.value,
						
								};

							}else if(input_type == 'text'){
								
								obj_values = {
									text_hint_label :vala.group_id.toString()
								};
							}

							arrValues.push(obj_values);

						}); 

					});

		  		}

			});

		
		}else if(input_type == 'input_range'){
	

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_jazestore_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var min_field_label;
					var max_field_label;
					var suffix_field_label;
					var separator_field_label;

					const forEach = async () => {
					  	const values = data;
					  	for (const vala of values){

							if(vala.key_name == 'separator label'){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == 'min label'){
								min_field_label = vala.key_value;
							}

							if(vala.key_name == 'max label'){
								max_field_label = vala.key_value;
							}

							if(vala.key_name == 'suffix'){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								min_field_label:min_field_label,
								max_field_label:max_field_label,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

					  	}
				  		const result = await returnData(obj_values);
					}
					 
					const returnData = x => {
					  return new Promise((resolve, reject) => {
					    setTimeout(() => {
					      resolve(x);
					    }, 300);
					  });
					}
					 
					forEach().then(() => {

						arrValues.push(obj_values);
					
					})
		  		}

			});
					
		}else if(input_type == 'select_range'){
	

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_jazestore_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var min_field_label;
					var min_field_value;
					var max_field_label;
					var max_field_value;
					var interval_value;
					var suffix_field_label;
					var separator_field_label;

					const forEach = async () => {
					  	const values = data;
					  	for (const vala of values){

							if(vala.key_name == 'separator label'){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == 'min label'){
								min_field_label = vala.key_value;
							}


							if(vala.key_name == 'min value'){
								min_field_value = vala.key_value;
							}

							if(vala.key_name == 'max label'){
								max_field_label = vala.key_value;
							}


							if(vala.key_name == 'max value'){
								max_field_value = vala.key_value;
							}

							if(vala.key_name == 'interval value'){
								interval_value = vala.key_value;
							}


							if(vala.key_name == 'suffix'){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								id:'0',
								min_field_label:min_field_label,
								min_field_value:min_field_value,
								max_field_label:max_field_label,
								max_field_value:max_field_value,
								internal_value:interval_value,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

					  	}
				  		const result = await returnData(obj_values);
					}
					 
					const returnData = x => {
					  return new Promise((resolve, reject) => {
					    setTimeout(() => {
					      resolve(x);
					    }, 300);
					  });
					}
					 
					forEach().then(() => {
						arrValues.push(obj_values);
					
					})
		  		}

			});
					
		}

		setTimeout(function(){ 

			if(singleObj.includes(input_type)){
				return_result(obj_values);
			}else{
				return_result(arrValues.sort(sortingArrayObject('title')));
			}
			
		}, 500);

	}
	catch(err)
	{ console.log("getCategoryAttributeValueProperty",err); return_result([]); }	
};



methods.postProperty = function(arrParams,return_result){

	try
	{	

		var queGroupMax = "SELECT MAX(product_id) AS lastGrpId FROM jaze_products_meta";
		var lastGrpId = 0;

		methods.getLastGroupId(queGroupMax, function(queGrpRes){

			lastGrpId = queGrpRes[0].lastGrpId;

			var accParams = {account_id:arrParams.account_id};

			methods.getCompanyProfile(accParams, function(retAccount){

				if(retAccount!=null)
				{
					var company_category_id = '';
					if(retAccount.category_id){
						company_category_id = retAccount.category_id;
					}

				  	var category_id = '210';
				  	var jaze_category_all = '210,208'
				  	if(parseInt(arrParams.flag) === 1){
						category_id = '209';
						jaze_category_all = '209,208'
				  	}

				  	var arrMainInsert = [];

					var objMainInsert = {

						account_id  : arrParams.account_id,
						name        : arrParams.title,
						title       : arrParams.title,
						ribbon      : '',
						content     : '',
						category_id : 0,
						company_category_id   : 0,
						jaze_category_id      : category_id,
						jaze_category_all     : jaze_category_all,
						jaze_brand_id  : 0,
						jaze_instore   : 1,
						keywords       : '',
						product_type   : 'product',
						currency_id    : retAccount.currency,
						visibility     : 1,
						agent_id       : 0,
						parent_id      : 0,
						
						country_id     : arrParams.country_id,
						state_id       : arrParams.state_id,
						city_id        : arrParams.city_id,
						area_id        : arrParams.area_id,
						created_date   : newdateForm,
						modified_date  : newdateForm
						
					};

					arrMainInsert.push(objMainInsert);

					var arrAttributes_filters = [];
					var objAttributes_filters = {};
					var objAttributes_filters_insert = {};
					var arrAttributes_filters_insert = [];

	  				for (var param of arrParams.attributes){

						objAttributes_filters_insert['attribute_filter_'+param.attribute_id] = param.value

						if(parseInt(param.attribute_id) === 30 ){
							objAttributes_filters[param.attribute_id] = 'type';
						}
						if(parseInt(param.attribute_id) === 31 ){
							objAttributes_filters[param.attribute_id] = 'rent_period';
						}
						if(parseInt(param.attribute_id) === 43 ){
							objAttributes_filters[param.attribute_id] = 'property_type';
						}
						if(parseInt(param.attribute_id) === 44 ){
							objAttributes_filters[param.attribute_id] = 'property_type';
						}
						if(parseInt(param.attribute_id) === 49 ){
							objAttributes_filters[param.attribute_id] = 'furnishing';
						}
						if(parseInt(param.attribute_id) === 50 ){
							objAttributes_filters[param.attribute_id] = 'amenities';			
						}
						if(parseInt(param.attribute_id) === 59 ){
							objAttributes_filters[param.attribute_id] = 'fittings';			
						}
						if(parseInt(param.attribute_id) === 82 ){
							objAttributes_filters[param.attribute_id] = 'price';
						}	
						if(parseInt(param.attribute_id) === 83 ){
							objAttributes_filters[param.attribute_id] = 'sqft';
						}	
						if(parseInt(param.attribute_id) === 89 ){
							objAttributes_filters[param.attribute_id] = 'bedrooms';					
						}	
						if(parseInt(param.attribute_id) === 90 ){
							objAttributes_filters[param.attribute_id] = 'completion_status';
						}	

						arrAttributes_filters.push(objAttributes_filters);
						arrAttributes_filters_insert.push(objAttributes_filters_insert);

	  				}

  					var arrAttributes_filters = php.serialize(arrAttributes_filters[0]);

					for (var rows in arrMainInsert) 
				 	{	
						DB.query("INSERT INTO jaze_products SET ?",[arrMainInsert[rows]], function(mData, error){
			
						 	if(mData!=null){

	 							var arrImages = php.serialize(arrParams.images.split(","));

								var arrMetaInsert = {

									sku                    : '',
									regular_price          : arrParams.price,
									sale_price             : arrParams.price+'.00',
									sale_discount          : '',
									sale_onsale            : '',
									sale_onsale_schedule   : '',
									sale_price_start_from  : '',
									sale_price_start_to    : '',
									featured_image         : arrParams.images.split(",")[0],
									gallery_images         : arrImages,
									stocks                 : '',
									stocks_status          : 1,
									manage_stocks          : 0,
									default_attributes     : '',
									product_attributes     : '',
									weight                 : '',
									height                 : '',
									length                 : '',
									product_sections       : '',
									attribute_filters      : arrAttributes_filters
								}

								let merged = {...arrMetaInsert, ...arrAttributes_filters_insert[0]};

								var querInsert = `INSERT INTO jaze_products_meta (product_id, meta_key, meta_value) VALUES (?,?,?)`;
				 			 	for (var key in merged) 
			 					{
									DB.query(querInsert,[mData.insertId,key,merged[key]], function(data, error){
							 		 	if(parseInt(data.affectedRows) > 0){
					 						return_result(1);
								 	   	}else{
						   					return_result(0);
								 	   	}
							 	 	});
							  	};

					 	   	}else{
			   					return_result(0);
					 	   	}
				 	 	});

				  	};

				}
			})
		});

	}
	catch(err)
	{ console.log("postProperty",err); return_result(0);}

};



// --------------------------------------------------- Post Property End  ------------------------------------------ //


// --------------------------------------------------- Post cars start  ------------------------------------------ //


methods.getCategoryAttributesAuto = function(arrParams,return_result){


	try
	{
		var mainArr = [];
		var objController = {};
		var sort_order;
		var trigger = 0;
		var object_name;

		var quer = "";
		if(parseInt(arrParams.flag) === 1){
			quer = "SELECT * FROM jaze_jazestore_attributes_groups WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups WHERE meta_key = 'cat_id' AND (meta_value ='56' OR meta_value ='57') ) ";
		}else if(parseInt(arrParams.flag) === 2){
			quer = "SELECT * FROM jaze_jazestore_attributes_groups WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups WHERE meta_key = 'cat_id' AND (meta_value ='56' OR meta_value ='58') ";
		}else if(parseInt(arrParams.flag) === 3){
			quer = "SELECT * FROM jaze_jazestore_attributes_groups WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups WHERE meta_key = 'cat_id' AND (meta_value ='56' OR meta_value ='59') ) ";
		}


		DB.query(quer,null, function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){	


					const forEach = async () => {
					  	const values = data;
					  	for (const val of retConverted){
								
		
				  			methods.getCategoryAttributeValueAuto(val.group_id,val.input_type, function(retMainArr){ 

		  						if(val.input_type == 'multiselect_image_button'){
									controller_type = '1';
									objController = {
										controller_type:'1',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'all',
										items_value_list:retMainArr
									};
								}else if(val.input_type == 'multiselect_dropdown'){
									controller_type = '2';
									objController = {
										controller_type:'2',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'all',
										items_value_list:retMainArr
									};
								}else if(val.input_type == 'color_picker'){
									controller_type = '3';
									objController = {
										controller_type:'3',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'all',
										items_color_list:retMainArr
									};
								}else if(val.input_type == 'image_button'){
									controller_type = '4';
									objController = {
										controller_type:'4',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'all',
										items_value_list:retMainArr
									};
								}else if(val.input_type == 'dropdown'){
									controller_type = '5';
									objController = {
										controller_type:'5',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'all',
										items_value_list:retMainArr
									};
								}else if(val.input_type == 'radio_button'){
									controller_type = '6';
									objController = {
										controller_type:'6',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'',
										items_sing_value_list:retMainArr
									};
								}else if(val.input_type == 'select_range'){
									controller_type = '7';
									objController = {
										controller_type:'7',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'',
										items_select_range_value:retMainArr
									};
								}else if(val.input_type == 'input_range'){
									controller_type = '8';
									objController = {
										controller_type:'8',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'all',
										items_input_range_value:retMainArr
									};
								}else if(val.input_type == 'text'){
									controller_type = '9';
									objController = {
										controller_type:'9',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'',
										items_edittext_value:retMainArr
									};
								}else if(val.input_type == 'button'){
									controller_type = '10';
									objController = {
										controller_type:'10',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
										home_sort_order:val.details_sort_order.toString(),
										adv_status:val.is_listing_page.toString(),
										adv_sort_order:val.listing_sort_order.toString(),
										attribute_option_label:'',
										items_sing_value_list:retMainArr
									};
								}

								mainArr.push(objController);

							});
					  	}
				  		const result = await returnData(mainArr);
					}
					 
					const returnData = x => {
					  return new Promise((resolve, reject) => {
					    setTimeout(() => {
					      resolve(x);
					    }, 500);
					  });
					}
					 
					forEach().then(() => {
				
						
						var sortedArray = mainArr.sort(function(a, b) {
						    return a.sort_order > b.sort_order ? 1 : a.sort_order < b.sort_order ? -1 : 0;
						});

						return_result(sortedArray);
			
					})

				});
	        }
	        else
	        { 
	        	return_result([]);
	        }

		});

	}
	catch(err)
	{ console.log("getCategoryAttributesAuto",err); return_result(null); }	
};


methods.getCategoryAttributeValueAuto = function(group_id,input_type,return_result){

	try
	{
		var arrValues = [];
		var obj_values = {};
		var controller_type;
		var objController = {};
		var value;
		var logo;

		if(input_type == 'color_picker'){
			obj_values = {
				id:'0',
				title:'all',
				color_code:''
			};
			arrValues.push(obj_values);
		}

		var singleObj = ['text','input_range','select_range'];
	 	var arrSameInputs = ['multiselect_image_button','multiselect_dropdown','image_button','dropdown'];

		if(arrSameInputs.includes(input_type) || input_type == 'color_picker' || input_type == 'radio_button' || input_type == 'text' || input_type == 'button'){

			var quer = `SELECT * FROM jaze_jazestore_attributes_groups_meta 
				WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
				AND group_id IN (SELECT group_id FROM  jaze_jazestore_attributes_groups_meta  WHERE meta_key = 'status' AND meta_value = '1' )`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					methods.convertMultiMetaArray(data, function(retConverted){	


						retConverted.forEach(vala => { 
						
							if(arrSameInputs.includes(input_type)){

								if(vala.value != null){
									value = vala.value;
									logo  = '';
								}else{
									value = '';
									logo = vala.logo;
								}

								if(input_type == 'multiselect_image_button'){

									obj_values = {
										id:vala.group_id.toString(),
										title:vala.key_name,
										value:'',
										logo:STATIC_IMG_URL+vala.value
									};

								}else if(input_type == 'image_button'){

									obj_values = {
										id:vala.group_id.toString(),
										title:vala.key_name,
										value:STATIC_IMG_URL+vala.value,
										logo:''
									};


								}else{

									obj_values = {
										id:vala.group_id.toString(),
										title:vala.key_name,
										value:value,
										logo:logo
									};

								}

							}else if(input_type == 'color_picker'){

								obj_values = {
									id:vala.group_id.toString(),
									title:vala.key_name,
									color_code:vala.value
								};

							}else if(input_type == 'radio_button'){


								obj_values = {
									id:vala.group_id.toString(),
									title:vala.key_name,
									value: vala.value,
						
								};

							}else if(input_type == 'button'){


								obj_values = {
									id:vala.group_id.toString(),
									title:vala.key_name,
									value: vala.value,
						
								};

							}else if(input_type == 'text'){
								
								obj_values = {
									text_hint_label :vala.group_id.toString()
								};
							}

							arrValues.push(obj_values);

						}); 

					});

		  		}

			});

		
		}else if(input_type == 'input_range'){
	

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_jazestore_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var min_field_label;
					var max_field_label;
					var suffix_field_label;
					var separator_field_label;

					const forEach = async () => {
					  	const values = data;
					  	for (const vala of values){

							if(vala.key_name == 'separator label'){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == 'min label'){
								min_field_label = vala.key_value;
							}

							if(vala.key_name == 'max label'){
								max_field_label = vala.key_value;
							}

							if(vala.key_name == 'suffix'){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								min_field_label:min_field_label,
								max_field_label:max_field_label,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

					  	}
				  		const result = await returnData(obj_values);
					}
					 
					const returnData = x => {
					  return new Promise((resolve, reject) => {
					    setTimeout(() => {
					      resolve(x);
					    }, 300);
					  });
					}
					 
					forEach().then(() => {

						arrValues.push(obj_values);
					
					})
		  		}

			});
					
		}else if(input_type == 'select_range'){
	

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_jazestore_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var min_field_label;
					var min_field_value;
					var max_field_label;
					var max_field_value;
					var interval_value;
					var suffix_field_label;
					var separator_field_label;

					const forEach = async () => {
					  	const values = data;
					  	for (const vala of values){

							if(vala.key_name == 'separator label'){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == 'min label'){
								min_field_label = vala.key_value;
							}


							if(vala.key_name == 'min value'){
								min_field_value = vala.key_value;
							}

							if(vala.key_name == 'max label'){
								max_field_label = vala.key_value;
							}


							if(vala.key_name == 'max value'){
								max_field_value = vala.key_value;
							}

							if(vala.key_name == 'interval value'){
								interval_value = vala.key_value;
							}


							if(vala.key_name == 'suffix'){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								id:'0',
								min_field_label:min_field_label,
								min_field_value:min_field_value,
								max_field_label:max_field_label,
								max_field_value:max_field_value,
								internal_value:interval_value,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

					  	}
				  		const result = await returnData(obj_values);
					}
					 
					const returnData = x => {
					  return new Promise((resolve, reject) => {
					    setTimeout(() => {
					      resolve(x);
					    }, 300);
					  });
					}
					 
					forEach().then(() => {
						arrValues.push(obj_values);
					
					})
		  		}

			});
					
		}

		setTimeout(function(){ 

			if(singleObj.includes(input_type)){
				return_result(obj_values);
			}else{
				return_result(arrValues.sort(sortingArrayObject('title')));
			}
			
		}, 500);

	}
	catch(err)
	{ console.log("getCategoryAttributeValueAuto",err); return_result([]); }	
};


methods.postCars = function(arrParams,return_result){

	try
	{	

		var queGroupMax = "SELECT MAX(product_id) AS lastGrpId FROM jaze_products_meta";
		var lastGrpId = 0;

		methods.getLastGroupId(queGroupMax, function(queGrpRes){

			lastGrpId = queGrpRes[0].lastGrpId;

			var accParams = {account_id:arrParams.account_id};

			methods.getCompanyProfile(accParams, function(retAccount){

				if(retAccount!=null)
				{
					var company_category_id = '';
					if(retAccount.category_id){
						company_category_id = retAccount.category_id;
					}

				  	var category_id;
				  	var jaze_category_all;

				  	if(parseInt(arrParams.flag) === 1){

						category_id = '57';
						jaze_category_all = '56,57';

				  	}else if(parseInt(arrParams.flag) === 1){

						category_id = '58';
						jaze_category_all = '56,58';

				  	}else if(parseInt(arrParams.flag) === 1){

						category_id = '59';
						jaze_category_all = '56,59'
				  	}

				  	var arrMainInsert = [];

					var objMainInsert = {

						account_id  : arrParams.account_id,
						name        : arrParams.title,
						title       : arrParams.title,
						ribbon      : '',
						content     : '',
						category_id : 0,
						company_category_id   : company_category_id,
						jaze_category_id      : category_id,
						jaze_category_all     : jaze_category_all,
						jaze_brand_id  : 0,
						jaze_instore   : 1,
						keywords       : '',
						product_type   : 'product',
						currency_id    : retAccount.currency,
						visibility     : 1,
						agent_id       : 0,
						parent_id      : 0,
						
						country_id     : arrParams.country_id,
						state_id       : arrParams.state_id,
						city_id        : arrParams.city_id,
						area_id        : arrParams.area_id,
						created_date   : newdateForm,
						modified_date  : newdateForm
						
					};

					arrMainInsert.push(objMainInsert);



					var arrAttributes_filters = [];
					var objAttributes_filters = {};
					var objAttributes_filters_insert = {};
					var arrAttributes_filters_insert = [];

			

	  				for (var param of arrParams.attributes){

						objAttributes_filters_insert['attribute_filter_'+param.attribute_id] = param.value

						if(parseInt(param.attribute_id) === 23 ){
							objAttributes_filters[param.attribute_id] = 'export_cars';
						}
						if(parseInt(param.attribute_id) === 24 ){
							objAttributes_filters[param.attribute_id] = 'transmission';
						}
						if(parseInt(param.attribute_id) === 64 ){
							objAttributes_filters[param.attribute_id] = 'seller_type';
						}
						if(parseInt(param.attribute_id) === 65 ){
							objAttributes_filters[param.attribute_id] = 'price';
						}
						if(parseInt(param.attribute_id) === 66 ){
							objAttributes_filters[param.attribute_id] = 'year';
						}
						if(parseInt(param.attribute_id) === 67 ){
							objAttributes_filters[param.attribute_id] = 'kilometers';			
						}
						if(parseInt(param.attribute_id) === 69 ){
							objAttributes_filters[param.attribute_id] = 'condition';			
						}
						if(parseInt(param.attribute_id) === 70 ){
							objAttributes_filters[param.attribute_id] = 'fuel';
						}	
						if(parseInt(param.attribute_id) === 71 ){
							objAttributes_filters[param.attribute_id] = 'color';
						}	
						if(parseInt(param.attribute_id) === 72 ){
							objAttributes_filters[param.attribute_id] = 'gcc';					
						}	
						if(parseInt(param.attribute_id) === 73 ){
							objAttributes_filters[param.attribute_id] = 'make';
						}	
						if(parseInt(param.attribute_id) === 74 ){
							objAttributes_filters[param.attribute_id] = 'car_type';
						}	
						if(parseInt(param.attribute_id) === 75 ){
							objAttributes_filters[param.attribute_id] = 'no_of_cylinders';
						}	
						if(parseInt(param.attribute_id) === 76 ){
							objAttributes_filters[param.attribute_id] = 'no_of_seats';
						}	
						if(parseInt(param.attribute_id) === 91 ){
							objAttributes_filters[param.attribute_id] = 'model';
						}	

						arrAttributes_filters.push(objAttributes_filters);
						arrAttributes_filters_insert.push(objAttributes_filters_insert);

	  				}

  					var arrAttributes_filters = php.serialize(arrAttributes_filters[0]);


					for (var rows in arrMainInsert) 
				 	{	

						DB.query("INSERT INTO jaze_products SET ?",[arrMainInsert[rows]], function(mData, error){
								
						 	if(mData!=null){

	 							var arrImages = php.serialize(arrParams.images.split(","));

								var arrMetaInsert = {

									sku                    : '',
									regular_price          : arrParams.price,
									sale_price             : arrParams.price+'.00',
									sale_discount          : '',
									sale_onsale            : '',
									sale_onsale_schedule   : '',
									sale_price_start_from  : '',
									sale_price_start_to    : '',
									featured_image         : arrParams.images.split(",")[0],	
									gallery_images         : arrImages,
									stocks                 : '',
									stocks_status          : 1,
									manage_stocks          : 0,
									default_attributes     : '',
									product_attributes     : '',
									weight                 : '',
									height                 : '',
									length                 : '',
									product_sections       : '',
									attribute_filters      : arrAttributes_filters
								}

								let merged = {...arrMetaInsert, ...arrAttributes_filters_insert[0]};

								var querInsert = `INSERT INTO jaze_products_meta (product_id, meta_key, meta_value) VALUES (?,?,?)`;
				 			 	for (var key in merged) 
			 					{
									DB.query(querInsert,[mData.insertId,key,merged[key]], function(data, error){
							 		 	if(parseInt(data.affectedRows) > 0){
					 						return_result(1);
								 	   	}else{
						   					return_result(0);
								 	   	}
							 	 	});
							  	};

					 	   	}else{
			   					return_result(0);
					 	   	}
				 	 	});

				  	};

				}
			})
		});

	}
	catch(err)
	{ console.log("postCars",err); return_result(0);}

};

// --------------------------------------------------- Post cars end  ------------------------------------------ //


methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
};


methods.__getCountryDetails = function(arrParams,return_result){

	try{	

			if(arrParams ==''){
				arrParams = 0;
			}

			let query  = " SELECT country_name FROM country  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}

};

methods.__getStateDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT state_name FROM states  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].state_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getCityDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT city_name FROM cities  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].city_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};


methods.__getAreaDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT area_name FROM area  WHERE area_id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].area_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].group_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}


methods.convertMultiMetaArrayCompany =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndexCompany(list,arrvalues[i].account_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndexCompany(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].account_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}

function sortingArrayObject(key, order='asc') {

	return function(a, b) {
	  if(!a.hasOwnProperty(key) || 
		 !b.hasOwnProperty(key)) {
		  return 0; 
	  }
	  
	  const varA = (typeof a[key] === 'string') ? 
		a[key].toUpperCase() : a[key];
	  const varB = (typeof b[key] === 'string') ? 
		b[key].toUpperCase() : b[key];
		
	  let comparison = 0;
	  if (varA > varB) {
		comparison = 1;
	  } else if (varA < varB) {
		comparison = -1;
	  }
	  return (
		(order == 'desc') ? 
		(comparison * -1) : comparison
	  );
	};
}


function distance(lat1, lon1, lat2, lon2, unit) {

	if ((lat1 == lat2) && (lon1 == lon2)) {
		return 0;
	}
	else 
	{
		var radlat1 = Math.PI * lat1/180;
		var radlat2 = Math.PI * lat2/180;
		var theta = lon1-lon2;
		var radtheta = Math.PI * theta/180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist);
		dist = dist * 180/Math.PI;
		dist = dist * 60 * 1.1515;
		if (unit.toString()=="K") { dist = dist * 1.609344 }
		if (unit.toString()=="N") { dist = dist * 0.8684 }
		return dist;
	}
}



methods.getFileTypes = function(ext,return_result){

	var type;

	var arrImages = ['.jpeg','.jpg','.gif','.png','.tif'];
	var arrVideos = ['.mp4','.avi','.mov','.flv','.wmv'];
	var arrAudio = ['.pcm','.wav','.aiff','.mp3','.aac','.wma','.flac','.alac'];
	var arrDocs = ['.odt','.xls','.xlsx','.ods','.ppt','.pptx','.txt','.doc'];
	var arrPdf = ['.pdf'];

	if(arrImages.includes(ext.toLowerCase())){
		type = '2';
	}else if(arrAudio.includes(ext.toLowerCase())){
		type = '3';
	}else if(arrVideos.includes(ext.toLowerCase())){
		type = '4';
	}else if(arrDocs.includes(ext.toLowerCase())){
		type = '5';
	}else if(arrPdf.includes(ext.toLowerCase())){
		type = '6';
	}else{
		type = '7';
	}

	return_result(type);
};


function mapOrder (array, order, key) {
  
  array.sort( function (a, b) {
    var A = a[key], B = b[key];
    
    if (order.indexOf(A) > order.indexOf(B)) {
      return 1;
    } else {
      return -1;
    }
    
  });
  
  return array;
};


function removeDuplicates(array) {
  return array.filter((a, b) => array.indexOf(a) === b)
};


function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}


module.exports = methods;
