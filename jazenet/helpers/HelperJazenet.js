const methods = {};
const DB = require('../../helpers/db_base.js');
const HelperGeneral = require('../../helpers/HelperGeneral.js');
const HelperGeneralJazenet = require('../helpers/HelperGeneralJazenet.js');
const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperJazechat = require('../../jazecom/helpers/HelperJazechat.js');
const HelperJazecom = require('../../jazecom/helpers/HelperJazecom.js');
const HelperJazeMail = require('../../jazecom/helpers/HelperJazeMail.js');
const promiseForeach = require('promise-foreach');
const moment = require("moment");
const fast_sort = require("fast-sort");
const crypto = require('crypto');


//------------------------------------------------------------------- Credential Module Start ---------------------------------------------------------------------------//
/*
 * check email and password
 */
methods.checkCredentialDetails = function (arrParams, return_result) {

	try {
		var jazemail_id = arrParams.jazemail_id;

		if (!emailIsValid(arrParams.jazemail_id)) {
			jazemail_id = jazemail_id + "@jazenet.com";
		}

		var query = " select account_id from jaze_user_credential_details where jazemail_id= '" + jazemail_id + "' and password = '" + arrParams.password + "' and status = '1' and account_type = '1' ";

		DB.query(query, null, function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

				return_result(data[0].account_id);
			} else {
				return_result('0');
			}
		});
	}
	catch (err) { console.log("checkCredentialDetails", err); return_result('0'); }
};

//------------------------------------------------------------------- Credential Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Device Module Start ---------------------------------------------------------------------------//

/*
 * save Device Details
 */
methods.saveAppDeviceDetails = function (arrParams, return_result) {

	try {
		if (arrParams !== null) {
			var query = "UPDATE `jaze_app_device_details` SET status = '0' WHERE status = '1' AND  app_type = '1' AND device_id = '" + arrParams.device_id + "' ";

			DB.query(query, null, function (data, error) {

				//save Device
				var retAPIToken = generateMobileAPI();

				if (retAPIToken !== null) {
					const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

					var query = " INSERT INTO jaze_app_device_details SET ";
					query += " account_id = '" + arrParams.account_id + "', ";
					query += " account_type = '1', ";
					query += " app_type  = '1', ";
					query += " parent_id = '0', ";
					query += " device_id = '" + arrParams.device_id + "', ";
					query += " device_name = '" + arrParams.device_name.replace(/['"]+/g, '') + "', ";
					query += " device_type = '" + arrParams.device_type + "', ";
					query += " fcm_token = '" + arrParams.device_fcm_token + "', ";
					query += " api_token = '" + retAPIToken + "', ";
					query += " create_date = '" + standardDTUTC + "', ";
					query += " last_update_date = '" + standardDTUTC + "', ";
					query += " status  = '1' ";

					DB.query(query, null, function (data, error) {

						if (data !== null) {

							if (parseInt(data.insertId) !== 0) {

								var objReturn = {
									device_details: {
										app_id: data.insertId.toString(),
										api_token: retAPIToken,
										account_id: arrParams.account_id.toString(),
									}
								}

								return_result(objReturn);
							}
							else {
								return_result(null);
							}
						}
						else {
							return_result(null);
						}
					});
				}
				else {
					return_result(null);
				}
			});
		}
		else { return_result(null); }
	}
	catch (err) { console.log("saveAppDeviceDetails", err); return_result(null); }
};

/*
 * update Device Details
 */
methods.updateAppDeviceDetails = function (flag, arrParams, return_result) {

	try {
		if (parseInt(flag) !== 0 && arrParams !== null) {
			var retAPIToken = generateMobileAPI();

			if (retAPIToken !== null) {
				const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

				var query = " UPDATE jaze_app_device_details SET ";

				if (parseInt(flag) === 1) {
					//login
					query += " account_id = '" + arrParams.account_id + "', ";
					query += " account_type = '" + arrParams.account_type + "', ";
				}
				else if (parseInt(flag) === 2) {
					//Update Device
					query += " fcm_token = '" + arrParams.device_fcm_token + "', ";
				}

				query += " api_token = '" + retAPIToken + "', ";
				query += " last_update_date = '" + standardDTUTC + "' ";
				query += " where  status = '1' ";
				query += " and app_type = 1  ";
				query += " and id = '" + arrParams.app_id + "'  ";

				DB.query(query, null, function (data, error) {

					var objReturn = {
						device_details: {
							app_id: arrParams.app_id.toString(),
							api_token: retAPIToken,
							account_id: arrParams.account_id.toString(),
						}
					};

					return_result(objReturn);
				});
			}
			else { return_result(null); }
		}
		else { return_result(null); }
	}
	catch (err) { console.log("updateAppDeviceDetails", err); return_result(null); }
};

/*
 * remove Device Details
 */
methods.removeAppDeviceDetails = function (arrParams, return_result) {

	try {
		if (arrParams !== null) {
			//checking already exists
			var query = "SELECT * FROM jaze_app_device_details WHERE status = '1' AND  app_type = '1' AND id = '" + arrParams.app_id + "' ";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

					var retAPIToken = generateMobileAPI();

					if (retAPIToken !== null) {
						var app_data = data[0];

						const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

						//Update Master account and child accounts
						var query = "UPDATE `jaze_app_device_details` SET status = '0' WHERE status = '1' AND  app_type = '1' AND (id = '" + arrParams.app_id + "' or parent_id = '" + arrParams.app_id + "') ";

						DB.query(query, null, function (data, error) {

							//insert Gust Accounts
							var query = " INSERT INTO jaze_app_device_details SET ";
							query += " account_id = '0', ";
							query += " account_type = '0', ";
							query += " app_type = '1', ";
							query += " parent_id = '0', ";
							query += " device_id = '" + app_data.device_id + "', ";
							query += " device_name = '" + app_data.device_name + "', ";
							query += " device_type = '" + app_data.device_type + "', ";
							query += " fcm_token = '" + app_data.fcm_token + "', ";
							query += " api_token = '" + retAPIToken + "', ";
							query += " create_date = '" + standardDTUTC + "', ";
							query += " last_update_date = '" + standardDTUTC + "', ";
							query += " status  = '1' ";

							DB.query(query, null, function (data, error) {

								var objReturn = {
									device_details: {
										app_id: data.insertId.toString(),
										api_token: retAPIToken,
										account_id: '0',
									},
									master_account_details: {},
									child_account_details: [],
									link_account_details: []
								};

								return_result(objReturn);
							});
						});
					}
					else { return_result(null); }
				}
				else { return_result(null); }
			});
		}
		else { return_result(null); }
	}
	catch (err) { console.log("removeAllDeviceDetails", err); return_result(null); }
};

//------------------------------------------------------------------- Device Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Link Account Module Start ---------------------------------------------------------------------------//

/*
 * get Master, Link and Child Account List
 */
methods.getMasterChildLinkAccountList = function (arrParams, return_result) {
	if (arrParams !== null) {
		var obj = {};

		//get master account details

		var reqParams = {
			account_id: arrParams.master_account_id,
			active_account_id: arrParams.account_id,
			active_account_type: arrParams.account_type,
			group_id: "0",
			master_status: "1"
		};

		methods.___getAccountList(reqParams, function (masterList) {

			obj = { master_account_details: masterList };

			methods.__getMasterChildAccountList(arrParams, function (childList) {
				
				methods.__deleteDeviceDetails(arrParams, function (deleteStatus) {

					methods.__saveDeviceDetails(arrParams, childList.account_list, function (statuschild) {

						var childaccount = arrParams.master_account_id;

						if (childList !== null) {
							childaccount = arrParams.master_account_id + "," + childList.account_id;

							Object.assign(obj, { child_account_details: childList.account_list });
						}
						else {
							Object.assign(obj, { child_account_details: [] });
						}
						
						methods.__getMasterLinkAccountList(arrParams, childaccount, function (LinkList) {
							
							methods.__saveDeviceDetails(arrParams, LinkList, function (statuschild) {
								
								if (LinkList !== null) {
									Object.assign(obj, { link_account_details: LinkList });
								}
								else {
									Object.assign(obj, { link_account_details: [] });
								}
								
								return_result(obj);

							});
						});
					});
				});
			});
		});
	}
	else { return_result(null); }
}

methods.___getAccountList = function (reqParams, return_result) {
	try {
		if (reqParams !== null) {
			HelperGeneral.getAccountDetails(reqParams, function (accRres) {

				if (accRres !== null) {
					var active_status = "0";

					var flag = "0";

					if (parseInt(reqParams.active_account_id) === parseInt(accRres.account_id) && parseInt(reqParams.active_account_type) === parseInt(accRres.account_type)) { active_status = "1"; }

					if (parseInt(reqParams.master_status) === 1) { flag = "1"; }
					else if (parseInt(reqParams.group_id) === 0) { flag = "2"; }
					else if (parseInt(reqParams.group_id) !== 0) { flag = "3"; }

					var newObj = {
						account_id: accRres.account_id,
						account_type: accRres.account_type,
						name: accRres.name,
						logo: accRres.logo,
						category_name: accRres.category,
						title: accRres.title,
						mobile_code: accRres.mobile_code,
						mobile_no: accRres.mobile_no,
						email: accRres.email,
						country: accRres.country_name,
						state: accRres.state_name,
						city: accRres.city_name,
						area: accRres.area_name,
						group_id: reqParams.group_id.toString(),
						active_status: active_status,
						master_status: reqParams.master_status,
						flag: flag
					};

					return_result(newObj);
				}
				else { return_result(null); }
			});
		}
		else { return_result(null); }
	}
	catch (err) { console.log("___getAccountList", err); return_result(null); }
}

methods.__getMasterChildAccountList = function (arrParams, return_result) {
	try 
	{
		if (parseInt(arrParams.master_account_id) !== 0) 
		{
			var query = "SELECT `prof_id`,`company_id`,`work_id`  FROM `jaze_user_account_details` WHERE `status` = '1' and `user_id` = '" + arrParams.master_account_id + "' ";
			
			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

					var reqProf = {};

					if (parseInt(data[0].prof_id) !== 0) {
						reqProf = {
							account_id: data[0].prof_id,
							active_account_id: arrParams.account_id,
							active_account_type: arrParams.account_type,
							group_id: "0",
							master_status: "0"
						};
					}
					else {
						reqProf = null;
					}

					methods.___getAccountList(reqProf, function (profAccRres) {

						var reqComp = {};

						if (parseInt(data[0].company_id) !== 0) {
							reqComp = {
								account_id: data[0].company_id,
								active_account_id: arrParams.account_id,
								active_account_type: arrParams.account_type,
								group_id: "0",
								master_status: "0"
							};
						}
						else {
							reqComp = null;
						}

						methods.___getAccountList(reqComp, function (companyAccRres) {

							var reqWork = {};

							if (parseInt(data[0].work_id) !== 0) {
								reqWork = {

									account_id: data[0].work_id,
									active_account_id: arrParams.account_id,
									active_account_type: arrParams.account_type,
									group_id: "0",
									master_status: "0"
								};
							}
							else {
								reqWork = null;
							}

							methods.___getWorkAccountDetails(reqWork, function (workAccRres) {

								var account_id = "";

								var account_list = [];

								if (profAccRres != null) {
									account_id = profAccRres.account_id;

									account_list.push(profAccRres);
								}

								if (workAccRres != null) {
									account_id = (account_id == "") ? workAccRres.account_id : account_id + "," + workAccRres.account_id;

									account_id = account_id+","+workAccRres.company_id;

									account_list.push(workAccRres);
								}

								if (companyAccRres != null) {
									if (workAccRres != null) {
										if (parseInt(companyAccRres.account_id) !== parseInt(workAccRres.company_id)) {
											account_list.push(companyAccRres);
										}
									}
									else {
										account_list.push(companyAccRres);
									}

									account_id = (account_id == "") ? companyAccRres.account_id : account_id + "," + companyAccRres.account_id;
								}


								if (account_list != null) {
									var obj = {
										account_id: account_id,
										account_list: fast_sort(account_list).asc('account_type')
									};

									return_result(obj);
								}
								else { return_result(null); }

							});
						});
					});
				}
				else { return_result(null); }
			});
		}
		else { return_result(null); }
	}
	catch (err) { console.log("getMasterChildAccounts", err); return_result(null); }
}

methods.__getMasterLinkAccountList = function (arrParams, child_account_id, return_result) {
	try {
		if (parseInt(arrParams.master_account_id) !== 0) {

			var query = " SELECT * FROM jaze_user_link_account_details WHERE `group_id` in (SELECT group_id FROM jaze_user_link_account_details WHERE  `meta_key` = 'account_id' and `meta_value` = '" + arrParams.master_account_id + "' and  `group_id` in (SELECT group_id FROM jaze_user_link_account_details WHERE  `meta_key` = 'status' and `meta_value` = '1' ";
			query += " and  `group_id` in (SELECT group_id FROM jaze_user_link_account_details WHERE  `meta_key` = 'link_account_id' and `meta_value` not in (" + child_account_id + "))  ";
			query += " ))";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

					HelperRestriction.convertMultiMetaArray(data, function (data_result) {

						if (data_result !== null) {
							var counter = data_result.length;

							var array_rows = [];

							if (parseInt(counter) !== 0) {
								promiseForeach.each(data_result, [function (list) {

									return new Promise(function (resolve, reject) {

										var reqlink = {
											account_id: list.link_account_id,
											active_account_id: arrParams.account_id,
											active_account_type: arrParams.account_type,
											group_id: list.group_id,
											master_status: "0"
										};
										
										methods.___getAccountList(reqlink, function (masterList) {
											
											if (masterList !== null) {
												array_rows.push(masterList);
											}

											resolve(array_rows);

											counter -= 1;

										});
									})
								}],
									function (result, current) {

										if (counter === 0) {return_result(result[0]); }
									});
							}
							else { return_result(null); }
						}
						else { return_result(null); }

					});
				}
				else { return_result(null); }
			});
		}
		else { return_result(null); }
	}
	catch (err) { console.log("getMasterChildAccounts", err); return_result(null); }
}

methods.__saveDeviceDetails = function (arrParams, account_list, return_result) {

	try {
		if (arrParams !== null && account_list !== null && parseInt(account_list.length) > 0) {
			//checking already exists
			var query = "SELECT * FROM jaze_app_device_details WHERE status = '1' AND  app_type = '1' AND id = '" + arrParams.app_id + "' ";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

					var app_data = data[0];

					const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

					var value_query = "";

					account_list.forEach(function (row) {
						//
						if(value_query !== "")
						{
							value_query += ",";
						}

						value_query += "('1','" + row.account_id + "','" + row.account_type + "','" + arrParams.app_id + "','" + app_data.device_id + "','" + app_data.device_name + "','" + app_data.device_type + "', ";
						value_query += " '" + app_data.fcm_token + "','" + app_data.api_token + "','" + standardDTUTC + "','" + standardDTUTC + "','1')";
					});
					
					if (value_query !== "") {

						var query = " INSERT INTO jaze_app_device_details  ";
							query += " (`app_type`, `account_id`, `account_type`, `parent_id`, `device_id`, `device_name`, `device_type`, `fcm_token`, `api_token`, `create_date`, `last_update_date`, `status`)";
							query += " VALUES ";
							query += value_query;

							DB.query(query, null, function (data, error) { 

								return_result(null);
							});
					}
				}
				else { return_result(null); }
			});
		}
		else { return_result(null); }
	}
	catch (err) { console.log("__saveDeviceDetails", err); return_result(null); }
};

methods.__deleteDeviceDetails = function (arrParams, return_result) {

	try {
		if (arrParams !== null) {
			//checking already exists
			var query = "DELETE FROM `jaze_app_device_details` WHERE `parent_id`  = '" + arrParams.app_id + "' ";

			DB.query(query, null, function (data, error) {
				return_result(1);
			});
		}
		else { return_result(null); }
	}
	catch (err) { console.log("__deleteDeviceDetails", err); return_result(null); }
};

methods.___getWorkAccountDetails = function (reqParams, return_result) {
	try {
		if (reqParams !== null) {
			var query = " SELECT *  FROM jaze_user_basic_details WHERE meta_key in ('team_fname','team_lname','team_display_pic','team_position','team_company_id') and  account_type = '4'  and account_id = '" + reqParams.account_id + "' and account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key = 'team_status' and meta_value = '1') ";

			DB.query(query, null, function (data, error) {

				if (typeof data[0] !== 'undefined' && data[0] !== null) {
					var list = [];

					Object.assign(list, { account_id: data[0].account_id });
					Object.assign(list, { account_type: data[0].account_type });

					for (var i in data) {
						Object.assign(list, { [data[i].meta_key]: data[i].meta_value });
					}

					var accParams = { account_id: list.team_company_id };

					HelperGeneral.getAccountDetails(accParams, function (compAccRres) {

						HelperGeneral.__getMobileCode(list, function (mobile_code) {

							var active_status = "0";

							var flag = "0";

							if (parseInt(reqParams.active_account_id) === parseInt(list.account_id) && parseInt(reqParams.active_account_type) === 4) { active_status = "1"; }

							if (parseInt(reqParams.master_status) === 1) { flag = "1"; }
							else if (parseInt(reqParams.group_id) === 0) { flag = "2"; }
							else if (parseInt(reqParams.group_id) !== 0) { flag = "3"; }

							var newObj = {
								account_id: list.account_id.toString(),
								account_type: list.account_type.toString(),
								name: list.team_fname + ' ' + list.team_lname,
								logo: G_STATIC_IMG_URL + 'uploads/jazenet/' + list.team_display_pic,
								category_name: list.team_position,
								title: "work",
								mobile_code: mobile_code.toString(),
								mobile_no: list.team_mobile_no,
								email: list.team_email,
								company_id: compAccRres.account_id.toString(),
								country: compAccRres.country_name,
								state: compAccRres.state_name,
								city: compAccRres.city_name,
								area: compAccRres.area_name,
								group_id: reqParams.group_id.toString(),
								active_status: active_status,
								master_status: reqParams.master_status,
								flag: flag
							};

							return_result(newObj);
						});
					});
				}
				else { return_result(null); }
			});
		}
		else { return_result(null); }
	}
	catch (err) { console.log("___getWorkAccountDetails", err); return_result(null); }
}

//------------------------------------------------------------------- Link Account Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Home Module Start ---------------------------------------------------------------------------//

methods.getActiveAccountDetails = function (reqParams, return_result) {
	try {
		if (parseInt(reqParams.active_account_id) !== 0) {

			if (parseInt(reqParams.active_account_type) === 4) {
				//work profile
				var accParams = { account_id: reqParams.active_account_id };

				HelperGeneralJazenet.getAccountDetails(accParams, function (accRres) {
					
					if (accRres !== null) {
						//check is he admin or not
						methods.__getWorkAccountAdminStatus(reqParams, accRres.company_id, function (admin_status) {

							var objUser = {
								account_id: accRres.account_id,
								account_type: accRres.account_type,
								category: accRres.category,
								name: accRres.name,
								title: accRres.title,
								profession: '',
								first_name: accRres.fname,
								last_name: accRres.lname,
								logo: accRres.logo,
								admin_status: admin_status.toString(),
								admin_id: '0',
								admin_name: '',
								admin_logo: '',
								company_id: accRres.company_id,
								company_type: accRres.company_type,
								company_name: accRres.company_name,
								company_logo: accRres.company_logo,
								dob: accRres.dob,
								email: accRres.email,
								jazenet_id: accRres.jazenet_id,
								mobile_code: accRres.mobile_code,
								mobile_no: accRres.mobile_no,
								nationality_id: accRres.nationality_id,
								nationality: accRres.nationality,
								country_id: accRres.country_id,
								country_name: accRres.country,
								state_id: accRres.state_id,
								state_name: accRres.state,
								city_id: accRres.city_id,
								city_name: accRres.city,
								area_id: accRres.area_id,
								area_name: accRres.area,
								currency_id: accRres.currency_id.toString(),
								currency: accRres.currency
							};

							return_result(objUser);
						});
					}
					else { return_result({}); }
				});
			}
			else {
				var objUser = {};

				var accParam = { account_id: reqParams.active_account_id };
				
				HelperGeneralJazenet.getAccountDetails(accParam, function (accRres) {
					
					if (accRres !== null) {

						methods.__getCompanyAccountAdminDetails(accRres, function (adminRres) {

							if (parseInt(accRres.account_type) === 2) {

								objUser = {
									account_id: accRres.account_id,
									account_type: accRres.account_type,
									category: accRres.category,
									name: accRres.name,
									title: accRres.title,
									profession: accRres.profession,
									first_name: accRres.fname,
									last_name: accRres.lname,
									logo: accRres.logo,
									admin_status: '0',
									company_id: '0',
									company_type: '0',
									company_name: '',
									company_logo: '',
									dob: accRres.dob,
									email: accRres.email,
									jazenet_id: accRres.jazenet_id,
									mobile_code: accRres.mobile_code,
									mobile_no: accRres.mobile_no,
									nationality_id: accRres.nationality_id,
									nationality: accRres.nationality,
									country_id: accRres.country_id,
									country_name: accRres.country,
									state_id: accRres.state_id,
									state_name: accRres.state,
									city_id: accRres.city_id,
									city_name: accRres.city,
									area_id: accRres.area_id,
									area_name: accRres.area,
									currency_id: accRres.currency_id.toString(),
									currency: accRres.currency
								};

							}
							else {

								objUser = {
									account_id: accRres.account_id,
									account_type: accRres.account_type,
									category: accRres.category,
									title: accRres.title,
									name: accRres.name,
									logo: accRres.logo,
									admin_status: '0',
									admin_id: adminRres.admin_id,
									admin_name: adminRres.admin_name,
									admin_logo: adminRres.admin_logo,
									company_id: '0',
									company_type: '0',
									company_name: '',
									company_logo: '',
									dob: accRres.dob,
									email: accRres.email,
									jazenet_id: accRres.jazenet_id,
									mobile_code: accRres.mobile_code,
									mobile_no: accRres.mobile_no,
									nationality_id: accRres.nationality_id,
									nationality: accRres.nationality,
									country_id: accRres.country_id,
									country_name: accRres.country,
									state_id: accRres.state_id,
									state_name: accRres.state,
									city_id: accRres.city_id,
									city_name: accRres.city,
									area_id: accRres.area_id,
									area_name: accRres.area,
									currency_id: accRres.currency_id.toString(),
									currency: accRres.currency
								};
							}

							return_result(objUser);
						});
					}
					else { return_result({}); }
				});
			}
		}
		else { return_result({}); }
	}
	catch (err) { console.log("getActiveAccountDetails", err); return_result({}); }
};

methods.getHomeNotificatioNCount = function (account_id, return_result) {

	var return_obj = {
		total_count: "0",
		mail: "0",
		call: "0",
		feed: "0",
		pay: "0",
		notice: "0",
		diary: "0"
	};

	try {
		if (parseInt(account_id) !== 0) {
			//mail
			methods.__getNotificationCount(112, account_id, function (mailCount) {

				//call
				// methods.__getNotificationCount(112,account_id, function(callCount){	

				//feed
				methods.__getNotificationCount(105, account_id, function (feedCount) {

					//feed Replay
					methods.__getNotificationCount(106, account_id, function (feedReplayCount) {

						//pay
						// methods.__getNotificationCount(119,account_id, function(noticeCount){	

						//notice
						methods.__getNotificationCount(119, account_id, function (noticeCount) {

							//diary appointment received
							methods.__getNotificationCount(107, account_id, function (diaryAppReciCount) {

								//diary appointment accepted
								methods.__getNotificationCount(108, account_id, function (diaryAppAcceptCount) {

									//diary appointment rejected
									methods.__getNotificationCount(109, account_id, function (diaryAppRejectCount) {

										return_obj = {
											total_count: (parseInt(mailCount) + parseInt(feedCount) + parseInt(feedReplayCount) + parseInt(noticeCount) + parseInt(diaryAppReciCount) + parseInt(diaryAppAcceptCount) + parseInt(diaryAppRejectCount)).toString(),
											mail: mailCount.toString(),
											call: "0",
											feed: (parseInt(feedCount) + parseInt(feedReplayCount)).toString(),
											pay: "0",
											notice: noticeCount.toString(),
											diary: (parseInt(diaryAppReciCount) + parseInt(diaryAppAcceptCount) + parseInt(diaryAppRejectCount)).toString()
										};

										return_result(return_obj);

									});
								});
							});
						});
						// });
					});
				});
				// });
			});
		}
		else { return_result(return_obj); }
	}
	catch (err) {
		console.log("getNotificatioNCount", err); return_result(return_obj);
	}
};

methods.__getNotificationCount = function (type, account_id, return_result) {
	try {
		if (parseInt(type) !== 0 && parseInt(account_id) !== 0) {
			var query = " SELECT group_id FROM `jaze_notification_list`  WHERE  ";
			query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'status' AND meta_value = 1)   AND  ";
			query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'notify_type' AND meta_value = '" + type + "') AND  ";
			query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'read_status' AND meta_value = '0') AND  ";
			query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'receiver_account_id' AND meta_value = '" + account_id + "') AND ";
			query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'create_date' AND meta_value >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)) ";
			query += " group by group_id  ";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data) !== 0) {

					return_result(data.length);
				}
				else { return_result("0"); }
			});
		}
		else { return_result("0"); }
	}
	catch (err) { console.log("__getNotificationCount", err); return_result("0"); }
};

methods.getHomeMenuList = function (reqParams, return_result) {

	try {
		if (reqParams !== null && parseInt(reqParams.active_account_type) !== 3 && parseInt(reqParams.active_account_type) !== 5) {
			var query = " SELECT meta_value FROM `jaze_home_page_cat_list_values`  WHERE  meta_key in ('cat_id') AND ";
			query += " group_id IN (SELECT group_id FROM jaze_home_page_cat_list_values WHERE meta_key = 'status' AND meta_value = 1  AND  ";
			query += " group_id IN (SELECT group_id FROM jaze_home_page_cat_list_values WHERE meta_key = 'account_type' AND meta_value = '" + reqParams.active_account_type + "'))  ";
			query += " group by group_id  ";

			DB.query(query, null, function (listData, error) {

				if (typeof listData !== 'undefined' && listData !== null && parseInt(listData) !== 0) {

					//Get Cat list 
					var query = " SELECT category_list FROM jaze_home_cat_order_list WHERE status = '1' and account_id = '" + reqParams.active_account_id + "' ";

					DB.query(query, null, function (userList, error) {

						methods.__getHomeMenuCatList(listData[0].meta_value, function (menuList) {

							return_result(mapCatOrder(menuList, userList));

						});
					});
				}
				else { return_result([]); }
			});
		}
		else { return_result([]); }

	}
	catch (err) { console.log("__getMenuJaznet", err); return_result(null); }
};

methods.__getHomeMenuCatList = function (cat_list, return_result) {
	try {
		if (cat_list !== "") {
			//Account Access Menu
			var result_flag1 = [15, 16, 19, 22, 24, 29, 30, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 49, 50, 51];

			//Portls
			var result_flag2 = [2, 3, 4, 5, 12];

			//Other
			var result_flag3 = [1, 6, 7, 8, 9, 10, 11];

			var query = " SELECT * FROM jaze_home_page_cat_list WHERE status = '1' and  id IN (" + cat_list + ") order by sort_order ";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data) !== 0) {

					var counter = data.length;

					var array_rows = [];

					if (parseInt(counter) !== 0) {
						promiseForeach.each(data, [function (list) {

							return new Promise(function (resolve, reject) {

								var flag = '';

								if (result_flag1.indexOf(parseInt(list.id)) !== -1) {
									flag = '1';
								}
								else if (result_flag2.indexOf(parseInt(list.id)) !== -1) {
									flag = '2';
								}
								else if (result_flag3.indexOf(parseInt(list.id)) !== -1) {
									flag = '3';
								}

								var default_logo = "";

								if (list.image_url !== "" && list.image_url !== null) { default_logo = G_STATIC_IMG_URL + 'uploads/' + list.image_url; }

								var newObj = {
									id: list.id.toString(),
									name: list.cate_name.toString(),
									image: default_logo,
									flag: flag.toString(),
									status: '1'
								};

								array_rows.push(newObj);

								counter -= 1;

								resolve(array_rows);

							});
						}],
							function (result, current) {

								if (counter === 0) { return_result(result[0]); }
							});
					}
					else { return_result(null); }
				}
				else { return_result(null); }
			});
		}
		else { return_result(null); }
	}
	catch (err) { console.log("__getHomeMenuCatList", err); return_result(null); }
};


methods.getAccessMenuList = function (reqParams, return_result) {

	try {

		if (parseInt(reqParams.active_account_type) === 3 || parseInt(reqParams.active_account_type) === 5) {

			methods.getCompanyAccessMenuDetails(reqParams.active_account_type, function (menuList) {

				return_result(menuList);
			});
		}
		else if (parseInt(reqParams.active_account_type) === 4) {
			//get group id and company id

			var query = " SELECT * FROM jaze_company_team_details WHERE meta_key in ('account_id')   ";
			query += " and group_id in (SELECT group_id  FROM jaze_company_team_details WHERE  meta_key ='team_account_id' and meta_value = '" + reqParams.active_account_id + "' ";
			query += " and group_id in (SELECT group_id  FROM jaze_company_team_details WHERE  meta_key ='status' and meta_value = '1')) ";

			DB.query(query, null, function (data, error) {

				if (typeof data[0] !== 'undefined' && data[0] !== null && parseInt(data.length) > 0) {
					methods.getPermissionAccessMenuDetails(data[0].group_id, data[0].meta_value, function (menuList) {

						return_result(menuList);
					});
				}
				else { return_result([]); }
			});
		}
		else { return_result([]); }

	}
	catch (err) { console.log("getAccessMenuList", err); return_result([]); }
};

methods.getCompanyAccessMenuDetails = function (account_type, return_result) {
	try {
		if (parseInt(account_type) !== 0) {

			var query = " SELECT * FROM jaze_master_menu_groups WHERE  group_id not in(34,35,36,38,39,85,40,84,7,81)";
			query += " and group_id in (SELECT group_id  FROM jaze_master_menu_groups WHERE  meta_key ='status' and meta_value = '1'  ";
			query += " and group_id in (SELECT group_id  FROM jaze_master_menu_groups WHERE  meta_key ='account_type' and FIND_IN_SET('" + account_type + "', meta_value))) ";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

					HelperRestriction.convertMultiMetaArray(data, function (data_result) {

						var counter = data_result.length;

						var not_list = [34, 35, 36, 38, 39, 85, 40, 84, 7, 81];

						if (parseInt(counter) !== 0) {
							var master_id_array = [];

							var array_rows = [];

							promiseForeach.each(data_result, [function (list) {

								return new Promise(function (resolve, reject) {

									if (master_id_array.indexOf(list.group_id) === -1) {
										master_id_array.push(list.group_id);

										var newObj = {
											menu_id: list.group_id.toString(),
											menu_master_id: '0',
											menu_title: list.name,
											sort_order: parseInt(list.sort_order)
										};

										array_rows.push(newObj);
									}

									counter -= 1;

									resolve(array_rows);
								})
							}],
								function (result, current) {
									if (counter === 0) { return_result(fast_sort(result[0]).asc('sort_order')); }
								});
						}
						else { return_result([]); }
					});
				}
				else { return_result([]); }
			});
		}
		else { return_result([]); }
	}
	catch (err) { console.log("getCompanyAccessMenuDetails", err); return_result(null); }
}

methods.getPermissionAccessMenuDetails = function (member_id, company_id, return_result) {
	try {
		if (parseInt(member_id) !== 0 && parseInt(company_id) !== 0) {
			var query = " SELECT permission_ids FROM jaze_user_permissions_access WHERE member_id ='" + member_id + "' AND account_id = '" + company_id + "' ";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

					var query = " SELECT * FROM jaze_master_menu_groups WHERE  group_id not in(34,35,36,38,39,85,40,84,7,81)";
					query += " and group_id in (SELECT group_id  FROM jaze_master_menu_groups WHERE  meta_key ='status' and meta_value = '1'  ";
					query += " and group_id in (" + data[0].permission_ids.split(",") + ")) ";

					DB.query(query, null, function (data, error) {

						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

							HelperRestriction.convertMultiMetaArray(data, function (data_result) {

								var counter = data_result.length;

								var not_list = [34, 35, 36, 38, 39, 85, 40, 84, 7, 81];

								if (parseInt(counter) !== 0) {
									var master_id_array = [];

									var array_rows = [];

									promiseForeach.each(data_result, [function (list) {

										return new Promise(function (resolve, reject) {

											methods.__getMenuMasterDetails(list.parent_id, function (master_list) {

												if (master_list !== null) {
													if (not_list.indexOf(master_list.group_id) === -1) {
														if (master_id_array.indexOf(master_list.group_id) === -1) {
															master_id_array.push(master_list.group_id);

															var newObj = {
																menu_id: master_list.group_id.toString(),
																menu_master_id: master_list.parent_id.toString(),
																menu_title: master_list.name.toString(),
																sort_order: parseInt(master_list.sort_order)
															};
															array_rows.push(newObj);
														}
													}
												}
												else {
													if (master_id_array.indexOf(list.group_id) === -1) {
														master_id_array.push(list.group_id);

														var newObj = {
															menu_id: list.group_id.toString(),
															menu_master_id: '0',
															menu_title: list.name,
															sort_order: parseInt(list.sort_order)
														};

														array_rows.push(newObj);
													}
												}

												counter -= 1;

												resolve(array_rows);
											});
										})
									}],
										function (result, current) {
											if (counter === 0) { return_result(fast_sort(result[0]).asc('sort_order')); }
										});
								}
								else { return_result([]); }
							});
						}
						else { return_result([]); }
					});
				}
				else { return_result([]); }
			});
		}
		else { return_result([]); }
	}
	catch (err) { console.log("getPermissionAccessMenuDetails", err); return_result(null); }
}

methods.__getMenuMasterDetails = function (parent_id, return_result) {
	try {
		if (parseInt(parent_id) !== 0) {
			var query = " SELECT * FROM jaze_master_menu_groups WHERE meta_key in('parent_id','name','sort_order') ";
			query += " and group_id in (SELECT group_id  FROM jaze_master_menu_groups WHERE  meta_key ='status' and meta_value = '1'  ";
			query += " and group_id = '" + parent_id + "')";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

					HelperRestriction.convertMultiMetaArray(data, function (data_result) {

						return_result(data_result[0]);
					});
				}
				else { return_result(null); }
			});
		}
		else { return_result(null); }
	}
	catch (err) { console.log("__getMenuMasterDetails", err); return_result(null); }
};

methods.__getWorkAccountAdminStatus = function (reqParams, company_id, return_result) {
	try {
		if (reqParams !== null && parseInt(company_id) !== 0) {
			var query = " SELECT `id` FROM `jaze_user_account_details` WHERE `status` = 1 and `user_id` = '" + reqParams.account_id + "' and `company_id` = '" + company_id + "' and `work_id`= '" + reqParams.active_account_id + "'  ";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

					return_result(1);
				}
				else { return_result(0); }
			});
		}
		else { return_result(0); }
	}
	catch (err) { console.log("__getWorkAccountAdminStatus", err); return_result(0); }
};

methods.__getCompanyAccountAdminDetails = function (reqParams, return_result) {

	try {

		var return_obj = {
			admin_id: '0',
			admin_name: '',
			admin_logo: '',
		};


		if (parseInt(reqParams.account_type) === 3 || parseInt(reqParams.account_type) === 5) {

			var query = " SELECT user_id FROM `jaze_user_account_details` WHERE `status` = 1 and  `company_id` = '" + reqParams.account_id + "' and  user_id != 0";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

					var accParam = { account_id: data[0].user_id };

					HelperGeneralJazenet.getAccountDetails(accParam, function (accRres) {

						return_obj = {
							admin_id	: accRres.account_id,
							admin_name	: accRres.name,
							admin_logo	: accRres.logo,
						};

						return_result(return_obj);
					});
				}
				else { return_result(return_obj); }
			});
		}
		else { return_result(return_obj); }
	}
	catch (err) { console.log("__getCompanyAccountAdminDetails", err); return_result(return_obj); }
};
//------------------------------------------------------------------- Home Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Account Cerification Pin Module Start -----------------------------------------------------//

methods.accountVerification = function (arrParams, return_result) {
	try {
		var query = `SELECT * FROM jaze_user_credential_details WHERE jaze_security_pin =? AND account_id=? AND status = '1'`;

		DB.query(query, [arrParams.security_pin, arrParams.account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

				return_result(data[0]);
			} else {
				return_result(null);
			}
		});

	}
	catch (err) { console.log("accountVerification", err); return_result(null); }

};

//------------------------------------------------------------------- Account Cerification Pin Module End -----------------------------------------------------//


// ----------------------------------- communication history  -------------------------------- //


methods.checkCommunicationHistory = function (arrParams, return_result) {


	var newObj = {
		convers_id: '',
		status: '0',
	};

	try {

		var query = `SELECT * FROM jaze_jazecom_contact_list WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value=?) `;

		if (parseInt(arrParams.flag) === 1) {
			query += ` AND group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'chat_account_id' AND FIND_IN_SET(?,meta_value)>0 )`;
		}
		else if (parseInt(arrParams.flag) === 2) {
			query += ` AND group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'call_account_id' AND FIND_IN_SET(?,meta_value)>0 )`;
		}
		else if (parseInt(arrParams.flag) === 3) {
			query += ` AND group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'mail_account_id' AND FIND_IN_SET(?,meta_value)>0 )`;
		}
		else if (parseInt(arrParams.flag) === 4) {
			query += ` AND group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'diary_account_id' AND FIND_IN_SET(?,meta_value)>0 )`;
		}

		DB.query(query, [arrParams.account_id, arrParams.request_account_id], function (data, error) {


			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {
				newObj.status = "1";
				if (parseInt(arrParams.flag) === 1) {

					var conQuery = `SELECT * FROM jaze_jazechat_conversation_list WHERE group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'account_id' AND (meta_value =? OR meta_value =?) )
					AND group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'contact_account_id' AND (meta_value =? OR meta_value =?) )`

					DB.query(conQuery, [arrParams.account_id, arrParams.request_account_id, arrParams.request_account_id, arrParams.account_id], function (dataconv, error) {

						if (typeof dataconv !== 'undefined' && dataconv !== null && parseInt(dataconv.length) > 0) {

							methods.convertMultiMetaArray(dataconv, function (retConverted) {

								newObj.convers_id = retConverted[0].conversa_id.toString();
								return_result(newObj);

							});

						}
					});
				}
				else {
					return_result(newObj);
				}

			}
			else {

				var query = `SELECT * FROM jaze_jazecom_contact_list WHERE group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'account_id' AND meta_value =?) `;
				if (parseInt(arrParams.flag) === 1) {
					query += ` AND group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'chat_blocked_account_id' AND FIND_IN_SET(?,meta_value)>0 )`;
				}
				else if (parseInt(arrParams.flag) === 2) {
					query += ` AND group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'call_blocked_account_id' AND FIND_IN_SET(?,meta_value)>0 )`;
				}
				else if (parseInt(arrParams.flag) === 3) {
					query += ` AND group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'mail_blocked_account_id' AND FIND_IN_SET(?,meta_value)>0 )`;
				}
				else if (parseInt(arrParams.flag) === 4) {
					query += ` AND group_id IN (SELECT group_id FROM jaze_jazecom_contact_list WHERE meta_key = 'diary_blocked_account_id' AND FIND_IN_SET(?,meta_value)>0 )`;
				}

				DB.query(query, [arrParams.account_id, arrParams.request_account_id], function (data, error) {


					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {
						newObj.status = "2";
						if (parseInt(arrParams.flag) === 1) {

							var conQuery = `SELECT * FROM jaze_jazechat_conversation_list WHERE group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'account_id' AND (meta_value =? OR meta_value =?) )
							AND group_id IN (SELECT group_id FROM jaze_jazechat_conversation_list WHERE meta_key = 'contact_account_id' AND (meta_value =? OR meta_value =?) )`

							DB.query(conQuery, [arrParams.account_id, arrParams.request_account_id, arrParams.request_account_id, arrParams.account_id], function (dataconv, error) {

								if (typeof dataconv !== 'undefined' && dataconv !== null && parseInt(dataconv.length) > 0) {

									methods.convertMultiMetaArray(dataconv, function (retConverted) {

										newObj.convers_id = retConverted[0].conversa_id.toString();
										return_result(newObj);
									});

								}
							});
						}
						else {
							return_result(newObj);
						}

					}
					else {

						return_result(newObj);
					}

				});
			}

		});


	}
	catch (err) { console.log("checkCommunicationHistory", err); return_result(newObj); }
}


// ----------------------------------- communication history -------------------------------- //


//------------------------------------------------------------------- Jazenet to Jazecom Module Start ---------------------------------------------------------------------------//

/*
 * Jazecom Device Details 
 */
methods.getJazenetJazecomDeviceDetails = function (arrParams, return_result) {

	try {

		var objReturn = {
			account_id: arrParams.account_id.toString(),
			account_type: arrParams.account_type.toString(),
			jazecom_flag: "2",
			connection_key: crypto.createHash('md5').update("Jazenet-" + arrParams.account_id + arrParams.account_type + "-Jazecom").digest("hex"),
			jazecom_device_details: {}
		};

		// jazecom_flag =  0 : not installed, 1 : active account, 2 : different account


		//get current device details
		var query = " SELECT * FROM `jaze_app_device_details` WHERE `status` = 1 and `app_type` = 1 ";
		query += " and (`id` = '" + arrParams.app_id + "' or `parent_id` = '" + arrParams.app_id + "') ";
		query += " and `api_token` = '" + arrParams.api_token + "' ";
		query += " and `account_id` = '" + arrParams.account_id + "' ";

		DB.query(query, null, function (dataJazenet, error) {

			if (typeof dataJazenet !== 'undefined' && dataJazenet !== null && parseInt(dataJazenet.length) > 0) {

				//get jazecom device details
				var query = " SELECT *, ";
				query += " (SELECT m.account_id from jaze_app_device_details m WHERE m.`status` = 1 and m.id = jaze_app_device_details.parent_id) as master_id ";
				query += " FROM `jaze_app_device_details` WHERE `status` = 1 and `app_type` = 2 ";
				query += " and `account_id` = '" + dataJazenet[0].account_id + "' ";
				//query += " AND `device_id` = '" + dataJazenet[0].device_id + "' ";
				query += " AND `device_name` = '" + dataJazenet[0].device_name + "' ";
				query += " AND `device_type` = '" + dataJazenet[0].device_type + "' ";

				DB.query(query, null, function (dataJazecom, error) {

					if (typeof dataJazecom !== 'undefined' && dataJazecom !== null && parseInt(dataJazecom.length) > 0) {

						var master_account_id = 0;

						if (parseInt(dataJazecom[0].master_id) !== 0 && dataJazecom[0].master_id !== null) { master_account_id = dataJazecom[0].master_id; }
						else { master_account_id = dataJazecom[0].account_id; }

						var master_app_id = 0;

						if (parseInt(dataJazecom[0].parent_id) !== 0) { master_app_id = dataJazecom[0].parent_id; }
						else { master_app_id = dataJazecom[0].id; }

						//get jazecom  master account details 
						var accParam = { account_id: master_account_id };

						HelperGeneralJazenet.getAccountDetails(accParam, function (accRres) {

							if (accRres !== null) {

								var objResult = {
									app_id: master_app_id.toString(),
									api_token: dataJazecom[0].api_token.toString(),
									master_account_id: accRres.account_id.toString(),
									master_account_name: accRres.name.toString(),
									master_account_logo: accRres.logo.toString()
								};

								Object.assign(objReturn, { jazecom_flag: '1' });

								Object.assign(objReturn, { jazecom_device_details: objResult });

								return_result(objReturn);
							}
							else { return_result(objReturn); }
						});
					}
					else { return_result(objReturn); }
				});
			}
			else { return_result(objReturn); }
		});
	}
	catch (err) { console.log("getJazenetJazecomDeviceDetails", err); return_result(objReturn); }
};

/*
 * Jazechat 
 */
methods.getJazenetJazecomChatDetails = function (arrParams, return_result) {
	// status_flag
	// 0 = new conversation
	// 1 = continue conversation
	// 2 = account blocked
	// 3 =  module inactive
	// 4 =  error
	// 5 =  pending chat request

	// connect_flag	
	// 0 = request
	// 1 = open
	// 2 = error

	var objReturn = {
		status_flag: '4',
		connect_flag: '2',
		convers_id: '0',
		status_msg: "Sorry, the service isn't available right now. Please try again later."
	};

	try {
		if (arrParams !== null) {
			if (parseInt(arrParams.account_type) === 1 && parseInt(arrParams.reciv_account_type) === 1) {
				objReturn = {
					status_flag: '4',
					connect_flag: '2',
					convers_id: '0',
					status_msg: "Sorry, can't communicate same user account."
				};

				return_result(objReturn);
			}
			else {
				var accParam = {
					account_id: arrParams.account_id,
					account_type: arrParams.account_type,
					request_account_id: arrParams.reciv_account_id,
					request_account_type: arrParams.reciv_account_type,
				};

				HelperJazechat.checkAccountJazecomChatStatus(accParam, function (chatStatus) {

					if (parseInt(chatStatus) === 1) {
						var connect_flag = '0';

						if (parseInt(accParam.reciv_account_type) === 1) { connect_flag = '0'; }
						else { connect_flag = '1'; }

						HelperJazechat.checkContactAlreadyExists(accParam, function (conDet) {

							if (conDet !== null) {

								if (parseInt(conDet.status) === 1) {
									objReturn = {
										status_flag: '1',
										connect_flag: '1',
										convers_id: conDet.conversa_id,
										status_msg: "jazechat conversation details"
									};

									return_result(objReturn);
								}
								else {

									objReturn = {
										status_flag: '5',
										connect_flag: '0',
										convers_id: '0',
										status_msg: "awaiting request submission"
									};

									return_result(objReturn);
								}
							}
							else {
								objReturn = {
									status_flag: '0',
									connect_flag: connect_flag,
									convers_id: '0',
									status_msg: "jazechat conversation details"
								};

								return_result(objReturn);
							}
						});
					}
					else {
						var status_msg = '';

						if (parseInt(chatStatus) === 2) { status_msg = 'Sorry, The account has been blocked.'; }
						else if (parseInt(chatStatus) === 3) { status_msg = "Sorry, the service isn't available right now. Please try again later."; }


						objReturn = {
							status_flag: chatStatus,
							connect_flag: '0',
							convers_id: '0',
							status_msg: status_msg
						};

						return_result(objReturn);
					}
				});
			}
		}
		else { return_result(objReturn); }
	}
	catch (err) { console.log("getJazenetJazecomChatDetails", err); return_result(objReturn); }
};

/*
 * Jazemail
 */
methods.getJazenetJazecomMailDetails = function (arrParams, return_result) {
	// status_flag
	// 0 = new conversation
	// 1 = continue conversation
	// 2 = account blocked
	// 3 =  module inactive

	var objReturn = {
		status_flag: '4',
		mail_account_id: '0',
		receiver_account_id: '0',
		receiver_account_name: '',
		receiver_account_logo: '',
		receiver_account_email: '',
		status_msg: "Sorry, the service isn't available right now. Please try again later."
	};

	try {
		if (arrParams !== null) {
			//get owner Module and block Status
			HelperJazecom.__checkModuleBlockStatus(arrParams.account_id, arrParams.account_type, arrParams.reciv_account_id, function (ownerBlockStatus) {

				if (ownerBlockStatus !== null) {
					//get contact Module and block Status
					HelperJazecom.__checkModuleBlockStatus(arrParams.reciv_account_id, arrParams.reciv_account_type, arrParams.account_id, function (contactBlockStatus) {

						if (contactBlockStatus !== null) {
							var mail_status = __checkModuleBlockCombination(ownerBlockStatus.mail, contactBlockStatus.mail);

							if (parseInt(mail_status) === 1 || parseInt(mail_status) === 2) {
								//get mail status	
								HelperJazecom.__getJazeMailStatus(mail_status, arrParams.account_id, arrParams.reciv_account_id, function (mailStatus) {

									if (parseInt(mailStatus) !== 0) {
										//get mail id	
										methods.___getEmailAccountId(arrParams.account_id, function (mail_account_id) {

											//get jazecom  master account details 
											var accParam = { account_id: arrParams.reciv_account_id };

											HelperGeneralJazenet.getAccountDetails(accParam, function (accRres) {

												var status_flag = '1';

												if (parseInt(mailStatus) === 1) { status_flag = '0'; }
												else if (parseInt(mailStatus) === 2) { status_flag = '1'; }

												objReturn = {
													status_flag: status_flag,
													mail_account_id: mail_account_id.toString(),
													receiver_account_id: accRres.account_id.toString(),
													receiver_account_name: accRres.name.toString(),
													receiver_account_logo: accRres.logo.toString(),
													receiver_account_email: accRres.email.toString(),
													status_msg: "jazemail conversation details"
												};

												return_result(objReturn);
											});
										});
									}
									else { return_result(objReturn); }
								});
							}
							else {
								var status_msg = '';
								var status_flag = '4';

								if (parseInt(mail_status) === 3) { status_flag = '2'; status_msg = 'Sorry, The account has been blocked.'; }
								else if (parseInt(mail_status) === 0) { status_flag = '3'; status_msg = "Sorry, the service isn't available right now. Please try again later."; }

								objReturn = {
									status_flag: status_flag,
									mail_account_id: '0',
									receiver_account_id: '0',
									receiver_account_name: '',
									receiver_account_logo: '',
									receiver_account_email: '',
									status_msg: status_msg
								};

								return_result(objReturn);
							}
						}
						else { return_result(objReturn); }
					});
				}
				else { return_result(objReturn); }
			});
		}
		else { return_result(objReturn); }
	}
	catch (err) { console.log("getJazenetJazecomMailDetails", err); return_result(objReturn); }
};

/*
 * Jazefeed
 */
methods.getJazenetJazecomFeedDetails = function (arrParams, return_result) {
	// status_flag
	// 0 = new conversation
	// 1 = continue conversation
	// 2 = account blocked
	// 3 =  module inactive

	var objReturn = {
		status_flag: '4',
		receiver_account_id: '0',
		receiver_account_name: '',
		receiver_account_logo: '',
		status_msg: "Sorry, the service isn't available right now. Please try again later."
	};

	try {
		if (arrParams !== null) {
			//get owner Module and block Status
			HelperJazecom.__checkModuleBlockStatus(arrParams.account_id, arrParams.account_type, arrParams.reciv_account_id, function (ownerBlockStatus) {

				if (ownerBlockStatus !== null) {
					//get contact Module and block Status
					HelperJazecom.__checkModuleBlockStatus(arrParams.reciv_account_id, arrParams.reciv_account_type, arrParams.account_id, function (contactBlockStatus) {

						if (contactBlockStatus !== null) {
							var feed_status = __checkModuleBlockCombination(ownerBlockStatus.feed, contactBlockStatus.feed);

							if (parseInt(feed_status) === 1 || parseInt(feed_status) === 2) {
								//get jazecom  master account details 
								var accParam = { account_id: arrParams.reciv_account_id };

								HelperGeneralJazenet.getAccountDetails(accParam, function (accRres) {

									var status_flag = '1';

									if (parseInt(feed_status) === 1) { status_flag = '0'; }
									else if (parseInt(feed_status) === 2) { status_flag = '1'; }

									objReturn = {
										status_flag: status_flag,
										receiver_account_id: accRres.account_id.toString(),
										receiver_account_name: accRres.name.toString(),
										receiver_account_logo: accRres.logo.toString(),
										status_msg: "jazefeed conversation details"
									};

									return_result(objReturn);
								});
							}
							else {
								var status_msg = '';
								var status_flag = '4';

								if (parseInt(feed_status) === 3) { status_flag = '2'; status_msg = 'Sorry, The account has been blocked.'; }
								else if (parseInt(feed_status) === 0) { status_flag = '3'; status_msg = "Sorry, the service isn't available right now. Please try again later."; }

								objReturn = {
									status_flag: status_flag,
									receiver_account_id: '0',
									receiver_account_name: '',
									receiver_account_logo: '',
									status_msg: status_msg
								};

								return_result(objReturn);
							}
						}
						else { return_result(objReturn); }
					});
				}
				else { return_result(objReturn); }
			});
		}
		else { return_result(objReturn); }
	}
	catch (err) { console.log("getJazenetJazecomFeedDetails", err); return_result(objReturn); }
};

/*
 * Jazediary
 */
methods.getJazenetJazecomDiaryDetails = function (arrParams, return_result) {
	// status_flag
	// 0 = new conversation
	// 1 = continue conversation
	// 2 = account blocked
	// 3 =  module inactive

	var objReturn = {
		status_flag: '4',
		receiver_account_id: '0',
		receiver_account_name: '',
		receiver_account_logo: '',
		status_msg: "Sorry, the service isn't available right now. Please try again later."
	};

	try {
		if (arrParams !== null) {
			//get owner Module and block Status
			HelperJazecom.__checkModuleBlockStatus(arrParams.account_id, arrParams.account_type, arrParams.reciv_account_id, function (ownerBlockStatus) {

				if (ownerBlockStatus !== null) {
					//get contact Module and block Status
					HelperJazecom.__checkModuleBlockStatus(arrParams.reciv_account_id, arrParams.reciv_account_type, arrParams.account_id, function (contactBlockStatus) {

						if (contactBlockStatus !== null) {
							var diary_status = __checkModuleBlockCombination(ownerBlockStatus.diary, contactBlockStatus.diary);

							if (parseInt(diary_status) === 1 || parseInt(diary_status) === 2) {
								//get jazecom  master account details 
								var accParam = { account_id: arrParams.reciv_account_id };

								HelperGeneralJazenet.getAccountDetails(accParam, function (accRres) {

									var status_flag = '1';

									if (parseInt(diary_status) === 1) { status_flag = '0'; }
									else if (parseInt(diary_status) === 2) { status_flag = '1'; }

									objReturn = {
										status_flag: status_flag,
										receiver_account_id: accRres.account_id.toString(),
										receiver_account_name: accRres.name.toString(),
										receiver_account_logo: accRres.logo.toString(),
										status_msg: "jazedairy conversation details"
									};

									return_result(objReturn);
								});
							}
							else {
								var status_msg = '';
								var status_flag = '4';

								if (parseInt(diary_status) === 3) { status_flag = '2'; status_msg = 'Sorry, The account has been blocked.'; }
								else if (parseInt(diary_status) === 0) { status_flag = '3'; status_msg = "Sorry, the service isn't available right now. Please try again later."; }

								objReturn = {
									status_flag: status_flag,
									receiver_account_id: '0',
									receiver_account_name: '',
									receiver_account_logo: '',
									status_msg: status_msg
								};

								return_result(objReturn);
							}
						}
						else { return_result(objReturn); }
					});
				}
				else { return_result(objReturn); }
			});
		}
		else { return_result(objReturn); }
	}
	catch (err) { console.log("getJazenetJazecomDiaryDetails", err); return_result(objReturn); }
};

function __checkModuleBlockCombination(owner_status, contact_status) {

	var return_status = "0";

	if (owner_status !== "" && contact_status !== "") {
		var new_array = ['1-1', '1-2'];

		var continue_array = ['2-2', '2-1'];

		var block_array = ['2-3', '3-2', '3-3', '1-3', '3-1'];

		var inactive_array = ['2-0', '0-2', '0-0', '0-1', '1-0', '0-3', '3-0'];

		var id = owner_status + "-" + contact_status;

		if (new_array.indexOf(id) != -1) {
			return_status = "1";
		}
		else if (continue_array.indexOf(id) != -1) {
			return_status = "2";
		}
		else if (block_array.indexOf(id) != -1) {
			return_status = "3";
		}
		else if (inactive_array.indexOf(id) != -1) {
			return_status = "0";
		}

		return return_status;
	}
	else { return "0" }
}

methods.___getAccountJazecomStatus = function (account_id, account_type, return_result) {
	try {
		if (parseInt(account_id) !== 0) {
			if (parseInt(account_type) === 4) {
				var query = " SELECT * FROM jaze_user_basic_details WHERE meta_key in ('team_diary_status','team_mail_status','team_chat_status','team_call_status') and  `account_id` IN (SELECT `account_id`  FROM jaze_user_basic_details WHERE `meta_key` = 'team_status' AND meta_value = '1' ) ";
				query += " and account_id = '" + account_id + "' and account_type = '4' ";

				DB.query(query, null, function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

						var list = [];

						Object.assign(list, { account_id: data[0].account_id });
						Object.assign(list, { account_type: data[0].account_type });

						for (var i in data) {
							Object.assign(list, { [data[i].meta_key]: data[i].meta_value });
						}

						var objResult = {
							account_id: list.account_id.toString(),
							jaze_diary: list.team_diary_status.toString(),
							jaze_feed: '0',
							jaze_call: list.team_call_status.toString(),
							jaze_mail: list.team_mail_status.toString(),
							jaze_chat: list.team_chat_status.toString(),
						};

						return_result(objResult);
					}
					else { return_result(null); }
				});
			}
			else {
				var query = "SELECT * FROM jaze_user_jazecome_status WHERE  `group_id` IN (SELECT `group_id`  FROM jaze_user_jazecome_status WHERE `meta_key` = 'account_id' AND meta_value = '" + account_id + "' ) ";

				DB.query(query, null, function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

						HelperRestriction.convertMetaArray(data, function (statusRes) {

							return_result(statusRes);
						});
					}
					else { return_result(null); }
				});
			}
		}
		else { return_result(null); }
	}
	catch (err) { console.log("___getAccountJazecomStatus", err); return_result(null); }
}

methods.___getEmailAccountId = function (account_id, return_result) {
	try {
		if (parseInt(account_id) !== 0) {
			var query = " SELECT group_id FROM jaze_email_accounts WHERE `meta_key` = 'account_id' AND meta_value = '" + account_id + "'  ";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

					return_result(data[0].group_id);
				}
				else { return_result(0); }
			});
		}
		else { return_result(0); }
	}
	catch (err) { console.log("___getEmailAccountId", err); return_result(0); }
}
//------------------------------------------------------------------- Jazenet to Jazecom Module Start ---------------------------------------------------------------------------//
//------------------------------------------------------------------- Forgot email/password Module Start ---------------------------------------------------------------------------//

methods.resetEmailPassword = function (reqParams, return_result) {

	try {
		if (reqParams !== null) {
			//check is valid entry
			var query = " SELECT * FROM jaze_user_basic_details WHERE account_type = '1' and meta_key = 'email'  ";
			query += " AND account_id IN (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";

			if (reqParams.email !== "" && reqParams.email !== null) {
				query += " AND account_id IN (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'email'  AND meta_value = '" + reqParams.email + "') ";
			}
			else if (parseInt(reqParams.country_code_id) !== 0 && reqParams.country_code_id !== "" && reqParams.mobile_number !== "" && reqParams.mobile_number !== null) {
				query += " AND account_id IN (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'mobile_country_code_id'  AND meta_value = '" + reqParams.country_code_id + "'  ";
				query += " AND account_id IN (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'mobile'  AND meta_value = '" + reqParams.mobile_number + "')) ";
			}

			query += ")";

			DB.query(query, null, function (dataAccount, error) {

				if (typeof dataAccount !== 'undefined' && dataAccount !== null && parseInt(dataAccount.length) > 0) {

					methods.__generateVerificationCode(dataAccount[0].account_id, reqParams.flag, function (retConverted) {

						return_result(retConverted);

					});
				}
				else { return_result(null) }
			});
		}
		else { return_result(null) }
	}
	catch (err) { console.log("resetEmailPassword", err); return_result(null); }
};

methods.resendVerificationCode = function (reqParams, return_result) {

	try {
		if (reqParams !== null) {
			//check account status
			var query = " SELECT status FROM jaze_user_credential_details WHERE account_id = '" + reqParams.account_id + "' and account_type = '1' and status = '2' ";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

					methods.__generateVerificationCode(reqParams.account_id, reqParams.flag, function (retConverted) {

						return_result(retConverted);

					});
				}
				else { return_result(null) }
			});
		}
		else { return_result(null) }
	}
	catch (err) { console.log("resendVerificationCode", err); return_result(null); }
};

methods.__generateVerificationCode = function (account_id, flag, return_result) {

	try {
		if (parseInt(account_id) !== 0 && parseInt(flag) !== 0) {
			//get account id
			var query = " SELECT * FROM jaze_user_basic_details WHERE account_type = '1' and meta_key = 'email'  ";
			query += " AND account_id IN (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1') ";
			query += " AND account_id = '" + account_id + "' ";

			DB.query(query, null, function (dataAccount, error) {

				if (typeof dataAccount !== 'undefined' && dataAccount !== null && parseInt(dataAccount.length) > 0) {

					//update Credential Detail
					var query = " UPDATE jaze_user_credential_details SET ";
					query += " status  = '2' ";
					query += " where account_id = '" + dataAccount[0].account_id + "' and account_type = '1' and (status  = '1' or status  = '2')  ";

					DB.query(query, null, function (data, error) {

						if (data !== null) {

							//create verification code

							var characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

							var acc_id = (dataAccount[0].id.toString().length == 1) ? "0" + dataAccount[0].id : dataAccount[0].id;

							var randomString = (characters.split('').sort(function () { return 0.5 - Math.random() }).join('').substring(0, 4)) + acc_id.toString().substr(-2);

							var verif_code = randomString.split('').sort(function () { return 0.5 - Math.random() }).join('');

							var verif_code_md5 = crypto.createHash('md5').update(verif_code).digest("hex");

							//want to check code already exist 
							var query = " SELECT id FROM jaze_user_account_verification WHERE meta_key = 'reset_verification_code'  AND meta_value = '" + verif_code_md5 + "'   ";

							DB.query(query, null, function (data, error) {

								if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

									methods.__generateVerificationCode(dataAccount[0].account_id, function (retConverted) {
										return_result(retConverted)
									});
								}
								else {
									//delete existing data
									var query = " DELETE FROM jaze_user_account_verification WHERE meta_key = 'reset_verification_code' and account_id = '" + dataAccount[0].account_id + "'   ";

									DB.query(query, null, function (data, error) {

										if (data !== null) {

											//save to  tb
											const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');

											var query = " INSERT INTO jaze_user_account_verification SET ";
											query += " account_id = '" + dataAccount[0].account_id + "', ";
											query += " flag = '" + flag + "', ";
											query += " meta_key = 'reset_verification_code', ";
											query += " meta_value = '" + verif_code_md5 + "', ";
											query += " create_date = '" + standardDTUTC + "', ";
											query += " status  = '1' ";

											DB.query(query, null, function (data, error) {

												if (data !== null) {

													methods.__getEmailTemplate(verif_code, function (message) {

														//call email function
														var reqParamsEmail = {
															to_email: dataAccount[0].meta_value,
															subject: "verification code",
															message: message,
														};

														HelperJazeMail.sendSystemMailer(reqParamsEmail, function (retConverted) {

															return_result(dataAccount[0]);
														});
													});
												}
												else { return_result(null) }
											});
										}
										else { return_result(null) }
									});
								}
							});
						}
						else { return_result(null) }
					});
				}
				else { return_result(null) }
			});
		}
		else { return_result(null) }
	}
	catch (err) { console.log("__generateVerificationCode", err); return_result(null); }
};

methods.checkResetVerificationCode = function (reqParams, return_result) {

	try {
		if (reqParams !== null) {
			//check is valid 
			var query = " SELECT * FROM jaze_user_account_verification WHERE meta_key = 'reset_verification_code'  AND meta_value = '" + crypto.createHash('md5').update(reqParams.verif_code).digest("hex") + "' and status = '1'  ";

			DB.query(query, null, function (dataVerification, error) {

				if (typeof dataVerification !== 'undefined' && dataVerification !== null && parseInt(dataVerification.length) > 0) {

					//check code is expired
					var create_date = moment(dataVerification[0].create_date).format('YYYY-MM-DD HH:mm:ss');

					var duration = moment.duration(moment(moment().utc().format('YYYY-MM-DD HH:mm:ss')).diff(create_date));

					if (parseInt(duration.years()) === 0 && parseInt(duration.days()) === 0 && parseInt(duration.hours()) === 0 && parseFloat(duration.minutes()) <= 5) {
						// delete data
						var query = " DELETE FROM `jaze_user_account_verification` WHERE `id` = '" + dataVerification[0].id + "' ";

						DB.query(query, null, function (data, error) {

							if (data !== null) {

								if (parseInt(dataVerification[0].flag) === 1) {
									//update Credential Detail
									var query = " UPDATE jaze_user_credential_details SET ";
									query += " status  = '1' ";
									query += " where account_id = '" + dataVerification[0].account_id + "' and account_type = '1' and status  = '2' ";

									DB.query(query, null, function (data, error) {

										if (data !== null) {

											//get email
											var query = " SELECT jazemail_id FROM jaze_user_credential_details ";
											query += " where account_id = '" + dataVerification[0].account_id + "' and account_type = '1' and status  = '1' ";

											DB.query(query, null, function (data, error) {

												if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

													var reqobj = {
														account_id: dataVerification[0].account_id.toString(),
														email: data[0].jazemail_id,
														flag: dataVerification[0].flag.toString(),
													};

													return_result(1, reqobj);
												}
												else { return_result(0, null) }
											});
										}
										else { return_result(0, null) }
									});
								}
								else if (parseInt(dataVerification[0].flag) === 2) {
									var reqobj = {
										account_id: dataVerification[0].account_id.toString(),
										email: "",
										flag: dataVerification[0].flag.toString(),
									};

									return_result(1, reqobj);
								}
								else { return_result(0, null) }
							}
							else { return_result(0, null) }

						});
					}
					else { return_result(2, null) }
				}
				else { return_result(0, null) }
			});
		}
		else { return_result(0, null) }
	}
	catch (err) { console.log("checkResetVerificationCode", err); return_result(0, null); }
};

methods.resetPassword = function (reqParams, return_result) {

	try {
		if (reqParams !== null && parseInt(reqParams.account_id) !== 0) {
			//check is valid 
			var query = " SELECT * FROM  jaze_user_credential_details WHERE account_id = '" + reqParams.account_id + "' and (status = '1' or status = '2') ";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0) {

					if (data[0].password !== reqParams.password) {
						//update Credential Detail
						var query = " UPDATE jaze_user_credential_details SET ";
						query += " password  = '" + reqParams.password + "', ";
						query += " status  = '1' ";
						query += " where account_id = '" + data[0].account_id + "' and account_type = '1' ";

						DB.query(query, null, function (data, error) {

							if (data !== null) {

								return_result(1)
							}
							else { return_result(0) }
						});
					}
					else { return_result(2) }
				}
				else { return_result(0) }
			});
		}
		else { return_result(0) }
	}
	catch (err) { console.log("resetPassword", err); return_result(0); }
};

methods.__getEmailTemplate = function (code, return_result) {

	try {

		var html_template = '<!DOCTYPE html>';
		html_template += '<html>';
		html_template += '<head>';
		html_template += '<meta name="viewport" content="width=device-width">';
		html_template += '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
		html_template += '<title>Simple Transactional Email</title>';
		html_template += '</head>';
		html_template += '<body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">';
		html_template += '<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;background-color:#f6f6f6;"><tr>';
		html_template += '<td style="font-family:sans-serif;font-size:16px !important;vertical-align:top;"> </td>';
		html_template += '<td class="container" style="font-family:sans-serif;font-size:16px !important;vertical-align:top;display:block;margin:0 auto !important;max-width:1010px;padding:0 !important;width:100% !important;">';
		html_template += '<div class="content" style="box-sizing:border-box;display:block;margin:0 auto;max-width:1010px;padding:0 !important;">';
		html_template += '<table role="presentation" class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;background:#ffffff;border-radius:0 !important;border-left-width:0 !important;border-right-width:0 !important;">';
		html_template += '<td class="wrapper" style="font-family:sans-serif;font-size:16px !important;vertical-align:top;box-sizing:border-box;padding:10px !important;">';
		html_template += '<table role="presentation" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">';
		html_template += '<tr class="header-des" style="margin-bottom: 20px;">';
		html_template += '<td style="font-family:sans-serif;font-size:16px;vertical-align:top;justify-content:center !important;font-weight:normal;margin:0;border-bottom:1px solid #b2b2b2;padding-bottom:20px;">';
		html_template += '<table role="presentation" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">';
		html_template += '<tr>';
		html_template += '<td style="font-family:sans-serif;font-size:16px !important;vertical-align:top;">';
		html_template += ' <img src="https://www.img.jazenet.com/images/master.resources/uploads/jazenet/logo-jazenet.jpg" alt="Logo" title="Logo" width="" height="" style="display:block;border:none;-ms-interpolation-mode:bicubic;max-width:100%;width:15%;">';
		html_template += '</td>';
		html_template += '<td style="font-family:sans-serif;font-size:16px !important;vertical-align:top;">';
		html_template += '<p style="font-family:sans-serif;font-size:16px !important;font-weight:normal;margin:0;margin-bottom:0px;text-align:right;margin-top:20px;">&nbsp;</p>';
		html_template += '</td>';
		html_template += '</tr></table>';
		html_template += '</td>';
		html_template += '</tr>';
		html_template += '<tr>';
		html_template += '<td style="font-family:sans-serif;font-size:16px !important;vertical-align:top;">';
		html_template += '<p style="margin-top: 35px; font-family: sans-serif; font-size: 16px!important; font-weight: normal; margin: 0; margin-bottom: 37px; padding-top: 15px;">To authenticate, please use the following One Time Password (OTP):</p>';
		html_template += '<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;box-sizing:border-box;"><tbody><tr>';
		html_template += '<td align="left" style="font-family:sans-serif;font-size:16px !important;vertical-align:top;padding-bottom:15px;">';
		html_template += '<table role="presentation" border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100% !important;"><tbody><tr>';
		html_template += '<td class="otp-code" style="font-family:sans-serif;font-size:28px !important;vertical-align:top;font-weight:bold;background-color:#ffffff;border-radius:5px;">' + code + '</td>';
		html_template += '</tr></tbody></table>';
		html_template += '</td>';
		html_template += '</tr></tbody></table>';
		html_template += '<p style="font-family:sans-serif;font-size:13px !important;font-weight:normal;margin:0;margin-bottom:34px; margin-top: 20px;">Do not share this OTP with anyone. Jazenet takes your account security very seriously. Jazenet Customer Service will never ask you to disclose or verify your Jazenet password, OTP, credit card, or banking account number. If you receive a suspicious email with a link to update your account information, do not click on the link—instead, report the email to Jazenet for investigation.</p>';
		html_template += '<p style="font-family:sans-serif;font-size:16px !important;font-weight:normal;margin:0;margin-bottom:15px;">We hope to see you again soon.</p>';
		html_template += '</td>';
		html_template += '</tr>';
		html_template += '</table>';
		html_template += '</td>';
		html_template += '</tr>';
		html_template += '</table>';
		html_template += '</div>';
		html_template += '</td>';
		html_template += '<td style="font-family:sans-serif;font-size:16px !important;vertical-align:top;"> </td>';
		html_template += ' </tr></table>';
		html_template += '</body>';
		html_template += '</html>';

		return_result(html_template)

	}
	catch (err) { console.log("__getEmailTemplate", err); return_result(null); }
};



//------------------------------------------------------------------- Forgot email/password Module End ---------------------------------------------------------------------------//
//------------------------------------------------------------------- General Module Start ---------------------------------------------------------------------------//

methods.convertMultiMetaArray = function (arrvalues, return_result) {
	try {
		if (typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0) {
			var list = [];

			for (var i in arrvalues) {
				var index = __getGroupIndex(list, arrvalues[i].group_id);

				if (index != "") {

					Object.assign(list[index], { [arrvalues[i].meta_key]: arrvalues[i].meta_value });
				}
				else {

					var obj_return = {
						group_id: arrvalues[i].group_id,
						[arrvalues[i].meta_key]: arrvalues[i].meta_value
					};

					list.push(obj_return);
				}
			}

			return_result(list);
		}
		else { return_result(null); }
	}
	catch (err) { console.log("convertMetaArray", err); return_result(null); }
};


function __getGroupIndex(list_array, index_value) {
	var retern_index = "";

	if (list_array !== null) {
		Object.keys(list_array).forEach(function (prop) {

			var value = list_array[prop].group_id;

			if (parseInt(value) === parseInt(index_value)) {
				retern_index = prop;
			}
		});
	}

	return retern_index;
}

function emailIsValid(email) {
	return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
};

function generateMobileAPI() {

	try {
		var randomString = '';

		var characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

		for (var i = 0; i < 25; i++) {
			randomString += characters[Math.floor(Math.random() * characters.length)];
		}

		//checking allready Exists		
		DB.query(" SELECT id FROM jaze_app_device_details WHERE api_token = ?", [randomString], function (data, error) {

			if (typeof data === 'undefined' && data === null) {
				randomString = generateMobileAPI();
			}
		});

		return randomString;
	}
	catch (err) {
		console.log("generateRandomString", err);
		return null;
	}
};

function mapCatOrder(menuList, userList) {

	if (typeof userList !== 'undefined' && userList !== null && parseInt(userList.length) > 0) {

		var order = userList[0].category_list.split(",")

		menuList.sort(function (a, b) {

			var A = a['id'], B = b['id'];

			if (order.indexOf(A) > order.indexOf(B)) {
				return 1;
			} else {
				return -1;
			}
		});
	}

	return menuList;
};

function sortingArrayObject(key, order = 'asc') {

	return function (a, b) {
		if (!a.hasOwnProperty(key) ||
			!b.hasOwnProperty(key)) {
			return 0;
		}

		const varA = (typeof a[key] === 'string') ?
			a[key].toUpperCase() : a[key];
		const varB = (typeof b[key] === 'string') ?
			b[key].toUpperCase() : b[key];

		let comparison = 0;
		if (varA > varB) {
			comparison = 1;
		} else if (varA < varB) {
			comparison = -1;
		}
		return (
			(order == 'desc') ?
				(comparison * -1) : comparison
		);
	};
};

methods.getAccountType = function (account_id, return_result) {
	try {
		if (parseInt(account_id) !== 0) {
			HelperGeneral.getAccountTypeDetails(account_id, function (reciv_account_type) {
				return_result(reciv_account_type);
			});
		}
		else { return_result(0); }
	}
	catch (err) { console.log("getAccountType", err); return_result(0); }
}
//------------------------------------------------------------------- General Module End ---------------------------------------------------------------------------//

module.exports = methods;