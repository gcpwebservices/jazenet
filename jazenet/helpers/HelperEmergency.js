const methods = {};
const DB = require('../../helpers/db_base.js');
const promiseForeach = require('promise-foreach');

// const express = require('express');
// const router = express.Router();

methods.getEmergencyDetails = function(arrParams,return_result)
{ 

	var newObj = {};
	var arrRows = [];

	try
	{
		var quer = `SELECT * FROM jaze_cityinfo_emergency_details_group WHERE group_id IN (SELECT group_id FROM jaze_cityinfo_emergency_details_group WHERE meta_key = 'city_id' AND meta_value =?) `;

		if(parseInt(arrParams.state_id) !==0 ){
	 		quer += " AND group_id IN (SELECT group_id FROM jaze_cityinfo_emergency_details_group WHERE meta_key = 'state_id' AND meta_value ='"+arrParams.state_id+"') ";
		}

		if(parseInt(arrParams.country_id) !==0 ){
	 		quer += " AND group_id IN (SELECT group_id FROM jaze_cityinfo_emergency_details_group WHERE meta_key = 'country_id' AND meta_value ='"+arrParams.country_id+"') ";
		}

		DB.query(quer, [arrParams.city_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				var querM = `SELECT * FROM jaze_cityinfo_emergency_details_meta WHERE 
				group_id IN (SELECT group_id FROM jaze_cityinfo_emergency_details_meta WHERE meta_key = 'parent_id' AND meta_value =?)`;

				DB.query(querM, [data[0].group_id], function(dataM, error){

					methods.convertMultiMetaArray(dataM, function(retConverted){

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {

							return new Promise(function (resolve, reject) {	

								DB.query("SELECT * FROM jaze_cityinfo_emergency_category WHERE group_id=?", [jsonItems.group_id], function(rData, error){

									methods.convertMultiMetaArray(rData, function(retMainConverted){
			
										if(typeof retMainConverted !== 'undefined' && retMainConverted !== null && parseInt(retMainConverted.length) > 0){

											newObj = {
												name:retMainConverted[0].cat_name,
												logo:G_STATIC_IMG_URL+'uploads/jazenet/category/cityinfo/emergency_category/'+retMainConverted[0].image_url,
												contact_no:jsonItems.conatct_no.toString()
											};

											arrRows.push(newObj);
											
										}

										resolve(arrRows);	
										
										counter -= 1;
										

									});
								});

							})
						}],
						function (result, current) { 
							
							if (counter === 0){ 
								return_result(result[0]);
							}

						});	
					});	

				});
			}
			else
			{
				return_result(null); 
			}

		});

	}
	catch(err)
	{ 
		console.log("getEmergencyDetails",err); 
		return_result(null); 
	}	
}



methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].group_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}




module.exports = methods;