const methods = {};
const DB = require('../../helpers/db_base.js');
const HelperGeneralJazenet = require('../helpers/HelperGeneralJazenet.js');
const HelperRestriction = require('../helpers/HelperRestriction.js');


const promiseForeach = require('promise-foreach');

const dateFormat = require('dateformat');
const now = new Date();
const standardDT = dateFormat(now,'yyyy-mm-dd HH:MM:ss');
const moment = require("moment-timezone");
const standardDTNew = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
const newdateForm = dateFormat(new Date(standardDTNew), 'yyyy-mm-dd HH:MM:ss');


const STATIC_IMG_CAT = G_STATIC_IMG_URL+"uploads/jazenet/category/jazestore/"; 
const STATIC_IMG_PROD = G_STATIC_IMG_URL+"uploads/jazenet/company/"; 
const STATIC_IMG_BRAND = G_STATIC_IMG_URL+"uploads/jazenet/category/jazestore_brands/";

const express = require('express');
const router = express.Router();


//--------------------------------------- jazestore home  -------------------------------------------------//

methods.getJazeStoreGeo = function(arrParams,return_result){

	try
	{	
		var query = `SELECT * FROM jaze_jazestore_geo WHERE group_id IN (SELECT group_id FROM jaze_jazestore_geo WHERE meta_key = 'country_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_jazestore_geo WHERE meta_key = 'state_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_jazestore_geo WHERE meta_key = 'city_id' AND meta_value =?)`;

		var obj_result = {
			featured_products : [],
			special_offers : [],
			new_arrivals   : [],
			best_offers    : [],
			categories     : []
		}

		var arr_result = [];

		DB.query(query,[arrParams.country_id,arrParams.state_id,arrParams.city_id], function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				if(parseInt(arrParams.flag) === 0)
				{

					methods.getJazeStoreFeaturedProducts(function(retFeatured){		
						obj_result.featured_products = retFeatured;
						arr_result.push(obj_result);
						return_result(arr_result);

					});

				}
				else if(parseInt(arrParams.flag) === 1)
				{

					methods.getJazeStoreSpecialProducts(data[0].group_id, function(retSpecialProducts){		

						obj_result.special_offers = retSpecialProducts;
						arr_result.push(obj_result);
						return_result(arr_result);

					});

				}
				else if(parseInt(arrParams.flag) === 2)
				{

					obj_result.new_arrivals = [];
					arr_result.push(obj_result);
					return_result(arr_result);
			

				}
				else if(parseInt(arrParams.flag) === 3)
				{

					methods.getJazeStoreBestProducts(data[0].group_id, function(retBestProducts){		

						obj_result.best_offers = retBestProducts;
						arr_result.push(obj_result);
						return_result(arr_result);

					});
				}

				else if(parseInt(arrParams.flag) === 4)
				{

					methods.getJazeStoreCategories(function(retCategories){		

						obj_result.categories = retCategories;
						arr_result.push(obj_result);
						return_result(arr_result);

					});

				}

			}
			else
			{
				return_result(null);
			}

		});
	}
	catch(err)
	{ 
		console.log("getJazeStoreGeo",err); return_result(null); 
	}	
};

methods.getJazeStoreFeaturedProducts = function(return_result){

	try
	{	

		var arrPdoducts = [];

		var query = `SELECT * FROM jaze_products WHERE is_featured = '1' AND parent_id='0' AND category_id NOT IN(1619,1837,1538,1835,1627,1756,1841,694,1765,695,1545,1530,1597,1596) ORDER BY DATE(created_date) DESC LIMIT 10`;

		DB.query(query,null, function(data, error){
		
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var counter = data.length;		
				var jsonItems = Object.values(data);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {		
						
						arrPdoducts.push(jsonItems.id);
						counter -= 1;
						if (counter === 0){ 
							resolve(arrPdoducts);
						}

					})
				}],
				function (result, current) {
					if (counter === 0){ 

						methods.getJazeProductDetails(result[0], function(retProducts){	
							return_result(retProducts);

						});
								
					}
				});

			}
			else
			{
				return_result([]);
			}

		});
	}
	catch(err)
	{ 
		console.log("getJazeStoreFeaturedProducts",err); return_result([]); 
	}	
};



methods.getJazeStoreSpecialProducts = function(geo_id,return_result){

	try
	{	



		var query = `SELECT * FROM jaze_jazestore_special_offer_products WHERE group_id IN (SELECT group_id FROM jaze_jazestore_special_offer_products WHERE meta_key = 'geo_id' AND meta_value =?)`;

		DB.query(query,[geo_id], function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){		
	
					methods.getJazeProductDetails(retConverted[0].prod_id, function(retProducts){		

						return_result(retProducts);

					});

				});

			}
			else
			{
				return_result([]);
			}

		});
	}
	catch(err)
	{ 
		console.log("getJazeStoreSpecialProducts",err); return_result([]); 
	}	
};


methods.getJazeStoreCategories = function(return_result){

	try
	{	

		var arrCategories = [];
		var objCategories = {};

		var query = `SELECT * FROM jaze_jazestore_category_list WHERE group_id IN (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key = 'parent_id' AND meta_value ='0')
		AND group_id NOT IN (56,155,193,208,248)`;

		DB.query(query,null, function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
							
							methods.checkCategoryStatus(jsonItems.group_id, function(retStat){		

								objCategories = {

									category_id     : jsonItems.group_id.toString(),
									category_name   : jsonItems.cate_name,
									category_logo   : STATIC_IMG_CAT+jsonItems.image_url,
									child_status    : retStat
								};

								arrCategories.push(objCategories);
								counter -= 1;
								if (counter === 0){ 
									resolve(arrCategories);
								}

							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							var sortedArray = result[0].sort(function(a, b) {
							    return a.category_name > b.category_name ? 1 : a.category_name < b.category_name ? -1 : 0;
							});		

							return_result(sortedArray);
									
						}
					});

				});

			}
			else
			{
				return_result([]);
			}

		});
	}
	catch(err)
	{ 
		console.log("getJazeStoreCategories",err); return_result([]); 
	}	
};


methods.getJazeStoreBestProducts = function(geo_id,return_result){

	try
	{	



		var query = `SELECT * FROM jaze_jazestore_best_offer_products WHERE group_id IN (SELECT group_id FROM jaze_jazestore_best_offer_products WHERE meta_key = 'geo_id' AND meta_value =?)`;

		DB.query(query,[geo_id], function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){		
	
					methods.getJazeProductDetails(retConverted[0].prod_id, function(retProducts){		

						return_result(retProducts);

					});

				});

			}
			else
			{
				return_result([]);
			}

		});
	}
	catch(err)
	{ 
		console.log("getJazeStoreBestProducts",err); return_result([]); 
	}	
};


methods.getJazeProductDetails = function(arrProductID,return_result){

	try
	{	

		var arrProducts = [];
		var objProducts	= {};

		var queryIn = "SELECT * FROM jaze_products WHERE id IN("+arrProductID+")";
		DB.query(queryIn,null, function(dataIn, error){
		
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		

			
				var counter = dataIn.length;		
				var jsonItems = Object.values(dataIn);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	

						methods.getJazeProductDetailsMeta(jsonItems.id, function(retProductMeta){		

							var accParams = {account_id:jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){										

								methods.getCurrency(retAccountDetails.currency_id, function(retCurrency){	

									methods.getProductBrand(jsonItems.jaze_brand_id, function(retBrand){	

										var sale_duration = '';
										if(Math.sign(retProductMeta.sale_duration) !='-1' && Math.sign(retProductMeta.sale_duration) !='0'){
											sale_duration = retProductMeta.sale_duration+' days left';
										}

										var product_image = "";
										if(retProductMeta.featured_image !=''){
											product_image = STATIC_IMG_PROD+jsonItems.account_id+'/products/'+jsonItems.id+'/'+retProductMeta.featured_image;
										}

										var discount = "";
										if(retProductMeta.sale_price!=''){
											discount = (retProductMeta.sale_price-retProductMeta.regular_price) / retProductMeta.regular_price * 100;
										}

										if(parseInt(Math.floor(discount)) === 0){
											discount = "";
										}else{
											discount = 	Math.floor(discount)+'%'
										}
							
										objProducts = {
											product_id       : jsonItems.id.toString(),
											product_name     : jsonItems.name,
											product_title    : jsonItems.title,
											brand_name       : retBrand,
											product_image    : product_image,
											regular_price    : retProductMeta.regular_price+' '+retCurrency,
											sale_price       : retProductMeta.sale_price+' '+retCurrency,
											percent_discount : discount,
											sale_duration    : sale_duration.toString()

										}

										arrProducts.push(objProducts);

										counter -= 1;
										if (counter === 0){ 
											resolve(arrProducts);
										}

									});
								});
							});
						});
					})
				}],
				function (result, current) {
					if (counter === 0){ 

						var sortedArray = result[0].sort(function(a, b) {
						    return a.product_name > b.product_name ? 1 : a.product_name < b.product_name ? -1 : 0;
						});	

						return_result(sortedArray);
								
					}
				});

			}
			else
			{
				return_result([]);
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getJazeProductDetails",err); return_result([]); 
	}	
};


methods.getJazeProductDetailsMeta = function(product_id,return_result){

	try
	{	

		var arrProducts = [];

		var objProducts	= {
			regular_price  : '',
			sale_price     : '',
			sale_discount  : '',
			featured_image : ''
		};

		var queryIn = "SELECT * FROM jaze_products_meta WHERE product_id =?";
		DB.query(queryIn,[product_id], function(dataIn, error){
		
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		

				methods.convertMultiMetaArrayProduct(dataIn, function(retConverted){		
	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var current_date = dateFormat(now,'yyyy-mm-dd');
							var sale_duration = '';
							if(parseInt(jsonItems.sale_onsale_schedule) === 1)
							{			

								var start = moment(current_date);
								var end =  moment(current_date);

								if(jsonItems.sale_price_start_from !='' && jsonItems.sale_price_start_to !='')
								{
									end = moment(jsonItems.sale_price_start_to);
								}
						
								sale_duration = end.diff(start, "days");
								
							}

							if(parseInt(sale_duration) === 0){
								sale_duration = '';
							}

							objProducts = {
			
								regular_price  : jsonItems.regular_price,
								sale_price     : jsonItems.sale_price,
								sale_discount  : jsonItems.sale_discount,
								featured_image : jsonItems.featured_image,
								sale_duration  : sale_duration

							}

							counter -= 1;
							if (counter === 0){ 
								resolve(objProducts);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(objProducts);
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getJazeProductDetailsMeta",err); return_result(objProducts); 
	}	
};


methods.getProductBrand = function(product_id,return_result){

	try
	{	

		var queryIn = "SELECT * FROM jaze_jazestore_brand_list WHERE group_id =?";
		DB.query(queryIn,[product_id], function(dataIn, error){
		
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		
				methods.convertMultiMetaArray(dataIn, function(retConverted){		
	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							counter -= 1;
							if (counter === 0){ 
								resolve(jsonItems.name);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result('');
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getProductBrand",err); return_result(''); 
	}	
};


//--------------------------------------- jazestore home  -------------------------------------------------//


//--------------------------------------- jazestore category  -------------------------------------------------//



methods.getJazeStoreCategoryList = function(arrParams,return_result){

	try
	{	

		var arrCategories = [];
		var arrResult     = [];
		var objCategories = {};

		var query = `SELECT * FROM jaze_jazestore_category_list WHERE group_id IN (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key = 'parent_id' AND meta_value =?)
		AND group_id NOT IN (56,155,193,208,248)`;

		DB.query(query,[arrParams.parent_id], function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.checkCategoryStatus(jsonItems.group_id, function(retStat){		
							
								objCategories = {
									category_id     : jsonItems.group_id.toString(),
									category_name   : jsonItems.cate_name,
									category_logo   : STATIC_IMG_CAT+jsonItems.image_url,
					  				child_status    : retStat
								}
								arrCategories.push(objCategories);
								counter -= 1;
								if (counter === 0){ 
									resolve(arrCategories);
								}
							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							var sortedArray = result[0].sort(function(a, b) {
							    return a.category_name > b.category_name ? 1 : a.category_name < b.category_name ? -1 : 0;
							});	

							var return_obj = {
								parent_category : {},
								child_list   : sortedArray,
							}

							var parent_obj = {};

							var queryP = `SELECT * FROM jaze_jazestore_category_list WHERE group_id =?`;

							DB.query(queryP,[arrParams.parent_id], function(dataP, error){	

								if(typeof dataP !== 'undefined' && dataP !== null && parseInt(dataP.length) > 0)
								{		
							
									methods.convertMultiMetaArray(dataP, function(retConvertedP){	


	
										var counterP = retConvertedP.length;		
										var jsonItemsP = Object.values(retConvertedP);

										promiseForeach.each(jsonItemsP,[function (jsonItemsP) {
											return new Promise(function (resolveP, reject) {
																console.log(jsonItemsP.parent_id);
												// if(parseInt(jsonItemsP.parent_id) !== 0)
												// {
												// 	counterP -= 1;
												// 	if (counterP === 0){ 
												// 		resolveP(parent_obj);
												// 	}
												// }	
												// else
												// {
													methods.checkCategoryStatus(jsonItemsP.group_id, function(retStatP){		
							
														parent_obj = {
															category_id     : jsonItemsP.group_id.toString(),
															category_name   : jsonItemsP.cate_name,
															category_logo   : STATIC_IMG_CAT+jsonItemsP.image_url,
											  				child_status    : retStatP
														}
						
														counterP -= 1;
														if (counterP === 0){ 
															resolveP(parent_obj);
														}
													});
												// }
										
											})
										}],
										function (resultP, current) {
											if (counterP === 0){ 

												return_obj.parent_category = resultP[0];
												return_result(return_obj);					
											}
										});

									});


								}
								else
								{
									return_result(return_obj);					
								}

							});	
									
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});
	}
	catch(err)
	{ 
		console.log("getJazeStoreCategoryList",err); return_result(null); 
	}	
};



methods.checkCategoryStatus = function(parent_id,return_result)
{ 

	try
	{
		if(parseInt(parent_id) == 57 || parseInt(parent_id) == 208 || parseInt(parent_id) == 696)
		{
		
			return_result('3');
		}
		else
		{
			var query = `SELECT COUNT(*) AS count FROM jaze_jazestore_category_list WHERE group_id IN (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key = 'parent_id' AND meta_value =?)
								AND group_id IN (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key = 'status' AND meta_value = '1') `;

			DB.query(query, [parent_id], function(data, error){

				if( parseInt(data[0].count) !== 0)
				{
					var quer = `SELECT COUNT(*) AS count FROM jaze_user_basic_details WHERE meta_key = 'category_id' AND meta_value =?`;

					DB.query(quer, [parent_id], function(cdata, error){
	
						if( parseInt(data[0].count) === 1 && parseInt(cdata[0].count) === 1)
						{
							return_result('2'); 
						}
						else
						{ 
							return_result('0'); 
						}
					});
				}
				else
				{ 
					return_result('1'); 
				}
			});

		}


	}
	catch(err)
	{ console.log("checkCategoryStatus",err); return_result("0"); }	
}

//--------------------------------------- jazestore category  -------------------------------------------------//


//--------------------------------------- jazestore search product  -------------------------------------------------//

methods.searchJazeStoreProducts = function(arrParams,return_result){

	try
	{	

		var arrProdIDS = [];
		var query = "SELECT * FROM jaze_products WHERE title LIKE '%"+arrParams.keywords+"%' OR country_id='"+arrParams.country_id+"' ";
			query += " OR state_id='"+arrParams.state_id+"' OR city_id='"+arrParams.city_id+"'" ;
			


		DB.query(query,null, function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var counter = data.length;		
				var jsonItems = Object.values(data);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	
										
						arrProdIDS.push(jsonItems.id);
						counter -= 1;
						if (counter === 0){ 
							resolve(arrProdIDS);
						}
					
					})
				}],
				function (result, current) {
					if (counter === 0){ 
						
						var obj_result = {
							keywords  : [],
							product   : [],
							company   : [],
							brand     : []
						};

						var filteredArray = removeDuplicates(result[0]);
					
						if(parseInt(arrParams.flag) === 1)
						{

							methods.getProductsFromSearch(filteredArray,arrParams, function(retProducts){

								methods.getproductKeywords(arrParams, function(retKeywords){
	
									if(retProducts.length > 0 || retKeywords.length > 0)
									{							
										obj_result.product = retProducts;
										obj_result.keywords = retKeywords;
										return_result(obj_result);
									}
									else
									{
										return_result(null);
									}

								});
							});
						}
						else if(parseInt(arrParams.flag) === 2)
						{

							methods.getCompanyFromSearch(arrParams, function(retCompany){

								if(retCompany.length > 0 )
								{																					
									obj_result.company = retCompany;										
									return_result(obj_result);
								}
								else
								{
									return_result(null);
								}
							
							});

						}
						else if(parseInt(arrParams.flag) === 3)
						{
							
							methods.getProductBrandFromSearch(arrParams, function(retBrands){

								if(retBrands.length > 0 )
								{																					
									obj_result.brand = retBrands;
									return_result(obj_result);
								}
								else
								{
									return_result(null);
								}
						
							});

						}									
					}
				});

			}
			else
			{
				return_result(null);
			}

		});
	}
	catch(err)
	{ 
		console.log("searchJazeStoreProducts",err); return_result(null); 
	}	
};

methods.getproductKeywords = function(arrParams,return_result){

	try
	{	

		var arrKeywords = [];
		var objKeywords = {};

		var queryIn = "SELECT * FROM jaze_products WHERE keywords LIKE '%"+arrParams.keywords+"%' AND jaze_category_id NOT IN(208,209,210,56,57,58,59,155,162,248)";
		DB.query(queryIn,null, function(dataIn, error){
		
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		

				var counter = dataIn.length;		
				var jsonItems = Object.values(dataIn);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	

						var keySplit = jsonItems.keywords.split(",");
						var search = new RegExp(arrParams.keywords , 'i'); 
						var b = keySplit.filter(item => search.test(item));

						arrKeywords.push(b);

						counter -= 1;
						if (counter === 0){ 
							resolve(arrKeywords);
						}
					})
				}],
				function (result, current) {
					if (counter === 0){ 

						var arrKeywordsIn = [];

						var counterIn = result[0].length;		
						var jsonItemsIn = Object.values(result[0]);

						promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
							return new Promise(function (resolveIn, reject) {	

								jsonItemsIn.forEach(element => 
									arrKeywordsIn.push(element)
								);

								counterIn -= 1;
								if (counterIn === 0){ 									
									resolveIn(arrKeywordsIn);
								}
							})
						}],
						function (resultIn, current) {
							if (counterIn === 0){ 

								var arrKeywordsInn = [];

								var filteredArray = removeDuplicates(resultIn[0]);

								var counterInn = filteredArray.length;		
								var jsonItemsInn = Object.values(filteredArray);

								promiseForeach.each(jsonItemsInn,[function (jsonItemsInn) {
									return new Promise(function (resolveInn, reject) {	

										objKeywords = {
											keywords : jsonItemsInn
										};

										arrKeywordsInn.push(objKeywords);
										counterInn -= 1;
										if (counterInn === 0){ 
											
											resolveInn(arrKeywordsInn);
										}
									})
								}],
								function (resultInn, current) {
									if (counterInn === 0){ 

										var spliced = resultInn[0];
										if(parseInt(resultInn[0].length) > 5){
											spliced = resultInn[0].splice(0,5);
										}
										
										var sorted = sort_keywords(spliced,arrParams.keywords);

										return_result(sorted);


									}
								});

							}
						});
					
					}
				});
		
			}
			else
			{
				return_result([]);
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getproductKeywords",err); return_result([]); 
	}	
};


methods.getProductsFromSearch = function(arrProductIDs,arrParams,return_result){

	try
	{	

		var arrProducts = [];
		var objProducts = {};

		var queryIn = "SELECT * FROM jaze_products WHERE id IN(?) AND title LIKE '%"+arrParams.keywords+"%' AND jaze_category_id NOT IN(208,209,210,56,57,58,59,155,162,248) AND category_id NOT IN(1841)";
		DB.query(queryIn,[arrProductIDs], function(dataIn, error){
		
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		

				var counter = dataIn.length;		
				var jsonItems = Object.values(dataIn);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	

	
						methods.getJazeProductDetailsMeta(jsonItems.id, function(retProductMeta){		

							// var accParams = {account_id:jsonItems.account_id};
							// HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){										

							// 	methods.getCurrency(retAccountDetails.currency_id, function(retCurrency){	

									methods.getProductBrand(jsonItems.jaze_brand_id, function(retBrand){	

										var sale_duration = '';
										if(Math.sign(retProductMeta.sale_duration) !='-1' && Math.sign(retProductMeta.sale_duration) !='0'){
											sale_duration = retProductMeta.sale_duration+' days left';
										}

										var product_image = "";
										if(retProductMeta.featured_image !=''){
											product_image = STATIC_IMG_PROD+jsonItems.account_id+'/products/'+jsonItems.id+'/thumb/'+retProductMeta.featured_image;
										}

										var discount = "";
										if(retProductMeta.sale_price!=''){
											discount = (retProductMeta.sale_price-retProductMeta.regular_price) / retProductMeta.regular_price * 100;
										}

										if(parseInt(Math.floor(discount)) === 0){
											discount = "";
										}else{
											discount = 	Math.floor(discount)+'%'
										}
							
										objProducts = {
											product_id       : jsonItems.id.toString(),
											product_name     : jsonItems.name,
											product_title    : jsonItems.title,
											brand_name       : retBrand,
											product_image    : product_image,
											regular_price    : (parseFloat(retProductMeta.regular_price).toFixed(2)),
											sale_price       : (parseFloat(retProductMeta.sale_price).toFixed(2)),
											percent_discount : discount,
											sale_duration    : sale_duration.toString(),
											result_flag      : '1'

										}

										arrProducts.push(objProducts);

										counter -= 1;
										if (counter === 0){ 
											resolve(arrProducts);
										}

									});
							// 	});
							// });
						});
					})
				}],
				function (result, current) {
					if (counter === 0){ 

						var sorted = sort_prod(result[0],arrParams.keywords);

						return_result(sorted);					
					}
				});

			}
			else
			{
				return_result([]);
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getProductsFromSearch",err); return_result([]); 
	}	
};


methods.getCompanyFromSearch = function(arrParams,return_result){

	try
	{	

		var arrResCompany = [];
		var objResCompany = {};

		var query = "SELECT * FROM jaze_user_basic_details WHERE account_id IN (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'company_name' AND meta_value LIKE '%"+arrParams.keywords+"%') ";
			query += " AND account_id IN (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'is_merchant_type' AND meta_value ='1')";
			query += " AND account_type = '3' ";

		DB.query(query,null, function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArrayCompany(data,function(retConverted){

					var counterIn = retConverted.length;		
					var jsonItemsIn = Object.values(retConverted);

					promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
						return new Promise(function (resolveIn, reject) {	

							var accParams = {account_id: jsonItemsIn.account_id};

							HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

								if(retAccountDetails!=null)
								{

									methods.getCompanyCategory(retAccountDetails.category_id, function(retCategory){	

										objResCompany = {
											account_id     : retAccountDetails.account_id.toString(),
											name           : retAccountDetails.name,
											logo           : retAccountDetails.logo,							
											category_name  : retCategory.category_name,							
											result_flag    : '2',
										}

										arrResCompany.push(objResCompany);
										counterIn -= 1;
										if (counterIn === 0){ 
											resolveIn(arrResCompany);
										}
									});
								}
								else
								{
									counterIn -= 1;
								}
					
							});
						})
					}],
					function (resultIn, current) {
						if (counterIn === 0){ 
							
							var sortedArray = resultIn[0].sort(function(a, b) {
						   	 	return a.name > b.name ? 1 : a.name < b.name ? -1 : 0;
							});		

							return_result(sortedArray);

						}
					});




				});

			}
			else
			{
				return_result([]);
			}

		});

		// var queryIn = "SELECT * FROM jaze_products WHERE id IN(?) AND jaze_category_id NOT IN(208,209,210,56,57,58,59) ";

		// DB.query(queryIn,[arrProductIDs], function(dataIn, error){
		
		// 	if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
		// 	{		

		// 		var counter = dataIn.length;		
		// 		var jsonItems = Object.values(dataIn);

		// 		promiseForeach.each(jsonItems,[function (jsonItems) {
		// 			return new Promise(function (resolve, reject) {	

		// 				arrCompany.push(jsonItems.account_id);
		// 				counter -= 1;
		// 				if (counter === 0){ 
		// 					resolve(arrCompany);
		// 				}
				
		// 			})
		// 		}],
		// 		function (result, current) {
		// 			if (counter === 0){ 

		// 				var filteredArray = removeDuplicates(result[0]);

		// 				var arrResCompany = [];
		// 				var objResCompany = {};

		// 				var counterIn = filteredArray.length;		
		// 				var jsonItemsIn = Object.values(filteredArray);

		// 				promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
		// 					return new Promise(function (resolveIn, reject) {	

		// 						var accParams = {account_id: jsonItemsIn};

		// 						HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

		// 							if(retAccountDetails!=null)
		// 							{

		// 								methods.getCompanyCategory(retAccountDetails.category_id, function(retCategory){	

		// 									objResCompany = {
		// 										account_id     : retAccountDetails.account_id.toString(),
		// 										name           : retAccountDetails.name,
		// 										logo           : retAccountDetails.logo,							
		// 										category_name  : retCategory.category_name,							
		// 										result_flag    : '2',
		// 									}

		// 									if(retAccountDetails.is_merchant == 1){
		// 										arrResCompany.push(objResCompany);
		// 									}

		// 									counterIn -= 1;
		// 									if (counterIn === 0){ 
		// 										resolveIn(arrResCompany);
		// 									}
		// 								});
		// 							}
		// 							else
		// 							{
		// 								counterIn -= 1;
		// 							}
						
		// 						});
		// 					})
		// 				}],
		// 				function (resultIn, current) {
		// 					if (counterIn === 0){ 
								
		// 						var sortedArray = resultIn[0].sort(function(a, b) {
		// 					   	 	return a.name > b.name ? 1 : a.name < b.name ? -1 : 0;
		// 						});		

		// 						return_result(sortedArray);

		// 					}
		// 				});

		// 			}
		// 		});

		
		// 	}
		// 	else
		// 	{
		// 		return_result([]);
		// 	}
			
		// });
	}
	catch(err)
	{ 
		console.log("getCompanyFromSearch",err); return_result([]); 
	}	
};


methods.getCompanyCategory = function(category_id,return_result){

	try
	{	

		var objCategory = {
			category_id    : '',
			category_name  : '',
			category_logo  : ''
		};

		var queryIn = "SELECT * FROM jaze_jazestore_category_list WHERE group_id =?";
		DB.query(queryIn,[category_id], function(dataIn, error){
		
			

			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		

				methods.convertMultiMetaArray(dataIn, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var logo = '';
							if(jsonItems.image_url!=''){
								logo = STATIC_IMG_CAT+jsonItems.image_url;
							}

							objCategory = {
								category_id    : jsonItems.group_id.toString(),
								category_name  : jsonItems.cate_name,
								category_logo  : logo
							}

							counter -= 1;
							if (counter === 0){ 
								resolve(objCategory);							
							}
					
						})
					}],
					function (result, current) {
						if (counter === 0){ 
	
							return_result(result[0]);

						}
					});
				});

		
			}
			else
			{
				return_result(objCategory);
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getCompanyCategory",err); return_result(objCategory); 
	}	
};

methods.getProductBrandFromSearch = function(arrParams,return_result){

	try
	{	

		var arrBrands = [];
		var objBrands = {};

		var queryIn = "SELECT * FROM jaze_jazestore_brand_list WHERE group_id IN (SELECT group_id FROM jaze_jazestore_brand_list WHERE meta_key ='name' AND meta_value LIKE '%"+arrParams.keywords+"%' ) ";
			queryIn += " AND group_id IN (SELECT group_id FROM jaze_jazestore_brand_list WHERE meta_key ='status' AND meta_value = '1' )";

		DB.query(queryIn,null, function(dataIn, error){
		
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		

				methods.convertMultiMetaArray(dataIn, function(retConverted){	

					var counterIn = retConverted.length;		
					var jsonItemsIn = Object.values(retConverted);

					promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
						return new Promise(function (resolveIn, reject) {	
							
							methods.getCategoryBrand(jsonItemsIn.group_id, function(retCatBrand){

								if(retCatBrand!=null){
									arrBrands.push(retCatBrand);
								}

								var logo = '';
								if(jsonItemsIn.logo){
									if(jsonItemsIn.logo !=''){
										logo = 'https://www.img.jazenet.com/images/master.resources/uploads/jazenet/category/jazestore_brands/'+jsonItemsIn.logo;
									}
								}

								objBrands = {
									brand_id       : jsonItemsIn.group_id.toString(),
									brand_name     : jsonItemsIn.name,
									brand_logo     : logo,
									brand_keywords : '',
									result_flag    : '3',
								};
						

								arrBrands.push(objBrands);
								counterIn -= 1;
								if (counterIn === 0){ 
									resolveIn(arrBrands);
								}

							});
						})
					}],
					function (resultIn, current) {
						if (counterIn === 0){ 

							var sortedArray = resultIn[0].sort(function(a, b) {
			   	 				return a.result_flag > b.result_flag ? 1 : a.result_flag < b.result_flag ? -1 : 0;
							});		

							return_result(sortedArray);
						}
					});

				});

			}
			else
			{
				return_result([]);
			}
			
		});

	}
	catch(err)
	{ 
		console.log("getProductBrandFromSearch",err); return_result([]); 
	}	
};


methods.getCategoryBrand = function(brand_id,return_result){

	try
	{	

		var arrBrands = [];
		var objBrands = {};

		var queryIn = "SELECT * FROM jaze_jazestore_category_brand_list  WHERE group_id IN (SELECT group_id FROM jaze_jazestore_category_brand_list WHERE meta_key = 'brand_id' AND FIND_IN_SET(?,meta_value)>0 ) ";
		DB.query(queryIn,[brand_id], function(dataIn, error){
		

			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		
				methods.convertMultiMetaArray(dataIn, function(retConverted){	

	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getJazeCategoryDetails(jsonItems.category_id,brand_id, function(retCategory){

								objBrands = {
									brand_id       : jsonItems.category_id.toString(),
									brand_name     : retCategory.brand_name,
									brand_logo     : retCategory.brand_logo,
									brand_keywords : retCategory.category_name,
									result_flag    : '4',
								};

					
								counter -= 1;
								if (counter === 0){ 
									resolve(objBrands);							
								}

							});
					
						})
					}],
					function (result, current) {
						if (counter === 0){ 
										
							return_result(result[0]);

						}
					});
				});

		
			}
			else
			{
				return_result(null);
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getCategoryBrand",err); return_result(null); 
	}	
};


methods.getJazeCategoryDetails = function(category_id,brand_id,return_result){

	try
	{	

		var arrCategories = [];
		var objCategories = {};

		var query = `SELECT * FROM jaze_jazestore_category_list WHERE group_id=?`;

		DB.query(query,[category_id], function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
							
							var queryIn = "SELECT * FROM jaze_jazestore_brand_list  WHERE group_id=? ";
							DB.query(queryIn,[brand_id], function(dataIn, error){
							

								if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
								{		
									methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

										var logo = '';
										if(retConvertedIn[0].logo){
											if(retConvertedIn[0].logo !=''){
												logo = 'https://www.img.jazenet.com/images/master.resources/uploads/jazenet/category/jazestore_brands/'+retConvertedIn[0].logo;
											}
										}

										objCategories = {

											brand_id       : retConvertedIn[0].group_id.toString(),
											brand_name     : retConvertedIn[0].name,
											brand_logo     : logo,
											category_id    : jsonItems.group_id.toString(),
											category_name  : jsonItems.cate_name,
											category_logo  : STATIC_IMG_CAT+jsonItems.image_url,
									
										};

										counter -= 1;
										if (counter === 0){ 
											resolve(objCategories);
										}
									});
								}

							});

						})
					}],
					function (result, current) {
						if (counter === 0){ 

			

							return_result(result[0]);
									
						}
					});

				});

			}
			else
			{
				return_result([]);
			}

		});
	}
	catch(err)
	{ 
		console.log("getJazeCategoryDetails",err); return_result([]); 
	}	
};

methods.searchJazeStoreProductsCount = function(arrParams,return_result){

	try
	{	

		var arrProdIDS = [];

		var obj_result = {
			product_count   : '0',
			company_count   : '0',
			brand_count     : '0'
		};

		var query = "SELECT * FROM jaze_products WHERE title LIKE '%"+arrParams.keywords+"%' OR country_id='"+arrParams.country_id+"' ";
			query += " OR state_id='"+arrParams.state_id+"' OR city_id='"+arrParams.city_id+"'";

		DB.query(query,null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var counter = data.length;		
				var jsonItems = Object.values(data);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	

						arrProdIDS.push(jsonItems.id);
				
						counter -= 1;
						if (counter === 0){ 
							resolve(arrProdIDS);
						}
					
					})
				}],
				function (result, current) {
					if (counter === 0){ 
					

						var filteredArray = removeDuplicates(result[0]);

						methods.getProductsFromSearchCount(filteredArray,arrParams, function(retProductsCount){

							methods.getproductKeywordsCount(arrParams, function(retKeywordsCount){


								methods.getCompanyFromSearchCount(arrParams, function(retCompanyCount){

									methods.getProductBrandFromSearchCount(arrParams, function(retBrandCount){

										var sumKeyProd = parseInt(retProductsCount)+parseInt(retKeywordsCount);
										obj_result.product_count = sumKeyProd.toString(); 
										obj_result.company_count = retCompanyCount;
										obj_result.brand_count   = retBrandCount;

										return_result(obj_result);

									});
								});
							});
						});
								
					}
				});

			}
			else
			{

				methods.getproductKeywordsCount(arrParams, function(retKeywordsCount){

					methods.getCompanyFromSearchCount(arrParams, function(retCompanyCount){

						methods.getProductBrandFromSearchCount(arrParams, function(retBrandCount){

							var sumKeyProd = parseInt(retKeywordsCount);
							obj_result.product_count = sumKeyProd.toString(); 
							obj_result.company_count = retCompanyCount;
							obj_result.brand_count   = retBrandCount;

							return_result(obj_result);

						});
					});
				});
			}

		});
	}
	catch(err)
	{ 
		console.log("searchJazeStoreProductsCount",err); return_result(obj_result); 
	}	
};



methods.getproductKeywordsCount = function(arrParams,return_result){

	try
	{	

		var arrKeywords = [];
		var objKeywords = {};

		var queryIn = "SELECT * FROM jaze_products WHERE keywords LIKE '%"+arrParams.keywords+"%' AND jaze_category_id NOT IN(208,209,210,56,57,58,59)";
		DB.query(queryIn,null, function(dataIn, error){
		
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		
	
				var counter = dataIn.length;		
				var jsonItems = Object.values(dataIn);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	

						var keySplit = jsonItems.keywords.split(",");
						var search = new RegExp(arrParams.keywords , 'i'); 
						var b = keySplit.filter(item => search.test(item));

						arrKeywords.push(b);

						counter -= 1;
						if (counter === 0){ 
							resolve(arrKeywords);
						}
					})
				}],
				function (result, current) {
					if (counter === 0){ 

						var arrKeywordsIn = [];

						var counterIn = result[0].length;		
						var jsonItemsIn = Object.values(result[0]);

						promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
							return new Promise(function (resolveIn, reject) {	

								jsonItemsIn.forEach(element => 
									arrKeywordsIn.push(element)
								);

								counterIn -= 1;
								if (counterIn === 0){ 
									
									resolveIn(arrKeywordsIn);
								}
							})
						}],
						function (resultIn, current) {
							if (counterIn === 0){ 

								var arrKeywordsInn = [];

								var filteredArray = removeDuplicates(resultIn[0]);

								var counterInn = filteredArray.length;		
								var jsonItemsInn = Object.values(filteredArray);

								promiseForeach.each(jsonItemsInn,[function (jsonItemsInn) {
									return new Promise(function (resolveInn, reject) {	

										objKeywords = {
											keywords : jsonItemsInn
										};

										arrKeywordsInn.push(objKeywords);
										counterInn -= 1;
										if (counterInn === 0){ 
											
											resolveInn(arrKeywordsInn);
										}
									})
								}],
								function (resultInn, current) {
									if (counterInn === 0){ 
										return_result(resultInn[0].length.toString());
									}
								});

							}
						});
					}
				});
			}
			else
			{
				return_result('0');
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getproductKeywordsCount",err); return_result('0'); 
	}	
};


methods.getProductsFromSearchCount = function(arrProductIDs,arrParams,return_result){

	try
	{	
		
		var arrProducts = [];
		var objProducts = {};

		var queryIn = "SELECT COUNT(*) as count FROM jaze_products WHERE id IN(?) AND title LIKE '%"+arrParams.keywords+"%' AND jaze_category_id NOT IN(208,209,210,56,57,58,59,155,162,248) AND category_id NOT IN(1841) ";
		DB.query(queryIn,[arrProductIDs], function(dataIn, error){
		
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0){		
				return_result(dataIn[0]['count'].toString());
			}else{
				return_result('0');
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getProductsFromSearchCount",err); return_result('0'); 
	}	
};


methods.getCompanyFromSearchCount = function(arrParams,return_result){

	try
	{	


		var arrResCompany = [];

		var query = "SELECT * FROM jaze_user_basic_details WHERE account_id IN (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'company_name' AND meta_value LIKE '%"+arrParams.keywords+"%') ";
			query += " AND account_id IN (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'is_merchant_type' AND meta_value ='1')";
			query += " AND account_type = '3' ";

		DB.query(query,null, function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArrayCompany(data,function(retConverted){

					var counterIn = retConverted.length;		
					var jsonItemsIn = Object.values(retConverted);

					promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
						return new Promise(function (resolveIn, reject) {	

							arrResCompany.push(jsonItemsIn.account_id);
							counterIn -= 1;
							if (counterIn === 0){ 
								resolveIn(arrResCompany);
							}

						})
					}],
					function (resultIn, current) {
						if (counterIn === 0){ 
							
							var filteredArray = removeDuplicates(resultIn[0]);
							var company_count = filteredArray.length.toString();
							return_result(company_count);

						}
					});




				});

			}
			else
			{
				return_result('0');
			}

		});

		// var arrCompany = [];
		// var objCompany = {};

		// var queryIn = "SELECT * FROM jaze_products WHERE id IN(?) AND title LIKE '%"+arrParams.keywords+"%' AND jaze_category_id NOT IN(208,209,210,56,57,58,59) ";
		// DB.query(queryIn,[arrProductIDs], function(dataIn, error){
		
		// 	if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
		// 	{		

		// 		var counter = dataIn.length;		
		// 		var jsonItems = Object.values(dataIn);

		// 		promiseForeach.each(jsonItems,[function (jsonItems) {
		// 			return new Promise(function (resolve, reject) {	

		// 				arrCompany.push(jsonItems.account_id);
		// 				counter -= 1;
		// 				if (counter === 0){ 
		// 					resolve(arrCompany);
		// 				}
				
		// 			})
		// 		}],
		// 		function (result, current) {
		// 			if (counter === 0){ 

		// 				var filteredArray = removeDuplicates(result[0]);

		// 				methods.checkCompanyisMerchant(filteredArray,function(retCount){
		// 					return_result(retCount);
		// 				});

						

		// 			}
		// 		});

		
		// 	}
		// 	else
		// 	{
		// 		return_result('0');
		// 	}
			
		// });
	}
	catch(err)
	{ 
		console.log("getCompanyFromSearchCount",err); return_result('0'); 
	}	
};


methods.checkCompanyisMerchant = function(arrAccountID,return_result){

	try
	{	

		var arrCompany = [];
		var objCompany = {};

		var queryIn = `SELECT * FROM jaze_user_basic_details WHERE account_id IN (?) 
		AND account_id IN (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'is_merchant_type' AND meta_value = '1') `;

		DB.query(queryIn,[arrAccountID], function(dataIn, error){
		
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		
				methods.convertMultiMetaArrayCompany(dataIn,function(retConverted){

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							arrCompany.push(jsonItems.account_id);
							counter -= 1;
							if (counter === 0){ 
								resolve(arrCompany);
							}
					
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							var company_count = result[0].length.toString();
							return_result(company_count);

						}
					});

				});

			}
			else
			{
				return_result('0');
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getCompanyFromSearchCount",err); return_result('0'); 
	}	
};

methods.getProductBrandFromSearchCount = function(arrParams,return_result){

	try
	{	

		var arrBrands = [];
		var objBrands = {};

		var queryIn = "SELECT * FROM jaze_jazestore_brand_list WHERE group_id IN (SELECT group_id FROM jaze_jazestore_brand_list WHERE meta_key ='name' AND meta_value LIKE '%"+arrParams.keywords+"%' ) ";
			queryIn += " AND group_id IN (SELECT group_id FROM jaze_jazestore_brand_list WHERE meta_key ='status' AND meta_value = '1' ) ";

		DB.query(queryIn,null, function(dataIn, error){
				
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		

				var counter = dataIn.length;		
				var jsonItems = Object.values(dataIn);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	

						methods.getCategoryBrandCount(jsonItems.group_id, function(retCategory){

							if(retCategory!=null){
								arrBrands.push(retCategory);
							}

							arrBrands.push(jsonItems.group_id);
							counter -= 1;
							if (counter === 0){ 
								resolve(arrBrands);
							}
				
						});
					})
				}],
				function (result, current) {
					if (counter === 0){ 

						var filteredArray = removeDuplicates(result[0]);
						var brand_count = filteredArray.length.toString();
						
						return_result(brand_count);

					}
				});

			}
			else
			{
				return_result('0');
			}
			
		});

	}
	catch(err)
	{ 
		console.log("getProductBrandFromSearchCount",err); return_result('0'); 
	}	
};



methods.getCategoryBrandCount = function(brand_id,return_result){

	try
	{	

		var arrBrands = [];
		var objBrands = {};

		var queryIn = "SELECT * FROM jaze_jazestore_category_brand_list  WHERE group_id IN (SELECT group_id FROM jaze_jazestore_category_brand_list WHERE meta_key = 'brand_id' AND FIND_IN_SET(?,meta_value)>0 ) ";
		DB.query(queryIn,[brand_id], function(dataIn, error){

			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0){		

				return_result(dataIn[0].group_id);

			}else{
				return_result(null);
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getCategoryBrandCount",err); return_result(null); 
	}	
};

//--------------------------------------- jazestore search product  -------------------------------------------------//


//--------------------------------------- jazestore company profile  -------------------------------------------------//

methods.getJazeStoreCompanyProfile = function(arrParams,return_result){

	try
	{	

	
		var arrReturn = [];
		var arrObject = {
			company_information : '',
			category_list    : '',
			product_list     : '',
		};

		var accParams = {account_id: arrParams.company_id};

		HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

			if(retAccountDetails!=null)
			{

				methods.getCompanyCategory(retAccountDetails.category_id, function(retCategory){

					methods.getCompanyCategoryDetails(retAccountDetails.account_id, function(retCategoryDetails){

						methods.getCompanyFollowersCount(retAccountDetails.account_id, function(retFollowersCount){

							methods.getCompanyReviews(retAccountDetails.account_id, function(retReviews){

								methods.getCompanyProductList(retCategoryDetails[0].category_id, function(retProd){

									var delivery_hours   = [];
									var delivery_charges = [];
									var payment_methods  = [];

									var delivery_obj1 = {
										day   : 'Sunday - Wednesday',
										time  : '10:00am - 6:00pm',
									}
									var delivery_obj2 = {
										day   : 'Thursday - Saturday',
										time  : '09:00am - 4:00pm',
									}

									var delivery_cha1 = {
										place       : 'JLT Cluster C',
										min_amount  : '300 AED',
										fee         : '10 AED',
									};

									var delivery_cha2 = {
										place       : 'Al Wahda Mall',
										min_amount  : '500 AED',
										fee         : '30 AED',
									};

									var payment_obj1 = {
										name   : 'Jazepay',
										logo   : 'jazepay.jpg',
									};

									var payment_obj2 = {
										name   : 'Paypal',
										logo   : 'paypal.jpg',
									};

									delivery_hours.push(delivery_obj1);
									delivery_hours.push(delivery_obj2);

									delivery_charges.push(delivery_cha1);
									delivery_charges.push(delivery_cha2);

									payment_methods.push(payment_obj1);
									payment_methods.push(payment_obj2);

									arrObject.company_information = {
										account_id    : retAccountDetails.account_id,
										account_type  : retAccountDetails.account_type,
										name          : retAccountDetails.name,
										logo          : retAccountDetails.logo,
										categoryi_id  : retCategory.category_id,
										category_name : retCategory.category_name,
										followers_count : retFollowersCount,
										reviews_count   : retReviews,
										delivery_info : {

											delivery_hours   : delivery_hours,
											payment_methods  : payment_methods,
											delivery_charges : delivery_charges,

										}

									};

									arrObject.category_list = retCategoryDetails;

									arrObject.product_list = [];


									arrReturn.push(arrObject);

									return_result(arrObject);

								});
							});
						});
					});
				});
			}
			else
			{
				return_result(null)
			}
		});

	}
	catch(err)
	{ 
		console.log("getJazeStoreCompanyProfile",err); return_result(null); 
	}	
};



methods.getCompanyCategoryDetails = function(account_id,return_result){

	try
	{	

		var objCategory = {};
		var arrCategory = [];

		var queryIn = `SELECT * FROM jaze_company_product_comp_category 
			WHERE group_id IN (SELECT group_id FROM jaze_company_product_comp_category WHERE meta_key = 'account_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_company_product_comp_category WHERE meta_key = 'parent_id' AND meta_value ='0')`;

		DB.query(queryIn,[account_id], function(dataIn, error){
		
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		

				methods.convertMultiMetaArray(dataIn, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getCategoryChilds(jsonItems.group_id, function(retChild){

								objCategory = {
									parent_id      : jsonItems.parent_id.toString(),
									category_id    : jsonItems.group_id.toString(),
									category_name  : jsonItems.category_name,
									sort_order     : jsonItems.sort_order.toString(),
									status         : '0',
									child_list     : retChild
								}

								arrCategory.push(objCategory);
								counter -= 1;
								if (counter === 0){ 
									resolve(arrCategory);	
								}
					
							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							var sortedArray = result[0].sort(function(a, b) {
						      	return b.sort_order < a.sort_order ? 1 : b.sort_order < a.sort_order ? -1 : 0;
							});

							sortedArray[0].status = '1';
							return_result(sortedArray);
						}
					});
				});
			}
			else
			{
				return_result([]);
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getCompanyCategoryDetails",err); return_result([]); 
	}	
};


methods.getCategoryChilds = function(parent_id,return_result){

	try
	{	

		var objCategory = {};
		var arrCategory = [];

		var queryIn = `SELECT * FROM jaze_company_product_comp_category WHERE 
			group_id IN (SELECT group_id FROM jaze_company_product_comp_category WHERE meta_key = 'parent_id' AND meta_value =?)`;

		DB.query(queryIn,[parent_id], function(dataIn, error){
		
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		

				methods.convertMultiMetaArray(dataIn, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							objCategory = {
								parent_id      : jsonItems.parent_id.toString(),
								category_id    : jsonItems.group_id.toString(),
								category_name  : jsonItems.category_name
							}

							arrCategory.push(objCategory);

							counter -= 1;
							if (counter === 0){ 
								resolve(arrCategory);							
							}
					
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});
				});

			}
			else
			{
				return_result([]);
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getCategoryChilds",err); return_result([]); 
	}	
};


methods.getCompanyFollowersCount = function(account_id,return_result){

	try
	{	

		var objCategory = {};
		var arrCategory = [];

		var queryIn = `SELECT * FROM jaze_follower_groups_meta WHERE group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'follow_account_id' AND meta_value =?)`;

		DB.query(queryIn,[account_id], function(dataIn, error){
		
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		

				methods.convertMultiMetaArray(dataIn, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getCategoryChilds(jsonItems.group_id, function(retChild){

								arrCategory.push(jsonItems.group_id);
								counter -= 1;
								if (counter === 0){ 
									resolve(arrCategory);	
								}
					
							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0].length.toString());
						}
					});
				});
			}
			else
			{
				return_result('0');
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getCompanyFollowersCount",err); return_result('0'); 
	}	
};


methods.getCompanyReviews =  function (account_id,return_result){
	
	try
	{
		if(parseInt(account_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_reviews WHERE  meta_key IN ('rating') ";
				query += " AND group_id IN (SELECT group_id FROM jaze_reviews WHERE meta_key = 'profile_account_id'  AND meta_value = "+account_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_reviews WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						methods.convertMultiMetaArray(data, function(data_result){
						
							var no_reviews = 0;

							var tot_stars = 0;

							for (var i in data_result) 
							{
								if( parseFloat(data_result[i].rating) > 0)
								{
									tot_stars =  tot_stars + parseFloat(data_result[i].rating);
									no_reviews++;
								}
							}

							var average = (tot_stars / no_reviews).toFixed(1);

							return_result(average);

						});
					}
					else
					{
						return_result('0');
					}
			    });
		}
		else
		{
			return_result('0');
		}
	}
	catch(err)
	{ console.log("getCompanyReviews",err); return_result('0'); }	
};


methods.getCompanyProductList = function(category_id,return_result){

	try
	{	
		var arrCategory = [];
		var query = `SELECT * FROM jaze_company_product_comp_category WHERE group_id IN (SELECT group_id FROM jaze_company_product_comp_category WHERE meta_key = 'parent_id' AND meta_value =?)`;

		DB.query(query,[category_id], function(data, error){
		
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							arrCategory.push(jsonItems.group_id);
							counter -= 1;
							if (counter === 0){ 
								resolve(arrCategory);	
							}			
					
						})
					}],
					function (result, current) {
						if (counter === 0){ 

		
							var arrProducts = [];
							var objProducts	= {};

							var queryIn = "SELECT * FROM jaze_products WHERE company_category_id IN("+result[0]+")";
							DB.query(queryIn,null, function(dataIn, error){
							
								if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
								{		
								
									var counterIn = dataIn.length;		
									var jsonItemsIn = Object.values(dataIn);

									promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
										return new Promise(function (resolveIn, reject) {	

											methods.getJazeProductDetailsMeta(jsonItemsIn.id, function(retProductMeta){		

												var accParams = {account_id:jsonItemsIn.account_id};
												HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){										

													methods.getCurrency(retAccountDetails.currency_id, function(retCurrency){	

														methods.getProductBrand(jsonItemsIn.jaze_brand_id, function(retBrand){	

															var sale_duration = '';
															if(Math.sign(retProductMeta.sale_duration) !='-1' && Math.sign(retProductMeta.sale_duration) !='0'){
																sale_duration = retProductMeta.sale_duration+' days left';
															}

															var product_image = "";
															if(retProductMeta.featured_image !=''){
																product_image = STATIC_IMG_PROD+jsonItemsIn.account_id+'/products/'+jsonItemsIn.id+'/thumb/'+retProductMeta.featured_image;
															}

															var discount = "";
															if(retProductMeta.sale_price!=''){
																discount = (retProductMeta.sale_price-retProductMeta.regular_price) / retProductMeta.regular_price * 100;
															}

															if(parseInt(Math.floor(discount)) === 0){
																discount = "";
															}else{
																discount = 	Math.floor(discount)+'%'
															}
												
															objProducts = {
																product_id       : jsonItemsIn.id.toString(),
																product_name     : jsonItemsIn.name,
																product_title    : jsonItemsIn.title,
																brand_name       : retBrand,
																product_image    : product_image,
																regular_price    : retProductMeta.regular_price+' '+retCurrency,
																sale_price       : retProductMeta.sale_price+' '+retCurrency,
																percent_discount : discount,
																sale_duration    : sale_duration.toString()

															}

															arrProducts.push(objProducts);

															counterIn -= 1;
															if (counterIn === 0){ 
																resolveIn(arrProducts);
															}

														});
													});
												});
											});
										})
									}],
									function (resultIn, current) {
										if (counterIn === 0){ 

											var sortedArray = resultIn[0].sort(function(a, b) {
											    return a.product_name > b.product_name ? 1 : a.product_name < b.product_name ? -1 : 0;
											});	

											return_result(sortedArray);
													
										}
									});

								}
								else
								{
									return_result([]);
								}
								
							});
						}
					});

				});
			}
			else
			{

				var arrProducts = [];
				var objProducts	= {};

				var queryIn = "SELECT * FROM jaze_products WHERE company_category_id IN("+category_id+")";
				DB.query(queryIn,[category_id], function(dataIn, error){
				
					if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
					{		

					
						var counterIn = dataIn.length;		
						var jsonItemsIn = Object.values(dataIn);

						promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
							return new Promise(function (resolveIn, reject) {	

								methods.getJazeProductDetailsMeta(jsonItemsIn.id, function(retProductMeta){		

									var accParams = {account_id:jsonItemsIn.account_id};
									HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){										

										methods.getCurrency(retAccountDetails.currency_id, function(retCurrency){	

											methods.getProductBrand(jsonItemsIn.jaze_brand_id, function(retBrand){	

												var sale_duration = '';
												if(Math.sign(retProductMeta.sale_duration) !='-1' && Math.sign(retProductMeta.sale_duration) !='0'){
													sale_duration = retProductMeta.sale_duration+' days left';
												}

												var product_image = "";
												if(retProductMeta.featured_image !=''){
													product_image = STATIC_IMG_PROD+jsonItemsIn.account_id+'/products/'+jsonItemsIn.id+'/thumb/'+retProductMeta.featured_image;
												}

												var discount = "";
												if(retProductMeta.sale_price!=''){
													discount = (retProductMeta.sale_price-retProductMeta.regular_price) / retProductMeta.regular_price * 100;
												}

												if(parseInt(Math.floor(discount)) === 0){
													discount = "";
												}else{
													discount = 	Math.floor(discount)+'%'
												}
									
												objProducts = {
													product_id       : jsonItemsIn.id.toString(),
													product_name     : jsonItemsIn.name,
													product_title    : jsonItemsIn.title,
													brand_name       : retBrand,
													product_image    : product_image,
													regular_price    : retProductMeta.regular_price+' '+retCurrency,
													sale_price       : retProductMeta.sale_price+' '+retCurrency,
													percent_discount : discount,
													sale_duration    : sale_duration.toString()

												}

												arrProducts.push(objProducts);

												counterIn -= 1;
												if (counterIn === 0){ 
													resolveIn(arrProducts);
												}

											});
										});
									});
								});
							})
						}],
						function (resultIn, current) {
							if (counterIn === 0){ 

								var sortedArray = resultIn[0].sort(function(a, b) {
								    return a.product_name > b.product_name ? 1 : a.product_name < b.product_name ? -1 : 0;
								});	

								return_result(sortedArray);
										
							}
						});

					}
					else
					{
						return_result([]);
					}
					
				});
			}

		});

	}
	catch(err)
	{ 
		console.log("getCompanyProductList",err); return_result([]); 
	}	
};


//--------------------------------------- jazestore company profile  -------------------------------------------------//


//--------------------------------------- jazestore company category products  -------------------------------------------------//


methods.getCompanyCategoryProducts =  function (arrParams,return_result){
	
	try
	{			
		var ret_obj = {
			category_list : {},
			product_list  : []
		}

		var accParams = {account_id: arrParams.company_id};
		HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

			if(retAccountDetails !=null)
			{

				var arrPdoducts = [];
				var query = "SELECT * FROM jaze_products WHERE account_id =? AND company_category_id IN("+arrParams.category_id+") AND is_featured='1' AND parent_id='0' ORDER BY DATE(created_date) DESC";
				DB.query(query,[arrParams.company_id], function(data, error){

					console.log(data);
				
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{

						var counter = data.length;		
						var jsonItems = Object.values(data);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
								
								arrPdoducts.push(jsonItems.id);
								counter -= 1;
								if (counter === 0){ 
									resolve(arrPdoducts);
								}

							})
						}],
						function (result, current) {
							if (counter === 0){ 

								methods.getCompanyCategoryProductsParent(arrParams, function(retMain){	

									ret_obj.category_list = retMain;

									methods.getJazeProductDetails(result[0], function(retProducts){	
									
										ret_obj.product_list = retProducts;

										return_result(ret_obj);

									});	
								});	
							}
						});

					}
					else
					{
						methods.getCompanyCategoryProductsParent(arrParams, function(retMain){	
							ret_obj.category_list = retMain;
							return_result(ret_obj);
						});	
					}

				});
			}
			else
			{
				return_result(null);
			}

		});
	
	}
	catch(err)
	{ console.log("getCompanyCategoryProducts",err); return_result(null); }	
};

methods.getCompanyCategoryProductsParent =  function (arrParams,return_result){
	
	try
	{	


		var query = `SELECT * FROM jaze_company_product_comp_category WHERE group_id =?`;
		DB.query(query,[arrParams.category_id], function(data, error){
		
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
							

							methods.checkCategoryStatus(jsonItems.group_id, function(retStat){		

								var objCategories = {

									category_id     : jsonItems.group_id.toString(),
									category_name   : jsonItems.category_name,
									child_status    : retStat
								};
					
								counter -= 1;
								if (counter === 0){ 
									resolve(objCategories);
								}

							});
			
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							return_result(result[0]);
				
						}
					});
				});

			}
			else
			{
				return_result({});
			}

		});
	
	}
	catch(err)
	{ console.log("getCompanyCategoryProducts",err); return_result({}); }	
};


//--------------------------------------- jazestore company category products   ------------------------------------------------//


//--------------------------------------- jazestore product attributes ---------------------------------------------------------//


methods.getJazeStoreProductAttributes = function(arrParams,return_result){


	try
	{
		var mainArr = [];
		var objController = {};

		var sort_order;
		var trigger = 0;
		var object_name;

		var quer = `SELECT * FROM jaze_jazestore_attributes_groups WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups WHERE meta_key = 'cat_id' AND meta_value =? ) `;
		quer +=	` AND group_id IN (SELECT group_id FROM  jaze_jazestore_attributes_groups  WHERE meta_key = 'status' AND meta_value = '1' )`;

		DB.query(quer,[arrParams.category_id], function(data, error){



			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){	


					if(retConverted)
					{
						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (val) {
							return new Promise(function (resolve, reject) {		
								
	  							methods.getCategoryAttributeValue(val.group_id,val.input_type, function(retMainArr){ 


									if(val.input_type)
									{

										if(val.input_type === "multiselect_image_button"){

											controller_type = '1';
											attribute_option_labe = 'all';

										 	objController = {
												controller_type:'1',
												attribute_id:val.group_id.toString(),
												attribute_label:val.label_name,
												home_status:val.is_details_page.toString(),
											 	home_sort_order:val.details_sort_order.toString(),
											 	adv_status:val.is_listing_page.toString(),
											 	adv_sort_order:val.listing_sort_order.toString(),
											 	attribute_option_label:'all',
											 	items_value_list:retMainArr
											};
							
										}else if(val.input_type === "multiselect_dropdown"){

											controller_type = '2';
											attribute_option_labe = 'all';

										 	objController = {
												controller_type:'2',
												attribute_id:val.group_id.toString(),
												attribute_label:val.label_name,
												home_status:val.is_details_page.toString(),
											 	home_sort_order:val.details_sort_order.toString(),
											 	adv_status:val.is_listing_page.toString(),
											 	adv_sort_order:val.listing_sort_order.toString(),
											 	attribute_option_label:'all',
											 	items_value_list:retMainArr
											};

										}else if(val.input_type === "color_picker"){
											controller_type = '3';
											attribute_option_labe = 'all';

										 	objController = {
												controller_type:'3',
												attribute_id:val.group_id.toString(),
												attribute_label:val.label_name,
												home_status:val.is_details_page.toString(),
											 	home_sort_order:val.details_sort_order.toString(),
											 	adv_status:val.is_listing_page.toString(),
											 	adv_sort_order:val.listing_sort_order.toString(),
											 	attribute_option_label:'all',
											 	items_color_list:retMainArr
											};
							
										}else if(val.input_type === "image_button"){

											controller_type = '4';
											attribute_option_labe = 'all';

										 	objController = {
												controller_type:'4',
												attribute_id:val.group_id.toString(),
												attribute_label:val.label_name,
												home_status:val.is_details_page.toString(),
											 	home_sort_order:val.details_sort_order.toString(),
											 	adv_status:val.is_listing_page.toString(),
											 	adv_sort_order:val.listing_sort_order.toString(),
											 	attribute_option_label:'all',
											 	items_value_list:retMainArr
											};
							
										}else if(val.input_type === "dropdown"){
											controller_type = '5';
											attribute_option_labe = 'all';

										 	objController = {
												controller_type:'5',
												attribute_id:val.group_id.toString(),
												attribute_label:val.label_name,
												home_status:val.is_details_page.toString(),
											 	home_sort_order:val.details_sort_order.toString(),
											 	adv_status:val.is_listing_page.toString(),
											 	adv_sort_order:val.listing_sort_order.toString(),
											 	attribute_option_label:'all',
											 	items_value_list:retMainArr
											};
								
										}else if(val.input_type === "radio_button"){
											controller_type = '6';

										 	objController = {
												controller_type:'6',
												attribute_id:val.group_id.toString(),
												attribute_label:val.label_name,
												home_status:val.is_details_page.toString(),
											 	home_sort_order:val.details_sort_order.toString(),
											 	adv_status:val.is_listing_page.toString(),
											 	adv_sort_order:val.listing_sort_order.toString(),
											 	attribute_option_label:'',
											 	items_sing_value_list:retMainArr
											};
						
										}else if(val.input_type === "select_range"){
											controller_type = '7';
										 	objController = {
												controller_type:'7',
												attribute_id:val.group_id.toString(),
												attribute_label:val.label_name,
												home_status:val.is_details_page.toString(),
											 	home_sort_order:val.details_sort_order.toString(),
											 	adv_status:val.is_listing_page.toString(),
											 	adv_sort_order:val.listing_sort_order.toString(),
											 	attribute_option_label:'',
											 	items_select_range_value:retMainArr
											};
						
										}else if(val.input_type === "input_range"){
											controller_type = '8';
											attribute_option_labe = 'all';
										 	objController = {
												controller_type:'8',
												attribute_id:val.group_id.toString(),
												attribute_label:val.label_name,
												home_status:val.is_details_page.toString(),
											 	home_sort_order:val.details_sort_order.toString(),
											 	adv_status:val.is_listing_page.toString(),
											 	adv_sort_order:val.listing_sort_order.toString(),
											 	attribute_option_label:'all',
											 	items_input_range_value:retMainArr
											};
									
										}else if(val.input_type === "text"){
											controller_type = '9';
										 	objController = {
												controller_type:'9',
												attribute_id:val.group_id.toString(),
												attribute_label:val.label_name,
												home_status:val.is_details_page.toString(),
											 	home_sort_order:val.details_sort_order.toString(),
											 	adv_status:val.is_listing_page.toString(),
											 	adv_sort_order:val.listing_sort_order.toString(),
											 	attribute_option_label:'',
											 	items_edittext_value:retMainArr
											};
								
										}else if(val.input_type === "button"){
											controller_type = '10';
										 	objController = {
												controller_type:'10',
												attribute_id:val.group_id.toString(),
												attribute_label:val.label_name,
												home_status:val.is_details_page.toString(),
											 	home_sort_order:val.details_sort_order.toString(),
											 	adv_status:val.is_listing_page.toString(),
											 	adv_sort_order:val.listing_sort_order.toString(),
											 	attribute_option_label:'',
											 	items_sing_value_list:retMainArr
											};
								
										}

										mainArr.push(objController);
									}

									
									counter -= 1;
									if (counter === 0){ 
										resolve(mainArr);
									}

								});
								
							})
						}],
						function (result, current) {
							if (counter === 0){ 
			
								var sortedArray = result[0].sort(function(a, b) {
								    return a.sort_order > b.sort_order ? 1 : a.sort_order < b.sort_order ? -1 : 0;
								});

								return_result(sortedArray);
											
							}
						});


					}


				});
	        }
	        else
	        { 
	        	return_result([]);
	        }

		});

	}
	catch(err)
	{ console.log("getJazeStoreProductAttributes",err); return_result(null); }	
};


methods.getCategoryAttributeValue = function(group_id,input_type,return_result){

	try
	{


		var arrValues = [];
		var obj_values = {};
		var controller_type;
		var objController = {};
		var value;
		var logo;

		if(input_type == 'color_picker'){
			obj_values = {
				id:'0',
				title:'all',
				color_code:''
			};
			arrValues.push(obj_values);
		}

		var singleObj = ['text','input_range','select_range'];
	 	var arrSameInputs = ['multiselect_image_button','multiselect_dropdown','image_button','dropdown'];

		if(arrSameInputs.includes(input_type) || input_type == 'color_picker' || input_type == 'radio_button' || input_type == 'text'){

	
			var quer = `SELECT * FROM jaze_jazestore_attributes_groups_meta 
				WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
				AND group_id IN (SELECT group_id FROM  jaze_jazestore_attributes_groups_meta  WHERE meta_key = 'status' AND meta_value = '1' )`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (vala) {
							return new Promise(function (resolve, reject) {		
				
								if(arrSameInputs.includes(input_type)){

									if(vala.value != null){
										value = vala.value;
										logo  = '';
									}else{
										value = '';
										logo = vala.logo;
									}

									if(input_type == 'multiselect_image_button'){

										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value:'',
											logo:STATIC_IMG_URL+vala.value
										};

									}else{

										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value:value,
											logo:logo
										};

									}

								}else if(input_type == 'color_picker'){

									obj_values = {
										id:vala.group_id.toString(),
										title:vala.key_name,
										color_code:vala.value
									};

								}else if(input_type == 'radio_button'){


									obj_values = {
										id:vala.group_id.toString(),
										title:vala.key_name,
										value: vala.value,
							
									};

								}else if(input_type == 'text'){
									
									obj_values = {
										text_hint_label :vala.key_name
									};
								}
								arrValues.push(obj_values);
								counter -= 1;
								if (counter === 0){ 
									resolve(arrValues);
								}
							})
						}],
						function (result, current) {});
						
					});

		  		}

			});

		
		}else if(input_type == 'input_range'){
	

			var quer = `SELECT * FROM jaze_jazestore_attributes_groups_meta 
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM  jaze_jazestore_attributes_groups_meta  WHERE meta_key = 'status' AND meta_value = '1' )`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var min_field_label;
					var max_field_label;
					var suffix_field_label;
					var separator_field_label;


					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (vala) {
							return new Promise(function (resolve, reject) {		
					
								if(vala.key_name == 'separator label'){
									separator_field_label = vala.value;
								}

								if(vala.key_name == 'min label'){
									min_field_label = vala.value;
								}

								if(vala.key_name == 'max label'){
									max_field_label = vala.value;
								}

								if(vala.key_name == 'suffix'){
									suffix_field_label = vala.value;
								}

								obj_values = {
									min_field_label:min_field_label,
									max_field_label:max_field_label,
									suffix_field_label:suffix_field_label,
									separator_field_label:separator_field_label
								};

								arrValues.push(obj_values);
								counter -= 1;
								if (counter === 0){ 
									resolve(arrValues);
								}
							})
						}],
						function (result, current) {});

					});
	
		  		}

			});
					
		}else if(input_type == 'select_range'){
	

			var quer = `SELECT * FROM jaze_jazestore_attributes_groups_meta 
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM  jaze_jazestore_attributes_groups_meta  WHERE meta_key = 'status' AND meta_value = '1' )`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var min_field_label;
					var min_field_value;
					var max_field_label;
					var max_field_value;
					var interval_value;
					var suffix_field_label;
					var separator_field_label;

					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (vala) {
							return new Promise(function (resolve, reject) {		
					
								if(vala.key_name == 'separator label'){
									separator_field_label = vala.value;
								}

								if(vala.key_name == 'min label'){
									min_field_label = vala.value;
								}


								if(vala.key_name == 'min value'){
									min_field_value = vala.value;
								}

								if(vala.key_name == 'max label'){
									max_field_label = vala.value;
								}


								if(vala.key_name == 'max value'){
									max_field_value = vala.value;
								}

								if(vala.key_name == 'interval value'){
									interval_value = vala.value;
								}


								if(vala.key_name == 'suffix'){
									suffix_field_label = vala.value;
								}

								obj_values = {
									id:'0',
									min_field_label:min_field_label,
									min_field_value:min_field_value,
									max_field_label:max_field_label,
									max_field_value:max_field_value,
									internal_value:interval_value,
									suffix_field_label:suffix_field_label,
									separator_field_label:separator_field_label
								};

								arrValues.push(obj_values);
								counter -= 1;
								if (counter === 0){ 
									resolve(arrValues);
								}
							})
						}],
						function (result, current) {});

					});

		  		}

			});
					
		}

		setTimeout(function(){ 

			if(singleObj.includes(input_type)){
				return_result(obj_values);
			}else{
				return_result(arrValues.sort(sortingArrayObject('title')));
			}
			
		}, 1000);

	}
	catch(err)
	{ console.log("getCategoryAttributeValue",err); return_result([]); }	
};



methods.getProductsFromCategoryAttributes = function(arrParams,return_result){

	try
	{	

		var arrProducts = [];
		var objProducts = {};

		var pagination_details  = {
			total_page   : '0',
        	current_page : '0',
        	next_page    : '0',
          	total_result : '0'
        };

		var queryIn = "SELECT * FROM jaze_products WHERE jaze_category_id=? AND country_id=? AND state_id=? AND city_id=? ORDER BY created_date DESC";
		DB.query(queryIn,[arrParams.category_id,arrParams.country_id,arrParams.state_id,arrParams.city_id], function(dataIn, error){

		
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		

				var counter = dataIn.length;		
				var jsonItems = Object.values(dataIn);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	

	
						methods.getJazeProductDetailsMeta(jsonItems.id, function(retProductMeta){		

							var accParams = {account_id:jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){										

								methods.getCurrency(retAccountDetails.currency_id, function(retCurrency){	

									methods.getProductBrand(jsonItems.jaze_brand_id, function(retBrand){	

										var sale_duration = '';
										if(Math.sign(retProductMeta.sale_duration) !='-1' && Math.sign(retProductMeta.sale_duration) !='0'){
											sale_duration = retProductMeta.sale_duration+' days left';
										}

										var product_image = "";
										if(retProductMeta.featured_image !=''){
											product_image = STATIC_IMG_PROD+jsonItems.account_id+'/products/'+jsonItems.id+'/thumb/'+retProductMeta.featured_image;
										}

										var discount = "";
										if(retProductMeta.sale_price!=''){
											discount = (retProductMeta.sale_price-retProductMeta.regular_price) / retProductMeta.regular_price * 100;
										}

										if(parseInt(Math.floor(discount)) === 0){
											discount = "";
										}else{
											discount = 	Math.floor(discount)+'%'
										}
							
										objProducts = {
											product_id       : jsonItems.id.toString(),
											product_name     : jsonItems.name,
											product_title    : jsonItems.title,
											brand_name       : retBrand,
											product_image    : product_image,
											regular_price    : retProductMeta.regular_price+' '+retCurrency,
											sale_price       : retProductMeta.sale_price+' '+retCurrency,
											percent_discount : discount,
											sale_duration    : sale_duration.toString(),
											result_flag      : '1'

										}

										arrProducts.push(objProducts);

										counter -= 1;
										if (counter === 0){ 
											resolve(arrProducts);
										}

									});
								});
							});
						});
					})
				}],
				function (result, current) {
					if (counter === 0){ 

						var newArray     = [];
						var total_page   = 0;
						var total_result = result[0].length.toString();
									
						if(parseInt(result[0].length) > 10){
		
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 10 ), (parseInt(arrParams.page) + 1) * 10);
							total_page = Math.ceil((parseInt(result[0].length) - 10 ) / 10);

						}else{
	
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 10 ), (parseInt(arrParams.page) + 1) * 10);
							total_page = 0;
						}

						var next_page = 0;
						if(parseInt(newArray.length) > 0){
							next_page  = parseInt(arrParams.page)+1;
						}


			            pagination_details.total_page   = total_page.toString();
			            pagination_details.current_page = arrParams.page.toString();
			            pagination_details.next_page    = next_page.toString();
			            pagination_details.total_result = total_result.toString();

						return_result(newArray,pagination_details);		

			
					}
				});

			}
			else
			{
				return_result([],pagination_details);
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getProductsFromCategoryAttributes",err); return_result([],pagination_details); 
	}	
};

//--------------------------------------- jazestore product attributes ---------------------------------------------------------//


//--------------------------------------- jazestore search product attributes -------------------------------------------------//


methods.searchProductAttribtues =  function(arrParams,return_result){
	try
	{			

		var arrCategory = [];
		var objController = {};

		var pagination_details  = {
			total_page   : '0',
        	current_page : '0',
        	next_page    : '0',
          	total_result : '0'
        };

		var counter = arrParams.attributes.length;		
		var jsonItems = Object.values(arrParams.attributes);

		promiseForeach.each(jsonItems,[function (param) {
			return new Promise(function (resolve, reject) {		

				var queryIn = '';
				if(parseInt(param.type) >= 1 &&  parseInt(param.type) <= 6 && parseInt(param.attribute_id) !== 92)
				{	
					var title = param.value.toLowerCase();
					if(param.value.toLowerCase() != ''){
						title = param.value.toLowerCase().split(",");
					}
	
					var arrTitle = title;
					var queryIn = "SELECT * FROM jaze_products_meta WHERE meta_key=?";
				}
				else if(parseInt(param.type) == 7)
				{
					var queryIn = "SELECT * FROM jaze_products_meta WHERE meta_key =? AND ( meta_value >='"+param.min_value+"' AND meta_value <='"+param.max_value+"')";
				}
				else if(parseInt(param.type) == 8)
				{
					var queryIn = "SELECT * FROM jaze_products_meta WHERE meta_key=?";
				}
				else if(parseInt(param.type) == 9)
				{
					var queryIn = "SELECT * FROM jaze_products_meta WHERE meta_key=? AND meta_value LIKE '%" + param.value + "%' GROUP BY meta_key LIMIT 1";
				}
				else if(parseInt(param.type) == 10)
				{
					var queryIn = "SELECT * FROM jaze_products_meta WHERE meta_key=? AND meta_value LIKE '%" + param.value + "%' ";
				}


				// console.log('query ',queryIn);
				// console.log('arrProductID ',result[0]);
				// console.log('attri_id ', param.attribute_id);

				DB.query(queryIn,['attribute_filter_'+param.attribute_id], function(data, error){

				
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			  			if(parseInt(param.type) >= 1 &&  parseInt(param.type) <= 6)
						{	
					
							data.forEach(function(value) {
								var arrValues = value.meta_value.toLocaleLowerCase().split(",");
								if(arrValues.some(item => arrTitle.includes(item))){
									arrCategory.push(value.product_id);
								}

						  	});
						}
						else if(parseInt(param.type) == 7)
						{
							arrCategory.push(data[0].product_id);
						}
						else if(parseInt(param.type) == 8)
						{
							var arrValues = data[0].meta_value.split(",");
					  		if( parseInt(param.min_value) >= parseInt(arrValues[0]) && parseInt(param.max_value) >= parseInt(arrValues[1]) ){
					  			arrCategory.push(data[0].product_id);
					  		}
						}
						else if(parseInt(param.type) == 9)
						{
							arrCategory.push(data[0].product_id);
						}
						else if(parseInt(param.type) == 10)
						{
							data.forEach(function(value) {
								arrCategory.push(value.product_id);
						  	});
						}

						counter -= 1;
						if (counter === 0){ 
							resolve(arrCategory);
						}
	
					}

				});

			})
		}],
		function (result, current) {
			if (counter === 0){ 

				methods.processProductsFromAttributesSearch(result[0],arrParams,function(retProducts){

					var newArray     = [];
					var total_page   = 0;
					var total_result = retProducts.length.toString();
								
					if(parseInt(retProducts.length) > 10){
	
						newArray   = retProducts.slice(Math.ceil( parseInt(arrParams.page) * 10 ), (parseInt(arrParams.page) + 1) * 10);
						total_page = Math.ceil((parseInt(retProducts.length) - 10 ) / 10);

					}else{

						newArray   = retProducts.slice(Math.ceil( parseInt(arrParams.page) * 10 ), (parseInt(arrParams.page) + 1) * 10);
						total_page = 0;
					}

					var next_page = 0;
					if(parseInt(newArray.length) > 0){
						next_page  = parseInt(arrParams.page)+1;
					}


		            pagination_details.total_page   = total_page.toString();
		            pagination_details.current_page = arrParams.page.toString();
		            pagination_details.next_page    = next_page.toString();
		            pagination_details.total_result = total_result.toString();

					return_result(newArray,pagination_details);	

					
				});
			}
		});


	}
	catch(err)
	{ console.log("searchProductAttribtues",err); return_result(null);}
};


methods.processProductsFromAttributesSearch =  function(arrProductIDs,arrParams,return_result){
	try
	{	
		var arrProducts = [];
		var objProducts = {};

		var queryIn = "SELECT * FROM jaze_products WHERE id IN(?) AND country_id=? AND state_id=? AND city_id=? ORDER BY created_date DESC";
		DB.query(queryIn,[arrProductIDs,arrParams.country_id,arrParams.state_id,arrParams.city_id], function(dataIn, error){

			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		

				var counter = dataIn.length;		
				var jsonItems = Object.values(dataIn);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	

	
						methods.getJazeProductDetailsMeta(jsonItems.id, function(retProductMeta){		

							var accParams = {account_id:jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){										

								methods.getCurrency(retAccountDetails.currency_id, function(retCurrency){	

									methods.getProductBrand(jsonItems.jaze_brand_id, function(retBrand){	

										var sale_duration = '';
										if(Math.sign(retProductMeta.sale_duration) !='-1' && Math.sign(retProductMeta.sale_duration) !='0'){
											sale_duration = retProductMeta.sale_duration+' days left';
										}

										var product_image = "";
										if(retProductMeta.featured_image !=''){
											product_image = STATIC_IMG_PROD+jsonItems.account_id+'/products/'+jsonItems.id+'/thumb/'+retProductMeta.featured_image;
										}

										var discount = "";
										if(retProductMeta.sale_price!=''){
											discount = (retProductMeta.sale_price-retProductMeta.regular_price) / retProductMeta.regular_price * 100;
										}

										if(parseInt(Math.floor(discount)) === 0){
											discount = "";
										}else{
											discount = 	Math.floor(discount)+'%'
										}
							
										objProducts = {
											product_id       : jsonItems.id.toString(),
											product_name     : jsonItems.name,
											product_title    : jsonItems.title,
											brand_name       : retBrand,
											product_image    : product_image,
											regular_price    : retProductMeta.regular_price+' '+retCurrency,
											sale_price       : retProductMeta.sale_price+' '+retCurrency,
											percent_discount : discount,
											sale_duration    : sale_duration.toString(),
											result_flag      : '1'

										}

										arrProducts.push(objProducts);

										counter -= 1;
										if (counter === 0){ 
											resolve(arrProducts);
										}

									});
								});
							});
						});
					})
				}],
				function (result, current) {
					if (counter === 0){ 
						return_result(result[0]);					
					}
				});

			}
			else
			{
				return_result(null);
			}
			
		});

	}
	catch(err)
	{ console.log("processProductsFromAttributesSearch",err); return_result(null);}
};


//--------------------------------------- jazestore search product attributes ------------------------------------------------//


//--------------------------------------- jazestore product brand ------------------------------------------------------------//


methods.getProductsBrandPerID =  function (arrParams,return_result){
	
	try
	{			

		var arrBrands = [];
		var objBrands = {};

		if(parseInt(arrParams.flag) === 3)
		{
			methods.getProductsFromBrandCategory(arrParams.brand_id,arrParams, function(retProducts,retPaginate){
				return_result(retProducts,retPaginate); 
			});
		}
		else if(parseInt(arrParams.flag) === 4)
		{

			var queryIn = "SELECT * FROM jaze_jazestore_category_brand_list  WHERE group_id IN (SELECT group_id FROM jaze_jazestore_category_brand_list WHERE meta_key = 'category_id' AND meta_value =? ) ";
			DB.query(queryIn,[arrParams.brand_id], function(dataIn, error){
			

				if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
				{		
					methods.convertMultiMetaArray(dataIn, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {	

								arrBrands.push(jsonItems.category_id);
								counter -= 1;
								if (counter === 0){ 
									resolve(arrBrands);							
								}

							})
						}],
						function (result, current) {
							if (counter === 0){ 
											
								methods.getCategoryPerBrand(result[0], function(retCategory){
									if(retCategory!=null)
									{
										methods.getProductsFromBrandCategory(retCategory,arrParams, function(retProducts,retPaginate){
											return_result(retProducts,retPaginate); 						
										});
									}
									else
									{
										return_result(null);
									}

								});

							}
						});
					});

				}
				else
				{
					return_result(null);
				}
				
			});

		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ 
		console.log("getProductsBrandPerID",err); return_result(null); 
	}

};

methods.getCategoryPerBrand =  function (arrParams,return_result){
	
	try
	{			

		var arrBrands = [];

		var queryIn = "SELECT * FROM jaze_jazestore_category_list WHERE group_id IN("+arrParams+") OR group_id IN (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key = 'parent_id' AND meta_value IN("+arrParams+"))";

		DB.query(queryIn,null, function(dataIn, error){


			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		
				methods.convertMultiMetaArray(dataIn, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							arrBrands.push(jsonItems.group_id);
							counter -= 1;
							if (counter === 0){ 
								resolve(arrBrands);
							}

						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
							console.log(result[0]);
							return_result(result[0]);

						}
					});
				});

			}
			else
			{
				return_result(null);
			}
			
		});

	}
	catch(err)
	{ 
		console.log("getCategoryPerBrand",err); return_result(null); 
	}

};

methods.getProductsFromBrandCategory = function(arrGroupID,arrParams,return_result){

	try
	{	

		var arrProducts = [];
		var objProducts = {};

		var pagination_details  = {
			total_page   : '0',
        	current_page : '0',
        	next_page    : '0',
          	total_result : '0'
        };

        var queryIn = "SELECT * FROM jaze_products WHERE jaze_category_id IN(?) ";
        if(parseInt(arrParams.flag) === 3)
        {
			queryIn = "SELECT * FROM jaze_products WHERE jaze_brand_id =? ";
        }

        if(parseInt(arrParams.country_id) !== 0)
        {
        	queryIn += " AND country_id ="+arrParams.country_id+" ";
        }

        if(parseInt(arrParams.state_id) !== 0)
        {
        	queryIn += " AND state_id ="+arrParams.state_id+" ";
        }

        if(parseInt(arrParams.city_id) !== 0)
        {
        	queryIn += " AND city_id ="+arrParams.city_id+" ";
        }
		
		queryIn += " ORDER BY created_date DESC";

		DB.query(queryIn,[arrGroupID], function(dataIn, error){

			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		

				var counter = dataIn.length;		
				var jsonItems = Object.values(dataIn);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	

						methods.getJazeProductDetailsMeta(jsonItems.id, function(retProductMeta){		

							// var accParams = {account_id:jsonItems.account_id};
							// HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){										

							// 	methods.getCurrency(retAccountDetails.currency_id, function(retCurrency){	

									methods.getProductBrand(jsonItems.jaze_brand_id, function(retBrand){	

										var sale_duration = '';
										if(Math.sign(retProductMeta.sale_duration) !='-1' && Math.sign(retProductMeta.sale_duration) !='0'){
											sale_duration = retProductMeta.sale_duration+' days left';
										}

										var product_image = "";
										if(retProductMeta.featured_image !=''){
											product_image = STATIC_IMG_PROD+jsonItems.account_id+'/products/'+jsonItems.id+'/thumb/'+retProductMeta.featured_image;
										}

										var discount = "";
										if(retProductMeta.sale_price!=''){
											discount = (retProductMeta.sale_price-retProductMeta.regular_price) / retProductMeta.regular_price * 100;
										}

										if(parseInt(Math.floor(discount)) === 0){
											discount = "";
										}else{
											discount = 	Math.floor(discount)+'%'
										}
							

										objProducts = {
											product_id       : jsonItems.id.toString(),
											product_name     : jsonItems.name,
											product_title    : jsonItems.title,
											brand_name       : retBrand,
											product_image    : product_image,
											regular_price    : (parseFloat(retProductMeta.regular_price).toFixed(2)).toString(),
											sale_price       : (parseFloat(retProductMeta.sale_price).toFixed(2)).toString(),
											percent_discount : discount,
											sale_duration    : sale_duration.toString(),
											created_date     : jsonItems.created_date,
											result_flag      : '1'
										}

										arrProducts.push(objProducts);

										counter -= 1;
										if (counter === 0){ 
											resolve(arrProducts);
										}

									});
							// 	});
							// });
						});
					})
				}],
				function (result, current) {
					if (counter === 0){ 
									
						var newArray     = [];
						var total_page   = 0;
						var total_result = result[0].length.toString();
									
						if(parseInt(result[0].length) > 10){
		
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 10 ), (parseInt(arrParams.page) + 1) * 10);
							total_page = Math.ceil((parseInt(result[0].length) - 10 ) / 10);

						}else{
	
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 10 ), (parseInt(arrParams.page) + 1) * 10);
							total_page = 0;
						}

						var next_page = 0;
						if(parseInt(newArray.length) > 0){
							next_page  = parseInt(arrParams.page)+1;
						}

			            pagination_details.total_page   = total_page.toString();
			            pagination_details.current_page = arrParams.page.toString();
			            pagination_details.next_page    = next_page.toString();
			            pagination_details.total_result = total_result.toString();

            		  	newArray.sort(function(a, b){ 
	                   	 	return new Date(b.created_date) - new Date(a.created_date); 
	                	});

						return_result(newArray,pagination_details);		
					}
				});

			}
			else
			{
				return_result([],pagination_details);
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getProductsFromBrandCategory",err); return_result([],pagination_details); 
	}	
};

//--------------------------------------- jazestore product brand ------------------------------------------------------------//


//--------------------------------------- jazestore cart list ----------------------------------------------------------------//

// methods.getCartList =  function (arrParams,return_result){
	
// 	try
// 	{	

// 		var main_obj = {};
// 		var prod_list_obj = {};
// 		var returnArr = [];

// 		var query = `SELECT * FROM jaze_carts_orders WHERE group_id IN (SELECT group_id FROM jaze_carts_orders WHERE meta_key = 'account_id' AND meta_value =?)
// 		AND group_id IN (SELECT group_id FROM jaze_carts_orders WHERE meta_key = 'status' AND meta_value ='0')`;

// 		DB.query(query,[arrParams.account_id], function(data, error){
		
// 			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
// 			{
	
// 				methods.convertMultiMetaArray(data, function(retConverted){	

// 					var counter = retConverted.length;		
// 					var jsonItems = Object.values(retConverted);

// 					promiseForeach.each(jsonItems,[function (jsonItems) {
// 						return new Promise(function (resolve, reject) {		
							
// 							var accParams = {account_id: jsonItems.profile_id}
// 							HelperGeneralJazenet.getAccountDetails(accParams, function(retAccount){	

// 								methods.getCartDetails(jsonItems.group_id, function(retItems){		

// 									var item_count = 0;
// 									var total = 0;
// 									retItems.forEach( function (value)
// 									{	
// 									    total += parseInt(value.total_price);
// 									    item_count += parseInt(value.qty);
// 									});
								
// 									main_obj = {
// 										cart_id      : jsonItems.group_id.toString(),
// 										account_id   : retAccount.account_id,
// 										name         : retAccount.name,
// 										logo         : retAccount.logo,
// 										item_count   : item_count.toString(),
// 										total_price  : total.toString(),
// 										created_date : jsonItems.date_created,
// 										item_list    : retItems
// 									};
						
// 									counter -= 1;
// 									returnArr.push(main_obj);
// 									if (counter === 0){ 
// 										resolve(returnArr);
// 									}

// 								});
// 							});
			
// 						})
// 					}],
// 					function (result, current) {
// 						if (counter === 0){ 
// 						  	result[0].sort(function(a, b){ 
// 		                   	 	return new Date(b.created_date) - new Date(a.created_date); 
// 		                	});
// 		                	return_result(result[0]);
				
// 						}
// 					});
// 				});

// 			}
// 			else
// 			{
// 				return_result(null);
// 			}

// 		});
	
// 	}
// 	catch(err)
// 	{ console.log("getCartList",err); return_result(null); }	
// };


// methods.getCartDetails =  function (group_id,return_result){
	
// 	try
// 	{	

// 		var main_obj      = {};
// 		var prod_list_obj = {};
// 		var returnArr     = [];
// 		var arrCount      = [];

// 		var total_price = 0;

// 		var query = `SELECT * FROM jaze_carts WHERE group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'order_id' AND meta_value =?)`;
// 		DB.query(query,[group_id], function(data, error){
		
// 			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
// 			{

// 				methods.convertMultiMetaArray(data, function(retConverted){	

// 					var counter = retConverted.length;		
// 					var jsonItems = Object.values(retConverted);

// 					promiseForeach.each(jsonItems,[function (jsonItems) {
// 						return new Promise(function (resolve, reject) {	

// 							var queryIn = "SELECT * FROM jaze_products WHERE id=?";
// 							DB.query(queryIn,[jsonItems.product_id], function(dataIn, error){
						
// 								if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
// 								{	

// 									methods.getProductBrand(dataIn[0].jaze_brand_id, function(retBrand){	

// 										var product_image = '';
// 										if(jsonItems.featured_image){
// 											product_image = STATIC_IMG_PROD+jsonItems.company_id+'/products/'+jsonItems.product_id+'/thumb/'+jsonItems.featured_image;
// 										}

// 										var current_date = dateFormat(now,'yyyy-mm-dd');
// 										var sale_duration = 0;
// 										if(parseInt(jsonItems.sale_onsale) === 1)
// 										{			

// 											var start = moment(current_date);
// 											var end =  moment(current_date);

// 											if(jsonItems.sale_price_start_from !='' && jsonItems.sale_price_start_to !='')
// 											{
// 												end = moment(jsonItems.sale_price_start_to);
// 											}
									
// 											sale_duration = end.diff(start, "days");
											
// 										}

// 										var product_price = jsonItems.regular_price;

// 										if(Math.sign(sale_duration) > 0){
// 											product_price = jsonItems.sale_price;
// 										}

// 										var total_price = parseInt(product_price) * parseInt(jsonItems.quantity);


						
// 										main_obj = {
// 											item_id		  : jsonItems.group_id.toString(),
// 											product_id    : jsonItems.product_id,
// 											product_name  : jsonItems.product_name,
// 											product_logo  : product_image,
// 											brand_id      : dataIn[0].jaze_brand_id.toString(),
// 											brand_name    : retBrand,
// 											qty           : jsonItems.quantity,
// 											price         : product_price,
// 											total_price   : total_price.toString(),
// 											created_date  : jsonItems.modified_datetime
										
// 										};
							
// 										counter -= 1;
// 										returnArr.push(main_obj);
// 										if (counter === 0){ 
// 											resolve(returnArr);
// 										}

// 									});
// 								}

// 							});
// 						})
// 					}],
// 					function (result, current) {
// 						if (counter === 0){ 

// 						   	result[0].sort(function(a, b){ 
// 		                   	 	return new Date(b.created_date) - new Date(a.created_date); 
// 		                	});

// 							return_result(result[0]);
				
// 						}
// 					});
// 				});

// 			}
// 			else
// 			{
// 				return_result([]);
// 			}

// 		});
	
// 	}
// 	catch(err)
// 	{ console.log("getCartDetails",err); return_result([]); }	
// };


methods.getCartList =  function (arrParams,return_result){
	
	try
	{	

		var main_obj = {};
		var prod_list_obj = {};

		var returnArr = [];
		var arrAccounts = [];

		var query = `SELECT * FROM jaze_carts WHERE group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'account_id' AND meta_value =?)
            AND group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'order_id' AND meta_value ='0')`;

		DB.query(query,[arrParams.account_id], function(data, error){
		
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
	
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
							
							arrAccounts.push(jsonItems.company_id);
							counter -= 1;
							if (counter === 0){ 
								resolve(arrAccounts);
							}

			
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							var filteredArray = removeDuplicates(result[0]);

							var counterIn = filteredArray.length;		
							var jsonItemsIn = Object.values(filteredArray);

							promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
								return new Promise(function (resolveIn, reject) {		
								

									var cartParams = {company_id:jsonItemsIn, account_id:arrParams.account_id };
									methods.getCartDetails(cartParams, function(retItems){		

										var accParams = {account_id: jsonItemsIn}
										HelperGeneralJazenet.getAccountDetails(accParams, function(retAccount){	

											var item_count = 0;
											var total = 0;
											retItems.forEach( function (value)
											{	
											    total += parseInt(value.total_price);
											    item_count += parseInt(value.qty);
											});
										
											main_obj = {

												account_id   : retAccount.account_id,
												name         : retAccount.name,
												logo         : retAccount.logo,
												item_count   : item_count.toString(),
												total_price  : total.toString(),
												created_date : jsonItems.date_created,
												item_list    : retItems
											};
								
											counterIn -= 1;
											returnArr.push(main_obj);
											if (counterIn === 0){ 
												resolveIn(returnArr);
											}

										});
									});

								})
							}],
							function (resultIn, current) {
								if (counterIn === 0){ 

								  	resultIn[0].sort(function(a, b){ 
				                   	 	return new Date(b.created_date) - new Date(a.created_date); 
				                	});

				                	return_result(resultIn[0]);
											
								}
							});
				
						}
					});
				});

			}
			else
			{
				return_result(null);
			}

		});
	
	}
	catch(err)
	{ console.log("getCartList",err); return_result(null); }	
};

methods.getCartDetails =  function (arrParams,return_result){
	
	try
	{	

		var main_obj      = {};
		var prod_list_obj = {};
		var returnArr     = [];
		var arrCount      = [];

		var total_price = 0;

		var query = `SELECT * FROM jaze_carts WHERE group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'account_id' AND meta_value =?)
				AND group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'company_id' AND meta_value =?)
				AND group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'order_id' AND meta_value ='0')`;
		DB.query(query,[arrParams.account_id,arrParams.company_id], function(data, error){
		
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var queryIn = "SELECT * FROM jaze_products WHERE id=?";
							DB.query(queryIn,[jsonItems.product_id], function(dataIn, error){
						
								if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
								{	

									methods.getProductBrand(dataIn[0].jaze_brand_id, function(retBrand){	

										var product_image = '';
										if(jsonItems.featured_image){
											product_image = STATIC_IMG_PROD+jsonItems.company_id+'/products/'+jsonItems.product_id+'/thumb/'+jsonItems.featured_image;
										}

										var current_date = dateFormat(now,'yyyy-mm-dd');
										var sale_duration = 0;
										if(parseInt(jsonItems.sale_onsale) === 1)
										{			

											var start = moment(current_date);
											var end =  moment(current_date);

											if(jsonItems.sale_price_start_from !='' && jsonItems.sale_price_start_to !='')
											{
												end = moment(jsonItems.sale_price_start_to);
											}
									
											sale_duration = end.diff(start, "days");
											
										}

										var product_price = jsonItems.regular_price;

										if(Math.sign(sale_duration) > 0){
											product_price = jsonItems.sale_price;
										}

										var total_price = parseInt(product_price) * parseInt(jsonItems.quantity);


						
										main_obj = {
											item_id		  : jsonItems.group_id.toString(),
											product_id    : jsonItems.product_id,
											product_name  : jsonItems.product_name,
											product_logo  : product_image,
											brand_id      : dataIn[0].jaze_brand_id.toString(),
											brand_name    : retBrand,
											qty           : jsonItems.quantity,
											price         : product_price,
											total_price   : total_price.toString(),
											created_date  : jsonItems.modified_datetime
										
										};
							
										counter -= 1;
										returnArr.push(main_obj);
										if (counter === 0){ 
											resolve(returnArr);
										}

									});
								}

							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 

						   	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.created_date) - new Date(a.created_date); 
		                	});

							return_result(result[0]);
				
						}
					});
				});

			}
			else
			{
				return_result([]);
			}

		});
	
	}
	catch(err)
	{ console.log("getCartDetails",err); return_result([]); }	
};

//--------------------------------------- jazestore cart list ----------------------------------------------------------------//

//--------------------------------------- jazestore add to cart --------------------------------------------------------------//

methods.addtoCart =  function (arrParams,return_result){
	
	try
	{	
		if(parseInt(arrParams.product_id) !== 0)
		{	
			
			methods.checkCartDetails(arrParams,function(returnData){


				if(returnData!=null)
				{
					methods.processInsertOrUpdateCart(arrParams,2,returnData,function(procStat){

						if(parseInt(procStat) === 1){
							return_result(1);
						}else{
							return_result(0);
						}
					});
				}
				else
				{
					//insert
					methods.processInsertOrUpdateCart(arrParams,1,[],function(procStat){

						if(parseInt(procStat) === 1){
							return_result(1);
						}else{
							return_result(0);
						}

					});

				}

			});

		}
		else
		{
			return_result(0);
		}
	
	}
	catch(err)
	{ console.log("addtoCart",err); return_result(0); }	
};


methods.processInsertOrUpdateCart =  function (arrParams,procType,arrDetails,return_result){
	
	try
	{	

		if(parseInt(procType) === 1 )
		{	

			//insert
			methods.getJazeProductDetailsForCart(arrParams,0,function(retProdDetails){


				var arrValues = Object.keys(retProdDetails).length;

				var queGroupMaxIn = "SELECT MAX(group_id) AS lastGrpId FROM jaze_carts";
				var lastGrpIdIn = 0;

				methods.getLastGroupId(queGroupMaxIn, function(queGrpResIn){

					lastGrpIdIn = queGrpResIn[0].lastGrpId;
					var plusOneGroupIDIn = lastGrpIdIn+1;

					var querInsertIn = `insert into jaze_carts (group_id, meta_key, meta_value) VALUES (?,?,?)`;
					var insertedVal = [];

					for (var keyIn in retProdDetails) 
				 	{
						DB.query(querInsertIn,[plusOneGroupIDIn,keyIn,retProdDetails[keyIn]], function(dataIn, error){
						
							insertedVal.push(dataIn);
							if(parseInt(insertedVal.length) ===  parseInt(arrValues)){
								return_result(1);
							}
						});

				  	};

				});
			});


		}
		else if(parseInt(procType) === 2)
		{

			//update
			var updateQue = `update jaze_carts SET meta_value =? WHERE meta_key =? AND group_id =?`;

			arrDetails.forEach(function(row) {

				var qty = parseInt(row.qty)+parseInt(arrParams.qty);

				var	arrUpdate = {
					quantity            : qty,
					modified_datetime   : newdateForm
				};
	
				for (var key in arrUpdate) 
			 	{	
					DB.query(updateQue, [arrUpdate[key],key,row.group_id], function(data, error){

 				 		if(data !== null){
							return_result(1);
				 	   	}else{
			 	   			return_result(0);
				 	   	}
					});
				
				}

		  	});

		
		}
	
	}
	catch(err)
	{ console.log("processInsertOrUpdateCart",err); return_result(0); }	
};


methods.checkCartDetails =  function (arrParams,return_result){
	
	try
	{			

		var arrGroupID = [];
		var cartObj = {};

		var queryIn = `SELECT * FROM jaze_carts WHERE group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'account_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'company_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'product_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'order_id' AND meta_value ='0')`;

		DB.query(queryIn,[arrParams.account_id,arrParams.company_id,arrParams.product_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConvertedIn){	

					var counterIn = retConvertedIn.length;		
					var jsonItemsIn = Object.values(retConvertedIn);

					promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
						return new Promise(function (resolveIn, reject) {		
							
							var current_date = dateFormat(now,'yyyy-mm-dd');
							var sale_duration = 0;
							if(parseInt(jsonItemsIn.sale_onsale) === 1)
							{			

								var start = moment(current_date);
								var end =  moment(current_date);

								if(jsonItemsIn.sale_price_start_from !='' && jsonItemsIn.sale_price_start_to !='')
								{
									end = moment(jsonItemsIn.sale_price_start_to);
								}
						
								sale_duration = end.diff(start, "days");
								
							}

							var product_price = jsonItemsIn.regular_price;

							if(Math.sign(sale_duration) > 0){
								product_price = jsonItemsIn.sale_price;
							}

							cartObj = {
								group_id : jsonItemsIn.group_id,
								qty      : parseInt(jsonItemsIn.quantity),
								price    : product_price
							}

							arrGroupID.push(cartObj);
							counterIn -= 1;
							if (counterIn === 0){ 
								resolveIn(arrGroupID);
							}
						})
					}],
					function (resultIn, current) {
						if (counterIn === 0){ 
							var update_type = 1;
							return_result(resultIn[0]);
						}
					});

				});

			}
			else
			{
				return_result(null);

			}

		});
	
	}
	catch(err)
	{ console.log("checkCartDetails",err); return_result(null); }	
};

// methods.checkCartDetails =  function (arrParams,return_result){
	
// 	try
// 	{			
// 		var arrOrderID = [];

// 		var query = `SELECT * FROM jaze_carts_orders WHERE group_id IN (SELECT group_id FROM jaze_carts_orders WHERE meta_key = 'status' AND meta_value ='0')
// 			AND group_id IN (SELECT group_id FROM jaze_carts_orders WHERE meta_key = 'account_id' AND meta_value =?)
// 			AND group_id IN (SELECT group_id FROM jaze_carts_orders WHERE meta_key = 'profile_id' AND meta_value =?)`;

// 		DB.query(query,[arrParams.account_id,arrParams.company_id], function(data, error){


// 			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
// 			{
	
// 				methods.convertMultiMetaArray(data, function(retConverted){	

// 					var counter = retConverted.length;		
// 					var jsonItems = Object.values(retConverted);

// 					promiseForeach.each(jsonItems,[function (jsonItems) {
// 						return new Promise(function (resolve, reject) {		
							
// 							arrOrderID.push(jsonItems.group_id);
// 							counter -= 1;
							
// 							if (counter === 0){ 
// 								resolve(arrOrderID);
// 							}
				
// 						})
// 					}],
// 					function (result, current) {
// 						if (counter === 0){ 

// 							var arrGroupID = [];
// 							var cartObj = {};

// 							var queryIn = `SELECT * FROM jaze_carts WHERE group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'product_id' AND meta_value =?)
// 										AND group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'order_id' AND meta_value IN(?) )`;

// 							DB.query(queryIn,[arrParams.product_id,result[0]], function(dataIn, error){
							
// 								if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
// 								{
									
// 									methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

// 										var counterIn = retConvertedIn.length;		
// 										var jsonItemsIn = Object.values(retConvertedIn);

// 										promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
// 											return new Promise(function (resolveIn, reject) {		
												
// 												var current_date = dateFormat(now,'yyyy-mm-dd');
// 												var sale_duration = 0;
// 												if(parseInt(jsonItemsIn.sale_onsale) === 1)
// 												{			

// 													var start = moment(current_date);
// 													var end =  moment(current_date);

// 													if(jsonItemsIn.sale_price_start_from !='' && jsonItemsIn.sale_price_start_to !='')
// 													{
// 														end = moment(jsonItemsIn.sale_price_start_to);
// 													}
											
// 													sale_duration = end.diff(start, "days");
													
// 												}

// 												var product_price = jsonItemsIn.regular_price;

// 												if(Math.sign(sale_duration) > 0){
// 													product_price = jsonItemsIn.sale_price;
// 												}

// 												cartObj = {
// 													order_id : jsonItemsIn.order_id,
// 													group_id : jsonItemsIn.group_id,
// 													qty      : parseInt(jsonItemsIn.quantity),
// 													price    : product_price
// 												}

// 												arrGroupID.push(cartObj);
// 												counterIn -= 1;
// 												if (counterIn === 0){ 
// 													resolveIn(arrGroupID);
// 												}
// 											})
// 										}],
// 										function (resultIn, current) {
// 											if (counterIn === 0){ 
// 												var update_type = 1;
// 												return_result(resultIn[0],update_type);
												
// 											}
// 										});

// 									});

// 								}
// 								else
// 								{

// 									var update_type = 0;
// 									cartObj = {
// 										order_id : data[0].group_id,
// 										group_id : '',
// 										qty      : '',
// 										price    : ''
// 									}

// 									return_result(cartObj,update_type);


// 								}

// 							});
// 						}
// 					});
// 				});

// 			}
// 			else
// 			{
// 				return_result(null);
// 			}

// 		});
	
// 	}
// 	catch(err)
// 	{ console.log("checkCartDetails",err); return_result(null); }	
// };

methods.getJazeProductDetailsForCart = function(arrParams,group_id,return_result){

	try
	{	

		var arrProducts = [];
		var objProducts	= {};

		var query = "SELECT * FROM jaze_products WHERE id=?";
		DB.query(query,[arrParams.product_id], function(data, error){
		
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{		
				objProducts = {

					addons          	: '',
					addons_meta         : '',
					addons_total        : '',
					price_purchased     : '',
					account_id          : arrParams.account_id,
					product_id          : data[0].id,
					quantity            : arrParams.qty,
					order_id            : group_id,
					content             : data[0].content,
					jaze_brand_id       : data[0].jaze_brand_id,
					jaze_category_id    : data[0].jaze_category_id,
					company_category_id : data[0].company_category_id,
					product_parent_id   : data[0].parent_id,
					company_id          : data[0].account_id,
					product_name        : data[0].title,
					category_id         : data[0].category_id,
					created_datetime    : newdateForm,
					modified_datetime   : newdateForm

				};


				var arrProductsMeta = [];
				var queryIn = "SELECT * FROM jaze_products_meta WHERE product_id=?";
				DB.query(queryIn,[data[0].id], function(dataIn, error){

					if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
					{

						var counter = dataIn.length;		
						var jsonItems = Object.values(dataIn);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
								
								Object.assign(objProducts, {[jsonItems.meta_key] : jsonItems.meta_value});
								counter -= 1;
								if (counter === 0){ 
									resolve(objProducts);
								}
							})
						}],
						function (result, current) {
							if (counter === 0){ 
								return_result(result[0]);	
							}
						});

					}
					else
					{
						return_result(null);
					}

				});

			}
			else
			{
				return_result(null);
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getJazeProductDetailsForCart",err); return_result(null); 
	}	
};


//--------------------------------------- jazestore add to cart --------------------------------------------------------------//


//--------------------------------------- jazestore update cart --------------------------------------------------------------//

methods.UpdateCart =  function (arrParams,return_result){
	
	try
	{			

		DB.query(`SELECT * FROM jaze_carts WHERE group_id =?`,[arrParams.item_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	

					var	arrUpdate = {
						quantity            : arrParams.qty,
						modified_datetime   : newdateForm
					};
					var updateQue = `update jaze_carts SET meta_value =? WHERE meta_key =? AND group_id =?`;
					for (var key in arrUpdate) 
				 	{	
					 
						DB.query(updateQue, [arrUpdate[key],key,retConverted[0].group_id], function(data, error){

	 				 		if(data !== null){
								return_result(1);
					 	   	}else{
				 	   			return_result(0);
					 	   	}
						});
					
					}
			
				});
			}
			else
			{
				return_result(2);
			}

		});
		
	}
	catch(err)
	{ console.log("UpdateCart",err); return_result(0); }	
};


//--------------------------------------- jazestore update cart --------------------------------------------------------------//


//--------------------------------------- jazestore remove to cart -----------------------------------------------------------//

// methods.removeToCart =  function (arrParams,return_result){
	
// 	try
// 	{			
// 		var arrChildID = [];
// 		if(parseInt(arrParams.flag) === 1)	// remove cart
// 		{	

// 			DB.query(`SELECT * FROM jaze_carts_orders WHERE group_id =?`,[arrParams.cart_id], function(data, error){

// 				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
// 				{

// 					var queDel = `DELETE FROM jaze_carts_orders WHERE group_id =? `;
// 					DB.query(queDel,[data[0].group_id], function(dataP, error){

// 					 	if(parseInt(dataP.affectedRows) > 0)
// 					 	{
	 					
// 			 				DB.query(`SELECT  * FROM jaze_carts WHERE group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'order_id' AND meta_value =?)`,[data[0].group_id], function(dataC, error){

// 								if(typeof dataC !== 'undefined' && dataC !== null && parseInt(dataC.length) > 0)
// 								{
// 									methods.convertMultiMetaArray(dataC, function(retConverted){	

// 										var counter = retConverted.length;		
// 										var jsonItems = Object.values(retConverted);

// 										promiseForeach.each(jsonItems,[function (jsonItems) {
// 											return new Promise(function (resolve, reject) {		
												
// 												arrChildID.push(jsonItems.group_id);
// 												counter -= 1;
// 												if (counter === 0){ 
// 													resolve(arrChildID);
// 												}
// 											})
// 										}],
// 										function (result, current) {
// 											if (counter === 0){ 
												
// 												var queDelC = `DELETE FROM jaze_carts WHERE group_id IN(?) `;
// 												DB.query(queDelC,[result[0]], function(dataDel, error){
// 												 	if(parseInt(dataDel.affectedRows) > 0)
// 						 							{
// 														return_result(1);
// 					 								}
// 					 								else
// 					 								{
// 														return_result(0);
// 					 								}
// 												});
			
// 											}
// 										});

// 									});
// 								}
// 								else
// 								{
// 									return_result(1);
// 								}

// 							});


// 				 	   	}else{
// 		   					return_result(0);
// 				 	   	}

// 					});

// 				}
// 				else
// 				{
// 					return_result(2);
// 				}
// 			});

// 		}
// 		else // remove item
// 		{

// 			DB.query("SELECT  * FROM jaze_carts WHERE group_id IN("+arrParams.item_id+")",null, function(dataC, error){

// 				if(typeof dataC !== 'undefined' && dataC !== null && parseInt(dataC.length) > 0)
// 				{
// 					methods.convertMultiMetaArray(dataC, function(retConverted){

// 						var order_id = 	retConverted[0].order_id;

// 						var counter = retConverted.length;		
// 						var jsonItems = Object.values(retConverted);

// 						promiseForeach.each(jsonItems,[function (jsonItems) {
// 							return new Promise(function (resolve, reject) {		
								
// 								arrChildID.push(jsonItems.group_id);
// 								counter -= 1;
// 								if (counter === 0){ 
// 									resolve(arrChildID);
// 								}
// 							})
// 						}],
// 						function (result, current) {
// 							if (counter === 0){ 
									
// 								var queDelC = "DELETE FROM jaze_carts WHERE group_id IN("+arrParams.item_id+") ";
// 								DB.query(queDelC,[result[0]], function(dataDel, error){
// 								 	if(parseInt(dataDel.affectedRows) > 0)
// 		 							{
// 										DB.query("SELECT  * FROM jaze_carts WHERE group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'order_id' AND meta_value =?)",[order_id], function(dataCheck, error){

// 											if(typeof dataCheck !== 'undefined' && dataCheck !== null && parseInt(dataCheck.length) > 0)
// 											{
// 												return_result(1);
// 											}
// 											else
// 											{
// 												var queDelMain = "DELETE FROM jaze_carts_orders WHERE group_id=?";
// 												DB.query(queDelMain,[order_id], function(datacheckDel, error){	
// 												 	if(parseInt(datacheckDel.affectedRows) > 0)
// 		 											{
// 														return_result(1);
// 	 												}
// 	 												else
// 	 												{
// 	 													return_result(0);
// 	 												}

// 												});

// 											}

// 										});

// 	 								}
// 	 								else
// 	 								{
// 										return_result(0);
// 	 								}
// 								});
					
// 							}
// 						});

// 					});
// 				}
// 				else
// 				{
// 					return_result(1);
// 				}

// 			});

// 		}
	
// 	}
// 	catch(err)
// 	{ console.log("removeToCart",err); return_result(0); }	
// };

methods.removeToCart =  function (arrParams,return_result){
	
	try
	{			
		var arrGroupID = [];
		if(parseInt(arrParams.flag) === 1)	
		{	
			// remove cart
			var queryIn = `SELECT * FROM jaze_carts WHERE group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'account_id' AND meta_value =?)
				AND group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'order_id' AND meta_value ='0')`;

			DB.query(queryIn,[arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConvertedIn){	

						var counterIn = retConvertedIn.length;		
						var jsonItemsIn = Object.values(retConvertedIn);

						promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
							return new Promise(function (resolveIn, reject) {		
								
								arrGroupID.push(jsonItemsIn.group_id);
								counterIn -= 1;
								if (counterIn === 0){ 
									resolveIn(arrGroupID);
								}
							})
						}],
						function (resultIn, current) {
							if (counterIn === 0){ 
							
								var queDelC = "DELETE FROM jaze_carts WHERE group_id IN("+resultIn[0]+") ";
								DB.query(queDelC,null, function(dataDel, error){
								 	if(parseInt(dataDel.affectedRows) > 0)
		 							{
										return_result(1);
	 								}
	 								else
	 								{
										return_result(0);
	 								}
								});

							}
						});

					});

				}
				else
				{
					return_result(2);

				}

			});

		}
		else if(parseInt(arrParams.flag) === 2)
		{
			// remove items
			DB.query("SELECT  * FROM jaze_carts WHERE group_id IN("+arrParams.item_id+")",null, function(dataC, error){
			
				if(typeof dataC !== 'undefined' && dataC !== null && parseInt(dataC.length) > 0)
				{
					methods.convertMultiMetaArray(dataC, function(retConverted){

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
								
								arrGroupID.push(jsonItems.group_id);
								counter -= 1;
								if (counter === 0){ 
									resolve(arrGroupID);
								}
							})
						}],
						function (result, current) {
							if (counter === 0){ 
			
								var queDelC = "DELETE FROM jaze_carts WHERE group_id IN("+result[0]+") ";
								DB.query(queDelC,null, function(dataDel, error){
								 	if(parseInt(dataDel.affectedRows) > 0)
		 							{
										return_result(1);
	 								}
	 								else
	 								{
										return_result(0);
	 								}
								});
					
							}
						});

					});
				}
				else
				{
					return_result(2);
				}

			});

		}
	
	}
	catch(err)
	{ console.log("removeToCart",err); return_result(0); }	
};


//--------------------------------------- jazestore remove to cart -----------------------------------------------------------//


//--------------------------------------- jazestore checkout information -----------------------------------------------------//

methods.getCheckoutInformation =  function (arrParams,return_result){
	
	try
	{			

		methods.getUserShippingInfo(arrParams,function(retShip){

			methods.getUserPaymentInfo(arrParams,function(retPay){

				var ret_obj = {

					delivery_info : retShip,
					payment_info  : retPay,

				}

				return_result(ret_obj);

			});

		});

	}
	catch(err)
	{ console.log("getCheckoutInformation",err); return_result(null); }	
};

methods.getUserShippingInfo =  function (arrParams,return_result){
	
	try
	{			
		var arrResult = [];
		var arrObj    = {};
		var query = `SELECT * FROM jaze_user_billing_address WHERE group_id IN (SELECT group_id FROM jaze_user_billing_address WHERE meta_key = 'account_id' AND meta_value =?)
		AND group_id IN (SELECT group_id FROM jaze_user_billing_address WHERE meta_key = 'is_default' AND meta_value = '1')`;

		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){

					methods.__getCountryDetails(retConverted[0].country_id, function(country){

						methods.__getStateDetails(retConverted[0].state_id, function(state){

							methods.__getCityDetails(retConverted[0].city_id, function(city){

								methods.__getAreaDetails(retConverted[0].area_id, function(area){

									methods.__getMobileCodeID(retConverted[0].mobile_country_code, function(mobcode){

										methods.__getMobileCodeID(retConverted[0].landline_country_code, function(lancode){

											arrObj = {
												id               : retConverted[0].group_id.toString(),
												first_name       : retConverted[0].first_name,
												last_name        : retConverted[0].last_name,
												mobile_code      : retConverted[0].mobile_country_code,
												mobile_code_id   : mobcode,
												mobile_no        : retConverted[0].mobile_no,
												landline_code    : retConverted[0].landline_country_code,
												landline_code_id : lancode,
												landline_no      : retConverted[0].landline_no,
												building_no      : retConverted[0].building_no,
												building_name    : retConverted[0].building_name,
												street_no        : retConverted[0].street_no,
												street_name      : retConverted[0].street_name,
												country_id       : retConverted[0].country_id,
												country_name     : country,
												state_id         : retConverted[0].state_id,
												state_name       : state,
												city_id          : retConverted[0].city_id,
												city_name        : city,
												area_id          : retConverted[0].area_id,
												area_name        : area,
												shipping_note    : retConverted[0].shipping_note,
												landmark         : retConverted[0].landmark,
												location_type    : retConverted[0].location_type,
												is_default       : retConverted[0].is_default
									
											};

											return_result(arrObj);

										});
									});
								});

							});

						});

					});

				});

			}
			else
			{
				
				var query = `SELECT * FROM jaze_user_billing_address WHERE group_id IN (SELECT group_id FROM jaze_user_billing_address WHERE meta_key = 'account_id' AND meta_value =?)`;

				DB.query(query,[arrParams.account_id], function(data, error){

		
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{

						methods.convertMultiMetaArray(data, function(retConverted){

							methods.__getCountryDetails(retConverted[0].country_id, function(country){

								methods.__getStateDetails(retConverted[0].state_id, function(state){

									methods.__getCityDetails(retConverted[0].city_id, function(city){

										methods.__getAreaDetails(retConverted[0].area_id, function(area){

											methods.__getMobileCodeID(retConverted[0].mobile_country_code, function(mobcode){

												methods.__getMobileCodeID(retConverted[0].landline_country_code, function(lancode){

													arrObj = {
														id               : retConverted[0].group_id.toString(),
														first_name       : retConverted[0].first_name,
														last_name        : retConverted[0].last_name,
														mobile_code      : retConverted[0].mobile_country_code,
														mobile_code_id   : mobcode,
														mobile_no        : retConverted[0].mobile_no,
														landline_code    : retConverted[0].landline_country_code,
														landline_code_id : lancode,
														landline_no      : retConverted[0].landline_no,
														building_no      : retConverted[0].building_no,
														building_name    : retConverted[0].building_name,
														street_no        : retConverted[0].street_no,
														street_name      : retConverted[0].street_name,
														country_id       : retConverted[0].country_id,
														country_name     : country,
														state_id         : retConverted[0].state_id,
														state_name       : state,
														city_id          : retConverted[0].city_id,
														city_name        : city,
														area_id          : retConverted[0].area_id,
														area_name        : area,
														shipping_note    : retConverted[0].shipping_note,
														landmark         : retConverted[0].landmark,
														location_type    : retConverted[0].location_type,
														is_default       : retConverted[0].is_default,
											
													};

													return_result(arrObj);

												});
											});
										});

									});

								});

							});

						});

					}
					else
					{
						return_result({});
					}

				});

			}


		});
		
	
	}
	catch(err)
	{ console.log("getUserShippingInfo",err); return_result({}); }	
};

methods.getUserPaymentInfo =  function (arrParams,return_result){
	
	try
	{			


		var arrObj    = {};
		var arrRes    = [];
		var obj_return = {

			credit_debit_cart : '',
			jazepay_balance   : '',
			cash_on_delivery  : ''
		};

		var query = `SELECT * FROM jaze_user_credit_card WHERE group_id IN (SELECT group_id FROM jaze_user_credit_card WHERE meta_key = 'account_id' AND meta_value = ?)
		AND group_id IN (SELECT group_id FROM jaze_user_credit_card WHERE meta_key = 'is_default' AND meta_value = '1')
		AND group_id IN (SELECT group_id FROM jaze_user_credit_card WHERE meta_key = 'status' AND meta_value = '1')`;

		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){

					methods.getUserJazePay(arrParams, function(retjazepay){

						arrObj = {
							id          : retConverted[0].group_id.toString(),
							card_name   : retConverted[0].card_name,
							bank_name   : retConverted[0].bank_name,
							card_no     : retConverted[0].card_no,
							exp_month   : retConverted[0].exp_month,
							exp_year    : retConverted[0].exp_year,
						
		
						}

						obj_return = {

							credit_debit_cart : arrObj,
							jazepay_balance   : retjazepay,
					
						}

						arrRes.push(obj_return)

						return_result(obj_return);

					});
				});

			}
			else
			{
				
				var query = `SELECT * FROM jaze_user_credit_card WHERE group_id IN (SELECT group_id FROM jaze_user_credit_card WHERE meta_key = 'account_id' AND meta_value =?)
					AND group_id IN (SELECT group_id FROM jaze_user_credit_card WHERE meta_key = 'status' AND meta_value = '1')`;

				DB.query(query,[arrParams.account_id], function(data, error){

		
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{

						methods.convertMultiMetaArray(data, function(retConverted){

							methods.getUserJazePay(arrParams, function(retjazepay){

								arrObj = {
									card_name   : retConverted[0].card_name,
									bank_name   : retConverted[0].bank_name,
									card_no     : retConverted[0].card_no,
									exp_month   : retConverted[0].exp_month,
									exp_year    : retConverted[0].exp_year,
								
				
								}

								obj_return = {

									credit_debit_cart : arrObj,
									jazepay_balance   : retjazepay,
				
								}

								arrRes.push(obj_return)

								return_result(obj_return);

							});
							
						});
					}
					else
					{
						return_result(obj_return);
					}

				});

			}
			

		});
		
	
	}
	catch(err)
	{ console.log("getUserPaymentInfo",err); return_result(obj_return); }	
};

methods.getUserJazePay =  function (arrParams,return_result){
	
	try
	{				
		var query = `SELECT * FROM jaze_jazepay_accounts WHERE group_id IN (SELECT group_id FROM jaze_jazepay_accounts WHERE meta_key = 'account_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_jazepay_accounts WHERE meta_key = 'status' AND meta_value = '1')`;

		DB.query(query,[arrParams.account_id], function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){

					return_result(retConverted[0].current_balance);
					
				});
			}
			else
			{
				return_result('');
			}

		});

	}
	catch(err)
	{ console.log("getUserJazePay",err); return_result(''); }	
};

//--------------------------------------- jazestore checkout information -----------------------------------------------------//


//--------------------------------------- jazestore process checkout  --------------------------------------------------------//

methods.processCheckout =  function (arrParams,return_result){
	
	try
	{				

		var arrCompanyID = [];
		var objvalues  = {};
		var total_price   = 0;

		var query = `SELECT * FROM jaze_carts WHERE group_id IN (?)
			AND group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'order_id' AND meta_value ='0' )`;

		DB.query(query,[arrParams.item_id], function(data, error){
		
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
			
							arrCompanyID.push(jsonItems.company_id);
							counter -= 1;

							if (counter === 0){ 
								resolve(arrCompanyID);
							}					
		
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
							var arrRes = [];
							var filteredCompanyID = removeDuplicates(result[0]);

							var counterIn = filteredCompanyID.length;		
							var jsonItemsIn = Object.values(filteredCompanyID);

							promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
								return new Promise(function (resolveIn, reject) {		
										
									methods.getAllCompanyOrders(jsonItemsIn,arrParams,function(processed){
									
										counterIn -= 1;
										arrRes.push(processed);
										if (counterIn === 0){ 
											resolveIn(arrRes);
										}	

									});													
				
								})
							}],
							function (resultIn, current) {
								if (counterIn === 0){ 
									//return_result(resultIn[0]);
							
									methods.processJazeCartOrder(resultIn[0],arrParams,function(retstat){
										return_result(retstat);

									});
							
								}
							});

					
						}
					});

				});

			}
			else
			{
				return_result(2);
			}

		});

	}
	catch(err)
	{ console.log("processCheckout",err); return_result(0); }	
};

methods.getAllCompanyOrders =  function (company_id,arrParams,return_result){
	
	try
	{	
		var obj = {};
		var arr = [];
		var arrGroupID = [];
		var total_price = 0;

		var queryIn = `SELECT * FROM jaze_carts WHERE group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'company_id' AND meta_value =? )
			AND group_id IN(?)
			AND group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'account_id' AND meta_value =? )
			AND group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'order_id' AND meta_value ='0' )`;

	
		DB.query(queryIn,[company_id,arrParams.item_id,arrParams.account_id], function(dataIn, error){
		
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{
				methods.convertMultiMetaArray(dataIn, function(retConverted){		

					var counterIn = retConverted.length;	
					var jsonItemsIn = Object.values(retConverted);

					promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
						return new Promise(function (resolveIn, reject) {		

							methods.checkOrderPrice(jsonItemsIn,function(totPrice){

								total_price += totPrice;
								arrGroupID.push(jsonItemsIn.group_id);
								counterIn -= 1;
		
								if (counterIn === 0){ 

									obj = {
										group_id   : arrGroupID,
										company_id : jsonItemsIn.company_id,
										account_id : jsonItemsIn.account_id,
										total      : total_price.toString()
									}
									resolveIn(obj);
								}	
							});				
		
						})
					}],
					function (resultIn, current) {
						if (counterIn === 0){ 
		
							return_result(resultIn[0])
						}
					});

				});
			}

		});



	}
	catch(err)
	{ console.log("getAllCompanyOrders",err); return_result(null); }	
};


methods.checkOrderPrice =  function (jsonItems,return_result){
	
	try
	{				
		var product_price = 0;
		var total_price   = 0;

		var current_date = dateFormat(now,'yyyy-mm-dd');
		var sale_duration = 0;
		if(parseInt(jsonItems.sale_onsale) === 1)
		{			

			var start = moment(current_date);
			var end =  moment(current_date);

			if(jsonItems.sale_price_start_from !='' && jsonItems.sale_price_start_to !='')
			{
				end = moment(jsonItems.sale_price_start_to);
			}
	
			sale_duration = end.diff(start, "days");
			
		}

		product_price = jsonItems.regular_price;

		if(Math.sign(sale_duration) > 0){
			product_price = jsonItems.sale_price;
		}

		total_price = parseInt(product_price) * parseInt(jsonItems.quantity);

		return_result(total_price);


	}
	catch(err)
	{ console.log("checkOrderPrice",err); return_result(0); }	
};

methods.processJazeCartOrder =  function(arrObjects,arrParams,return_result){
	try
	{	

		var counter   = arrObjects.length;		
		var jsonItems = Object.values(arrObjects);

		var ctr = 1;
		for (let i = 0; i < counter; i++) {

			setTimeout( function timer(){

				methods.getCardType(arrParams,function(card_type){	

					var objInsert = {
						account_id      : jsonItems[i].account_id,
						profile_id      : jsonItems[i].company_id,
						card_type       : card_type,
						total           : jsonItems[i].total,
						delivery_charge : '25.25',
						delivery_id     : arrParams.delivery_id,
						payment_id      : arrParams.payment_id,
						payment_type    : arrParams.payment_type,
						date_delivered  : newdateForm,
						date_created    : newdateForm,
						status          : '1'
					};

					methods.insertJazeCartOrders(objInsert,arrObjects,function(retInsert){			

						if(parseInt(ctr) == parseInt(counter)){
							if(parseInt(retInsert) === 1){
								return_result(1);
							}else{
								return_result(0);
							}
							
						}

						ctr++;
					});
				});

			}, i*3000 );

		}

	}
	catch(err)
	{ console.log("processJazeCartOrder",err); return_result(0); }	
};

methods.insertJazeCartOrders =  function(arrInsert,arrReturn,return_result){
	try
	{	
		var queGroupMax = `SELECT MAX(group_id) AS lastGrpId FROM jaze_carts_orders`;
		var querInsert  = `insert into jaze_carts_orders (group_id, meta_key, meta_value) VALUES (?,?,?)`;
		var updateQue   = `update jaze_carts SET meta_value =? WHERE meta_key =? AND group_id =?`;

		var lastGrpId;

		methods.getLastGroupId(queGroupMax, function(queGrpRes){

	  		lastGrpId = queGrpRes[0].lastGrpId;

			var newgrp_id = lastGrpId+1

			// var arrInsert = {
			// 	account_id      : arrObjects.account_id,
			// 	profile_id      : arrObjects.profile_id,
			// 	card_type       : arrObjects.card_type,
			// 	total           : arrObjects.total,
			// 	delivery_charge : arrObjects.delivery_charge,
			// 	delivery_id     : arrObjects.delivery_id,
			// 	payment_id      : arrObjects.payment_id,
			// 	payment_type    : arrObjects.payment_type,
			// 	date_delivered  : arrObjects.date_delivered,
			// 	date_created    : arrObjects.date_created,
			// 	status          : arrObjects.status
			// };

			var	arrUpdate = {
				order_id            : newgrp_id,
				modified_datetime   : newdateForm
			};

			var objlenght_i = Object.keys(arrInsert).length;
			var objlenght_u = Object.keys(arrUpdate).length;

			var num_rows_insert = 0;
			var num_rows_update = 0;

		 	for (var key_i in arrInsert) 
		 	{
				DB.query(querInsert,[newgrp_id,key_i,arrInsert[key_i]], function(data_i, error){
					num_rows_insert += data_i.affectedRows;
					if(parseInt(num_rows_insert) === objlenght_i){

						arrReturn.forEach(function(row) {

							if(row.company_id == arrInsert.profile_id){

								row.group_id.forEach(function(row_in) {
									//console.log(row_in+' '+newgrp_id);
									for (var key_u in arrUpdate) 
								 	{	
										DB.query(updateQue, [arrUpdate[key_u],key_u,row_in], function(data_u, error){
											num_rows_update += data_u.affectedRows;
											if(parseInt(num_rows_update) === objlenght_u){
												return_result(1);
											}
					 				 
										});
									}
								});
							}
					
						});

					}
				});
		  	};

	  	});

	}
	catch(err)
	{ console.log("insertJazeCartOrders",err); return_result(0); }	
};


methods.getCardType =  function (arrParams,return_result){
	
	try
	{	

		if(parseInt(arrParams.payment_type) !== 0)
		{

			var queryC = `SELECT * FROM jaze_user_credit_card WHERE group_id IN (SELECT group_id FROM jaze_user_credit_card WHERE meta_key = 'account_id' AND meta_value =?)
			AND group_id =?`;
	
			DB.query(queryC,[arrParams.account_id,arrParams.payment_id], function(dataC, error){
			
				if(typeof dataC !== 'undefined' && dataC !== null && parseInt(dataC.length) > 0)
				{
					return_result(2);
				}
				else
				{
					var queryB = `SELECT * FROM jaze_user_credit_card WHERE group_id IN (SELECT group_id FROM jaze_user_credit_card WHERE meta_key = 'account_id' AND meta_value =?)
					AND group_id =?`;

					DB.query(queryB,[arrParams.account_id,arrParams.payment_id], function(dataB, error){
					
						if(typeof dataB !== 'undefined' && dataB !== null && parseInt(dataB.length) > 0)
						{
							return_result(1);
						}
						else
						{
							return_result(0);
						}

					});
				}

			});
			
		}
		else
		{
			return_result(0);
		}

	}
	catch(err)
	{ console.log("getCardType",err); return_result(0); }	
};

//--------------------------------------- jazestore process checkout  --------------------------------------------------------//


//--------------------------------------- jazestore order list ---------------------------------------------------------------//

methods.getOrderList =  function (arrParams,return_result){
	
	try
	{	

		var main_obj = {};
		var returnArr = [];


		var query = `SELECT * FROM jaze_carts_orders WHERE group_id IN (SELECT group_id FROM jaze_carts_orders WHERE meta_key = 'account_id' AND meta_value =?)
            AND group_id IN (SELECT group_id FROM jaze_carts_orders WHERE meta_key = 'status' AND meta_value ='1')`;

		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
	
				methods.convertMultiMetaArray(data, function(retConverted){	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
							
							var orderParams = {account_id:arrParams.account_id, order_id:jsonItems.group_id};
							methods.getOrderDetails(orderParams,function(details){

								methods.getUserShippingInfoForOrders(jsonItems.delivery_id,function(delivery){

									var paymentParams = { group_id:jsonItems.payment_id, card_type:jsonItems.card_type, payment_type:jsonItems.payment_type, account_id:arrParams.account_id };
									methods.getUserPaymentInfoforOrders(paymentParams,function(payment){
									
										var sub_total = parseInt(jsonItems.total)+parseFloat(jsonItems.delivery_charge);
										var item_count = 0;

										details.forEach( function (value){							
										    item_count += parseInt(value.qty);
										});

										var obj_paymentinfo = {
											status       : '1',
											created_date : jsonItems.date_created
										};

										main_obj = {
											id               : jsonItems.group_id.toString(),
											order_number     : pad_with_zeroes(jsonItems.group_id,5),
											delivery_charge  : jsonItems.delivery_charge,
											sub_total        : jsonItems.total,
											grand_total      : sub_total.toString(),
											item_count       : item_count.toString(),
											status           : jsonItems.status,
											payment_type     : jsonItems.payment_type.toString(),
											card_type        : jsonItems.card_type.toString(),
											created_date     : jsonItems.date_created,
											products         : details,
											delivery_details : delivery,
											payment_details  : payment,
											checkout_info    : [obj_paymentinfo]
										}


										returnArr.push(main_obj);
										counter -= 1;
										if (counter === 0){ 
											resolve(returnArr);
										}

									});

								});

							});
			
						})
					}],
					function (result, current) {
						if (counter === 0){ 

						   	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.created_date) - new Date(a.created_date); 
		                	});

							return_result(result[0]);
						}
					});
				});

			}
			else
			{
				return_result(null);
			}

		});
	
	}
	catch(err)
	{ console.log("getOrderList",err); return_result(null); }	
};

methods.getOrderDetails =  function (orderParams,return_result){
	
	try
	{	

		var main_obj      = {};
		var returnArr     = [];

		var query = `SELECT * FROM jaze_carts WHERE group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'account_id' AND meta_value =?)
				AND group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'order_id' AND meta_value =?)`;

		DB.query(query,[orderParams.account_id,orderParams.order_id], function(data, error){
		
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var queryIn = "SELECT * FROM jaze_products WHERE id=?";
							DB.query(queryIn,[jsonItems.product_id], function(dataIn, error){
						
								if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
								{	

									methods.getProductBrand(dataIn[0].jaze_brand_id, function(retBrand){	

										var product_image = '';
										if(jsonItems.featured_image){
											product_image = STATIC_IMG_PROD+jsonItems.company_id+'/products/'+jsonItems.product_id+'/thumb/'+jsonItems.featured_image;
										}

										var current_date = dateFormat(now,'yyyy-mm-dd');
										var sale_duration = 0;
										if(parseInt(jsonItems.sale_onsale) === 1)
										{			

											var start = moment(current_date);
											var end =  moment(current_date);

											if(jsonItems.sale_price_start_from !='' && jsonItems.sale_price_start_to !='')
											{
												end = moment(jsonItems.sale_price_start_to);
											}

											sale_duration = end.diff(start, "days");
											
										}

										var product_price = jsonItems.regular_price;

										if(Math.sign(sale_duration) > 0){
											product_price = jsonItems.sale_price;
										}

										var total_price = parseInt(product_price) * parseInt(jsonItems.quantity);

										main_obj = {
											item_id		  : jsonItems.group_id.toString(),
											product_id    : jsonItems.product_id,
											product_name  : jsonItems.product_name,
											product_logo  : product_image,
											brand_id      : dataIn[0].jaze_brand_id.toString(),
											brand_name    : retBrand,
											qty           : jsonItems.quantity,
											price         : product_price,
											total_price   : total_price.toString(),
											created_date  : jsonItems.modified_datetime
										
										};
							
										counter -= 1;
										returnArr.push(main_obj);
										if (counter === 0){ 
											resolve(returnArr);
										}

									});
								}

							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 

						   	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.created_date) - new Date(a.created_date); 
		                	});
							return_result(result[0]);
						}
					});
				});

			}
			else
			{
				return_result([]);
			}

		});
	
	}
	catch(err)
	{ console.log("getOrderDetails",err); return_result([]); }	
};

methods.getUserShippingInfoForOrders =  function (group_id,return_result){
	
	try
	{			
	
		var arrObj    = {};
		var query = `SELECT * FROM jaze_user_billing_address WHERE group_id =?`;

		DB.query(query,[group_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){

					methods.__getCountryDetails(retConverted[0].country_id, function(country){

						methods.__getStateDetails(retConverted[0].state_id, function(state){

							methods.__getCityDetails(retConverted[0].city_id, function(city){

								methods.__getAreaDetails(retConverted[0].area_id, function(area){

									methods.__getMobileCodeID(retConverted[0].mobile_country_code, function(mobcode){

										methods.__getMobileCodeID(retConverted[0].landline_country_code, function(lancode){

											arrObj = {
												id               : retConverted[0].group_id.toString(),
												first_name       : retConverted[0].first_name,
												last_name        : retConverted[0].last_name,
												mobile_code      : retConverted[0].mobile_country_code,
												mobile_code_id   : mobcode,
												mobile_no        : retConverted[0].mobile_no,
												landline_code    : retConverted[0].landline_country_code,
												landline_code_id : lancode,
												landline_no      : retConverted[0].landline_no,
												building_no      : retConverted[0].building_no,
												building_name    : retConverted[0].building_name,
												street_no        : retConverted[0].street_no,
												street_name      : retConverted[0].street_name,
												country_id       : retConverted[0].country_id,
												country_name     : country,
												state_id         : retConverted[0].state_id,
												state_name       : state,
												city_id          : retConverted[0].city_id,
												city_name        : city,
												area_id          : retConverted[0].area_id,
												area_name        : area,
												shipping_note    : retConverted[0].shipping_note,
												landmark         : retConverted[0].landmark,
												location_type    : retConverted[0].location_type,
												is_default       : retConverted[0].is_default
									
											};

											return_result(arrObj);

										});
									});
								});

							});

						});

					});

				});

			}
			else
			{
				return_result('');
			}

		});
		
	}
	catch(err)
	{ console.log("getUserShippingInfo",err); return_result(''); }	
};

methods.getUserPaymentInfoforOrders =  function (arrPayment,return_result){
	
	try
	{			

		var arrObj    = {};

		var return_obj = {
			jaze_pay    : '',
			bank        : '',
			credit_card : ''
		};

		if(parseInt(arrPayment.payment_type) === 1)
		{
			methods.getUserJazePay(arrPayment, function(retjazepay){	

				return_obj.jaze_pay = retjazepay;
				return_result(return_obj);

			});

		}
		else if(parseInt(arrPayment.payment_type) === 2)
		{

			if(parseInt(arrPayment.card_type) === 1)
			{
				var query = `SELECT * FROM jaze_user_bank_details WHERE group_id=?`;
				DB.query(query,[arrPayment.group_id], function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						methods.convertMultiMetaArray(data, function(retConverted){

							arrObj = {

								id			    : retConverted[0].group_id.toString(),
								holder_name		: retConverted[0].holder_name,
								account_no	 	: retConverted[0].account_no.toString(),
								bank_name	 	: retConverted[0].bank_name,
								branch_name	 	: retConverted[0].branch_name

							};
							return_obj.bank = arrObj;
							return_result(return_obj);

						});

					}else{
						return_result('');
					}
					
				});
			}
			else
			{
				
				var query = `SELECT * FROM jaze_user_credit_card WHERE group_id=?`;
				DB.query(query,[arrPayment.group_id], function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						methods.convertMultiMetaArray(data, function(retConverted){

							arrObj = {

								id          : retConverted[0].group_id.toString(),
								card_name   : retConverted[0].card_name,
								bank_name   : retConverted[0].bank_name,
								card_no     : retConverted[0].card_no,
								exp_month   : retConverted[0].exp_month,
								exp_year    : retConverted[0].exp_year,
			
							}

							return_obj.credit_card = arrObj;
							return_result(return_obj);
					
						});

					}else{
						return_result(return_obj);
					}
					
				});
			}
		}


	

		
	
	}
	catch(err)
	{ console.log("getUserPaymentInfoforOrders",err); return_result(return_obj); }	
};

//--------------------------------------- jazestore order list ---------------------------------------------------------------//

//--------------------------------------- jazestore order details -------------------------------------------------------------//

methods.getOrderDetailsPerId =  function (arrParams,return_result){
	
	try
	{	

		var main_obj = {};
		var returnArr = [];

		var query = `SELECT * FROM jaze_carts_orders WHERE group_id IN (SELECT group_id FROM jaze_carts_orders WHERE meta_key = 'account_id' AND meta_value =?)
            AND group_id=?`;

		DB.query(query,[arrParams.account_id,arrParams.order_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
	
				methods.convertMultiMetaArray(data, function(retConverted){	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
							
							var orderParams = {account_id:arrParams.account_id, order_id:jsonItems.group_id};
							methods.getProductDetailsPerOrderId(orderParams,function(details){

								methods.getUserShippingInfoForOrders(jsonItems.delivery_id,function(delivery){

									var paymentParams = { group_id:jsonItems.payment_id, card_type:jsonItems.card_type, payment_type:jsonItems.payment_type, account_id:arrParams.account_id };
									methods.getUserPaymentInfoforOrders(paymentParams,function(payment){
									
										var sub_total = parseInt(jsonItems.total)+parseFloat(jsonItems.delivery_charge);
										var item_count = 0;

										details.forEach( function (value){							
										    item_count += parseInt(value.qty);
										});

										main_obj = {
											id               : jsonItems.group_id.toString(),
											order_number     : pad_with_zeroes(jsonItems.group_id,5),
											delivery_charge  : jsonItems.delivery_charge,
											sub_total        : jsonItems.total,
											grand_total      : sub_total.toString(),
											item_count       : item_count.toString(),
											status           : jsonItems.status,
											payment_type     : jsonItems.payment_type.toString(),
											card_type        : jsonItems.card_type.toString(),
											created_date     : jsonItems.date_created,
											products         : details,
											delivery_details : delivery,
											payment_details  : payment
										}

										returnArr.push(main_obj);
										counter -= 1;
										if (counter === 0){ 
											resolve(returnArr);
										}

									});

								});

							});
			
						})
					}],
					function (result, current) {
						if (counter === 0){ 

						   	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.created_date) - new Date(a.created_date); 
		                	});

							return_result(result[0]);
						}
					});
				});

			}
			else
			{
				return_result(null);
			}

		});
	
	}
	catch(err)
	{ console.log("getOrderDetailsPerId",err); return_result(null); }	
};

methods.getProductDetailsPerOrderId =  function (orderParams,return_result){
	
	try
	{	

		var main_obj      = {};
		var returnArr     = [];

		var query = `SELECT * FROM jaze_carts WHERE group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'account_id' AND meta_value =?)
				AND group_id IN (SELECT group_id FROM jaze_carts WHERE meta_key = 'order_id' AND meta_value =?)`;

		DB.query(query,[orderParams.account_id,orderParams.order_id], function(data, error){
		
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var queryIn = "SELECT * FROM jaze_products WHERE id=?";
							DB.query(queryIn,[jsonItems.product_id], function(dataIn, error){
						
								if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
								{	

									methods.getProductBrand(dataIn[0].jaze_brand_id, function(retBrand){	

										var product_image = '';
										if(jsonItems.featured_image){
											product_image = STATIC_IMG_PROD+jsonItems.company_id+'/products/'+jsonItems.product_id+'/thumb/'+jsonItems.featured_image;
										}

										var current_date = dateFormat(now,'yyyy-mm-dd');
										var sale_duration = 0;
										if(parseInt(jsonItems.sale_onsale) === 1)
										{			

											var start = moment(current_date);
											var end =  moment(current_date);

											if(jsonItems.sale_price_start_from !='' && jsonItems.sale_price_start_to !='')
											{
												end = moment(jsonItems.sale_price_start_to);
											}

											sale_duration = end.diff(start, "days");
											
										}

										var product_price = jsonItems.regular_price;

										if(Math.sign(sale_duration) > 0){
											product_price = jsonItems.sale_price;
										}

										var total_price = parseInt(product_price) * parseInt(jsonItems.quantity);

										main_obj = {
											item_id		  : jsonItems.group_id.toString(),
											product_id    : jsonItems.product_id,
											product_name  : jsonItems.product_name,
											product_logo  : product_image,
											brand_id      : dataIn[0].jaze_brand_id.toString(),
											brand_name    : retBrand,
											qty           : jsonItems.quantity,
											price         : product_price,
											total_price   : total_price.toString(),
											created_date  : jsonItems.modified_datetime
										
										};
							
										counter -= 1;
										returnArr.push(main_obj);
										if (counter === 0){ 
											resolve(returnArr);
										}

									});
								}

							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 

						   	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.created_date) - new Date(a.created_date); 
		                	});
							return_result(result[0]);
						}
					});
				});

			}
			else
			{
				return_result([]);
			}

		});
	
	}
	catch(err)
	{ console.log("getProductDetailsPerOrderId",err); return_result([]); }	
};

methods.getUserShippingInfoForOrderDetails =  function (group_id,return_result){
	
	try
	{			
	
		var arrObj    = {};
		var query = `SELECT * FROM jaze_user_billing_address WHERE group_id =?`;

		DB.query(query,[group_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){

					methods.__getCountryDetails(retConverted[0].country_id, function(country){

						methods.__getStateDetails(retConverted[0].state_id, function(state){

							methods.__getCityDetails(retConverted[0].city_id, function(city){

								methods.__getAreaDetails(retConverted[0].area_id, function(area){

									methods.__getMobileCodeID(retConverted[0].mobile_country_code, function(mobcode){

										methods.__getMobileCodeID(retConverted[0].landline_country_code, function(lancode){

											arrObj = {
												id               : retConverted[0].group_id.toString(),
												first_name       : retConverted[0].first_name,
												last_name        : retConverted[0].last_name,
												mobile_code      : retConverted[0].mobile_country_code,
												mobile_code_id   : mobcode,
												mobile_no        : retConverted[0].mobile_no,
												landline_code    : retConverted[0].landline_country_code,
												landline_code_id : lancode,
												landline_no      : retConverted[0].landline_no,
												building_no      : retConverted[0].building_no,
												building_name    : retConverted[0].building_name,
												street_no        : retConverted[0].street_no,
												street_name      : retConverted[0].street_name,
												country_id       : retConverted[0].country_id,
												country_name     : country,
												state_id         : retConverted[0].state_id,
												state_name       : state,
												city_id          : retConverted[0].city_id,
												city_name        : city,
												area_id          : retConverted[0].area_id,
												area_name        : area,
												shipping_note    : retConverted[0].shipping_note,
												landmark         : retConverted[0].landmark,
												location_type    : retConverted[0].location_type,
												is_default       : retConverted[0].is_default
									
											};

											return_result(arrObj);

										});
									});
								});

							});

						});

					});

				});

			}
			else
			{
				return_result('');
			}

		});
		
	}
	catch(err)
	{ console.log("getUserShippingInfoForOrderDetails",err); return_result(''); }	
};

methods.getUserPaymentInfoforOrderDetails =  function (arrPayment,return_result){
	
	try
	{			

		var arrObj    = {};

		var return_obj = {
			jaze_pay    : '',
			bank        : '',
			credit_card : ''
		};

		if(parseInt(arrPayment.payment_type) === 1)
		{
			methods.getUserJazePay(arrPayment, function(retjazepay){	

				return_obj.jaze_pay = retjazepay;
				return_result(return_obj);

			});

		}
		else if(parseInt(arrPayment.payment_type) === 2)
		{

			if(parseInt(arrPayment.card_type) === 1)
			{
				var query = `SELECT * FROM jaze_user_bank_details WHERE group_id=?`;
				DB.query(query,[arrPayment.group_id], function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						methods.convertMultiMetaArray(data, function(retConverted){

							arrObj = {

								id			    : retConverted[0].group_id.toString(),
								holder_name		: retConverted[0].holder_name,
								account_no	 	: retConverted[0].account_no.toString(),
								bank_name	 	: retConverted[0].bank_name,
								branch_name	 	: retConverted[0].branch_name

							};
							return_obj.bank = arrObj;
							return_result(return_obj);

						});

					}else{
						return_result('');
					}
					
				});
			}
			else
			{
				
				var query = `SELECT * FROM jaze_user_credit_card WHERE group_id=?`;
				DB.query(query,[arrPayment.group_id], function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						methods.convertMultiMetaArray(data, function(retConverted){

							arrObj = {

								id          : retConverted[0].group_id.toString(),
								card_name   : retConverted[0].card_name,
								bank_name   : retConverted[0].bank_name,
								card_no     : retConverted[0].card_no,
								exp_month   : retConverted[0].exp_month,
								exp_year    : retConverted[0].exp_year,
			
							}

							return_obj.credit_card = arrObj;
							return_result(return_obj);
					
						});

					}else{
						return_result(return_obj);
					}
					
				});
			}
		}

	
	}
	catch(err)
	{ console.log("getUserPaymentInfoforOrderDetails",err); return_result(return_obj); }	
};

//--------------------------------------- jazestore order details -------------------------------------------------------------//


//--------------------------------------- jazestore search keywords-------------------------------------------------------------//


methods.getKeywordSearch = function(arrParams,return_result){

	try
	{	

		var arrProducts = [];
		var objProducts = {};

		var pagination_details  = {
			total_page   : '0',
        	current_page : '0',
        	next_page    : '0',
          	total_result : '0'
        };

		var queryIn = "SELECT * FROM jaze_products WHERE keywords LIKE '%"+arrParams.keywords+"%' AND jaze_category_id NOT IN(208,209,210,56,57,58,59,155,162,248,0) AND category_id NOT IN(1841) ";


		if(parseInt(arrParams.country_id) !== 0)
		{
			queryIn += " AND country_id="+arrParams.country_id+" ";
		}

		if(parseInt(arrParams.state_id) !== 0)
		{
			queryIn += " AND state_id="+arrParams.state_id+" ";
		}


		if(parseInt(arrParams.city_id) !== 0)
		{
			queryIn += " AND city_id="+arrParams.city_id+" ";
		}

		queryIn += " ORDER BY created_date DESC";
		
		DB.query(queryIn,null, function(dataIn, error){


			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		

				var counter = dataIn.length;		
				var jsonItems = Object.values(dataIn);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	

						methods.getJazeProductDetailsMeta(jsonItems.id, function(retProductMeta){		


							methods.getProductBrand(jsonItems.jaze_brand_id, function(retBrand){	

								var sale_duration = '';
								if(Math.sign(retProductMeta.sale_duration) !='-1' && Math.sign(retProductMeta.sale_duration) !='0'){
									sale_duration = retProductMeta.sale_duration+' days left';
								}

								var product_image = "";
								if(retProductMeta.featured_image !=''){
									product_image = STATIC_IMG_PROD+jsonItems.account_id+'/products/'+jsonItems.id+'/thumb/'+retProductMeta.featured_image;
								}

								var discount = "";
								if(retProductMeta.sale_price!=''){
									discount = (retProductMeta.sale_price-retProductMeta.regular_price) / retProductMeta.regular_price * 100;
								}

								if(parseInt(Math.floor(discount)) === 0){
									discount = "";
								}else{
									discount = 	Math.floor(discount)+'%'
								}
					
								objProducts = {
									product_id       : jsonItems.id.toString(),
									product_name     : jsonItems.name,
									product_title    : jsonItems.title,
									brand_name       : retBrand,
									product_image    : product_image,
									regular_price    : (parseFloat(retProductMeta.regular_price).toFixed(2)).toString(),
									sale_price       : (parseFloat(retProductMeta.sale_price).toFixed(2)).toString(),
									percent_discount : discount,
									sale_duration    : sale_duration.toString(),
									created_date     : jsonItems.created_date,
									result_flag      : '1'
								}

								arrProducts.push(objProducts);

								counter -= 1;
								if (counter === 0){ 
									resolve(arrProducts);
								}

							});
							
						});
					})
				}],
				function (result, current) {
					if (counter === 0){ 
									
						var newArray     = [];
						var total_page   = 0;
						var total_result = result[0].length.toString();
									
						if(parseInt(result[0].length) > 10){
		
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 10 ), (parseInt(arrParams.page) + 1) * 10);
							total_page = Math.ceil((parseInt(result[0].length) - 10 ) / 10);

						}else{
	
							newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 10 ), (parseInt(arrParams.page) + 1) * 10);
							total_page = 0;
						}

						var next_page = 0;
						if(parseInt(newArray.length) > 0){
							next_page  = parseInt(arrParams.page)+1;
						}

			            pagination_details.total_page   = total_page.toString();
			            pagination_details.current_page = arrParams.page.toString();
			            pagination_details.next_page    = next_page.toString();
			            pagination_details.total_result = total_result.toString();

            		  	newArray.sort(function(a, b){ 
	                   	 	return new Date(b.created_date) - new Date(a.created_date); 
	                	});

						return_result(newArray,pagination_details);		
					}
				});

			}
			else
			{
				return_result(null,pagination_details);
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getKeywordSearch",err); return_result(null,pagination_details); 
	}	
};


//--------------------------------------- jazestore search keywords-------------------------------------------------------------//


//--------------------------------------- general modules -------------------------------------------------//

methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMultiMetaArray",err); return_result(null);}
};


function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].group_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}


methods.convertMultiMetaArrayProduct =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndexProduct(list,arrvalues[i].product_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							product_id : arrvalues[i].product_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMultiMetaArray",err); return_result(null);}
};


function __getGroupIndexProduct(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].product_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}



methods.__getCountryDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT country_name FROM country  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getStateDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT state_name FROM states  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].state_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getCityDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT city_name FROM cities  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].city_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getAreaDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT area_name FROM area  WHERE area_id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].area_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};


methods.getCurrency =  function(currency_id,return_result){
	try
	{
		DB.query("SELECT code FROM currency_master WHERE currency_id=?",[currency_id], function(data, error){

			if(data!=null){
				return_result(data[0].code);
			}else{
				return_result('');
			}

		});

	}
	catch(err)
	{ console.log("getCurrency",err); return_result(null);}
};


function removeDuplicates(array) {
	return array.filter((a, b) => array.indexOf(a) === b)
};


function removeDuplicatesOjects(originalArray, prop) {
     var newArray = [];
     var lookupObject  = {};

     for(var i in originalArray) {
        lookupObject[originalArray[i][prop]] = originalArray[i];
     }

     for(i in lookupObject) {
         newArray.push(lookupObject[i]);
     }
      return newArray;
 }


function sort_prod(array,keyword)
{
	return array.filter(o => o.product_name.toLowerCase().includes(keyword.toLowerCase()))
  	.sort((a, b) => a.product_name.toLowerCase().indexOf(keyword.toLowerCase()) - b.product_name.toLowerCase().indexOf(keyword.toLowerCase()));
}


function sort_keywords(array,keyword)
{
	return array.filter(o => o.keywords.toLowerCase().includes(keyword.toLowerCase()))
  	.sort((a, b) => a.keywords.toLowerCase().indexOf(keyword.toLowerCase()) - b.keywords.toLowerCase().indexOf(keyword.toLowerCase()));
}


function sortingArrayObject(key, order='asc') {

	return function(a, b) {
	  if(!a.hasOwnProperty(key) || 
		 !b.hasOwnProperty(key)) {
		  return 0; 
	  }
	  
	  const varA = (typeof a[key] === 'string') ? 
		a[key].toUpperCase() : a[key];
	  const varB = (typeof b[key] === 'string') ? 
		b[key].toUpperCase() : b[key];
		
	  let comparison = 0;
	  if (varA > varB) {
		comparison = 1;
	  } else if (varA < varB) {
		comparison = -1;
	  }
	  return (
		(order == 'desc') ? 
		(comparison * -1) : comparison
	  );
	};
}

methods.convertMultiMetaArrayCompany =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndexCompany(list,arrvalues[i].account_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndexCompany(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].account_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}


methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
};


methods.__getCountryDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT country_name FROM country  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('');
	}
};

methods.__getStateDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT state_name FROM states  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].state_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('');
	}
};

methods.__getCityDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT city_name FROM cities  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].city_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('');
	}
};

methods.__getAreaDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT area_name FROM area  WHERE area_id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].area_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('');
	}
};


methods.__getMobileCode= function (phone_code,return_result){
	try
	{
		if(parseInt(phone_code) !== 0)
		{
			var query = " SELECT `phonecode` FROM `country` WHERE `id` = '"+phone_code+"'; ";
			DB.query(query, null, function(data, error){
				
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result("+"+data[0].phonecode);
				}
				else
				{ return_result(''); }
			});

			
		}
		else
		{
			return_result('');
		}
	}
	catch(err)
	{ console.log("getMobileCode",err); return_result(''); }	
};


methods.__getMobileCodeID = function (phone_code,return_result){
	try
	{
		if(parseInt(phone_code) !== 0)
		{
			var query = " SELECT `id` FROM `country` WHERE `phonecode` = '"+phone_code+"'; ";
			DB.query(query, null, function(data, error){
				
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result(data[0].id.toString());
				}
				else
				{ return_result(''); }
			});

			
		}
		else
		{
			return_result('');
		}
	}
	catch(err)
	{ console.log("getMobileCode",err); return_result(''); }	
};


function pad_with_zeroes(number, length) {

    var my_string = '' + number;
    while (my_string.length < length) {
        my_string = '0' + my_string;
    }

    return my_string;

}

//--------------------------------------- general modules -------------------------------------------------//

module.exports = methods;