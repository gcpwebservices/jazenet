const methods = {};
const DB = require('../../helpers/db_base.js');





const express = require('express');
const router = express.Router();

methods.getCityDetails = function(arrParams,return_result)
{ 

	var newObj = {};
	var arrRows = [];

	try
	{
		var quer = `SELECT co.timezone,co.id AS country_id,co.country_code AS country_code, s.id AS state_id ,c.id AS city_id, c.city_name, co.country_name, s.state_name
			FROM  country AS co 
			INNER JOIN states AS s ON s.country_id = co.id
			INNER JOIN cities AS c ON c.state_id = s.id 
		WHERE c.id =? AND co.country_code >''`;

		DB.query(quer, [arrParams.city_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

		  		newObj = {
		  			city_id:data[0].city_id.toString(),
		  			city_name:data[0].city_name,
		  			state_name:data[0].state_name,
		  			country_name:data[0].country_name,
		  			country_code:data[0].country_code
		  		};

		  		return_result(newObj); 

			}
			else
			{
				return_result(null); 
			}

		});

	}
	catch(err)
	{ 
		console.log("getCity",err); 
		return_result(null); 
	}	
}





module.exports = methods;