const methods = {};
const DB = require('../../helpers/db_base.js');

const promiseForeach = require('promise-foreach');

const dateFormat = require('dateformat');
const now = new Date();
const standardDT = dateFormat(now,'yyyy-mm-dd HH:MM:ss');

// const express = require('express');
// const router = express.Router();
const HelperGeneral = require('../../helpers/HelperGeneral.js');
const HelperNotification = require('../../jazecom/helpers/HelperNotification.js');

const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

const fs = require('fs');
const download = require('download-file');
const url = require('url');
const path = require('path');
const mime = require('mime-types');

methods.getCompanyProfile =  function(arrParams,return_result){
	try
	{
			
		var arrAccounts = [];
		var queryAccountType = `SELECT account_id,account_type FROM jaze_user_basic_details WHERE account_id =?`;

		DB.query(queryAccountType,[arrParams.account_id], function(data, error){

			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var queryAccDetails = `SELECT * FROM jaze_user_basic_details WHERE account_id =? and account_type=?`;

				DB.query(queryAccDetails,[data[0].account_id,data[0].account_type], function(allData, error){
	
					if(typeof allData !== 'undefined' && allData !== null && parseInt(allData.length) > 0)
					{

						methods.convertMultiMetaArrayCompany(allData, function(retConverted){	

							var counter = retConverted.length;		
							var jsonItems = Object.values(retConverted);

							promiseForeach.each(jsonItems,[function (val) {
								return new Promise(function (resolve, reject) {	

									methods.__getCountryDetails(val.country_id, function(retCountry){

										methods.__getStateDetails(val.state_id, function(retState){

											methods.__getCityDetails(val.city_id, function(retCity){

												methods.__getNationality(val.nationality_id, function(retNationality){

													methods.getCurrency(val.currency_id, function(retCurrency){
								
												  		var default_logo = '';

														var newObj = {};

												  		if(parseInt(data[0].account_type) === 1)
												  		{
												  			if(val.profile_logo){
																default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + val.profile_logo + '';
															}

															newObj = {
																account_id	  : val.account_id.toString(),
																account_type  : data[0].account_type.toString(),
																name		  : (val.fname +" "+val.lname).toString(),
																logo		  : default_logo,
																contact_no    : val.mobile,
																contact_phone_code : val.mobile_country_code_id,
																country_id    : val.country_id,
																state_id      : val.state_id,
																city_id       : val.city_id,
																country_name  : retCountry,
																state_name    : retState,
																city_name     : retCity,
																national_id   : val.nationality_id,
																nationality   : retNationality,
																currency      : retCurrency,
																currency_id   : val.currency_id.toString()
															}	

												  		}
												  		else if(parseInt(data[0].account_type) === 2)
												  		{

									  						if(val.profile_logo){
																default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + val.profile_logo + '';
															}

															newObj = {
																account_id	  : val.account_id.toString(),
																account_type  : data[0].account_type.toString(),
																name		  : (val.fname +" "+val.lname).toString(),
																logo		  : default_logo,
																contact_no    : val.mobile,
																contact_phone_code : val.mobile_country_code_id,
																country_id    : val.country_id,
																state_id      : val.state_id,
																city_id       : val.city_id,
																country_name  : retCountry,
																state_name    : retState,
																city_name     : retCity,
																national_id   : val.nationality_id,
																nationality   : retNationality,
																currency      : retCurrency,
																currency_id   : val.currency_id.toString()
															}	

												  		}
											  			else if(parseInt(data[0].account_type) === 3)
										  				{

			  				 								if(val.listing_images){
									  							default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/listing_images/' + val.listing_images + '';
															
															}else if(val.company_logo){
																default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/logo/' + val.company_logo + '';
															}else{
																default_logo = "";
															}

															newObj = {
																account_id	  : val.account_id.toString(),
																account_type  : data[0].account_type.toString(),
																name		  : val.company_name,
																logo		  : default_logo,
																contact_no    : val.company_phone,
																contact_phone_code : val.company_phone_code,
																country_id    : val.country_id,
																state_id      : val.state_id,
																city_id       : val.city_id,
																country_name  : retCountry,
																state_name    : retState,
																city_name     : retCity,
																national_id   : val.nationality_id,
																nationality   : retNationality,
																currency      : retCurrency,
																currency_id   : val.currency_id.toString()
															}	
										  				}

														counter -= 1;
														if (counter === 0){ 
															resolve(newObj);
														}

													});	
												});	
											});	
										});	
									
									});	
									
								})
							}],
							function (result, current) {
								if (counter === 0){ 
									return_result(result[0]);
								}
							});

						});

					}
					else
					{
						return_result(null);
					}
				});

		
			}
			else
			{
				return_result(null);
			}
		});

	}
	catch(err)
	{ console.log("getCompanyProfile",err); return_result(null);}
};


methods.getCompanyProfileFew =  function(arrParams,return_result){
	try
	{
		
		var arrAccounts = [];

		var queryAccountType = `SELECT account_id,account_type FROM jaze_user_basic_details WHERE account_id =?`;

		DB.query(queryAccountType,[arrParams.account_id], function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var queryAccDetails = `SELECT * FROM jaze_user_basic_details WHERE account_id =? and account_type=?`;

				DB.query(queryAccDetails,[data[0].account_id,data[0].account_type], function(allData, error){
	
					if(typeof allData !== 'undefined' && allData !== null && parseInt(allData.length) > 0)
					{

						methods.convertMultiMetaArrayCompany(allData, function(retConverted){	

							var counter = retConverted.length;		
							var jsonItems = Object.values(retConverted);

							promiseForeach.each(jsonItems,[function (val) {
								return new Promise(function (resolve, reject) {		
						
							  		var default_logo = '';

									var newObj = {};

							  		if(parseInt(data[0].account_type) === 1)
							  		{
							  			if(val.profile_logo){
											default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + val.profile_logo + '';
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : (val.fname +" "+val.lname).toString(),
											logo		  : default_logo
										}	

							  		}
							  		else if(parseInt(data[0].account_type) === 2)
							  		{

				  						if(val.professional_logo){
											default_logo = G_STATIC_IMG_URL + "uploads/jazenet/professional_account/"+val.account_id+"/logo_thumb/thumb_" + val.professional_logo;
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : (val.fname +" "+val.lname).toString(),
											logo		  : default_logo
										}	

							  		}
						  			else if(parseInt(data[0].account_type) === 3)
					  				{

		  								if(val.listing_images){
				  							default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/listing_images/' + val.listing_images + '';
										
										}else if(val.company_logo){
											default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/logo/' + val.company_logo + '';
										}else{
											default_logo = "";
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : val.company_name,
											logo		  : ''
										}	

					  				}

									counter -= 1;
									if (counter === 0){ 
										resolve(newObj);
									}
									
								})
							}],
							function (result, current) {
								if (counter === 0){ 
									return_result(result[0]);
								}
							});

						});


					}
					else
					{
						return_result(null);
					}
				});

		
			}
			else
			{
				return_result(null);
			}
		});

	}
	catch(err)
	{ console.log("getCompanyProfileFew",err); return_result(null);}
};



methods.getAttributeParentJobs = function(arrParams,return_result){

	try
	{
		var arrRows = [];
		var newObj = {};
		var status = '1';
		var category_id = '823';

		if(parseInt(arrParams.category_id) !== 0){
			category_id = arrParams.category_id;
		}

		var quer = `SELECT t1.* FROM(
				SELECT group_id,
					(CASE WHEN meta_key = 'parent_id' THEN meta_value END) AS parent_id,
					(SELECT c.meta_value FROM jaze_category_list c WHERE c.meta_key = 'sort_order' AND c.group_id = jaze_category_list.group_id) AS sort_order
				FROM  jaze_category_list
				WHERE group_id =? ) AS t1 WHERE parent_id != ''`;


		DB.query(quer,[category_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				var querParent = `SELECT * FROM jaze_category_list WHERE group_id IN (SELECT group_id FROM jaze_category_list WHERE meta_key = 'parent_id' AND meta_value =? ) `;
				DB.query(querParent,[data[0].parent_id], function(pdata, error){

					if(typeof pdata !== 'undefined' && pdata !== null && parseInt(pdata.length) > 0){


						methods.convertMultiMetaArray(pdata, function(retConverted){
		
							const forEach = async () => {
							  	const values = retConverted;
							  	for (const val of values){
									var status = '1';
									if(val.group_id !== parseInt(category_id) ){
										status = '0';
									}

									if(parseInt(val.group_id) !==0){
										newObj = {
											title: val.cate_name,
											active_status: status,
											category_id: val.group_id.toString()
										};
										arrRows.push(newObj);
									}
							
							  	}

						  		const result = await returnData(arrRows);
							}
							 
							const returnData = x => {
							  return new Promise((resolve, reject) => {
							    setTimeout(() => {
							      resolve(x);
							    }, 300);
							  });
							}
							 
							forEach().then(() => {
								return_result(arrRows);
							})
	
						});
					}

				});
	
	        }
	        else
	        { 
	        	return_result(null);
	        }

		});

	}
	catch(err)
	{ console.log("getAttributeParentJobs",err); return_result(null); }	

};



methods.getCategoryAttributesJobs = function(arrParams,return_result){


	try
	{
		var category_id = '6';
		var mainArr = [];
		var objController = {};

		if(parseInt(arrParams.category_id) !== 0){
			category_id = arrParams.category_id;
		}


		var quer = `SELECT * FROM jaze_category_attributes_groups WHERE group_id IN (SELECT group_id FROM jaze_category_attributes_groups WHERE meta_key = 'cat_id' AND (meta_value =? OR meta_value ='6' )  ) 
		AND group_id IN (SELECT group_id FROM  jaze_category_attributes_groups  WHERE meta_key = 'status' AND meta_value = '1' ) `;
		if(parseInt(arrParams.category_id) === 0){
			quer += `OR group_id = '23'`;
		}

		DB.query(quer,[category_id], function(data, error){

				
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (val) {
						return new Promise(function (resolve, reject) {	
							
							methods.getCategoryAttributeValueJobs(val.group_id,val.input_type, function(retMainArr){ 
								
								if(val.input_type === "multiselect_image_button"){

									controller_type = '1';
									attribute_option_labe = 'all';

								 	objController = {
										controller_type:'1',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'all',
									 	items_value_list:retMainArr
									};
					
								}else if(val.input_type === "multiselect_dropdown"){
									controller_type = '2';
									attribute_option_labe = 'all';

								 	objController = {
										controller_type:'2',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'all',
									 	items_value_list:retMainArr
									};

						
								}else if(val.input_type === "color_picker"){
									controller_type = '3';
									attribute_option_labe = 'all';

								 	objController = {
										controller_type:'3',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'all',
									 	items_color_list:retMainArr
									};
					
								}else if(val.input_type === "image_button"){
									controller_type = '4';
									attribute_option_labe = 'all';

								 	objController = {
										controller_type:'4',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'all',
									 	items_value_list:retMainArr
									};

						
								}else if(val.input_type === "dropdown"){
									controller_type = '5';
									attribute_option_labe = 'all';

								 	objController = {
										controller_type:'5',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'all',
									 	items_value_list:retMainArr
									};
						
								}else if(val.input_type === "radio_button"){
									controller_type = '6';

								 	objController = {
										controller_type:'6',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'',
									 	items_sing_value_list:retMainArr
									};
				
								}else if(val.input_type === "select_range"){
									controller_type = '7';
								 	objController = {
										controller_type:'7',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'',
									 	items_select_range_value:retMainArr
									};
				
								}else if(val.input_type === "input_range"){
									controller_type = '8';
									attribute_option_labe = 'all';
								 	objController = {
										controller_type:'8',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'all',
									 	items_input_range_value:retMainArr
									};
							
								}else if(val.input_type === "text"){
									controller_type = '9';
								 	objController = {
										controller_type:'9',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'',
									 	items_edittext_value:retMainArr
									};
						
								}else if(val.input_type === "button"){
									controller_type = '10';
								 	objController = {
										controller_type:'10',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'',
									 	items_sing_value_list:retMainArr
									};
						
								}
								
								mainArr.push(objController);
								counter -= 1;
								if (counter === 0){ 
									resolve(mainArr);
								}
							});


						
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							var sortedArray = result[0].sort(function(a, b) {
							    return a.home_sort_order > b.home_sort_order ? 1 : a.home_sort_order < b.home_sort_order ? -1 : 0;
							});

							return_result(sortedArray);
						}
					});


				});
	        }
	        else
	        { 
	        	return_result([]);
	        }

		});

	}
	catch(err)
	{ console.log("getCategoryAttributesJobs",err); return_result(null); }	
};


methods.getCategoryAttributeValueJobs = function(group_id,input_type,return_result){

	try
	{
		var arrValues = [];
		var obj_values = {};
		var controller_type;
		var objController = {};
		var value;
		var logo;

		if(input_type == 'color_picker'){
			obj_values = {
				id:'0',
				title:'all',
				color_code:''
			};
			arrValues.push(obj_values);
		}

		var singleObj = ['text','input_range','select_range'];
	 	var arrSameInputs = ['multiselect_image_button','multiselect_dropdown','image_button','dropdown'];

		if(arrSameInputs.includes(input_type) || input_type == 'color_picker' || input_type == 'radio_button' || input_type == 'text'  || input_type == 'button'){

			var quer = `SELECT * FROM jaze_category_attributes_groups_meta 
				WHERE group_id IN (SELECT group_id FROM jaze_category_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
				AND group_id IN (SELECT group_id FROM  jaze_category_attributes_groups_meta  WHERE meta_key = 'status' AND meta_value = '1' )`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					methods.convertMultiMetaArray(data, function(retConverted){	


						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (vala) {
							return new Promise(function (resolve, reject) {		
					
													
								if(arrSameInputs.includes(input_type)){

									if(vala.value != null){
										value = vala.value;
										logo  = '';
									}else{
										value = '';
										logo = vala.logo;
									}

									if(input_type == 'multiselect_image_button'){

										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value:'',
											logo:G_STATIC_IMG_URL+vala.value
										};

									}else if(input_type == 'image_button'){

										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value:value,
											logo:logo
										};

									}else{
										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value:value,
											logo:logo
										};
									}

								}else{



									if(input_type == 'color_picker'){

										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											color_code:vala.value
										};

									}else if(input_type == 'radio_button'){


										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value: vala.value,
								
										};

									}else if(input_type == 'button'){


										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value: vala.value,
								
										};

									}else if(input_type == 'text'){
										
										obj_values = {
											text_hint_label :vala.key_name
										};
									}

								}

								
							
								counter -= 1;
								resolve(obj_values);
							
							})
						}],
						function (result, current) {
							if (counter === 0){ 
								arrValues.push(result[0]);
							
							}
						});

					});

		  		}

			});

		
		}else if(input_type == 'input_range'){
	

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_jazestore_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var counter = data.length;		
					var jsonItems = Object.values(data);

					var min_field_label;
					var max_field_label;
					var suffix_field_label;
					var separator_field_label;

					promiseForeach.each(jsonItems,[function (vala) {
						return new Promise(function (resolve, reject) {		
							
			

							if(vala.key_name == 'separator label'){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == 'min label'){
								min_field_label = vala.key_value;
							}

							if(vala.key_name == 'max label'){
								max_field_label = vala.key_value;
							}

							if(vala.key_name == 'suffix'){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								min_field_label:min_field_label,
								max_field_label:max_field_label,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

							counter -= 1;
							if (counter === 0){ 
								resolve(obj_values);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							arrResults.push(result[0]);
						}
					});

		  		}

			});
					
		}else if(input_type == 'select_range'){
	
		

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_jazestore_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){


					var counter = data.length;		
					var jsonItems = Object.values(data);

					var min_field_label;
					var min_field_value;
					var max_field_label;
					var max_field_value;
					var interval_value;
					var suffix_field_label;
					var separator_field_label;

					promiseForeach.each(jsonItems,[function (vala) {
						return new Promise(function (resolve, reject) {		
								
							if(vala.key_name == "separator label"){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == "min label"){
								min_field_label = vala.key_value;
							}


							if(vala.key_name == "min value"){
								min_field_value = vala.key_value;
							}

							if(vala.key_name == "max label"){
								max_field_label = vala.key_value;
							}


							if(vala.key_name == "max value"){
								max_field_value = vala.key_value;
							}

							if(vala.key_name == "interval value"){
								interval_value = vala.key_value;
							}


							if(vala.key_name == "suffix"){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								id:'0',
								min_field_label:min_field_label,
								min_field_value:min_field_value,
								max_field_label:max_field_label,
								max_field_value:max_field_value,
								internal_value:interval_value,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

							counter -= 1;
							if (counter === 0){ 
								resolve(obj_values);
							}
							
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							arrValues.push(result[0]);
						}
					});

		  		}

			});
					
		}

		setTimeout(function(){ 

			if(singleObj.includes(input_type)){
				return_result(obj_values);
			}else{
				return_result(arrValues.sort(sortingArrayObject('title')));
			}
			
		}, 500);

	}
	catch(err)
	{ console.log("getCategoryAttributeValue",err); return_result([]); }	
};



// ----------------------------- search jobs start ------------------------------//

methods.searchJobs = function(arrParams,return_result)
{

	try
	{
		var arrCategory = [];
		var arrGroupID  = [];

		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };

	    let category_id = '824';

		if(parseInt(arrParams.category_id) !== 0){
  			category_id = arrParams.category_id;
  		}

	  	if(arrParams.attributes.length > 0)
	  	{

			var query = `SELECT * FROM jaze_category_list WHERE group_id =?`;

			DB.query(query, [category_id], function(mainCat, error){

				if(typeof mainCat !== 'undefined' && mainCat !== null && parseInt(mainCat.length) > 0)
				{	

					methods.convertMultiMetaArray(mainCat, function(retMainCat){

						var querSubCat = "SELECT * FROM jaze_category_attributes_groups WHERE group_id IN ";
							querSubCat += " (SELECT group_id FROM jaze_category_attributes_groups WHERE meta_key = 'cat_id' AND ( meta_value = '"+retMainCat[0].group_id+"' OR  meta_value = '"+retMainCat[0].parent_id+"') )";
						DB.query(querSubCat, null, function(subCat, error){

							if(typeof subCat !== 'undefined' && subCat !== null && parseInt(subCat.length) > 0)
							{	
								
								var counter = arrParams.attributes.length;		
								var jsonItems = Object.values(arrParams.attributes);

								promiseForeach.each(jsonItems,[function (jsonItems) {
									return new Promise(function (resolve, reject) {		
						
								  		var parameter = '';
								  		var childQuer = '';
							  		
										if(parseInt(jsonItems.type) >= 1 &&  parseInt(jsonItems.type) <= 6)
										{	
											var title = jsonItems.value.toLowerCase();
											if(jsonItems.value.toLowerCase() != ''){
												title = jsonItems.value.toLowerCase().split(",");
											}
											parameter = title;
											childQuer = "SELECT * FROM jaze_jobs_attributes_values WHERE meta_key=? AND meta_value IN (?)";
										}
										else if(parseInt(jsonItems.type) == 7)
										{	
											childQuer = "SELECT * FROM jaze_jobs_attributes_values WHERE meta_key=? ";
											childQuer += " AND ( SUBSTRING_INDEX(meta_value, '-', 1) >='"+jsonItems.min_value+"' AND  SUBSTRING_INDEX(meta_value, '-', 2) <='"+jsonItems.max_value+"' )";
										}
										else if(parseInt(jsonItems.type) == 8)
										{
											parameter = jsonItems.value;
											childQuer = "SELECT * FROM jaze_jobs_attributes_values WHERE meta_key=? AND meta_value =? ";
										}
										else if(parseInt(jsonItems.type) == 9 || parseInt(jsonItems.type) == 10)
										{
											childQuer = "SELECT * FROM jaze_jobs_attributes_values WHERE meta_key=? AND LOWER(meta_value) LIKE '%" +jsonItems.value.toLowerCase()+ "%' ";
										}
							
										DB.query(childQuer,[jsonItems.attribute_id,parameter], function(child, error){

											if(typeof child !== 'undefined' && child !== null && parseInt(child.length) > 0)
											{	
												methods.convertMultiMetaArray(child, function(retChild){

													retChild.forEach(function(val) {
														arrGroupID.push(val.group_id.toString());
													});

													counter -= 1;
													if (counter === 0){ 
														resolve(arrGroupID);
													}
												});
											}
											else
											{
												counter -= 1;
												if (counter === 0){ 
													resolve(arrGroupID);
												}
											}
										});

									})
								}],
								function (result, current) {
									if (counter === 0){ 
										if(parseInt(result[0].length) > 0)
										{
											methods.searchPeopleForJobsIn(result[0],arrParams, function(retCompany,pagination_details){	
												if(retCompany!=null){
													return_result(retCompany,pagination_details);
												}else{
													return_result(null,pagination_details);
												}
											});
										}
										else
										{
											return_result(null,pagination_details);
										}
									}
								});

							}
							else
							{
								return_result(null,pagination_details);
							}

						});
					});

				}
				else
				{
					return_result(null,pagination_details);
				}

			});
		}
		else
		{

			let group_id;

			var query = `SELECT * FROM jaze_category_list WHERE group_id =?`;

			DB.query(query, [category_id], function(mainCat, error){

				if(typeof mainCat !== 'undefined' && mainCat !== null && parseInt(mainCat.length) > 0)
				{	

					methods.convertMultiMetaArray(mainCat, function(retMainCat){

						group_id = retMainCat[0].group_id;

						methods.searchPeopleForJobs(group_id,arrParams, function(retCompany,pagination_details){	

							if(retCompany!=null){
								return_result(retCompany,pagination_details);
							}else{
								return_result(null,pagination_details);
							}
						
						});

					});


				}else{
					return_result(null,pagination_details);
				}

			});	

		}

	}
	catch(err)
	{ 
		console.log("searchJobs",err); 
		return_result(null,pagination_details); 
	}	

};


methods.searchPeopleForJobsIn = function(arrGroupId,arrParams, return_result)
{
		
	try
	{

		var arrAccounts = [];
		var arrObjects = {};

		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };


		var querSubCat = `SELECT * FROM jaze_job_people_list WHERE group_id IN (?)
		 				AND group_id IN (SELECT group_id FROM jaze_job_people_list WHERE meta_key = 'status' AND meta_value ='1') `;

		if(parseInt(arrParams.country_id) !==0 ){
			querSubCat += " AND group_id IN (SELECT group_id FROM jaze_job_people_list WHERE meta_key = 'countries' AND  FIND_IN_SET('"+arrParams.country_id+"', meta_value)  )";
		}

		if(parseInt(arrParams.state_id) !==0 ){
			querSubCat += " AND group_id IN (SELECT group_id FROM jaze_job_people_list WHERE meta_key = 'states' AND  FIND_IN_SET('"+arrParams.state_id+"', meta_value)  )";
		}

		if(parseInt(arrParams.city_id) !==0 ){
			querSubCat += " AND group_id IN (SELECT group_id FROM jaze_job_people_list WHERE meta_key = 'locations' AND  FIND_IN_SET('"+arrParams.city_id+"', meta_value)  )";
		}


		DB.query(querSubCat, [arrGroupId], function(data, error){
	
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParams = {account_id:jsonItems.account_id};
							methods.getCompanyProfile(accParams, function(retProfile){	


								methods.__gePeopleProfileLogo(jsonItems.account_id, function(basicPhoto){	

									if(retProfile!=null)
									{

										var default_logo = retProfile.logo;
										if(jsonItems.profile_logo != ""){

											default_logo = G_STATIC_IMG_URL+"uploads/jazenet/individual_account/jobs/"+jsonItems.account_id+"/"+jsonItems.profile_logo;
										}
										else if(basicPhoto != ""){

											default_logo = G_STATIC_IMG_URL+"uploads/jazenet/individual_account/jobs/"+jsonItems.account_id+"/"+basicPhoto;
										}

										arrObjects = {
											group_id    : jsonItems.group_id.toString(),
											account_id  : jsonItems.account_id.toString(),
											first_name  : jsonItems.first_name,
											last_name   : jsonItems.last_name,
											logo        : default_logo,
											professional_title  : jsonItems.professional_title,
											skills              : jsonItems.skills,
											country_name        : retProfile.country_name,
											state_name          : retProfile.state_name,
											city_name           : retProfile.city_name,
											nationality         : retProfile.nationality

										}
									
										counter -= 1;
										arrAccounts.push(arrObjects);

									}
								
									if (counter === 0){ 
										resolve(arrAccounts);
									}

								});
							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							var newArray = [];
							var total_result = result[0].length.toString();
							var total_page = 0;

							if(parseInt(result[0].length) > 50){
								newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
								total_page = Math.ceil((parseInt(result[0].length) - 50 ) / 50);
							}else{
								newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
								total_page = 0;
							}

							var next_page = 0;
							if(parseInt(total_page) !== 0){
								next_page  = parseInt(arrParams.page)+1;
							}

		                 	if(parseInt(total_page) >= parseInt(arrParams.page)){

								pagination_details  = {
									total_page   : total_page.toString(),
					                current_page : arrParams.page.toString(),
					                next_page    : next_page.toString(),
					                total_result : total_result
					            };

							  	return_result(newArray.sort(sortingArrayObject('first_name')),pagination_details);
		
				            }else{
			            		return_result(null,pagination_details);
				            }	

						}
					});

				});
			}
			else
			{
				return_result(null,pagination_details);
			}


		});

	}
	catch(err)
	{ 
		console.log("searchPeopleForJobs",err); 
		return_result(null,pagination_details); 
	}	

};

methods.__gePeopleProfileLogo  =  function (account_id,return_result){
	
	try
	{
		if(parseInt(account_id) !== 0)
		{
			var query  = " SELECT meta_value FROM jaze_job_cv_basic_details WHERE meta_key in('profile_logo') ";
				query += " and `group_id` in (SELECT group_id FROM jaze_job_cv_basic_details WHERE `meta_key` = 'status' AND `meta_value` = '1' ";
				query += " and `group_id` in (SELECT group_id FROM jaze_job_cv_basic_details WHERE `meta_key` = 'account_id' AND `meta_value` = '"+account_id+"')) ";   
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0].meta_value);
					}
					else
					{ return_result(""); }
			    });
		}
		else
		{ return_result(""); }
	}
	catch(err)
	{ console.log("__gePeopleProfileLogo",err);  return_result(""); }	
};



methods.searchPeopleForJobs = function(category_id,arrParams, return_result)
{
		
	try
	{

		var arrAccounts = [];
		var arrObjects = {};

		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };


		var querSubCat = `SELECT * FROM jaze_job_people_list WHERE group_id IN (SELECT group_id FROM jaze_job_people_list WHERE meta_key = 'job_category' AND meta_value =?)
		 				AND group_id IN (SELECT group_id FROM jaze_job_people_list WHERE meta_key = 'status' AND meta_value ='1') `;

		if(parseInt(arrParams.country_id) !==0 ){
			querSubCat += " AND group_id IN (SELECT group_id FROM jaze_job_people_list WHERE meta_key = 'countries' AND  FIND_IN_SET('"+arrParams.country_id+"', meta_value)  )";
		}

		if(parseInt(arrParams.state_id) !==0 ){
			querSubCat += " AND group_id IN (SELECT group_id FROM jaze_job_people_list WHERE meta_key = 'states' AND  FIND_IN_SET('"+arrParams.state_id+"', meta_value)  )";
		}

		if(parseInt(arrParams.city_id) !==0 ){
			querSubCat += " AND group_id IN (SELECT group_id FROM jaze_job_people_list WHERE meta_key = 'locations' AND  FIND_IN_SET('"+arrParams.city_id+"', meta_value)  )";
		}


		DB.query(querSubCat, [category_id], function(data, error){
	
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParams = {account_id:jsonItems.account_id};
							methods.getCompanyProfile(accParams, function(retProfile){	

								methods.__gePeopleProfileLogo(jsonItems.account_id, function(basicPhoto){	

									if(retProfile!=null)
									{

										var default_logo = retProfile.logo;

										if(jsonItems.profile_logo != ""){

											default_logo = G_STATIC_IMG_URL+"uploads/jazenet/individual_account/jobs/"+jsonItems.account_id+"/"+jsonItems.profile_logo;
										}
										else if(basicPhoto != ""){

											default_logo = G_STATIC_IMG_URL+"uploads/jazenet/individual_account/jobs/"+jsonItems.account_id+"/"+basicPhoto;
										}


										

										arrObjects = {
											group_id    : jsonItems.group_id.toString(),
											account_id  : jsonItems.account_id.toString(),
											first_name  : jsonItems.first_name,
											last_name   : jsonItems.last_name,
											logo        : default_logo,
											professional_title  : jsonItems.professional_title,
											skills              : jsonItems.skills,
											country_name        : retProfile.country_name,
											state_name          : retProfile.state_name,
											city_name           : retProfile.city_name,

										}
									
										counter -= 1;
										arrAccounts.push(arrObjects);

									}
								
									if (counter === 0){ 
										resolve(arrAccounts);
									}
								});
							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							var newArray = [];
							var total_result = result[0].length.toString();
							var total_page = 0;

							if(parseInt(result[0].length) > 50){
								newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
								total_page = Math.ceil((parseInt(result[0].length) - 50 ) / 50);
							}else{
								newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
								total_page = 0;
							}

							var next_page = 0;
							if(parseInt(total_page) !== 0){
								next_page  = parseInt(arrParams.page)+1;
							}


			                 if(parseInt(total_page) >= parseInt(arrParams.page)){

								pagination_details  = {
									total_page   : total_page.toString(),
					                current_page : arrParams.page.toString(),
					                next_page    : next_page.toString(),
					                total_result : total_result
					            };

					     
							  	return_result(newArray.sort(sortingArrayObject('first_name')),pagination_details);
		
				            }else{

			            		return_result(null,pagination_details);

				            }	

						}
					});

				});
			}
			else
			{
				return_result(null,pagination_details);
			}


		});

	}
	catch(err)
	{ 
		console.log("searchPeopleForJobs",err); 
		return_result(null,pagination_details); 
	}	

};



// ----------------------------- search jobs end ------------------------------//



// ---------- job details start --------------- //

methods.getPeopleDetails =  function(arrParams,return_result){
	try
	{
		var objDetails = {};
		var arrDetails = [];

		var obj_basic_details = {};
		var obj_company_details = {};

		var arr_obj_specification = [];
			var obj_specification = {};
			var arr_inner_obj_specification = [];
				var inner_obj_specification = {};

		var query = "SELECT * FROM jaze_job_people_list WHERE group_id=?";

		DB.query(query,[arrParams.group_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	

					var query = "SELECT * FROM  jaze_jobs_attributes_values WHERE group_id=?";
					DB.query(query,[retConverted[0].group_id], function(detail, error){

						// if(typeof detail !== 'undefined' && detail !== null && parseInt(detail.length) > 0)
						// {
		
							var accParams = {account_id: retConverted[0].account_id};

							methods.getCompanyProfile(accParams, function(retProfile){

								methods.getCvStatus(retConverted[0].account_id, function(retCvStatus){
									var shortParam = {group_id: retConverted[0].group_id, account_id:arrParams.account_id };

									methods.getShortListStatus(shortParam, function(retShort){

										methods.__gePeopleProfileLogo(retConverted[0].account_id, function(basicPhoto){	

											methods.getShortListDetails(arrParams, function(retShortListGroups){

												methods.getRequestCvStatus(arrParams, function(retRequestCv){	

													methods.__getCountryDetails(retConverted[0].last_worked_country, function(retCountry){	

														methods.__getCityDetails(retConverted[0].last_worked_city, function(retCity){	

															if(retProfile!=null){	
															
																var obj_values = {
																	work_experience : '',
																	notice_period   : '',
																	education       : '',
																	commitment      : '',
																	salary_exp      : '',
																	category        : '',
																	career_level    : ''
																};

																if(typeof detail !== 'undefined' && detail !== null && parseInt(detail.length) > 0)
																{
																	detail.forEach(function(value) {
																		if(value.meta_key == '9'){
																			obj_values.notice_period = value.meta_value;
																		}
																		if(value.meta_key == '10'){
																			obj_values.salary_exp = value.meta_value;
																		}
																		if(value.meta_key == '11'){
																			obj_values.career_level = value.meta_value;
																		}
																		if(value.meta_key == '12'){
																			obj_values.work_experience = value.meta_value;
																		}
																		if(value.meta_key == '13'){
																			obj_values.education = value.meta_value;
																		}
																		if(value.meta_key == '14'){
																			obj_values.commitment = value.meta_value;
																		}
																		if(value.meta_key == '24'){
																			obj_values.category = value.meta_value;
																		}
																	});

																}

																var default_logo = retProfile.logo;

																if(retConverted[0].profile_logo != ""){

																	default_logo = G_STATIC_IMG_URL+"uploads/jazenet/individual_account/jobs/"+retConverted[0].account_id+"/"+retConverted[0].profile_logo;
																}
																else if(basicPhoto != ""){

																	default_logo = G_STATIC_IMG_URL+"uploads/jazenet/individual_account/jobs/"+retConverted[0].account_id+"/"+basicPhoto;
																}

																var comments = '';
																var about = '';
																if(retConverted[0].comment){
																	comments = retConverted[0].comment;
																}
																if(retConverted[0].about){
																	about = retConverted[0].about;
																}

																var to_date = '';
																if(retConverted[0].currently_working){
																	if(parseInt(retConverted[0].currently_working) !==0){
																		to_date = retConverted[0].end_date;
																	}
																}

																var recent_roles = {};
																if(retConverted[0].last_company_name){

																	recent_roles = {
																		title          : retConverted[0].designation,
																		country        : retCountry,
																		city           : retCity,
																		company_name   : retConverted[0].last_company_name,
																		from_date      : retConverted[0].start_date,
																		to_date        : to_date
																	};

																}
														
																var jaze_chat_status = '0';
																var jaze_call_status = '0';
																var jaze_mail_status = '0';

																if(retConverted[0].jaze_chat_status){
																	jaze_chat_status=retConverted[0].jaze_chat_status.toString();
																}
																if(retConverted[0].jaze_call_status){
																	jaze_chat_status=retConverted[0].jaze_call_status.toString();
																}
																if(retConverted[0].jaze_mail_status){
																	jaze_chat_status=retConverted[0].jaze_mail_status.toString();
																}
																	

																objDetails = {
																	account_id   : retConverted[0].account_id.toString(),
																	first_name   : retConverted[0].first_name,
																	last_name    : retConverted[0].last_name,
																	logo         : default_logo,
																	title        : retConverted[0].professional_title,
																	city_name    : retProfile.city_name,
																	country_name : retProfile.country_name,
																	personal_details      : {
																		gender          : retConverted[0].gender, 
																		nationality     : retProfile.nationality,
																		nationality_id  : retProfile.national_id.toString(),
																		notice_period   : retConverted[0].notice_period
																
																	},
																	job_details   : {
																		career_level    : obj_values.career_level,
																		category        : obj_values.category,
																		commitment      : obj_values.commitment,
																		education       : obj_values.education,
																		experience      : obj_values.work_experience,
																		salary_exp      : obj_values.salary_exp
																	},
																	most_recent_role   : recent_roles,
																	skills       : retConverted[0].skills,
																	about_me     : {
																		description: about
																	},
																	comments     : {
																		description: comments
																	},
																

																	jazecom_status   : {
																		chat     : jaze_chat_status,
																		call     : jaze_call_status,
																		mail     : jaze_mail_status

																	},
																	cv_status         : retCvStatus.toString(),
																	shortlist_status  : retShort.toString(),
																	shortlist_groups  : retShortListGroups,
																	request_cv_status : retRequestCv
																
																};
															}

															return_result(objDetails);

										
														});
													});
												});
											});
										});

									});
								});
							});
					
						// }else{
						// 	return_result(null);
						// }

					});
				});

			}
			else
			{ return_result(null); }

		});

	}
	catch(err)
	{ console.log("getPeopleDetails",err); return_result(null);}
};


methods.getCvStatus =  function(account_id,return_result){
	try
	{
		
		var query = `SELECT * FROM jaze_job_cv_additional_info WHERE group_id IN (SELECT group_id FROM jaze_job_cv_additional_info WHERE meta_key = 'account_id' AND meta_value =?)`;

		DB.query(query,[account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	

					if(retConverted[0].privacy){
						return_result(retConverted[0].privacy);
					}else{
						return_result(0);
					}

				});

			}
			else
			{
			 	return_result(0);
			}
		});



	}
	catch(err)
	{ console.log("getCvStatus",err); return_result(0);}
};


methods.getRequestCvStatus =  function(arrParams,return_result){
	try
	{
		
		var query = `SELECT * FROM jaze_job_shortlisted_people WHERE group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'account_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'people_group_id' AND meta_value =?)`;

		DB.query(query,[arrParams.account_id,arrParams.group_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	

					var status = '0';

					if(retConverted[0].request_cv)
					{	
						if(parseInt(retConverted[0].request_cv) === 0){
							status = '1';
						}else if(parseInt(retConverted[0].request_cv) === 1){
							status = '2';
						}else if(parseInt(retConverted[0].request_cv) === 2){
							status = '3';
						}
						
						return_result(status);
					}
					else
					{
						return_result('0');
					}

				});

			}
			else
			{
			 	return_result('0');
			}
		});



	}
	catch(err)
	{ console.log("getCvStatus",err); return_result('0');}
};

methods.getShortListDetails =  function(arrParams,return_result){
	try
	{
		
		var query = `SELECT * FROM jaze_job_shortlisted_people WHERE group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'account_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'people_group_id' AND meta_value =?)`;

		DB.query(query,[arrParams.account_id,arrParams.group_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	
					return_result(retConverted[0].department_id.toString());
				});

			}
			else
			{
			 	return_result('');
			}
		});



	}
	catch(err)
	{ console.log("getShortListDetails",err); return_result('');}
};

methods.getShortListStatus =  function(shortParam,return_result){
	try
	{
		
		var query = `SELECT * FROM jaze_job_shortlisted_people WHERE group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'people_group_id' AND meta_value =? ) 
		AND group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'account_id' AND meta_value =? ) `;

		DB.query(query,[shortParam.group_id,shortParam.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				return_result(1);
			}
			else
			{
			 	return_result(0);
			}
		});



	}
	catch(err)
	{ console.log("getShortListStatus",err); return_result(0);}
};


// ---------- job details end --------------- //




// shortlist ------------------------ start //


methods.shortilistGroups =  function(arrParams,return_result){
	try
	{
		
		if(parseInt(arrParams.flag) == 1 )
		{

			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_job_shortlist_groups";
  			var lastGrpId = 0;

			methods.getLastGroupId(queGroupMax, function(queGrpRes){

			  	queGrpRes.forEach(function(row) {
					lastGrpId = row.lastGrpId;
			  	});

				var arrInsert = {
					group_name  : arrParams.department_name,
					account_id  : arrParams.account_id,
					type        : 1,
					create_date : standardDT,
					status      : 1
				};
			
				var plusOneGroupID = lastGrpId+1;
		  		var querInsert = `insert into jaze_job_shortlist_groups (group_id, meta_key, meta_value) VALUES (?,?,?)`;

		  		var returnFunction;
			 	for (var key in arrInsert) 
			 	{
					DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
			 		 	if(parseInt(data.affectedRows) > 0){
	 						return_result(1);
				 	   	}else{
		   					return_result(0);
				 	   	}
			 	 	});
			  	};

			});

		}
		else if(parseInt(arrParams.flag) == 2 )
		{

			var query = `SELECT * FROM jaze_job_shortlist_groups WHERE group_id=?`;

			DB.query(query,[arrParams.department_id], function(datau, error){

				if(typeof datau !== 'undefined' && datau !== null && parseInt(datau.length) > 0)
				{
					var updateQ = `UPDATE jaze_job_shortlist_groups SET meta_value =? WHERE meta_key ='group_name' AND group_id =?`;

					DB.query(updateQ,[arrParams.department_name,arrParams.department_id], function(data, error){
					
			 		 	if(parseInt(data.affectedRows) > 0){
							return_result(2);
				 	   	}else{
		   					return_result(0);
				 	   	}
				
			 	 	});
		 	 	}
		 	 	else
		 	 	{
		 	 		return_result(4);
		 	 	}

	 		});

		}
		else if(parseInt(arrParams.flag) == 3 )
		{


			var query = `SELECT * FROM jaze_job_shortlist_groups WHERE group_id=?`;

			DB.query(query,[arrParams.department_id], function(datau, error){

				if(typeof datau !== 'undefined' && datau !== null && parseInt(datau.length) > 0)
				{
					var updateQ = `DELETE FROM jaze_job_shortlist_groups WHERE group_id=?`;

					DB.query(updateQ,[arrParams.department_id], function(data, error){
					
			 		 	if(parseInt(data.affectedRows) > 0){
							return_result(3);
				 	   	}else{
		   					return_result(0);
				 	   	}
				
			 	 	});
		 	 	}
		 	 	else
		 	 	{
		 	 		return_result(4);
		 	 	}

	 		});

		}

	}
	catch(err)
	{ console.log("shortilistGroups",err); return_result(0);}
};


methods.getShortListGroups =  function(arrParams,return_result){
	try
	{
		var arrResults = [];
		var arrObjects = {};

		var query = `SELECT * FROM jaze_job_shortlist_groups WHERE group_id IN (SELECT group_id FROM jaze_job_shortlist_groups WHERE meta_key='account_id' AND meta_value=?)`;

		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getShortlistedAccounts(jsonItems.group_id, function(retCount){
					
								arrObjects = {
									department_id    : jsonItems.group_id.toString(),
									department_name  : jsonItems.group_name,
									department_type  : jsonItems.type.toString(),
									shortlist_count  : retCount.toString()
								};

								counter -= 1;
								arrResults.push(arrObjects);
						
								if (counter === 0){ 
									resolve(arrResults);
								}

							});

						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0].sort(sortingArrayObject('department_name')));
						}
					});

				});

	 	 	}
	 	 	else
	 	 	{
	 	 		return_result(null);
	 	 	}

 		});


	}
	catch(err)
	{ console.log("getShortListGroups",err); return_result(null);}
};


methods.getShortlistedAccounts =  function(arrParams,return_result){
	try
	{
		var arrResults = [];
		var arrObjects = {};

		var query = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'people_group_id' THEN meta_value END) AS people_group_id
			FROM  jaze_job_shortlisted_people
			WHERE  group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'department_id' AND FIND_IN_SET(?, meta_value) ) ) AS t1 WHERE people_group_id != ''`;

		DB.query(query,[arrParams], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				var counter = data.length;		
				return_result(counter);
	 	 	}
	 	 	else
	 	 	{
	 	 		return_result(0);
	 	 	}

 		});

	}
	catch(err)
	{ console.log("getShortListGroups",err); return_result(null);}
};

methods.addToShortListGroup =  function(arrParams,return_result){
	try
	{
		var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_job_shortlisted_people";
			var lastGrpId = 0;

		methods.getLastGroupId(queGroupMax, function(queGrpRes){

		  	queGrpRes.forEach(function(row) {
				lastGrpId = row.lastGrpId;
		  	});

			var arrInsert = {
				account_id       : arrParams.account_id,
				department_id    : arrParams.department_id.toString(),
				people_group_id  : arrParams.account_group_id,
				create_date      : standardDT,
				shortlisted_type : 1,
				status           : 1
			};
		
			var plusOneGroupID = lastGrpId+1;
	  		var querInsert = `insert into jaze_job_shortlisted_people (group_id, meta_key, meta_value) VALUES (?,?,?)`;

	  		var returnFunction;
		 	for (var key in arrInsert) 
		 	{
				DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
		 		 	if(parseInt(data.affectedRows) > 0){
 						return_result(1);
			 	   	}else{
	   					return_result(0);
			 	   	}
		 	 	});
		  	};

		});
	}
	catch(err)
	{ console.log("addToShortListGroup",err); return_result(null);}
};

methods.removeToShortListGroup  =  function (arrParams,return_result){
	
	try
	{
		if(arrParams !== null)
		{
			var query  = " SELECT group_id FROM jaze_job_shortlisted_people WHERE  ";
				query += "   group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'account_id'  AND meta_value = "+arrParams.account_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'people_group_id'  AND meta_value = "+arrParams.account_group_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'status' AND meta_value = 1 ))); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						//delete
						var query  = " DELETE FROM `jaze_job_shortlisted_people` WHERE `group_id` = '"+data[0].group_id+"'; ";

						DB.query(query,null, function(data_check, error){

							return_result(1);

						});
					}
					else
					{ return_result(0); }
			    });
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{ console.log("removeToShortListGroup",err); return_result(0); }	
};




// shortlist ------------------------ end //


// request cv -------------------------- start //


methods.addToRequestCv =  function(arrParams,return_result){
	try
	{

		var arrResults = [];
		var arrObjects = {};

		var query = `SELECT * FROM jaze_job_shortlisted_people WHERE group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'account_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'people_group_id' AND meta_value =?)`;

		DB.query(query,[arrParams.account_id,arrParams.account_group_id], function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				methods.convertMultiMetaArray(data, function(retConverted){	

					var jsonItems = Object.values(retConverted);

					if(jsonItems[0].request_cv)
					{

						if(parseInt(jsonItems[0].request_cv == 1)){
							return_result(3);
						}else{
							return_result(2);
						}

					}
					else
					{


	 					methods.getCvOwnerDetails(jsonItems[0].people_group_id,function(retAccountID){   

							var accParam = {account_id:retAccountID};

							methods.getCompanyProfileFew(accParam,function(retCVOwner){
								if(retCVOwner!=null)
	 		 					{
									methods.getCompanyProfileFew(arrParams,function(retRequestOwner){

										if(retRequestOwner!=null)
	 		 		 					{

											var notiParams = {	
												group_id             : jsonItems[0].group_id.toString(),
												account_id           : retRequestOwner.account_id,
												account_type         : retRequestOwner.account_type,
												request_account_id   : retCVOwner.account_id,
												request_account_type : retCVOwner.account_type,
												message              : "",
												flag                 :'20'
											};

											HelperNotification.sendPushNotification(notiParams, function(retNoti){		

												if(parseInt(retNoti) !==0 )
												{

											  		var querInsert = `insert into jaze_job_shortlisted_people (group_id, meta_key, meta_value) VALUES (?,?,?)`;
										  			DB.query(querInsert,[jsonItems[0].group_id,'request_cv','0'], function(data, error){
											 		 	if(parseInt(data.affectedRows) > 0){
															return_result(1);
												 	   	}else{
										   					return_result(0);
												 	   	}
											 	 	});

												}
												else
												{
													return_result(0);
												}


											});

 		 		 						}

									});
 		 						}

	 						});

	 					});

					}

				});

	 	 	}
	 	 	else
	 	 	{

				var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_job_shortlisted_people";
				var lastGrpId = 0;

				methods.getLastGroupId(queGroupMax, function(queGrpRes){

				  	queGrpRes.forEach(function(row) {
						lastGrpId = row.lastGrpId;
				  	});

					var arrInsert = {
						account_id       : arrParams.account_id,
						department_id    : arrParams.department_id.toString(),
						people_group_id  : arrParams.account_group_id,
						create_date      : standardDT,
						shortlisted_type : 1,
						request_cv       : 0,
						status           : 1
					};
				
					var plusOneGroupID = lastGrpId+1;
			  		var querInsert = `insert into jaze_job_shortlisted_people (group_id, meta_key, meta_value) VALUES (?,?,?)`;

			  		var returnFunction;
				 	for (var key in arrInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
				 		 	if(parseInt(data.affectedRows) > 0){
		 						returnFunction = 1;
					 	   	}else{
			   					returnFunction = 0;
					 	   	}
				 	 	});
				  	};

				  	setTimeout(function(){

						var query = `SELECT * FROM jaze_job_shortlisted_people WHERE group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'account_id' AND meta_value =?)
							AND group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'people_group_id' AND meta_value =?)`;

						DB.query(query,[arrParams.account_id,arrParams.account_group_id], function(data, error){


							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
							{
								methods.convertMultiMetaArray(data, function(retConverted){	

									var jsonItems = Object.values(retConverted);

				 					methods.getCvOwnerDetails(jsonItems[0].people_group_id,function(retAccountID){   

										var accParam = {account_id:retAccountID};
										methods.getCompanyProfileFew(accParam,function(retCVOwner){
													

											if(retCVOwner!=null)
				 		 					{

												methods.getCompanyProfileFew(arrParams,function(retRequestOwner){
					
													if(retRequestOwner!=null)
				 		 		 					{

														var notiParams = {	
															group_id             : jsonItems[0].group_id.toString(),
															account_id           : retRequestOwner.account_id,
															account_type         : retRequestOwner.account_type,
															request_account_id   : retCVOwner.account_id,
															request_account_type : retCVOwner.account_type,
															message              : "",
															flag                 :'20'
														};

														HelperNotification.sendPushNotification(notiParams, function(retNoti){		

															if(parseInt(retNoti) !==0 ){
																return_result(1);
															}else{
																return_result(0);
															}

														});

			 		 		 						}

												});
			 		 						}

				 						});

				 					});


								});

							}
							else
							{
								return_result(0);
							}

						});


				  	 }, 300);

				});

	 	 	}

 		});
	}
	catch(err)
	{ console.log("addToRequestCv",err); return_result(null);}
};


methods.getCvOwnerDetails =  function(account_group_id,return_result){
	try
	{
		var arrResults = [];
		var arrObjects = {};

		var query = `SELECT * FROM jaze_job_people_list WHERE group_id =?`;

		DB.query(query,[account_group_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				methods.convertMultiMetaArray(data, function(retConverted){	
					var jsonItems = Object.values(retConverted);

					return_result(jsonItems[0].account_id);
 				});
	 	 	}
	 	 	else
	 	 	{
	 	 		return_result(null);
	 	 	}

 		});

	}
	catch(err)
	{ console.log("getCvOwnerDetails",err); return_result(null);}
};


// add to request cv -------------- end //



// request cv status -------------------- start //

methods.setAcceptRejectRequestCv =  function(arrParams,return_result){
	try
	{
		//get reciver account id
		var query  = "  SELECT meta_value FROM jaze_notification_list WHERE meta_key in ('account_id') AND  ";
			query += " group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'receiver_account_id' AND meta_value = '"+arrParams.account_id+"' and group_id = '"+arrParams.group_id+"') ";

			DB.query(query,null, function(dataNoti, error){

				if(typeof dataNoti !== 'undefined' && dataNoti !== null && parseInt(dataNoti.length) > 0)
				{
					//get shortlisted people group id
					var query  = " SELECT group_id FROM jaze_job_shortlisted_people WHERE ";
						query += " group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'account_id' AND meta_value = '"+dataNoti[0].meta_value+"' ";
						query += " AND group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'people_group_id' AND meta_value = '"+arrParams.id+"')) group by group_id ";

						DB.query(query,null, function(dataPeop, error){

							if(typeof dataPeop !== 'undefined' && dataPeop !== null && parseInt(dataPeop.length) > 0)
							{
								//update status
								var query = "UPDATE jaze_job_shortlisted_people SET meta_value = '"+arrParams.status+"' WHERE meta_key = 'request_cv' AND group_id = '"+dataPeop[0].group_id+"' ";

								DB.query(query,null, function(data, error){

									// delete notification
									// var query =  " SELECT group_id FROM jaze_notification_list WHERE ";
									// 	query += " `group_id` IN (SELECT `group_id` FROM jaze_notification_list WHERE `meta_key` = 'status' AND meta_value = '1' ) AND ";
									// 	query += " `group_id` IN (SELECT `group_id` FROM jaze_notification_list WHERE `meta_key` = 'account_id' AND (meta_value = '"+arrParams.account_id+"' or meta_value = '"+dataNoti[0].meta_value+"')) AND ";
									// 	query += " `group_id` IN (SELECT `group_id` FROM jaze_notification_list WHERE `meta_key` = 'receiver_account_id' AND (meta_value = '"+arrParams.account_id+"' or meta_value = '"+dataNoti[0].meta_value+"')) AND ";
									// 	query += " `group_id` IN (SELECT `group_id` FROM jaze_notification_list WHERE `meta_key` = 'notify_id' AND meta_value = '"+arrParams.id+"') AND ";
									// 	query += " `group_id` IN (SELECT `group_id` FROM jaze_notification_list WHERE `meta_key` = 'notify_type' AND meta_value IN (120,121,122)) group by group_id";

									// DB.query(query,null, function(dataDele, error){

										var query = " DELETE FROM `jaze_notification_list` WHERE `group_id` = '"+arrParams.group_id+"' ";

										DB.query(query,null, function(data, error){

											//send push
											HelperGeneral.getAccountTypeDetails(arrParams.account_id, function(ownerAccType){		

												HelperGeneral.getAccountTypeDetails(dataNoti[0].meta_value, function(recivAccType){		

													var flag;
													
													if(parseInt(arrParams.status) === 1){ flag = '21'; }
													else if(parseInt(arrParams.status) === 2){ flag = '22'; }

													var notiParams = {	
															group_id             : arrParams.id.toString(),
															account_id           : arrParams.account_id,
															account_type         : ownerAccType,
															request_account_id   : dataNoti[0].meta_value,
															request_account_type : recivAccType,
															message              : "",
															flag                 : flag
													};

													HelperNotification.sendPushNotification(notiParams, function(retNoti){		

														return_result(1); 

													});
												});
											});
										});
									// });
								});
							}
							else
							{ return_result(0); }
						});
				}
				else
				{ return_result(0); }
			});
	}
	catch(err)
	{ console.log("requestCvStatus",err); return_result(0);}
};


methods.getPeopleListDetails =  function(arrParams,return_result){
	try
	{
		var arrResults = [];
		var arrObjects = {};

		var query = `SELECT * FROM jaze_job_people_list WHERE group_id IN (SELECT group_id FROM jaze_job_people_list WHERE meta_key = 'account_id' AND meta_value =?)`;

		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				methods.convertMultiMetaArray(data, function(retConverted){	
					var jsonItems = Object.values(retConverted);
					return_result(jsonItems[0].group_id);
 				});
	 	 	}
	 	 	else
	 	 	{
	 	 		return_result(null);
	 	 	}

 		});

	}
	catch(err)
	{ console.log("getCvOwnerDetails",err); return_result(null);}
};


methods.processNotificationStatus =  function(arrParams,return_result){
	try
	{
		var arrResults = [];
		var arrObjects = {};

		var query = `SELECT * FROM jaze_notification_list WHERE group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'account_id' AND meta_value =?) 
			AND group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'receiver_account_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_notification_list WHERE meta_key = 'status' AND meta_value ='1')`;

		DB.query(query,[arrParams.department_account_id,arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				methods.convertMultiMetaArray(data, function(retConverted){	
					var jsonItems = Object.values(retConverted);
					
					var updateQuer = `UPDATE jaze_notification_list SET meta_value ='0' WHERE meta_key = 'status' AND group_id =?`;

					DB.query(updateQuer,[jsonItems[0].group_id], function(data, error){

					 	if(parseInt(data.affectedRows) > 0){
					 		return_result(1);
				 	   	}else{
		   					return_result(0);
				 	   	}
					});
				
 				});
	 	 	}
	 	 	else
	 	 	{
	 	 		return_result(3);
	 	 	}

 		});

	}
	catch(err)
	{ console.log("getCvOwnerDetails",err); return_result(0);}
};

// request cv status -------------------- End //



// ---------------------------------------------- Job Manager Start -------------------------------------------------------- //


methods.getShortListGroupsDepartment =  function(arrParams,return_result){
	try
	{
		var arrResults = [];
		var arrObjects = {};

		var general = {
			group_id: 0,
			group_name: 'General'
		};

		var query = `SELECT * FROM jaze_job_shortlist_groups WHERE group_id IN (SELECT group_id FROM jaze_job_shortlist_groups WHERE meta_key='account_id' AND meta_value=?)`;

		DB.query(query,[arrParams.account_id], function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	
			
				methods.convertMultiMetaArray(data, function(retConverted){	

					retConverted.push(general);
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var arrActive = {group_id: jsonItems.group_id, account_id:arrParams.account_id};
							methods.getShortlistedAccountsForManager(arrActive, function(retAccounts){

								methods.getShortlistedAccountsActive(arrActive, function(retActiveCount){
					
									arrObjects = {
										department_id    : jsonItems.group_id.toString(),
										department_name  : jsonItems.group_name,
								
										candidates_count : retAccounts.toString(),
										active_count     : retActiveCount.toString()
									};

									counter -= 1;
									arrResults.push(arrObjects);
							
									if (counter === 0){ 
										resolve(arrResults);
									}

								});

							});

						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0].sort(sortingArrayObject('department_name')));
						}
					});

				});

	 	 	}
	 	 	else
	 	 	{
				var arrActive = {group_id: 0, account_id:arrParams.account_id};
				methods.getShortlistedAccountsForManager(arrActive, function(retAccounts){

					methods.getShortlistedAccountsActive(arrActive, function(retActiveCount){
		
						arrObjects = {
							department_id    : '0',
							department_name  : 'General',
							candidates_count : retAccounts.toString(),
							active_count     : retActiveCount.toString()
						};

	
						arrResults.push(arrObjects);
				
						return_result(arrResults.sort(sortingArrayObject('department_name')));

					});

				});
	 	 	}

 		});


	}
	catch(err)
	{ console.log("getShortListGroupsDepartment",err); return_result(null);}
};


methods.getShortlistedAccountsForManager =  function(arrParams,return_result){
	try
	{
		var arrResults = [];
		var arrObjects = {};

		var query = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'people_group_id' THEN meta_value END) AS people_group_id
			FROM  jaze_job_shortlisted_people
			WHERE  group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'department_id' AND FIND_IN_SET(?, meta_value) ) 
			AND  group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'account_id' AND meta_value =? ) 
			) AS t1 WHERE people_group_id != ''`;

		DB.query(query,[arrParams.group_id,arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				var counter = data.length;		
				return_result(counter);
	 	 	}
	 	 	else
	 	 	{
	 	 		return_result(0);
	 	 	}

 		});

	}
	catch(err)
	{ console.log("getShortlistedAccountsForManager",err); return_result(null);}
};

methods.getShortlistedAccountsActive =  function(arrParams,return_result){
	try
	{
		var arrResults = [];
		var arrObjects = {};

		var query = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'people_group_id' THEN meta_value END) AS people_group_id
			FROM  jaze_job_shortlisted_people
			WHERE  group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'department_id' AND FIND_IN_SET(?, meta_value) ) 
			AND  group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'shortlisted_type' AND meta_value = '1' ) 
			AND  group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'account_id' AND meta_value =? ) 
			) AS t1 WHERE people_group_id != ''`;

		DB.query(query,[arrParams.group_id,arrParams.account_id], function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var counter = data.length;		
				var jsonItems = Object.values(data);	
				return_result(counter);

	 	 	}
	 	 	else
	 	 	{
	 	 		return_result(0);
	 	 	}

 		});


	}
	catch(err)
	{ console.log("getShortlistedAccountsActive",err); return_result(null);}
};


methods.getShortListGroupsPeople =  function(arrParams,return_result){
	try
	{
		var arrResults = [];
		var arrObjects = {};

		var query = `SELECT * FROM jaze_job_shortlisted_people WHERE group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key='account_id' AND meta_value=?)
		AND group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'department_id' AND FIND_IN_SET(?, meta_value) )`;

		// if(parseInt(arrParams.flag) === 1){
		// 	query += `AND group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'shortlisted_type' AND meta_value ='1' )`;
		// }else if(parseInt(arrParams.flag) === 2){
		// 	query += `AND group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'shortlisted_type' AND meta_value ='2' )`;
		// }else if(parseInt(arrParams.flag) === 3){
		// 	query += `AND group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'shortlisted_type' AND meta_value ='0' )`;
		// }

		DB.query(query,[arrParams.account_id,arrParams.department_id], function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getShortlistPeopleDetails(jsonItems.people_group_id, function(retAccounts){

								// var status = "Maybe";
								// if(parseInt(jsonItems.shortlisted_type) === 1){
								// 	status = "Active";
								// }else if(parseInt(jsonItems.shortlisted_type) === 0){
								// 	status = "Rejected";
								// }
					
								arrObjects = {
									account_group_id : jsonItems.people_group_id.toString(),
									account_id       : retAccounts.account_id,
									name             : retAccounts.name,
									logo             : retAccounts.logo,
									shortlist_type   : jsonItems.shortlisted_type.toString(),
									date             : formatDate(jsonItems.create_date)
								};

								counter -= 1;
								arrResults.push(arrObjects);
						
								if (counter === 0){ 
									resolve(arrResults);
								}

							});

						})
					}],
					function (result, current) {
						if (counter === 0){ 
	
							result[0].sort(function(a, b) {
								a = new Date(a.date);
								b = new Date(b.date);
								return a>b ? -1 : a<b ? 1 : 0;
							});

							return_result(result[0]);

						}
					});

				});

	 	 	}
	 	 	else
	 	 	{
	 	 		return_result(null);
	 	 	}

 		});


	}
	catch(err)
	{ console.log("getShortListGroupsPeople",err); return_result(null);}
};



methods.getShortlistPeopleDetails =  function(people_group_id,return_result){
	try
	{
		var arrResults = [];
		var arrObjects = {};

		var query = `SELECT * FROM jaze_job_people_list WHERE group_id=?`;

		DB.query(query,[people_group_id], function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var logo = "";
							if(jsonItems.profile_logo){
								logo = G_STATIC_IMG_URL+"uploads/jazenet/individual_account/jobs/"+jsonItems.profile_logo;
							}
							arrObjects = {
								account_id : jsonItems.account_id.toString(),
								name       : jsonItems.first_name+" "+jsonItems.last_name,
								logo       : logo,
							};

							counter -= 1;
							resolve(arrObjects);

						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						
						}
					});

				});

	 	 	}
	 	 	else
	 	 	{
	 	 		return_result(null);
	 	 	}

 		});


	}
	catch(err)
	{ console.log("getShortlistPeopleDetails",err); return_result(null);}
};



methods.updateShortListStatus = function(arrParams,return_result){

	try
	{
			
		var query = `SELECT * FROM jaze_job_shortlisted_people WHERE group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key='account_id' AND meta_value=?)
			AND group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'department_id' AND FIND_IN_SET(?, meta_value) )	
			AND group_id IN (SELECT group_id FROM jaze_job_shortlisted_people WHERE meta_key = 'people_group_id' AND meta_value = ? )`;

		DB.query(query,[arrParams.account_id,arrParams.department_id,arrParams.account_group_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
 				
				var	arrUpdate = {
					shortlisted_type : arrParams.status,
					create_date      : standardDT,
				};

				var updateQue = `update jaze_job_shortlisted_people SET meta_value =? WHERE meta_key =? AND group_id =?`;
				
				for (var key in arrUpdate) 
			 	{	
				     if(arrUpdate[key] !== null && arrUpdate[key] != ""){
						DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(data, error){
					 		if(data !== null){
				 				return_result(1);
					 	   	}else{
				 	   			return_result(0);
					 	   	}
						});
					}
				}

	 	   	}
	 	   	else
	 	   	{
	 	   		return_result(2);
	 	   	}

 	 	});

	
	}
	catch(err)
	{ 
		console.log("updateLibrary",err); return_result(0); 
	}	
};

// ---------------------------------------------- Job Manager End -------------------------------------------------------- //



// ----------------------------- search employer start  ---------------------------------------------- //


methods.searchEmployer = function(arrParams,return_result)
{

	try
	{
		var arrCategory = [];
		var arrGroupID  = [];

		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };

	    let category_id = '823';

		if(parseInt(arrParams.category_id) !== 0){
  			category_id = arrParams.category_id;
  		}

	  	if(arrParams.attributes.length > 0)
	  	{

			var query = `SELECT * FROM jaze_category_list WHERE group_id =?`;

			DB.query(query, [category_id], function(mainCat, error){
	

				if(typeof mainCat !== 'undefined' && mainCat !== null && parseInt(mainCat.length) > 0)
				{	

					methods.convertMultiMetaArray(mainCat, function(retMainCat){

						var querSubCat = "SELECT * FROM jaze_category_attributes_groups WHERE group_id IN ";
							querSubCat += " (SELECT group_id FROM jaze_category_attributes_groups WHERE meta_key = 'cat_id' AND ( meta_value = '"+retMainCat[0].group_id+"' OR  meta_value = '"+retMainCat[0].parent_id+"') )";
						DB.query(querSubCat, null, function(subCat, error){

							if(typeof subCat !== 'undefined' && subCat !== null && parseInt(subCat.length) > 0)
							{	
								
								var counter = arrParams.attributes.length;		
								var jsonItems = Object.values(arrParams.attributes);

		

								promiseForeach.each(jsonItems,[function (jsonItems) {
									return new Promise(function (resolve, reject) {		
						
								  		var parameter = '';
								  		var childQuer = '';
							  		
										if(parseInt(jsonItems.type) >= 1 &&  parseInt(jsonItems.type) <= 6)
										{	
											var title = jsonItems.value.toLowerCase();
											if(jsonItems.value.toLowerCase() != ''){
												title = jsonItems.value.toLowerCase().split(",");
											}
											parameter = title;
											childQuer = "SELECT * FROM jaze_job_post_job_attributes WHERE meta_key=? AND meta_value IN (?)";
										}
										else if(parseInt(jsonItems.type) == 7)
										{	
											childQuer = "SELECT * FROM jaze_job_post_job_attributes WHERE meta_key=? ";
											childQuer += " AND ( SUBSTRING_INDEX(meta_value, '-', 1) >='"+jsonItems.min_value+"' AND  SUBSTRING_INDEX(meta_value, '-', 2) <='"+jsonItems.max_value+"' )";
										}
										else if(parseInt(jsonItems.type) == 8)
										{
											parameter = jsonItems.value;
											childQuer = "SELECT * FROM jaze_job_post_job_attributes WHERE meta_key=? AND meta_value =? ";
										}
										else if(parseInt(jsonItems.type) == 9 || parseInt(jsonItems.type) == 10)
										{
											childQuer = "SELECT * FROM jaze_job_post_job_attributes WHERE meta_key=? AND LOWER(meta_value) LIKE '%" +jsonItems.value.toLowerCase()+ "%' ";
										}
							
										DB.query(childQuer,[jsonItems.attribute_id,parameter], function(child, error){

											if(typeof child !== 'undefined' && child !== null && parseInt(child.length) > 0)
											{	
												methods.convertMultiMetaArray(child, function(retChild){

													retChild.forEach(function(val) {
														arrGroupID.push(val.group_id.toString());
													});

													counter -= 1;
													if (counter === 0){ 
														resolve(arrGroupID);
													}
												});
											}
											else
											{
												counter -= 1;
												if (counter === 0){ 
													resolve(arrGroupID);
												}
											}
										});

									})
								}],
								function (result, current) {
									if (counter === 0){ 
										if(parseInt(result[0].length) > 0)
										{
											methods.searchEmployerForPeopleIn(result[0],arrParams, function(retCompany,pagination_details){	
												if(retCompany!=null){
													return_result(retCompany,pagination_details);
												}else{
													return_result(null,pagination_details);
												}
											});
										}
										else
										{
											return_result(null,pagination_details);
										}
									}
								});

							}
							else
							{
								return_result(null,pagination_details);
							}

						});
					});

				}
				else
				{
					return_result(null,pagination_details);
				}

			});
		}
		else
		{

			let group_id;

			var query = `SELECT * FROM jaze_category_list WHERE group_id =?`;

			DB.query(query, [category_id], function(mainCat, error){

				if(typeof mainCat !== 'undefined' && mainCat !== null && parseInt(mainCat.length) > 0)
				{	

					methods.convertMultiMetaArray(mainCat, function(retMainCat){

						group_id = retMainCat[0].group_id;

						methods.searchEmployerForPeople(group_id,arrParams, function(retCompany,pagination_details){	

							if(retCompany!=null){
								return_result(retCompany,pagination_details);
							}else{
								return_result(null,pagination_details);
							}
						
						});

					});


				}else{
					return_result(null,pagination_details);
				}

			});	

		}

	}
	catch(err)
	{ 
		console.log("searchEmployerForPeopleIn",err); 
		return_result(null,pagination_details); 
	}	

};


methods.searchEmployerForPeopleIn = function(arrGroupId,arrParams, return_result)
{
		
	try
	{

		var arrAccounts = [];
		var arrObjects  = {};

		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };


		var querSubCat = `SELECT * FROM jaze_job_post_job WHERE group_id IN (?)
		 				AND group_id IN (SELECT group_id FROM jaze_job_post_job WHERE meta_key = 'status' AND meta_value ='1') `;

		if(parseInt(arrParams.country_id) !==0 ){
			querSubCat += " AND group_id IN (SELECT group_id FROM jaze_job_post_job WHERE meta_key = 'country' AND  meta_value ='"+arrParams.country_id+"' ) ";
		}

		if(parseInt(arrParams.state_id) !==0 ){
			querSubCat += " AND group_id IN (SELECT group_id FROM jaze_job_post_job WHERE meta_key = 'state' AND   meta_value ='"+arrParams.state_id+"'  ) ";
		}

		if(parseInt(arrParams.city_id) !==0 ){
			querSubCat += " AND group_id IN (SELECT group_id FROM jaze_job_post_job WHERE meta_key = 'city' AND   meta_value ='"+arrParams.city_id+"'  ) ";
		}


		DB.query(querSubCat, [arrGroupId], function(data, error){
	
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParams = {account_id:jsonItems.account_id};
							methods.getCompanyProfile(accParams, function(retProfile){	

								if(retProfile!=null)
								{

									methods.__getCountryDetails(jsonItems.country,function(retCountry){

										methods.__getStateDetails(jsonItems.state,function(retState){

											methods.__getCityDetails(jsonItems.city,function(retCity){

												methods.getEmployerAttributes(jsonItems.group_id,function(retAttri){

													var applyParams = {account_id:arrParams.account_id, group_id:jsonItems.group_id};
													methods.getApplyStatus(applyParams,function(retAppStatus){
														
														var start_date = "";
														var end_date = "";
														var start_time = "";
														var end_time = "";
													
														var int_type = "0";

														if(jsonItems.start_date && jsonItems.start_time && jsonItems.end_date && jsonItems.end_time){
															
															var job_start = dateFormat(jsonItems.start_date+' '+jsonItems.start_time,'yyyy-mm-dd HH:MM:ss');
															var job_end = dateFormat(jsonItems.end_date+' '+jsonItems.end_time,'yyyy-mm-dd HH:MM:ss');
												

															if( (new Date(standardDT).getTime() >= new Date(job_start).getTime()) && (new Date(standardDT).getTime() <= new Date(job_end).getTime())  ){

																int_type = "1";

																start_date = jsonItems.start_date;
																end_date   = jsonItems.end_date;
																start_time = jsonItems.start_time;
																end_time   = jsonItems.end_time;

															}
														}
		
														arrObjects = {
															group_id     : jsonItems.group_id.toString(),
															job_category : jsonItems.job_category.toString(),
															account_id   : jsonItems.account_id.toString(),
															name         : retProfile.name,
															logo         : retProfile.logo,
															country_name : retCountry,
															state_name   : retState,
															city_name    : retCity,
															job_title    : jsonItems.job_title,
															job_description : jsonItems.job_description,
															currency     : retProfile.currency,
															salary       : retAttri.monthly_salary,
															interview_type : int_type,
															start_date     : start_date,
															end_date       : end_date,
															start_time     : start_time,
															end_time       : end_time,
															create_date  : dateFormat(jsonItems.create_date,'yyyy-mm-dd HH:MM:ss'),	
															apply_status : retAppStatus
											
														}

													
														counter -= 1;
														arrAccounts.push(arrObjects);

														if (counter === 0){ 
															resolve(arrAccounts);
														}
				
													
													});
												});
											});
										});
									});
								}

							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							var newArray = [];
							var total_result = result[0].length.toString();
							var total_page = 0;

							if(parseInt(result[0].length) > 50){
								newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
								total_page = Math.ceil((parseInt(result[0].length) - 50 ) / 50);
							}else{
								newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
								total_page = 0;
							}

							var next_page = 0;
							if(parseInt(total_page) !== 0){
								next_page  = parseInt(arrParams.page)+1;
							}

		                 	if(parseInt(total_page) >= parseInt(arrParams.page)){

								pagination_details  = {
									total_page   : total_page.toString(),
					                current_page : arrParams.page.toString(),
					                next_page    : next_page.toString(),
					                total_result : total_result
					            };

	                    		newArray.sort(function(a, b) {
									a = new Date(a.create_date);
									b = new Date(b.create_date);
									return a>b ? -1 : a<b ? 1 : 0;
								});

							  	return_result(newArray,pagination_details);

							  	//return_result(newArray.sort(sortingArrayObject('first_name')),pagination_details);
		
				            }else{
			            		return_result(null,pagination_details);
				            }	

						}
					});

				});
			}
			else
			{
				return_result(null,pagination_details);
			}


		});

	}
	catch(err)
	{ 
		console.log("searchEmployerForPeopleIn",err); 
		return_result(null,pagination_details); 
	}	

};

methods.searchEmployerForPeople = function(category_id,arrParams, return_result)
{
		
	try
	{

		var arrAccounts = [];
		var arrObjects = {};

		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };


		var querSubCat = `SELECT * FROM jaze_job_post_job WHERE group_id IN (SELECT group_id FROM jaze_job_post_job WHERE meta_key = 'job_category' AND meta_value =?)
		 				AND group_id IN (SELECT group_id FROM jaze_job_post_job WHERE meta_key = 'status' AND meta_value ='1')  `;

		if(parseInt(arrParams.country_id) !==0 ){
			querSubCat += " AND group_id IN (SELECT group_id FROM jaze_job_post_job WHERE meta_key = 'country' AND  meta_value ='"+arrParams.country_id+"' ) ";
		}

		if(parseInt(arrParams.state_id) !==0 ){
			querSubCat += " AND group_id IN (SELECT group_id FROM jaze_job_post_job WHERE meta_key = 'state' AND   meta_value ='"+arrParams.state_id+"'  ) ";
		}

		if(parseInt(arrParams.city_id) !==0 ){
			querSubCat += " AND group_id IN (SELECT group_id FROM jaze_job_post_job WHERE meta_key = 'city' AND   meta_value ='"+arrParams.city_id+"'  ) ";
		}


		DB.query(querSubCat, [category_id], function(data, error){
	
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				methods.convertMultiMetaArray(data, function(retConverted){	

					//return_result(retConverted,pagination_details);

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParams = {account_id:jsonItems.account_id};
							methods.getCompanyProfile(accParams, function(retProfile){	

								if(retProfile!=null)
								{

									methods.__getCountryDetails(jsonItems.country,function(retCountry){

										methods.__getStateDetails(jsonItems.state,function(retState){

											methods.__getCityDetails(jsonItems.city,function(retCity){

												methods.getEmployerAttributes(jsonItems.group_id,function(retAttri){



													var applyParams = {account_id:arrParams.account_id, group_id:jsonItems.group_id};
													methods.getApplyStatus(applyParams,function(retAppStatus){

														var start_date = "";
														var end_date = "";
														var start_time = "";
														var end_time = "";
													
														var int_type = "0";

														if(jsonItems.start_date && jsonItems.start_time && jsonItems.end_date && jsonItems.end_time){
															
															var job_start = dateFormat(jsonItems.start_date+' '+jsonItems.start_time,'yyyy-mm-dd HH:MM:ss');
															var job_end = dateFormat(jsonItems.end_date+' '+jsonItems.end_time,'yyyy-mm-dd HH:MM:ss');
												

															if( (new Date(standardDT).getTime() >= new Date(job_start).getTime()) && (new Date(standardDT).getTime() <= new Date(job_end).getTime())  ){

																int_type = "1";

																start_date = jsonItems.start_date;
																end_date   = jsonItems.end_date;
																start_time = jsonItems.start_time;
																end_time   = jsonItems.end_time;

															}
														}
		
														arrObjects = {
															group_id     : jsonItems.group_id.toString(),
															job_category : jsonItems.job_category.toString(),
															account_id   : jsonItems.account_id.toString(),
															name         : retProfile.name,
															logo         : retProfile.logo,
															country_name : retCountry,
															state_name   : retState,
															city_name    : retCity,
															job_title    : jsonItems.job_title,
															job_description : jsonItems.job_description,
															currency     : retProfile.currency,
															salary       : retAttri.monthly_salary,
															interview_type : int_type,
															start_date     : start_date,
															end_date       : end_date,
															start_time     : start_time,
															end_time       : end_time,
															create_date  : dateFormat(jsonItems.create_date,'yyyy-mm-dd HH:MM:ss'),	
															apply_status : retAppStatus
											
														}
													
														counter -= 1;
														arrAccounts.push(arrObjects);

														if (counter === 0){ 
															resolve(arrAccounts);
														}
				
													
													});
												});
											});
										});
									});
								}
							
							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
							var newArray = [];
							var total_result = result[0].length.toString();
							var total_page = 0;

							if(parseInt(result[0].length) > 50){
								newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
								total_page = Math.ceil((parseInt(result[0].length) - 50 ) / 50);
							}else{
								newArray   = result[0].slice(Math.ceil( parseInt(arrParams.page) * 50 ), (parseInt(arrParams.page) + 1) * 50);
								total_page = 0;
							}

							var next_page = 0;
							if(parseInt(total_page) !== 0){
								next_page  = parseInt(arrParams.page)+1;
							}

			                 if(parseInt(total_page) >= parseInt(arrParams.page)){

								pagination_details  = {
									total_page   : total_page.toString(),
					                current_page : arrParams.page.toString(),
					                next_page    : next_page.toString(),
					                total_result : total_result
					            };

			            		newArray.sort(function(a, b) {
									a = new Date(a.create_date);
									b = new Date(b.create_date);
									return a>b ? -1 : a<b ? 1 : 0;
								});

							  	return_result(newArray,pagination_details);
		
				            }else{

			            		return_result(null,pagination_details);

				            }	

						}
					});

				});
			}
			else
			{
				return_result(null,pagination_details);
			}


		});

	}
	catch(err)
	{ 
		console.log("searchEmployerForPeople",err); 
		return_result(null,pagination_details); 
	}	

};




methods.getEmployerAttributes = function(arrGroupID,return_result){
	try
	{
		var query = "SELECT * FROM  jaze_job_post_job_attributes WHERE group_id=?";
		if(parseInt(arrGroupID.length) > 0)
		{
			query = "SELECT * FROM  jaze_job_post_job_attributes WHERE group_id IN(?)";
		}

		var obj_values = {
			employment_type  : '',
			monthly_salary   : '',
			career_level     : '',
			min_exp          : '',
			min_edu          : '',
		};
	
		DB.query(query,[arrGroupID], function(detail, error){

			if(typeof detail !== 'undefined' && detail !== null && parseInt(detail.length) > 0)
			{	
			
					
				detail.forEach(function(value) {
					if(value.meta_key == '10'){
						obj_values.monthly_salary = value.meta_value;
					}
					if(value.meta_key == '11'){
						obj_values.career_level = value.meta_value;
					}
					if(value.meta_key == '12'){
						obj_values.min_exp = value.meta_value;
					}
					if(value.meta_key == '13'){
						obj_values.min_edu = value.meta_value;
					}
					if(value.meta_key == '14'){
						obj_values.employment_type = value.meta_value;
					}
				
				});


				return_result(obj_values);
			}
			else
			{
				return_result(obj_values);
			}
		});
	
	}
	catch(err)
	{ console.log("getEmployerAttributes",err); return_result(obj_values);}

};



methods.getBasicJobProfileStatus = function(account_id,return_result){

	try
	{

		var qry = `SELECT * FROM jaze_job_basic_profile WHERE group_id IN (SELECT group_id FROM jaze_job_basic_profile WHERE meta_key = 'account_id' AND meta_value =?)`;
		DB.query(qry, [account_id], function(data, error){	
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){	
				return_result("1");
	 		}else{
				return_result("0");
	 	   	}
		});

	}
	catch(err)
	{ console.log("getBasicJobProfileStatus",err); return_result("0");}
};


methods.getCvQualificationStatus = function(account_id,return_result){

	try
	{
		var qry = `SELECT * FROM jaze_job_cv_qualification_details WHERE group_id IN (SELECT group_id FROM jaze_job_cv_qualification_details WHERE meta_key = 'account_id' AND meta_value =?)`;
		DB.query(qry, [account_id], function(data, error){	
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){	
				return_result("1");
	 		}else{
				return_result("0");
	 	   	}
		});
	}
	catch(err)
	{ console.log("getCvQualificationstatus",err); return_result("0");}
};


// --------------------------- search employer end ------------------------------------------ //


// --------------------------- search details start  ------------------------------------------ //

methods.getEmployerJobDetails =  function(arrParams,return_result){
	try
	{

		var arrDetails = [];

		var query = "SELECT * FROM jaze_job_post_job WHERE group_id=?";

		DB.query(query,[arrParams.group_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	

					var query = "SELECT * FROM  jaze_job_post_job_attributes WHERE group_id=?";
					DB.query(query,[retConverted[0].group_id], function(detail, error){

						if(typeof detail !== 'undefined' && detail !== null && parseInt(detail.length) > 0)
						{
							
							var accParams = {account_id: retConverted[0].account_id};

							methods.getCompanyProfile(accParams, function(retProfile){

								if(retProfile!=null){	

									methods.__getCountryDetails(retConverted[0].country,function(retCountry){

										methods.__getStateDetails(retConverted[0].state,function(retState){

											methods.__getCityDetails(retConverted[0].city,function(retCity){

												methods.getApplyStatus(arrParams,function(retAppStatus){

													methods.getQuestionStatus(retConverted[0].group_id,function(retQuestion){


														methods.getBasicJobProfileStatus(arrParams.account_id,function(retJobProfile){

															methods.getCvQualificationStatus(arrParams.account_id,function(retCvStatus){
		
																var obj_employer = {
																	//group_id     : retConverted[0].group_id.toString(),
																	// job_category : retConverted[0].job_category.toString(),
																	account_id   : retConverted[0].account_id.toString(),
																	name         : retProfile.name,
																	logo         : retProfile.logo,
																	country_name : retCountry,
																	state_name   : retState,
																	city_name    : retCity,
																	job_title    : retConverted[0].job_title,
																	date         : retConverted[0].create_date
																};

																var obj_values = {
																	career_level     : '',
																	commitment       : '',
																	experience       : '',
																	education        : '',
																	//company_size     : retConverted[0].company_size,
																	salary           : '',
																	join_period      : retConverted[0].joining_period,
																	hirinng_process  : retConverted[0].hiring_process
											
																};

																detail.forEach(function(value) {
																	if(value.meta_key == '10'){
																		obj_values.salary = value.meta_value;
																	}
																	if(value.meta_key == '11'){
																		obj_values.career_level = value.meta_value;
																	}
																	if(value.meta_key == '12'){
																		obj_values.experience = value.meta_value;
																	}
																	if(value.meta_key == '13'){
																		obj_values.education = value.meta_value;
																	}
																	if(value.meta_key == '14'){
																		obj_values.commitment = value.meta_value;
																	}
																
																});

															
																var arrRespon = [];
																var arrReq    = [];
																var description = "";

																if(retConverted[0].job_description){
																	description = entities.encode(retConverted[0].job_description);
																}
															
																if(retConverted[0].responsibilities){
																	arrRespon    = retConverted[0].responsibilities.split("|");
																}

																if(retConverted[0].requirements){
																	arrReq    = retConverted[0].requirements.split("|");
																}

																var start_date = "";
																var end_date = "";
																var start_time = "";
																var end_time = "";
															
																var int_type = "0";

																if(retConverted[0].start_date && retConverted[0].start_time && retConverted[0].end_date && retConverted[0].end_time){
																	
																	var job_start = dateFormat(retConverted[0].start_date+' '+retConverted[0].start_time,'yyyy-mm-dd HH:MM:ss');
																	var job_end = dateFormat(retConverted[0].end_date+' '+retConverted[0].end_time,'yyyy-mm-dd HH:MM:ss');
														

																	if( (new Date(standardDT).getTime() >= new Date(job_start).getTime()) && (new Date(standardDT).getTime() <= new Date(job_end).getTime())  ){

																		int_type = "1";

																		start_date = retConverted[0].start_date;
																		end_date   = retConverted[0].end_date;
																		start_time = retConverted[0].start_time;
																		end_time   = retConverted[0].end_time;

																	}
																}

																var main_obj = {
																	basic_details    : obj_employer,
																	job_details      : obj_values,
																	description      : description,
																	requirements     : arrReq,
																	responsibility   : arrRespon,
																	apply_status     : retAppStatus,
																	question_status  : retQuestion,
																	cv_status        : retCvStatus,
																	profile_status   : retJobProfile,
																	interview_type : int_type,
																	start_date     : start_date,
																	end_date       : end_date,
																	start_time     : start_time,
																	end_time       : end_time,
																	create_date  : dateFormat(retConverted[0].create_date,'yyyy-mm-dd HH:MM:ss'),	
																}
											
																return_result(main_obj);

															});
														});
													});
												});
											});
										});

									});
								}
					
							});
					
						}else{
							return_result(null);
						}

					});
				});

			}
			else
			{ return_result(null); }

		});

	}
	catch(err)
	{ console.log("getEmployerJobDetails",err); return_result(null);}
};



// --------------------------- job questions start  ------------------------------------------ //

methods.getEmployerQuestions = function(arrParams,return_result){


	var qry = `SELECT * FROM jaze_job_post_job_questions WHERE group_id IN (SELECT group_id FROM jaze_job_post_job_questions WHERE meta_key = 'parent_id' AND meta_value =?)`;

	DB.query(qry, [arrParams.group_id], function(data, error){	

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			var arrObjects = {};
			var arrResults = [];

			methods.convertMultiMetaArray(data, function(retConverted){	

				var counter = retConverted.length;		
				var jsonItems = Object.values(retConverted);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {		
						
						var mandatory = "1"
						if(jsonItems.answer !== 'required'){
							mandatory = "0";
						}

						var type = jsonItems.type;
						if(jsonItems.type == "yes/no"){
							type = "single choose";
						}

						arrObjects = {
							id        : jsonItems.group_id.toString(),
							question  : jsonItems.question,
							type      : type,
							mandatory : mandatory
						};

						arrResults.push(arrObjects);
						counter -= 1;
						if (counter === 0){ 
							resolve(arrResults);
						}
					})
				}],
				function (result, current) {
					if (counter === 0){ 
						return_result(result[0]);
					}
				});

			});

 		}
 		else
 		{
			return_result(null);
 	   	}


	});


};


// --------------------------- job questions end  ------------------------------------------ //

methods.getQuestionStatus = function(group_id,return_result){

	try{

		var qry = `SELECT * FROM jaze_job_post_job_questions WHERE 
			group_id IN (SELECT group_id FROM jaze_job_post_job_questions WHERE meta_key = 'parent_id' AND meta_value =?) 
			AND group_id IN (SELECT group_id FROM jaze_job_post_job_questions WHERE meta_key = 'status' AND meta_value ='1')`;

		DB.query(qry, [group_id], function(data, error){

 	  		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
				return_result('1');
	 	   	}else{
				return_result('0');
	 	   	}

		});

	}
	catch(err)
	{ console.log("getQuestionStatus",err); return_result('0');}

};


methods.getApplyStatus = function(arrParams,return_result){

	try{

		if(parseInt(arrParams.account_id) !== 0){

			var qry = `SELECT * FROM jaze_job_post_job_candidates WHERE 
				group_id IN (SELECT group_id FROM jaze_job_post_job_candidates WHERE meta_key = 'account_id' AND meta_value =?) 
				AND group_id IN (SELECT group_id FROM jaze_job_post_job_candidates WHERE meta_key = 'job_id' AND meta_value =?)`;

			DB.query(qry, [arrParams.account_id,arrParams.group_id], function(data, error){

	 	  		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result('1');
		 	   	}else{
					return_result('0');
		 	   	}

			});

		}else{
			return_result('0');
		}

	}
	catch(err)
	{ console.log("getApplyStatus",err); return_result('0');}

};


// --------------------------- search details start  ------------------------------------------ //



// --------------------------- apply job  start  ------------------------------------------ //


methods.applyForJobVacancy = function(arrParams,return_result){


	var qry = `SELECT * FROM jaze_job_basic_profile WHERE group_id IN (SELECT group_id FROM jaze_job_basic_profile WHERE meta_key = 'account_id' AND meta_value =?)`;

	DB.query(qry, [arrParams.account_id], function(data, error){	

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	

			var objInsert = {};
			var counter = data.length;		
			var jsonItems = Object.values(data);

			promiseForeach.each(jsonItems,[function (jsonItems) {
				return new Promise(function (resolve, reject) {		
			
					objInsert[jsonItems.meta_key]  = jsonItems.meta_value;

		
					counter -= 1;
					if (counter === 0){ 
						resolve(objInsert);
					}
				})
			}],
			function (result, current) {
				if (counter === 0){ 
					
					var qry = `SELECT * FROM jaze_job_post_job_candidates WHERE 
						group_id IN (SELECT group_id FROM jaze_job_post_job_candidates WHERE meta_key = 'account_id' AND meta_value =?) 
						AND group_id IN (SELECT group_id FROM jaze_job_post_job_candidates WHERE meta_key = 'job_id' AND meta_value =?)`;

					DB.query(qry, [arrParams.account_id,arrParams.group_id], function(jdata, error){

				  		if(typeof jdata !== 'undefined' && jdata !== null && parseInt(jdata.length) > 0)
				  		{
							return_result(3);
				 	   	}
				 	   	else
				 	   	{
							
							var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_job_post_job_candidates";
				  			var lastGrpId = 0;

							methods.getLastGroupId(queGroupMax, function(queGrpRes){

								var plusOneGroupID = queGrpRes[0].lastGrpId+1;

								var arrQuestions = [];
								var objQuestions = {};
								var questionValue = "";

							  	if(arrParams.questions.length > 0)
	  							{
	  								arrParams.questions.forEach(function(val){
	  									objQuestions[val.id]  = val.value;
	  									arrQuestions.push(objQuestions);
	  								});
	  								questionValue = JSON.stringify(arrQuestions[0]);
  								}

								var arrInsert = {
									account_id       : arrParams.account_id,
									shortlisted_type : "-1",
									questions        : questionValue,
									candidate_details : JSON.stringify(result[0]),
									job_id           : arrParams.group_id,
									create_date      : standardDT,
									status:1
								};

	
								var querInsert = `insert into jaze_job_post_job_candidates (group_id, meta_key, meta_value) VALUES (?,?,?)`;

								//console.log(arrInsert);

							 	for (var key in arrInsert) 
							 	{
									DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
							 		 	if(parseInt(data.affectedRows) > 0){
					 						return_result(1);
								 	   	}else{
						   					return_result(0);
								 	   	}
							 	 	});
							  	};


				  			});

				 	   	}

					});


				}
				
			});

 		}
 		else
 		{
			return_result(2);
 	   	}


	});


};



methods.cancelApplyForJobVacancy = function(arrParams,return_result){

	try
	{
		var qry = `SELECT * FROM jaze_job_post_job_candidates WHERE 
			group_id IN (SELECT group_id FROM jaze_job_post_job_candidates WHERE meta_key = 'account_id' AND meta_value =?) 
			AND group_id IN (SELECT group_id FROM jaze_job_post_job_candidates WHERE meta_key = 'job_id' AND meta_value =?)`;

		DB.query(qry, [arrParams.account_id,arrParams.group_id], function(jdata, error){

	  		if(typeof jdata !== 'undefined' && jdata !== null && parseInt(jdata.length) > 0)
	  		{	

				var queDel = `DELETE FROM jaze_job_post_job_candidates where group_id =?`;
				DB.query(queDel,[jdata[0].group_id], function(data, error){

				 	if(parseInt(data.affectedRows) > 0){
				 		return_result(1);
			 		}else{
			 			return_result(0);
			 		}

				});

	 	   	}
	 	   	else
	 	   	{
				return_result(3);
	 	   	}

		});
		
	}
	catch(err)
	{ console.log("cancelApplyForJobVacancy",err); return_result(null);}

};


// --------------------------- apply job  end  ------------------------------------------ //


//----------------------------- view account cv ---------------------------------------//

methods.viewAccountCv = function(arrParams,return_result){

	try
	{

		var qry = `SELECT * FROM jaze_job_people_list WHERE group_id IN (SELECT group_id FROM jaze_job_people_list WHERE meta_key='account_id' AND meta_value =?)`;

		DB.query(qry, [arrParams.request_account_id], function(data, error){	

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
					
							methods.getCvBasicDetails(jsonItems.account_id,function(retBasic){

								methods.__getNationality(jsonItems.nationality,function(retNational){

									methods.getBasicJobProfile(jsonItems.account_id,function(retBasicJob){

										methods.getJobLanguages(jsonItems.account_id,function(retLanguages){

											methods.getPreviousCompany(jsonItems.account_id,function(retPrevRole){

												methods.getCvQualifications(jsonItems.account_id,function(retQualification){

													methods.getCvAboutDetails(jsonItems.account_id,function(retAbout){

														var accParam = { account_id: jsonItems.account_id };
														methods.getCompanyProfile(accParam,function(retUserBasic){

															if(retUserBasic!=null)
															{	

																var profile_logo = retUserBasic.logo;
																if(jsonItems.profile_logo){
																	var profile_logo = G_STATIC_IMG_URL+"uploads/jazenet/individual_account/jobs/"+jsonItems.account_id+"/"+jsonItems.profile_logo;
																}

																var obj_basic_details = {
																	account_group_id   : jsonItems.group_id.toString(),
																	account_id         : jsonItems.account_id.toString(),
																	first_name         : jsonItems.first_name,
																	last_name          : jsonItems.last_name,
																	professional_title : jsonItems.professional_title,						
																	country_name       : retUserBasic.country_name,
																	city_name          : retUserBasic.city_name,
																	logo               : profile_logo,
																	gender             : jsonItems.gender,
																	dob                : retBasic.dob,
																	nationality        : retNational,
																	current_location_country : retBasic.current_country,
																	current_location_city    : retBasic.current_city,													
																	commitment         : retBasicJob.commitment,
																	education          : retBasicJob.education_level,
																	experience         : retBasicJob. work_experience,
																	notice_period      : retBasicJob.notice_period,
																	expectation        : retBasicJob.expectation
																};									

															
																var main_obj = {

																	basic_details     : obj_basic_details,
																	languages         : retLanguages,
																	most_recent_role  : retPrevRole,
																	qualifications    : retQualification,
																	skills            : retAbout.skills,
																	about_me          : {
																		description : retAbout.about
																	},
																}

															
																counter -= 1;
																if (counter === 0){ 
																	resolve(main_obj);
																}

															}

														});
													});

												});
											});
										});
									});
								});
							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});

				});

	 		}
	 		else
	 		{
				return_result(null);
	 	   	}

		});

	}
	catch(err)
	{ console.log("viewAccountCv",err); return_result(null);}
};



methods.getCvBasicDetails = function(account_id,return_result){

	try
	{

		var qry = `SELECT * FROM jaze_job_cv_basic_details WHERE group_id IN (SELECT group_id FROM jaze_job_cv_basic_details WHERE meta_key = 'account_id' AND meta_value =?)`;

		var main_obj = {
			current_country    : '',
			current_city       : '',
			profile_logo       : '',
			dob                : '',
		}

		DB.query(qry, [account_id], function(data, error){	

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);


					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
				
							methods.__getCountryDetails(jsonItems.current_country, function(retCountry){

								methods.__getCityDetails(jsonItems.city, function(retCity){
					
									main_obj = {

										current_country    : retCountry,
										current_city       : retCity,
										profile_logo       : jsonItems.profile_logo,
										dob                : jsonItems.dob
				
									}

									counter -= 1;
									if (counter === 0){ 
										resolve(main_obj);
									}

								});
							});
						
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});

				});

	 		}
	 		else
	 		{
				return_result(main_obj);
	 	   	}

		});

	}
	catch(err)
	{ console.log("getCvBasicDetails",err); return_result(main_obj);}
};


methods.getBasicJobProfile = function(account_id,return_result){

	try
	{

		var qry = `SELECT * FROM jaze_job_basic_profile WHERE group_id IN (SELECT group_id FROM jaze_job_basic_profile WHERE meta_key = 'account_id' AND meta_value =?)`;

		var main_obj = {
			commitment         : '',
			work_experience    : '',
			education_level    : '',
			notice_period      : '',
			expectation        : '',
		}


		DB.query(qry, [account_id], function(data, error){	

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);


					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
				
							main_obj = {
								commitment         : jsonItems.commitment,
								work_experience    : jsonItems.work_experience,
								education_level    : jsonItems.education_level,
								notice_period      : jsonItems.notice_period,
								expectation        : jsonItems.expectation,
								skill              : jsonItems.skill,
								about              : jsonItems.about

							}

							counter -= 1;
							if (counter === 0){ 
								resolve(main_obj);
							}
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});

				});

	 		}
	 		else
	 		{
				return_result(main_obj);
	 	   	}

		});

	}
	catch(err)
	{ console.log("getBasicJobProfile",err); return_result(main_obj);}
};

methods.getJobLanguages = function(account_id,return_result){

	try
	{

		var arrLanguages = [];
		var qry = `SELECT * FROM jaze_job_cv_language_details WHERE group_id IN (SELECT group_id FROM jaze_job_cv_language_details WHERE meta_key = 'account_id' AND meta_value =?)`;

		DB.query(qry, [account_id], function(data, error){	

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);


					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
				
							var main_obj = {
						
								language    : jsonItems.language,
								fluency     : jsonItems.fluency
							}

							arrLanguages.push(main_obj);

							counter -= 1;
							if (counter === 0){ 
								resolve(arrLanguages);
							}
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});

				});

	 		}
	 		else
	 		{
				return_result([]);
	 	   	}

		});

	}
	catch(err)
	{ console.log("getJobLanguages",err); return_result([]);}
};


methods.getPreviousCompany = function(account_id,return_result){

	try
	{


		var obj_prev_role = {};
		var arrPrevRole = [];

		var qry = `SELECT * FROM jaze_job_cv_work_experience WHERE group_id IN (SELECT group_id FROM jaze_job_cv_work_experience WHERE meta_key = 'account_id' AND meta_value =?)`;

		DB.query(qry, [account_id], function(data, error){	

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);


					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							methods.__getCountryDetails(jsonItems.country,function(retCountry){

								methods.__getCityDetails(jsonItems.city,function(retCity){


									obj_prev_role = {
										job_title       : jsonItems.job_title,
										company_name    : jsonItems.company_name,
										description     : jsonItems.description,
										from_date       : jsonItems.from_date,
										to_date         : jsonItems.to_date,
										country         : retCountry,
										city            : retCity
									};
									
									arrPrevRole.push(obj_prev_role);
									counter -= 1;
									if (counter === 0){ 
										resolve(arrPrevRole);
									}

								});
							});
			
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							result[0].sort(function(a,b){
								if(a.from_date && b.from_date)
								{
									var splitA = a.from_date.split("/");
									var splitB = b.from_date.split("/");
							  		return splitA[1] < splitB[1];	
								}
						
							})

							return_result(result[0]);
						}
					});

				});

	 		}
	 		else
	 		{
				return_result([]);
	 	   	}

		});

	}
	catch(err)
	{ console.log("getPreviousCompany",err); return_result([]);}
};

methods.getCvQualifications = function(account_id,return_result){

	try
	{

		var obj_qualification = {};
		var arrQualifications = [];
		var qry = `SELECT * FROM jaze_job_cv_qualification_details WHERE group_id IN (SELECT group_id FROM jaze_job_cv_qualification_details WHERE meta_key = 'account_id' AND meta_value =?)`;

		DB.query(qry, [account_id], function(data, error){	

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							methods.__getCountryDetails(jsonItems.country,function(retCountry){

								methods.__getCityDetails(jsonItems.city,function(retCity){

	
									obj_qualification = {
										id                : jsonItems.group_id.toString(),
										title             : jsonItems.title,
										institute_name    : jsonItems.institute_name,
										subject           : jsonItems.subject,
										from_date         : jsonItems.from_date,
										to_date           : jsonItems.to_date,
										country           : retCountry,
										city              : retCity
									};

									arrQualifications.push(obj_qualification);
									counter -= 1;
									if (counter === 0){ 
										resolve(arrQualifications);
									}

								});
							});
			
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							result[0].sort(function(a,b){
								if(a.from_date && b.from_date)
								{
									var splitA = a.from_date.split("/");
									var splitB = b.from_date.split("/");
							  		return splitA[1] < splitB[1];	
								}
						
							})
							
							return_result(result[0]);
						}
					});

				});

	 		}
	 		else
	 		{
				return_result([]);
	 	   	}

		});

	}
	catch(err)
	{ console.log("getCvQualifications",err); return_result([]);}
};


methods.getCvAboutDetails = function(account_id,return_result){

	try
	{

		var obj_qualification = {
			about   : "",
			skills  : ""
		};

		var qry = `SELECT * FROM jaze_job_cv_additional_info WHERE group_id IN (SELECT group_id FROM jaze_job_cv_additional_info WHERE meta_key = 'account_id' AND meta_value =?)`;

		DB.query(qry, [account_id], function(data, error){	

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							methods.__getCountryDetails(jsonItems.country,function(retCountry){

								methods.__getCityDetails(jsonItems.city,function(retCity){

	
									obj_qualification = {
										about             : jsonItems.about,
										skills            : jsonItems.skills
									};

				
									counter -= 1;
									if (counter === 0){ 
										resolve(obj_qualification);
									}

								});
							});
			
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});

				});

	 		}
	 		else
	 		{
				return_result(obj_qualification);
	 	   	}

		});

	}
	catch(err)
	{ console.log("getCvAboutDetails",err); return_result(obj_qualification);}
};



//----------------------------- view account cv end ---------------------------------------//


//----------------------------- add account cv builder ---------------------------------------//

methods.addCvBasicDetails = function(arrParams,return_result){

	try
	{

		if(parseInt(arrParams.flag) === 1)
		{ 
			
			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_job_cv_basic_details";
  			var lastGrpId = 0;

			methods.getLastGroupId(queGroupMax, function(queGrpRes){

			
				lastGrpId = queGrpRes[0].lastGrpId;

				var arrInsert = {
					account_id       : arrParams.account_id,
					first_name       : arrParams.first_name,
					last_name        : arrParams.last_name,
					dob              : arrParams.dob,
					gender           : arrParams.gender,
					nationality_id   : arrParams.nationality_id,
					mobile_code_id   : arrParams.mobile_code_id,
					mobile_number    : arrParams.mobile_number,
					mobile_code_id_2 : arrParams.mobile_code_id_2,
					mobile_number_2  : arrParams.mobile_number_2,
					country_id       : arrParams.country_id,
					city_id          : arrParams.city_id,
					profile_logo     : arrParams.file_name,
					status           : 1,
					create_date      : standardDT,
					update_date      : standardDT,

				};


				var plusOneGroupID = lastGrpId+1;
		  		var querInsert = `insert into jaze_job_cv_basic_details (group_id, meta_key, meta_value) VALUES (?,?,?)`;

			 	for (var key in arrInsert) 
			 	{
					DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
			 		 	if(parseInt(data.affectedRows) > 0){
	 						return_result(1);
				 	   	}else{
		   					return_result(0);
				 	   	}
			 	 	});
			  	};

			});

		}
		else if(parseInt(arrParams.flag) === 2)
		{
			if(parseInt(arrParams.account_id) !==0 )
			{			

				var queLib = `SELECT * FROM jaze_job_cv_basic_details WHERE group_id IN (SELECT group_id FROM jaze_job_cv_basic_details WHERE meta_key = 'account_id' AND meta_value =?) `;

				DB.query(queLib,[arrParams.account_id], function(dataL, error){

					if(typeof dataL !== 'undefined' && dataL !== null && parseInt(dataL.length) > 0)
					{
		 				
						methods.convertMultiMetaArray(dataL, function(retConverted){


							

			 				if(arrParams.file_name !="" ){

			 					if(retConverted[0].profile_logo){

		 							var storagePath = G_STATIC_IMG_PATH+'individual_account/jobs/'+arrParams.account_id+'/'+retConverted[0].profile_logo;
			 	
									fs.unlink(storagePath, function (err) {
										return_result(err);
									});
			 					}
			 				}

							var	arrUpdate = {
								account_id       : arrParams.account_id,
								first_name       : arrParams.first_name,
								last_name        : arrParams.last_name,
								dob              : arrParams.dob,
								gender           : arrParams.gender,
								nationality_id   : arrParams.nationality_id,
								mobile_code_id   : arrParams.mobile_code_id,
								mobile_number    : arrParams.mobile_number,
								mobile_code_id_2 : arrParams.mobile_code_id_2,
								mobile_number_2  : arrParams.mobile_number_2,
								country_id       : arrParams.country_id,
								city_id          : arrParams.city_id,
								profile_logo     : arrParams.file_name,
								status           : 1,
								create_date      : standardDT,
								update_date      : standardDT,
							};

							var updateQue = `UPDATE jaze_job_cv_basic_details SET meta_value =? WHERE meta_key =? AND group_id =?`;
							
							for (var key in arrUpdate) 
						 	{	
							     if(arrUpdate[key] !== null && arrUpdate[key] != ""){
									DB.query(updateQue, [arrUpdate[key],key,retConverted[0].group_id], function(data, error){
								 		if(data !== null){
							 				return_result(1);
								 	   	}else{
							 	   			return_result(0);
								 	   	}
									});
								}
							}

					 	});
			 	   	}
			 	   	else
			 	   	{
			 	   		return_result(2);
			 	   	}

		 	 	});

			}
			else
			{ 
				return_result(0); 
			}

		}


	}
	catch(err)
	{ 
		console.log("addCvBasicDetails",err); return_result(null); 
	}	
};

//----------------------------- add account cv builder end---------------------------------------//

//----------------------------- add account cv qualification ---------------------------------------//

methods.addCvQualificationDetails = function(arrParams,return_result){

	try
	{

		if(parseInt(arrParams.flag) === 0)
		{

			if(parseInt(arrParams.account_id) !==0 )
			{			

				var arrResults = [];
				var arrObjects = {};

				var queLib = `SELECT * FROM jaze_job_cv_qualification_details WHERE group_id IN (SELECT group_id FROM jaze_job_cv_qualification_details WHERE meta_key = 'account_id' AND meta_value =?) `;

				DB.query(queLib,[arrParams.account_id], function(dataL, error){

					if(typeof dataL !== 'undefined' && dataL !== null && parseInt(dataL.length) > 0)
					{

 						methods.convertMultiMetaArray(dataL, function(retConverted){	

							var counter = retConverted.length;		
							var jsonItems = Object.values(retConverted);

							promiseForeach.each(jsonItems,[function (jsonItems) {
								return new Promise(function (resolve, reject) {		
									
									methods.__getCountryDetails(jsonItems.country, function(retCountry){

										methods.__getCityDetails(jsonItems.city, function(retCity){

											arrObjects = {
												id             : jsonItems.group_id.toString(),
												institute_name : jsonItems.institute_name,
												title          : jsonItems.title,
												subject        : jsonItems.subject,
												from_date      : jsonItems.from_date,
												to_date        : jsonItems.to_date,
												country        : jsonItems.country,
												city           : jsonItems.city
											};

											arrResults.push(arrObjects);
											counter -= 1;
											resolve(arrResults);
										});
									});
								})
							}],
							function (result, current) {
								if (counter === 0){ 

									result[0].sort(function(a,b){
										var splitA = a.to_date.split("/");
										var splitB = b.to_date.split("/");
									  	return splitA[1] < splitB[1];
									})

									return_result(result[0]);
								}
							});

						});
			 	   	}
			 	   	else
			 	   	{
			 	   		return_result(2);
			 	   	}

		 	 	});

			}
			else
			{ 
				return_result(0); 
			}


		}
		else if(parseInt(arrParams.flag) === 1)
		{ 
			
			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_job_cv_qualification_details";
  			var lastGrpId = 0;

			methods.getLastGroupId(queGroupMax, function(queGrpRes){

			
				lastGrpId = queGrpRes[0].lastGrpId;

				var arrInsert = {
					account_id       : arrParams.account_id,
					institute_name   : arrParams.institute_name,
					title            : arrParams.title,
					subject          : arrParams.subject,
					from_date        : arrParams.from_date,
					to_date          : arrParams.to_date,
					city             : arrParams.city_id,
					country          : arrParams.country_id,
					status           : 1,
					create_date      : standardDT,
					update_date      : standardDT

				};


				var plusOneGroupID = lastGrpId+1;
		  		var querInsert = `insert into jaze_job_cv_qualification_details (group_id, meta_key, meta_value) VALUES (?,?,?)`;

			 	for (var key in arrInsert) 
			 	{
					DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
			 		 	if(parseInt(data.affectedRows) > 0){
	 						return_result(1);
				 	   	}else{
		   					return_result(0);
				 	   	}
			 	 	});
			  	};

			});

		}
		else if(parseInt(arrParams.flag) === 2)
		{

			if(parseInt(arrParams.account_id) !==0 )
			{			

				var queUp = `SELECT * FROM jaze_job_cv_qualification_details WHERE group_id IN (SELECT group_id FROM jaze_job_cv_qualification_details WHERE meta_key = 'account_id' AND meta_value =?) 
				AND group_id =?`;

				DB.query(queUp,[arrParams.account_id,arrParams.id], function(dataU, error){

					if(typeof dataU !== 'undefined' && dataU !== null && parseInt(dataU.length) > 0)
					{
		 			
						methods.convertMultiMetaArray(dataU, function(retConverted){

							var	arrUpdate = {
								account_id       : arrParams.account_id,
								institute_name   : arrParams.institute_name,
								title            : arrParams.title,
								subject          : arrParams.subject,
								from_date        : arrParams.from_date,
								to_date          : arrParams.to_date,
								city             : arrParams.city_id,
								country          : arrParams.country_id,
								status           : 1,
								create_date      : standardDT,
								update_date      : standardDT
							};

							var updateQue = `UPDATE jaze_job_cv_qualification_details SET meta_value =? WHERE meta_key =? AND group_id =?`;
							
							for (var key in arrUpdate) 
						 	{	
							     if(arrUpdate[key] !== null && arrUpdate[key] != ""){
									DB.query(updateQue, [arrUpdate[key],key,retConverted[0].group_id], function(data, error){
								 		if(data !== null){
							 				return_result(1);
								 	   	}else{
							 	   			return_result(0);
								 	   	}
									});
								}
							}

					 	});
			 	   	}
			 	   	else
			 	   	{
			 	   		return_result(2);
			 	   	}

		 	 	});

			}
			else
			{ 
				return_result(0); 
			}

		}

	}
	catch(err)
	{ 
		console.log("addCvQualificationDetails",err); return_result(null); 
	}	
};

//----------------------------- add account cv qualification end---------------------------------------//

//----------------------------- add account cv work experience ---------------------------------------//

methods.addCVWorkExperience = function(arrParams,return_result){

	try
	{

		if(parseInt(arrParams.flag) === 0)
		{

			if(parseInt(arrParams.account_id) !==0 )
			{			

				var arrResults = [];
				var arrObjects = {};

				var queLib = `SELECT * FROM jaze_job_cv_work_experience WHERE group_id IN (SELECT group_id FROM jaze_job_cv_work_experience WHERE meta_key = 'account_id' AND meta_value =?) `;

				DB.query(queLib,[arrParams.account_id], function(dataL, error){

					if(typeof dataL !== 'undefined' && dataL !== null && parseInt(dataL.length) > 0)
					{

 						methods.convertMultiMetaArray(dataL, function(retConverted){	

							var counter = retConverted.length;		
							var jsonItems = Object.values(retConverted);

							promiseForeach.each(jsonItems,[function (jsonItems) {
								return new Promise(function (resolve, reject) {		
									
									methods.__getCountryDetails(jsonItems.country, function(retCountry){

										methods.__getCityDetails(jsonItems.city, function(retCity){

											var to_date = "";
											if(jsonItems.to_date){
												to_date = jsonItems.to_date;
											}

											arrObjects = {
												id             : jsonItems.group_id.toString(),
												company_name   : jsonItems.company_name,
												job_title      : jsonItems.job_title,
												description    : jsonItems.description,
												from_date      : jsonItems.from_date,
												to_date        : to_date,
												country        : jsonItems.country,
												city           : jsonItems.city
											};

											arrResults.push(arrObjects);
											counter -= 1;
											resolve(arrResults);
										});
									});
								})
							}],
							function (result, current) {
								if (counter === 0){ 

									result[0].sort(function(a,b){
										if(a.from_date && b.from_date)
										{
											var splitA = a.from_date.split("/");
											var splitB = b.from_date.split("/");
									  		return splitA[1] < splitB[1];	
										}
								
									})

									return_result(result[0]);
								}
							});

						});
			 	   	}
			 	   	else
			 	   	{
			 	   		return_result(2);
			 	   	}

		 	 	});

			}
			else
			{ 
				return_result(0); 
			}


		}
		else if(parseInt(arrParams.flag) === 1)
		{ 
			
			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_job_cv_work_experience";
  			var lastGrpId = 0;

			methods.getLastGroupId(queGroupMax, function(queGrpRes){

			
				lastGrpId = queGrpRes[0].lastGrpId;

				var arrInsert = {
					account_id       : arrParams.account_id,
					company_name     : arrParams.company_name,
					job_title        : arrParams.job_title,
					description      : arrParams.description,
					from_date        : arrParams.from_date,
					to_date          : arrParams.to_date,
					city             : arrParams.city_id,
					country          : arrParams.country_id,
					status           : 1,
					create_date      : standardDT,
					update_date      : standardDT

				};


				var plusOneGroupID = lastGrpId+1;
		  		var querInsert = `insert into jaze_job_cv_work_experience (group_id, meta_key, meta_value) VALUES (?,?,?)`;

			 	for (var key in arrInsert) 
			 	{
					DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
			 		 	if(parseInt(data.affectedRows) > 0){
	 						return_result(1);
				 	   	}else{
		   					return_result(0);
				 	   	}
			 	 	});
			  	};

			});

		}
		else if(parseInt(arrParams.flag) === 2)
		{

			if(parseInt(arrParams.account_id) !==0 )
			{			

				var queUp = `SELECT * FROM jaze_job_cv_work_experience WHERE group_id IN (SELECT group_id FROM jaze_job_cv_work_experience WHERE meta_key = 'account_id' AND meta_value =?) 
				AND group_id =?`;

				DB.query(queUp,[arrParams.account_id,arrParams.id], function(dataU, error){

					if(typeof dataU !== 'undefined' && dataU !== null && parseInt(dataU.length) > 0)
					{
		 			
						methods.convertMultiMetaArray(dataU, function(retConverted){

							var	arrUpdate = {
								account_id       : arrParams.account_id,
								company_name     : arrParams.company_name,
								job_title        : arrParams.job_title,
								description      : arrParams.description,
								from_date        : arrParams.from_date,
								to_date          : arrParams.to_date,
								city             : arrParams.city_id,
								country          : arrParams.country_id,
								status           : 1,
								create_date      : standardDT,
								update_date      : standardDT
							};

							var updateQue = `UPDATE jaze_job_cv_work_experience SET meta_value =? WHERE meta_key =? AND group_id =?`;
							
							for (var key in arrUpdate) 
						 	{	
							     if(arrUpdate[key] !== null && arrUpdate[key] != ""){
									DB.query(updateQue, [arrUpdate[key],key,retConverted[0].group_id], function(data, error){
								 		if(data !== null){
							 				return_result(1);
								 	   	}else{
							 	   			return_result(0);
								 	   	}
									});
								}
							}

					 	});
			 	   	}
			 	   	else
			 	   	{
			 	   		return_result(2);
			 	   	}

		 	 	});

			}
			else
			{ 
				return_result(0); 
			}

		}

	}
	catch(err)
	{ 
		console.log("addCVWorkExperience",err); return_result(null); 
	}	
};

//----------------------------- add account cv work experience end---------------------------------------//


//----------------------------- add account cv about details ---------------------------------------//

methods.addCvAboutDetails = function(arrParams,return_result){

	try
	{

		if(parseInt(arrParams.flag) === 0)
		{

			if(parseInt(arrParams.account_id) !==0 )
			{			

				var arrResults = [];
				var arrObjects = {};

				var queLib = `SELECT * FROM jaze_job_cv_additional_info WHERE group_id IN (SELECT group_id FROM jaze_job_cv_additional_info WHERE meta_key = 'account_id' AND meta_value =?) `;

				DB.query(queLib,[arrParams.account_id], function(dataL, error){

					if(typeof dataL !== 'undefined' && dataL !== null && parseInt(dataL.length) > 0)
					{

 						methods.convertMultiMetaArray(dataL, function(retConverted){	

							var counter = retConverted.length;		
							var jsonItems = Object.values(retConverted);

							promiseForeach.each(jsonItems,[function (jsonItems) {
								return new Promise(function (resolve, reject) {		
									
									methods.__getCountryDetails(jsonItems.country, function(retCountry){

										methods.__getCityDetails(jsonItems.city, function(retCity){

											methods.getJobLanguages(jsonItems.account_id, function(retLang){

												

												arrObjects = {
													id              : jsonItems.group_id.toString(),
													about           : jsonItems.about,
													skills          : jsonItems.skills,
													relocate_status : jsonItems.relocate_status.toString(),
													privacy         : jsonItems.privacy.toString(),
													languages       : retLang
										
												};

												arrResults.push(arrObjects);
												counter -= 1;
												resolve(arrResults);

											});

										});
									});
								})
							}],
							function (result, current) {
								if (counter === 0){ 

									return_result(result[0]);
								}
							});

						});
			 	   	}
			 	   	else
			 	   	{
			 	   		return_result(2);
			 	   	}

		 	 	});

			}
			else
			{ 
				return_result(0); 
			}


		}
		else if(parseInt(arrParams.flag) === 1)
		{ 
			
			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_job_cv_additional_info";
  			var lastGrpId = 0;

			methods.getLastGroupId(queGroupMax, function(queGrpRes){

			
				lastGrpId = queGrpRes[0].lastGrpId;

				var is_relocate = "No";
				if(parseInt(arrParams.is_relocate) === 1){
					is_relocate = "Yes";
				}

				var arrInsert = {
					account_id       : arrParams.account_id,
					about            : arrParams.about,
					skills           : arrParams.skills,
					relocate_status  : is_relocate,
					privacy          : arrParams.privacy,
					status           : 1,
					create_date      : standardDT,
					update_date      : standardDT

				};


				var plusOneGroupID = lastGrpId+1;
		  		var querInsert = `insert into jaze_job_cv_additional_info (group_id, meta_key, meta_value) VALUES (?,?,?)`;

			 	for (var key in arrInsert) 
			 	{
					DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){});
			  	};

			});		

			if(parseInt(Object.keys(arrParams.languages).length) > 0)	
			{	

				var queUp = `SELECT * FROM jaze_job_cv_language_details WHERE group_id IN (SELECT group_id FROM jaze_job_cv_language_details WHERE meta_key = 'account_id' AND meta_value =?);`

				DB.query(queUp,[arrParams.account_id], function(dataU, error){

					if(typeof dataU !== 'undefined' && dataU !== null && parseInt(dataU.length) > 0)
					{

						methods.convertMultiMetaArray(dataU, function(retConverted){

							var arrGroupIDS = [];
							retConverted.forEach(function(grp){
								arrGroupIDS.push(grp.group_id);
							});

			
							var queDel = `DELETE FROM jaze_job_cv_language_details where group_id IN(?)`;
							DB.query(queDel,[arrGroupIDS], function(data, error){

							 	if(parseInt(data.affectedRows) > 0)
							 	{

			 						var returnValue = 1;
									var counter = Object.keys(arrParams.languages).length;		
									var jsonItems = Object.values(arrParams.languages);
									var count = 1;
									promiseForeach.each(jsonItems,[function (jsonItems) {
										return new Promise(function (resolve, reject) {		
							
							  				var queGroupMaxLang = "SELECT MAX(group_id) AS lastGrpId FROM jaze_job_cv_language_details";
											methods.getLastGroupId(queGroupMaxLang, function(queGrpRes){

												var lastGrpIdLang = queGrpRes[0].lastGrpId+count;
									
												var arrInsertLang = {
													account_id       : arrParams.account_id,
													language         : jsonItems.language,
													fluency          : jsonItems.fluency,
													status           : 1,
													create_date      : standardDT,
													update_date      : standardDT

												};

												count++;
												methods.procInsertLang(lastGrpIdLang,arrInsertLang,function(retInsert){});
												counter -= 1;
												if (counter === 0){ 
													resolve(returnValue);
												}

									    	});
										})
									}],
									function (result, current) {
										if (counter === 0){ 
											return_result(result[0]);
										}
									});

						 	   	}
						 	   	else
						 	   	{
				   					return_result(0);
						 	   	}

							});

						});
					}
					else
					{

						var returnValue = 1;
						var counter = Object.keys(arrParams.languages).length;		
						var jsonItems = Object.values(arrParams.languages);
						var count = 1;
						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
				
				  				var queGroupMaxLang = "SELECT MAX(group_id) AS lastGrpId FROM jaze_job_cv_language_details";
								methods.getLastGroupId(queGroupMaxLang, function(queGrpRes){

									var lastGrpIdLang = queGrpRes[0].lastGrpId+count;
						
									var arrInsertLang = {
										account_id       : arrParams.account_id,
										language         : jsonItems.language,
										fluency          : jsonItems.fluency,
										status           : 1,
										create_date      : standardDT,
										update_date      : standardDT

									};

									count++;
									methods.procInsertLang(lastGrpIdLang,arrInsertLang,function(retInsert){});
									counter -= 1;
									if (counter === 0){ 
										resolve(returnValue);
									}

						    	});
							})
						}],
						function (result, current) {
							if (counter === 0){ 
								return_result(result[0]);
							}
						});


					}

				});
			}



		}
		else if(parseInt(arrParams.flag) === 2)
		{

			if(parseInt(arrParams.account_id) !==0 )
			{			

				var queUp = `SELECT * FROM jaze_job_cv_additional_info WHERE group_id IN (SELECT group_id FROM jaze_job_cv_additional_info WHERE meta_key = 'account_id' AND meta_value =?) 
				AND group_id =?`;

				DB.query(queUp,[arrParams.account_id,arrParams.id], function(dataU, error){

					if(typeof dataU !== 'undefined' && dataU !== null && parseInt(dataU.length) > 0)
					{
		 			
						methods.convertMultiMetaArray(dataU, function(retConverted){

							var is_relocate = "No";
							if(parseInt(arrParams.is_relocate) === 1){
								is_relocate = "Yes";
							}

							var arrUpdate = {
								account_id       : arrParams.account_id,
								about            : arrParams.about,
								skills           : arrParams.skills,
								relocate_status  : is_relocate,
								privacy          : arrParams.privacy,
								status           : 1,
								create_date      : standardDT,
								update_date      : standardDT

							};


							var updateQue = `UPDATE jaze_job_cv_additional_info SET meta_value =? WHERE meta_key =? AND group_id =?`;
							
							for (var key in arrUpdate) 
						 	{	
							     if(arrUpdate[key] !== null && arrUpdate[key] != ""){
									DB.query(updateQue, [arrUpdate[key],key,retConverted[0].group_id], function(data, error){});
								}
							}

					 	});


						if(parseInt(Object.keys(arrParams.languages).length) > 0)	
						{	

							var queUp = `SELECT * FROM jaze_job_cv_language_details WHERE group_id IN (SELECT group_id FROM jaze_job_cv_language_details WHERE meta_key = 'account_id' AND meta_value =?);`

							DB.query(queUp,[arrParams.account_id], function(dataU, error){

								if(typeof dataU !== 'undefined' && dataU !== null && parseInt(dataU.length) > 0)
								{

									methods.convertMultiMetaArray(dataU, function(retConverted){

										var arrGroupIDS = [];
										retConverted.forEach(function(grp){
											arrGroupIDS.push(grp.group_id);
										});

						
										var queDel = `DELETE FROM jaze_job_cv_language_details where group_id IN(?)`;
										DB.query(queDel,[arrGroupIDS], function(data, error){

										 	if(parseInt(data.affectedRows) > 0)
										 	{

						 						var returnValue = 1;
												var counter = Object.keys(arrParams.languages).length;		
												var jsonItems = Object.values(arrParams.languages);
												var count = 1;
												promiseForeach.each(jsonItems,[function (jsonItems) {
													return new Promise(function (resolve, reject) {		
										
										  				var queGroupMaxLang = "SELECT MAX(group_id) AS lastGrpId FROM jaze_job_cv_language_details";
														methods.getLastGroupId(queGroupMaxLang, function(queGrpRes){

															var lastGrpIdLang = queGrpRes[0].lastGrpId+count;
												
															var arrInsertLang = {
																account_id       : arrParams.account_id,
																language         : jsonItems.language,
																fluency          : jsonItems.fluency,
																status           : 1,
																create_date      : standardDT,
																update_date      : standardDT

															};

															count++;
															methods.procInsertLang(lastGrpIdLang,arrInsertLang,function(retInsert){});
															counter -= 1;
															if (counter === 0){ 
																resolve(returnValue);
															}

												    	});
													})
												}],
												function (result, current) {
													if (counter === 0){ 
														return_result(result[0]);
													}
												});

									 	   	}
									 	   	else
									 	   	{
							   					return_result(0);
									 	   	}

										});

									});
								}
								else
								{

									var returnValue = 1;
									var counter = Object.keys(arrParams.languages).length;		
									var jsonItems = Object.values(arrParams.languages);
									var count = 1;
									promiseForeach.each(jsonItems,[function (jsonItems) {
										return new Promise(function (resolve, reject) {		
							
							  				var queGroupMaxLang = "SELECT MAX(group_id) AS lastGrpId FROM jaze_job_cv_language_details";
											methods.getLastGroupId(queGroupMaxLang, function(queGrpRes){

												var lastGrpIdLang = queGrpRes[0].lastGrpId+count;
									
												var arrInsertLang = {
													account_id       : arrParams.account_id,
													language         : jsonItems.language,
													fluency          : jsonItems.fluency,
													status           : 1,
													create_date      : standardDT,
													update_date      : standardDT

												};

												count++;
												methods.procInsertLang(lastGrpIdLang,arrInsertLang,function(retInsert){});
												counter -= 1;
												if (counter === 0){ 
													resolve(returnValue);
												}

									    	});
										})
									}],
									function (result, current) {
										if (counter === 0){ 
											return_result(result[0]);
										}
									});


								}

							});
						}


			 	   	}
			 	   	else
			 	   	{
			 	   		return_result(2);
			 	   	}

		 	 	});

			}
			else
			{ 
				return_result(0); 
			}

		}

	}
	catch(err)
	{ 
		console.log("addCvAboutDetails",err); return_result(null); 
	}	
};


methods.procInsertLang = function(queGrpRes,arrInsertLang,return_result){

	var querInsertLang = `insert into jaze_job_cv_language_details (group_id, meta_key, meta_value) VALUES (?,?,?)`;
 	for (var key in arrInsertLang) 
 	{
		DB.query(querInsertLang,[queGrpRes,key,arrInsertLang[key]], function(data, error){
 		 	if(parseInt(data.affectedRows) > 0){
 	
				return_result(1);
	 	   	}else{
					return_result(0);
	 	   	}
 	 	});
  	};
};

//----------------------------- add account cv about details end---------------------------------------//

//----------------------------- add remove account cv start---------------------------------------//

methods.removeAccountCv = function(arrParams, return_result){

	try {

		var querBasic = `SELECT * FROM jaze_job_cv_basic_details WHERE group_id IN (SELECT group_id FROM jaze_job_cv_basic_details WHERE meta_key = 'account_id' AND meta_value =?)`;

		DB.query(querBasic, [arrParams.account_id], function(data, error){
	 	 	if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){


				var query = `DELETE FROM jaze_job_cv_basic_details WHERE group_id=?`
				methods.deleteCvByAccount(query,data[0].group_id,function(retBasic){
					if(parseInt(retBasic) === 1){

						var querQuali = `SELECT * FROM jaze_job_cv_qualification_details WHERE group_id IN (SELECT group_id FROM jaze_job_cv_qualification_details WHERE meta_key = 'account_id' AND meta_value =?)`;
						DB.query(querQuali, [arrParams.account_id], function(dquali, error){
					 	 	if(typeof dquali !== 'undefined' && dquali !== null && parseInt(dquali.length) > 0)
					 	 	{

				 	 			var arrQuali = [];
				 	 			methods.convertMultiMetaArray(dquali, function(retConvertedQuali){

					 	 			retConvertedQuali.forEach(function(val){
					 	 				arrQuali.push(val.group_id);

					 	 			});

				 	 			});

				 	 			var delQuali = `DELETE FROM jaze_job_cv_qualification_details WHERE group_id IN(?)`
		 	 					methods.deleteCvByAccount(delQuali,arrQuali,function(retQuali){
		 	 						if(parseInt(retQuali) === 1){

										var querExp = `SELECT * FROM jaze_job_cv_work_experience WHERE group_id IN 
										(SELECT group_id FROM jaze_job_cv_work_experience WHERE meta_key = 'account_id' AND meta_value =?)`;

										DB.query(querExp, [arrParams.account_id], function(dexp, error){

					 	 					if(typeof dexp !== 'undefined' && dexp !== null && parseInt(dexp.length) > 0)
					 	 					{
				 	 							var arrExp = [];
			 	 								methods.convertMultiMetaArray(dexp, function(retConvertedExp){

									 	 			retConvertedExp.forEach(function(val){
									 	 				arrExp.push(val.group_id);

									 	 			});

								 	 			});

					 	 						var delExp = `DELETE FROM jaze_job_cv_work_experience WHERE group_id IN(?)`
						 	 					methods.deleteCvByAccount(delExp,arrExp,function(retExp){
						 	 						if(parseInt(retExp) === 1){

											
 														var querExp = `SELECT * FROM jaze_job_cv_language_details WHERE group_id IN 
														(SELECT group_id FROM jaze_job_cv_language_details WHERE meta_key = 'account_id' AND meta_value =?)`;

														DB.query(querExp, [arrParams.account_id], function(dlang, error){

									 	 					if(typeof dlang !== 'undefined' && dlang !== null && parseInt(dlang.length) > 0)
									 	 					{
								 	 							var arrLang = [];
							 	 								methods.convertMultiMetaArray(dlang, function(retConvertedLang){

													 	 			retConvertedLang.forEach(function(val){
													 	 				arrLang.push(val.group_id);

													 	 			});

												 	 			});

									 	 						var delLang = `DELETE FROM jaze_job_cv_language_details WHERE group_id IN(?)`
										 	 					methods.deleteCvByAccount(delLang,arrLang,function(retdelLang){
										 	 						if(parseInt(retdelLang) === 1){

									 	 								var queAdditional = `SELECT * FROM jaze_job_cv_additional_info WHERE group_id IN 
																		(SELECT group_id FROM jaze_job_cv_additional_info WHERE meta_key = 'account_id' AND meta_value =?)`;

																		DB.query(queAdditional, [arrParams.account_id], function(daddi, error){

													 	 					if(typeof daddi !== 'undefined' && daddi !== null && parseInt(daddi.length) > 0)
													 	 					{
																				
																				var delLang = `DELETE FROM jaze_job_cv_additional_info WHERE group_id=?`
										 	 									methods.deleteCvByAccount(delLang,daddi[0].group_id,function(retAddi){
								 	 												if(parseInt(retdelLang) === 1){
								 	 													return_result(1);
																					}
								 	 											});
																			}
																			else
																			{
																				return_result(1);
																			}

																		});
																		
													 	 			}
									 	 						});

									 	 					}
									 	 					else
									 	 					{
					 	 										var delLang = `DELETE FROM jaze_job_cv_language_details WHERE group_id IN(?)`
										 	 					methods.deleteCvByAccount(delLang,arrLang,function(retdelLang){
										 	 						if(parseInt(retdelLang) === 1){

									 	 								var queAdditional = `SELECT * FROM jaze_job_cv_additional_info WHERE group_id IN 
																		(SELECT group_id FROM jaze_job_cv_additional_info WHERE meta_key = 'account_id' AND meta_value =?)`;

																		DB.query(queAdditional, [arrParams.account_id], function(daddi, error){

													 	 					if(typeof daddi !== 'undefined' && daddi !== null && parseInt(daddi.length) > 0)
													 	 					{
																				
																				var delLang = `DELETE FROM jaze_job_cv_additional_info WHERE group_id=?`
										 	 									methods.deleteCvByAccount(delLang,daddi[0].group_id,function(retAddi){
								 	 												if(parseInt(retdelLang) === 1){
								 	 													return_result(1);
																					}
																					else
																					{
																						return_result(1);
																					}
								 	 											});
																			}
																			else
																			{
																				return_result(1);
																			}

																		});
																		
													 	 			}
									 	 						});
									 	 					}

							 	 						});

									 	 			}

					 	 						});

					 	 					}
					 	 					else
					 	 					{
				 	 							var querExp = `SELECT * FROM jaze_job_cv_language_details WHERE group_id IN 
												(SELECT group_id FROM jaze_job_cv_language_details WHERE meta_key = 'account_id' AND meta_value =?)`;

												DB.query(querExp, [arrParams.account_id], function(dlang, error){

							 	 					if(typeof dlang !== 'undefined' && dlang !== null && parseInt(dlang.length) > 0)
							 	 					{
						 	 							var arrLang = [];
					 	 								methods.convertMultiMetaArray(dlang, function(retConvertedLang){

											 	 			retConvertedLang.forEach(function(val){
											 	 				arrLang.push(val.group_id);

											 	 			});

										 	 			});
									 	 			}
									 	 			else
									 	 			{

							 	 						var queAdditional = `SELECT * FROM jaze_job_cv_additional_info WHERE group_id IN 
														(SELECT group_id FROM jaze_job_cv_additional_info WHERE meta_key = 'account_id' AND meta_value =?)`;

														DB.query(queAdditional, [arrParams.account_id], function(daddi, error){

									 	 					if(typeof daddi !== 'undefined' && daddi !== null && parseInt(daddi.length) > 0)
									 	 					{
																
																var delLang = `DELETE FROM jaze_job_cv_additional_info WHERE group_id=?`
						 	 									methods.deleteCvByAccount(delLang,daddi[0].group_id,function(retAddi){
				 	 												if(parseInt(retdelLang) === 1){
				 	 													return_result(1);
																	}
																	else
																	{
																		return_result(1);
																	}
				 	 											});
															}
															else
															{
																return_result(1);
															}

														});

									 	 			}

							 	 				});
					 	 					}

			 	 						});
					 	 			}
	 	 						});
				 	 		}
				 	 		else
				 	 		{
			 	 				
								var querExp = `SELECT * FROM jaze_job_cv_work_experience WHERE group_id IN 
								(SELECT group_id FROM jaze_job_cv_work_experience WHERE meta_key = 'account_id' AND meta_value =?)`;

								DB.query(querExp, [arrParams.account_id], function(dexp, error){

			 	 					if(typeof dexp !== 'undefined' && dexp !== null && parseInt(dexp.length) > 0)
			 	 					{
		 	 							var arrExp = [];
	 	 								methods.convertMultiMetaArray(dexp, function(retConvertedExp){

							 	 			retConvertedExp.forEach(function(val){
							 	 				arrExp.push(val.group_id);

							 	 			});

						 	 			});

			 	 						var delExp = `DELETE FROM jaze_job_cv_work_experience WHERE group_id IN(?)`
				 	 					methods.deleteCvByAccount(delExp,arrExp,function(retExp){
				 	 						if(parseInt(retExp) === 1){
												
												var querExp = `SELECT * FROM jaze_job_cv_language_details WHERE group_id IN 
												(SELECT group_id FROM jaze_job_cv_language_details WHERE meta_key = 'account_id' AND meta_value =?)`;

												DB.query(querExp, [arrParams.account_id], function(dlang, error){

							 	 					if(typeof dlang !== 'undefined' && dlang !== null && parseInt(dlang.length) > 0)
							 	 					{
						 	 							var arrLang = [];
					 	 								methods.convertMultiMetaArray(dlang, function(retConvertedLang){

											 	 			retConvertedLang.forEach(function(val){
											 	 				arrLang.push(val.group_id);

											 	 			});

										 	 			});

							 	 						var delLang = `DELETE FROM jaze_job_cv_language_details WHERE group_id IN(?)`
								 	 					methods.deleteCvByAccount(delLang,arrLang,function(retdelLang){
								 	 						if(parseInt(retdelLang) === 1){

				 	 											var queAdditional = `SELECT * FROM jaze_job_cv_additional_info WHERE group_id IN 
																(SELECT group_id FROM jaze_job_cv_additional_info WHERE meta_key = 'account_id' AND meta_value =?)`;

																DB.query(queAdditional, [arrParams.account_id], function(daddi, error){

											 	 					if(typeof daddi !== 'undefined' && daddi !== null && parseInt(daddi.length) > 0)
											 	 					{
																		
																		var delLang = `DELETE FROM jaze_job_cv_additional_info WHERE group_id=?`
								 	 									methods.deleteCvByAccount(delLang,daddi[0].group_id,function(retAddi){
						 	 												if(parseInt(retdelLang) === 1){
						 	 													return_result(1);
																			}
																			else
																			{
																				return_result(1);
																			}
						 	 											});
																	}
																	else
																	{
																		return_result(1);
																	}

																});

							 	 							}
							 	 							else
							 	 							{

		 	 													var queAdditional = `SELECT * FROM jaze_job_cv_additional_info WHERE group_id IN 
																(SELECT group_id FROM jaze_job_cv_additional_info WHERE meta_key = 'account_id' AND meta_value =?)`;

																DB.query(queAdditional, [arrParams.account_id], function(daddi, error){

											 	 					if(typeof daddi !== 'undefined' && daddi !== null && parseInt(daddi.length) > 0)
											 	 					{
																		
																		var delLang = `DELETE FROM jaze_job_cv_additional_info WHERE group_id=?`
								 	 									methods.deleteCvByAccount(delLang,daddi[0].group_id,function(retAddi){
						 	 												if(parseInt(retdelLang) === 1){
						 	 													return_result(1);
																			}
																			else
																			{
																				return_result(1);
																			}
						 	 											});
																	}
																	else
																	{
																		return_result(1);
																	}

																});
							 	 							}

														});

							 	 					}
							 	 					else
							 	 					{


		 	 											var queAdditional = `SELECT * FROM jaze_job_cv_additional_info WHERE group_id IN 
														(SELECT group_id FROM jaze_job_cv_additional_info WHERE meta_key = 'account_id' AND meta_value =?)`;

														DB.query(queAdditional, [arrParams.account_id], function(daddi, error){

									 	 					if(typeof daddi !== 'undefined' && daddi !== null && parseInt(daddi.length) > 0)
									 	 					{
																
																var delLang = `DELETE FROM jaze_job_cv_additional_info WHERE group_id=?`
						 	 									methods.deleteCvByAccount(delLang,daddi[0].group_id,function(retAddi){
				 	 												if(parseInt(retdelLang) === 1){
				 	 													return_result(1);
																	}
																	else
																	{
																		return_result(1);
																	}
				 	 											});
															}
															else
															{
																return_result(1);
															}

														});
																
							 	 					}

				 	 							});	

			 	 							}

 	 									});

	 	 							}
	 	 							else
	 	 							{

	 	 								var querExp = `SELECT * FROM jaze_job_cv_language_details WHERE group_id IN 
										(SELECT group_id FROM jaze_job_cv_language_details WHERE meta_key = 'account_id' AND meta_value =?)`;

										DB.query(querExp, [arrParams.account_id], function(dlang, error){

					 	 					if(typeof dlang !== 'undefined' && dlang !== null && parseInt(dlang.length) > 0)
					 	 					{
				 	 							var arrLang = [];
			 	 								methods.convertMultiMetaArray(dlang, function(retConvertedLang){

									 	 			retConvertedLang.forEach(function(val){
									 	 				arrLang.push(val.group_id);

									 	 			});

								 	 			});

					 	 						var delLang = `DELETE FROM jaze_job_cv_language_details WHERE group_id IN(?)`
						 	 					methods.deleteCvByAccount(delLang,arrLang,function(retdelLang){
						 	 						if(parseInt(retdelLang) === 1){

		 	 											var queAdditional = `SELECT * FROM jaze_job_cv_additional_info WHERE group_id IN 
														(SELECT group_id FROM jaze_job_cv_additional_info WHERE meta_key = 'account_id' AND meta_value =?)`;

														DB.query(queAdditional, [arrParams.account_id], function(daddi, error){

									 	 					if(typeof daddi !== 'undefined' && daddi !== null && parseInt(daddi.length) > 0)
									 	 					{
																
																var delLang = `DELETE FROM jaze_job_cv_additional_info WHERE group_id=?`
						 	 									methods.deleteCvByAccount(delLang,daddi[0].group_id,function(retAddi){
				 	 												if(parseInt(retdelLang) === 1){
				 	 													return_result(1);
																	}
																	else
																	{
																		return_result(1);
																	}
				 	 											});
															}
															else
															{
																return_result(1);
															}

														});
					 	 							}
					 	 							else
					 	 							{
				 	 									return_result(1);
					 	 							}

												});

					 	 					}
					 	 					else
					 	 					{


			 	 								var queAdditional = `SELECT * FROM jaze_job_cv_additional_info WHERE group_id IN 
												(SELECT group_id FROM jaze_job_cv_additional_info WHERE meta_key = 'account_id' AND meta_value =?)`;

												DB.query(queAdditional, [arrParams.account_id], function(daddi, error){

							 	 					if(typeof daddi !== 'undefined' && daddi !== null && parseInt(daddi.length) > 0)
							 	 					{
														
														var delLang = `DELETE FROM jaze_job_cv_additional_info WHERE group_id=?`
				 	 									methods.deleteCvByAccount(delLang,daddi[0].group_id,function(retAddi){
		 	 												if(parseInt(retdelLang) === 1){
		 	 													return_result(1);
															}
															else
															{
																return_result(1);
															}
		 	 											});
													}
													else
													{
														return_result(1);
													}

												});

					 	 					}

										});

	 	 							}

 								});

				 	 		}

						});

					}
					else
					{
						return_result(0);
					}


				});

	 	   	}else{
				return_result(2);
	 	   	}
		});




	}catch(err){
		console.log("removeAccountCv",err); return_result(null); 
	}

};




methods.deleteCvByAccount = function(query,parameter,return_result){

	
	DB.query(query,[parameter], function(data, error){

	 	if(parseInt(data.affectedRows) > 0){
				return_result(1);
 	   	}else{
				return_result(0);
 	   	}

	});
};

//----------------------------- add remove account cv end---------------------------------------//



methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
};

methods.convertMultiMetaArrayCompany =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndexCompany(list,arrvalues[i].account_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndexCompany(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].account_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}


methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].group_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}


methods.convertMultiMetaArrayProduct =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndexProduct(list,arrvalues[i].product_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							product_id : arrvalues[i].product_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMultiMetaArrayProduct",err); return_result(null);}
};


function __getGroupIndexProduct(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].product_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}


function sortingArrayObject(key, order='asc') {

	return function(a, b) {
	  if(!a.hasOwnProperty(key) || 
		 !b.hasOwnProperty(key)) {
		  return 0; 
	  }
	  
	  const varA = (typeof a[key] === 'string') ? 
		a[key].toUpperCase() : a[key];
	  const varB = (typeof b[key] === 'string') ? 
		b[key].toUpperCase() : b[key];
		
	  let comparison = 0;
	  if (varA > varB) {
		comparison = 1;
	  } else if (varA < varB) {
		comparison = -1;
	  }
	  return (
		(order == 'desc') ? 
		(comparison * -1) : comparison
	  );
	};
}

function removeDuplicates(array) {
  return array.filter((a, b) => array.indexOf(a) === b)
};


methods.getCurrency =  function(currency_id,return_result){
	try
	{
		DB.query("SELECT code FROM currency_master WHERE currency_id=?",[currency_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
				if(data[0].code){
					return_result(data[0].code);
				}else{
					return_result('');
				}
			
			}else{
				return_result('');
			}

		});

	}
	catch(err)
	{ console.log("getCurrency",err); return_result(null);}
};

methods.__getCountryDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT country_name FROM country  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getStateDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT state_name FROM states  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].state_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getCityDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT city_name FROM cities  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].city_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};


methods.__getNationality = function(arrParams,return_result){

	try{

		if(parseInt(arrParams) !==0){

			let query  = " SELECT country_name FROM country  WHERE id =? ";
			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
			
		}else{
			return_result('');
		}
	
	}
	catch(err)
	{
		return_result('')
	}
};



methods.insertTBQuery = function(tbName,grpId,arrInsert,return_result){

	try
	{
		for (var meta_key in arrInsert) 
		{
			var quer = "insert into "+tbName+" (group_id, meta_key, meta_value) VALUES ('"+grpId+"','"+meta_key+"','"+arrInsert[meta_key]+"');";
				DB.query(quer,null, function(data, error){
				if(data == null){
					return_result(0);
				}
			});
		};
		return_result(grpId);
	}
	catch(err)
	{
		console.log(err);
		return_result(0);
	}
}


function formatDate(current_datetime) {
	var dt = new Date(current_datetime);
	const months = ["January", "February", "March","April", "May", "June", "July", "August", "September", "October", "November", "December"];
	let formatted_date = dt.getDate() + " " + months[dt.getMonth()] + " " +dt.getFullYear() + " " +dt.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
	return formatted_date;
}


function formatDateOnly(current_datetime) {
	var dt = new Date(current_datetime);
	const months = ["January", "February", "March","April", "May", "June", "July", "August", "September", "October", "November", "December"];
	let formatted_date = dt.getDate() + " " + months[dt.getMonth()] + " " +dt.getFullYear();
	return formatted_date;
}


var sortBy = (function () {
	var toString = Object.prototype.toString,
		// default parser function
		parse = function (x) { return x; },
		// gets the item to be sorted
		getItem = function (x) {
		  var isObject = x != null && typeof x === "object";
		  var isProp = isObject && this.prop in x;
		  return this.parser(isProp ? x[this.prop] : x);
		};
		
	/**
	 * Sorts an array of elements.
	 *
	 * @param {Array} array: the collection to sort
	 * @param {Object} cfg: the configuration options
	 * @property {String}   cfg.prop: property name (if it is an Array of objects)
	 * @property {Boolean}  cfg.desc: determines whether the sort is descending
	 * @property {Function} cfg.parser: function to parse the items to expected type
	 * @return {Array}
	 */
	return function sortby (array, cfg) {
	  if (!(array instanceof Array && array.length)) return [];
	  if (toString.call(cfg) !== "[object Object]") cfg = {};
	  if (typeof cfg.parser !== "function") cfg.parser = parse;
	  cfg.desc = !!cfg.desc ? -1 : 1;
	  return array.sort(function (a, b) {
		a = getItem.call(cfg, a);
		b = getItem.call(cfg, b);
		return cfg.desc * (a < b ? -1 : +(a > b));
	  });
	};
	
  }());


module.exports = methods;
