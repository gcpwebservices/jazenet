const methods = {};
const DB = require('../../helpers/db_base.js');
const HelperGeneralJazenet = require('../helpers/HelperGeneralJazenet.js');
const HelperRestriction = require('../helpers/HelperRestriction.js');
const promiseForeach = require('promise-foreach');
// const express = require('express');
// const router = express.Router();
const dateFormat = require('dateformat');
const now = new Date();
const standardDT = dateFormat(now,'yyyy-mm-dd HH:MM:ss');

const moment = require("moment");

const fast_sort = require("fast-sort");

const mime = require('mime-types');
const multer = require('multer');
const fs = require('fs');
const http = require('http');
const download = require('download-file');
const url = require('url');
const path = require('path');
const storagePath = G_STATIC_IMG_PATH+"company/";
const STATIC_IMG_URL = G_STATIC_IMG_URL+"uploads/jazenet/company/";

//------------------------------------------------------------------- Company Profile Module Start ---------------------------------------------------------------------------//


methods.getCompanyProfile =  function(arrParams,return_result){
	try
	{
			
		var arrAccounts = [];
		var queryAccountType = `SELECT account_id,account_type FROM jaze_user_basic_details WHERE account_id =?`;

		DB.query(queryAccountType,[arrParams.account_id], function(data, error){

			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var queryAccDetails = `SELECT * FROM jaze_user_basic_details WHERE account_id =? and account_type=?`;

				DB.query(queryAccDetails,[data[0].account_id,data[0].account_type], function(allData, error){
	
					if(typeof allData !== 'undefined' && allData !== null && parseInt(allData.length) > 0)
					{

						methods.convertMultiMetaArrayCompany(allData, function(retConverted){	

							var counter = retConverted.length;		
							var jsonItems = Object.values(retConverted);

							promiseForeach.each(jsonItems,[function (val) {
								return new Promise(function (resolve, reject) {	

									methods.__getCountryDetails(val.country_id, function(retCountry){

										methods.__getStateDetails(val.state_id, function(retState){

											methods.__getCityDetails(val.city_id, function(retCity){

												methods.__getNationality(val.nationality_id, function(retNationality){

													methods.getCurrency(val.currency_id, function(retCurrency){
								
												  		var default_logo = '';

														var newObj = {};

														var currency_id = "0";

														if(typeof val.currency_id !== 'undefined' && val.currency_id !== null && parseInt(val.currency_id) > 0)
														{ currency_id = val.currency_id; }

												  		if(parseInt(data[0].account_type) === 1)
												  		{
												  			if(val.profile_logo){
																default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + val.profile_logo + '';
															}

															newObj = {
																account_id	  : val.account_id.toString(),
																account_type  : data[0].account_type.toString(),
																name		  : (val.fname +" "+val.lname).toString(),
																logo		  : default_logo,
																contact_no    : val.mobile,
																contact_phone_code : val.mobile_country_code_id,
																country_id    : val.country_id,
																state_id      : val.state_id,
																city_id       : val.city_id,
																country_name  : retCountry,
																state_name    : retState,
																city_name     : retCity,
																national_id   : val.nationality_id,
																nationality   : retNationality,
																currency      : retCurrency,
																currency_id   : currency_id.toString(),
																team_department_id : '',
																team_position : ''
															}	

												  		}
												  		else if(parseInt(data[0].account_type) === 2)
												  		{

									  						if(val.profile_logo){
																default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + val.profile_logo + '';
															}
															
															newObj = {
																account_id	  : val.account_id.toString(),
																account_type  : data[0].account_type.toString(),
																name		  : (val.fname +" "+val.lname).toString(),
																logo		  : default_logo,
																contact_no    : val.mobile,
																contact_phone_code : val.mobile_country_code_id.toString(),
																country_id    : val.country_id,
																state_id      : val.state_id,
																city_id       : val.city_id,
																country_name  : retCountry,
																state_name    : retState,
																city_name     : retCity,
																national_id   : val.nationality_id,
																nationality   : retNationality,
																currency      : retCurrency,
																currency_id   : currency_id.toString(),
																team_department_id : '',
																team_position : ''
															}	

												  		}
											  			else if(parseInt(data[0].account_type) === 3)
										  				{

			  				 								if(val.listing_images){
									  							default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/listing_images/' + val.listing_images + '';
															
															}else if(val.company_logo){
																default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/logo/' + val.company_logo + '';
															}else{
																default_logo = "";
															}

															newObj = {
																account_id	  : val.account_id.toString(),
																account_type  : data[0].account_type.toString(),
																name		  : val.company_name,
																logo		  : default_logo,
																contact_no    : val.company_phone,
																contact_phone_code : val.company_phone_code,
																country_id    : val.country_id,
																state_id      : val.state_id,
																city_id       : val.city_id,
																country_name  : retCountry,
																state_name    : retState,
																city_name     : retCity,
																national_id   : val.nationality_id,
																nationality   : retNationality,
																currency      : retCurrency,
																currency_id   : currency_id.toString(),
																team_department_id : '',
																team_position : ''
															}	
										  				}
								  						else if(parseInt(data[0].account_type) === 4)
										  				{

			  				 								if(val.team_display_pic){
									  							default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/'+val.team_display_pic;
															
															}else{
																default_logo = "";
															}

															newObj = {
																account_id	  : val.account_id.toString(),
																account_type  : data[0].account_type.toString(),
																name		  : (val.team_fname +" "+val.team_lname).toString(),
																logo		  : default_logo,
																contact_no    : val.team_mobile_no,
																contact_phone_code : val.team_mobile_code,
																country_id    : '',
																state_id      : '',
																city_id       : '',
																country_name  : '',
																state_name    : '',
																city_name     : '',
																national_id   : '',
																nationality   : '',
																currency      : '',
																currency_id   : '',
																team_department_id : val.team_department_id,
																team_position : val.team_position
															}	
										  				}

														counter -= 1;
														if (counter === 0){ 
															resolve(newObj);
														}

													});	
												});	
											});	
										});	
									
									});	
									
								})
							}],
							function (result, current) {
								if (counter === 0){ 
									return_result(result[0]);
								}
							});

						});

					}
					else
					{
						return_result(null);
					}
				});

		
			}
			else
			{
				return_result(null);
			}
		});

	}
	catch(err)
	{ console.log("getCompanyProfile",err); return_result(null);}
};

methods.getCompanyProfileDetails = function(reqParams,return_result)
{ 
	try
	{
		if(parseInt(reqParams.company_id) !== 0)
		{
			var query = " SELECT *  FROM jaze_user_basic_details WHERE meta_key not in ('company_about') and  account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key = 'status' and meta_value = '1') ";
		    	query += " and account_id = '"+reqParams.company_id+"' ";

				DB.query(query,null, function(data, error){

					if (typeof data[0] !== 'undefined' && data[0] !== null && parseInt(data.length) > 0){

						var list = [];

							Object.assign(list, {account_id : data[0].account_id});
							Object.assign(list, {account_type : data[0].account_type});

							for (var i in data) 
							{
								Object.assign(list, {[data[i].meta_key]:  data[i].meta_value});
							}
							
								methods.__getLocationDetails(list.area_id,list.city_id, function(location_details){	
									
									methods.__getCompanyCategoryName(list, function(category){	
										
										methods.__getMobileCode(list.company_phone_code, function(mobile_code){
											
											methods.__getJazecomDtatus(list.account_id, function(jazecom_status){
												
												methods.__getCompanyReviews(list.account_id, function(review_details){

													methods.__getCompanyFollowers(list.account_id, function(followers_count){

														methods.__getCompanyMenuStatus(list.account_id, function(menu_status){

															methods.__getUserFollowStatus(reqParams.account_id,list.account_id, function(follow_status){

																methods.__jobVacancyStatus(reqParams.company_id, function(vacantStatus){

																	methods.__getUserFavStatus(reqParams.account_id,list.account_id, function(fav_status){

																		methods.__getCompanyKeywords(list.account_id, function(keywords){

																			var default_logo = '';

																			if(list.company_logo !== "" && list.company_logo !== null)
																			{
																				default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+list.account_id+'/logo_thumb/thumb_' + list.company_logo + '';
																			}

																			var cover_bg_img = G_STATIC_IMG_URL+'others/mob_comp_prof.jpg';

																			if(parseInt(list.account_type) === 5)
																			{
																				cover_bg_img = G_STATIC_IMG_URL+'others/mob_comp_prof_government.jpg';
																			}


																			var newObj = {
																				company_id	    : list.account_id.toString(),
																				company_type	: list.account_type.toString(),
																				company_name	: list.company_name.toString(),
																				category_name   : category.category_name.toString(),
																				company_logo	: encodeURI(default_logo.toString()),
																				phone_code   	: mobile_code.toString(),
																				phone_no     	: list.company_phone.toString(),
																				street_name	 	: list.street_name,
																				street_no	 	: list.street_no,
																				building_name   : list.building_name,
																				building_no	 	: list.building_no,
																				area_name		: location_details.area_name,
																				city_name		: location_details.city_name,
																				state_name	 	: location_details.state_name,
																				country_name   	: location_details.country_name,
																				email 		    : list.company_email.toString(),
																				website 		: list.company_website.toString(),
																				latitude  		: list.latitude.toString(),
																				longitude  		: list.longitude.toString(),
																				jaze_call		: jazecom_status.jaze_call.toString(),
																				jaze_chat		: jazecom_status.jaze_chat.toString(),
																				jaze_mail		: jazecom_status.jaze_mail.toString(),
																				jaze_request	: jazecom_status.jaze_diary.toString(),
																				company_about_url	: G_STATIC_WEB_URL+"jazecompany/jazecompanyAbout/"+list.account_id,
																				order_online_status	: "0",
																				review_rating	: review_details.toString(),
																				followers_count	: followers_count.toString(),
																				cover_bg_img	: cover_bg_img.toString(),
																				user_follow_status : follow_status,
																				user_fav_status    : fav_status,
																				keywords		: keywords,
																				menu_vacancies  : vacantStatus

																			};

																			newObj = Object.assign({}, newObj, menu_status);

																			return_result(newObj); 

																		});
																	});
																});
															});
														});
													});
												});	
											});								
										});
									});
							});
					}
					else
					{
						return_result(null); 
					}
				});
		}
		else
		{
			return_result(null); 
		}
	}
	catch(err)
	{ 
		console.log("getCompanyProfileDetails",err); 
		return_result(null); 
	}	
}

methods.__getJazecomDtatus = function(account_id,return_result)
{ 
	try
	{
		var obj_return = {
			jaze_call : "0",
			jaze_chat : "0",
			jaze_mail : "0",
			jaze_diary : "0"
		};	

		if(parseInt(account_id) !== 0)
		{
			var query = " SELECT *  FROM jaze_user_jazecome_status WHERE meta_key not in ('account_id','jaze_pay','jaze_feed') and  group_id in (SELECT group_id  FROM jaze_user_jazecome_status WHERE  meta_key = 'account_id' and meta_value = '"+account_id+"') ";
		   
			DB.query(query,null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					HelperRestriction.convertMultiMetaArray(data, function(data_result){	
								
						if(data_result !== null)
						{
							var result = data_result[0];

							delete result.group_id;

							return_result(result);
						}
					});
				}
				else
				{ return_result(obj_return); }
			});
		}
		else
		{ return_result(obj_return); }
	}
	catch(err)
	{ 
		console.log("getJazecomMenus1",err); 
		return_result(arrRows); 
	}	
}

//Get Company Category Name 
methods.__getCompanyCategoryName =  function (account_list,return_result){
	
	var return_obj = {
			account_type  : "",
			title_name 	  : "",
			category_name : ""
		};

	try
	{
		if(account_list !== null)
		{
			if(parseInt(account_list.account_type) === 3)
			{
				if((Object.keys(account_list)).indexOf("category_id") !== -1)
				{
					var query = "  SELECT meta_value  FROM jaze_category_list WHERE  meta_key = 'cate_name' and group_id = '"+account_list.category_id+"'; ";
					
					DB.query(query, null, function(data, error){
					
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_obj = {
								account_type  : "3",
								title_name 	  : "company",
								category_name : data[0].meta_value
							};

							return_result(return_obj);
						}
						else
						{
							return_obj = {
								account_type  : "3",
								title_name 	  : "company",
								category_name : "company"
							};

							return_result(return_obj);
						}
					});
				}
				else
				{
					return_obj = {
						account_type  : "3",
						title_name 	  : "company",
						category_name : "company"
					};

					return_result(return_obj);
				}
			}
			else if(parseInt(account_list.account_type) === 5)
			{
				return_obj = {
					account_type  : "5",
					title_name 	  : "government",
					category_name : "government"
				};

				return_result(return_obj);
			}
		}
		else
		{
			return_result(return_obj);
		}
	}
	catch(err)
	{ console.log("__getCompanyCategoryName",err); return_result(return_obj); }	
};

methods.__getCompanyReviews =  function (account_id,return_result){
	
	try
	{
		if(parseInt(account_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_reviews WHERE  meta_key IN ('rating') ";
				query += " AND group_id IN (SELECT group_id FROM jaze_reviews WHERE meta_key = 'profile_account_id'  AND meta_value = "+account_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_reviews WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var no_reviews = 0;

							var tot_stars = 0;

							for (var i in data_result) 
							{
								if( parseFloat(data_result[i].rating) > 0)
								{
									tot_stars =  tot_stars + parseFloat(data_result[i].rating);
									no_reviews++;
								}
							}

							var average = (tot_stars / no_reviews).toFixed(1);

							return_result(average);

						});
					}
					else
					{
						return_result('0');
					}
			    });
		}
		else
		{
			return_result('0');
		}
	}
	catch(err)
	{ console.log("__getCompanyReviews",err); return_result('0'); }	
};

methods.__getCompanyFollowers =  function (company_id,return_result){
	
	try
	{
		if(parseInt(company_id) !== 0)
		{
			var query  = " SELECT COUNT(DISTINCT group_id) AS count FROM jaze_follower_groups_meta WHERE meta_key IN ('follow_groups') and ";
				query += "group_id IN (  SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'follow_account_id' AND meta_value = "+company_id+" )  ";
				
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0].count);
					}
					else
					{
						return_result('0');
					}
			    });
		}
		else
		{
			return_result('0');
		}
	}
	catch(err)
	{ console.log("__getCompanyFollowers",err); return_result('company'); }	
};

methods.__getCompanyMenuStatus =  function (account_id,return_result){
	
	try
	{
		if(parseInt(account_id) !== 0)
		{

			var tb_array = ['jaze_company_photo','jaze_products','jaze_company_notice','jaze_portfolios','jaze_company_team_details','jaze_company_branch_meta','jaze_company_division','jaze_company_accreditation'];

			var counter = tb_array.length;

			var array_rows = [];

			Object.assign(array_rows, {menu_about : "1"});

			//Object.assign(array_rows, {menu_vacancies : "0"});

			Object.assign(array_rows, {menu_reviews : "1"});
						
				if(parseInt(counter) !== 0)
				{
					promiseForeach.each(tb_array,[function (list) {
					
						return new Promise(function (resolve, reject) {	

							var query  = "";

								if(list === "jaze_products")
								{
									query  = "SELECT count(id) AS count FROM "+list+" WHERE `account_id` = '"+account_id+"' ";
								}
								else
								{
									query  = " SELECT COUNT(DISTINCT group_id) AS count FROM "+list+" WHERE  ";
									query += " group_id IN (  SELECT group_id FROM "+list+" WHERE meta_key = 'status' AND meta_value = '1'  ";
									query += " and group_id IN (  SELECT group_id FROM "+list+" WHERE meta_key = 'account_id' AND meta_value = "+account_id+" ))  ";	
								}
							    
								DB.query(query, null, function(data, error){
									
									var obj = {};

									var key = list.replace('jaze_company_','');

									key = key.replace('jaze_','');

									key = key.replace('_details','');

									key = key.replace('_meta','');

									key = "menu_"+key;


									if(parseInt(data[0].count) !== 0){

										obj[key] = "1";
									}
									else
									{
										if(key === "menu_branch")
										{
											obj[key] = "1";
										}
										else
										{
											obj[key] = "0";
										}
										
									}
									
									array_rows = Object.assign({}, array_rows, obj);

									counter -= 1;
									resolve(array_rows);
								});
						})
					}],
					function (result, current) {			
						if (counter === 0)
						{ return_result(result[0]); }
					});	
				}			
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("__getCompanyMenuStatus",err); return_result(null); }	
};


methods.__jobVacancyStatus =  function (account_id,return_result){
	
	try
	{
		if(parseInt(account_id) !== 0 )
		{
			var query  = `SELECT * FROM jaze_job_post_job WHERE group_id IN (SELECT group_id FROM jaze_job_post_job WHERE meta_key = 'account_id' AND meta_value =?) 
							AND group_id IN (SELECT group_id FROM jaze_job_post_job WHERE meta_key = 'status' AND meta_value ='1')`;

				
				DB.query(query, [account_id], function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result('1');
					}
					else
					{
						return_result('0');
					}
				});
		}
		else
		{
			return_result('0');
		}
	}
	catch(err)
	{ console.log("__jobVacancyStatus",err); return_result('0'); }	
};

methods.__getUserFollowStatus =  function (account_id,company_id,return_result){
	
	try
	{
		if(parseInt(account_id) !== 0 && parseInt(company_id) !== 0)
		{
			var query  = " SELECT group_id  FROM jaze_follower_groups_meta WHERE meta_key IN ('follow_groups') and ";
				query += " group_id IN (  SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'account_id' AND meta_value = "+account_id+"   ";
				query += " and group_id IN (  SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'follow_account_id' AND meta_value = "+company_id+" ))  ";
				
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result('1');
					}
					else
					{
						return_result('0');
					}
				});
		}
		else
		{
			return_result('0');
		}
	}
	catch(err)
	{ console.log("__getUserFollowStatus",err); return_result('0'); }	
};

methods.__getUserFavStatus =  function (account_id,company_id,return_result){
	
	try
	{
		if(parseInt(account_id) !== 0 && parseInt(company_id) !== 0)
		{
			var query  = " SELECT group_id  FROM jaze_user_favorite_list WHERE  ";
				query += " group_id IN (  SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'account_id' AND meta_value = "+account_id+"   ";
				query += " and group_id IN (  SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'fav_account_id' AND meta_value = "+company_id+" ))  ";
				
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result('1');
					}
					else
					{
						return_result('0');
					}
				});
		}
		else
		{
			return_result('0');
		}
	}
	catch(err)
	{ console.log("__getUserFavStatus",err); return_result('0'); }	
};

methods.__getCompanyKeywords =  function (company_id,return_result){
	
	try
	{
		if(parseInt(company_id) !== 0)
		{
			var query  = " SELECT meta_value  FROM jaze_company_keywords WHERE meta_key IN ('keywords') and ";
				query += " group_id IN (  SELECT group_id FROM jaze_company_keywords WHERE meta_key = 'account_id' AND meta_value = "+company_id+"   ";
				query += " and group_id IN (  SELECT group_id FROM jaze_company_keywords WHERE meta_key = 'status' AND meta_value = 1 ))  ";
				
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0].meta_value);
					}
					else
					{
						return_result('');
					}
				});
		}
		else
		{
			return_result('');
		}
	}
	catch(err)
	{ console.log("__getCompanyKeywords",err); return_result(''); }	
};

//------------------------------------------------------------------- Company Profile Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Accreditation Module Start ---------------------------------------------------------------------------//

methods.getAccreditationList  =  function (company_id,return_result){
	
	try
	{
		if(parseInt(company_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_company_accreditation WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_company_accreditation WHERE meta_key = 'account_id'  AND meta_value = "+company_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_company_accreditation WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											var objAccRres = {
												id            : list.group_id.toString(),
												title         : list.accreditation_name.toString(),
												image         : G_STATIC_IMG_URL+'uploads/jazenet/company/'+list.account_id+'/jazeaccreditations/'+list.accreditation_pic.toString(),
												date          : dateFormat(list.create_date.toString(), 'yyyy-mm-dd HH:MM:ss')
											};

											array_rows.push(objAccRres);

											resolve(array_rows);

											counter -= 1;
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ 
											var returnResult = result[0];

											if(returnResult !== null)
											{
												//sorting 
												sortBy(returnResult, {
													prop: "date",
													desc: true,
													parser: function(item) { return new Date(item); }
												});
											}
											
											return_result(returnResult);
										}
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getAccreditationList",err); return_result(null); }	
};



methods.addAccreditation  =  function (arrParams,return_result){
	
	try
	{

		HelperRestriction.getNewGroupId('jaze_company_accreditation', function (group_id) {

			if(parseInt(group_id) !== 0){

				var	arrInsert = {
					account_id	  		: arrParams.company_id,
					accreditation_pic	: arrParams.file_name,
					accreditation_name  : arrParams.title,
					create_date   		: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
					status 		  		: '1'						
				};

				
		  		var querInsert = `insert into jaze_company_accreditation (group_id, meta_key, meta_value) VALUES (?,?,?)`;

			 	for (var key in arrInsert) 
			 	{
					DB.query(querInsert,[group_id,key,arrInsert[key]], function(data, error){});
			  	};
			  	return_result(1);
			}
		});


	}
	catch(err)
	{ console.log("addAccreditation",err); return_result(0); }	
};

methods.updateAccreditation  =  function (arrParams,return_result){
	
	try
	{

		var query  = " SELECT * FROM jaze_company_accreditation WHERE group_id =? ";
			
		DB.query(query, [arrParams.id], function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				var	arrUpdate = {
					account_id	  		: arrParams.company_id,
					accreditation_pic	: arrParams.file_name,
					accreditation_name  : arrParams.title,
					create_date   		: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
					status 		  		: '1'						
				};

				var updateQue = `update jaze_company_accreditation SET meta_value =? WHERE meta_key =? AND group_id =?`;
						
				for (var key in arrUpdate) 
			 	{	
				     if(arrUpdate[key] !== null && arrUpdate[key] != ""){

						DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(datau, error){
					 		if(datau !== null){
				 				return_result(1);
					 	   	}else{
				 	   			return_result(0);
					 	   	}
						});
					}
				}
			}
			else
			{
				return_result(2);
			}

		});

	}
	catch(err)
	{ console.log("updateAccreditation",err); return_result(0); }	
};

methods.delAccreditation  =  function (arrParams,return_result){
	
	try
	{
		var query  = " SELECT * FROM jaze_company_accreditation WHERE group_id =? ";
		DB.query(query, [arrParams.id], function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				var query  = " DELETE FROM `jaze_company_accreditation` WHERE `group_id` = '"+arrParams.id+"'; ";
				DB.query(query,null, function(data_check, error){
					return_result(1);
				});
			}
			else
			{
				return_result(2);
			}

		});
	}
	catch(err)
	{ console.log("delAccreditation",err); return_result(0); }	
};






//------------------------------------------------------------------- Accreditation Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Branches Module Start ---------------------------------------------------------------------------//

methods.getBranchList  =  function (reqParams,return_result){
	
	try
	{
		if(reqParams !== null)
		{
			if(parseInt(reqParams.company_id) !== 0 && parseInt(reqParams.result_flag) === 0)
			{
				methods.__getOwnBranchList(reqParams.company_id, function(compList){	

					return_result(compList);

				});
			}
			else if(parseInt(reqParams.result_flag) !== 0 && parseInt(reqParams.branch_id) !== 0)
			{
				methods.__getSearchBranchAccountList(reqParams, function(compList){	
					
					methods.__getSearchBranchChildAccountList(compList.company_id_list,reqParams, function(childList){	

						var returnresult = {};
						
						if(compList.company_list !== null && parseInt(compList.company_list.length) > 0)
						{ returnresult = compList.company_list; }

						if(childList !== null && parseInt(childList.length) > 0)
						{ 
							if(returnresult !== null && parseInt(returnresult.length) > 0)
							{
								returnresult = Object.assign({}, returnresult,childList); 
							}
							else
							{ returnresult = childList; }
						}


						if(returnresult !== null && parseInt(returnresult.length) > 0)
						{ 
							return_result(returnresult);
						}
						else
						{
							return_result([]);
						}
					});

				});
			}
			else
			{ return_result(null); }
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("getBranchList",err); return_result(null); }	
};

methods.__getOwnBranchList  =  function (company_array,return_result){
	
	try
	{
		if(company_array !== null)
		{
			var query = " SELECT group_concat(meta_value) as branch_id  FROM jaze_company_branch_groups WHERE  meta_key in ('branch_id')  and ";
				query += " group_id IN (SELECT group_id FROM jaze_company_branch_groups WHERE meta_key = 'account_id'  AND meta_value in ("+company_array+") ";
				query += " AND group_id IN (SELECT group_id FROM jaze_company_branch_groups WHERE meta_key = 'status' AND meta_value = 1 )); ";

			    DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var query  = " SELECT * FROM jaze_company_branch_meta WHERE  ";
							query += " group_id IN (SELECT group_id FROM jaze_company_branch_meta WHERE meta_key = 'branch_id'  AND meta_value in ("+data[0].branch_id+")  ";
							query += " AND group_id IN (SELECT group_id FROM jaze_company_branch_meta WHERE meta_key = 'status' AND meta_value = 1 )); ";
							
						DB.query(query, null, function(data, error){
							
							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
		
								HelperRestriction.convertMultiMetaArray(data, function(data_result){
								
									var array_rows = [];
		
									var counter = data_result.length;
		
									if(parseInt(counter) !== 0)
									{
											promiseForeach.each(data_result,[function (list) {
		
												return new Promise(function (resolve, reject) {
		
													methods.__getLocationDetails(list.area_id,list.city_id, function(location_details){	
		
														methods.__getMobileCode(list.phone_code_1, function(phone_code_1){
		
																methods.__getMobileCode(list.phone_code_2, function(phone_code_2){
		
																	var objAccRres = {
																		id            	    : list.branch_id.toString(),
																		account_id          : list.account_id.toString(),
																		name            	: list.branch_name.toString(),
																		email   			: list.branch_email.toString(),
																		website   			: list.website.toString(),
																		phone_code_1   		: phone_code_1.toString(),
																		phone_number_1   	: list.phone_1.toString(),
																		phone_code_2   		: phone_code_2.toString(),
																		phone_number_2   	: list.phone_2.toString(),
																		street_no	 		: list.street_number,
																		street_name	 		: list.street_name,
																		building_no	 		: list.building_no,
																		building_name   	: list.building_name,
																		area_name			: location_details.area_name,
																		city_name			: location_details.city_name,
																		state_name	 		: location_details.state_name,
																		country_name   		: location_details.country_name,
																		latitude  			: list.latitude.toString(),
																		longitude  			: list.longitude.toString(),
																		link_company_id		: "0"
																	};
		
																	array_rows.push(objAccRres);
		
																	resolve(array_rows);
		
																	counter -= 1;
																});
														});
													});
												});
											}],
											function (result, current) {
												if (counter === 0)
												{  return_result(result[0]); }
											});	
									}
									else
									{ return_result(null); }
								});
							}
							else
							{
								return_result(null);
							}
						});
					}
					else
					{ return_result(null); }
				});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("getAccreditationList",err); return_result(null); }	
};

methods.__getSearchBranchAccountList = function (reqParams,return_result){
	
	try
	{

	var return_array = {
		company_id_list : reqParams.company_id,
		company_list : ""
	};

		if(reqParams !== null)
		{
			var fild_key , fild_value = "";

			if(parseInt(reqParams.result_flag) === 1)
			{
				fild_key = "country_id"; 
				fild_value = reqParams.country_id;
			}
			else if(parseInt(reqParams.result_flag) === 2)
			{
				fild_key = "state_id"; 
				fild_value = reqParams.state_id;
			}
			else if(parseInt(reqParams.result_flag) === 3)
			{
				fild_key = "city_id"; 
				fild_value = reqParams.city_id;
			}

			//get main account 
			var query  = " SELECT group_concat(meta_value) as account_id FROM jaze_company_branch_groups WHERE meta_key in('account_id') ";
				query += " and `group_id` in (SELECT group_id FROM jaze_company_branch_groups WHERE `meta_key` = 'branch_id' AND `meta_value` = '" +reqParams.branch_id+ "'  ";
            	query += " and `group_id` IN(SELECT `group_id` from jaze_company_branch_groups WHERE `meta_key` = 'branch_type' and `meta_value` = 3 )) ";  

				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var company_id_list = data[0].account_id;
						
						var query  = " SELECT * FROM jaze_user_basic_details WHERE meta_key not in('company_about') ";
							query += " and `account_id` in (SELECT account_id FROM jaze_user_basic_details WHERE `meta_key` = 'status' AND `meta_value` = '1'  ";
							query += " and `account_id` IN(SELECT `account_id` from jaze_user_basic_details WHERE `meta_key` = '"+fild_key+"' and `meta_value` = '"+fild_value+"' ";
							query += " and `account_id` in (SELECT account_id FROM jaze_user_basic_details WHERE `meta_key` = 'status' AND `meta_value` = '1')))  ";
							query += " and `account_id` in ("+company_id_list+") and  account_type = '3' ";   
							
							DB.query(query, null, function(data, error){
								
								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

									HelperGeneralJazenet.convertMultiMetaArray(data, function(data_result){

										var array_rows = [];
									
										var counter = data_result.length;
										
										if(parseInt(counter) !== 0)
										{
												promiseForeach.each(data_result,[function (list) {
			
													return new Promise(function (resolve, reject) {
														
														methods.__getLocationDetails(list.area_id,list.city_id, function(location_details){	
			
															methods.__getMobileCode(list.company_phone_code, function(phone_code_1){
			
																	methods.__getMobileCode(list.company_phone_code_2, function(phone_code_2){
			
																		var objAccRres = {
																			id            	    : list.account_id.toString(),
																			account_id          : list.account_id.toString(),
																			name            	: list.company_name.toString(),
																			email   			: list.company_email.toString(),
																			website   			: list.company_website.toString(),
																			phone_code_1   		: phone_code_1.toString(),
																			phone_number_1   	: list.company_phone.toString(),
																			phone_code_2   		: phone_code_2.toString(),
																			phone_number_2   	: list.company_phone_2.toString(),
																			street_no	 		: list.street_no,
																			street_name	 		: list.street_name,
																			building_no	 		: list.building_no,
																			building_name   	: list.building_name,
																			area_name			: location_details.area_name,
																			city_name			: location_details.city_name,
																			state_name	 		: location_details.state_name,
																			country_name   		: location_details.country_name,
																			latitude  			: list.latitude.toString(),
																			longitude  			: list.longitude.toString(),
																			link_company_id		: list.account_id.toString()
																		};
																		
																		array_rows.push(objAccRres);
			
																		resolve(array_rows);
			
																		counter -= 1;
																	});
															});
														});
													});
												}],
												function (result, current) {
													if (counter === 0)
													{ 
														return_array = {
															company_id_list : company_id_list,
															company_list : result[0]
														};
														
														return_result(return_array);
													}
												});	
										}
										else
										{ return_result(return_array); }

									});
								}
								else
								{ return_result(return_array); }
							});
					}
					else
					{ return_result(return_array); }
				});
		}
		else
		{ return_result(return_array); }
	}
	catch(err)
	{ console.log("getBranchAccountList",err); return_result(return_array); }	
};

methods.__getSearchBranchChildAccountList  =  function (account_list,reqParams,return_result){
	
	try
	{
		if(account_list !== "" && reqParams !== null)
		{
			var query  = " SELECT group_concat(meta_value) as account_id FROM jaze_company_branch_groups WHERE meta_key in('branch_id') ";
				query += " and `group_id` in (SELECT group_id FROM jaze_company_branch_groups WHERE `meta_key` = 'account_id' AND `meta_value` in ("+account_list+")  ";
				query += " and `group_id` IN(SELECT `group_id` from jaze_company_branch_groups WHERE `meta_key` = 'status' and `meta_value` = '1' ";
				query += " and `group_id` IN(SELECT `group_id` from jaze_company_branch_groups WHERE `meta_key` = 'branch_type' and `meta_value` in (1,2)))) ";            

				DB.query(query, null, function(data, error){
				
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
						if(data[0].account_id !== null && data[0].account_id !== ""){


							var fild_key , fild_value = "";

								if(parseInt(reqParams.result_flag) === 1)
								{
									fild_key = "country_id"; 
									fild_value = reqParams.country_id;
								}
								else if(parseInt(reqParams.result_flag) === 2)
								{
									fild_key = "state_id"; 
									fild_value = reqParams.state_id;
								}
								else if(parseInt(reqParams.result_flag) === 3)
								{
									fild_key = "city_id"; 
									fild_value = reqParams.city_id;
								}

							var query  = " SELECT * FROM jaze_company_branch_meta WHERE  ";
								query += " `group_id` in (SELECT group_id FROM jaze_company_branch_meta WHERE `meta_key` = 'branch_id' AND `meta_value` in ("+data[0].account_id+")  ";
								query += " and `group_id` IN( SELECT `group_id` from jaze_company_branch_meta WHERE `meta_key` = '"+fild_key+"' and `meta_value` = '"+fild_value+"' ";
								query += " and `group_id` IN( SELECT `group_id` from jaze_company_branch_meta WHERE `meta_key` = 'status' and `meta_value` = '1' ))) ";            

								DB.query(query, null, function(data, error){
									
									if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

										HelperRestriction.convertMultiMetaArray(data, function(data_result){

											var array_rows = [];
										
											var counter = data_result.length;
											
											if(parseInt(counter) !== 0)
											{
													promiseForeach.each(data_result,[function (list) {
				
														return new Promise(function (resolve, reject) {
															
															methods.__getLocationDetails(list.area_id,list.city_id, function(location_details){	
			
																methods.__getMobileCode(list.phone_code_1, function(phone_code_1){
				
																		methods.__getMobileCode(list.phone_code_2, function(phone_code_2){
				
																			var objAccRres = {
																				id            	    : list.branch_id.toString(),
																				account_id          : list.account_id.toString(),
																				name            	: list.branch_name.toString(),
																				email   			: list.branch_email.toString(),
																				website   			: list.website.toString(),
																				phone_code_1   		: phone_code_1.toString(),
																				phone_number_1   	: list.phone_1.toString(),
																				phone_code_2   		: phone_code_2.toString(),
																				phone_number_2   	: list.phone_2.toString(),
																				street_no	 		: list.street_number,
																				street_name	 		: list.street_name,
																				building_no	 		: list.building_no,
																				building_name   	: list.building_name,
																				area_name			: location_details.area_name,
																				city_name			: location_details.city_name,
																				state_name	 		: location_details.state_name,
																				country_name   		: location_details.country_name,
																				latitude  			: list.latitude.toString(),
																				longitude  			: list.longitude.toString(),
																				link_company_id		: "0"
																			};
				
																			array_rows.push(objAccRres);
				
																			resolve(array_rows);
				
																			counter -= 1;
																		});
																});
															});
														});
													}],
													function (result, current) {
														if (counter === 0)
														{  return_result(result[0]); }
													});	
											}
											else
											{ return_result(return_array); }

										});
									}
								});	
						}
						else
						{ return_result(null); }
					}
					else
					{ return_result(null); }
			    });
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("__getSearchBranchChildAccountList",err);  return_result(null); }	
};


/*
* Branch Location List
*/
methods.getBranchLocationList  =  function (company_id,return_result){
	
	try
	{
		if(parseInt(company_id) !== 0)
		{
			//parent Branch List 
			methods.__getBranchGroupsParentList(company_id, function(parent_result){

				methods.__getBranchCompanyAccountList(parent_result, function(branch_main_accounts){
					
					methods.__getBranchChildAccountList(branch_main_accounts.account_list, function(branch_city_id){

						var city_id = [];

						if(branch_main_accounts.city_id !== null)
						{ city_id = branch_main_accounts.city_id.split(','); }

						if(branch_city_id !== null)
						{ city_id = city_id.concat(branch_city_id.split(','));}

						    city_id = Array.from(new Set(city_id))

							var counter = city_id.length;
							
							var array_rows = [];

							var country_rows = [];

							var state_rows = [];

							var city_rows = [];

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(city_id,[function (list) {
				
									return new Promise(function (resolve, reject) {

										methods.__getLocationDetails(0,list, function(city_details){

											if(country_rows.indexOf(city_details.country_id)== -1)
											{
												var countryObj = {
													country_id	  		: city_details.country_id.toString(),
													country_name  		: city_details.country_name.toString(),
													state_id	  		: "",
													state_name    		: "",
													city_id	  			: "",
													city_name    		: "",
													result_flag    		: "1",
													branch_id			:parent_result.branch_id.toString()
												};

												country_rows.push(city_details.country_id);

												array_rows.push(countryObj);
											}

											if(state_rows.indexOf(city_details.state_id)== -1)
											{
												var stateObj = {
													country_id	  		: city_details.country_id.toString(),
													country_name  		: city_details.country_name.toString(),
													state_id	  		: city_details.state_id.toString(),
													state_name    		: city_details.state_name.toString(),
													city_id	  			: "",
													city_name    		: "",
													result_flag    		: "2",
													branch_id			:parent_result.branch_id.toString()
												};

												state_rows.push(city_details.state_id);

												array_rows.push(stateObj);
											}

											if(city_rows.indexOf(city_details.city_id)== -1)
											{
												var cityObj = {
													country_id	  		: city_details.country_id.toString(),
													country_name  		: city_details.country_name.toString(),
													state_id	  		: city_details.state_id.toString(),
													state_name    		: city_details.state_name.toString(),
													city_id	  			: city_details.city_id.toString(),
													city_name    		: city_details.city_name.toString(),
													result_flag    		: "3",
													branch_id			:parent_result.branch_id.toString()
												};

												city_rows.push(city_details.city_id);

												array_rows.push(cityObj);
											}

											counter -= 1;
									
											resolve(array_rows);

										});
									})
								}],
								function (result, current) {
									
									if (counter === 0)
									{ return_result(result[0]);  }
								});
							}
							else
							{ return_result(null); }		
					});
				});
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("getBranchLocationList",err); return_result(null); }	
};

methods.__getBranchGroupsParentList  =  function (company_id,return_result){
	
	var return_array = {
		branch_id : "0",
		head_company_id : company_id
	};	

	try
	{
		if(parseInt(company_id) !== 0)
		{
			var query  = " SELECT group_id FROM jaze_company_branch_jazeid WHERE  ";
				query += " `group_id` in (SELECT group_id FROM jaze_company_branch_jazeid WHERE `meta_key` = 'account_id' AND `meta_value` = '" +company_id+ "'  ";
            	query += " and `group_id` IN(SELECT `group_id` from jaze_company_branch_jazeid WHERE `meta_key` = 'status' and `meta_value` = '1' ";
            	query += " and `group_id` IN(SELECT `group_id` from jaze_company_branch_jazeid WHERE `meta_key` = 'branch_type' and `meta_value` in (1) ))) ";

			    DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_array = {
							branch_id : data[0].group_id,
							head_company_id : company_id
						};	

						return_result(return_array);
					}
					else
					{ 
						var query  = " SELECT meta_value FROM jaze_company_branch_groups WHERE meta_key in('branch_id') ";
							query += " and `group_id` in (SELECT group_id FROM jaze_company_branch_groups WHERE `meta_key` = 'account_id' AND `meta_value` = '"  +company_id+ "'  ";
                        	query += " and `group_id` IN(SELECT `group_id` from jaze_company_branch_groups WHERE `meta_key` = 'status' and `meta_value` = '1' ";
                        	query += " and `group_id` IN(SELECT `group_id` from jaze_company_branch_groups WHERE `meta_key` = 'branch_type' and `meta_value` in (3) ))) ";
							
							DB.query(query, null, function(data, error){
					
								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

									var query  = " SELECT meta_value FROM jaze_company_branch_jazeid WHERE `meta_key` = 'account_id' AND `group_id` = '" + data[0].meta_value + "' ";

									DB.query(query, null, function(datajaze, error){
					
										if(typeof datajaze !== 'undefined' && datajaze !== null && parseInt(datajaze.length) > 0){

											return_array = {
												branch_id : data[0].meta_value,
												head_company_id : datajaze[0].meta_value
											};	
											
											return_result(return_array);

										}
										else
										{ return_result(return_array); }
									});
								}
								else
								{ 
									var query  = " SELECT group_id FROM jaze_company_branch_jazeid WHERE ";
										query += "   `group_id` in (SELECT group_id FROM jaze_company_branch_jazeid WHERE `meta_key` = 'account_id' AND `meta_value` = '"  +company_id+ "'  ";
                        				query += " and `group_id` IN(SELECT `group_id` from jaze_company_branch_jazeid WHERE `meta_key` = 'status' and `meta_value` = '1' )) ";
							
										DB.query(query, null, function(datajaze, error){
						
											if(typeof datajaze !== 'undefined' && datajaze !== null && parseInt(datajaze.length) > 0){

												return_array = {
													branch_id : datajaze[0].group_id,
													head_company_id : company_id
												};	
												
												return_result(return_array);

											}
											else
											{ return_result(return_array); }
										});
								 }
							});
					}
				});
		}
		else
		{ return_result(return_array); }
	}
	catch(err)
	{ console.log("__getBranchGroupsParentList",err); return_result(return_array); }	
};

methods.__getBranchCompanyAccountList  =  function (parent_result,return_result){
	
	var return_array = {
		account_list : parent_result.head_company_id,
		city_id : ""
	};

	try
	{
		if(parseInt(parent_result.branch_id) !== 0)
		{
			var query  = " SELECT group_concat(meta_value) as account_id FROM jaze_company_branch_groups WHERE meta_key in('account_id') ";
				query += " and `group_id` in (SELECT group_id FROM jaze_company_branch_groups WHERE `meta_key` = 'branch_id' AND `meta_value` = '" +parent_result.branch_id+ "'  ";
            	query += " and `group_id` IN(SELECT `group_id` from jaze_company_branch_groups WHERE `meta_key` = 'status' and `meta_value` = '1' ";
            	query += " and `group_id` IN(SELECT `group_id` from jaze_company_branch_groups WHERE `meta_key` = 'branch_type' and `meta_value` = 3 ))) ";  
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var account_list = data[0].account_id+','+parent_result.head_company_id;

						methods.__getBranchCompanyCityList(account_list, function(city_list){

							return_array = {
								account_list : account_list,
								city_id : city_list
							};

							return_result(return_array);
						});
					}
					else
					{ 
						methods.__getBranchCompanyCityList(parent_result.head_company_id, function(city_list){

							return_array = {
								account_list : parent_result.head_company_id,
								city_id : city_list
							};

							return_result(return_array);
						});
					}
			    });
		}
		else
		{ 
			methods.__getBranchCompanyCityList(parent_result.head_company_id, function(city_list){

				return_array = {
					account_list : parent_result.head_company_id,
					city_id : city_list
				};

				return_result(return_array);
			});
		}
	}
	catch(err)
	{ console.log("__getBranchCompanyAccountList",err);  return_result(parent_result.head_company_id); }	
};

methods.__getBranchCompanyCityList  =  function (account_list,return_result){
	
	try
	{
		if(account_list !== null)
		{
			var query  = " SELECT group_concat(DISTINCT(meta_value) SEPARATOR ',') as city_id FROM jaze_user_basic_details WHERE meta_key in('city_id') ";
				query += " and `account_id` in (SELECT account_id FROM jaze_user_basic_details WHERE `meta_key` = 'status' AND `meta_value` = '1')  ";
				query += " and `account_id` in ("+account_list+") and  account_type = '3' ";   
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0].city_id);
					}
					else
					{ return_result(null); }
			    });
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("__getBranchCompanyCityList",err);  return_result(null); }	
};

methods.__getBranchChildAccountList  =  function (account_list,return_result){
	
	try
	{
		if(account_list !== "")
		{
			var query  = " SELECT group_concat(meta_value) as account_id FROM jaze_company_branch_groups WHERE meta_key in('branch_id') ";
				query += " and `group_id` in (SELECT group_id FROM jaze_company_branch_groups WHERE `meta_key` = 'account_id' AND `meta_value` in ("+account_list+")  ";
				query += " and `group_id` IN(SELECT `group_id` from jaze_company_branch_groups WHERE `meta_key` = 'status' and `meta_value` = '1' ";
				query += " and `group_id` IN(SELECT `group_id` from jaze_company_branch_groups WHERE `meta_key` = 'branch_type' and `meta_value` in (1,2)))) ";            

				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						var query  = " SELECT group_concat(DISTINCT(meta_value) SEPARATOR ',') as city_id FROM jaze_company_branch_meta WHERE meta_key in('city_id') ";
							query += " and `group_id` in (SELECT group_id FROM jaze_company_branch_meta WHERE `meta_key` = 'branch_id' AND `meta_value` in ("+data[0].account_id+")  ";
							query += " and `group_id` IN(SELECT `group_id` from jaze_company_branch_meta WHERE `meta_key` = 'status' and `meta_value` = '1' )) ";            

							DB.query(query, null, function(data, error){
								
								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

									return_result(data[0].city_id);
								}
							});	
					}
					else
					{ return_result(null); }
			    });
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("__getBranchChildAccountList",err);  return_result(null); }	
};


methods.getBranchManagerList  =  function (reqParams,return_result){
	
	try
	{	
		var query = `SELECT * FROM jaze_company_branch_groups 
	WHERE group_id IN (SELECT group_id FROM jaze_company_branch_groups WHERE meta_key = 'account_id' AND meta_value =?)`;

		DB.query(query, [reqParams.company_id], function(data, error){
							
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				HelperRestriction.convertMultiMetaArray(data, function(data_result){
				
					var array_rows = [];

					var counter = data_result.length;

					if(parseInt(counter) !== 0)
					{
							promiseForeach.each(data_result,[function (list) {

								return new Promise(function (resolve, reject) {

									methods.__getBranchDetails(list.branch_id,list.branch_type, function(branch_details){	

										if(branch_details!=null){
											array_rows.push(branch_details);
										}

										counter -= 1;
										if (counter === 0)
										{
											resolve(array_rows);
										}
								
									});
								});
							}],
							function (result, current) {
								if (counter === 0)
								{  return_result(result[0]); }
							});	
					}
					else
					{ return_result(null); }
				});
			}
			else
			{
				return_result(null);
			}
		});
	}
	catch(err)
	{ console.log("getBranchManagerList",err); return_result(null); }	
};


methods.__getBranchDetails  =  function (branch_id,branch_type,return_result){
	
	try
	{	
		var query = `SELECT * FROM jaze_company_branch_meta 
		WHERE group_id IN (SELECT group_id FROM jaze_company_branch_meta WHERE meta_key = 'branch_id' AND meta_value =?)`;

		DB.query(query, [branch_id], function(data, error){
							
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				HelperRestriction.convertMultiMetaArray(data, function(data_result){
				
					var array_rows = [];

					var counter = data_result.length;

					if(parseInt(counter) !== 0)
					{
						promiseForeach.each(data_result,[function (list) {

							return new Promise(function (resolve, reject) {

								methods.__getMobileCode(list.phone_code_1, function(phone_code_1){

									methods.__getCityDetails(list.city_id, function(retCity){

										methods.__getAreaDetails(list.area_id, function(retArea){

											var branch_t = "Partial";
											if(parseInt(branch_type) === 1)
											{
												branch_t = "Simple";
											}

											var objAccRres = {

												id            	    : list.branch_id.toString(),
												name            	: list.branch_name.toString(),
												phone_code_1   		: phone_code_1.toString(),
												phone_number_1   	: list.phone_1.toString(),
												street_no	 		: list.street_number,
												street_name	 		: list.street_name,
												building_no	 		: list.building_number,
												building_name   	: list.building_name,
												area_name			: retArea,
												city_name			: retCity,
												branch_type         : branch_t

											};

											counter -= 1;
											if (counter === 0)
											{
												resolve(objAccRres);
											}
					
										});
									});
								});

							});
						}],
						function (result, current) {
							if (counter === 0)
							{  return_result(result[0]); }
						});	
					}
					else
					{ return_result(null); }
				});
			}
			else
			{
				return_result(null);
			}
		});
	}
	catch(err)
	{ console.log("__getBranchDetails",err); return_result(null); }	
};

//------------------------------------------------------------------- Branches Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Division Module Start ---------------------------------------------------------------------------//

methods.getDivisionList  =  function (company_id,return_result){
	
	try
	{
		if(parseInt(company_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_company_division WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_company_division WHERE meta_key = 'account_id'  AND meta_value = "+company_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_company_division WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											methods.__getLocationDetails(list.area_id,list.city_id, function(location_details){	

												methods.__getMobileCode(list.landline_code, function(landline_code){

														methods.__getMobileCode(list.mobile_code, function(mobile_code){

															var objAccRres = {
																id                  : list.group_id.toString(),
																name            	: list.division_name.toString(),
																email   			: list.division_email.toString(),
																//website   			: list.division_website.toString(),
																landline_code   	: landline_code.toString(),
																landline_number   	: list.landline_number.toString(),
																mobile_code   		: mobile_code.toString(),
																mobile_number   	: list.mobile_number.toString(),
																postal_code	 		: list.postal_code,
																street_no	 		: list.street_no,
																street_name	 		: list.street_name,
																building_no	 		: list.building_no,
																building_name   	: list.building_name,
																area_name			: location_details.area_name,
																city_name			: location_details.city_name,
																state_name	 		: location_details.state_name,
																country_name   		: location_details.country_name,
																//latitude  			: list.latitude.toString(),
																//longitude  			: list.longitude.toString()
															};

															array_rows.push(objAccRres);

															resolve(array_rows);

															counter -= 1;
														});
											    });
											});
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ 
											return_result(result[0]);
										}
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getDivisionList",err); return_result(null); }	
};

//------------------------------------------------------------------- Division Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Notice Module Start ---------------------------------------------------------------------------//

methods.getNoticeList  =  function (company_id,return_result){
	
	try
	{
		if(parseInt(company_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_company_notice WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_company_notice WHERE meta_key = 'account_id'  AND meta_value = "+company_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_company_notice WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											var objAccRres = {
												title         : list.title.toString(),
												description   : list.description.toString(),
												image         : G_STATIC_IMG_URL+'uploads/jazenet/company/'+list.account_id+'/jazenotice/'+list.image_url.toString(),
												date          : dateFormat(list.created_date.toString(), 'yyyy-mm-dd HH:MM:ss')
											};

											array_rows.push(objAccRres);

											resolve(array_rows);

											counter -= 1;
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ 
											var returnResult = result[0];

											if(returnResult !== null)
											{
												//sorting 
												sortBy(returnResult, {
													prop: "date",
													desc: true,
													parser: function(item) { return new Date(item); }
												});
											}
											
											return_result(returnResult);
										}
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getNoticeList",err); return_result(null); }	
};

//------------------------------------------------------------------- Notice Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Photos Module Start ---------------------------------------------------------------------------//

methods.getPhotoList  =  function (company_id,return_result){
	
	try
	{
		if(parseInt(company_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_company_photo WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_company_photo WHERE meta_key = 'account_id'  AND meta_value = "+company_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_company_photo WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											var objAccRres = {
												title         : list.photo_title.toString(),
												image         : G_STATIC_IMG_URL+'uploads/jazenet/company/'+list.account_id+'/jazephotos/'+list.photo_url.toString(),
												date          : dateFormat(list.create_date.toString(), 'yyyy-mm-dd HH:MM:ss')
											};

											array_rows.push(objAccRres);

											resolve(array_rows);

											counter -= 1;
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ 
											var returnResult = result[0];

											if(returnResult !== null)
											{
												//sorting 
												sortBy(returnResult, {
													prop: "date",
													desc: true,
													parser: function(item) { return new Date(item); }
												});
											}
											
											return_result(returnResult);
										}
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getPhotoList",err); return_result(null); }	
};

//------------------------------------------------------------------- Photos Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Products Module Start ---------------------------------------------------------------------------//






//------------------------------------------------------------------- Products Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Portfolio Module Start ---------------------------------------------------------------------------//

methods.getPortfolioList  =  function (company_id,return_result){
	
	try
	{
		if(parseInt(company_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_portfolios WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_portfolios WHERE meta_key = 'account_id'  AND meta_value = "+company_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_portfolios WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											var arrParamss = {account_id:list.portfolio_account_id};

											HelperGeneralJazenet.getAccountDetails(arrParamss, function(accRres){

												if(accRres !== null)
												{
													var objAccRres = {
														portfolio_name	 : accRres.name,
														portfolio_logo	 : accRres.logo,
													};

													array_rows.push(objAccRres);

													resolve(array_rows);
												}

												counter -= 1;
											});
										});
									}],
									function (result, current) {
										if (counter === 0)
										{  return_result(result[0]); }
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getPhotoList",err); return_result(null); }	
};

//------------------------------------------------------------------- Portfolio Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Reviews Module Start ---------------------------------------------------------------------------//

methods.getReviewList  =  function (company_id,return_result){
	
	try
	{
		if(parseInt(company_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_reviews WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_reviews WHERE meta_key = 'profile_account_id'  AND meta_value = "+company_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_reviews WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											var arrParamss = {account_id:list.account_id};

											HelperGeneralJazenet.getAccountDetails(arrParamss, function(accRres){

												if(accRres !== null)
												{
													var location = '';

													if(accRres.city != '')
													{ location = accRres.city+','+accRres.country; }
													else 
													{ location = accRres.country; }

													var objAccRres = {
														name		  : accRres.name,
														logo		  : accRres.logo,
														location      : location,
														rating        : list.rating.toString(),
														message       : list.comment.toString(),
														date          : dateFormat(list.created_date.toString(), 'yyyy-mm-dd HH:MM:ss')
													};

													array_rows.push(objAccRres);

													resolve(array_rows);
												}

												counter -= 1;

											});
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ 
											var returnResult = result[0];

											if(returnResult !== null)
											{
												//sorting 
												sortBy(returnResult, {
													prop: "date",
													desc: true,
													parser: function(item) { return new Date(item); }
												});
											}
											
											return_result(returnResult);
										}
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getReviewList",err); return_result(null); }	
};

methods.postReview = function(arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{
			//create
			HelperRestriction.getNewGroupId('jaze_reviews', function (group_id) {

				if(parseInt(group_id) !== 0){

					var	arrInsert = {
						account_id	  		: arrParams.account_id,
						profile_account_id	: arrParams.company_id,
						comment 			: arrParams.message,
						rating 	  			: arrParams.rating,
						created_date   		: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
						status 		  		: '1'						
					};

					HelperRestriction.insertTBQuery('jaze_reviews',group_id,arrInsert, function (queGrpId) {

						return_result(queGrpId);
					});
				}
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("postReview",err); return_result(null); }	
};

//------------------------------------------------------------------- Reviews Module End  ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Team Module Start ---------------------------------------------------------------------------//

methods.getTeamList  =  function (company_id,return_result){
	
	try
	{
		if(parseInt(company_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_company_team_details WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_company_team_details WHERE meta_key = 'account_id'  AND meta_value = "+company_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_company_team_details WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											methods.__getTeamDetails(list.team_account_id, function(team_details){

												if(team_details!==null)
												{
													array_rows.push(team_details);
												}

												resolve(array_rows);

												counter -= 1;
											});
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ return_result(fast_sort(result[0]).asc('name')); }
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getTeamList",err); return_result(null); }	
};

methods.getPendingTeamList  =  function (company_id,return_result){
	
	try
	{
		if(parseInt(company_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_company_team_details WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_company_team_details WHERE meta_key = 'account_id'  AND meta_value = "+company_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_company_team_details WHERE meta_key = 'status' AND meta_value = 0 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											methods.__getTeamDetails(list.team_account_id, function(team_details){

												if(team_details!==null)
												{
													array_rows.push(team_details);
												}
								
												resolve(array_rows);

												counter -= 1;
											});
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ return_result(fast_sort(result[0]).asc('name')); }
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getPendingTeamList",err); return_result(null); }	
};


methods.manageDepartmentList  =  function (company_id,return_result){
	
	try
	{
		if(parseInt(company_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_company_team_departments WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_company_team_departments WHERE meta_key = 'account_id'  AND meta_value = "+company_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_company_team_departments WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											var objs = {
												id                 :   list.group_id.toString(),
												department_name    :   list.department_name,
												date               :   list.create_date,
											};

											array_rows.push(objs);

											resolve(array_rows);

											counter -= 1;
										
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ return_result(fast_sort(result[0]).asc('department_name')); }
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("manageDepartmentList",err); return_result(null); }	
};

methods.__getTeamDetails =  function (account_id,return_result){
	
	try
	{
		if(parseInt(account_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_user_basic_details WHERE  account_id = '"+account_id+"'  and account_type = '4'";

				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
						var list = [];

						for (var i in data) 
						{
							Object.assign(list, {[data[i].meta_key]:  data[i].meta_value});
						}

						methods.__getDepartmentName(list.team_department_id, function(department_name){

								var newObj = {
										id              : list.team_group_id.toString(),
										account_id	    : account_id.toString(),
										account_type    : '4',
										name		    : (list.team_fname +' '+list.team_lname).toString(),
										logo		    : G_STATIC_IMG_URL + 'uploads/jazenet/'+list.team_display_pic,
										department 	    : department_name,
										position        : list.team_position,
										chat_status 	: list.team_chat_status.toString(),
										mail_status 	: list.team_mail_status.toString(),
										call_status 	: list.team_call_status.toString(),
										request_status 	: list.team_diary_status.toString(),
										profile_status 	: list.team_add_to_link.toString(),
										link_account_id : list.team_user_link_account_id.toString()
								};


							return_result(newObj);
						});
					}
					else
					{
						return_result(null);
					}
				});
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("__getTeamDetails",err); return_result(null); }	
};

methods.__getDepartmentName =  function (department_id,return_result){
	
	try
	{	
		if(parseInt(department_id) !== 0)
		{
			var quer  = " SELECT meta_value  FROM jaze_company_team_departments WHERE meta_key IN ('department_name') and ";
				quer += " group_id IN (  SELECT group_id FROM jaze_company_team_departments WHERE meta_key = 'status' AND meta_value = 1) ";
				quer += " and group_id =  "+department_id+" ";

				DB.query(quer, null, function(data, error){
						
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0].meta_value);
					}
					else
					{
						return_result('');
					}
				});
		}
		else
		{  return_result(''); }
	}
	catch(err)
	{ console.log("__getDepartmentName",err); return_result(''); }	
};
 
//------------------------------------------------------------------- Team Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Company Vacancies Start ---------------------------------------------------------------------------//
methods.getCompanyVacancies  =  function (arrParams,return_result){
	
	try
	{

		var arrAccounts = [];
		var arrObjects = {};

		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };


		var querSubCat = `SELECT * FROM jaze_job_post_job WHERE group_id IN (SELECT group_id FROM jaze_job_post_job WHERE meta_key = 'account_id' AND meta_value =?)
		 				AND group_id IN (SELECT group_id FROM jaze_job_post_job WHERE meta_key = 'status' AND meta_value ='1')  `;


		DB.query(querSubCat, [arrParams.company_id], function(data, error){
	
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				HelperRestriction.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParams = {account_id:jsonItems.account_id};
							methods.getCompanyProfile(accParams, function(retProfile){	

								if(retProfile!=null)
								{

									methods.__getCountryDetails(jsonItems.country,function(retCountry){

										methods.__getStateDetails(jsonItems.state,function(retState){

											methods.__getCityDetails(jsonItems.city,function(retCity){

												methods.getEmployerAttributes(jsonItems.group_id,function(retAttri){

													var applyParams = {account_id:arrParams.account_id, group_id:jsonItems.group_id};
													methods.getApplyStatus(applyParams,function(retAppStatus){

														var start_date = "";
														var end_date = "";
														var start_time = "";
														var end_time = "";
													
														var int_type = "0";

														if(jsonItems.start_date && jsonItems.start_time && jsonItems.end_date && jsonItems.end_time){
															
															var job_start = dateFormat(jsonItems.start_date+' '+jsonItems.start_time,'yyyy-mm-dd HH:MM:ss');
															var job_end = dateFormat(jsonItems.end_date+' '+jsonItems.end_time,'yyyy-mm-dd HH:MM:ss');
												

															if( (new Date(standardDT).getTime() >= new Date(job_start).getTime()) && (new Date(standardDT).getTime() <= new Date(job_end).getTime())  ){

																int_type = "1";

																start_date = jsonItems.start_date;
																end_date   = jsonItems.end_date;
																start_time = jsonItems.start_time;
																end_time   = jsonItems.end_time;

															}
														}
		
														arrObjects = {
															group_id     : jsonItems.group_id.toString(),
															job_category : jsonItems.job_category.toString(),
															account_id   : jsonItems.account_id.toString(),
															name         : retProfile.name,
															logo         : retProfile.logo,
															country_name : retCountry,
															state_name   : retState,
															city_name    : retCity,
															job_title    : jsonItems.job_title,
															job_description : jsonItems.job_description,
															currency     : retProfile.currency,
															salary       : retAttri.monthly_salary,
															interview_type : int_type,
															start_date     : start_date,
															end_date       : end_date,
															start_time     : start_time,
															end_time       : end_time,
															create_date  : dateFormat(jsonItems.create_date,'yyyy-mm-dd HH:MM:ss'),	
															apply_status : retAppStatus
											
														}
													
														counter -= 1;
														arrAccounts.push(arrObjects);

														if (counter === 0){ 
															resolve(arrAccounts);
														}
				
													
													});
												});
											});
										});
									});
								}
							
							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							return_result(result[0]);	

						}
					});

				});
			}
			else
			{
				return_result(null);
			}


		});

	}
	catch(err)
	{ 
		console.log("getCompanyVacancies",err); 
		return_result(null); 
	}	

};



methods.getEmployerAttributes = function(arrGroupID,return_result){
	try
	{
		var query = "SELECT * FROM  jaze_job_post_job_attributes WHERE group_id=?";
	
		var obj_values = {
			employment_type  : '',
			monthly_salary   : '',
			career_level     : '',
			min_exp          : '',
			min_edu          : '',
		};
	
		DB.query(query,[arrGroupID], function(detail, error){

			if(typeof detail !== 'undefined' && detail !== null && parseInt(detail.length) > 0)
			{	
			
					
				detail.forEach(function(value) {
					if(value.meta_key == '10'){
						obj_values.monthly_salary = value.meta_value;
					}
					if(value.meta_key == '11'){
						obj_values.career_level = value.meta_value;
					}
					if(value.meta_key == '12'){
						obj_values.min_exp = value.meta_value;
					}
					if(value.meta_key == '13'){
						obj_values.min_edu = value.meta_value;
					}
					if(value.meta_key == '14'){
						obj_values.employment_type = value.meta_value;
					}
				
				});


				return_result(obj_values);
			}
		});
	
	}
	catch(err)
	{ console.log("getEmployerAttributes",err); return_result(obj_values);}

};


//------------------------------------------------------------------- Company Vacancies End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Company Favourite Module Start ---------------------------------------------------------------------------//

methods.saveCompanyFavourite  =  function (arrParams,return_result){
	
	try
	{
		if(arrParams !== null)
		{
			var query  = " SELECT group_id FROM jaze_user_favorite_list WHERE  ";
				query += "     group_id IN (SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'account_id'  AND meta_value = "+arrParams.account_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'fav_account_id'  AND meta_value = "+arrParams.company_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'status' AND meta_value = 1 ))); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						//delete

						var query  = " DELETE FROM `jaze_user_favorite_list` WHERE `group_id` = '"+data[0].group_id+"'; ";

						DB.query(query,null, function(data_check, error){

							return_result(2);

						});
					}
					else
					{
						//insert
						HelperRestriction.getNewGroupId('jaze_user_favorite_list', function (group_id) {

							if(parseInt(group_id) !== 0){
			
								var	arrInsert = {
									account_id	  		: arrParams.account_id,
									fav_account_id		: arrParams.company_id,
									fav_account_type    : '3',
									fav_type    		: '1',
									create_date   		: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
									status 		  		: '1'						
								};
			
								HelperRestriction.insertTBQuery('jaze_user_favorite_list',group_id,arrInsert, function (queGrpId) {
									return_result(1);
								});
							}
						});
					}
			    });
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{ console.log("setFollowCompany",err); return_result(0); }	
};

//------------------------------------------------------------------- Company Favourite Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Company Keyword Search Module Start ---------------------------------------------------------------------------//

methods.getSearchKeywordCompanyList = function(arrParams,return_result){

	var return_obj = {
		result      : null,
		tot_count   : "0",	
		tot_page    : "0"				                        
	};

	try{

		methods.__getCountCompany(arrParams,function(comp_t_count){

			if(comp_t_count !== null)
			{
				var query  = " SELECT group_concat(meta_value) as id FROM jaze_company_keywords WHERE  meta_key IN ('account_id') ";
					query += " and  group_id IN  (SELECT group_id FROM jaze_company_keywords WHERE meta_key = 'status'  AND meta_value = '1') ";
					query += " and  group_id IN  (SELECT group_id FROM jaze_company_keywords WHERE meta_key = 'account_id'  AND meta_value in ("+comp_t_count+")) ";
					query += " and  group_id IN  (SELECT group_id FROM jaze_company_keywords WHERE meta_key IN ('keywords')  AND LOWER(meta_value) like '%"+arrParams.keyword.toLowerCase()+"%') ";
					query += " group by meta_value  ORDER BY (meta_value AND meta_key = 'keywords') asc  ";

				DB.query(query,null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						methods.__getCompanyDistance(arrParams,data, function(compRres){

							var array_rows = [];

							var t_counter = data.length;
							
							if(parseInt(counter) !== 0)
							{
								var counter = compRres.length;
								
									promiseForeach.each(compRres,[function (list) {
										
										return new Promise(function (resolve, reject) {

											var arrParamss = {account_id:list.company_id};
											
											HelperGeneralJazenet.getAccountDetails(arrParamss, function(accRres){

												var newObj = {
													company_id		: accRres.account_id,
													company_type	: accRres.account_type,
												  	company_name	: accRres.name,
												  	company_image	: accRres.logo,
													distance		: list.distance.toString()
												};

												array_rows.push(newObj);
																								
												counter -= 1;
																								
												resolve(array_rows);

											});
										});
									}],
									function (result, current) {
										
										if (counter === 0)
										{  
											var obj = {
												result : result[0],
												tot_count : t_counter.toString(),
												tot_page  : Math.ceil(parseInt(t_counter) / 30)
											}

											return_result(obj); 
										}
									});	
							}
							else
							{return_result(return_obj)}
						});
					}
					else
					{ return_result(return_obj); }
				});
			}
			else
			{ return_result(return_obj); }
		});
	}
	catch(err)
	{
		return_result(return_obj)
	}
};


methods.getCurrency =  function(currency_id,return_result){
	try
	{
		DB.query("SELECT code FROM currency_master WHERE currency_id=?",[currency_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
				if(data[0].code){
					return_result(data[0].code);
				}else{
					return_result('');
				}
			
			}else{
				return_result('');
			}

		});

	}
	catch(err)
	{ console.log("getCurrency",err); return_result(null);}
};

methods.__getCountCompany = function(arrParams,return_result){

	try{
		
				var query  = " SELECT account_id FROM jaze_user_basic_details WHERE account_id not in ("+arrParams.account_id+") and ( account_type = 3 or account_type = 5)  ";
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1' ";
					query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '0') ";

					if(parseInt(arrParams.country_id) !== 0)
					{
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country_id+"') ";
					}

					if(parseInt(arrParams.state_id) !== 0)
					{
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state_id+"') ";
					}
						
					if(parseInt(arrParams.city_id) !== 0)
					{
						query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city_id+"') ";
					}	

					query += " ) ";

					query += " group by account_id  ";
					
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
						
						var account_id_id_list = "";

						data.forEach(function(row){
							account_id_id_list += (account_id_id_list  == "") ? row.account_id: ','+row.account_id;	
						});

						

						return_result(account_id_id_list);
					}
					else
					{return_result(null)}
				});
	}
	catch(err)
	{ return_result(null); }
};

methods.__getCompanyDistance = function(arrParams,companyid_array,return_result){

	try{

		if(companyid_array !== null)
		{
			var account_id_id_list = [];

			var start = '0';

			var count = '30';

			if(parseInt(arrParams.page) !== 0)
			{  start = parseInt(arrParams.page) * count; }


			if(parseInt(arrParams.lat) !== 0 && parseInt(arrParams.lon) !== 0 )
			{
				var id_list = "";

				companyid_array.forEach(function(row){
					id_list += (id_list  == "") ? row.id: ','+row.id;	
				});

				var query = " SELECT account_id, (ROUND(111.045* haversine_distance(GROUP_CONCAT(meta_value ORDER BY meta_value ASC SEPARATOR ':') , '"+arrParams.lat+":"+arrParams.lon+"'), 2 )) AS distance_in_km  FROM jaze_user_basic_details  WHERE account_id in ("+id_list+")  ";
					query += " AND meta_key IN ('latitude','longitude')  GROUP BY account_id ORDER BY distance_in_km ASC limit "+start+", "+count+" ";
					
					DB.query(query,null, function(data, error){
					
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							data.forEach(function(row){

								var obj = {
									company_id : row.account_id,
									distance   : row.distance_in_km,
								}
			
								account_id_id_list.push(obj)
									
							});
			
							return_result(account_id_id_list);
						}
					});
			}
			else
			{										
				var arrvalues = companyid_array.splice(start, count); 

				arrvalues.forEach(function(row){

					var obj = {
						company_id : row.id,
						distance   : "",
					}

					account_id_id_list.push(obj)
						
				});

				return_result(account_id_id_list);
			}
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ return_result(null); }
};

//------------------------------------------------------------------- Company Keyword Search Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Location Module End ---------------------------------------------------------------------------//


//--------------------------------------------------------------------- Company Essentials Start -------------------------------------------------------------------------//


methods.getUserIDEssentials = function (account_id,return_result){
	try
	{

		let query  = `SELECT * FROM jaze_user_jazenet_id WHERE account_id=?`;

		DB.query(query,[account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				return_result(data[0].jazenet_id+data[0].unique_id);
			}
			else
			{return_result('')}
		});


	}
	catch(err)
	{ console.log("getUserIDEssentials",err); return_result(''); }	
};

methods.getEssentialsDetails= function (arrParams,return_result){
	try
	{	
		var query = `SELECT *FROM jaze_user_basic_details WHERE account_id =?`;

		DB.query(query,[arrParams.company_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				var accParams = {account_id: data[0].account_id};

				methods.convertMultiMetaArrayCompany(data, function(retConverted){

					methods.__getCountryDetails(retConverted[0].country_id, function(retCountry){

						methods.__getStateDetails(retConverted[0].state_id, function(retState){

							methods.__getCityDetails(retConverted[0].city_id, function(retCity){

								methods.__getAreaDetails(retConverted[0].area_id, function(retArea){

									methods.getCompanyKeywords(retConverted[0].account_id, function(retKeywords){

										methods.getCompanyJazeCategory(retConverted[0].category_id, function(retCategory){

											methods.getUserIDEssentials(arrParams.company_id, function(retJazenetId){

												methods.getCurrency(retConverted[0].currency_id, function(retCurrency){

													methods.__getMobileCode(retConverted[0].company_phone_code, function(mobCode){

														methods.__getMobileCode(retConverted[0].company_landline_code, function(lanCode){

															var company_logo = "";
															var cover_image = "";
															if(retConverted[0].company_logo !=""){
																company_logo = STATIC_IMG_URL+arrParams.company_id+'/logo/'+retConverted[0].company_logo;
															}

															if(retConverted[0].cover_image !=""){
																cover_image = STATIC_IMG_URL+arrParams.company_id+'/cover_image/'+retConverted[0].cover_image;
															}


															var obj_result = {

																jazenet_id          :   retJazenetId,
																name                :   retConverted[0].company_name,
																category_id         :   retConverted[0].category_id,
																category_name       :   retCategory,
																trading_name        :   retConverted[0].trading_name,
																email               :   retConverted[0].company_email,
																company_logo        :   company_logo,
																cover_image         :   cover_image,
																company_about       :   retConverted[0].company_about,
																website             :   retConverted[0].company_website,
																mobile_phone_code   :   mobCode,
																mobile_country      :   retConverted[0].company_phone_code,
																mobile              :   retConverted[0].company_phone,
																landline_phone_code :   lanCode,
																landline_country    :   retConverted[0].company_landline_code,
																landline            :   retConverted[0].company_landline,
																country_id          :   retConverted[0].country_id,
																country_name        :   retCountry,
																state_id            :   retConverted[0].state_id,
																state_name          :   retState,
																city_id             :   retConverted[0].city_id,
																city_name           :   retCity,
																area_id             :   retConverted[0].area_id,
																area_name           :   retArea,
																street_name         :   retConverted[0].street_name,
																street_no           :   retConverted[0].street_no,
																building_name       :   retConverted[0].building_name,
																building_no         :   retConverted[0].building_no,
																postal_code         :   retConverted[0].postal_code,
																currency_id         :   retConverted[0].currency_id,
																currency            :   retCurrency,
																latitude            :   retConverted[0].latitude,
																longitude           :   retConverted[0].longitude,
																branch_type         :   retConverted[0].branch_type,
																is_merchant_type    :   retConverted[0].is_merchant_type,
																is_company_type     :   retConverted[0].is_company_type,
																keywords            :   retKeywords

															}

															return_result(obj_result);

														});
													});
												});
											});
										});
									});
								});
							});
						});
					});
				});


	 	   	}else{
				return_result(null);
	 	   	}

 	 	});


	}
	catch(err)
	{ console.log("getEssentialsDetails",err); return_result(null); }	
};


methods.updateEssentials =  function(arrParams,return_result){
	try
	{

		var query = `SELECT * FROM jaze_user_basic_details WHERE account_id=?`;
		DB.query(query,[arrParams.company_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArrayCompany(data, function(retEssDetails){	

					if(retEssDetails!=null){

						var	arrUpdate = {

							company_name          :   arrParams.name,
							category_id           :   arrParams.category_id,
							company_email         :   arrParams.email,
							company_website       :   arrParams.company_website,
							company_phone_code    :   arrParams.mobile_phone_code,
							company_phone         :   arrParams.mobile_number,
							company_landline_code :   arrParams.landline_phone_code,
							company_landline    :   arrParams.landline_number,
							country_id          :   arrParams.country_id,
							state_id            :   arrParams.state_id,
							city_id             :   arrParams.city_id,
							area_id             :   arrParams.area_id,
							street_name         :   arrParams.street_name,
							street_no           :   arrParams.street_no,
							building_name       :   arrParams.bldg_name,
							building_no         :   arrParams.bldg_no,
							postal_code         :   arrParams.postal_code,
							currency_id         :   arrParams.currency_id,
							latitude            :   arrParams.latitude,
							longitude           :   arrParams.longitude,
							is_merchant_type    :   arrParams.is_merchant_type
				
						
						};

						//return_result(arrUpdate);

						var updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key =? AND account_id =?`;
						
						for (var key in arrUpdate) 
					 	{	
						     if(arrUpdate[key] !== null && arrUpdate[key] != ""){

								DB.query(updateQue, [arrUpdate[key],key,retEssDetails[0].account_id], function(datau, error){
							 		if(datau !== null){
						 				return_result(1);
							 	   	}else{
						 	   			return_result(0);
							 	   	}
								});
							}
						}


						var queryKeywords = `SELECT * FROM jaze_company_keywords WHERE 
								group_id IN (SELECT group_id FROM jaze_company_keywords WHERE meta_key = 'account_id' AND meta_value =?)`;

						DB.query(queryKeywords,[arrParams.company_id], function(dataKey, error){

							if(typeof dataKey !== 'undefined' && dataKey !== null && parseInt(dataKey.length) > 0){
								

								var	arrUpdateKeywords = {
									keywords     : arrParams.keywords,
									created_date : standardDT,
								};

								var updateQueKey = `update jaze_company_keywords SET meta_value =? WHERE meta_key =? AND group_id =?`;
								
								for (var key in arrUpdateKeywords) 
							 	{	
								     if(arrUpdateKeywords[key] !== null && arrUpdateKeywords[key] != ""){
										DB.query(updateQueKey, [arrUpdateKeywords[key],key,dataKey[0].group_id], function(data, error){});
									}
								}

						  	}else{

			  					if(arrParams.keywords !=""){

									var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_company_keywords";
						  			var lastGrpId = 0;

									methods.getLastGroupId(queGroupMax, function(queGrpRes){

										 
										lastGrpId = queGrpRes[0].lastGrpId;


										var arrInsert = {
											account_id   : arrParams.company_id,
											keywords     : arrParams.keywords,
											created_date : standardDT,
											status       : 1
										};
									
										var plusOneGroupID = lastGrpId+1;
								  		var querInsert = `insert into jaze_company_keywords (group_id, meta_key, meta_value) VALUES (?,?,?)`;

								  		var returnFunction;
									 	for (var key in arrInsert) 
									 	{
											DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){});
									  	};
										  
									});
								}

						  	}	
						  	
				 	 	});

					}
					else
					{
						return_result(0);
					}

				});

			}

		});

	}
	catch(err)
	{ console.log("updateProfessionalEssDetails",err); return_result(0);}
};

methods.getCompanyKeywords = function (account_id,return_result){
	try
	{

		let query  = `SELECT * FROM jaze_company_keywords 
		WHERE group_id IN (SELECT group_id FROM jaze_company_keywords WHERE meta_key = 'account_id' AND meta_value =?)`;

		DB.query(query,[account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){
					return_result(retConverted[0].keywords);
				});
			}
			else
			{return_result('')}
		});


	}
	catch(err)
	{ console.log("getCompanyKeywords",err); return_result(''); }	
};

methods.getCompanyJazeCategory = function (group_id,return_result){
	try
	{

		let query  = `SELECT * FROM jaze_category_list WHERE group_id=?`;

		DB.query(query,[group_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){
					return_result(retConverted[0].cate_name);
				});
			}
			else
			{return_result('')}
		});


	}
	catch(err)
	{ console.log("getCompanyJazeCategory",err); return_result(''); }	
};


//--------------------------------------------------------------------- Company Essentials -------------------------------------------------------------------------//



//--------------------------------------------------------------------- Company Essentials End -------------------------------------------------------------------------//

//--------------------------------------------------------------------- Company Photo Start -------------------------------------------------------------------------//


methods.saveUpdateCompanyPhotoVideo = function (arrParams,return_result){
	try
	{

		let query  = `SELECT * FROM jaze_user_basic_details WHERE account_id=?`;

		DB.query(query,[arrParams.company_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArrayCompany(data, function(retConverted){

					var accParams = {account_id:arrParams.company_id};

					methods.getCompanyProfile(accParams, function(retAcc){

						if(parseInt(arrParams.flag) == 1)
						{	
							//company_logo
							if(retConverted[0].company_logo)
							{
								var updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key ='company_logo' AND account_id =?`;
							
								DB.query(updateQue, [arrParams.item,retAcc.account_id], function(datau, error){
							 		if(datau !== null){
						 				return_result(1);
							 	   	}else{
						 	   			return_result(0);
							 	   	}
								});

							}
							else
							{
						  		var querInsert = `insert into jaze_user_basic_details (account_id, account_type, meta_key, meta_value) VALUES (?,?,?,?)`;
						  		DB.query(querInsert,[retAcc.account_id,retAcc.account_type,'company_logo',arrParams.item], function(data, error){
					  				if(data !== null){
						 				return_result(1);
							 	   	}else{
						 	   			return_result(0);
							 	   	}
						  		});
				
							}
						}
						else if(parseInt(arrParams.flag) == 2)
						{	
							//listing_images
							if(retConverted[0].listing_images)
							{
								var updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key ='listing_images' AND account_id =?`;
								DB.query(updateQue, [arrParams.item,retAcc.account_id], function(datau, error){
							 		if(datau !== null){
						 				return_result(1);
							 	   	}else{
						 	   			return_result(0);
							 	   	}
								});

							}
							else
							{
					  			var querInsert = `insert into jaze_user_basic_details (account_id, account_type, meta_key, meta_value) VALUES (?,?,?,?)`;
						  		DB.query(querInsert,[retAcc.account_id,retAcc.account_type,'listing_images',arrParams.item], function(data, error){
					  				if(data !== null){
						 				return_result(1);
							 	   	}else{
						 	   			return_result(0);
							 	   	}
						  		});
							}
						}
						else if(parseInt(arrParams.flag) == 3)
						{	
							//cover_image
							if(retConverted[0].cover_image)
							{

								var updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key ='cover_image' AND account_id =?`;
								DB.query(updateQue, [arrParams.item,retAcc.account_id], function(datau, error){
							 		if(datau !== null){
						 				return_result(1);
							 	   	}else{
						 	   			return_result(0);
							 	   	}
								});
							}
							else
							{
						  		var querInsert = `insert into jaze_user_basic_details (account_id, account_type, meta_key, meta_value) VALUES (?,?,?,?)`;
						  		DB.query(querInsert,[retAcc.account_id,retAcc.account_type,'cover_image',arrParams.item], function(data, error){
					  				if(data !== null){
						 				return_result(1);
							 	   	}else{
						 	   			return_result(0);
							 	   	}
						  		});
							}
						}
						else if(parseInt(arrParams.flag) == 4)
						{	
							//you tube
							// if ( typeof retConverted[0].youtube_url !== 'undefined' && retConverted[0].youtube_url )
							// {

								var updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key ='youtube_url' AND account_id =?`;
								DB.query(updateQue, [arrParams.item,retAcc.account_id], function(datau, error){
							 		if(datau !== null){
						 				return_result(1);
							 	   	}else{
						 	   			return_result(0);
							 	   	}
								});
					
							// }
							// else
							// {
						 //  		var querInsert = `insert into jaze_user_basic_details (account_id, account_type, meta_key, meta_value) VALUES (?,?,?,?)`;
						 //  		DB.query(querInsert,[retAcc.account_id,retAcc.account_type,'youtube_url',arrParams.item], function(data, error){
					  // 				if(data !== null){
					  // 					console.log(data);
						 // 				return_result(1);
							//  	   	}else{
						 // 	   			return_result(0);
							//  	   	}
						 //  		});
							// }
						}

					});
				});
			}
			else
			{return_result(2)}
		});


	}
	catch(err)
	{ console.log("getCompanyJazeCategory",err); return_result(0); }	
};


methods.getCompanyProfilePhotosDetails = function (arrParams,return_result){
	try
	{


		var obj_return = {
			company_log    : '',
			listing_images : '',
			cover_image    : '',
			video          : ''
		}

		var query = `SELECT * FROM jaze_user_basic_details WHERE account_id =?`;
		DB.query(query,[arrParams.company_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArrayCompany(data,function(retConverted){

	
					if(retConverted[0].company_logo){
						obj_return.company_log = STATIC_IMG_URL+arrParams.company_id+'/logo/'+retConverted[0].company_logo;
					}
					if(retConverted[0].listing_images){
						obj_return.listing_images = STATIC_IMG_URL+arrParams.company_id+'/listing_images/'+retConverted[0].listing_images;
					}
					if(retConverted[0].cover_image){
						obj_return.cover_image = STATIC_IMG_URL+arrParams.company_id+'/cover_image/'+retConverted[0].cover_image;
					}
					if(retConverted[0].youtube_url){
						obj_return.video = 'https://www.youtube.com/watch?v='+retConverted[0].youtube_url;
					}


					return_result(obj_return);

				});
				
			
			}
			else
			{
				return_result(obj_return);
			}

		});
	}
	catch(err)
	{ console.log("getCompanyProfilePhotosDetails",err); return_result(obj_return); }	
};


//------------------------------------------------------------------- Company Photo End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Company communication stat  ---------------------------------------------------------------------------//

methods.updateCompanyCommunication = function (arrParams,return_result){
	try
	{

		var query = `SELECT * FROM jaze_user_jazecome_status WHERE 
		group_id IN (SELECT group_id FROM jaze_user_jazecome_status WHERE meta_key = 'account_id' AND meta_value =? )`;
		DB.query(query,[arrParams.company_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var	arrUpdate = {
					jaze_diary : arrParams.jaze_diary,
					jaze_feed  : arrParams.jaze_feed,
					jaze_call  : arrParams.jaze_call,
					jaze_chat  : arrParams.jaze_chat,
					jaze_mail  : arrParams.jaze_mail,
				};

				var updateQue = `update jaze_user_jazecome_status SET meta_value =? WHERE meta_key =? AND group_id =? `;
				
				for (var key in arrUpdate) 
			 	{	
				     if(arrUpdate[key] !== null && arrUpdate[key] != ""){
						DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(datau, error){
					 		if(datau !== null){
				 				return_result(1);
					 	   	}else{
				 	   			return_result(0);
					 	   	}
						});
					}
				}	

			}
			else
			{



				var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_user_jazecome_status";
	  			var lastGrpId = 0;

				methods.getLastGroupId(queGroupMax, function(queGrpRes){

				  	queGrpRes.forEach(function(row) {
						lastGrpId = row.lastGrpId;
				  	});

					var arrInsert = {
						account_id : arrParams.company_id,
						jaze_diary : arrParams.jaze_diary,
						jaze_feed  : arrParams.jaze_feed,
						jaze_call  : arrParams.jaze_call,
						jaze_chat  : arrParams.jaze_chat,
						jaze_mail  : arrParams.jaze_mail,
					};
				
					var plusOneGroupID = lastGrpId+1;
			  		var querInsert = `insert into jaze_user_jazecome_status (group_id, meta_key, meta_value) VALUES (?,?,?)`;

			  		var returnFunction;
				 	for (var key in arrInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
				 		 	if(parseInt(data.affectedRows) > 0){
		 						return_result(1);
					 	   	}else{
			   					return_result(0);
					 	   	}
				 	 	});
				  	};

				});

			}

		});

	
	}
	catch(err)
	{ console.log("updateCompanyCommunication",err); return_result(obj_return); }	
};


methods.getCompanyCommunication = function (arrParams,return_result){
	try
	{


		var obj_return = {
			jaze_diary : '0',
			jaze_feed  : '0',
			jaze_call  : '0',
			jaze_chat  : '0',
			jaze_mail  : '0'
		}


		var query = `SELECT * FROM jaze_user_jazecome_status WHERE 
		group_id IN (SELECT group_id FROM jaze_user_jazecome_status WHERE meta_key = 'account_id' AND meta_value =? )`;

		DB.query(query,[arrParams.company_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data,function(retConverted){

	
					if(retConverted[0].jaze_diary){
						obj_return.jaze_diary = retConverted[0].jaze_diary.toString();
					}
					if(retConverted[0].jaze_feed){
						obj_return.jaze_feed = retConverted[0].jaze_feed.toString();
					}
					if(retConverted[0].jaze_call){
						obj_return.jaze_call = retConverted[0].jaze_call.toString();
					}
					if(retConverted[0].jaze_chat){
						obj_return.jaze_chat = retConverted[0].jaze_chat.toString();
					}
					if(retConverted[0].jaze_mail){
						obj_return.jaze_mail = retConverted[0].jaze_mail.toString();
					}


					return_result(obj_return);

				});
				
			
			}
			else
			{

				return_result(obj_return);

				
			}

		});
	
	}
	catch(err)
	{ console.log("getCompanyCommunication",err); return_result(obj_return); }	
};

//------------------------------------------------------------------- Company communication end  ---------------------------------------------------------------------------//

//------------------------------------------------------------------- About Update Module Start ---------------------------------------------------------------------------//

methods.getCompanyAbout = function (arrParams,return_result){
	try
	{

		var query = `SELECT * FROM jaze_user_basic_details WHERE account_id =? AND meta_key = 'company_about'`;
		DB.query(query,[arrParams.company_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				return_result(data[0].meta_value);

			}
			else
			{
				return_result(null);
			}

		});
	}
	catch(err)
	{ console.log("getCompanyAbout",err); return_result(null); }	
};

methods.updateAbout = function (arrParams,return_result){
	try
	{

		
	    if(parseInt(arrParams.flag) === 2)
		{
			var query = `SELECT * FROM jaze_user_basic_details WHERE account_id =? AND meta_key = 'company_about'`;
			DB.query(query,[arrParams.company_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					var updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key ='company_about' AND account_id =?`;
					DB.query(updateQue, [arrParams.about,data[0].account_id], function(datau, error){
				 		if(datau !== null){
			 				return_result(1);
				 	   	}else{
			 	   			return_result(0);
				 	   	}
					});

				}
				else
				{
					return_result(0);
				}

			});
		}
		else
		{
			return_result(0);
		}
		// else if(parseInt(arrParams.flag) === 3)
		// {

		// 	var query = `SELECT * FROM jaze_user_basic_details WHERE account_id =? AND meta_key = 'company_about'`;
		// 	DB.query(query,[arrParams.account_id], function(data, error){

		// 		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		// 		{
					
		// 			var updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key ='company_about' AND account_id =?`;
		// 			DB.query(updateQue, ["",data[0].account_id], function(datau, error){
		// 		 		if(datau !== null){
		// 	 				return_result(1);
		// 		 	   	}else{
		// 	 	   			return_result(0);
		// 		 	   	}
		// 			});

		// 		}
		// 		else
		// 		{
		// 			return_result(0);
		// 		}
				
		// 	});
		// }
	}
	catch(err)
	{ console.log("updateAbout",err); return_result(0); }	
};

//------------------------------------------------------------------- About Update Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Photo Manager Start  ---------------------------------------------------------------------------//

methods.getCompanyPhotos = function (arrParams,return_result){
	try
	{

		let query  = `SELECT * FROM jaze_company_photo WHERE group_id 
			IN (SELECT group_id FROM jaze_company_photo WHERE meta_key = 'account_id' AND meta_value =?)`;

		var arrObjects = {};
		var arrResults = [];

		DB.query(query,[arrParams.company_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
				
							arrObjects = {
								id    :jsonItems.group_id.toString(),
								title :jsonItems.photo_title,
								path  :G_STATIC_IMG_URL+"uploads/jazenet/company/"+arrParams.company_id+"/jazephotos/"+jsonItems.photo_url
							};

							counter -= 1;
							arrResults.push(arrObjects);
						
							if (counter === 0){ 
								resolve(arrResults);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});

				});
			}
			else
			{return_result(null)}
		});


	}
	catch(err)
	{ console.log("getCompanyPhotos",err); return_result(null); }	
};

methods.addCompanyPhotos = function(arrParams,return_result){

	try
	{
		if(arrParams.file_name !== '')
		{	
						
			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_company_photo";
  			var lastGrpId = 0;

			methods.getLastGroupId(queGroupMax, function(queGrpRes){

			  	queGrpRes.forEach(function(row) {
					lastGrpId = row.lastGrpId;
			  	});

			 

				var arrInsert = {
					account_id  : arrParams.company_id,
					photo_url   : arrParams.file_name,
					photo_title : arrParams.title,
					create_date : standardDT,
					status:1
				};
			
				var plusOneGroupID = lastGrpId+1;
		  		var querInsert = `insert into jaze_company_photo (group_id, meta_key, meta_value) VALUES (?,?,?)`;

		  		var returnFunction;
			 	for (var key in arrInsert) 
			 	{
					DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
			 		 	if(parseInt(data.affectedRows) > 0){
	 						return_result(data.affectedRows);
				 	   	}else{
		   					return_result(0);
				 	   	}
			 	 	});
			  	};
			  	
			});
		}
		else
		{ 
			return_result(0); 
		}
	}
	catch(err)
	{ 
		console.log("addCompanyPhotos",err); return_result(0); 
	}	
};


methods.updateCompanyPhoto = function(arrParams,return_result){

	try
	{
		
		if(arrParams.id !='' )
		{
			var	arrUpdate = {
				photo_url       : arrParams.file_name,
				photo_title     : arrParams.title,
				create_date     : standardDT
			};

			var updateQue = `update jaze_company_photo SET meta_value =? WHERE meta_key =? AND group_id =? `;
			
			for (var key in arrUpdate) 
		 	{	
			     if(arrUpdate[key] !== null && arrUpdate[key] != ""){
					DB.query(updateQue, [arrUpdate[key],key,arrParams.id], function(datau, error){
				 		if(datau !== null){
			 				return_result(1);
				 	   	}else{
			 	   			return_result(0);
				 	   	}
					});
				}
			}	
		}
		else
		{
			return_result(0);
		}

	}
	catch(err)
	{ 
		console.log("updateCompanyPhoto",err); return_result(0); 
	}	
};


methods.delCompanyPhoto = function(arrParams,return_result){

	try
	{
		
		if(arrParams.id !='' )
		{	

			var qry = `SELECT * FROM jaze_company_photo WHERE group_id =?`;

			DB.query(qry, [arrParams.id], function(data, error){

	 	  		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
	 	  		{
					

					var delQue = `DELETE FROM jaze_company_photo WHERE group_id=? `;
					
					DB.query(delQue, [data[0].group_id], function(datau, error){
				 		if(datau !== null){
			 				return_result(1);
				 	   	}else{
			 	   			return_result(0);
				 	   	}
					});
					
		 	   	}
		 	   	else
		 	   	{
					return_result(2);
		 	   	}

			});
	
		}
		else
		{
			return_result(0);
		}

	}
	catch(err)
	{ 
		console.log("delCompanyPhoto",err); return_result(0); 
	}	
};

//------------------------------------------------------------------- Photo Manger End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- billing & shipping address Module Start ---------------------------------------------------------------------------//

methods.getShippingDetails = function(arrParams,return_result)
{ 
	try
	{
		var query = " SELECT *  FROM jaze_user_billing_address WHERE group_id in (SELECT group_id  FROM jaze_user_billing_address WHERE  meta_key = 'account_id' and meta_value = '"+arrParams.company_id+"' and group_id in ";
		    query += " (SELECT group_id  FROM jaze_user_billing_address WHERE  meta_key = 'status' and meta_value = '1')) ";

		DB.query(query,null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				HelperRestriction.convertMultiMetaArray(data, function(data_result){	
							
					if(data_result !== null)
					{
						var counter = data_result.length;

						var array_rows = [];

						if(parseInt(counter) !== 0)
						{
							promiseForeach.each(data_result,[function (list) {
				
								return new Promise(function (resolve, reject) {

									var country_id = '';
									var state_id = '';
									var city_id = '';
									var area_id = '';

									methods.__getCountryDetails(list.country, function(country){	

										methods.__getStateDetails(list.state, function(state){	
			
											methods.__getCityDetails(list.city, function(city){	
			
												methods.__getAreaDetails(list.area, function(area){	
													
													methods.__getMobileCode(list.mobile_country_code, function(mobcode){	
			
														methods.__getMobileCode(list.landline_country_code, function(phocode){	
			
															if(list.landline_no!=""){
																landline = (phocode+" "+list.landline_no).toString();
															}	
															
															var location_type = "";

															if(parseInt(list.location_type) == 1){
																location_type = 'home';
															}else if(parseInt(list.location_type) == 2){
																location_type = 'office';
															}

															if(list.country){

																country_id = list.country;
															}

															if(list.state){
																state_id = list.state;
															}

															if(list.city){
																city_id = list.city;
															}

															if(list.area){
																area_id = list.area;
															}

															  var newObj = {
																  id:list.group_id.toString(),
																  first_name	 : list.first_name.toString(),
																  last_name		 : list.last_name.toString(),
																  country_id	 : country_id,
																  country_name   : country,
																  state_id		 : state_id,
																  state_name	 : state,
																  city_id		 : city_id,
																  city_name		 : city,
																  area_id		 : area_id,
																  area_name		 : area,
																  street_name	 :list.street_name,
																  street_no	 	 :list.street_no,
																  building_name  :list.building_name,
																  building_no	 :list.building_no,
																  landmark       :list.landmark,
																  location_type  :list.location_type,
																  mobile_code_id : list.mobile_country_code.toString(),
																  mobile_code    : mobcode.toString(),
																  mobile_no 	 : list.mobile_no.toString(),
																  landline_code_id : list.landline_country_code.toString(),
																  landline_code    : phocode,
																  landline_no 	   : list.landline_no.toString(),
																  shipping_note	   :list.shipping_note,
																  default		   :list.is_default 
																  
															  };
			
															  array_rows.push(newObj);
					
															  resolve(array_rows);

															counter -= 1;
			
														});
													});
												});
											});
										});
									});
								})
							}],
							function (result, current) {
												
								if (counter === 0)
								{  return_result(result[0]); }
							});
						}
						else
						{ return_result(null); }
					}
					else
					{ return_result(null); }

				});
			}
			else
			{
				return_result(null); 
			}
		});
	}
	catch(err)
	{ 
		console.log("getShippingDetails",err); 
		return_result(null); 
	}	
}

methods.addBillingAddress = function (arrParams,return_result){

	try
	{


		if(parseInt(arrParams.is_default) == 1 )
		{

			var query = `SELECT group_id FROM jaze_user_billing_address WHERE group_id IN 
				(SELECT group_id FROM jaze_user_billing_address WHERE meta_key = 'account_id' AND meta_value =?) GROUP BY group_id`;

				DB.query(query, [arrParams.company_id], function(data, error){
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					var	arrUpdate = {is_default   : 0};
					var updateQue = `update jaze_user_billing_address SET meta_value =? WHERE meta_key =? AND group_id =?`;
					for (let i in data) { 
				  		for (var key in arrUpdate) 
					 	{	
					 		DB.query(updateQue, [arrUpdate[key],key,data[i].group_id], function(data, error){});
					  	};
					}
				}
			
			});

		}

			HelperRestriction.getNewGroupId('jaze_user_billing_address', function(lastGrpId){

			var arrInsert = {
				account_id            : arrParams.company_id,
				first_name            : arrParams.first_name,
				last_name             : arrParams.last_name,
				mobile_country_code   : arrParams.mobile_country_code,
				mobile_no             : arrParams.mobile_no,
				landline_country_code : arrParams.landline_country_code,
				landline_no           : arrParams.landline_no,
				country            : arrParams.country,
				state              : arrParams.state,
				city               : arrParams.city,
				area               : arrParams.area,
				street_name           : arrParams.street_name,
				street_no             : arrParams.street_no,
				building_name         : arrParams.building_name,
				building_no           : arrParams.building_no,
				shipping_note         : arrParams.shipping_note,
				landmark              : arrParams.landmark,
				location_type         : arrParams.location_type,
				is_default            : arrParams.is_default,
				create_date           : standardDT,
				status                : 1
			};

	  		var querInsert = `insert into jaze_user_billing_address (group_id, meta_key, meta_value) VALUES (?,?,?)`;
	  		var arrRows = [];
		 	for (var key in arrInsert) 
		 	{
				DB.query(querInsert,[lastGrpId,key,arrInsert[key]], function(data, error){

		 		 	if(parseInt(data.affectedRows) > 0){
						arrRows.push(data.affectedRows);
			 	   	}

		 	 	});
		  	};

  			setTimeout(function(){ 		
				if(arrRows.length > 0){
					return_result(1);
				}else{
					return_result(0);
				}
  			}, 500);

		});


	}
	catch(err)
	{ 
		console.log("addBillingAddress",err); 
		return_result(0); 
	}	

};

methods.updateBillingAddress = function (arrParams,return_result){

	try
	{

		if(parseInt(arrParams.id) !== 0 )
		{

			if(parseInt(arrParams.is_default) == 1 )
			{

				var query = `SELECT group_id FROM jaze_user_billing_address WHERE group_id IN 
					(SELECT group_id FROM jaze_user_billing_address WHERE meta_key = 'account_id' AND meta_value =?) GROUP BY group_id`;

					DB.query(query, [arrParams.company_id], function(data, error){
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						var	arrUpdate = {is_default   : 0};
						var updateQue = `update jaze_user_billing_address SET meta_value =? WHERE meta_key =? AND group_id =? and group_id !=?`;
						for (let i in data) { 
					  		for (var key in arrUpdate) 
						 	{	
						 		DB.query(updateQue, [arrUpdate[key],key,data[i].group_id,arrParams.id], function(data, error){});
						  	};
						}
					}
				
				});


			}

			var query = `SELECT group_id FROM jaze_user_billing_address WHERE group_id =? LIMIT 1`;
			DB.query(query, [arrParams.id], function(data, error){
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
						
					var arrUpdate = {
						account_id            : arrParams.company_id,
						first_name            : arrParams.first_name,
						last_name             : arrParams.last_name,
						mobile_country_code   : arrParams.mobile_country_code,
						mobile_no             : arrParams.mobile_no,
						landline_country_code : arrParams.landline_country_code,
						landline_no           : arrParams.landline_no,
						country            : arrParams.country,
						state              : arrParams.state,
						city               : arrParams.city,
						area               : arrParams.area,
						street_name           : arrParams.street_name,
						street_no             : arrParams.street_no,
						building_name         : arrParams.building_name,
						building_no           : arrParams.building_no,
						shipping_note         : arrParams.shipping_note,
						landmark              : arrParams.landmark,
						location_type         : arrParams.location_type,
						is_default            : arrParams.is_default,
						create_date           : standardDT,
						status                : 1
					};

					var updateQue = `update jaze_user_billing_address SET meta_value =? WHERE meta_key =? AND group_id =?`;
					for (let i in data) { 
				  		for (var key in arrUpdate) 
					 	{	
					 		DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(data, error){
					 			if(data!=null){
					 				return_result(1);
					 			}
					 		});
					  	};
					}
				}
				else
				{
					return_result(2);
				}
			
			});

		}
		else
		{
			return_result(0);
		}


	}
	catch(err)
	{ 
		console.log("addBillingAddress",err); 
		return_result(0); 
	}	

};

methods.delBillingAddress = function(group_ids,arrParams, return_result){


	try
	{

		if(group_ids.length > 0 )
		{

			var query = `SELECT * FROM jaze_user_billing_address WHERE group_id IN(?)`;
			DB.query(query,[group_ids], function(qdata, error){

				if(typeof qdata !== 'undefined' && qdata !== null && parseInt(qdata.length) > 0)
				{	

					var queDel = `DELETE FROM jaze_user_billing_address WHERE group_id IN(?)`;
					DB.query(queDel,[group_ids], function(rdata, error){

						if(rdata == null){
				 	   		return_result(0);
				 	   	}else{
				 	   		

		 	   				var query = `SELECT * FROM jaze_user_billing_address WHERE 
		 	   				group_id IN (SELECT group_id FROM jaze_user_billing_address WHERE meta_key = 'account_id' AND meta_value ='41623') ORDER BY group_id DESC LIMIT 1`;

								DB.query(query, [arrParams.company_id], function(data, error){
						
								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
								{	
									var	arrUpdate = {is_default   : 1};
									var updateQue = `update jaze_user_billing_address SET meta_value =? WHERE meta_key =? AND group_id =?`;
							
							  		for (var key in arrUpdate) 
								 	{	
								 		DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(udata, error){});
								  	};
									
								}
							
							});
							return_result(1);
				 	   	}

					});
				}
				else
				{
					return_result(2);
				}

			});
		}
		else
		{
			return_result(0);
		}

	}
	catch(err)
	{ 
		console.log("delBillingAddress",err); 
		return_result(0); 
	}	
};

//------------------------------------------------------------------- billing & shipping address Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Card Module Start ---------------------------------------------------------------------------//

methods.getCreditCardDetails = function (arrParams,return_result){

	try
	{
		var query = " SELECT *  FROM jaze_user_credit_card WHERE group_id in (SELECT group_id  FROM jaze_user_credit_card WHERE  meta_key = 'account_id' and meta_value = '"+arrParams.company_id+"' and group_id in ";
		    query += " (SELECT group_id  FROM jaze_user_credit_card WHERE  meta_key = 'status' and meta_value = '1')) ";

			DB.query(query,null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
					HelperRestriction.convertMultiMetaArray(data, function(data_result){	
								
			
						if(data_result !== null)
						{
							var counter = data_result.length;
	
							var array_rows = [];
	
							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data_result,[function (list) {
					
									return new Promise(function (resolve, reject) {
										
										var card_name  = list.card_name !="" ? list.card_name : "";
										var bank_name  = list.bank_name !="" ? list.bank_name : "";
										var card_no    = list.card_no !="" ? list.card_no : "";
										var exp_month  = list.exp_month !="" ? list.exp_month : "";
										var exp_year   = list.exp_year !="" ? list.exp_year : "";
										var csv        = list.csv !="" ? list.csv : "";

										var newObj = {
											id			    : list.group_id.toString(),
											is_default	 	: list.is_default.toString(),
											card_name		: card_name,
											bank_name	 	: bank_name,
											card_no	 		: card_no,
											exp_month	 	: exp_month,
											exp_year  		: exp_year,
											csv	 			: csv											
										};

										array_rows.push(newObj);

										resolve(array_rows);

									  counter -= 1;
									})
								}],
								function (result, current) {
													
									if (counter === 0)
									{  return_result(result[0]); }
								});
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }
	
					});
				}
				else
				{
					return_result(null); 
				}
			});
	}
	catch(err)
	{ 
		console.log("getCreditCardDetails",err); 
		return_result(null); 
	}	
};

methods.addCardDetails = function (arrParams,return_result){

	try
	{

		if(parseInt(arrParams.is_default) == 1 )
		{

			var query = `SELECT group_id FROM jaze_user_credit_card WHERE group_id IN 
				(SELECT group_id FROM jaze_user_credit_card WHERE meta_key = 'account_id' AND meta_value =?) GROUP BY group_id`;

				DB.query(query, [arrParams.company_id], function(data, error){
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					var	arrUpdate = {is_default   : 0};
					var updateQue = `update jaze_user_credit_card SET meta_value =? WHERE meta_key =? AND group_id =?`;
					for (let i in data) { 
				  		for (var key in arrUpdate) 
					 	{	
					 		DB.query(updateQue, [arrUpdate[key],key,data[i].group_id], function(data, error){});
					  	};
					}
				}
			
			});

		}

			HelperRestriction.getNewGroupId('jaze_user_credit_card', function(lastGrpId){


			var arrInsert = {
				account_id     : arrParams.company_id,
				card_name      : arrParams.card_name,
				bank_name      : arrParams.bank_name,
				card_no        : arrParams.card_no,
				exp_month      : arrParams.exp_month,
				exp_year       : arrParams.exp_year,
				csv            : arrParams.csv,
				is_default     : arrParams.is_default,
				create_date    : standardDT,
				status         : 1
			};
		
	  		var querInsert = `insert into jaze_user_credit_card (group_id, meta_key, meta_value) VALUES (?,?,?)`;

		 	for (var key in arrInsert) 
		 	{
				DB.query(querInsert,[lastGrpId,key,arrInsert[key]], function(data, error){

		 		 	if(parseInt(data.affectedRows) > 0){
						return_result(data.affectedRows)
			 	   	}else{
		   				return_result(0);
			 	   	}
		 	 	});
		  	};

		});


	}
	catch(err)
	{ 
		console.log("addCardDetails",err); 
		return_result(0); 
	}	

};

methods.updateCardDetails = function (arrParams,return_result){

	try
	{

		if(parseInt(arrParams.id) !== 0 )
		{

			if(parseInt(arrParams.is_default) == 1 )
			{

				var query = `SELECT group_id FROM jaze_user_credit_card WHERE group_id IN 
					(SELECT group_id FROM jaze_user_credit_card WHERE meta_key = 'account_id' AND meta_value =?) GROUP BY group_id`;

					DB.query(query, [arrParams.company_id], function(data, error){
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						var	arrUpdate = {is_default   : 0};
						var updateQue = `update jaze_user_credit_card SET meta_value =? WHERE meta_key =? AND group_id =? and group_id !=?`;
						for (let i in data) { 
					  		for (var key in arrUpdate) 
						 	{	
						 		DB.query(updateQue, [arrUpdate[key],key,data[i].group_id,arrParams.id], function(data, error){});
						  	};
						}
					}
				
				});


			}

			var query = `SELECT group_id FROM jaze_user_credit_card WHERE group_id =? LIMIT 1`;
			DB.query(query, [arrParams.id], function(data, error){
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
						
					var arrUpdate = {
						account_id     : arrParams.company_id,
						card_name      : arrParams.card_name,
						bank_name      : arrParams.bank_name,
						card_no        : arrParams.card_no,
						exp_month      : arrParams.exp_month,
						exp_year       : arrParams.exp_year,
						csv            : arrParams.csv,
						is_default     : arrParams.is_default,
						create_date    : standardDT,
						status         : 1
					};

					var updateQue = `update jaze_user_credit_card SET meta_value =? WHERE meta_key =? AND group_id =?`;
					for (let i in data) { 
				  		for (var key in arrUpdate) 
					 	{	
		 				   if(arrUpdate[key] !== null && arrUpdate[key] != ""){
						 		DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(data, error){
						 			if(data!=null){
						 				return_result(1);
						 			}
						 		});
					 		}
					  	};
					}
				}
				else
				{
					return_result(2);
				}
			
			});

		}
		else
		{
			return_result(0);
		}


	}
	catch(err)
	{ 
		console.log("updateCardDetails",err); 
		return_result(0); 
	}	

};

methods.delCardDetails = function(group_ids,arrParams, return_result){


	try
	{

		if(group_ids.length > 0 )
		{

			var query = `SELECT * FROM jaze_user_credit_card WHERE group_id IN(?)`;
			DB.query(query,[group_ids], function(qdata, error){

				if(typeof qdata !== 'undefined' && qdata !== null && parseInt(qdata.length) > 0)
				{	

					var queDel = `DELETE FROM jaze_user_credit_card WHERE group_id IN(?)`;
					DB.query(queDel,[group_ids], function(rdata, error){

						if(rdata == null){
				 	   		return_result(0);
				 	   	}else{
				 	   		

		 	   				var query = `SELECT * FROM jaze_user_credit_card WHERE 
		 	   				group_id IN (SELECT group_id FROM jaze_user_credit_card WHERE meta_key = 'account_id' AND meta_value =?) ORDER BY group_id DESC LIMIT 1`;

								DB.query(query, [arrParams.company_id], function(data, error){
						
								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
								{	
									var	arrUpdate = {is_default   : 1};
									var updateQue = `update jaze_user_credit_card SET meta_value =? WHERE meta_key =? AND group_id =?`;
							
							  		for (var key in arrUpdate) 
								 	{	
								 		DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(udata, error){});
								  	};
									
								}
							
							});
							return_result(1);
				 	   	}

					});
				}
				else
				{
					return_result(2);
				}

			});
		}
		else
		{
			return_result(0);
		}

	}
	catch(err)
	{ 
		console.log("delCardDetails",err); 
		return_result(0); 
	}	
};

//------------------------------------------------------------------- Card Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Bank Module Start ---------------------------------------------------------------------------//

methods.getBankDetails = function (arrParams,return_result){

	try
	{
		var query = " SELECT *  FROM jaze_user_bank_details WHERE group_id in (SELECT group_id  FROM jaze_user_bank_details WHERE  meta_key = 'account_id' and meta_value = '"+arrParams.company_id+"' and group_id in ";
		    query += " (SELECT group_id  FROM jaze_user_bank_details WHERE  meta_key = 'status' and meta_value = '1')) ";

			DB.query(query,null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
					HelperRestriction.convertMultiMetaArray(data, function(data_result){	
								
						if(data_result !== null)
						{
							var counter = data_result.length;
							
							var array_rows = [];
	
							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data_result,[function (list) {
					
									return new Promise(function (resolve, reject) {
										
										if(list.account_no !== "" && list.account_no !== null)
										{

											var holder_name = list.holder_name !="" ? list.holder_name : "";
											var bank_name   = list.bank_name !="" ? list.bank_name : "";
											var branch_name = list.branch_name !="" ? list.branch_name : "";
											var iban_no     = list.iban_no !="" ? list.iban_no : "";
											var swift_code  = list.swift_code !="" ? list.swift_code : "";
										

											var newObj = {
												id			    : list.group_id.toString(),
												is_default	 	: list.is_default.toString(),
												holder_name		: holder_name,
												account_no	 	: list.account_no.toString(),
												bank_name	 	: bank_name,
												branch_name	 	: branch_name,
												iban_no  		: iban_no,
												swift_code	 	: swift_code											
											};
											
											array_rows.push(newObj);

											resolve(array_rows);
										}

									     counter -= 1;
									})
								}],
								function (result, current) {
													
									if (counter === 0)
									{  return_result(result[0]); }
								});
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }
	
					});
				}
				else
				{
					return_result(null); 
				}
			});
	}
	catch(err)
	{ 
		console.log("getBankDetails",err); 
		return_result(null); 
	}	

};

methods.addBankDetails = function (arrParams,return_result){

	try
	{

		if(parseInt(arrParams.is_default) == 1 )
		{

			var query = `SELECT group_id FROM jaze_user_bank_details WHERE group_id IN 
				(SELECT group_id FROM jaze_user_bank_details WHERE meta_key = 'account_id' AND meta_value =?) GROUP BY group_id`;

				DB.query(query, [arrParams.company_id], function(data, error){
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					var	arrUpdate = {is_default   : 0};
					var updateQue = `update jaze_user_bank_details SET meta_value =? WHERE meta_key =? AND group_id =?`;
					for (let i in data) { 
				  		for (var key in arrUpdate) 
					 	{	
					 		DB.query(updateQue, [arrUpdate[key],key,data[i].group_id], function(data, error){});
					  	};
					}
				}
			
			});

		}

			HelperRestriction.getNewGroupId('jaze_user_bank_details', function(lastGrpId){

			var arrInsert = {
				account_id            : arrParams.company_id,
				holder_name           : arrParams.holder_name,
				account_no            : arrParams.account_no,
				bank_name             : arrParams.bank_name,
				branch_name           : arrParams.branch_name,
				iban_no               : arrParams.iban_no,
				swift_code            : arrParams.swift_code,
				is_default            : arrParams.is_default,
				create_date           : standardDT,
				status                : 1
			};
		
	  		var querInsert = `insert into jaze_user_bank_details (group_id, meta_key, meta_value) VALUES (?,?,?)`;

		 	for (var key in arrInsert) 
		 	{
				DB.query(querInsert,[lastGrpId,key,arrInsert[key]], function(data, error){

		 		 	if(parseInt(data.affectedRows) > 0){
						return_result(data.affectedRows)
			 	   	}else{
		   				return_result(0);
			 	   	}
		 	 	});
		  	};

		});


	}
	catch(err)
	{ 
		console.log("addBank",err); 
		return_result(0); 
	}	

};

methods.updateBankDetails = function (arrParams,return_result){

	try
	{

		if(parseInt(arrParams.id) !== 0 )
		{

			if(parseInt(arrParams.is_default) == 1 )
			{

				var query = `SELECT group_id FROM jaze_user_bank_details WHERE group_id IN 
					(SELECT group_id FROM jaze_user_bank_details WHERE meta_key = 'account_id' AND meta_value =?) GROUP BY group_id`;

					DB.query(query, [arrParams.company_id], function(data, error){
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						var	arrUpdate = {is_default   : 0};
						var updateQue = `update jaze_user_bank_details SET meta_value =? WHERE meta_key =? AND group_id =? and group_id !=?`;
						for (let i in data) { 
					  		for (var key in arrUpdate) 
						 	{	
						 		DB.query(updateQue, [arrUpdate[key],key,data[i].group_id,arrParams.id], function(data, error){});
						  	};
						}
					}
				
				});


			}

			var query = `SELECT group_id FROM jaze_user_bank_details WHERE group_id =? LIMIT 1`;
			DB.query(query, [arrParams.id], function(data, error){
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
						
					var arrUpdate = {
						account_id            : arrParams.company_id,
						holder_name           : arrParams.holder_name,
						account_no            : arrParams.account_no,
						bank_name             : arrParams.bank_name,
						branch_name           : arrParams.branch_name,
						iban_no               : arrParams.iban_no,
						swift_code            : arrParams.swift_code,
						is_default            : arrParams.is_default,
						create_date           : standardDT,
						status                : 1
					};

					var updateQue = `update jaze_user_bank_details SET meta_value =? WHERE meta_key =? AND group_id =?`;
					for (let i in data) { 
				  		for (var key in arrUpdate) 
					 	{	
				 		   if(arrUpdate[key] !== null && arrUpdate[key] != ""){
						 		DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(data, error){
						 			if(data!=null){
						 				return_result(1);
						 			}
						 		});
					 		}
					  	};
					}
				}
				else
				{
					return_result(2);
				}
			
			});

		}
		else
		{
			return_result(0);
		}


	}
	catch(err)
	{ 
		console.log("updateBankDetails",err); 
		return_result(0); 
	}	

};

methods.delBankDetails = function(group_ids,arrParams, return_result){


	try
	{

		if(group_ids.length > 0 )
		{

			var query = `SELECT * FROM jaze_user_bank_details WHERE group_id IN(?)`;
			DB.query(query,[group_ids], function(qdata, error){

				if(typeof qdata !== 'undefined' && qdata !== null && parseInt(qdata.length) > 0)
				{	

					var queDel = `DELETE FROM jaze_user_bank_details WHERE group_id IN(?)`;
					DB.query(queDel,[group_ids], function(rdata, error){

						if(rdata == null){
				 	   		return_result(0);
				 	   	}else{
				 	   		

		 	   				var query = `SELECT * FROM jaze_user_bank_details WHERE 
		 	   				group_id IN (SELECT group_id FROM jaze_user_bank_details WHERE meta_key = 'account_id' AND meta_value =?) ORDER BY group_id DESC LIMIT 1`;

								DB.query(query, [arrParams.company_id], function(data, error){
						
								if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
								{	
									var	arrUpdate = {is_default   : 1};
									var updateQue = `update jaze_user_bank_details SET meta_value =? WHERE meta_key =? AND group_id =?`;
							
							  		for (var key in arrUpdate) 
								 	{	
								 		DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(udata, error){});
								  	};
									
								}
							
							});
							return_result(1);
				 	   	}

					});
				}
				else
				{
					return_result(2);
				}

			});
		}
		else
		{
			return_result(0);
		}

	}
	catch(err)
	{ 
		console.log("delBankDetails",err); 
		return_result(0); 
	}	
};

//------------------------------------------------------------------- Bank Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- portfolio Module Start ---------------------------------------------------------------------------//

methods.getPortfolio = function (arrParams,return_result){

	try
	{
		var query = ` SELECT * FROM jaze_portfolios WHERE group_id IN (SELECT group_id FROM jaze_portfolios WHERE meta_key = 'account_id' AND meta_value =?)`;

			DB.query(query,[arrParams.company_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
					HelperRestriction.convertMultiMetaArray(data, function(data_result){	
								
						if(data_result !== null)
						{
							var counter = data_result.length;
	
							var array_rows = [];
	
							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data_result,[function (list) {
					
									return new Promise(function (resolve, reject) {

										var accParams = {account_id:list.portfolio_account_id};

										methods.getCompanyProfile(accParams, function(retAccount){

											if(retAccount!=null)
											{
												var newObj = {
													id		    : list.group_id.toString(),
													account_id	: list.portfolio_account_id,
													account_type	: retAccount.account_type,
													name		: retAccount.name,
													logo	 	: retAccount.logo							
												};

												counter -= 1;
												array_rows.push(newObj);

												if(counter === 0){
													resolve(array_rows);
												}
											}
										
									
										  
								  		});

									})
								}],
								function (result, current) {
													
									if (counter === 0)
									{  return_result(result[0]); }
								});
							}
							else
							{ return_result(null); }
						}
						else
						{ return_result(null); }
	
					});
				}
				else
				{
					return_result(null); 
				}
			});
	}
	catch(err)
	{ 
		console.log("getPortfolio",err); 
		return_result(null); 
	}	
};

methods.updatePortfolio = function (arrParams,return_result){

	try
	{

		if(parseInt(arrParams.company_id) !== 0 )
		{

			if(parseInt(arrParams.flag) == 1 )
			{


				var query = `SELECT * FROM jaze_portfolios WHERE group_id IN (SELECT group_id FROM jaze_portfolios WHERE meta_key = 'account_id' AND meta_value =?)
						AND group_id IN (SELECT group_id FROM jaze_portfolios WHERE meta_key = 'portfolio_account_id' AND meta_value =?)`;
				DB.query(query, [arrParams.company_id,arrParams.request_company_id], function(data, error){
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
								
						return_result(3);

					}
					else
					{
						
						HelperRestriction.getNewGroupId('jaze_portfolios', function (group_id) {

							if(parseInt(group_id) !== 0){

								var arrInsert = {
									account_id            : arrParams.account_id,
									portfolio_account_id  : arrParams.company_id,
									create_date           : standardDT,
									status                : 1
								};

								HelperRestriction.insertTBQuery('jaze_portfolios',group_id,arrInsert, function (queGrpId) {

									return_result(1);
								});
							}
						});

					}
				
				});

			}
			else if(parseInt(arrParams.flag) == 3 )
			{



				var query = `SELECT * FROM jaze_portfolios WHERE group_id =?`;
				DB.query(query, [arrParams.id], function(data, error){
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						return_result(2);
					}
					else
					{
						
						var query = `DELETE FROM jaze_portfolios WHERE group_id =?`;
						DB.query(query, [data[0].group_id], function(ddata, error){
							if(ddata !== null){
								return_result(1);
							}else{
								return_result(0);
							}
						
						});

					}
				
				});

			}
			else
			{
				return_result(0);
			}


		}
		else
		{
			return_result(0);
		}


	}
	catch(err)
	{ 
		console.log("updatePortfolio",err); 
		return_result(0); 
	}	

};


//------------------------------------------------------------------- portfolio Module end ---------------------------------------------------------------------------//

//------------------------------------------------------------------- security Module end ---------------------------------------------------------------------------//

methods.updateJazeSecurity = function (arrParams,return_result){

	try
	{

		if(parseInt(arrParams.account_id) !== 0 )
		{

			if(parseInt(arrParams.flag) == 1 )
			{


   				var query = `SELECT * FROM jaze_user_credential_details WHERE account_id =? AND password=?`;

				DB.query(query, [arrParams.company_id,arrParams.old_password], function(data, error){
			
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{	
						
						if(arrParams.new_password == arrParams.con_password )
						{
							var updateQue = `update jaze_user_credential_details SET password =? WHERE account_id =?`;
						
					 		DB.query(updateQue, [arrParams.new_password], function(udata, error){
					 			return_result('1');
					 		});
						}
						else
						{
							return_result('Confirm password is not match.');
						}  	
						
					}
					else
					{
						return_result('Old Password does not match.');
					}
				
				});

			}
			else if(parseInt(arrParams.flag) == 2 )
			{



   				var query = `SELECT * FROM jaze_user_credential_details WHERE account_id =? AND jaze_security_pin=?`;

				DB.query(query, [arrParams.company_id,arrParams.old_sec_pin], function(data, error){
			
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{	
						
						if(arrParams.new_sec_pin == arrParams.con_sec_pin )
						{
							var updateQue = `update jaze_user_credential_details SET jaze_security_pin =? WHERE account_id =?`;
						
					 		DB.query(updateQue, [arrParams.new_sec_pin], function(udata, error){
					 			return_result('1');
					 		});
						}
						else
						{
							return_result('Confirm ecurity pin is not match.');
						}  	
						
					}
					else
					{
						return_result('Old security pin does not match.');
					}
				
				});

			}
			else if(parseInt(arrParams.flag) == 3 )
			{
				

   				var query = `SELECT * FROM jaze_user_basic_details WHERE account_id =? AND meta_key='company_phone'`;

				DB.query(query, [arrParams.company_id], function(data, error){
			
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{	
						
						if(arrParams.new_country_code !=""  && arrParams.new_mobile_no !="" )
						{	

				 			var	arrUpdate = {
								company_phone      : arrParams.new_mobile_no,
								company_phone_code : arrParams.new_country_code
							};

							var updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key =? AND account_id =?`;
							methods.updateQuery(updateQue,arrUpdate,arrParams.account_id, function(updateStat){
								if(parseInt(updateStat) > 0){
									return_result('1');
								}
								else
								{
									return_result('Unable to process request. Please try again.');
								}

							});
						}
						else
						{
							return_result('County code and mobile number should not be empty.');
						}  	
						
					}
					else
					{

						var	arrInsert = {
							company_phone      : arrParams.new_mobile_no,
							company_phone_code : arrParams.new_country_code					
						};

						HelperRestriction.insertTBQuery('jaze_user_basic_details',account_id,arrInsert, function (queGrpId) {
							return_result(1);
						});
					}
				
				});

			}
			else if(parseInt(arrParams.flag) == 4 )
			{

   				var query = `SELECT * FROM jaze_user_basic_details WHERE account_id =? AND meta_key='company_email'`;

				DB.query(query, [arrParams.company_id,arrParams.old_email], function(data, error){
			
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{	
						
			 			var	arrUpdate = {
							company_email : arrParams.new_email
						};

						var updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key =? AND account_id =?`;
						methods.updateQuery(updateQue,arrUpdate,arrParams.account_id, function(updateStat){
							if(parseInt(updateStat) > 0){
								return_result('1');
							}
							else
							{
								return_result('Unable to process request. Please try again.');
							}

						});
						
					}
					else
					{
						return_result('Old email does not match.');
					}
				
				});

			}


		}
		else
		{
			return_result(0);
		}


	}
	catch(err)
	{ 
		console.log("updatePortfolio",err); 
		return_result(0); 
	}	

};


//------------------------------------------------------------------- security Module end ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Company Profile Logo Start -------------------------------------------------------------------------//

methods.getCompanyLogos =  function(arrParams,return_result){
	try
	{

		var arrAccounts = [];

		var query = `SELECT * FROM jaze_user_basic_details WHERE account_id =? AND meta_key ='company_logo'`;
		if(parseInt(arrParams.flag) == 2){
			query = `SELECT * FROM jaze_user_basic_details WHERE account_id =? AND meta_key ='listing_images'`;
		}

		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArrayCompany(data,function(retConverted){

					var	updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key ='company_logo' AND account_id =?`;
					var storagePathLogo = G_STATIC_IMG_PATH+'company/'+arrParams.account_id+'/logo/'+retConverted[0].company_logo;
			 	
					if(parseInt(arrParams.flag) == 2){
						updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key ='listing_images' AND account_id =?`;
						storagePathLogo = G_STATIC_IMG_PATH+'company/'+arrParams.account_id+'/listing_images/'+retConverted[0].listing_images;
					}

					fs.unlink(storagePathLogo, function (err) {});

					DB.query(updateQue, [arrParams.file_name,data[0].account_id], function(datau, error){
				 		if(datau !== null){
			 				return_result(1);
				 	   	}else{
			 	   			return_result(0);
				 	   	}
					});

				});
			
			}
			else
			{
				return_result(null);
			}
		});

	}
	catch(err)
	{ console.log("getProfilePicture",err); return_result(null);}
};

//------------------------------------------------------------------- Company Profile Logo End---------------------------------------------------------------------------//

//------------------------------------------------------------------- Company jazepay start ---------------------------------------------------------------------------//

methods.getJazePayStatus = function (arrParams,return_result){
	try
	{


		var query = `SELECT * FROM jaze_user_jazecome_status WHERE 
		group_id IN (SELECT group_id FROM jaze_user_jazecome_status WHERE meta_key = 'account_id' AND meta_value =? )`;

		DB.query(query,[arrParams.company_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data,function(retConverted){

	
					if(retConverted[0].jaze_pay){
						return_result(retConverted[0].jaze_pay.toString());
					}
					else
					{
						return_result('0');
					}
					

				});
				
			
			}
			else
			{

				return_result('0');

				
			}

		});
	
	}
	catch(err)
	{ console.log("getJazePayStatus",err); return_result('0'); }	
};


methods.updateJazePaystatus = function (arrParams,return_result){
	try
	{


		var query = `SELECT * FROM jaze_user_jazecome_status WHERE 
		group_id IN (SELECT group_id FROM jaze_user_jazecome_status WHERE meta_key = 'account_id' AND meta_value =? )`;
		DB.query(query,[arrParams.company_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var group_id = data[0].group_id;

				var inQuery = `SELECT * FROM jaze_user_jazecome_status WHERE meta_key = 'jaze_pay' 
				AND group_id IN ( SELECT group_id FROM jaze_user_jazecome_status WHERE meta_key ='account_id' AND meta_value =?)`;

				DB.query(inQuery,[arrParams.company_id], function(inData, error){

					if(typeof inData !== 'undefined' && inData !== null && parseInt(inData.length) > 0)
					{


						var	arrUpdate = {
							jaze_pay  : arrParams.status,
						};

						var updateQue = `update jaze_user_jazecome_status SET meta_value =? WHERE meta_key =? AND group_id =? `;
						
						for (var key in arrUpdate) 
					 	{	
						     if(arrUpdate[key] !== null && arrUpdate[key] != ""){
								DB.query(updateQue, [arrUpdate[key],key,group_id], function(datau, error){
							 		if(datau !== null){
						 				return_result(1);
							 	   	}else{
						 	   			return_result(0);
							 	   	}
								});
							}
						}	

					}
					else
					{

						var arrInsert = {
							jaze_pay  : arrParams.status,
						};
					
						var plusOneGroupID = lastGrpId+1;
				  		var querInsert = `insert into jaze_user_jazecome_status (group_id, meta_key, meta_value) VALUES (?,?,?)`;

				  		var returnFunction;
					 	for (var key in arrInsert) 
					 	{
							DB.query(querInsert,[group_id,key,arrInsert[key]], function(data, error){
					 		 	if(parseInt(data.affectedRows) > 0){
			 						return_result(1);
						 	   	}else{
				   					return_result(0);
						 	   	}
					 	 	});
					  	};

					}

				});


			}
			else
			{



				var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_user_jazecome_status";
	  			var lastGrpId = 0;

				methods.getLastGroupId(queGroupMax, function(queGrpRes){

				  	queGrpRes.forEach(function(row) {
						lastGrpId = row.lastGrpId;
				  	});

					var arrInsert = {
						account_id : arrParams.company_id,
						jaze_call  : 0,
						jaze_chat  : 0,
						jaze_mail  : 0,
						jaze_diary : 0,
						jaze_pay   : arrParams.status
					};
				
					var plusOneGroupID = lastGrpId+1;
			  		var querInsert = `insert into jaze_user_jazecome_status (group_id, meta_key, meta_value) VALUES (?,?,?)`;

			  		var returnFunction;
				 	for (var key in arrInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
				 		 	if(parseInt(data.affectedRows) > 0){
		 						return_result(1);
					 	   	}else{
			   					return_result(0);
					 	   	}
				 	 	});
				  	};

				});

			}

		});
	
	}
	catch(err)
	{ console.log("updateJazePaystatus",err); return_result('0'); }	
};

//------------------------------------------------------------------- jazepay  Module end ---------------------------------------------------------------------------//

// ------------------------------------------------------ Company Category Selection Module Start  --------------------------------------------------------------//   

methods.getCategoryParentDetails = function(arrParams,return_result)
{
	var return_obj = {
			current_cat_id 		: "0",
			current_cat_name 	: "",
			previous_cat_id 	: "-1",
			previous_cat_name 	: "",
			main_cat_id     	: '0',
			main_cat_name   	: 'home',
			parent_status   	: '0'
		}
	   
	try
	{ 
		if(arrParams !== null)
		{
			if(parseInt(arrParams.category_id) === 0)
			{
				//home category 
				return_result(return_obj);
			}
			else if(parseInt(arrParams.category_id) === -1)
			{
				return_result(return_obj);
			}
			else if(parseInt(arrParams.category_id) !== 0)
			{
				//get parent details	
				methods.__getCategoryParentId(arrParams.category_id,arrParams.flag, function(parent_id){	

					if(parseInt(arrParams.category_id) !== 0)
					{
						methods.__getCategoryDetails(parent_id, function(currentDetails){	

							methods.__getCategoryDetails(currentDetails.parent_id, function(previousDetails){	

								return_obj = {
									current_cat_id 		: currentDetails.group_id.toString(),
									current_cat_name 	: currentDetails.cate_name,
									previous_cat_id 	: previousDetails.group_id.toString(),
									previous_cat_name 	: previousDetails.cate_name,
									main_cat_id     	: '0',
									main_cat_name   	: 'home',
									parent_status   	: '1'
								}

								return_result(return_obj);
							});
						});
					}
					else
					{
						return_obj = {
							current_cat_id 		: "1",
							current_cat_name 	: "products and services",
							previous_cat_id 	: "-1",
							previous_cat_name 	: "",
							main_cat_id     	: '0',
							main_cat_name   	: 'home',
							parent_status   	: '1'
						}

						return_result(return_obj);
					}
				});
			}
			else
			{ return_result(return_obj);}
		}
		else
		{ return_result(return_obj);}
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}
 
methods.getCategoryList = function(arrParams,return_result)
{
	try
	{
		if(arrParams !== null)
		{
			if(parseInt(arrParams.category_id) === 0)
			{
				//home category 
				var query  = " SELECT * FROM jaze_category_list WHERE  ";
					query += " group_id IN (SELECT group_id FROM jaze_category_list WHERE meta_key = 'status'  AND meta_value = 1) ";
					query += " and group_id IN (8,1834,9,1833,1331,1354,1355,1)";

					DB.query(query,null, function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
	
							HelperRestriction.convertMultiMetaArray(data, function(retConverted){	
							
								var array_rows = [];

								var counter = retConverted.length;
								
								if(parseInt(counter) !== 0)
								{
									// var arrObj = {
									// 	cat_id 			: '-1',
									// 	cat_name 		: 'products',
									// 	cat_image       : '',
									// 	child_status    : '0',
									// 	status   		: '0'
									// }
									
									// array_rows.push(arrObj);

									promiseForeach.each(retConverted,[function (list) {

										return new Promise(function (resolve, reject) {

											methods.__getChildCategoryDetails(list.group_id, function(childcat){	

												var child_status = '0';

												if(parseInt(childcat) !== 0)
												{ child_status = '1'; }

												var arrObj = {
													id 				: list.group_id.toString(),
													name 			: list.cate_name,
													parent_id       : '0',
													logo       		: '',
													child_status    : child_status.toString(),
													status   		: '0'
												}
												
												array_rows.push(arrObj);
												
												counter -= 1;

												resolve(array_rows);
											});
										});
									}],
									function (result, current) {										
										if (counter === 0)
										{ return_result(fast_sort(result[0]).asc('cat_name')); }
									});	
								}
								else
								{ return_result(null); }

							});
						}
						else
						{ return_result(null); }
					});
			}
			else if(parseInt(arrParams.category_id) === -1)
			{
				return_result(null);
			}
			else if(parseInt(arrParams.category_id) !== 0)
			{
				//get parent id

				methods.__getCategoryParentId(arrParams.category_id,arrParams.flag, function(parent_id){	

					var query  = " SELECT * FROM jaze_category_list WHERE  ";
						query += " group_id IN (SELECT group_id FROM jaze_category_list WHERE meta_key = 'status'  AND meta_value = 1 ";
						query += " and group_id IN (SELECT group_id FROM jaze_category_list WHERE meta_key = 'parent_id'  AND meta_value = '"+parent_id+"')) ";

						DB.query(query,null, function(data, error){

							if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

								HelperRestriction.convertMultiMetaArray(data, function(retConverted){	
								
									var array_rows = [];

									var counter = retConverted.length;
									
									if(parseInt(counter) !== 0)
									{
										promiseForeach.each(retConverted,[function (list) {

											return new Promise(function (resolve, reject) {

												methods.__getChildCategoryDetails(list.group_id, function(childcat){	

													var child_status = '0';

													if(parseInt(childcat) !== 0)
													{ child_status = '1'; }

													var status = '0';

													if(parseInt(list.group_id) === parseInt(arrParams.category_id))
													{ status = '1'; }

													var arrObj = {
														id 				: list.group_id.toString(),
														name 			: list.cate_name,
														logo       		: G_STATIC_IMG_URL+'uploads/services_cat_images/'+list.image_url,
														parent_id       : list.parent_id.toString(),
														child_status    : child_status.toString(),
														status   		: status
													}
													
													array_rows.push(arrObj);
													
													counter -= 1;

													resolve(array_rows);
												});
											});
										}],
										function (result, current) {										
											if (counter === 0)
											{ return_result(fast_sort(result[0]).asc('name')); }
										});	
									}
									else
									{ return_result(null); }

								});
							}
							else
							{ 
								//get current category details
								var query  = " SELECT * FROM jaze_category_list WHERE  ";
									query += " group_id IN (SELECT group_id FROM jaze_category_list WHERE meta_key = 'status'  AND meta_value = 1) ";
									query += " and group_id  = '"+arrParams.category_id+"' ";

									DB.query(query,null, function(data, error){

										if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					
											HelperRestriction.convertMultiMetaArray(data, function(retConverted){	

												if(retConverted !== null)
												{
													var arrObj = {
														id 				: retConverted[0].group_id.toString(),
														name 			: retConverted[0].cate_name,
														logo       		: G_STATIC_IMG_URL+'uploads/services_cat_images/'+retConverted[0].image_url,
														parent_id       : retConverted[0].parent_id.toString(),
														child_status    : '0',
														status   		: '1'
													}

													return_result(arrObj);
												}
												else
												{ return_result(null); }
											});
										}
										else
										{ return_result(null); }
									});
							}
						});
				});
			
			}
			else
			{ return_result(null); }
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

/*
* get Main Category Details
*/
methods.__getCategoryDetails = function(category_id,return_result)
{
	var return_obj = {
		group_id 	: '-1',
		cate_name   : '',
		parent_id   : '0',
		image_url   : ''
	}

	try
	{
		if(parseInt(category_id) !== 0)
		{ 
			var query  = " SELECT * FROM jaze_category_list WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_category_list WHERE meta_key = 'status'  AND meta_value = 1 ";
				query += " and group_id  = '"+category_id+"') ";
			
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(retConverted){	

							if(retConverted !== null)
							{ return_result(retConverted[0]); }
							else
							{ return_result(return_obj); }
						});
					}
					else
					{ return_result(return_obj); }
				});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log('__getCategoryDetails',err);
		return_result(return_obj);
	}
}

/*
* get have Child Category
*/
methods.__getCategoryParentId = function(category_id,flag,return_result)
{
	try
	{
		if(parseInt(flag) === 1)
		{
			var query  = " SELECT meta_value FROM jaze_category_list WHERE meta_key = 'parent_id'  AND group_id = '"+category_id+"'  ";
			
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0].meta_value); 
					}
					else
					{ return_result(0); }
				});
		}
		else
		{ return_result(category_id); }
	}
	catch(err)
	{
		console.log('__getCategoryParentId',err);
		return_result(0);
	}
}

/*
* check have Child Category
*/
methods.__getChildCategoryDetails = function(category_id,return_result)
{
	try
	{
		if(parseInt(category_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_category_list WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_category_list WHERE meta_key = 'status'  AND meta_value = 1 ";
				query += " and group_id IN (SELECT group_id FROM jaze_category_list WHERE meta_key = 'parent_id'  AND meta_value = '"+category_id+"')) ";
			
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data.length); 
					}
					else
					{ return_result(0); }
				});
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{
		console.log('__getChildCategoryDetails',err);
		return_result(0);
	}
}
// ------------------------------------------------------ Company Category Selection Module End  --------------------------------------------------------------//   


// ------------------------------------------------------ Company Team Start ----------------------------------------------------------------------------------//   

methods.getManageTeamList = function(arrParams,return_result)
{
	try
	{
		
		var query  = `SELECT * FROM jaze_company_team_details WHERE  
		group_id IN (SELECT group_id FROM jaze_company_team_details WHERE meta_key = 'account_id'  AND meta_value =?)
		AND group_id IN (SELECT group_id FROM jaze_company_team_details WHERE meta_key = 'status' AND meta_value = 1 )`;

		DB.query(query,[arrParams.company_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				HelperRestriction.convertMultiMetaArray(data, function(retConverted){	
				
					var array_rows = [];

					var counter = retConverted.length;
					
					if(parseInt(counter) !== 0)
					{
						promiseForeach.each(retConverted,[function (list) {

							return new Promise(function (resolve, reject) {

								var accParams = {account_id:list.team_account_id};

								methods.getCompanyProfile(accParams, function(retProfile){

									methods.getTeamDeparment(retProfile.team_department_id, function(retDepartment){

										var arrObj = {
											id 				: list.group_id.toString(),
											name 			: retProfile.name,
											logo       		: retProfile.logo,
											title       	: retProfile.team_position,
											department      : retDepartment.department_name,
											date            : list.create_date,
								
										}
										
										array_rows.push(arrObj);
										
										counter -= 1;
										if (counter === 0)
										{
											resolve(array_rows);
										}
									
									});
								});
							});
						}],
						function (result, current) {										
							if (counter === 0)
							{ 
								return_result(fast_sort(result[0]).asc('name')); 
							}
						});	
					}
					else
					{ return_result(null); }

				});

			}
			else
			{ return_result(null); }
		});

	}
	catch(err)
	{
		console.log('getManageTeamList',err);
		return_result(null);
	}

};


methods.getPendingManageTeam = function(arrParams,return_result)
{
	try
	{
		
		var query  = `SELECT * FROM jaze_company_team_details WHERE  
		group_id IN (SELECT group_id FROM jaze_company_team_details WHERE meta_key = 'account_id'  AND meta_value =?)
		AND group_id IN (SELECT group_id FROM jaze_company_team_details WHERE meta_key = 'status' AND meta_value = 0 )`;

		DB.query(query,[arrParams.company_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				HelperRestriction.convertMultiMetaArray(data, function(retConverted){	
				
					var array_rows = [];

					var counter = retConverted.length;

					//return_result(retConverted);
					
					if(parseInt(counter) !== 0)
					{
						promiseForeach.each(retConverted,[function (list) {

							return new Promise(function (resolve, reject) {

								var accParams = {account_id:list.team_account_id};

								methods.getCompanyProfile(accParams, function(retProfile){

									methods.getTeamDeparment(retProfile.team_department_id, function(retDepartment){

										var dep_name = "";
										if(retDepartment.department_name)
										{
											dep_name = retDepartment.department_name;
										}

										var arrObj = {
											id 				: list.group_id.toString(),
											name 			: retProfile.name,
											logo       		: retProfile.logo,
											title       	: retProfile.team_position,
											department      : dep_name,
											date            : list.create_date,
								
										}
										
										array_rows.push(arrObj);
										
										counter -= 1;
										if (counter === 0)
										{
											resolve(array_rows);
										}
									
									});
								});
							});
						}],
						function (result, current) {										
							if (counter === 0)
							{ 
								return_result(fast_sort(result[0]).asc('name')); 
							}
						});	
					}
					else
					{ return_result(null); }

				});

			}
			else
			{ return_result(null); }
		});

	}
	catch(err)
	{
		console.log('getPendingManageTeam',err);
		return_result(null);
	}

};


methods.getTeamDeparment = function(group_id,return_result)
{
	try
	{
		
		var query  =`SELECT * FROM jaze_company_team_departments WHERE  
		group_id=? AND group_id IN (SELECT group_id FROM jaze_company_team_departments WHERE meta_key = 'status' AND meta_value ='1' )`;

		DB.query(query,[group_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				HelperRestriction.convertMultiMetaArray(data, function(retConverted){

					return_result(retConverted[0]);

				});

			}
			else
			{ return_result(""); }
		});

	}
	catch(err)
	{
		console.log('getTeamDeparment',err);
		return_result("");
	}

};

// ------------------------------------------------------ Company Team End  ----------------------------------------------------------------------------------//  

// ------------------------------------------------------ Company Team Permission Start  ----------------------------------------------------------------------------------//  

methods.getTeamPermission = function(arrParams,return_result)
{
	try
	{
		var array_rows = [];
		var arrObj     = {};

		var query  = `SELECT * FROM jaze_user_permissions_access WHERE account_id =?`;

		DB.query(query,[arrParams.company_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				var counter = data.length;
				promiseForeach.each(data,[function (list) {

					return new Promise(function (resolve, reject) {

						methods.__getCompanyDetails(list.member_id, function(retDetails){

							var count = list.permission_ids.split(",");
			
							var access = "Partial";
							if(parseInt(count.length) >= 48 )
							{
								access = "Full access";
							}
							
							arrObj = {
								id 				: list.access_id.toString(),
								team 			: retDetails.name,
								logo       		: retDetails.logo,
								department      : retDetails.department,
								access          : access,
								date            : dateFormat(list.created_date,'yyyy-mm-dd HH:MM:ss'),
					
							}
							
							array_rows.push(arrObj);
							
							counter -= 1;
							if (counter === 0)
							{
								resolve(array_rows);
							}
					
						});

					});
				}],
				function (result, current) {										
					if (counter === 0)
					{ 
						return_result(result[0]); 
					}
				});	
		
			}
			else
			{ return_result(null); }
		});

	}
	catch(err)
	{
		console.log('getManageTeamList',err);
		return_result(null);
	}

};

methods.__getCompanyDetails = function(member_id,return_result)
{
	try
	{
		var obj_return = {};

		var query  = `SELECT * FROM jaze_company_team_details WHERE group_id =?`;

		DB.query(query,[member_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data,function(retConverted){

					var accParams = {account_id:retConverted[0].team_account_id};
					methods.getCompanyProfile(accParams, function(retProfile){	

						if(retProfile!=null)
						{
							methods.getTeamDeparment(retProfile.team_department_id, function(retDepartment){

								obj_return = {
									team       : retProfile.name,
									logo       : retProfile.logo,
									department : retDepartment.department_name
								};

								return_result(obj_return);

							});
						}
						else
						{
							return_result(null);
						}

					});	

				});	

		
			}
			else
			{ return_result(null); }
		});

	}
	catch(err)
	{
		console.log('__getCompanyDetails',err);
		return_result(null);
	}

};

// ------------------------------------------------------ Company Team Permission End   ----------------------------------------------------------------------------------//  

//------------------------------------------------------------------- Location Module Start ---------------------------------------------------------------------------//

methods.__getLocationDetails = function(area_id,city_id,return_result){

	try{
			var obj_return = {
				country_id : "0",
				country_name : "",
				state_id : "0",
				state_name : "",
				city_id : "0",
				city_name : "",
				area_id : "0",
				area_name : "",
			};

			if(parseInt(area_id) !== 0)
			{
				var query  =" SELECT  con.id as country_id,con.country_name ,st.id as state_id,st.state_name,city.id as city_id,city.city_name,ar.area_id,ar.area_name ";
					query +=" FROM area ar  ";
					query +=" INNER JOIN cities city ON city.id = ar.city_id ";
                    query +=" INNER JOIN states st ON st.id = city.state_id ";
                    query +=" INNER JOIN country con ON con.id=st.country_id ";
					query +=" WHERE ar.status = '1' and city.status = '1' and st.status = '1' and con.status = '1' and ar.area_id = '"+area_id+"' " ;
 
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0]);
					}
					else
					{return_result(obj_return)}
				});
			}
			else if(parseInt(city_id) !== 0)
			{
				var query  =" SELECT  con.id as country_id,con.country_name ,st.id as state_id,st.state_name,city.id as city_id,city.city_name, '0' as area_id, '' as area_name ";
					query +=" FROM cities city  ";
                    query +=" INNER JOIN states st ON st.id = city.state_id ";
                    query +=" INNER JOIN country con ON con.id=st.country_id ";
					query +=" WHERE city.status = '1' and st.status = '1' and con.status = '1' and city.id = '"+city_id+"' " ;
 
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0]);
					}
					else
					{return_result(obj_return)}
				});
			}
			else
			{ return_result(obj_return); }

			
	}
	catch(err)
	{
		return_result(obj_return)
	}
};


methods.getApplyStatus = function(arrParams,return_result){

	try{

		if(parseInt(arrParams.account_id) !== 0){

			var qry = `SELECT * FROM jaze_job_post_job_candidates WHERE 
				group_id IN (SELECT group_id FROM jaze_job_post_job_candidates WHERE meta_key = 'account_id' AND meta_value =?) 
				AND group_id IN (SELECT group_id FROM jaze_job_post_job_candidates WHERE meta_key = 'job_id' AND meta_value =?)`;

			DB.query(qry, [arrParams.account_id,arrParams.group_id], function(data, error){

	 	  		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result('1');
		 	   	}else{
					return_result('0');
		 	   	}

			});

		}else{
			return_result('0');
		}

	}
	catch(err)
	{ console.log("getApplyStatus",err); return_result('0');}

};

methods.__getCountryDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT country_name FROM country  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};


methods.__getStateDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT state_name FROM states  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].state_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getCityDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT city_name FROM cities  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].city_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getAreaDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT area_name FROM area  WHERE area_id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].area_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};



methods.__getNationality = function(arrParams,return_result){

	try{

		if(parseInt(arrParams) !==0){

			let query  = " SELECT country_name FROM country  WHERE id =? ";
			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
			
		}else{
			return_result('');
		}
	
	}
	catch(err)
	{
		return_result('')
	}
};


methods.__getMobileCode= function (phone_code,return_result){
	try
	{
		if(parseInt(phone_code) !== 0)
		{
			var query = " SELECT `phonecode` FROM `country` WHERE `id` = '"+phone_code+"'; ";
			DB.query(query, null, function(data, error){
				
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result("+"+data[0].phonecode);
				}
				else
				{ return_result('0'); }
			});

			
		}
		else
		{
			return_result('0');
		}
	}
	catch(err)
	{ console.log("getMobileCode",err); return_result('0'); }	
};

methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
};

methods.updateQuery = function(querY,arrUpdate,group_id,return_result){

	for (var key in arrUpdate) 
 	{	
 		DB.query(querY, [arrUpdate[key],key,group_id], function(data, error){
 			if(data == null){
	 	   		return_result(0);
	 	   	}else{
	  			return_result(data.affectedRows);
	 	   	}
		});
  	};
}

//------------------------------------------------------------------- Location Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- General Module Start ---------------------------------------------------------------------------//


methods.__getMobileCode= function (phone_code,return_result){
	try
	{
		if(parseInt(phone_code) !== 0)
		{
			var query = " SELECT `phonecode` FROM `country` WHERE `id` = '"+phone_code+"'; ";
			DB.query(query, null, function(data, error){
				
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result("+"+data[0].phonecode);
				}
				else
				{ return_result('0'); }
			});

			
		}
		else
		{
			return_result('0');
		}
	}
	catch(err)
	{ console.log("getMobileCode",err); return_result('0'); }	
};


var sortBy = (function () {
	var toString = Object.prototype.toString,
		// default parser function
		parse = function (x) { return x; },
		// gets the item to be sorted
		getItem = function (x) {
		  var isObject = x != null && typeof x === "object";
		  var isProp = isObject && this.prop in x;
		  return this.parser(isProp ? x[this.prop] : x);
		};
		
	/**
	 * Sorts an array of elements.
	 *
	 * @param {Array} array: the collection to sort
	 * @param {Object} cfg: the configuration options
	 * @property {String}   cfg.prop: property name (if it is an Array of objects)
	 * @property {Boolean}  cfg.desc: determines whether the sort is descending
	 * @property {Function} cfg.parser: function to parse the items to expected type
	 * @return {Array}
	 */
	return function sortby (array, cfg) {
	  if (!(array instanceof Array && array.length)) return [];
	  if (toString.call(cfg) !== "[object Object]") cfg = {};
	  if (typeof cfg.parser !== "function") cfg.parser = parse;
	  cfg.desc = !!cfg.desc ? -1 : 1;
	  return array.sort(function (a, b) {
		a = getItem.call(cfg, a);
		b = getItem.call(cfg, b);
		return cfg.desc * (a < b ? -1 : +(a > b));
	  });
	};
	
  }());


  	/*
	 * split values push to array
	 */
	function convert_array(array_val) {

		var list = [];

		if(typeof array_val !== 'undefined' && array_val !== null && array_val !== "")
		{
			if(array_val.toString().indexOf(',') != -1 ){
				list = array_val.split(',');
			}
			else {
				list.push(array_val);
			}
		}
		
		return list;
	}


methods.convertMultiMetaArrayCompany =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndexCompany(list,arrvalues[i].account_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndexCompany(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].account_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}

methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].group_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}

//------------------------------------------------------------------- General Module End ---------------------------------------------------------------------------//

function getExtension(path) {
    var basename = path.split(/[\\/]/).pop(),  // extract file name from full path ...
                                             	 // (supports `\\` and `/` separators)
        pos = basename.lastIndexOf(".");       // get last position of `.`
    if (basename === "" || pos < 1)            // if file name is empty or ...
        return "";                             //  `.` not found (-1) or comes first (0)
    return basename.slice(pos + 1);            // extract extension ignoring `.`
}

function getFilename(path) {
    var basename = path.split(/[\\/]/).pop(),      
        pos = basename.split('.'); 
    if (basename === "" || pos.length < 1)           
        return "";                          
    return pos[0];            
}


function getCleanFileName(path) {

	var basename = path.split(/\.(?=[^\.]+$)/);
	var cleanString = basename[0].replace(/[|&;$%@"<>()+1234567890,]/g, "");
         
    return cleanString;           
}


module.exports = methods;