const methods = {};
const DB = require('../../helpers/db_base.js');
const HelperRestriction = require('../../helpers/HelperRestriction.js');
const HelperGeneral = require('../../helpers/HelperGeneral');
const HelperJazefeed = require('../../jazecom/helpers/HelperJazefeed.js');
const HelperJazeEMail = require('../../jazecom/helpers/HelperJazeEMail');
const HelperJazepay = require('../helpers/HelperJazepay.js');

const fast_sort = require("fast-sort");
const promiseForeach = require('promise-foreach');
const dateFormat = require('dateformat');
const fs = require('fs');

//-------------------------------------------------------------------  Home Notification List Module Start ---------------------------------------------------------------------------//


methods.getHomeNotificationList = function(arrParams,return_result){

	var return_obj = {
		notification_count : {
			diary_unread_count   : '0',
			mail_unread_count    : '0',
			feed_unread_count    : '0',
			call_unread_count    : '0',
			jobs_unread_count    : '0',
			notice_unread_count  : '0',
			pay_unread_count  	 : '0',
			diary_total_count    : '0',
			mail_total_count     : '0',
			feed_total_count     : '0',
			call_total_count     : '0',
			jobs_total_count     : '0',
			notice_total_count   : '0',
			pay_total_count  	 : '0',
		},
		notification_list : []
	};

	try
	{	
		if(arrParams !== null)
		{
			if(arrParams.date ==='')
			{ arrParams.date = dateFormat(new Date(),'yyyy-mm-dd'); }

			var query  = " SELECT * FROM jaze_notification_list WHERE  ";
				query += " group_id IN (SELECT group_id FROM  jaze_notification_list  WHERE meta_key = 'status' AND meta_value = '1')  ";
				query += " AND group_id IN (SELECT group_id FROM  jaze_notification_list  WHERE meta_key = 'receiver_account_id' AND meta_value = '"+arrParams.account_id+"')  ";
				query += " AND group_id IN (SELECT group_id FROM  jaze_notification_list  WHERE meta_key = 'notify_type' AND meta_value not in (101,102,103,104,110,111,113,117,118))  ";
				query += " AND group_id IN (SELECT group_id FROM  jaze_notification_list  WHERE meta_key = 'create_date' AND DATE(meta_value) = '"+arrParams.date+"') ";

				// if(parseInt(arrParams.status) == 0 )
				// {
				// 	query += ` AND group_id IN (SELECT group_id FROM  jaze_notification_list  WHERE meta_key = 'read_status' AND meta_value ='0' ) `;
				// }

				DB.query(query,null, function(data, error){

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						HelperRestriction.convertMultiMetaArray(data, function(data_result){	

							var counter = data_result.length;

							var arrRows =[];

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data_result,[function (list) {
									
									return new Promise(function (resolve, reject) {
					
										HelperGeneral.getAccountBasicDetails(list.account_id, function(sender_details){

											var flag = __getNotificationListFlag(list.notify_type);

											if(parseInt(flag) !== 0)
											{
												//Mail
												methods.__getMailMessageResult(flag,list,sender_details, function(mail_details){

												if(mail_details !== null)
												{ 
													arrRows.push(mail_details);  
													
													return_obj.notification_count.mail_total_count = (parseInt(return_obj.notification_count.mail_total_count) + 1).toString();

													if(parseInt(mail_details.read_status) === 0)
													{
														return_obj.notification_count.mail_unread_count = (parseInt(return_obj.notification_count.mail_unread_count) + 1).toString();
													}
												}									

													//Feed
													methods.__getFeedMessageResult(flag,list,sender_details, function(feed_details){	

														if(feed_details !== null)
														{ 
															arrRows.push(feed_details); 

															return_obj.notification_count.feed_total_count = (parseInt(return_obj.notification_count.feed_total_count) + 1).toString();

															if(parseInt(feed_details.read_status) === 0)
															{
																return_obj.notification_count.feed_unread_count = (parseInt(return_obj.notification_count.feed_unread_count) + 1).toString();
															}
														}	

														//Diary
														methods.__getDiaryMessageResult(flag,list,sender_details, function(diary_details){

															if(diary_details !== null)
															{ 
																arrRows.push(diary_details); 

																return_obj.notification_count.diary_total_count = (parseInt(return_obj.notification_count.diary_total_count) + 1).toString();

																if(parseInt(diary_details.read_status) === 0)
																{
																	return_obj.notification_count.diary_unread_count = (parseInt(return_obj.notification_count.diary_unread_count) + 1).toString();
																}
															}	

															//Jobs
															methods.__getJobsMessageResult(flag,list,sender_details, function(job_details){

																if(job_details !== null)
																{ 
																	arrRows.push(job_details); 

																	return_obj.notification_count.jobs_total_count = (parseInt(return_obj.notification_count.jobs_total_count) + 1).toString();

																	if(parseInt(job_details.read_status) === 0)
																	{
																		return_obj.notification_count.jobs_unread_count = (parseInt(return_obj.notification_count.jobs_unread_count) + 1).toString();
																	}
																}

																//Notice
																methods.__getNoticeMessageResult(flag,list,sender_details, function(notice_details){

																	if(notice_details !== null)
																	{  
																		arrRows.push(notice_details); 

																		return_obj.notification_count.notice_total_count = (parseInt(return_obj.notification_count.notice_total_count) + 1).toString();

																		if(parseInt(notice_details.read_status) === 0)
																		{
																			return_obj.notification_count.notice_unread_count = (parseInt(return_obj.notification_count.notice_unread_count) + 1).toString();
																		}
																	}

																	//Pay
																	methods.__getPayMessageResult(flag,list,sender_details, function(pay_details){

																		if(pay_details !== null)
																		{ 
																			arrRows.push(pay_details); 

																			return_obj.notification_count.pay_total_count = (parseInt(return_obj.notification_count.pay_total_count) + 1).toString();

																			if(parseInt(pay_details.read_status) === 0)
																			{
																				return_obj.notification_count.pay_unread_count = (parseInt(return_obj.notification_count.pay_unread_count) + 1).toString();
																			}
																		}
																																		
																		counter -= 1;
																		if (counter === 0)
																		{  resolve(arrRows); }
																																
																	});
																});
															});
														});
													});
												});
											}
											else 
											{ counter -= 1; if (counter === 0) {  resolve(arrRows); } }
										});
									});
								}],
								function (result, current) {
									if (counter === 0)
									{ 
										return_obj.notification_list = fast_sort(result[0]).asc('sort');

										return_result(return_obj); 
									} 
								});
							}
							else
							{ return_result(return_obj); }
						});
					}
					else
					{ return_result(return_obj); }
				});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{ 
		console.log("getNotificationList",err); 
		return_result(return_obj); 
	}	
};


/*
 * Mail
 */
methods.__getMailMessageResult = function(flag,reqParams,sender_details,return_result){

	try
	{
		if(parseInt(flag) === 3 && reqParams !== null && sender_details !== null)
		{
			var newObj = {
					id      		 	: reqParams.notify_id.toString(),
					group_id          	: reqParams.group_id.toString(),
					sender_account_id 	: sender_details.account_id.toString(),
					sender_account_type : sender_details.account_type.toString(),
					sender_name       	: sender_details.name,
					sender_logo       	: sender_details.logo,
					sender_title      	: sender_details.title,
					notify_text       	: reqParams.notify_text,
					notify_category   	: reqParams.notify_category,
					date              	: reqParams.create_date,
					read_status       	: reqParams.read_status.toString(),
					flag              	: '3',	
					sort              	: '1',	
					body_details      	: {}				  
				};

				var args = {
					account_id		:	reqParams.reqParams,
					mail_group_id	:	reqParams.notify_id,
					flag			:	1,
					history			:	0,
					read_status		: 	""														
				}
		
				//get mail details
				HelperJazeEMail.__getMailDetailsByGroupId(args, function (mail_details) 
				{
					if(mail_details !== null)
					{ Object.assign(newObj, {body_details:  mail_details}); }

					return_result(newObj);
				});
		  }
		  else
		  { return_result(null); }
	}
	catch(error)
	{
		console.log('__getMailMessageResult',error);
		return_result(null);
	}
};

/*
 * Feed
 */
methods.__getFeedMessageResult = function(flag,reqParams,sender_details,return_result){

	try{

		if(parseInt(flag) === 4 && reqParams !== null && sender_details !== null)
		{
			var query  = " SELECT * FROM jaze_jazefeed_groups_meta WHERE   ";
				query += " group_id IN (SELECT group_id FROM jaze_jazefeed_groups_meta WHERE meta_key = 'status'  AND meta_value ='1' ) AND ";
				query += " group_id IN (SELECT group_id FROM jaze_jazefeed_groups_meta WHERE meta_key = 'master_id'  AND  meta_value ='"+reqParams.notify_id+"') ";
			
		
				DB.query(query,null, function(data, error){

					console.log(query);

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						HelperRestriction.convertMetaArray(data, function(data_result)
						{ 
							//check fedd avtive and block status
							var status_arrParams = { account_id : reqParams.receiver_account_id, company_account_id: reqParams.account_id};

							HelperJazefeed.checkAccountJazecomFeedStatus(status_arrParams, function(feedStatus){

								var attachment_list = [];

								if(typeof data_result.attachment_url !== 'undefined' && data_result.attachment_url != "" && data_result.attachment_url != null)
								{ 	
									var attachArr = data_result.attachment_url.split(",");
																	
									var feed_path = G_STATIC_IMG_PATH+"jazefeed/"

									var feed_url = G_STATIC_IMG_URL+"uploads/jazenet/jazefeed/";

										if(attachArr.length !== 0)
										{
											attachArr.forEach(function(row) {

												var file_name = row.split("/")[1];

												var attachment_type =  __getFileTypes('.'+file_name.substring(file_name.indexOf(".") + 1));

												var path = feed_path+row;

												var f_size = "0";

												if (fs.existsSync(path)) {
													f_size = __formatSizeUnits(fs.statSync(path).size).toString();
												  } 

												var objFiles = {
													attachment_name: file_name,
													attachment_url: feed_url+row,
													attachment_size: f_size,
													attachment_type: attachment_type.toString()
												}

												attachment_list.push(objFiles);
											});			
										}
								}

								var newObj = {
									id      		 	: reqParams.notify_id.toString(),
									group_id          	: reqParams.group_id.toString(),
									sender_account_id 	: sender_details.account_id.toString(),
									sender_account_type : sender_details.account_type.toString(),
									sender_name       	: sender_details.name,
									sender_logo       	: sender_details.logo,
									sender_title      	: sender_details.title,
									notify_text       	: reqParams.notify_text,
									notify_category   	: reqParams.notify_category,
									date              	: reqParams.create_date,
									read_status       	: reqParams.read_status.toString(),
									flag              	: '4',	
									sort              	: '3',	
									body_details      	: {
										id 					: data_result.group_id.toString(),
										description 		: data_result.message,
										date				: data_result.create_date,
										read_status	  		: data_result.read_status.toString(),
										replay_status 		: feedStatus.toString(),
										attachment_list 	: attachment_list
									}				  
								};	

								return_result(newObj); 
							});
						});
					}
					else
					{ return_result(null); }
				});
		  }
		  else
		  {return_result(null);}
	}
	catch(error)
	{
		console.log('__getFeedMessageResult',error);
		return_result(null);
	}

};

/*
 * Diary
 */
methods.__getDiaryMessageResult = function(flag,reqParams,sender_details,return_result){

	try{

		if(parseInt(flag) === 2 && reqParams !== null && sender_details !== null)
		{
			var query  = " SELECT * FROM jaze_dairy_appointment_info WHERE   ";
				query += " group_id IN (SELECT group_id FROM jaze_dairy_appointment_info WHERE meta_key = 'status'  AND meta_value ='1' and group_id ='"+reqParams.notify_id+"') ";
		
				
				DB.query(query,null, function(data, error){

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						HelperRestriction.convertMetaArray(data, function(data_result)
						{ 

							var newObj = {
								id      		 	: reqParams.notify_id.toString(),
								group_id          	: reqParams.group_id.toString(),
								sender_account_id 	: sender_details.account_id.toString(),
								sender_account_type : sender_details.account_type.toString(),
								sender_name       	: sender_details.name,
								sender_logo       	: sender_details.logo,
								sender_title      	: sender_details.title,
								notify_text       	: reqParams.notify_text,
								notify_category   	: reqParams.notify_category,
								date              	: reqParams.create_date,
								read_status       	: reqParams.read_status.toString(),
								flag              	: '2',	
								sort              	: '6',	
								body_details      	: {
									id 					: data_result.group_id.toString(),
									subject				: data_result.subject,
									activity			: data_result.activity,
									venue 				: data_result.venues,
									no_pepole			: data_result.no_people,
									remarks	  			: data_result.remarks,
									repetition_type 	: data_result.repetition_type, 
									reminder_mins 		: data_result.reminder_mins, 
									appointment_date 	: data_result.appointment_date, 
									start_time			: data_result.start_time,
									end_time			: data_result.end_time,
									appointment_status 	: data_result.appointment_status.toString(),
									flag 				: data_result.appointment_type.toString()
								}				  
							};	

							return_result(newObj); 
						});
					}
					else
					{ return_result(null); }
				});
		  }
		  else
		  {return_result(null);}
	}
	catch(error)
	{
		console.log('__getDiaryMessageResult',error);
		return_result(null);
	}
};

/*
 * Jobs
 */
methods.__getJobsMessageResult = function(flag,reqParams,sender_details,return_result){

	try{

		if(parseInt(flag) === 5 && reqParams !== null && sender_details !== null)
		{
			var applay_status = 1;
			var message = "";

			if(parseInt(reqParams.notify_type) === 120)
			{ 
				applay_status = 0; 
				message = "we have viewed your basic profile and are interested in obtaining further information regarding your profile. accordingly, kindly finish us with your full cv.";
			}
			else if(parseInt(reqParams.notify_type) === 121)
			{ message = "we have viewed your basic profile and are interested in obtaining further information regarding your profile. accordingly, kindly finish us with your full cv."; }
			else if(parseInt(reqParams.notify_type) === 122)
			{ message = "we have viewed your basic profile and are interested in obtaining further information regarding your profile. accordingly, kindly finish us with your full cv."; }
			

			var newObj = {
				id      		 	: reqParams.notify_id.toString(),
				group_id          	: reqParams.group_id.toString(),
				sender_account_id 	: sender_details.account_id.toString(),
				sender_account_type : sender_details.account_type.toString(),
				sender_name       	: sender_details.name,
				sender_logo       	: sender_details.logo,
				sender_title      	: sender_details.title,
				notify_text       	: reqParams.notify_text,
				notify_category   	: reqParams.notify_category,
				date              	: reqParams.create_date,
				read_status       	: reqParams.read_status.toString(),
				flag              	: '5',	
				sort              	: '7',	
				body_details      	: {
					id 					: reqParams.notify_id.toString(),
					applay_status		: applay_status.toString(),
					message				: message
				}				  
			};	

			return_result(newObj); 
		  }
		  else
		  {return_result(null);}
	}
	catch(error)
	{
		console.log('__getJobsMessageResult',error);
		return_result(null);
	}
};

/*
 * Notice
 */
methods.__getNoticeMessageResult = function(flag,reqParams,sender_details,return_result){

	try
	{
		if(parseInt(flag) === 6 && reqParams !== null && sender_details !== null)
		{
			var newObj = {
				id      		 	: reqParams.notify_id.toString(),
				group_id          	: reqParams.group_id.toString(),
				sender_account_id 	: sender_details.account_id.toString(),
				sender_account_type : sender_details.account_type.toString(),
				sender_name       	: sender_details.name,
				sender_logo       	: sender_details.logo,
				sender_title      	: sender_details.title,
				notify_text       	: reqParams.notify_text,
				notify_category   	: reqParams.notify_category,
				date              	: reqParams.create_date,
				read_status       	: reqParams.read_status.toString(),
				sort              	: '5',
				flag				: '0',
				body_details		: {}
			};	

			if(parseInt(reqParams.notify_type) === 119)
			{
				//notice
				Object.assign(newObj, {flag:  '6'});
				
				var query  = " SELECT meta_value FROM jaze_follower_notice WHERE  meta_key = 'notice_id'  ";
					query += " AND group_id IN (SELECT group_id FROM jaze_follower_notice WHERE meta_key = 'status'  AND meta_value ='1' and group_id ='"+reqParams.notify_id+"') ";
	
				DB.query(query,null, function(data, error){
					
					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						query  = " SELECT * FROM jaze_company_notice WHERE   ";
						query += " group_id IN (SELECT group_id FROM jaze_company_notice WHERE meta_key = 'status'  AND meta_value ='1' and group_id ='"+data[0].meta_value+"') ";
			
						DB.query(query,null, function(data, error){

							if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
							{
								HelperRestriction.convertMetaArray(data, function(data_result)
								{ 	
									var body_details = {	
										id 				: data_result.group_id.toString(),
										title 			: data_result.title,
										logo			: G_STATIC_IMG_URL+"uploads/jazenet/company/"+data_result.account_id+'/jazenotice/'+data_result.image_url,
										description		: data_result.description
									};
	
									Object.assign(newObj, {body_details:  body_details});
								
									return_result(newObj);
								});
							}
							else
							{return_result(null);}
						});
					}
					else
					{return_result(null);}
				});
			}
			else if(parseInt(reqParams.notify_type) === 124)
			{
				//follow
				Object.assign(newObj, {flag:  '7'});

				//get follower groups 
				var query  = " SELECT * FROM jaze_follower_groups WHERE  id ='"+reqParams.notify_id+"' ";
					
				DB.query(query,null, function(data, error){

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						var body_details = {	
								id 				: data[0].id.toString(),
								group_name 		: data[0].group_name
							};

						Object.assign(newObj, {body_details:  body_details});

						return_result(newObj);
					}
					else
					{return_result(null);}
				});
			}
			else if(parseInt(reqParams.notify_type) === 114)
			{
				//team invitation
				Object.assign(newObj, {flag:  '8'});

				//get team details 
				var query  = " SELECT * FROM jaze_company_team_details WHERE   ";
					query += " group_id IN (SELECT group_id FROM jaze_company_team_details WHERE meta_key = 'status'  AND meta_value ='0' and group_id ='"+reqParams.notify_id+"') ";
		
				DB.query(query,null, function(data, error){

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						HelperRestriction.convertMetaArray(data, function(data_result)
						{ 
							methods.___getTeamDepartmentName(data_result.team_department_id,function(department_name)
							{ 
								var body_details = {	
										id 				: data_result.group_id.toString(),
										company_id 		: data_result.account_id.toString(),
										name			: data_result.first_name+' '+data_result.last_name,
										position		: data_result.team_position,
										logo			: G_STATIC_IMG_URL+"uploads/jazenet/"+data_result.team_logo_url,
										department_name	: department_name,
										diary_status	: data_result.jdiary_status.toString(),
										chat_status		: data_result.jchat_status.toString(),
										call_status		: data_result.jcall_status.toString(),
										mail_status		: data_result.jmail_status.toString(),
										applay_status 	: '0',
										message			: ''
									};

								Object.assign(newObj, {body_details:  body_details});
								
								return_result(newObj);
							});
						});
					} 
					else
					{return_result(null);}
				});
			}
			else if(parseInt(reqParams.notify_type) === 115 || parseInt(reqParams.notify_type) === 116)
			{
				//team invitation approved || rejected
				Object.assign(newObj, {flag:  '8'});

				var message = "";

				if(parseInt(reqParams.notify_type) === 115)
				{ message = 'team invitation approved' }
				else if(parseInt(reqParams.notify_type) === 116)
				{ message = 'team invitation rejected' }

				var body_details = {	
						id 				: reqParams.notify_id.toString(),
						applay_status 	: '1',
						message			: message
				}

				Object.assign(newObj, {body_details:  body_details});

				return_result(newObj);
			}
			else
		  	{ return_result(null); }
		}
		else
		{ return_result(null); }
	}
	catch(error)
	{
		console.log('__getNoticeMessageResult',error);
		return_result(null);
	}
};

/*
 * Team Department Name
 */
methods.___getTeamDepartmentName  = function(department_id,return_result){

	try
	{
		if(parseInt(department_id) !== 0)
		{
			var query  = " SELECT meta_value FROM jaze_company_team_departments WHERE  meta_key = 'department_name' ";
				query += " and group_id IN (SELECT group_id FROM jaze_company_team_departments WHERE meta_key = 'status'  AND meta_value ='1' and group_id ='"+department_id+"') ";
		
				DB.query(query,null, function(data, error){

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						return_result(data[0].meta_value);
					}
					else
					{ return_result(''); }
				});
		}
		else
		{return_result('');}
	}
	catch(error)
	{
		console.log('___getTeamDepartmentName',error);
		return_result('');
	}
};

//-------------------------------------------------------------------  Home Notification List Module End ---------------------------------------------------------------------------//

//-------------------------------------------------------------------  Jazepay Details Module Start ---------------------------------------------------------------------------//

methods.__getPayMessageResult = function(flag,reqParams,sender_details,return_result){

	try
	{
		if(parseInt(flag) === 9 && reqParams !== null && sender_details !== null)
		{
			var newObj = {
				id      		 	: reqParams.notify_id.toString(),
				group_id          	: reqParams.group_id.toString(),
				sender_account_id 	: sender_details.account_id.toString(),
				sender_account_type : sender_details.account_type.toString(),
				sender_name       	: sender_details.name,
				sender_logo       	: sender_details.logo,
				sender_title      	: sender_details.title,
				notify_text       	: reqParams.notify_text,
				notify_category   	: reqParams.notify_category,
				date              	: reqParams.create_date,
				read_status       	: reqParams.read_status.toString(),
				sort              	: '4',
				flag				: '9',
				body_details		: {}
			};	

			var arrParams = {};

			if(parseInt(reqParams.notify_type) === 150)
			{
				//intra fund received
				arrParams = {account_id:sender_details.account_id, id:reqParams.notify_id };
				HelperJazepay.getIntraTranferDetailsForNotification(arrParams, function(retDetails){

					if(retDetails!=null){
						newObj.body_details = retDetails;
					}

					return_result(newObj);
				});
			}
			else if(parseInt(reqParams.notify_type) === 151)
			{
				//quotations request received
				arrParams = { id:reqParams.notify_id }
				HelperJazepay.getQuotationRequestDetails(arrParams, function(retDetails){

					if(retDetails!=null){
						const removeProp = 'sender_details';
						const { [removeProp]: remove, ...rest } = retDetails;

						newObj.body_details = rest;
					}

					return_result(newObj);

				});
			}
			else if(parseInt(reqParams.notify_type) === 152)
			{
				//quotations request declined
				arrParams = { id:reqParams.notify_id }
				HelperJazepay.getQuotationRequestDetails(arrParams, function(retDetails){

					if(retDetails!=null){
						const removeProp = 'sender_details';
						const { [removeProp]: remove, ...rest } = retDetails;

						newObj.body_details = rest;
					}
					return_result(newObj);

				});

			}
			else if(parseInt(reqParams.notify_type) === 153)
			{
				//quotations received
				arrParams = { id:reqParams.notify_id }
				HelperJazepay.getQuotationReceiveDetails(arrParams, function(retDetails){

					if(retDetails!=null){
						const removeProp = 'sender_details';
						const { [removeProp]: remove, ...rest } = retDetails;

						newObj.body_details = rest;
					}
					return_result(newObj);

				});
			}
			else if(parseInt(reqParams.notify_type) === 154)
			{
				//quotations declined 
				arrParams = { id:reqParams.notify_id }
				HelperJazepay.getQuotationReceiveDetails(arrParams, function(retDetails){

					if(retDetails!=null){
						const removeProp = 'sender_details';
						const { [removeProp]: remove, ...rest } = retDetails;

						newObj.body_details = rest;
					}
					return_result(newObj);

				});
			}
			else if(parseInt(reqParams.notify_type) === 155)
			{
				//order received 
				arrParams = { id:reqParams.notify_id }
				HelperJazepay.getReceivableOrderReceivedDetails(arrParams, function(retDetails){

					if(retDetails!=null){
						const removeProp = 'sender_details';
						const { [removeProp]: remove, ...rest } = retDetails;

						newObj.body_details = rest;
					}
					return_result(newObj);

				});

			}
			else if(parseInt(reqParams.notify_type) === 156)
			{
				//order declined
				arrParams = { id:reqParams.notify_id }
				HelperJazepay.getReceivableOrderReceivedDetails(arrParams, function(retDetails){

					if(retDetails!=null){
						const removeProp = 'sender_details';
						const { [removeProp]: remove, ...rest } = retDetails;

						newObj.body_details = rest;
					}
					return_result(newObj);
					
				});
			}
			else if(parseInt(reqParams.notify_type) === 157)
			{
				//invoice received
				arrParams = { id:reqParams.notify_id }
				HelperJazepay.getReceivableInvoiceDetails(arrParams, function(retDetails){

					if(retDetails!=null){
						const removeProp = 'sender_details';
						const { [removeProp]: remove, ...rest } = retDetails;

						newObj.body_details = rest;
					}
					return_result(newObj);
					
				});
			}
			else if(parseInt(reqParams.notify_type) === 158)
			{
				//invoice declined
				arrParams = { id:reqParams.notify_id }
				HelperJazepay.getReceivableInvoiceDetails(arrParams, function(retDetails){

					if(retDetails!=null){
						const removeProp = 'sender_details';
						const { [removeProp]: remove, ...rest } = retDetails;

						newObj.body_details = rest;
					}
					return_result(newObj);
					
				});
			}
			else if(parseInt(reqParams.notify_type) === 159)
			{
				//payment received
				arrParams = { id:reqParams.notify_id }
				HelperJazepay.getReceivablePaymentsReceivedDetails(arrParams, function(retDetails){

					if(retDetails!=null){
						const removeProp = 'sender_details';
						const { [removeProp]: remove, ...rest } = retDetails;

						newObj.body_details = rest;
					}
					return_result(newObj);
					
				});
			}
			else if(parseInt(reqParams.notify_type) === 160)
			{
				//receipt received
				
			}
			else
			{return_result(null);}
		}
		else
		{return_result(null);}
	}
	catch(error)
	{
		console.log('__getPayMessageResult',error);
		return_result(null);
	}
};

//-------------------------------------------------------------------  Jazepay Details Module End ---------------------------------------------------------------------------//

//-------------------------------------------------------------------  Update Status notification Module Start ---------------------------------------------------------------------------//

methods.updateReadStatus = function(arrParams,return_result)
{ 
	try
	{	
		if(arrParams !== null)
		{
			var query  = " SELECT id FROM jaze_notification_list WHERE  meta_key = 'read_status' ";
				query += " AND group_id IN (SELECT group_id FROM  jaze_notification_list  WHERE meta_key = 'status' AND meta_value = '1')  ";
				query += " AND group_id IN (SELECT group_id FROM  jaze_notification_list  WHERE meta_key = 'read_status' AND meta_value = '0')  ";
				query += " AND group_id IN (SELECT group_id FROM  jaze_notification_list  WHERE meta_key = 'notify_id' AND meta_value = '"+arrParams.id+"' ";
				query += " AND group_id = '"+arrParams.group_id+"') ";

			DB.query(query, null, function(data, error){

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)	
				{
					if (typeof data[0].id !== 'undefined' && data[0].id !== null && parseInt(data[0].id) !== 0)	
					{
						query =  " UPDATE jaze_notification_list SET meta_value ='1' WHERE meta_key = 'read_status' and id = '"+data[0].id+"' ";

						DB.query(query, null, function(data, error){
							return_result(data); 
						});
					}
					else 
					{ return_result(null);  }
				}
				else
				{ return_result(null);  }
			});
		}
		else
		{ return_result(null);  }
	}
	catch(err)
	{ 
		console.log("updateReadStatus",err); 
		return_result(null); 
	}	
}

//-------------------------------------------------------------------  Update Status notification Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- General Module Start ---------------------------------------------------------------------------//

function __getNotificationListFlag(notify_type) {

	var return_status = '0';

	if (notify_type !== "") 
	{
		var mail_array = ['112'];

		var call_array = ['0'];

		var feed_array = ['105','106'];

		var pay_array = ['150','151','152','153','154','155','156','157','158','159'];

		var notice_array = ['114','115','116','119','124'];

		var diary_array = ['107','108','109'];

		var jobs_array = ['120','121','122'];

		var exclude_array = ['101','102','103','104','110','111','113','117','118'];

		// flag: call  = 1, diary = 2,mail  = 3,feed  = 4,jobs  = 5,notice  = 6, pay  = 7


		if (call_array.indexOf(notify_type) != -1) {
			return_status = "1";
		}
		else if (diary_array.indexOf(notify_type) != -1) {
			return_status= "2";
		}
		else if (mail_array.indexOf(notify_type) != -1) {
			return_status= "3";
		}
		else if (feed_array.indexOf(notify_type) != -1) {
			return_status= "4";
		}
		else if (jobs_array.indexOf(notify_type) != -1) {
			return_status= "5";
		}
		else if (notice_array.indexOf(notify_type) != -1) {
			return_status= "6";
		}
		else if (pay_array.indexOf(notify_type) != -1) {
			return_status= "9";
		}
		else if (exclude_array.indexOf(notify_type) != -1) {
			return_status= "0";
		}

		return return_status;
	}
	else 
	{ return return_status; }
}

function __getFileTypes(ext){

	var type;

	var arrImages = ['.jpeg','.jpg','.gif','.png','.tif'];
	var arrVideos = ['.mp4','.avi','.mov','.flv','.wmv'];
	var arrAudio = ['.pcm','.wav','.aiff','.mp3','.aac','.wma','.flac','.alac'];
	var arrDocs = ['.odt','.xls','.xlsx','.ods','.ppt','.pptx','.txt','.doc'];
	var arrPdf = ['.pdf'];

	if(arrImages.includes(ext.toLowerCase())){
		type = '2';
	}else if(arrAudio.includes(ext.toLowerCase())){
		type = '3';
	}else if(arrVideos.includes(ext.toLowerCase())){
		type = '4';
	}else if(arrDocs.includes(ext.toLowerCase())){
		type = '5';
	}else if(arrPdf.includes(ext.toLowerCase())){
		type = '6';
	}else{
		type = '7';
	}

	return type;
};

function __formatSizeUnits(bytes){
	if      (bytes >= 1073741824) { bytes = (bytes / 1073741824).toFixed(2) + " GB"; }
	else if (bytes >= 1048576)    { bytes = (bytes / 1048576).toFixed(2) + " MB"; }
	else if (bytes >= 1024)       { bytes = Math.ceil((bytes / 1024)) + " KB"; }
	else if (bytes > 1)           { bytes = bytes + " bytes"; }
	else if (bytes == 1)          { bytes = bytes + " byte"; }
	else                          { bytes = "0 bytes"; }
	return bytes;
}

//------------------------------------------------------------------- General Module End ---------------------------------------------------------------------------//



module.exports = methods;