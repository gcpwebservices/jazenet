const methods = {};
const DB = require('../../helpers/db_base.js');
const HelperGeneralJazenet = require('../helpers/HelperGeneralJazenet.js');
const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperNotification = require('../../jazecom/helpers/HelperNotification.js');

const promiseForeach = require('promise-foreach');

const STATIC_IMG_URL = G_STATIC_IMG_URL+"uploads/jazenet/";
const STATIC_PROF_ABOUT_URL = G_STATIC_WEB_URL+"jazeprofessional/jazeprofessionalAbout/";
const fs = require('fs');


// const express = require('express');
// const router = express.Router();

const moment = require("moment");
const dateFormat = require('dateformat');

const now = new Date();
const standardDT = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
const newdateForm = dateFormat(new Date(standardDT), 'yyyy-mm-dd HH:MM:ss');



methods.__checkAccountParameters = function(arrParams,return_result){

	try
	{
		var queryAccountType = `SELECT * FROM jaze_user_basic_details WHERE account_id =? and account_type=?`;

		DB.query(queryAccountType,[arrParams.account_id,arrParams.account_type], function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				return_result(1);
			}
			else 
			{

				if(parseInt(arrParams.account_id) === 0 && parseInt(arrParams.account_type) === 0)
				{
					return_result(1);
				}
				else
				{
					return_result(0);
				}

			}

		});

	}
	catch(err)
	{ 
		console.log("__checkAccountParameters",err); return_result(0); 
	}	
};

methods.getCompanyProfile =  function(arrParams,return_result){
	try
	{
		
		var arrAccounts = [];

		var queryAccountType = `SELECT account_id,account_type FROM jaze_user_basic_details WHERE account_id =?`;

		DB.query(queryAccountType,[arrParams.account_id], function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var queryAccDetails = `SELECT * FROM jaze_user_basic_details WHERE account_id =? and account_type=?`;

				DB.query(queryAccDetails,[data[0].account_id,data[0].account_type], function(allData, error){
	
					if(typeof allData !== 'undefined' && allData !== null && parseInt(allData.length) > 0)
					{

						methods.convertMultiMetaArrayCompany(allData, function(retConverted){	

							var counter = retConverted.length;		
							var jsonItems = Object.values(retConverted);

							promiseForeach.each(jsonItems,[function (val) {
								return new Promise(function (resolve, reject) {		
						
							  		var default_logo = '';

									var newObj = {};

							  		if(parseInt(data[0].account_type) === 1)
							  		{
							  			if(val.profile_logo){
											default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + val.profile_logo + '';
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : (val.fname +" "+val.lname).toString(),
											logo		  : default_logo
										}	

							  		}
							  		else if(parseInt(data[0].account_type) === 2)
							  		{

				  						if(val.professional_logo){
											default_logo = G_STATIC_IMG_URL + "uploads/jazenet/professional_account/"+val.account_id+"/logo_thumb/thumb_" + val.professional_logo;
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : (val.fname +" "+val.lname).toString(),
											logo		  : default_logo
										}	

							  		}
						  			else if(parseInt(data[0].account_type) === 3)
					  				{

		  								if(val.listing_images){
				  							default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/listing_images/' + val.listing_images + '';
										
										}else if(val.company_logo){
											default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/logo/' + val.company_logo + '';
										}else{
											default_logo = "";
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : val.company_name,
											logo		  : default_logo
										}	

					  				}

									counter -= 1;
									if (counter === 0){ 
										resolve(newObj);
									}
									
								})
							}],
							function (result, current) {
								if (counter === 0){ 
									return_result(result[0]);
								}
							});

						});


					}
					else
					{
						return_result(null);
					}
				});

		
			}
			else
			{
				return_result(null);
			}
		});

	}
	catch(err)
	{ console.log("getCompanyProfile",err); return_result(null);}
};


//------------------------------------------------------------------- Professional Profile Module Start ---------------------------------------------------------------------------//

methods.getProfessionalProfileDetails = function(reqParams,return_result)
{ 
	try
	{
		if(parseInt(reqParams.prof_id) !== 0)
		{
			var query = " SELECT *  FROM jaze_user_basic_details WHERE meta_key not in ('company_about') and  account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key = 'status' and meta_value = '1') ";
		    	query += " and account_id = '"+reqParams.prof_id+"' ";

				DB.query(query,null, function(data, error){

					if (typeof data[0] !== 'undefined' && data[0] !== null && parseInt(data.length) > 0){

						var list = [];

							Object.assign(list, {account_id : data[0].account_id});
							Object.assign(list, {account_type : data[0].account_type});

							for (var i in data) 
							{
								Object.assign(list, {[data[i].meta_key]:  data[i].meta_value});
							}
							
								methods.__getLocationDetails(list.area_id,list.city_id, function(location_details){	
									
									methods.__getJazecomDtatus(list.account_id, function(jazecom_status){

										methods.__getProfFollowers(list.account_id, function(followers_count){

											methods.__getProfMenuStatus(list.account_id, function(menu_status){

												methods.__getUserFollowStatus(reqParams.account_id,list.account_id, function(follow_status){

													methods.__getUserFavStatus(reqParams.account_id,list.account_id, function(fav_status){

														methods.__getProfLastQualiTitle(list.account_id, function(quali_title){

															methods.__getProfPositionDetails(list.account_id, function(position_details){

																		var default_logo = '';

																		if(list.professional_logo !== "" && list.professional_logo !== null)
																		{
																			default_logo = STATIC_IMG_URL + 'professional_account/'+list.account_id+'/logo/' + list.professional_logo + '';
																		}

																		var newObj = {
																			prof_id	    		: list.account_id.toString(),
																			prof_type			: list.account_type.toString(),
																			first_name			: list.fname.toString(),
																			last_name			: list.lname.toString(),
																			prof_title			: list.profession.toString(),
																			prof_logo			: encodeURI(default_logo.toString()),
																			prof_qualifi_title	: quali_title,
																			area_name			: location_details.area_name,
																			city_name			: location_details.city_name,
																			state_name	 		: location_details.state_name,
																			country_name   		: location_details.country_name,
																			jaze_call			: jazecom_status.jaze_call.toString(),
																			jaze_chat			: jazecom_status.jaze_chat.toString(),
																			jaze_mail			: jazecom_status.jaze_mail.toString(),
																			jaze_request		: jazecom_status.jaze_diary.toString(),
																			prof_about_url		: STATIC_PROF_ABOUT_URL+list.account_id,
																			followers_count		: followers_count.toString(),
																			user_follow_status  : follow_status,
																			user_fav_status     : fav_status,
																			
																		};

																		newObj = Object.assign({}, newObj, menu_status);

																		newObj = Object.assign({}, newObj, position_details);

																		return_result(newObj); 

															});
														});
													});
												});
											});
										});	
									});								
								});
					}
					else
					{ return_result(null);  }
				});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ 
		console.log("getProfessionalProfileDetails",err); 
		return_result(null); 
	}	
}

methods.__getJazecomDtatus = function(account_id,return_result)
{ 
	try
	{
		var obj_return = {
			jaze_call : "0",
			jaze_chat : "0",
			jaze_mail : "0",
			jaze_diary : "0"
		};	

		if(parseInt(account_id) !== 0)
		{
			var query = " SELECT *  FROM jaze_user_jazecome_status WHERE meta_key not in ('account_id','jaze_pay','jaze_feed') and  group_id in (SELECT group_id  FROM jaze_user_jazecome_status WHERE  meta_key = 'account_id' and meta_value = '"+account_id+"') ";
		   
			DB.query(query,null, function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					HelperRestriction.convertMultiMetaArray(data, function(data_result){	
								
						if(data_result !== null)
						{
							var result = data_result[0];

							delete result.group_id;

							return_result(result);
						}
					});
				}
				else
				{ return_result(obj_return); }
			});
		}
		else
		{ return_result(obj_return); }
	}
	catch(err)
	{ 
		console.log("__getJazecomDtatus",err); 
		return_result(arrRows); 
	}	
}

methods.__getProfFollowers =  function (account_id,return_result){
	
	try
	{
		if(parseInt(account_id) !== 0)
		{
			var query  = " SELECT COUNT(DISTINCT group_id) AS count FROM jaze_follower_groups_meta WHERE meta_key IN ('follow_groups') and ";
				query += "group_id IN (  SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'follow_account_id' AND meta_value = "+account_id+" )  ";
				
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0].count);
					}
					else
					{
						return_result('0');
					}
			    });
		}
		else
		{
			return_result('0');
		}
	}
	catch(err)
	{ console.log("__getProfFollowers",err); return_result('0'); }	
};

methods.__getProfMenuStatus =  function (account_id,return_result){
	
	try
	{
		if(parseInt(account_id) !== 0)
		{

			var tb_array = ['jaze_profe_professional_details','jaze_profe_qualification_details', 'jaze_profe_accreditation_details', 'jaze_profe_photo_details','jaze_profe_notice_details'];

			var counter = tb_array.length;

			var array_rows = [];

			Object.assign(array_rows, {menu_about : "1"});

			Object.assign(array_rows, {menu_testimonials : "1"});
						
				if(parseInt(counter) !== 0)
				{
					promiseForeach.each(tb_array,[function (list) {
					
						return new Promise(function (resolve, reject) {	

							var query  = " SELECT COUNT(DISTINCT group_id) AS count FROM "+list+" WHERE  ";
								query += " group_id IN (  SELECT group_id FROM "+list+" WHERE meta_key = 'status' AND meta_value = '1'  ";
								query += " and group_id IN (  SELECT group_id FROM "+list+" WHERE meta_key = 'account_id' AND meta_value = "+account_id+" ))  ";	
							    
								DB.query(query, null, function(data, error){
									
									var obj = {};

									var key = list.replace('jaze_profe_','');

									key = key.replace('jaze_','');

									key = key.replace('_details','');

									key = "menu_"+key;


									if(parseInt(data[0].count) !== 0){

										obj[key] = "1";
									}
									else
									{
										obj[key] = "0";
									}
									
									array_rows = Object.assign({}, array_rows, obj);

									counter -= 1;
									resolve(array_rows);
								});
						})
					}],
					function (result, current) {			
						if (counter === 0)
						{ return_result(result[0]); }
					});	
				}			
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("__getProfMenuStatus",err); return_result(null); }	
};

methods.__getUserFollowStatus =  function (account_id,company_id,return_result){
	
	try
	{
		if(parseInt(account_id) !== 0 && parseInt(company_id) !== 0)
		{
			var query  = " SELECT group_id  FROM jaze_follower_groups_meta WHERE meta_key IN ('follow_groups') and ";
				query += " group_id IN (  SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'account_id' AND meta_value = "+account_id+"   ";
				query += " and group_id IN (  SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'follow_account_id' AND meta_value = "+company_id+" ))  ";
				
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result('1');
					}
					else
					{
						return_result('0');
					}
				});
		}
		else
		{
			return_result('0');
		}
	}
	catch(err)
	{ console.log("__getUserFollowStatus",err); return_result('0'); }	
};

methods.__getUserFavStatus =  function (account_id,company_id,return_result){
	
	try
	{
		if(parseInt(account_id) !== 0 && parseInt(company_id) !== 0)
		{
			var query  = " SELECT group_id  FROM jaze_user_favorite_list WHERE  ";
				query += " group_id IN (  SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'account_id' AND meta_value = "+account_id+"   ";
				query += " and group_id IN (  SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'fav_account_id' AND meta_value = "+company_id+" ))  ";
				
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result('1');
					}
					else
					{
						return_result('0');
					}
				});
		}
		else
		{
			return_result('0');
		}
	}
	catch(err)
	{ console.log("__getUserFavStatus",err); return_result('0'); }	
};

methods.__getProfLastQualiTitle =  function (account_id,return_result){
	
	try
	{
		if(parseInt(account_id) !== 0)
		{
			var query  = " SELECT *  FROM jaze_profe_qualification_details WHERE  ";
				query += " group_id IN (  SELECT group_id FROM jaze_profe_qualification_details WHERE meta_key = 'account_id' AND meta_value = "+account_id+" )  ";
				
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){	

							//sorting 
							sortBy(data_result, {
								prop: "to_year",
								desc: true,
								parser: function(item) { return new Date(item); }
							});

							return_result(data_result[0].course);

						});
					}
					else
					{ return_result(''); }
				});
		}
		else
		{ return_result(''); }
	}
	catch(err)
	{ console.log("__getProfLastQualiTitle",err); return_result(''); }	
};


methods.__getProfPositionDetails =  function (account_id,return_result){
	
	try
	{
		var obj_return = {
			position_title : "",
			position_comp : "",
			position_city : ""
		};	

		if(parseInt(account_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_profe_professional_details WHERE ";
				query += " group_id IN ( SELECT group_id FROM jaze_profe_professional_details WHERE meta_key = 'account_id' AND meta_value = "+account_id+" "; 
				query += " and group_id IN ( SELECT group_id FROM jaze_profe_professional_details WHERE meta_key = 'to_month' AND meta_value = ''  ";
				query += " and group_id IN ( SELECT group_id FROM jaze_profe_professional_details WHERE meta_key = 'to_year' AND meta_value = '' )))  ";
				query += " ORDER BY  CASE WHEN meta_key = 'from_month' THEN meta_value END DESC,CASE WHEN meta_key = 'from_year' THEN meta_value END DESC; ";
				
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){	

							methods.__getLocationDetails(0,data_result[0].city_id, function(location_details){	

								obj_return = {
									position_title : data_result[0].position,
									position_comp  : data_result[0].company_name,
									position_city  : location_details.city_name
								};

								return_result(obj_return);

							});
						});
					}
					else
					{
						return_result(obj_return);
					}
				});
		}
		else
		{
			return_result(obj_return);
		}
	}
	catch(err)
	{ console.log("__getProfPositionDetails",err); return_result(obj_return); }	
};

//------------------------------------------------------------------- Professional Profile Module End ---------------------------------------------------------------------------//






//------------------------------------------------------------------- Accreditation Module Start ---------------------------------------------------------------------------//

methods.getAccreditationList  =  function (prof_id,return_result){
	
	try
	{
		if(parseInt(prof_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_profe_accreditation_details WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_profe_accreditation_details WHERE meta_key = 'account_id'  AND meta_value = "+prof_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_profe_accreditation_details WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						//console.log(data_result);
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											var objAccRres = {
												id            : list.group_id.toString(),
												title         : list.title.toString(),
												company       : list.organization.toString(),
												image         : STATIC_IMG_URL+'professional_account/'+list.account_id+'/accreditation/'+list.logo_path.toString(),
												month         : list.date_month.toString(),
												year          : list.date_year.toString(),
												date          : dateFormat((list.date_year+'-'+list.date_month+'-1'), 'yyyy-mm-dd HH:MM:ss')
											};

											array_rows.push(objAccRres);

											resolve(array_rows);

											counter -= 1;
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ 
											var returnResult = result[0];

											if(returnResult !== null)
											{
												//sorting 
												sortBy(returnResult, {
													prop: "date",
													desc: true,
													parser: function(item) { return new Date(item); }
												});
											}
											
											return_result(returnResult);
										}
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getAccreditationList",err); return_result(null); }	
};

//------------------------------------------------------------------- Accreditation Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Notice Module Start ---------------------------------------------------------------------------//

methods.getNoticeList  =  function (prof_id,return_result){
	
	try
	{
		if(parseInt(prof_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_profe_notice_details WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_profe_notice_details WHERE meta_key = 'account_id'  AND meta_value = "+prof_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_profe_notice_details WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											var logo = "";
											if(list.logo_url !=""){
												logo = STATIC_IMG_URL+'professional_account/'+list.account_id+'/notice_board/'+list.logo_url.toString();
											}

											var objAccRres = {
												id            : list.group_id.toString(),
												title         : list.title.toString(),
												description   : list.notice.toString(),
												image         : logo,
												date          : dateFormat(list.create_date.toString(), 'yyyy-mm-dd HH:MM:ss')
											};

											array_rows.push(objAccRres);

											resolve(array_rows);

											counter -= 1;
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ 
											var returnResult = result[0];

											if(returnResult !== null)
											{
												//sorting 
												sortBy(returnResult, {
													prop: "date",
													desc: true,
													parser: function(item) { return new Date(item); }
												});
											}
											
											return_result(returnResult);
										}
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getNoticeList",err); return_result(null); }	
};

//------------------------------------------------------------------- Notice Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Photos Module Start ---------------------------------------------------------------------------//

methods.getPhotoList  =  function (prof_id,return_result){
	
	try
	{
		if(parseInt(prof_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_profe_photo_details WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_profe_photo_details WHERE meta_key = 'account_id'  AND meta_value = "+prof_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_profe_photo_details WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;
							
							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											var objAccRres = {
												id            : list.group_id.toString(),
												title         : list.photo_title.toString(),
												image         : STATIC_IMG_URL+'professional_account/'+list.account_id+'/professional_photos/'+list.photo_url.toString(),
												date          : dateFormat(list.create_date.toString(), 'yyyy-mm-dd HH:MM:ss')
											};

											array_rows.push(objAccRres);
															
											counter -= 1;
															
											resolve(array_rows);

										});
									}],
									function (result, current) {

										if (counter === 0)
										{ 
											var returnResult = result[0];

											if(returnResult !== null)
											{
												//sorting 
												sortBy(returnResult, {
													prop: "date",
													desc: true,
													parser: function(item) { return new Date(item); }
												});
											}
										
											return_result(returnResult);
										}
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getPhotoList",err); return_result(null); }	
};

//------------------------------------------------------------------- Photos Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Professional History Module Start ---------------------------------------------------------------------------//

methods.getProfessionalHistoryList  =  function (prof_id,return_result){
	
	try
	{
		if(parseInt(prof_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_profe_professional_details WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_profe_professional_details WHERE meta_key = 'account_id'  AND meta_value = "+prof_id+" ); ";
				
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											methods.__getLocationDetails(0,list.city_id, function(location_details){	

												var objAccRres = {
													id             : list.group_id.toString(),
													company_name   : list.company_name.toString(),
													position       : list.position.toString(),
													city_id  	   : list.city_id.toString(),
													city_name 	   : location_details.city_name.toString(),
													country_id     : list.country_id.toString(),
													country_name   : location_details.country_name.toString(),
													from_month     : list.from_month.toString(),
													from_month_name : formatDateName(list.from_month),
													from_year      : list.from_year.toString(),
													to_month       : list.to_month.toString(),
													to_month_name  : formatDateName(list.to_month),
													to_year        : list.to_year.toString(),
												};

												array_rows.push(objAccRres);

												resolve(array_rows);

												counter -= 1;

											});
										});
									}],
									function (result, current) {
										if (counter === 0)
										{  
											var data_result = result[0];



											//sorting 
											sortBy(data_result, {
												prop: "to_year",
												asc: true,
												parser: function(item) { return new Date(item); }
											});

											return_result(data_result); 
										}
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getProfessionalHistoryList",err); return_result(null); }	
};

//------------------------------------------------------------------- Professional History Module Start ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Qualification Module Start ---------------------------------------------------------------------------//

methods.getQualificationList  =  function (prof_id,return_result){
	
	try
	{
		if(parseInt(prof_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_profe_qualification_details WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_profe_qualification_details WHERE meta_key = 'account_id'  AND meta_value = "+prof_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_profe_qualification_details WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											methods.__getLocationDetails(0,list.city_id, function(location_details){	

												var objAccRres = {
													id             : list.group_id.toString(),
													course         : list.course,
													institution    : list.institution,
													city_id  	   : list.city_id.toString(),
													city_name 	   : location_details.city_name,
													country_id     : list.country_id.toString(),
													country_name   : location_details.country_name,
													from_month     : list.from_month.toString(),
													from_month_name : formatDateName(list.from_month),
													from_year      : list.from_year.toString(),
													to_month       : list.to_month.toString(),
													to_month_name  : formatDateName(list.to_month),
													to_year        : list.to_year.toString(),
												};

												array_rows.push(objAccRres);

												resolve(array_rows);

												counter -= 1;

											});
										});
									}],
									function (result, current) {
										if (counter === 0)
										{  
											var data_result = result[0];

											//sorting 
											sortBy(data_result, {
												prop: "to_year",
												asc: true,
												parser: function(item) { return new Date(item); }
											});

											return_result(data_result); 
										}
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getQualificationList",err); return_result(null); }	
};

//------------------------------------------------------------------- Qualification Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Testimonials Module Start ---------------------------------------------------------------------------//

methods.getTestimonialListInbox  =  function (prof_id,return_result){
	
	try
	{
		if(parseInt(prof_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_profe_testimonial_details WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_profe_testimonial_details WHERE meta_key = 'profe_account_id'  AND meta_value = "+prof_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_profe_testimonial_details WHERE meta_key = 'status' AND meta_value = 0 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											var arrParamss = {account_id:list.account_id};

											HelperGeneralJazenet.getAccountDetails(arrParamss, function(accRres){

												if(accRres !== null)
												{
													var location = '';

													if(accRres.city != '')
													{ location = accRres.city+','+accRres.country; }
													else 
													{ location = accRres.country; }

													var objAccRres = {
														id   		  : list.group_id.toString(),
														name		  : accRres.name,
														logo		  : accRres.logo,
														location      : location,
														message       : list.description.toString(),
														date          : dateFormat(list.create_date.toString(), 'yyyy-mm-dd HH:MM:ss'),
														status        : list.status.toString()
													};

													array_rows.push(objAccRres);

													resolve(array_rows);
												}

												counter -= 1;

											});
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ 
											var returnResult = result[0];

											if(returnResult !== null)
											{
												//sorting 
												sortBy(returnResult, {
													prop: "date",
													desc: true,
													parser: function(item) { return new Date(item); }
												});
											}
											
											return_result(returnResult);
										}
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getTestimonialListInbox",err); return_result(null); }	
};



methods.getTestimonialListInboxCount  =  function (prof_id,flag,return_result){
	
	try
	{
		if(parseInt(prof_id) !== 0)
		{
			var query  = ` SELECT * FROM jaze_profe_testimonial_details WHERE  
				group_id IN (SELECT group_id FROM jaze_profe_testimonial_details WHERE meta_key = 'profe_account_id'  AND meta_value =?)
				AND group_id IN (SELECT group_id FROM jaze_profe_testimonial_details WHERE meta_key = 'status' AND meta_value =? ) GROUP BY group_id`;
					
			DB.query(query, [prof_id,flag], function(data, error){
				
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					return_result(parseInt(data.length));
				}
				else
				{
					return_result(0);
				}
		    });
		}
		else
		{
			return_result(0);
		}
	}
	catch(err)
	{ console.log("getTestimonialListInboxCount",err); return_result(0); }	
};

methods.getTestimonialList  =  function (prof_id,return_result){
	
	try
	{
		if(parseInt(prof_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_profe_testimonial_details WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_profe_testimonial_details WHERE meta_key = 'profe_account_id'  AND meta_value = "+prof_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_profe_testimonial_details WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											var arrParamss = {account_id:list.account_id};

											HelperGeneralJazenet.getAccountDetails(arrParamss, function(accRres){

												if(accRres !== null)
												{
													var location = '';

													if(accRres.city != '')
													{ location = accRres.city+','+accRres.country; }
													else 
													{ location = accRres.country; }

													var objAccRres = {
														id   		  : list.group_id.toString(),
														name		  : accRres.name,
														logo		  : accRres.logo,
														location      : location,
														message       : list.description.toString(),
														date          : dateFormat(list.create_date.toString(), 'yyyy-mm-dd HH:MM:ss'),
														status        : list.status.toString()
													};

													array_rows.push(objAccRres);

													resolve(array_rows);
												}

												counter -= 1;

											});
										});
									}],
									function (result, current) {
										if (counter === 0)
										{ 
											var returnResult = result[0];

											if(returnResult !== null)
											{
												//sorting 
												sortBy(returnResult, {
													prop: "date",
													desc: true,
													parser: function(item) { return new Date(item); }
												});
											}
											
											return_result(returnResult);
										}
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getTestimonialList",err); return_result(null); }	
};

methods.postTestimonial = function(arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{
			//create
			HelperRestriction.getNewGroupId('jaze_profe_testimonial_details', function (group_id) {

				if(parseInt(group_id) !== 0){

					var	arrInsert = {
						account_id	  		: arrParams.account_id,
						profe_account_id	: arrParams.prof_id,
						description 		: arrParams.message,
						create_date   		: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
						status 		  		: '1'						
					};

					HelperRestriction.insertTBQuery('jaze_profe_testimonial_details',group_id,arrInsert, function (queGrpId) {

						return_result(queGrpId);
					});
				}
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("postTestimonial",err); return_result(null); }	
};

//------------------------------------------------------------------- Testimonials Module End  ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Account Favourite Module Start ---------------------------------------------------------------------------//

methods.saveAccountFavourite  =  function (arrParams,return_result){
	
	try
	{
		if(arrParams !== null)
		{
			var query  = " SELECT group_id FROM jaze_user_favorite_list WHERE  ";
				query += "     group_id IN (SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'account_id'  AND meta_value = "+arrParams.account_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'fav_account_id'  AND meta_value = "+arrParams.prof_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_user_favorite_list WHERE meta_key = 'status' AND meta_value = 1 ))); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						//delete

						var query  = " DELETE FROM `jaze_user_favorite_list` WHERE `group_id` = '"+data[0].group_id+"'; ";

						DB.query(query,null, function(data_check, error){

							return_result(2);

						});
					}
					else
					{
						//insert
						HelperRestriction.getNewGroupId('jaze_user_favorite_list', function (group_id) {

							if(parseInt(group_id) !== 0){
			
								var	arrInsert = {
									account_id	  		: arrParams.account_id,
									fav_account_id		: arrParams.prof_id,
									fav_account_type    : '2',
									fav_type    		: '1',
									create_date   		: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
									status 		  		: '1'						
								};
			
								HelperRestriction.insertTBQuery('jaze_user_favorite_list',group_id,arrInsert, function (queGrpId) {
									return_result(1);
								});
							}
						});
					}
			    });
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{ console.log("setFollowCompany",err); return_result(0); }	
};

//------------------------------------------------------------------- Account Favourite Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Professional Essential Module Start ---------------------------------------------------------------------------//
methods.getUserIDEssentials = function(arrParams,return_result){

	try
	{

		var query = `SELECT * FROM jaze_user_jazenet_id WHERE account_id=?`;
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				var accParam = {account_id:data[0].account_id};
				methods.getProfessionalEssDetails(accParam,function(retEssDetails){

					if(retEssDetails!=null)
					{

						methods.__getNationality(retEssDetails.nationality_id,function(nationality){

							methods.__getCountryDetails(retEssDetails.country_id,function(country){

								methods.__getStateDetails(retEssDetails.state_id,function(state){

									methods.__getCityDetails(retEssDetails.city_id,function(city){

										methods.__getAreaDetails(retEssDetails.area_id,function(area){

											methods.__getProfessionalTitle(retEssDetails.professional_title_id,function(title){

												methods.__getMobileCode(retEssDetails.mobile_country_code_id,function(phoneCode1){

													methods.__getMobileCode(retEssDetails.mobile_country_code_id_2,function(phoneCode2){

														var dateText = removeText(retEssDetails.dob);
														var newdate = new Date(dateText);

														var essential = {
															title_id        : retEssDetails.professional_title_id,
															title           : title,
															first_name      : retEssDetails.fname,
															last_name       : retEssDetails.lname,
															email           : retEssDetails.email,
															dob             : dateFormat(newdate,'yyyy-mm-dd'),
															profession      : retEssDetails.job_title,
															nationality     : nationality,
															nationality_id  : retEssDetails.nationality_id,
															mobile_number   : retEssDetails.mobile,
															mobile_country  : phoneCode1,
															mobile_code     : retEssDetails.mobile_country_code_id,
															mobile_number_2   : retEssDetails.mobile_2,
															mobile_country_2  : phoneCode2,
															mobile_code_2     :retEssDetails.mobile_country_code_id_2,
															country_id      : retEssDetails.country_id,
															country_name    : country,
															state_id        : retEssDetails.state_id,
															state_name      : state,
															city_id         : retEssDetails.city_id,
															city_name       : city,
															area_id         : retEssDetails.area_id,
															area_name       : area,
															street_name    	: retEssDetails.street_name,
															street_no      	: retEssDetails.street_no,
															building_name  	: retEssDetails.building_name,
															building_no    	: retEssDetails.building_no,
															jazenet_id      : data[0].jazenet_id
														};

														var main_obj = {
															professional_essential_info : essential
														};

														return_result(main_obj);

													});
												});
											});
										});
									});
								});
							});
						});
					}
					else
					{
						return_result(null);
					}

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getUserIDEssentials",err); return_result(null);}

};


			

methods.updateProfessionalEssDetails =  function(arrParams,return_result){
	try
	{

		var query = `SELECT * FROM jaze_user_basic_details WHERE account_id=? AND account_type =2`;
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArrayCompany(data, function(retEssDetails){	

					if(retEssDetails!=null){


						var account_id = retEssDetails[0].account_id;

						var	arrUpdate = {
							fname           : arrParams.first_name,
							lname           : arrParams.last_name,
							email           : arrParams.email,
							dob             : formatDate(arrParams.dob),
							profession      : arrParams.profession,
							job_title       : arrParams.profession,
							nationality_id  : arrParams.nationality_id,
							mobile_country_code_id  : arrParams.mobile_country_code,
							mobile          : arrParams.mobile_number,
							mobile_country_code_id_2  : arrParams.mobile_country_code_2,
							mobile_2          : arrParams.mobile_number_2,
							country_id      : arrParams.country_id,
							state_id        : arrParams.state_id,
							city_id         : arrParams.city_id,
							area_id         : arrParams.area_id,
							professional_title_id : arrParams.title_id,
							street_name    : arrParams.street_name,
							street_no      : arrParams.street_no,
							building_name  : arrParams.building_name,
							building_no    : arrParams.building_no
						
						};

						var updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key =? AND account_id =? AND account_type =2 `;
						
						for (var key in arrUpdate) 
					 	{	
						     //if(arrUpdate[key] !== null && arrUpdate[key] != ""){
								DB.query(updateQue, [arrUpdate[key],key,account_id], function(datau, error){
							 		if(datau !== null){
						 				return_result(1);
							 	   	}else{
						 	   			return_result(0);
							 	   	}
								});
							//}
						}
					}
					else
					{
						return_result(0);
					}

				});

			}

		});

	}
	catch(err)
	{ console.log("updateProfessionalEssDetails",err); return_result(0);}
};

methods.getProfessionalEssDetails =  function(arrParams,return_result){
	try
	{

		var arrAccounts = [];

		var query = `SELECT * FROM jaze_user_basic_details WHERE account_id =?`;
		DB.query(query,[arrParams.account_id], function(data, error){
	
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				methods.convertMultiMetaArrayCompany(data, function(retConverted){	
		
						return_result(retConverted[0]);
				

				});
			}
			else
			{
				return_result(null);
			}
		});

	}
	catch(err)
	{ console.log("getProfessionalEssDetails",err); return_result(null);}
};
//------------------------------------------------------------------- Professional Essential Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Professional Upload Profile Picture Module Start ---------------------------------------------------------------------------//
methods.getProfilePicture =  function(arrParams,return_result){
	try
	{

		var arrAccounts = [];

		var query = `SELECT * FROM jaze_user_basic_details WHERE account_id =? AND meta_key = 'professional_logo'`;
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				methods.convertMultiMetaArrayCompany(data,function(retConverted){
					
					var storagePath = G_STATIC_IMG_PATH+'professional_account/'+arrParams.account_id+'/logo/'+retConverted[0].professional_logo;
					fs.unlink(storagePath, function (err) {});

					var updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key ='professional_logo' AND account_id =?`;
					DB.query(updateQue, [arrParams.file_name,data[0].account_id], function(datau, error){
				 		if(datau !== null){
			 				return_result(1);
				 	   	}else{
			 	   			return_result(0);
				 	   	}
					});

				});
			}
			else
			{
				return_result(null);
			}
		});

	}
	catch(err)
	{ console.log("getProfilePicture",err); return_result(null);}
};
//------------------------------------------------------------------- Professional Upload Profile Picture Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Location Module Start ---------------------------------------------------------------------------//

methods.__getLocationDetails = function(area_id,city_id,return_result){

	try{
			var obj_return = {
				country_id : "0",
				country_name : "",
				state_id : "0",
				state_name : "",
				city_id : "0",
				city_name : "",
				area_id : "0",
				area_name : "",
			};

			if(parseInt(area_id) !== 0)
			{
				var query  =" SELECT  con.id as country_id,con.country_name ,st.id as state_id,st.state_name,city.id as city_id,city.city_name,ar.area_id,ar.area_name ";
					query +=" FROM area ar  ";
					query +=" INNER JOIN cities city ON city.id = ar.city_id ";
                    query +=" INNER JOIN states st ON st.id = city.state_id ";
                    query +=" INNER JOIN country con ON con.id=st.country_id ";
					query +=" WHERE ar.status = '1' and city.status = '1' and st.status = '1' and con.status = '1' and ar.area_id = '"+area_id+"' " ;
 
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0]);
					}
					else
					{return_result(obj_return)}
				});
			}
			else if(parseInt(city_id) !== 0)
			{
				var query  =" SELECT  con.id as country_id,con.country_name ,st.id as state_id,st.state_name,city.id as city_id,city.city_name, '0' as area_id, '' as area_name ";
					query +=" FROM cities city  ";
                    query +=" INNER JOIN states st ON st.id = city.state_id ";
                    query +=" INNER JOIN country con ON con.id=st.country_id ";
					query +=" WHERE city.status = '1' and st.status = '1' and con.status = '1' and city.id = '"+city_id+"' " ;
 
				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data[0]);
					}
					else
					{return_result(obj_return)}
				});
			}
			else
			{ return_result(obj_return); }

			
	}
	catch(err)
	{
		return_result(obj_return)
	}
};

methods.__getCountryDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT country_name FROM country  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};


methods.__getCountryShortCode = function(arrParams,return_result){

	try{
			let query  = " SELECT country_short_code FROM country  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_short_code);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};


methods.__getMobileCode= function (phone_code,return_result){
	try
	{
		if(parseInt(phone_code) !== 0)
		{
			var query = " SELECT `phonecode` FROM `country` WHERE `id` = '"+phone_code+"'; ";
			DB.query(query, null, function(data, error){
				
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result("+"+data[0].phonecode);
				}
				else
				{ return_result(''); }
			});

			
		}
		else
		{
			return_result('');
		}
	}
	catch(err)
	{ console.log("getMobileCode",err); return_result(''); }	
};


methods.__getProfessionalTitle = function (prof_id,return_result){
	try
	{
		if(parseInt(prof_id) !== 0)
		{
			var query = "SELECT * FROM jaze_master_professional_title WHERE group_id=?";
			DB.query(query, [prof_id], function(data, error){
				
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					HelperRestriction.convertMultiMetaArray(data, function(data_result){	
								
						if(data_result !== null)
						{
							return_result(data_result[0].title);
						}
					});
				}
				else
				{ return_result(''); }
			});

			
		}
		else
		{
			return_result('');
		}
	}
	catch(err)
	{ console.log("__getProfessionalTitle",err); return_result(''); }	
};


methods.__getProfessionalTitleRequest = function (arrParams,return_result){
	try
	{
		var arrResults = [];

		var query = "SELECT * FROM jaze_master_professional_title";
		DB.query(query, null, function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				HelperRestriction.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
				
							var obj_title ={
								title_id   : jsonItems.group_id.toString(),
								title_name : jsonItems.title
							}

							arrResults.push(obj_title);
							counter -= 1;
							resolve(arrResults);
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});

				});

			}
			else
			{ return_result(null); }
		});

	
	}
	catch(err)
	{ console.log("__getProfessionalTitleRequest",err); return_result(null); }	
};
//------------------------------------------------------------------- Location Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- About Update Module Start ---------------------------------------------------------------------------//


methods.getProfAbout = function (arrParams,return_result){
	try
	{

		var query = `SELECT * FROM jaze_user_basic_details WHERE account_id =? AND meta_key = 'professional_about'`;
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				return_result(data[0].meta_value);

			}
			else
			{
				return_result(null);
			}

		});
	}
	catch(err)
	{ console.log("getProfAbout",err); return_result(null); }	
};

methods.updateAbout = function (arrParams,return_result){
	try
	{

	    if(parseInt(arrParams.flag) === 2)
		{
			var query = `SELECT * FROM jaze_user_basic_details WHERE account_id =? AND meta_key = 'professional_about'`;
			DB.query(query,[arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					var updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key ='professional_about' AND account_id =?`;
					DB.query(updateQue, [arrParams.about,data[0].account_id], function(datau, error){
				 		if(datau !== null){
			 				return_result(1);
				 	   	}else{
			 	   			return_result(0);
				 	   	}
					});

				}
				else
				{
					return_result(0);
				}

			});
		}
		else if(parseInt(arrParams.flag) === 3)
		{

			var query = `SELECT * FROM jaze_user_basic_details WHERE account_id =? AND meta_key = 'professional_about'`;
			DB.query(query,[arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					var updateQue = `update jaze_user_basic_details SET meta_value =? WHERE meta_key ='professional_about' AND account_id =?`;
					DB.query(updateQue, ["",data[0].account_id], function(datau, error){
				 		if(datau !== null){
			 				return_result(1);
				 	   	}else{
			 	   			return_result(0);
				 	   	}
					});

				}
				else
				{
					return_result(0);
				}
				
			});
		}
	}
	catch(err)
	{ console.log("updateAbout",err); return_result(0); }	
};

//------------------------------------------------------------------- About Update Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- About Update Professional History Start ---------------------------------------------------------------------------//
methods.updateProfessionalHistory = function (arrParams,return_result){
	try
	{

	    if(parseInt(arrParams.flag) === 1)
		{
			

			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_profe_professional_details";
  			var lastGrpId = 0;

			methods.getLastGroupId(queGroupMax, function(queGrpRes){

			  	queGrpRes.forEach(function(row) {
					lastGrpId = row.lastGrpId;
			  	});

			  
				var arrInsert = {

					account_id   : arrParams.account_id,
					position     : arrParams.position,
					company_name : arrParams.company_name,
					country_id   : arrParams.country_id,
					city_id      : arrParams.city_id,
					from_month   : arrParams.from_month,
					from_year    : arrParams.from_year,
					to_month     : arrParams.to_month,
					to_year      : arrParams.to_year,
					status       : 1
					
				};
			
				var plusOneGroupID = lastGrpId+1;
		  		var querInsert = `insert into jaze_profe_professional_details (group_id, meta_key, meta_value) VALUES (?,?,?)`;

		  		var returnFunction;
			 	for (var key in arrInsert) 
			 	{
					DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
			 		 	if(parseInt(data.affectedRows) > 0){
	 						return_result(1);
				 	   	}else{
		   					return_result(0);
				 	   	}
			 	 	});
			  	};

			});

		}
		else if(parseInt(arrParams.flag) === 2)
		{

			var queLib = `SELECT * FROM jaze_profe_professional_details WHERE group_id =?
			AND group_id IN (SELECT group_id FROM jaze_profe_professional_details WHERE meta_key = 'account_id' AND meta_value =?)`;

			DB.query(queLib,[arrParams.id,arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
	 				
					var arrUpdate = {
				
						position     : arrParams.position,
						company_name : arrParams.company_name,
						country_id   : arrParams.country_id,
						city_id      : arrParams.city_id,
						from_month   : arrParams.from_month,
						from_year    : arrParams.from_year,
						to_month     : arrParams.to_month,
						to_year      : arrParams.to_year
					};

					var updateQue = `update jaze_profe_professional_details SET meta_value =? WHERE meta_key =? AND group_id =?`;
					
					for (var key in arrUpdate) 
				 	{	
					     //if(arrUpdate[key] !== null && arrUpdate[key] != ""){
							DB.query(updateQue, [arrUpdate[key],key,arrParams.id], function(data, error){
						 		if(data !== null){
					 				return_result(1);
						 	   	}else{
					 	   			return_result(0);
						 	   	}
							});
						//}
					}

		 	   	}
		 	   	else
		 	   	{
		 	   		return_result(2);
		 	   	}

	 	 	});
		}
		else if(parseInt(arrParams.flag) === 3)
		{

			var queLib = `SELECT * FROM jaze_profe_professional_details WHERE group_id =?
			AND group_id IN (SELECT group_id FROM jaze_profe_professional_details WHERE meta_key = 'account_id' AND meta_value =?)`;

			DB.query(queLib,[arrParams.id,arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
	 				
 					var queLib = `DELETE FROM jaze_profe_professional_details WHERE group_id=?`;
					DB.query(queLib,[arrParams.id], function(data, error){	
						if(data !== null){
			 				return_result(1);
				 	   	}else{
			 	   			return_result(0);
				 	   	}
					});

		 	   	}
		 	   	else
		 	   	{
		 	   		return_result(2);
		 	   	}

	 	 	});

		}
	}
	catch(err)
	{ console.log("updateProfessionalHistory",err); return_result(0); }	
};



methods.getmyProfessionalHistory = function (arrParams,return_result){
	try
	{

		var query  = " SELECT * FROM jaze_profe_professional_details WHERE  ";
		query += " group_id IN (SELECT group_id FROM jaze_profe_professional_details WHERE meta_key = 'account_id'  AND meta_value = "+arrParams.account_id+" ); ";
		
		DB.query(query, null, function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				HelperRestriction.convertMultiMetaArray(data, function(data_result){
				
					var array_rows = [];

					var counter = data_result.length;

					if(parseInt(counter) !== 0)
					{
							promiseForeach.each(data_result,[function (list) {

								return new Promise(function (resolve, reject) {

									methods.__getLocationDetails(0,list.city_id, function(location_details){	

										var objAccRres = { 
											id             : list.group_id.toString(),
											company_name   : list.company_name.toString(),
											position       : list.position.toString(),
											city_id 	   : list.city_id.toString(),
											city_name 	   : location_details.city_name.toString(),
											country_id     : list.country_id.toString(),
											country_name   : location_details.country_name.toString(),
											from_month     : list.from_month.toString(),
											from_month_name : formatDateName(list.from_month),
											from_year      : list.from_year.toString(),
											to_month       : list.to_month.toString(),
											to_month_name   : formatDateName(list.to_month),
											to_year        : list.to_year.toString(),
										};

										counter -= 1;
										array_rows.push(objAccRres);

										if(counter === 0){
											resolve(array_rows);
										}
								
									});
								});
							}],
							function (result, current) {
								if (counter === 0)
								{  
									var data_result = result[0];

									sortBy(data_result, {
										prop: "to_year",
										asc: true,
										parser: function(item) { return new Date(item); }
									});

									return_result(data_result); 
								}
							});	
					}
					else
					{ return_result(null); }
				});
			}
			else
			{
				return_result(null);
			}
	    });

	
	}
	catch(err)
	{ console.log("getmyProfessionalHistory",err); return_result(0); }	
};

//------------------------------------------------------------------- About Update Professional History End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- About Update Qualification History Start ---------------------------------------------------------------------------//
methods.getMyQualificationList  =  function (arrParams,return_result){
	
	try
	{
		if(parseInt(arrParams.account_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_profe_qualification_details WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_profe_qualification_details WHERE meta_key = 'account_id'  AND meta_value = "+arrParams.account_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_profe_qualification_details WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {

										return new Promise(function (resolve, reject) {

											methods.__getLocationDetails(0,list.city_id, function(location_details){	

												var objAccRres = {
													id              : list.group_id.toString(),
													course          : list.course.toString(),
													institution     : list.institution.toString(),
													city_name 	    : location_details.city_name.toString(),
													city_id 	    : list.city_id.toString(),
													country_name    : location_details.country_name.toString(),
													country_id      : list.country_id.toString(),
													from_month_name : formatDateName(list.from_month),
													from_month     : list.from_month.toString(),
													from_year      : list.from_year.toString(),
													to_month       : list.to_month.toString(),
													to_month_name   : formatDateName(list.to_month),
													to_year        : list.to_year.toString(),
												};


												counter -= 1;
												array_rows.push(objAccRres);

												if (counter === 0){
													resolve(array_rows);
												}


											});
										});
									}],
									function (result, current) {
										if (counter === 0)
										{  
											var data_result = result[0];

											//sorting 
											sortBy(data_result, {
												prop: "to_year",
												asc: true,
												parser: function(item) { return new Date(item); }
											});

											return_result(data_result); 
										}
									});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getMyQualificationList",err); return_result(null); }	
};


methods.updateQualificationHistory = function (arrParams,return_result){
	try
	{

	    if(parseInt(arrParams.flag) === 1)
		{
			

			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_profe_qualification_details";
  			var lastGrpId = 0;

			methods.getLastGroupId(queGroupMax, function(queGrpRes){

			  	queGrpRes.forEach(function(row) {
					lastGrpId = row.lastGrpId;
			  	});

			  
				var arrInsert = {

					account_id   : arrParams.account_id,
					course       : arrParams.course,
					institution  : arrParams.institution,
					country_id   : arrParams.country_id,
					city_id      : arrParams.city_id,
					from_month   : arrParams.from_month,
					from_year    : arrParams.from_year,
					to_month     : arrParams.to_month,
					to_year      : arrParams.to_year,
					status       : 1
					
				};
			
				var plusOneGroupID = lastGrpId+1;
		  		var querInsert = `insert into jaze_profe_qualification_details (group_id, meta_key, meta_value) VALUES (?,?,?)`;

		  		var returnFunction;
			 	for (var key in arrInsert) 
			 	{
					DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
			 		 	if(parseInt(data.affectedRows) > 0){
	 						return_result(1);
				 	   	}else{
		   					return_result(0);
				 	   	}
			 	 	});
			  	};

			});

		}
		else if(parseInt(arrParams.flag) === 2)
		{

			var queLib = `SELECT * FROM jaze_profe_qualification_details WHERE group_id=? `;
			DB.query(queLib,[arrParams.id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
	 				
					var arrUpdate = {
				
						course       : arrParams.course,
						institution  : arrParams.institution,
						country_id   : arrParams.country_id,
						city_id      : arrParams.city_id,
						from_month   : arrParams.from_month,
						from_year    : arrParams.from_year,
						to_month     : arrParams.to_month,
						to_year      : arrParams.to_year
					};

					var updateQue = `update jaze_profe_qualification_details SET meta_value =? WHERE meta_key =? AND group_id =?`;
					
					for (var key in arrUpdate) 
				 	{	
					     //if(arrUpdate[key] !== null && arrUpdate[key] != ""){
							DB.query(updateQue, [arrUpdate[key],key,arrParams.id], function(data, error){
						 		if(data !== null){
					 				return_result(1);
						 	   	}else{
					 	   			return_result(0);
						 	   	}
							});
						//}
					}

		 	   	}
		 	   	else
		 	   	{
		 	   		return_result(2);
		 	   	}

	 	 	});
		}
		else if(parseInt(arrParams.flag) === 3)
		{

			var queLib = `SELECT * FROM jaze_profe_qualification_details WHERE group_id=? `;
			DB.query(queLib,[arrParams.id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
	 				
 					var queLib = `DELETE FROM jaze_profe_qualification_details WHERE group_id=?`;
					DB.query(queLib,[arrParams.id], function(data, error){	
						if(data !== null){
			 				return_result(1);
				 	   	}else{
			 	   			return_result(0);
				 	   	}
					});

		 	   	}
		 	   	else
		 	   	{
		 	   		return_result(2);
		 	   	}

	 	 	});

		}
	}
	catch(err)
	{ console.log("updateQualificationHistory",err); return_result(0); }	
};


//------------------------------------------------------------------- About Update Qualification History End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- About Update Photos Start ---------------------------------------------------------------------------//


methods.getMyPhotos  =  function (arrParams,return_result){
	
	try
	{
		if(parseInt(arrParams.account_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_profe_photo_details WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_profe_photo_details WHERE meta_key = 'account_id'  AND meta_value = "+arrParams.account_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_profe_photo_details WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data_result,[function (list) {

									return new Promise(function (resolve, reject) {

										var objAccRres = {
											id       : list.group_id.toString(),
											title    : list.photo_title,
											image    : STATIC_IMG_URL+'/professional_account/'+list.account_id+'/professional_photos/'+list.photo_url,
											date     : list.create_date
										};


										counter -= 1;
										array_rows.push(objAccRres);

										if (counter === 0){
											resolve(array_rows);
										}

									});
								}],
								function (result, current) {
									if (counter === 0)
									{  
										var data_result = result[0];
										return_result(data_result); 
									}
								});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getMyQualificationList",err); return_result(null); }	
};


methods.updateMyPhotos = function (arrParams,return_result){
	try
	{

	    if(parseInt(arrParams.flag) === 1)
		{
			

			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_profe_photo_details";
  			var lastGrpId = 0;

			methods.getLastGroupId(queGroupMax, function(queGrpRes){

			  	queGrpRes.forEach(function(row) {
					lastGrpId = row.lastGrpId;
			  	});

			  	var full_filename = arrParams.file_name+'.'+arrParams.file_ext;

			  	if(arrParams.file_name !=="" && arrParams.file_ext !==""){

					var arrInsert = {

						account_id   : arrParams.account_id,
						photo_title  : arrParams.title,
						photo_url    : full_filename,
						create_date  : newdateForm,
						status       : 1
						
					};
				
					var plusOneGroupID = lastGrpId+1;
			  		var querInsert = `insert into jaze_profe_photo_details (group_id, meta_key, meta_value) VALUES (?,?,?)`;

			  		var returnFunction;
				 	for (var key in arrInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
				 		 	if(parseInt(data.affectedRows) > 0){
		 						return_result(1);
					 	   	}else{
			   					return_result(0);
					 	   	}
				 	 	});
				  	};

			  	}
			  	else{
		  			return_result(0);
			  	}


			});

		}
		else if(parseInt(arrParams.flag) === 2)
		{

			var queLib = `SELECT * FROM jaze_profe_photo_details WHERE group_id=? `;
			DB.query(queLib,[arrParams.id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
	 				
				  	var full_filename = "";
			  	 	if(arrParams.file_name !=="" && arrParams.file_ext !==""){
			  	 		full_filename = arrParams.file_name+'.'+arrParams.file_ext;
			  	 	}
	 					  	
					var arrUpdate = {
				
						photo_title  : arrParams.title,
						photo_url    : full_filename,
						create_date  : newdateForm,
						status       : 1
					};

					var updateQue = `update jaze_profe_photo_details SET meta_value =? WHERE meta_key =? AND group_id =?`;
					
					for (var key in arrUpdate) 
				 	{	
					     if(arrUpdate[key] !== null && arrUpdate[key] != ""){
							DB.query(updateQue, [arrUpdate[key],key,arrParams.id], function(data, error){
						 		if(data !== null){
					 				return_result(1);
						 	   	}else{
					 	   			return_result(0);
						 	   	}
							});
						}
					}

		 	   	}
		 	   	else
		 	   	{
		 	   		return_result(2);
		 	   	}

	 	 	});
		}
		else if(parseInt(arrParams.flag) === 3)
		{

			var queLib = `SELECT * FROM jaze_profe_photo_details WHERE group_id=? `;
			DB.query(queLib,[arrParams.id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
	 				
 					var queLib = `DELETE FROM jaze_profe_photo_details WHERE group_id=?`;
					DB.query(queLib,[arrParams.id], function(data, error){	
						if(data !== null){
			 				return_result(1);
				 	   	}else{
			 	   			return_result(0);
				 	   	}
					});

		 	   	}
		 	   	else
		 	   	{
		 	   		return_result(2);
		 	   	}

	 	 	});

		}
	}
	catch(err)
	{ console.log("updateQualificationHistory",err); return_result(0); }	
};


//------------------------------------------------------------------- About Update Photos End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Update Accreditation Module Start ---------------------------------------------------------------------------//

methods.getMyAccreditation  =  function (arrParams,return_result){
	
	try
	{
		if(parseInt(arrParams.account_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_profe_accreditation_details WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_profe_accreditation_details WHERE meta_key = 'account_id'  AND meta_value = "+arrParams.account_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_profe_accreditation_details WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data_result,[function (list) {

									return new Promise(function (resolve, reject) {

							

										var objAccRres = {
											id            : list.group_id.toString(),
											title         : list.title,
											company  : list.organization,
											image     : STATIC_IMG_URL+list.account_id+'/accreditation/'+list.logo_path,
											month    : list.date_month,
											year     : list.date_year
										};


										counter -= 1;
										array_rows.push(objAccRres);

										if (counter === 0){
											resolve(array_rows);
										}


									});
								}],
								function (result, current) {
									if (counter === 0)
									{  
										var data_result = result[0];
										return_result(data_result); 
									}
								});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getMyAccreditation",err); return_result(null); }	
};


methods.updateAccreditation = function (arrParams,return_result){
	try
	{

	    if(parseInt(arrParams.flag) === 1)
		{
			

			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_profe_accreditation_details";
  			var lastGrpId = 0;

			methods.getLastGroupId(queGroupMax, function(queGrpRes){

			  	queGrpRes.forEach(function(row) {
					lastGrpId = row.lastGrpId;
			  	});

			  	var full_filename = arrParams.file_name+'.'+arrParams.file_ext;

			  	if(arrParams.file_name !=="" && arrParams.file_ext !==""){

					var arrInsert = {

						account_id   : arrParams.account_id,
						title        : arrParams.title,
						organization : arrParams.organization,
						logo_path    : full_filename,
						date_month   : arrParams.month,
						date_year    : arrParams.year,
						create_date  : newdateForm,
						status       : 1
						
					};	

			
				
					var plusOneGroupID = lastGrpId+1;
			  		var querInsert = `insert into jaze_profe_accreditation_details (group_id, meta_key, meta_value) VALUES (?,?,?)`;

			  		var returnFunction;
				 	for (var key in arrInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
				 		 	if(parseInt(data.affectedRows) > 0){
		 						return_result(1);
					 	   	}else{
			   					return_result(0);
					 	   	}
				 	 	});
				  	};

			  	}
			  	else{
		  			return_result(0);
			  	}


			});

		}
		else if(parseInt(arrParams.flag) === 2)
		{

			var queLib = `SELECT * FROM jaze_profe_accreditation_details WHERE group_id=? `;
			DB.query(queLib,[arrParams.id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
 					var full_filename = "";
 					if(arrParams.file_name !=="" && arrParams.file_ext !==""){
				  		full_filename = arrParams.file_name+'.'+arrParams.file_ext;
			  		}
			  		
					var arrUpdate = {
				
						title        : arrParams.title,
						organization : arrParams.organization,
						logo_path    : full_filename,
						date_month   : arrParams.month,
						date_year    : arrParams.year,
						create_date  : newdateForm,
					};

					var updateQue = `update jaze_profe_accreditation_details SET meta_value =? WHERE meta_key =? AND group_id =?`;
					
					for (var key in arrUpdate) 
				 	{	
					     if(arrUpdate[key] !== null && arrUpdate[key] != ""){
							DB.query(updateQue, [arrUpdate[key],key,arrParams.id], function(data, error){
						 		if(data !== null){
					 				return_result(1);
						 	   	}else{
					 	   			return_result(0);
						 	   	}
							});
						}
					}

		 	   	}
		 	   	else
		 	   	{
		 	   		return_result(2);
		 	   	}

	 	 	});
		}
		else if(parseInt(arrParams.flag) === 3)
		{

			var queLib = `SELECT * FROM jaze_profe_accreditation_details WHERE group_id=? `;
			DB.query(queLib,[arrParams.id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
	 				
 					var queLib = `DELETE FROM jaze_profe_accreditation_details WHERE group_id=?`;
					DB.query(queLib,[arrParams.id], function(data, error){	
						if(data !== null){
			 				return_result(1);
				 	   	}else{
			 	   			return_result(0);
				 	   	}
					});

		 	   	}
		 	   	else
		 	   	{
		 	   		return_result(2);
		 	   	}

	 	 	});

		}
	}
	catch(err)
	{ console.log("updateAccreditation",err); return_result(0); }	
};

//------------------------------------------------------------------- Update Accreditation Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Update Notice Module Start ---------------------------------------------------------------------------//
methods.getMyNotice  =  function (arrParams,return_result){
	
	try
	{
		if(parseInt(arrParams.account_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_profe_notice_details WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_profe_notice_details WHERE meta_key = 'account_id'  AND meta_value = "+arrParams.account_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_profe_notice_details WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data_result,[function (list) {

									return new Promise(function (resolve, reject) {

										var logo = "";
										if(list.logo_url !="")
										{
											logo =  STATIC_IMG_URL+'professional_account/'+list.account_id+'/notice_board/'+list.logo_url;
										}

										var objAccRres = {
											id            : list.group_id.toString(),
											title         : list.title,
											description   : list.notice,
											image         : logo,
											date          : list.create_date
										};


										counter -= 1;
										array_rows.push(objAccRres);

										if (counter === 0){
											resolve(array_rows);
										}


									});
								}],
								function (result, current) {
									if (counter === 0)
									{  
										var data_result = result[0];
										return_result(data_result); 
									}
								});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getMyNotice",err); return_result(null); }	
};


methods.getAllFollowerSendNoti  =  function (arrParams,return_result){
	
	try
	{
		var arrGroups = [];
		var arrAccounts = [];		

		var query  = "SELECT * FROM jaze_follower_groups WHERE account_id =? ";
		if(arrParams.groups !=""){
 			query  += " AND id IN("+arrParams.groups+")";
		}

		DB.query(query, [arrParams.account_id], function(data, error){
		
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var counter = data.length;		
				var jsonItems = Object.values(data);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {		

						var inQuerry = `SELECT * FROM jaze_follower_groups_meta WHERE group_id IN (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'follow_groups' AND FIND_IN_SET(?,meta_value) )`;
						DB.query(inQuerry, [jsonItems.id], function(indata, error){
		
						 	HelperRestriction.convertMultiMetaArray(indata, function(retConverted){	


								if(retConverted!=null){

									var counterIn = retConverted.length;		
									var jsonItemsIn = Object.values(retConverted);

									promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
										return new Promise(function (resolve, reject) {		
											arrAccounts.push(jsonItemsIn.account_id);
										})
									}],
									function (result, current) {});
								}


								counter -= 1;
								if (counter === 0){ 
									resolve(arrAccounts);
								}
	
							});
					
					    });
				
					})
				}],
				function (result, current) {
					if (counter === 0){ 
						
						var filteredArray = removeDuplicates(result[0]);

						filteredArray.forEach(function(value){

							var sendParams = {account_id:arrParams.account_id};
							methods.getCompanyProfile(sendParams,function(sendAccount){

								if(sendAccount!=null){
									var recParams = {account_id:value};
									methods.getCompanyProfile(recParams,function(recAccount){

										if(recAccount!=null){

											var notiParams = {	

												group_id             : arrParams.group_id.toString(),
												account_id           : sendAccount.account_id.toString(),
												account_type         : sendAccount.account_type.toString(),
												request_account_id   : recAccount.account_id.toString(),
												request_account_type : recAccount.account_type.toString(),
												message              : "",
												flag                 :'19'
											};

											HelperNotification.sendPushNotification(notiParams, function(returnNoti){
												return_result(returnNoti);
											});

										}

									});

								}

							});

						});
						
					}
				});

			}
			else
			{
				return_result(null);
			}
	    });

	}
	catch(err)
	{ console.log("getAllFollowerSendNoti",err); return_result(null); }	
};


methods.updateNotice = function (arrParams,return_result){
	try
	{

	    if(parseInt(arrParams.flag) === 1)
		{
			

			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_profe_notice_details";
  			var lastGrpId = 0;

			methods.getLastGroupId(queGroupMax, function(queGrpRes){

			  	queGrpRes.forEach(function(row) {
					lastGrpId = row.lastGrpId;
			  	});

			  	var full_filename = "";

			  	if(arrParams.file_name !=="" && arrParams.file_ext !==""){
			  		full_filename = arrParams.file_name+'.'+arrParams.file_ext
		  		}

					var arrInsert = {

						account_id   : arrParams.account_id,
						title        : arrParams.title,
						notice       : arrParams.notice,
						logo_url     : full_filename,
						create_date  : newdateForm,
						status       : 1
						
					};	


				
					var plusOneGroupID = lastGrpId+1;


					var notiParams = {account_id:arrParams.account_id, group_id:plusOneGroupID,groups:arrParams.groups};

					//for notification
					
					// methods.getAllFollowerSendNoti(notiParams,function(retFollowers){

					// 	if(parseInt(retFollowers)!==0){
					// 		console.log('notification sent '+retFollowers);
					// 	}

					// });

		  		var querInsert = `insert into jaze_profe_notice_details (group_id, meta_key, meta_value) VALUES (?,?,?)`;

		  		var returnFunction;
			 	for (var key in arrInsert) 
			 	{
					DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
			 		 	if(parseInt(data.affectedRows) > 0){
	 						return_result(1);
				 	   	}else{
		   					return_result(0);
				 	   	}
			 	 	});
			  	};


			});

		}
		else if(parseInt(arrParams.flag) === 2)
		{

			var queLib = `SELECT * FROM jaze_profe_notice_details WHERE group_id=? `;
			DB.query(queLib,[arrParams.id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
 					var full_filename = "";
 					if(arrParams.file_name !=="" && arrParams.file_ext !==""){
 						full_filename =  arrParams.file_name+'.'+arrParams.file_ext;
				  	}

					var arrUpdate = {
				
						title        : arrParams.title,
						notice       : arrParams.notice,
						logo_url     : full_filename,
						create_date  : newdateForm,
					};

					var updateQue = `update jaze_profe_notice_details SET meta_value =? WHERE meta_key =? AND group_id =?`;
					
					for (var key in arrUpdate) 
				 	{	
					     if(arrUpdate[key] !== null && arrUpdate[key] != ""){
							DB.query(updateQue, [arrUpdate[key],key,arrParams.id], function(data, error){
						 		if(data !== null){
					 				return_result(1);
						 	   	}else{
					 	   			return_result(0);
						 	   	}
							});
						}
					}

		 	   	}
		 	   	else
		 	   	{
		 	   		return_result(2);
		 	   	}

	 	 	});
		}
		else if(parseInt(arrParams.flag) === 3)
		{

			var queLib = `SELECT * FROM jaze_profe_notice_details WHERE group_id=? `;
			DB.query(queLib,[arrParams.id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
	 				
 					var queLib = `DELETE FROM jaze_profe_notice_details WHERE group_id=?`;
					DB.query(queLib,[arrParams.id], function(data, error){	
						if(data !== null){
			 				return_result(1);
				 	   	}else{
			 	   			return_result(0);
				 	   	}
					});

		 	   	}
		 	   	else
		 	   	{
		 	   		return_result(2);
		 	   	}

	 	 	});

		}
	}
	catch(err)
	{ console.log("updateNotice",err); return_result(0); }	
};

//------------------------------------------------------------------- Update Notice Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Update Testimonials Module End ---------------------------------------------------------------------------//
methods.getMyTestimonials  =  function (arrParams,return_result){
	
	try
	{
		if(parseInt(arrParams.account_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_profe_testimonial_details WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_profe_testimonial_details WHERE meta_key = 'profe_account_id'  AND meta_value = "+arrParams.account_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_profe_testimonial_details WHERE meta_key = 'status' AND meta_value = 1 )); ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data_result,[function (list) {

									return new Promise(function (resolve, reject) {

										var accParams = {account_id:list.account_id};
										methods.getCompanyProfile(accParams,function(retAccount){

											var objAccRres = {
												id                : list.group_id.toString(),
												account_id : retAccount.account_id,
												name       : retAccount.name,
												logo       : retAccount.logo,
												message   : list.description,
												date   : list.create_date,
												status        : list.status.toString()
											};


											counter -= 1;
											array_rows.push(objAccRres);

											if (counter === 0){
												resolve(array_rows);
											}
										});

									});
								}],
								function (result, current) {
									if (counter === 0)
									{  
										var data_result = result[0];
						

										sortBy(data_result, {
											prop: "create_date",
											desc: true,
											parser: function(item) { return new Date(item); }
										});

										return_result(data_result); 
											

									}
								});	
							}
							else
							{ return_result(null); }
						});
					}
					else
					{
						return_result(null);
					}
			    });
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("getMyTestimonials",err); return_result(null); }	
};


methods.getMyTestimonialsInbox  =  function (arrParams,return_result){
	
	try
	{
		if(parseInt(arrParams.account_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_profe_testimonial_details WHERE  ";
				query += " group_id IN (SELECT group_id FROM jaze_profe_testimonial_details WHERE meta_key = 'profe_account_id'  AND meta_value = "+arrParams.account_id+"  ";
				query += " AND group_id IN (SELECT group_id FROM jaze_profe_testimonial_details WHERE meta_key = 'status' AND meta_value = 0 )) ";
					
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						HelperRestriction.convertMultiMetaArray(data, function(data_result){
						
							var array_rows = [];

							var counter = data_result.length;

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data_result,[function (list) {

									return new Promise(function (resolve, reject) {

										var accParams = {account_id:list.account_id};
										methods.getCompanyProfile(accParams,function(retAccount){

											var objAccRres = {
													id                : list.group_id.toString(),
												account_id : retAccount.account_id,
												name       : retAccount.name,
												logo       : retAccount.logo,
												message   : list.description,
												date   : list.create_date,
												status        : list.status.toString()
											};


											counter -= 1;
											array_rows.push(objAccRres);

											if (counter === 0){
												resolve(array_rows);
											}
										});

									});
								}],
								function (result, current) {
									if (counter === 0)
									{  
										var data_result = result[0];

										sortBy(data_result, {
											prop: "create_date",
											desc: true,
											parser: function(item) { return new Date(item); }
										});

										return_result(data_result); 
									}
								});	
							}
							else
							{ return_result([]); }
						});
					}
					else
					{
						return_result([]);
					}
			    });
		}
		else
		{
			return_result([]);
		}
	}
	catch(err)
	{ console.log("getMyTestimonialsInbox",err); return_result([]); }	
};


methods.updateTestimonials = function (arrParams,return_result){
	try
	{
		
		if(parseInt(arrParams.flag) === 2)
		{

			var queLib = `SELECT * FROM jaze_profe_testimonial_details WHERE group_id=? `;
			DB.query(queLib,[arrParams.id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
	 				
	 				var updateQue = `update jaze_profe_testimonial_details SET meta_value =? WHERE meta_key ='status' AND group_id =?`;

					if(parseInt(arrParams.status) == 1 || parseInt(arrParams.status) == 2){

						DB.query(updateQue, [arrParams.status,data[0].group_id], function(data, error){
					 		if(data !== null){
				 				return_result(1);
					 	   	}else{
				 	   			return_result(0);
					 	   	}
						});

					}else{
							return_result(0);
					}
					
		 	   	}
		 	   	else
		 	   	{
		 	   		return_result(2);
		 	   	}

	 	 	});
		}
		else if(parseInt(arrParams.flag) === 3)
		{

			var queLib = `SELECT * FROM jaze_profe_testimonial_details WHERE group_id=? `;
			DB.query(queLib,[arrParams.id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
	 				
 					var querDel = `DELETE FROM jaze_profe_testimonial_details WHERE group_id=?`;
					DB.query(querDel,[arrParams.id], function(data, error){	
						if(data !== null){
			 				return_result(1);
				 	   	}else{
			 	   			return_result(0);
				 	   	}
					});

		 	   	}
		 	   	else
		 	   	{
		 	   		return_result(2);
		 	   	}

	 	 	});

		}
	}
	catch(err)
	{ console.log("updateTestimonials",err); return_result(0); }	
};
//------------------------------------------------------------------- Update Testimonials Module End ---------------------------------------------------------------------------//



methods.getProfessionalFollowersList  =  function (reqParams,return_result){
	
	try
	{

		var query  = " SELECT * FROM jaze_follower_groups WHERE status = 1 and account_id = "+reqParams.account_id+" AND group_type = '1' ";
		DB.query(query, null, function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				var array_rows = [];

				var counter = data.length;

				if(parseInt(counter) !== 0)
				{
					promiseForeach.each(data,[function (list) {

						return new Promise(function (resolve, reject) {
							var followParams = {account_id:reqParams.account_id, id:list.id};
							methods.getFollowersGroupCount(followParams,function(retCount){	
				
								var objAccRres = {
									id				: list.id.toString(),
									name		  	: list.group_name.toString(),
									count 	        : retCount.toString()
								};

								array_rows.push(objAccRres);

								resolve(array_rows);

								counter -= 1;

							});
						});
					}],
					function (result, current) {
						if (counter === 0)
						{ 
							var sortedGroups = result[0].sort(function(a, b) {
								    return a.count < b.count ? 1 : a.count > b.count ? -1 : 0;
							});

							var query2  = " SELECT * FROM jaze_follower_groups WHERE status = 1 and account_id = "+reqParams.account_id+" AND group_type = '2' ";
							DB.query(query2, null, function(data2, error){
								
								if(typeof data2 !== 'undefined' && data2 !== null && parseInt(data2.length) > 0){

									var array_rows2 = [];

									var counter2 = data2.length;

									if(parseInt(counter2) !== 0)
									{
										promiseForeach.each(data2,[function (list2) {

											return new Promise(function (resolve, reject) {
												var followParams2 = {account_id:reqParams.account_id, id:list2.id};
												methods.getFollowersGroupCount(followParams2,function(retCount2){	
									
													var objAccRres2 = {
														id				: list2.id.toString(),
														name		  	: list2.group_name.toString(),
														count 	        : retCount2.toString()
													};

													array_rows2.push(objAccRres2);

													resolve(array_rows2);

													counter2 -= 1;

												});
											});
										}],
										function (result2, current) {
											if (counter2 === 0)
											{ 

												var sortedSpecial = result2[0].sort(function(a, b) {
													    return a.count < b.count ? 1 : a.count > b.count ? -1 : 0;
												});

												var followers_list = {
													groups   :  sortedGroups,
													special  :  sortedSpecial
												}
												return_result(followers_list);

										 	}
										});	
									}
									else
									{ 
										var followers_list = {
											groups   :  sortedGroups,
											special  :  []
										}
										return_result(followers_list);
									}
								
								}
								else
								{
									var followers_list = {
										groups   :  sortedGroups,
										special  :  []
									}
									return_result(followers_list);
								}
						    });

					 	}
					});	
				}
				else
				{ 
					var query2  = " SELECT * FROM jaze_follower_groups WHERE status = 1 and account_id = "+reqParams.account_id+" AND group_type = '2' ";
					DB.query(query2, null, function(data2, error){
						
						if(typeof data2 !== 'undefined' && data2 !== null && parseInt(data2.length) > 0){

							var array_rows2 = [];

							var counter2 = data2.length;

							if(parseInt(counter2) !== 0)
							{
								promiseForeach.each(data2,[function (list2) {

									return new Promise(function (resolve, reject) {
										var followParams2 = {account_id:reqParams.account_id, id:list2.id};
										methods.getFollowersGroupCount(followParams2,function(retCount2){	
							
											var objAccRres2 = {
												id				: list2.id.toString(),
												name		  	: list2.group_name.toString(),
												count 	        : retCount2.toString()
											};

											array_rows2.push(objAccRres2);

											resolve(array_rows2);

											counter2 -= 1;

										});
									});
								}],
								function (result2, current) {
									if (counter2 === 0)
									{ 
										var sortedSpecial = result2[0].sort(function(a, b) {
										    return a.count < b.count ? 1 : a.count > b.count ? -1 : 0;
										});

										var followers_list = {
											groups   :  [],
											special  :  sortedSpecial
										}
										return_result(followers_list);

								 	}
								});	
							}
							else
							{ 
								var followers_list = {
									groups   :  [],
									special  :  []
								}
								return_result(followers_list);
							}
						
						}
						else
						{
							var followers_list = {
								groups   : [],
								special  : [],
							}
							return_result(followers_list);
						}
				    });
				}
	
			}
			else
			{
				var query2  = " SELECT * FROM jaze_follower_groups WHERE status = 1 and account_id = "+reqParams.account_id+" AND group_type = '2' ";
				DB.query(query2, null, function(data2, error){
					
					if(typeof data2 !== 'undefined' && data2 !== null && parseInt(data2.length) > 0){

						var array_rows2 = [];

						var counter2 = data2.length;

						if(parseInt(counter2) !== 0)
						{
							promiseForeach.each(data2,[function (list2) {

								return new Promise(function (resolve, reject) {
									var followParams2 = {account_id:reqParams.account_id, id:list2.id};
									methods.getFollowersGroupCount(followParams2,function(retCount2){	
						
										var objAccRres2 = {
											id				: list2.id.toString(),
											name		  	: list2.group_name.toString(),
											count 	        : retCount2.toString()
										};

										array_rows2.push(objAccRres2);

										resolve(array_rows2);

										counter2 -= 1;

									});
								});
							}],
							function (result2, current) {
								if (counter2 === 0)
								{ 
									var sortedSpecial = result2[0].sort(function(a, b) {
									    return a.count < b.count ? 1 : a.count > b.count ? -1 : 0;
									});

									var followers_list = {
										groups   :  [],
										special  :  sortedSpecial
									}
									return_result(followers_list);

							 	}
							});	
						}
						else
						{ 
							var followers_list = {
								groups   :  [],
								special  :  []
							}
							return_result(followers_list);
						}
					
					}
					else
					{
						var followers_list = {
							groups   : [],
							special  : []
						}
						return_result(followers_list);
					}
			    });
			}
	    });

	}
	catch(err)
	{ console.log("getCompanyFollowList",err); return_result(null); }	
};


methods.getFollowersGroupCount = function(arrParams,return_result){


	var quer = `SELECT COUNT(id) AS total FROM jaze_follower_groups_meta 
		WHERE group_id IN (SELECT group_id  FROM jaze_follower_groups_meta WHERE meta_key= 'follow_account_id' AND meta_value =?)
		AND group_id IN  (SELECT group_id FROM jaze_follower_groups_meta WHERE meta_key = 'follow_groups' AND FIND_IN_SET(?,meta_value)>0 ) GROUP BY group_id`;

	DB.query(quer,[arrParams.account_id,arrParams.id], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){     	       
          	return_result(data.length); 
        }else{ 
        	return_result(0);
        }

	});
};

methods.addGroups = function(arrParams,return_result){

	var querInsertGroups = `insert into jaze_follower_groups (account_id, group_name, group_type, status) VALUES (?,?,?,1)`;

	DB.query(querInsertGroups,[arrParams.account_id,arrParams.title,arrParams.type], function(data, error){

	 	if(parseInt(data.affectedRows) > 0){
			return_result(1);
 	   	}else{
			return_result(0);
 	   	}

	});

};


methods.deleteGroups = function(arrParams,return_result){


	var updateQue = "update jaze_follower_groups SET status ='0' WHERE id IN("+arrParams.id+")";

	DB.query(updateQue,[arrParams.id], function(data, error){

	 	if(data!=null){
			return_result(1);
 	   	}else{
			return_result(0);
 	   	}

	});

};


//------------------------------------------------------------------- Professional communication start  ---------------------------------------------------------------------------//

methods.updateProfessionalCommunication = function (arrParams,return_result){
	try
	{

		var query = `SELECT * FROM jaze_user_jazecome_status WHERE 
		group_id IN (SELECT group_id FROM jaze_user_jazecome_status WHERE meta_key = 'account_id' AND meta_value =? )`;
		DB.query(query,[arrParams.prof_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var	arrUpdate = {
					jaze_diary : arrParams.jaze_diary,
					jaze_feed  : arrParams.jaze_feed,
					jaze_call  : arrParams.jaze_call,
					jaze_chat  : arrParams.jaze_chat,
					jaze_mail  : arrParams.jaze_mail,
				};

				var updateQue = `update jaze_user_jazecome_status SET meta_value =? WHERE meta_key =? AND group_id =? `;
				
				for (var key in arrUpdate) 
			 	{	
				     if(arrUpdate[key] !== null && arrUpdate[key] != ""){
						DB.query(updateQue, [arrUpdate[key],key,data[0].group_id], function(datau, error){
					 		if(datau !== null){
				 				return_result(1);
					 	   	}else{
				 	   			return_result(0);
					 	   	}
						});
					}
				}	

			}
			else
			{



				var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_user_jazecome_status";
	  			var lastGrpId = 0;

				methods.getLastGroupId(queGroupMax, function(queGrpRes){

				  	queGrpRes.forEach(function(row) {
						lastGrpId = row.lastGrpId;
				  	});

					var arrInsert = {
						account_id : arrParams.company_id,
						jaze_diary : arrParams.jaze_diary,
						jaze_feed  : arrParams.jaze_feed,
						jaze_call  : arrParams.jaze_call,
						jaze_chat  : arrParams.jaze_chat,
						jaze_mail  : arrParams.jaze_mail,
					};
				
					var plusOneGroupID = lastGrpId+1;
			  		var querInsert = `insert into jaze_user_jazecome_status (group_id, meta_key, meta_value) VALUES (?,?,?)`;

			  		var returnFunction;
				 	for (var key in arrInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
				 		 	if(parseInt(data.affectedRows) > 0){
		 						return_result(1);
					 	   	}else{
			   					return_result(0);
					 	   	}
				 	 	});
				  	};

				});

			}

		});

	
	}
	catch(err)
	{ console.log("updateProfessionalCommunication",err); return_result(obj_return); }	
};


methods.getProfessionalCommunication = function (arrParams,return_result){
	try
	{


		var obj_return = {
			jaze_diary : '0',
			jaze_feed  : '0',
			jaze_call  : '0',
			jaze_chat  : '0',
			jaze_mail  : '0'
		}


		var query = `SELECT * FROM jaze_user_jazecome_status WHERE 
		group_id IN (SELECT group_id FROM jaze_user_jazecome_status WHERE meta_key = 'account_id' AND meta_value =? )`;

		DB.query(query,[arrParams.prof_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				HelperRestriction.convertMultiMetaArray(data,function(retConverted){

	
					if(retConverted[0].jaze_diary){
						obj_return.jaze_diary = retConverted[0].jaze_diary.toString();
					}
					if(retConverted[0].jaze_feed){
						obj_return.jaze_feed = retConverted[0].jaze_feed.toString();
					}
					if(retConverted[0].jaze_call){
						obj_return.jaze_call = retConverted[0].jaze_call.toString();
					}
					if(retConverted[0].jaze_chat){
						obj_return.jaze_chat = retConverted[0].jaze_chat.toString();
					}
					if(retConverted[0].jaze_mail){
						obj_return.jaze_mail = retConverted[0].jaze_mail.toString();
					}


					return_result(obj_return);

				});
				
			
			}
			else
			{

				return_result(obj_return);

				
			}

		});
	
	}
	catch(err)
	{ console.log("getProfessionalCommunication",err); return_result(obj_return); }	
};

//------------------------------------------------------------------- Professional communication end  ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Professional signup start   ---------------------------------------------------------------------------//



methods.professionalSignup =  function(arrParams,return_result){
	try
	{
		var query = `SELECT * FROM jaze_user_account_details WHERE user_id =?`;
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				var updateQue = `update jaze_user_account_details SET prof_id =? WHERE user_id =?`;
				DB.query(updateQue, [arrParams.new_account_id,arrParams.account_id], function(datau, error){
			 		if(datau !== null){
		 				
						var	arrInsert = {

							professional_title_id    : arrParams.title_id,
							fname                    : arrParams.first_name,
							lname                    : arrParams.last_name,
							email                    : arrParams.email,
							dob                      : formatDate(arrParams.dob),
							profession               : arrParams.profession,

							nationality_id           : arrParams.nationality_id,
							mobile_country_code_id   : arrParams.mobile_country_code,
							mobile                   : arrParams.mobile_number,
							mobile_country_code_id_2 : arrParams.mobile_country_code_2,
							mobile_2                 : arrParams.mobile_number_2,

							country_id               : arrParams.country_id,
							state_id                 : arrParams.state_id,
							city_id                  : arrParams.city_id,
							area_id                  : arrParams.area_id,
				
							street_name              : arrParams.street_name.toString(),
							street_no                : arrParams.street_no.toString(),
							building_name            : arrParams.building_name.toString(),
							building_no              : arrParams.building_no.toString(),
							professional_title_id    : '',
							job_title                : '',
							professional_logo        : '',
							professional_about       : standardDT,
							status                   : '0',

						};

						var querInsert = `insert into jaze_user_basic_details (account_id,account_type,meta_key, meta_value) VALUES (?,?,?,?)`;

					 	for (var key in arrInsert) 
					 	{
							DB.query(querInsert,[arrParams.new_account_id,'2',key,arrInsert[key]], function(data, error){
					 		 	if(parseInt(data.affectedRows) > 0){
			 						return_result(1);
						 	   	}else{
				   					return_result(0);
						 	   	}
					 	 	});
					  	};

			 	   	}else{
		 	   			return_result(0);
			 	   	}
				});

			}

 		});

	}
	catch(err)
	{ console.log("professionalSignup",err); return_result(0);}
};



methods.professionalSignupJazemail =  function(arrParams,return_result){
	try
	{
		var query = `SELECT * FROM jaze_user_credential_details WHERE jazemail_id =?`;
		DB.query(query,[arrParams.jazemail_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	
				//already exist
				return_result('account already exist!.');
			}
			else
			{
				var queryQ = `SELECT * FROM jaze_user_basic_details WHERE account_id =?`;
				DB.query(queryQ,[arrParams.prof_id], function(dataQ, error){
					if(typeof dataQ !== 'undefined' && dataQ !== null && parseInt(dataQ.length) > 0)
					{	

						var updateQue = `update jaze_user_basic_details SET meta_value ='1' WHERE meta_key ='status' AND account_id =?`;
						DB.query(updateQue, [arrParams.prof_id], function(datau, error){
					 		if(datau !== null)
					 		{
				 				
				 				if(arrParams.password == arrParams.confirm_password )
				 				{	
				 					if(arrParams.security_pin == arrParams.confirm_security_pin )
				 					{

		 								var arrValues = [arrParams.prof_id,'2',arrParams.security_pin,arrParams.jazenet_id,arrParams.password,'1'];
								  		var querInsert = `insert into jaze_user_credential_details (account_id,account_type,jaze_security_pin,jazemail_id,password,status) VALUES (?)`;

							  			DB.query(querInsert,[arrValues], function(dataIn, error){
	
							 		 		if(datau !== null)
							 		 		{
						 						return_result(1);
									 	   	}
									 	   	else
									 	   	{
							   					return_result(0);
									 	   	}

								 	 	});

			 						}
			 						else
			 						{
			 							// confirm security is not match
			 							return_result('security pin and confirm security pin is not match!.');
			 						}
				 				}
				 				else
				 				{
				 					// confirm password is not match
				 					return_result('password and confirm password is not match!.');
				 				}

					 	   	}
					 	   	else
					 	   	{
					 	   		// unable to process request pls try again
				 	   			return_result(0);
					 	   	}
						});

					}
					else
					{
						return_result('account not found!.');
					}

				});

			}
 		});

	}
	catch(err)
	{ console.log("professionalSignupJazemail",err); return_result(0);}
};

//------------------------------------------------------------------- Professional signup end   ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Professional jazenet_id start   ---------------------------------------------------------------------------//

methods.progessionalJaznetID =  function(arrParams,return_result){
	try
	{
		var query = `SELECT * FROM jaze_user_jazenet_id WHERE account_id =?`;
		DB.query(query,[arrParams.prof_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	
				//already exist
				return_result(2);
			}
			else
			{
				var queryQ = `SELECT * FROM jaze_user_basic_details WHERE account_id =? and account_type`;
				DB.query(queryQ,[arrParams.prof_id,arrParams.prof_type], function(dataQ, error){
					if(typeof dataQ !== 'undefined' && dataQ !== null && parseInt(dataQ.length) > 0)
					{	
						methods.convertMultiMetaArrayCompany(dataQ,function(retConverted){

					
							methods.__getCountryShortCode(retConverted[0].country_id,function(country){
										
								var country = country.toUpperCase();
								var f_name  = retConverted[0].fname.trim().toLowerCase().replace(/[^A-Za-z0-9\-]/," ");
								var l_name  = retConverted[0].lname.trim().toLowerCase().replace(/[^A-Za-z0-9\-]/," ");
								var dob     = dateFormat(removeText(retConverted[0].dob),'yyyy-mm-dd');
								var new_dob = dateFormat(dob, 'dd-mm-yy').replace(/\-/g,"");

								var con_f_name = f_name.substr(0, 3).toUpperCase();
								var con_l_name = l_name.substr(0, 3).toUpperCase();

								var obj = {
									fname  : con_f_name,
									lname  : con_l_name,
									fname_lname : con_f_name+con_l_name,
									dob      : new_dob,
									country  : country
								}

								var jazenet_id = "P1"+new_dob+con_f_name+con_l_name;
								var unique_id  = "AB01";

								//return_result(obj);

								var check = `SELECT * FROM jaze_user_jazenet_id WHERE jazenet_id =?`;
								DB.query(check,[jazenet_id], function(c_data, error){

									if(typeof c_data !== 'undefined' && c_data !== null && parseInt(c_data.length) > 0)
									{
										var uni_str = "AB01";
										var add = parseInt(c_data.length)+1;
										unique_id  = uni_str.slice(0, 2)+"0"+add;
									}


									var new_unique_id = unique_id;
									var new_jazenet_id = jazenet_id+new_unique_id;
									
									var querInsert = `insert into jaze_user_jazenet_id (account_id, category, jazenet_id, unique_id, status) 
														VALUES (?,?,?,?,?)`;

							  		DB.query(querInsert,[arrParams.prof_id,'1',jazenet_id,new_unique_id,'1'], function(data, error){
							 		 	if(parseInt(data.affectedRows) > 0)
							 		 	{
					 						
				 		 			// 		var i,s,e;
											// var arr = [];
											
											// for (i = 0; i < 7; i++) {

		 								// 		s = parseInt(i) * 2;
		 								// 		e = 2;

								 		// 		if ( parseInt(i) === 5) {
								   //                  s = 10;
								   //                  e = 6;
								   //              }
								   //              else if ( parseInt(i) === 6) 
								   //              {
								   //                  s = 16;
								   //                  e = 4;
								   //              }

								   //  			arr[i] = new_jazenet_id.substr(s, e);
											// }


											return_result(new_jazenet_id);

								 	   	}
								 	   	else
								 	   	{
						   					return_result(0);
								 	   	}
							 	 	});


								});


							});

						});
					}
					else
					{
						return_result(3);
					}

				});

			}
 		});

	}
	catch(err)
	{ console.log("professionalSignupJazemail",err); return_result(0);}
};

//------------------------------------------------------------------- Professional jazenet_id end   ---------------------------------------------------------------------------//


//------------------------------------------------------------------- General Module Start ---------------------------------------------------------------------------//


var sortBy = (function () {
	var toString = Object.prototype.toString,
		// default parser function
		parse = function (x) { return x; },
		// gets the item to be sorted
		getItem = function (x) {
		  var isObject = x != null && typeof x === "object";
		  var isProp = isObject && this.prop in x;
		  return this.parser(isProp ? x[this.prop] : x);
		};
		
	/**
	 * Sorts an array of elements.
	 *
	 * @param {Array} array: the collection to sort
	 * @param {Object} cfg: the configuration options
	 * @property {String}   cfg.prop: property name (if it is an Array of objects)
	 * @property {Boolean}  cfg.desc: determines whether the sort is descending
	 * @property {Function} cfg.parser: function to parse the items to expected type
	 * @return {Array}
	 */
	return function sortby (array, cfg) {
	  if (!(array instanceof Array && array.length)) return [];
	  if (toString.call(cfg) !== "[object Object]") cfg = {};
	  if (typeof cfg.parser !== "function") cfg.parser = parse;
	  cfg.desc = !!cfg.desc ? -1 : 1;
	  return array.sort(function (a, b) {
		a = getItem.call(cfg, a);
		b = getItem.call(cfg, b);
		return cfg.desc * (a < b ? -1 : +(a > b));
	  });
	};
	
  }());

   	/*
	 * split values push to array
	 */
	function convert_array(array_val) {

		var list = [];

		if(typeof array_val !== 'undefined' && array_val !== null && array_val !== "")
		{
			if(array_val.toString().indexOf(',') != -1 ){
				list = array_val.split(',');
			}
			else {
				list.push(array_val);
			}
		}
		
		return list;
	}


methods.convertMultiMetaArrayCompany =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndexCompany(list,arrvalues[i].account_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndexCompany(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].account_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}

methods.__getNationality = function(arrParams,return_result){

	try{

		if(parseInt(arrParams) !==0){

			let query  = " SELECT country_name FROM country  WHERE id =? ";
			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
			
		}else{
			return_result('');
		}
	
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getStateDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT state_name FROM states  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].state_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getCityDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT city_name FROM cities  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].city_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getAreaDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT area_name FROM area  WHERE area_id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].area_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

function removeText(value){

	var text = value;

	return text.replace(/th/g, '');

}

function formatDate(current_datetime) {
	var dt = new Date(current_datetime);
	const months = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
	let formatted_date = dt.getDate()+"th "+ months[dt.getMonth()]+" "+dt.getFullYear();
	return formatted_date;
}

function formatDateName(month) {
	var number = month - 1;
	const months = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
  	return months[number];
}


methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
};


function removeDuplicates(array) {
  return array.filter((a, b) => array.indexOf(a) === b)
};


//------------------------------------------------------------------- General Module End ---------------------------------------------------------------------------//



module.exports = methods;