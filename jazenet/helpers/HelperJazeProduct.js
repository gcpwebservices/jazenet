const methods = {};
const DB = require('../../helpers/db_base.js');
const HelperGeneralJazenet = require('../helpers/HelperGeneralJazenet.js');

const promiseForeach = require('promise-foreach');

const STATIC_IMG_URL = G_STATIC_IMG_URL+"uploads/jazenet/category/jazestore_attributes/"; 


// const express = require('express');
// const router = express.Router();
methods.getAttributeParent = function(arrParams,return_result){

	try
	{
		var arrRows = [];
		var newObj = {};
		var status = '1';
		var category_id = '57';

		if(parseInt(arrParams.category_id) !== 0){
			category_id = arrParams.category_id;
		}

		var quer = `SELECT t1.* FROM(
				SELECT group_id,
					(CASE WHEN meta_key = 'parent_id' THEN meta_value END) AS parent_id,
					(SELECT c.meta_value FROM jaze_jazestore_category_list c WHERE c.meta_key = 'sort_order' AND c.group_id = jaze_jazestore_category_list.group_id) AS sort_order
				FROM  jaze_jazestore_category_list
				WHERE group_id =? ) AS t1 WHERE parent_id != ''`;


		DB.query(quer,[category_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				var querParent = `SELECT * FROM jaze_jazestore_category_list WHERE group_id IN (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key = 'parent_id' AND meta_value =? ) `;

				DB.query(querParent,[data[0].parent_id], function(pdata, error){


					if(typeof pdata !== 'undefined' && pdata !== null && parseInt(pdata.length) > 0)
					{	

						methods.convertMultiMetaArray(pdata, function(retConverted){	

							var counter = retConverted.length;		
							var jsonItems = Object.values(retConverted);

							promiseForeach.each(jsonItems,[function (jsonItems) {
								return new Promise(function (resolve, reject) {		
									
							
									if(jsonItems.group_id){

										var status = '1';
										if(parseInt(jsonItems.group_id) !== parseInt(category_id) ){
											status = '0';
										}

										newObj = {
											title         : jsonItems.cate_name,
											active_status : status,
											category_id   : jsonItems.group_id.toString()
										};


										arrRows.push(newObj);
									
									}

									counter -= 1;
									if (counter === 0){ 
										resolve(arrRows);
									}
								})
							}],
							function (result, current) {
								if (counter === 0){ 
									return_result(result[0]);
								}
							});

						});

					}
					else
					{
						return_result(null);	
					}

				});
	
	        }
	        else
	        { 
	        	return_result(null);
	        }

		});

	}
	catch(err)
	{ console.log("getFavoritesListAllDetails",err); return_result(null); }	

};

methods.getCategoryAttributes = function(arrParams,return_result){


	try
	{
		var mainArr = [];
		var objController = {};
		var category_id = '57';
		var sort_order;
		var trigger = 0;
		var object_name;

		if(parseInt(arrParams.category_id) !== 0){
			category_id = arrParams.category_id;
		}
			var quer = `SELECT * FROM jaze_jazestore_attributes_groups WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups WHERE meta_key = 'cat_id' AND( meta_value =? or meta_value ='56') ) `;
			quer +=	` AND group_id IN (SELECT group_id FROM  jaze_jazestore_attributes_groups  WHERE meta_key = 'status' AND meta_value = '1' )`;

		DB.query(quer,[category_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){	

					if(retConverted)
					{
						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (val) {
							return new Promise(function (resolve, reject) {		
								
						
								if(parseInt(category_id) == parseInt(val.cat_id)){
									trigger = 1;
				  				}

	  							methods.getCategoryAttributeValue(val.group_id,val.input_type, function(retMainArr){ 

									if(val.input_type === "multiselect_image_button"){

										controller_type = '1';
										attribute_option_labe = 'all';

									 	objController = {
											controller_type:'1',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_value_list:retMainArr
										};
						
									}else if(val.input_type === "multiselect_dropdown"){
										controller_type = '2';
										attribute_option_labe = 'all';

									 	objController = {
											controller_type:'2',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_value_list:retMainArr
										};

							
									}else if(val.input_type === "color_picker"){
										controller_type = '3';
										attribute_option_labe = 'all';

									 	objController = {
											controller_type:'3',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_color_list:retMainArr
										};
						
									}else if(val.input_type === "image_button"){
										controller_type = '4';
										attribute_option_labe = 'all';

									 	objController = {
											controller_type:'4',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_value_list:retMainArr
										};

							
									}else if(val.input_type === "dropdown"){
										controller_type = '5';
										attribute_option_labe = 'all';

									 	objController = {
											controller_type:'5',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_value_list:retMainArr
										};
							
									}else if(val.input_type === "radio_button"){
										controller_type = '6';

									 	objController = {
											controller_type:'6',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'',
										 	items_sing_value_list:retMainArr
										};
					
									}else if(val.input_type === "select_range"){
										controller_type = '7';
									 	objController = {
											controller_type:'7',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'',
										 	items_select_range_value:retMainArr
										};
					
									}else if(val.input_type === "input_range"){
										controller_type = '8';
										attribute_option_labe = 'all';
									 	objController = {
											controller_type:'8',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_input_range_value:retMainArr
										};
								
									}else if(val.input_type === "text"){
										controller_type = '9';
									 	objController = {
											controller_type:'9',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'',
										 	items_edittext_value:retMainArr
										};
							
									}else if(val.input_type === "button"){
										controller_type = '10';
									 	objController = {
											controller_type:'10',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'',
										 	items_sing_value_list:retMainArr
										};
							
									}

									mainArr.push(objController);

									counter -= 1;
									if (counter === 0){ 
										resolve(mainArr);
									}

								});


								
							})
						}],
						function (result, current) {
							if (counter === 0){ 

								if(trigger !==0){
									var sortedArray = result[0].sort(function(a, b) {
									    return a.sort_order > b.sort_order ? 1 : a.sort_order < b.sort_order ? -1 : 0;
									});

									return_result(sortedArray);
								}else{
									return_result([]);
								}

						
							}
						});


					}


				});
	        }
	        else
	        { 
	        	return_result([]);
	        }

		});

	}
	catch(err)
	{ console.log("getCategoryAttributes",err); return_result(null); }	
};
// methods.getAttributeParent = function(arrParams,return_result){

// 	try
// 	{
// 		var arrRows = [];
// 		var newObj = {};
// 		var status = '1';
// 		var category_id = '57';

// 		if(parseInt(arrParams.category_id) !== 0){
// 			category_id = arrParams.category_id;
// 		}

// 		var quer = `SELECT t1.* FROM(
// 				SELECT group_id,
// 					(CASE WHEN meta_key = 'parent_id' THEN meta_value END) AS parent_id,
// 					(SELECT c.meta_value FROM jaze_jazestore_category_list c WHERE c.meta_key = 'sort_order' AND c.group_id = jaze_jazestore_category_list.group_id) AS sort_order
// 				FROM  jaze_jazestore_category_list
// 				WHERE group_id =? ) AS t1 WHERE parent_id != ''`;


// 		DB.query(quer,[category_id], function(data, error){

// 			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

// 				var querParent = `SELECT * FROM jaze_jazestore_category_list WHERE group_id IN (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key = 'parent_id' AND meta_value =? ) `;
// 				DB.query(querParent,[data[0].parent_id], function(pdata, error){

// 					if(typeof pdata !== 'undefined' && pdata !== null && parseInt(pdata.length) > 0){



// 						methods.convertMultiMetaArray(pdata, function(retConverted){
		
// 							const forEach = async () => {
// 							  	const values = retConverted;
// 							  	for (const val of values){
// 									var status = '1';
// 									if(val.group_id !== parseInt(category_id) ){
// 										status = '0';
// 									}

// 									if(parseInt(val.group_id) !==0){
// 										newObj = {
// 											title: val.cate_name,
// 											active_status: status,
// 											category_id: val.group_id.toString()
// 										};
// 										arrRows.push(newObj);
// 									}
							
// 							  	}

// 						  		const result = await returnData(arrRows);
// 							}
							 
// 							const returnData = x => {
// 							  return new Promise((resolve, reject) => {
// 							    setTimeout(() => {
// 							      resolve(x);
// 							    }, 300);
// 							  });
// 							}
							 
// 							forEach().then(() => {
// 								return_result(arrRows);
// 							})
	
// 						});
// 					}

// 				});
	
// 	        }
// 	        else
// 	        { 
// 	        	return_result(null);
// 	        }

// 		});

// 	}
// 	catch(err)
// 	{ console.log("getFavoritesListAllDetails",err); return_result(null); }	

// };

// methods.getCategoryAttributes = function(arrParams,return_result){


// 	try
// 	{
// 		var mainArr = [];
// 		var objController = {};
// 		var category_id = '57';
// 		var sort_order;
// 		var trigger = 0;
// 		var object_name;

// 		if(parseInt(arrParams.category_id) !== 0){
// 			category_id = arrParams.category_id;
// 		}
// 			var quer = `SELECT * FROM jaze_jazestore_attributes_groups WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups WHERE meta_key = 'cat_id' AND( meta_value =? or meta_value ='56') ) `;
// 			quer +=	` AND group_id IN (SELECT group_id FROM  jaze_jazestore_attributes_groups  WHERE meta_key = 'status' AND meta_value = '1' )`;

// 		DB.query(quer,[category_id], function(data, error){

// 			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

// 				methods.convertMultiMetaArray(data, function(retConverted){	

// 					const forEach = async () => {
// 					  	const values = data;
// 					  	for (const val of retConverted){
								
//   							if(parseInt(category_id) == parseInt(val.cat_id)){
// 								trigger = 1;
// 			  				}
		  		
// 				  			methods.getCategoryAttributeValue(val.group_id,val.input_type, function(retMainArr){ 

// 								if(val.input_type === "multiselect_image_button"){

// 									controller_type = '1';
// 									attribute_option_labe = 'all';

// 								 	objController = {
// 										controller_type:'1',
// 										attribute_id:val.group_id.toString(),
// 										attribute_label:val.label_name,
// 										home_status:val.is_details_page.toString(),
// 									 	home_sort_order:val.details_sort_order.toString(),
// 									 	adv_status:val.is_listing_page.toString(),
// 									 	adv_sort_order:val.listing_sort_order.toString(),
// 									 	attribute_option_label:'all',
// 									 	items_value_list:retMainArr
// 									};
					
// 								}else if(val.input_type === "multiselect_dropdown"){
// 									controller_type = '2';
// 									attribute_option_labe = 'all';

// 								 	objController = {
// 										controller_type:'2',
// 										attribute_id:val.group_id.toString(),
// 										attribute_label:val.label_name,
// 										home_status:val.is_details_page.toString(),
// 									 	home_sort_order:val.details_sort_order.toString(),
// 									 	adv_status:val.is_listing_page.toString(),
// 									 	adv_sort_order:val.listing_sort_order.toString(),
// 									 	attribute_option_label:'all',
// 									 	items_value_list:retMainArr
// 									};

						
// 								}else if(val.input_type === "color_picker"){
// 									controller_type = '3';
// 									attribute_option_labe = 'all';

// 								 	objController = {
// 										controller_type:'3',
// 										attribute_id:val.group_id.toString(),
// 										attribute_label:val.label_name,
// 										home_status:val.is_details_page.toString(),
// 									 	home_sort_order:val.details_sort_order.toString(),
// 									 	adv_status:val.is_listing_page.toString(),
// 									 	adv_sort_order:val.listing_sort_order.toString(),
// 									 	attribute_option_label:'all',
// 									 	items_color_list:retMainArr
// 									};
					
// 								}else if(val.input_type === "image_button"){
// 									controller_type = '4';
// 									attribute_option_labe = 'all';

// 								 	objController = {
// 										controller_type:'4',
// 										attribute_id:val.group_id.toString(),
// 										attribute_label:val.label_name,
// 										home_status:val.is_details_page.toString(),
// 									 	home_sort_order:val.details_sort_order.toString(),
// 									 	adv_status:val.is_listing_page.toString(),
// 									 	adv_sort_order:val.listing_sort_order.toString(),
// 									 	attribute_option_label:'all',
// 									 	items_value_list:retMainArr
// 									};

						
// 								}else if(val.input_type === "dropdown"){
// 									controller_type = '5';
// 									attribute_option_labe = 'all';

// 								 	objController = {
// 										controller_type:'5',
// 										attribute_id:val.group_id.toString(),
// 										attribute_label:val.label_name,
// 										home_status:val.is_details_page.toString(),
// 									 	home_sort_order:val.details_sort_order.toString(),
// 									 	adv_status:val.is_listing_page.toString(),
// 									 	adv_sort_order:val.listing_sort_order.toString(),
// 									 	attribute_option_label:'all',
// 									 	items_value_list:retMainArr
// 									};
						
// 								}else if(val.input_type === "radio_button"){
// 									controller_type = '6';

// 								 	objController = {
// 										controller_type:'6',
// 										attribute_id:val.group_id.toString(),
// 										attribute_label:val.label_name,
// 										home_status:val.is_details_page.toString(),
// 									 	home_sort_order:val.details_sort_order.toString(),
// 									 	adv_status:val.is_listing_page.toString(),
// 									 	adv_sort_order:val.listing_sort_order.toString(),
// 									 	attribute_option_label:'',
// 									 	items_sing_value_list:retMainArr
// 									};
				
// 								}else if(val.input_type === "select_range"){
// 									controller_type = '7';
// 								 	objController = {
// 										controller_type:'7',
// 										attribute_id:val.group_id.toString(),
// 										attribute_label:val.label_name,
// 										home_status:val.is_details_page.toString(),
// 									 	home_sort_order:val.details_sort_order.toString(),
// 									 	adv_status:val.is_listing_page.toString(),
// 									 	adv_sort_order:val.listing_sort_order.toString(),
// 									 	attribute_option_label:'',
// 									 	items_select_range_value:retMainArr
// 									};
				
// 								}else if(val.input_type === "input_range"){
// 									controller_type = '8';
// 									attribute_option_labe = 'all';
// 								 	objController = {
// 										controller_type:'8',
// 										attribute_id:val.group_id.toString(),
// 										attribute_label:val.label_name,
// 										home_status:val.is_details_page.toString(),
// 									 	home_sort_order:val.details_sort_order.toString(),
// 									 	adv_status:val.is_listing_page.toString(),
// 									 	adv_sort_order:val.listing_sort_order.toString(),
// 									 	attribute_option_label:'all',
// 									 	items_input_range_value:retMainArr
// 									};
							
// 								}else if(val.input_type === "text"){
// 									controller_type = '9';
// 								 	objController = {
// 										controller_type:'9',
// 										attribute_id:val.group_id.toString(),
// 										attribute_label:val.label_name,
// 										home_status:val.is_details_page.toString(),
// 									 	home_sort_order:val.details_sort_order.toString(),
// 									 	adv_status:val.is_listing_page.toString(),
// 									 	adv_sort_order:val.listing_sort_order.toString(),
// 									 	attribute_option_label:'',
// 									 	items_edittext_value:retMainArr
// 									};
						
// 								}else if(val.input_type === "button"){
// 									controller_type = '10';
// 								 	objController = {
// 										controller_type:'10',
// 										attribute_id:val.group_id.toString(),
// 										attribute_label:val.label_name,
// 										home_status:val.is_details_page.toString(),
// 									 	home_sort_order:val.details_sort_order.toString(),
// 									 	adv_status:val.is_listing_page.toString(),
// 									 	adv_sort_order:val.listing_sort_order.toString(),
// 									 	attribute_option_label:'',
// 									 	items_sing_value_list:retMainArr
// 									};
						
// 								}

// 								mainArr.push(objController);

// 							});
// 					  	}
// 				  		const result = await returnData(mainArr);
// 					}
					 
// 					const returnData = x => {
// 					  return new Promise((resolve, reject) => {
// 					    setTimeout(() => {
// 					      resolve(x);
// 					    }, 500);
// 					  });
// 					}
					 
// 					forEach().then(() => {
				
// 						if(trigger !==0){
// 							var sortedArray = mainArr.sort(function(a, b) {
// 							    return a.sort_order > b.sort_order ? 1 : a.sort_order < b.sort_order ? -1 : 0;
// 							});
// 							return_result(sortedArray);
// 						}else{
// 							return_result([]);
// 						}
// 					})

// 				});
// 	        }
// 	        else
// 	        { 
// 	        	return_result([]);
// 	        }

// 		});

// 	}
// 	catch(err)
// 	{ console.log("getCategoryAttributes",err); return_result(null); }	
// };

methods.getCategoryAttributeValue = function(group_id,input_type,return_result){

	try
	{
		var arrValues = [];
		var obj_values = {};
		var controller_type;
		var objController = {};
		var value;
		var logo;

		if(input_type == 'color_picker'){
			obj_values = {
				id:'0',
				title:'all',
				color_code:''
			};
			arrValues.push(obj_values);
		}

		var singleObj = ['text','input_range','select_range'];
	 	var arrSameInputs = ['multiselect_image_button','multiselect_dropdown','image_button','dropdown'];

		if(arrSameInputs.includes(input_type) || input_type == 'color_picker' || input_type == 'radio_button' || input_type == 'text'){

			var quer = `SELECT * FROM jaze_jazestore_attributes_groups_meta 
				WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
				AND group_id IN (SELECT group_id FROM  jaze_jazestore_attributes_groups_meta  WHERE meta_key = 'status' AND meta_value = '1' )`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					methods.convertMultiMetaArray(data, function(retConverted){	

						retConverted.forEach(vala => { 
						
							if(arrSameInputs.includes(input_type)){

								if(vala.value != null){
									value = vala.value;
									logo  = '';
								}else{
									value = '';
									logo = vala.logo;
								}

								if(input_type == 'multiselect_image_button'){

									obj_values = {
										id:vala.group_id.toString(),
										title:vala.key_name,
										value:'',
										logo:STATIC_IMG_URL+vala.value
									};

								}else{

									obj_values = {
										id:vala.group_id.toString(),
										title:vala.key_name,
										value:value,
										logo:logo
									};

								}

							}else if(input_type == 'color_picker'){

								obj_values = {
									id:vala.group_id.toString(),
									title:vala.key_name,
									color_code:vala.value
								};

							}else if(input_type == 'radio_button'){


								obj_values = {
									id:vala.group_id.toString(),
									title:vala.key_name,
									value: vala.value,
						
								};

							}else if(input_type == 'text'){
								
								obj_values = {
									text_hint_label :vala.key_name
								};
							}

							arrValues.push(obj_values);

						}); 

					});

		  		}

			});

		
		}else if(input_type == 'input_range'){
	

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_jazestore_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var min_field_label;
					var max_field_label;
					var suffix_field_label;
					var separator_field_label;

					const forEach = async () => {
					  	const values = data;
					  	for (const vala of values){

							if(vala.key_name == 'separator label'){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == 'min label'){
								min_field_label = vala.key_value;
							}

							if(vala.key_name == 'max label'){
								max_field_label = vala.key_value;
							}

							if(vala.key_name == 'suffix'){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								min_field_label:min_field_label,
								max_field_label:max_field_label,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

					  	}
				  		const result = await returnData(obj_values);
					}
					 
					const returnData = x => {
					  return new Promise((resolve, reject) => {
					    setTimeout(() => {
					      resolve(x);
					    }, 300);
					  });
					}
					 
					forEach().then(() => {

						arrValues.push(obj_values);
					
					})
		  		}

			});
					
		}else if(input_type == 'select_range'){
	

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_jazestore_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var min_field_label;
					var min_field_value;
					var max_field_label;
					var max_field_value;
					var interval_value;
					var suffix_field_label;
					var separator_field_label;

					const forEach = async () => {
					  	const values = data;
					  	for (const vala of values){

							if(vala.key_name == 'separator label'){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == 'min label'){
								min_field_label = vala.key_value;
							}


							if(vala.key_name == 'min value'){
								min_field_value = vala.key_value;
							}

							if(vala.key_name == 'max label'){
								max_field_label = vala.key_value;
							}


							if(vala.key_name == 'max value'){
								max_field_value = vala.key_value;
							}

							if(vala.key_name == 'interval value'){
								interval_value = vala.key_value;
							}


							if(vala.key_name == 'suffix'){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								id:'0',
								min_field_label:min_field_label,
								min_field_value:min_field_value,
								max_field_label:max_field_label,
								max_field_value:max_field_value,
								internal_value:interval_value,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

					  	}
				  		const result = await returnData(obj_values);
					}
					 
					const returnData = x => {
					  return new Promise((resolve, reject) => {
					    setTimeout(() => {
					      resolve(x);
					    }, 300);
					  });
					}
					 
					forEach().then(() => {
						arrValues.push(obj_values);
					
					})
		  		}

			});
					
		}

		setTimeout(function(){ 

			if(singleObj.includes(input_type)){
				return_result(obj_values);
			}else{
				return_result(arrValues.sort(sortingArrayObject('title')));
			}
			
		}, 500);

	}
	catch(err)
	{ console.log("getCategoryAttributeValue",err); return_result([]); }	
};


methods.searchAuto =  function(arrParams,return_result){
	try
	{			
		var arrValues = [];
		var accParams = {};
		var account_details = {};
		var arrResults = [];
		var arrAccounts = [];

		var arrProductID = [];

		var pagination_details  = {
			total_page   : '0',
        	current_page : '0',
        	next_page    : '0',
          	total_result : '0'
        };

        var arrCategory = [];
		var total_result;

		var filteredArrayAccId = [];

		var delay = 1000;

		if(parseInt(arrParams.attributes.length) > 10){
			delay = delay + 200 * 5;
		}

	
	  	if(arrParams.attributes.length > 0)
	  	{

			const forEach = async () => {
			  	const attributes = arrParams.attributes;
			  	for (const param of attributes){

					if(parseInt(param.type) >= 1 &&  parseInt(param.type) <= 6)
					{	
						var title = param.value.toLowerCase();
						if(param.value.toLowerCase() != ''){
							title = param.value.toLowerCase().split(",");
						}
		
						var arrTitle = title;
						var query = "SELECT * FROM jaze_products_meta WHERE product_id=? and meta_key=?	 ";
					}
					else if(parseInt(param.type) == 7)
					{
						var query = "SELECT * FROM jaze_products_meta WHERE product_id =? AND meta_key =? AND ( meta_value >='"+param.min_value+"' AND meta_value <='"+param.max_value+"')";
					}
					else if(parseInt(param.type) == 8)
					{
						var query = "SELECT * FROM jaze_products_meta WHERE product_id=? and meta_key=?	 ";
					}
					else if(parseInt(param.type) == 9)
					{
						var query = "SELECT * FROM jaze_products_meta WHERE product_id=? and meta_key=? AND meta_value LIKE '%" + param.value + "%' ";
					}


					DB.query(query,[arrParams.category_id,'attribute_filter_'+param.attribute_id], function(data, error){

				
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							
				  			if(parseInt(param.type) >= 1 &&  parseInt(param.type) <= 6)
							{	
								var arrValues = data[0].meta_value.toLocaleLowerCase().split(",");
								if(arrValues.some(item => arrTitle.includes(item))){
									arrCategory.push(data[0].product_id);
								}
							}
							else if(parseInt(param.type) == 7)
							{
								arrCategory.push(data[0].product_id);
							}
							else if(parseInt(param.type) == 8)
							{
								var arrValues = data[0].meta_value.split(",");
						  		if( parseInt(param.min_value) >= parseInt(arrValues[0]) && parseInt(param.max_value) >= parseInt(arrValues[1]) ){
						  			arrCategory.push(data[0].product_id);
						  		}
							}
							else if(parseInt(param.type) == 9)
							{
								arrCategory.push(data[0].product_id);
							}

						}

					});

			  	}

		  		const result = await returnData(arrCategory);
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, delay);
			  });
			}
			 
			forEach().then(() => {

				var newArray = [];
				var total_page = 0;

				var total_result = arrCategory.length.toString();
				
				if(parseInt(arrCategory.length) > 5){
					newArray   = arrResults.slice(Math.ceil( parseInt(arrParams.page) * 5 ), (parseInt(arrParams.page) + 1) * 5);
					total_page = Math.ceil((parseInt(arrCategory.length) - 5 ) / 5);
				}else{
					newArray   = arrCategory.slice(Math.ceil( parseInt(arrParams.page) * 5 ), (parseInt(arrParams.page) + 1) * 5);
					total_page = 0;
				}

				var next_page = 0;;
				if(parseInt(total_page) !== 0){
					next_page  = parseInt(arrParams.page)+1;
				}


				pagination_details  = {
					total_page   : total_page.toString(),
	                current_page : arrParams.page.toString(),
	                next_page    : next_page.toString(),
                   total_result : total_result.toString()
	            };



				methods.getProductList(arrCategory,arrParams, function(retProductList){	

					return_result(retProductList,pagination_details);

				});

			})

		}
		else
		{

	
			DB.query("SELECT * FROM jaze_products WHERE jaze_category_id IN (?)",[arrParams.category_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					
					const forEach = async () => {
					  	const values = data;
					  	for (const val of values){

					  		arrCategory.push(val.id);
					  	}

				  		const result = await returnData(arrCategory);
					}
					 
					const returnData = x => {
					  return new Promise((resolve, reject) => {
					    setTimeout(() => {
					      resolve(x);
					    }, delay);
					  });
					}
					 
					forEach().then(() => {


						var newArray = [];
						var total_page = 0;

						var total_result = arrCategory.length.toString();

						if(parseInt(arrCategory.length) > 100){
							newArray   = arrCategory.slice(Math.ceil( parseInt(arrParams.page) * 100 ), (parseInt(arrParams.page) + 1) * 100);
							total_page = Math.ceil((parseInt(arrCategory.length) - 100 ) / 100);
						}else{
							newArray   = arrCategory.slice(Math.ceil( parseInt(arrParams.page) * 100 ), (parseInt(arrParams.page) + 1) * 100);
							total_page = 0;
						}

						var next_page = 0;
						if(parseInt(total_page) !== 0){
							next_page  = parseInt(arrParams.page)+1;
						}



			            if(parseInt(total_page) >= parseInt(arrParams.page)){


							pagination_details  = {
								total_page   : total_page.toString(),
				                current_page : arrParams.page.toString(),
				                next_page    : next_page.toString(),
				                total_result : total_result.toString()
				            };



							methods.getProductList(newArray,arrParams, function(retProductList){	

								return_result(retProductList,pagination_details);

							});

			            }else{
		            		return_result(null,pagination_details);

			            }

					})
				}

			});

		}


	}
	catch(err)
	{ console.log("searchAuto",err); return_result(null);}
};




methods.getProductList =  function(arrProductID,arrParams,return_result){
	try
	{	

		var total_page = 0;
		var next_page = 0;
		var total_result = 0;
		var newArray = [];

		var objProduct = {};
		var objAttri = {};

		var arrProducts = [];

		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };

		var arrResults = [];
		var account_details = {};

		var delay = 1000;

		if(parseInt(arrProductID.length) > 10){
			delay = delay + 200 * 5;
		}

		var query = "SELECT * FROM jaze_products WHERE id IN (?) ";

		if(parseInt(arrParams.country_id) !==0 ){
			query += " AND country_id ='"+arrParams.country_id+"' ";
		}

		if(parseInt(arrParams.state_id) !==0 ){
			query += " AND state_id ='"+arrParams.state_id+"' ";
		}

		if(parseInt(arrParams.city_id) !==0 ){
			query += " AND city_id ='"+arrParams.city_id+"' ";
		}

		var image = "";

		DB.query(query,[arrProductID], function(cData, error){

			if(typeof cData !== 'undefined' && cData !== null && parseInt(cData.length) > 0)
			{

				const forEach = async () => {
				  	const values = cData;
				  	for (const val of values){

						DB.query("SELECT * FROM jaze_products_meta WHERE product_id=?",[val.id], function(inData, error){

							methods.convertMultiMetaArrayProduct(inData, function(retProductDetails){	

								retProductDetails.forEach(function(value){

									methods.getCurrency(val.currency_id, function(retCurrency){	

										methods.getAutoEmblem(value.attribute_filter_73, function(retEmblem){	

											var model = '';
		    								if(value.attribute_filter_91){
		    									model = value.attribute_filter_91;
		    								}

											objAttri = {
												make  : value.attribute_filter_73,
												year  : value.attribute_filter_66,
												color : value.attribute_filter_71,
												km    : value.attribute_filter_67,
												model : model,
												product_sections:value.product_sections
											}

											if(value.featured_image){
		    									image = G_STATIC_IMG_URL+"uploads/jazenet/company/"+val.account_id+"/products/"+val.id+"/thumb/"+value.featured_image;
		    								}

								        	objProduct = {
												product_id : val.id.toString(),
												name       : val.name,
												title      : val.title,
												logo       : retEmblem,
												image      : image,
												currency   : retCurrency,
												price      : value.regular_price.toString(),
												attributes : objAttri
											};
									
											arrProducts.push(objProduct);
									
							   	 		});
						   	 		});

							    });

			
							});
						});
				  	}
			  		const result = await returnData(arrProducts);
				}
				 
				const returnData = x => {
				  return new Promise((resolve, reject) => {
				    setTimeout(() => {
				      resolve(x);
				    }, delay);
				  });
				}

				forEach().then(() => {
					return_result(arrProducts);
				})

			}
			else
			{ return_result(null); }

		});
	}
	catch(err)
	{ console.log("getProductList",err); return_result(null);}
};


methods.getAutoEmblem =  function(name,return_result){
	try
	{	
		var querty = `SELECT * FROM jaze_jazestore_attributes_groups_meta WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key='key_name' AND meta_value =?)`;
		DB.query(querty,[name], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				methods.convertMultiMetaArray(data, function(retConverted){		

					var emblem = G_STATIC_IMG_URL+"uploads/jazenet/category/jazestore_attributes/";

					return_result(emblem+retConverted[0].value);

				});

			}else{
				return_result('');
			}

		});

	}
	catch(err)
	{ console.log("getAutoEmblem",err); return_result(null);}
};


methods.getCurrency =  function(currency_id,return_result){
	try
	{
		DB.query("SELECT code FROM currency_master WHERE currency_id=?",[currency_id], function(data, error){

			if(data!=null){
				return_result(data[0].code);
			}else{
				return_result('');
			}

		});

	}
	catch(err)
	{ console.log("getCurrency",err); return_result(null);}
};

methods.checkIfInGeo =  function(arrAccounts,arrParams,return_result){
	try
	{

		var accParams = {};
		var account_details = {};
		var arrResults = [];

		const forEach = async () => {
		  	const values = arrAccounts;
		  	for (const val of values){
		  		accParams = {account_id:val};
	  			HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

	  				if(parseInt(arrParams.country_id) == parseInt(retAccountDetails.country_id) 
	  					|| parseInt(arrParams.state_id) == parseInt(retAccountDetails.state_id) 
	  					|| parseInt(arrParams.city_id) == parseInt(retAccountDetails.city_id)  ){

						account_details = {
		  					account_id:retAccountDetails.account_id,
		  					name:retAccountDetails.name,
		  					logo:retAccountDetails.logo
		  				};
		  				arrResults.push(account_details)
  					}
				});
		  	}
	  		const result = await returnData(arrResults);
		}
		 
		const returnData = x => {
		  return new Promise((resolve, reject) => {
		    setTimeout(() => {
		      resolve(x);
		    }, 500);
		  });
		}

		forEach().then(() => {
			return_result(arrResults);
		})

	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};



// -------------------- property start ------------------------


methods.getAttributeParentProperty = function(arrParams,return_result){

	try
	{
		var arrRows = [];
		var newObj = {};
		var status = '1';
		var category_id = '208';

		if(parseInt(arrParams.category_id) !== 0){
			category_id = arrParams.category_id;
		}

		var quer = `SELECT t1.* FROM(
				SELECT group_id,
					(CASE WHEN meta_key = 'parent_id' THEN meta_value END) AS parent_id,
					(SELECT c.meta_value FROM jaze_jazestore_category_list c WHERE c.meta_key = 'sort_order' AND c.group_id = jaze_jazestore_category_list.group_id) AS sort_order
				FROM  jaze_jazestore_category_list
				WHERE group_id =? ) AS t1 WHERE parent_id != ''`;


		DB.query(quer,[category_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				var querParent = `SELECT * FROM jaze_jazestore_category_list WHERE group_id IN (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key = 'parent_id' AND meta_value ='208' ) `;
				if(parseInt(arrParams.category_id) !== 0){
					var querParent = `SELECT * FROM jaze_jazestore_category_list WHERE group_id IN (?) OR  group_id IN (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key = 'parent_id' AND meta_value ='208' ) `;
				}

				DB.query(querParent,[data[0].group_id], function(pdata, error){


					if(typeof pdata !== 'undefined' && pdata !== null && parseInt(pdata.length) > 0){



						methods.convertMultiMetaArray(pdata, function(retConverted){
		
							const forEach = async () => {
							  	const values = retConverted;
							  	for (const val of values){

									var status = '0';

				

									if(parseInt(category_id) == 208 && parseInt(val.group_id) == 209){
										status = '1';
									}
									else if(val.group_id == parseInt(category_id)){
										status = '1';
									}
				
									if(parseInt(val.group_id) !== 208){

										if(parseInt(val.group_id) !==0){
											newObj = {
												title: val.cate_name,
												active_status: status,
												category_id: val.group_id.toString()
											};
											arrRows.push(newObj);
										}
									}
								
							
							  	}

						  		const result = await returnData(arrRows);
							}
							 
							const returnData = x => {
							  return new Promise((resolve, reject) => {
							    setTimeout(() => {
							      resolve(x);
							    }, 300);
							  });
							}
							 
							forEach().then(() => {
								return_result(arrRows);
							})
	
						});
					}
					else
					{
						return_result(null);
					}

				});
	
	        }
	        else
	        { 
	        	return_result(null);
	        }

		});

	}
	catch(err)
	{ console.log("getAttributeParentProperty",err); return_result(null); }	

};


methods.getCategoryAttributesProperty = function(arrParams,return_result){


	try
	{
		var mainArr = [];
		var objController = {};
		var sort_order;
		var trigger = 0;
		var object_name;

		
		if(parseInt(arrParams.category_id) !== 0){
			var quer = "SELECT * FROM jaze_jazestore_attributes_groups WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups WHERE meta_key = 'cat_id' AND (meta_value ='"+arrParams.category_id+"' OR meta_value ='208' OR meta_value ='92') ) ";

		}else{
			var quer = "SELECT * FROM jaze_jazestore_attributes_groups WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups WHERE meta_key = 'cat_id' AND (meta_value ='209' OR meta_value ='210' OR  meta_value ='208' OR meta_value ='92') ) ";
			quer += "AND group_id NOT IN ('59','44')";
		}

			quer +=	` AND group_id IN (SELECT group_id FROM  jaze_jazestore_attributes_groups  WHERE meta_key = 'status' AND meta_value = '1' )`;

		DB.query(quer,null, function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){	


					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (val) {
						return new Promise(function (resolve, reject) {	


							if(parseInt(val.group_id) === 92)
							{

								methods.getLocationAttributes(arrParams.state_id,arrParams.city_id,function(retArea){

							 		objController = {
										controller_type:'2',
										attribute_id:val.group_id.toString(),
										attribute_label:val.label_name,
										home_status:val.is_details_page.toString(),
									 	home_sort_order:val.details_sort_order.toString(),
									 	adv_status:val.is_listing_page.toString(),
									 	adv_sort_order:val.listing_sort_order.toString(),
									 	attribute_option_label:'all',
									 	items_value_list:retArea
									};

									mainArr.push(objController);
									counter -= 1;
									if (counter === 0){ 
										resolve(mainArr);
									}

								});

							}
							else
							{
							
								methods.getCategoryAttributeValueProperty(val.group_id,val.input_type, function(retMainArr){ 
									
									var controller_type;
									var attribute_option_labe = '';
									var ret_object = {};

			  						if(val.input_type === "multiselect_image_button"){

									 	objController = {
											controller_type:'1',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_value_list:retMainArr
										};
						
									}else if(val.input_type === "multiselect_dropdown"){

									 	objController = {
											controller_type:'2',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_value_list:retMainArr
										};

							
									}else if(val.input_type === "color_picker"){

									 	objController = {
											controller_type:'3',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_color_list:retMainArr
										};
						
									}else if(val.input_type === "image_button"){

									 	objController = {
											controller_type:'4',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_value_list:retMainArr
										};

							
									}else if(val.input_type === "dropdown"){

									 	objController = {
											controller_type:'5',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_value_list:retMainArr
										};
							
									}else if(val.input_type === "radio_button"){
								
									 	objController = {
											controller_type:'6',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'',
										 	items_sing_value_list:retMainArr
										};
					
									}else if(val.input_type === "select_range"){
										
									 	objController = {
											controller_type:'7',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'',
										 	items_select_range_value:retMainArr
										};
					
									}else if(val.input_type === "input_range"){
						
									 	objController = {
											controller_type:'8',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'all',
										 	items_input_range_value:retMainArr
										};
								
									}else if(val.input_type === "text"){
										
									 	objController = {
											controller_type:'9',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'',
										 	items_edittext_value:retMainArr
										};
							
									}else if(val.input_type === "button"){
							
									 	objController = {
											controller_type:'10',
											attribute_id:val.group_id.toString(),
											attribute_label:val.label_name,
											home_status:val.is_details_page.toString(),
										 	home_sort_order:val.details_sort_order.toString(),
										 	adv_status:val.is_listing_page.toString(),
										 	adv_sort_order:val.listing_sort_order.toString(),
										 	attribute_option_label:'',
										 	items_sing_value_list:retMainArr
										};
							
									}

									mainArr.push(objController);
									counter -= 1;
									if (counter === 0){ 
										resolve(mainArr);
									}
								});

							}
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							var sortedArray = result[0].sort(function(a, b) {
							    return a.home_sort_order > b.home_sort_order ? 1 : a.home_sort_order < b.home_sort_order ? -1 : 0;
							});

							return_result(sortedArray);
						}
					});


				});
	        }
	        else
	        { 
	        	return_result([]);
	        }

		});

	}
	catch(err)
	{ console.log("getCategoryAttributesProperty",err); return_result(null); }	
};


methods.getCategoryAttributeValueProperty = function(group_id,input_type,return_result){

	try
	{
		var arrValues = [];
		var obj_values = {};
		var controller_type;
		var objController = {};
		var value;
		var logo;

		if(input_type == 'color_picker'){
			obj_values = {
				id:'0',
				title:'all',
				color_code:''
			};
			arrValues.push(obj_values);
		}

		var singleObj = ['text','input_range','select_range'];
	 	var arrSameInputs = ['multiselect_image_button','multiselect_dropdown','image_button','dropdown'];

		if(arrSameInputs.includes(input_type) || input_type == 'color_picker' || input_type == 'radio_button' || input_type == 'text'  || input_type == 'button'){

			var quer = `SELECT * FROM jaze_jazestore_attributes_groups_meta 
				WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
				AND group_id IN (SELECT group_id FROM  jaze_jazestore_attributes_groups_meta  WHERE meta_key = 'status' AND meta_value = '1' )`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					methods.convertMultiMetaArray(data, function(retConverted){	


						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (vala) {
							return new Promise(function (resolve, reject) {		
					
													
								if(arrSameInputs.includes(input_type)){

									if(vala.value != null){
										value = vala.value;
										logo  = '';
									}else{
										value = '';
										logo = vala.logo;
									}

									if(input_type == 'multiselect_image_button'){

										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value:'',
											logo:STATIC_IMG_URL+vala.value
										};

									}else if(input_type == 'image_button'){

										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value:STATIC_IMG_URL+value,
											logo:logo
										};

									}else{
										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value:value,
											logo:logo
										};
									}

								}else{



									if(input_type == 'color_picker'){

										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											color_code:vala.value
										};

									}else if(input_type == 'radio_button'){


										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value: vala.value,
								
										};

									}else if(input_type == 'button'){


										obj_values = {
											id:vala.group_id.toString(),
											title:vala.key_name,
											value: vala.value,
								
										};

									}else if(input_type == 'text'){
										
										obj_values = {
											text_hint_label :vala.group_id.toString()
										};
									}

								}

								
							
								counter -= 1;
								resolve(obj_values);

					
							
							})
						}],
						function (result, current) {
							if (counter === 0){ 
								arrValues.push(result[0]);
							
							}
						});

					});

		  		}

			});

		
		}else if(input_type == 'input_range'){
	

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_jazestore_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					var counter = data.length;		
					var jsonItems = Object.values(data);

					var min_field_label;
					var max_field_label;
					var suffix_field_label;
					var separator_field_label;

					promiseForeach.each(jsonItems,[function (vala) {
						return new Promise(function (resolve, reject) {		
							
							if(vala.key_name == 'separator label'){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == 'min label'){
								min_field_label = vala.key_value;
							}

							if(vala.key_name == 'max label'){
								max_field_label = vala.key_value;
							}

							if(vala.key_name == 'suffix'){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								min_field_label:min_field_label,
								max_field_label:max_field_label,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

							counter -= 1;
							if (counter === 0){ 
								resolve(obj_values);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							arrResults.push(result[0]);
						}
					});

		  		}

			});
					
		}else if(input_type == 'select_range'){
	

			var quer = `SELECT t1.* FROM(
			SELECT group_id,
				(CASE WHEN meta_key = 'attribute_id' THEN meta_value END) AS attribute_id,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'key_name' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_name,
				(SELECT c.meta_value FROM jaze_jazestore_attributes_groups_meta c WHERE c.meta_key = 'value' AND c.group_id = jaze_jazestore_attributes_groups_meta.group_id) AS key_value
			FROM  jaze_jazestore_attributes_groups_meta
			WHERE group_id IN (SELECT group_id FROM jaze_jazestore_attributes_groups_meta WHERE meta_key = 'attribute_id' AND meta_value =?)
			) AS t1 WHERE attribute_id != ""`;

			DB.query(quer,[group_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){


					var counter = data.length;		
					var jsonItems = Object.values(data);

					var min_field_label;
					var min_field_value;
					var max_field_label;
					var max_field_value;
					var interval_value;
					var suffix_field_label;
					var separator_field_label;

					promiseForeach.each(jsonItems,[function (vala) {
						return new Promise(function (resolve, reject) {		
								
							if(vala.key_name == 'separator label'){
								separator_field_label = vala.key_value;
							}

							if(vala.key_name == 'min label'){
								min_field_label = vala.key_value;
							}


							if(vala.key_name == 'min value'){
								min_field_value = vala.key_value;
							}

							if(vala.key_name == 'max label'){
								max_field_label = vala.key_value;
							}


							if(vala.key_name == 'max value'){
								max_field_value = vala.key_value;
							}

							if(vala.key_name == 'interval value'){
								interval_value = vala.key_value;
							}


							if(vala.key_name == 'suffix'){
								suffix_field_label = vala.key_value;
							}

							obj_values = {
								id:'0',
								min_field_label:min_field_label,
								min_field_value:min_field_value,
								max_field_label:max_field_label,
								max_field_value:max_field_value,
								internal_value:interval_value,
								suffix_field_label:suffix_field_label,
								separator_field_label:separator_field_label
							};

							counter -= 1;
							if (counter === 0){ 
								resolve(obj_values);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							arrValues.push(result[0]);
						}
					});

		  		}

			});
					
		}

		setTimeout(function(){ 

			if(singleObj.includes(input_type)){
				return_result(obj_values);
			}else{
				return_result(arrValues.sort(sortingArrayObject('title')));
			}
			
		}, 500);

	}
	catch(err)
	{ console.log("getCategoryAttributeValueProperty",err); return_result([]); }	
};

methods.getLocationAttributes =  function(state_id,city_id,return_result){
	try
	{
		if(parseInt( city_id ) === 0 && parseInt( state_id ) !== 0)
		{	
			DB.query(`SELECT * FROM cities WHERE state_id =?`,[state_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					DB.query(`SELECT * FROM area WHERE city_id =? AND status ='1'`,[data[0].id], function(cdata, error){

						if(typeof cdata !== 'undefined' && cdata !== null && parseInt(cdata.length) > 0){

							var counter = cdata.length;		
							var jsonItems = Object.values(cdata);
							var obj_area = {};
							var ret_array = [];

							promiseForeach.each(jsonItems,[function (jsonItems) {
								return new Promise(function (resolve, reject) {	

									
									obj_area = {
										id    : jsonItems.area_id.toString(),
										title : jsonItems.area_name,
										value : jsonItems.area_id.toString()
									};


									counter -= 1;
									ret_array.push(obj_area);

									if (counter === 0){ 
										resolve(ret_array);
									}
								})
							}],
							function (result, current) {
								if (counter === 0){ 

									return_result(result[0].sort(sortingArrayObject('title')));
						
											
								}
							});

						}
						else
						{
							return_result([]);
						}

					});

				}
				else
				{
					return_result([]);
				}

			});
		}
		else
		{
			DB.query(`SELECT * FROM area WHERE city_id =? AND status ='1'`,[city_id], function(cdata, error){

				if(typeof cdata !== 'undefined' && cdata !== null && parseInt(cdata.length) > 0){

					var counter = cdata.length;		
					var jsonItems = Object.values(cdata);
					var obj_area = {};
					var ret_array = [];

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							
							obj_area = {
								id    : jsonItems.area_id.toString(),
								title : jsonItems.area_name,
								value : jsonItems.area_id.toString()
							};


							counter -= 1;
							ret_array.push(obj_area);

							if (counter === 0){ 
								resolve(ret_array);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							return_result(result[0].sort(sortingArrayObject('title')));
				
									
						}
					});

				}
				else
				{
					return_result([]);
				}

			});
		}
	}
	catch(err)
	{ console.log("getLocationAttributes",err); return_result([]);}
};


methods.searchProperty =  function(arrParams,return_result){
	try
	{			
		var arrValues       = [];
		var accParams       = {};
		var account_details = {};
		var arrResults      = [];
		var arrAccounts     = [];
		var aProductIDS     = [];
		var arrProductID    = [];

		var pagination_details  = {
			total_page   : '0',
        	current_page : '0',
        	next_page    : '0',
          	total_result : '0'
        };

        var arrCategory = [];
		var total_result;

		var filteredArrayAccId = [];

	  	if(parseInt(arrParams.attributes.length) > 0)
	  	{

			var query = "SELECT * FROM jaze_products WHERE FIND_IN_SET(?,jaze_category_all)>0";
			DB.query(query,[arrParams.category_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{

					var counter = data.length;		
					var jsonItems = Object.values(data);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
				
							aProductIDS.push(jsonItems.id.toString());

							counter -= 1;
							if (counter === 0){ 
								resolve(aProductIDS);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							var counterInn = arrParams.attributes.length;		
							var jsonItemsInn = Object.values(arrParams.attributes);

							promiseForeach.each(jsonItemsInn,[function (param) {
								return new Promise(function (resolveInn, reject) {	

									if(parseInt(param.attribute_id) === 92)
									{	

										if(param.value != ''){
											title = param.value.split(",");
										}

										methods.getAreaFromAttribute(title,result[0],function(retid){

											if(retid!=null){
												counterInn -= 1;
												arrCategory.push(retid);
												
											}

											if (counterInn === 0){ 
												resolveInn(arrCategory[0]);
											}
											
										});
						
								
									}
									else
									{

										var queryIn = '';
										if(parseInt(param.type) >= 1 &&  parseInt(param.type) <= 6 && parseInt(param.attribute_id) !== 92)
										{	
											var title = param.value.toLowerCase();
											if(param.value.toLowerCase() != ''){
												title = param.value.toLowerCase().split(",");
											}
							
											var arrTitle = title;
											var queryIn = "SELECT * FROM jaze_products_meta WHERE product_id IN(?) and meta_key=?	 ";
										}
										else if(parseInt(param.type) == 7)
										{
											var queryIn = "SELECT * FROM jaze_products_meta WHERE product_id IN(?) AND meta_key =? AND ( meta_value >='"+param.min_value+"' AND meta_value <='"+param.max_value+"')";
										}
										else if(parseInt(param.type) == 8)
										{
											var queryIn = "SELECT * FROM jaze_products_meta WHERE product_id IN(?) and meta_key=?	 ";
										}
										else if(parseInt(param.type) == 9)
										{
											var queryIn = "SELECT * FROM jaze_products_meta WHERE product_id IN(?) and meta_key=? AND meta_value LIKE '%" + param.value + "%' GROUP BY meta_key LIMIT 1";
										}
										else if(parseInt(param.type) == 10)
										{
											var queryIn = "SELECT * FROM jaze_products_meta WHERE product_id IN(?) and meta_key=? AND meta_value LIKE '%" + param.value + "%' ";
										}

										var title = param.value.toLowerCase();
										if(param.value.toLowerCase() != ''){
											title = param.value.toLowerCase().split(",");
										}

										// console.log('query ',queryIn);
										// console.log('arrProductID ',result[0]);
										// console.log('attri_id ', param.attribute_id);

										DB.query(queryIn,[result[0],'attribute_filter_'+param.attribute_id], function(dataID, error){

										
											if(typeof dataID !== 'undefined' && dataID !== null && parseInt(dataID.length) > 0){

									  			if(parseInt(param.type) >= 1 &&  parseInt(param.type) <= 6)
												{	

											
													dataID.forEach(function(value) {
														var arrValues = value.meta_value.toLocaleLowerCase().split(",");
														if(arrValues.some(item => arrTitle.includes(item))){
															arrCategory.push(value.product_id);
														}

												  	});
												}
												else if(parseInt(param.type) == 7)
												{
													arrCategory.push(dataID[0].product_id);
												}
												else if(parseInt(param.type) == 8)
												{
													var arrValues = dataID[0].meta_value.split(",");
											  		if( parseInt(param.min_value) >= parseInt(arrValues[0]) && parseInt(param.max_value) >= parseInt(arrValues[1]) ){
											  			arrCategory.push(dataID[0].product_id);
											  		}
												}
												else if(parseInt(param.type) == 9)
												{
													arrCategory.push(dataID[0].product_id);
												}
												else if(parseInt(param.type) == 10)
												{
													dataID.forEach(function(value) {
														arrCategory.push(value.product_id);
												  	});
												}
						
												counterInn -= 1;
												if (counterInn === 0){ 
													resolveInn(arrCategory);
												}
							
											}

										});
									}
								
								})
							}],
							function (resultInn, current) {
								if (counterInn === 0){ 

									if(parseInt(resultInn[0].length) > 0)
									{
										methods.getPropertyList(resultInn[0],arrParams, function(retPropertyList,pagination_details){	
											return_result(retPropertyList,pagination_details);
										});
									}
									else
									{
										return_result(null,pagination_details);
									}
								}
							});

						}
					});

				}

			});

		}
		else
		{

			var query = "SELECT * FROM jaze_products WHERE  FIND_IN_SET('209',jaze_category_all)>0 OR FIND_IN_SET('212',jaze_category_all)>0";
			if(parseInt(arrParams.category_id) !== 0){
				query = "SELECT * FROM jaze_products WHERE FIND_IN_SET(?,jaze_category_all)>0";
			}
	
			DB.query(query,[arrParams.category_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{

					var counter = data.length;		
					var jsonItems = Object.values(data);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
				
							if( parseInt(arrParams.state_id) !==0 || parseInt(arrParams.city_id) !==0 )
							{
								methods.checkSpecialAttribute(arrParams,jsonItems.jaze_category_all,jsonItems.id,function(retSpecial){
									if(retSpecial!=null){
								
										arrCategory.push(retSpecial);
										counter -= 1;
										if (counter === 0){ 
											resolve(arrCategory);
										}
									}
						
								});
							}
							else
							{
					
								arrCategory.push(jsonItems.id);
								counter -= 1;
								if (counter === 0){ 
									resolve(arrCategory);
								}
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							methods.getPropertyList(result[0],arrParams, function(retPropertyList,pagination_details){	
								return_result(retPropertyList,pagination_details);
							});
						}
					});

				}
				else
				{ return_result(null); }

			});

		}
	}
	catch(err)
	{ console.log("searchProperty",err); return_result(null);}
};


methods.getAreaFromAttribute =  function(title,prod_id,return_result){
	try
	{

		DB.query(`SELECT * FROM area WHERE area_name IN(?) AND status ='1'`,[title], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				var counter = data.length;		
				var jsonItems = Object.values(data);
				var obj_area = {};
				var ret_array = [];

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	

						var inQUery = "SELECT * FROM jaze_products WHERE area_id ='"+jsonItems.area_id+"' ";
						
						inQUery += " AND id IN("+prod_id+") ";



						DB.query(inQUery,null, function(cdata, error){
							
							
							if(typeof cdata !== 'undefined' && cdata !== null && parseInt(cdata.length) > 0)
							{
								var counterIn = cdata.length;		
								var jsonItemsIn = Object.values(cdata);
								promiseForeach.each(jsonItemsIn,[function (jsonItemsIn) {
									return new Promise(function (resolve, reject) {	

								
										counterIn -= 1;
										ret_array.push(jsonItemsIn.id);

										if (counterIn === 0){ 
											resolve(ret_array);
										}
									})
								}],
								function (resultIn, current) {
									if (counterIn === 0){ 
										
										return_result(resultIn[0]);
							
												
									}
								});

								//counter -= 1;
							}
							else
							{
								return_result(null);
							}
						});

				
					})
				}],
				function (result, current) {});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("checkSpecialAttribute",err); return_result(null);}
};


methods.checkSpecialAttribute =  function(arrParams,category_id,id,return_result){

	try
	{
		

		DB.query("SELECT * FROM jaze_jazestore_attributes_groups WHERE group_id='92'",null, function(data, error){
	 		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				methods.convertMultiMetaArray(data, function(retConverted){		

					var jaze_category_all = category_id.split(",");

					if(jaze_category_all.includes(retConverted[0].cat_id))
					{
						
						DB.query("SELECT * FROM jaze_products WHERE id=?",[id], function(indata, error){
					 		if(typeof indata !== 'undefined' && indata !== null && parseInt(indata.length) > 0)
							{	
														
								methods.getAreaFromState(arrParams,function(returnSpecial){

									if(returnSpecial.includes(indata[0].area_id)){
										return_result(id);
									}else{
										return_result(null);
									}

								});
							}
							else
							{
								return_result(null);
							}

						});

					}
					else
					{
						return_result(null);
					}

				});

	 	   	}
	 	   	else
	 	   	{
				return_result(null);
	 	   	}
 	 	});

	}
	catch(err)
	{ console.log("checkSpecialAttribute",err); return_result(null);}
};


methods.getAreaFromState =  function(arrParams,return_result){
	try
	{
		if(parseInt( arrParams.city_id ) === 0 && parseInt( arrParams.state_id ) !== 0)
		{	
			DB.query(`SELECT * FROM cities WHERE state_id =?`,[arrParams.state_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					DB.query(`SELECT * FROM area WHERE city_id =?`,[data[0].id], function(cdata, error){

						if(typeof cdata !== 'undefined' && cdata !== null && parseInt(cdata.length) > 0){

							var counter = cdata.length;		
							var jsonItems = Object.values(cdata);
							var obj_area = {};
							var ret_array = [];

							promiseForeach.each(jsonItems,[function (jsonItems) {
								return new Promise(function (resolve, reject) {	

							
									counter -= 1;
									ret_array.push(jsonItems.area_id);

									if (counter === 0){ 
										resolve(ret_array);
									}
								})
							}],
							function (result, current) {
								if (counter === 0){ 

									return_result(result[0]);
						
											
								}
							});

						}
						else
						{
							return_result([]);
						}

					});

				}
				else
				{
					return_result([]);
				}

			});
		}
		else
		{
			DB.query(`SELECT * FROM area WHERE city_id =?`,[arrParams.city_id], function(cdata, error){

				if(typeof cdata !== 'undefined' && cdata !== null && parseInt(cdata.length) > 0){

					var counter = cdata.length;		
					var jsonItems = Object.values(cdata);
					var obj_area = {};
					var ret_array = [];

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

					
							counter -= 1;
							ret_array.push(jsonItems.area_id);

							if (counter === 0){ 
								resolve(ret_array);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							return_result(result[0]);
				
									
						}
					});

				}
				else
				{
					return_result([]);
				}

			});
		}
	}
	catch(err)
	{ console.log("getAreaFromState",err); return_result([]);}
};


methods.getPropertyList =  function(arrProductID,arrParams,return_result){
	try
	{	

		var total_page = 0;
		var next_page = 0;
		var total_result = 0;
		var newArray = [];

		var objProduct = {};
		var objAttri = {};

		var arrProducts = [];

		var pagination_details  = {
			total_page   : '0',
	    	current_page : '0',
	    	next_page    : '0',
	    	total_result : '0'
	    };

		var arrResults = [];
		var account_details = {};

		var delay = 1000;

		if(parseInt(arrProductID.length) > 10){
			delay = delay + 200 * 5;
		}

		var query = "SELECT * FROM jaze_products WHERE id IN (?) ";

		if(parseInt(arrParams.country_id) !==0 ){
			query += " AND country_id ='"+arrParams.country_id+"' ";
		}

		if(parseInt(arrParams.state_id) !==0 ){
			query += " AND state_id ='"+arrParams.state_id+"' ";
		}

		if(parseInt(arrParams.city_id) !==0 ){
			query += " AND city_id ='"+arrParams.city_id+"' ";
		}

		var image = "";
	

		DB.query(query,[arrProductID], function(cData, error){

			if(typeof cData !== 'undefined' && cData !== null && parseInt(cData.length) > 0)
			{

				const forEach = async () => {
				  	const values = cData;
				  	for (const val of values){

						DB.query("SELECT * FROM jaze_products_meta WHERE product_id=?",[val.id], function(inData, error){

							methods.convertMultiMetaArrayProduct(inData, function(retProductDetails){	

								retProductDetails.forEach(function(value){

									methods.getCurrency(val.currency_id, function(retCurrency){	

										methods.getPropertyLocation(val.id, function(retLocation){	

											// methods.getDefaultAttributes(value.default_attributes, function(retDefaultAttri){	

												var val1 = '';
												var val2 = '';
												var val3 = '';
												var val4 = '';
												var val5 = '';
												var val6 = '';

												if(value.attribute_filter_43){
													val1 = value.attribute_filter_43;
												}else{
													val1 = value.attribute_filter_44;
												}

												if(value.attribute_filter_83){
													val2 = value.attribute_filter_83;
												}

												if(value.attribute_filter_30){
													val3 = value.attribute_filter_30;
												}else{
													if(value.attribute_filter_84){
														val3 = value.attribute_filter_84;
													}else{
														val3 = '';
													}
												}

												if(value.attribute_filter_89){
													val4 = value.attribute_filter_89;
												}

												if(value.attribute_filter_92){
													val5 = value.attribute_filter_92;
												}

												if(value.attribute_filter_50){
													val6 = value.attribute_filter_50;
												}

												objAttri = {
													property_type    : val1,
													sqft             : val2,
													buy_or_rent      : val3,
													bedrooms         : val4,
													bathrooms        : val5,
													product_sections : val6
												};

							
												if(value.featured_image){
			    									image = G_STATIC_IMG_URL+"uploads/jazenet/company/"+val.account_id+"/products/"+val.id+"/thumb/"+value.featured_image;
			    								}

									        	objProduct = {
													product_id: val.id.toString(),
													name:val.name,
													title:val.title,
													logo:'',
													image:image,
													currency:retCurrency,
													price:value.regular_price.toString(),
													location:retLocation,
													attributes:objAttri
												};
										
												arrProducts.push(objProduct);
								
							    			// });
						    			});
							    	});
							    });

			
							});
						});
				  	}
			  		const result = await returnData(arrProducts);
				}
				 
				const returnData = x => {
				  return new Promise((resolve, reject) => {
				    setTimeout(() => {
				      resolve(x);
				    }, delay);
				  });
				}

				forEach().then(() => {

					var newArray = [];
					var total_page = 0;

					var total_result = arrProducts.length.toString();

					if(parseInt(arrProducts.length) > 100){
						newArray   = arrProducts.slice(Math.ceil( parseInt(arrParams.page) * 100 ), (parseInt(arrParams.page) + 1) * 100);
						total_page = Math.ceil((parseInt(arrProducts.length) - 100 ) / 100);
					}else{
						newArray   = arrProducts.slice(Math.ceil( parseInt(arrParams.page) * 100 ), (parseInt(arrParams.page) + 1) * 100);
						total_page = 0;
					}

					var next_page = 0;
					if(parseInt(total_page) !== 0){
						next_page  = parseInt(arrParams.page)+1;
					}


		            if(parseInt(total_page) >= parseInt(arrParams.page)){

						pagination_details  = {
							total_page   : total_page.toString(),
			                current_page : arrParams.page.toString(),
			                next_page    : next_page.toString(),
			                total_result : total_result
			            };

						var sortedArray = arrProducts.sort(function(a, b) {
						    return a.product_id < b.product_id ? 1 : a.product_id > b.product_id ? -1 : 0;
						});
						
						return_result(sortedArray,pagination_details);


		            }else{

        				pagination_details  = {
							total_page   : '0',
					    	current_page : '0',
					    	next_page    : '0',
					    	total_result : '0'
					    };

	            		return_result(null,pagination_details);

		            }

				})

			}
			else
			{ 
				pagination_details  = {
					total_page   : '0',
			    	current_page : '0',
			    	next_page    : '0',
			    	total_result : '0'
			    };

				return_result(null,pagination_details); 
			}

		});
	}
	catch(err)
	{ console.log("getPropertyList",err); return_result(null);}
};


methods.getDefaultAttributes =  function(params,return_result){

	try
	{
		var objreturn = {
			bedrooms:'',
			bathrooms:''
		};

		var arrDefaultAttri = [];
		var inner_obj_specification = {};
		var arr_inner_obj_specification = [];


		if(params){
			var default_attributes = unserialize(params);
			Object.keys(default_attributes).forEach(function(key) {
				arrDefaultAttri.push(default_attributes[key]);
			});

			arrDefaultAttri.forEach(function(values) {
   		
	   			if(values.name){

					for(const [key, value] of Object.entries(values.attributes)) {
						inner_obj_specification[values.name] = value;

						if(values.name == 'Bedrooms' || values.name == 'bedrooms'){
							objreturn.bedrooms = value;
						}
						
						if(values.name == 'Bathrooms'){
							objreturn.bathrooms = value;
						}
						
					}

					
	   			}

			});

			return_result(objreturn);

		}

	}
	catch(err)
	{ console.log("getDefaultAttributes",err); return_result(objreturn);}
};



methods.getPropertyLocation =  function(property_id,return_result){
	try
	{

		DB.query("SELECT * FROM jaze_products WHERE id=?",[property_id], function(data, error){
	 		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				DB.query("SELECT * FROM area WHERE area_id=?",[data[0].area_id], function(adata, error){
					if(typeof adata !== 'undefined' && adata !== null && parseInt(adata.length) > 0)
					{
						return_result(adata[0].area_name);
					}
					else
					{

						DB.query("SELECT * FROM cities WHERE id=?",[data[0].city_id], function(adata, error){
							if(typeof adata !== 'undefined' && adata !== null && parseInt(adata.length) > 0)
							{
								return_result(adata[0].city_name);
							}
							else
							{
								return_result('');
							}
		 				});

					}
 				});
	 	   	}
	 	   	else
	 	   	{
				return_result('');
	 	   	}
 	 	});

	}
	catch(err)
	{ console.log("getPropertyLocation",err); return_result('');}
};

// ------- property end ----------------



// ---------- property details start -------------- //

methods.getPropertyDetails =  function(arrParams,return_result){
	try
	{
		var objProduct = {};
		var arrProduct = [];

		var obj_basic_details = {};
		var obj_company_details = {};

		var arr_obj_specification = [];
			var obj_specification = {};
			var arr_inner_obj_specification = [];
				var inner_obj_specification = {};
		var obj_property_extra = {};
		var arr_specification = [];

		var query = "SELECT * FROM jaze_products where id =?";

		DB.query(query,[arrParams.product_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				DB.query("SELECT * FROM jaze_products_meta WHERE product_id=?",[data[0].id], function(inData, error){

					const IMG_URL_PROPERTY = G_STATIC_IMG_URL+'uploads/jazenet/company/'+data[0].account_id+'/products/'+arrParams.product_id+'/thumb/';

					var accParams = {account_id:data[0].account_id};
					methods.getCompanyProfile(accParams, function(retCompanyDetails){	
							

						if(retCompanyDetails!=null)
						{
							methods.convertMultiMetaArrayProduct(inData, function(retProductDetails){	

								var mobParams = {account_type:retCompanyDetails.account_type,phonecode:retCompanyDetails.contact_phone_code};
								
								methods.__getMobileCode(mobParams, function(retMobileCode){	

									methods.getCurrency(data[0].currency_id, function(retCurrency){	

										methods.getCompanyCategory(retCompanyDetails.category_id, function(retCategory){		

											var counter = retProductDetails.length;		
											var jsonItems = Object.values(retProductDetails);

											var arrImages = [];
											var arrDefaultAttri = [];

											var property_type = '';
											var price         = '';
											var image         = '';

											promiseForeach.each(jsonItems,[function (jsonItems) {
												return new Promise(function (resolve, reject) {		

													if(jsonItems.gallery_images){
														var photo_serialize = unserialize(jsonItems.gallery_images);
														Object.keys(photo_serialize).forEach(function(key) {
															arrImages.push(IMG_URL_PROPERTY+photo_serialize[key]);
														});
													}

													if(jsonItems.default_attributes){
														var default_attributes = unserialize(jsonItems.default_attributes);
														Object.keys(default_attributes).forEach(function(key) {
															arrDefaultAttri.push(default_attributes[key]);
														});

														arrDefaultAttri.forEach(function(values) {
											   		
												   			if(values.name){

										   						for(const [key, value] of Object.entries(values.attributes)) {
																	inner_obj_specification[values.name] = value;
																	
																}

																arr_inner_obj_specification.push(inner_obj_specification);
												   			}
										
														});
													}


													if(jsonItems.sale_price && parseInt(jsonItems.sale_price) !== 0 ){
														price = jsonItems.sale_price;
													}else{
														price = jsonItems.regular_price;
													}

													if(jsonItems.attribute_filter_43){
														property_type = jsonItems.attribute_filter_43;
													}

													if(jsonItems.featured_image){
														image = IMG_URL_PROPERTY+jsonItems.featured_image;
													}else{
														image = arrImages[0];
													}

													obj_basic_details = {

														product_id    : data[0].id.toString(),
														name          : data[0].name,
														title         : data[0].title,
														price         : jsonItems.regular_price,
														currency      : retCurrency,
														property_type : property_type,
														image         : image,
														gallery       : arrImages,
														
													};

													Object.keys(arr_inner_obj_specification[0]).forEach(function(key,val) {

													  	obj_specification  = {
													  		label:key,
													  		value:arr_inner_obj_specification[0][key]
													    };
													    arr_obj_specification.push(obj_specification);
													});

													obj_company_details = {
														account_id  : retCompanyDetails.account_id,
														name        : retCompanyDetails.name,
														logo        : retCompanyDetails.logo,
														contact_country_code:retMobileCode,
														contact_no  : retCompanyDetails.contact_no,
														category_name : retCategory
													};

												  	objProduct = {

										        		basic_details   : obj_basic_details,
										        		company_details : obj_company_details,
					        			        		about_details   : {
					        			        			specification   : arr_obj_specification,
					        			        			property_extras : jsonItems.product_sections.split("~"),
				        			        				seller_comments : {
				        			        					description:jsonItems.seller_comments
				        			        				}
					        			        		},
					        			        		floor_plan      : {},
					        			        		map_area        : {},
					        			        		agency_agent    : {},
					        			        		financial_calc  : {},
					        			        		finance_prov    : {}
													};

													counter -= 1;
													if (counter === 0){ 
														resolve(objProduct);
													}
								
												})
											}],
											function (result, current) {
												if (counter === 0){ 
													return_result(result[0]);
												}
											});
										});
									});

								});
							});
						}

					});

				});

			}
			else
			{ return_result(null); }

		});

	}
	catch(err)
	{ console.log("getPropertyDetails",err); return_result(null);}
};


methods.getCompanyProfile =  function(arrParams,return_result){
	try
	{
			
		var arrAccounts = [];
		var queryAccountType = `SELECT account_id,account_type FROM jaze_user_basic_details WHERE account_id =?`;

		DB.query(queryAccountType,[arrParams.account_id], function(data, error){

			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var queryAccDetails = `SELECT * FROM jaze_user_basic_details WHERE account_id =? and account_type=?`;

				DB.query(queryAccDetails,[data[0].account_id,data[0].account_type], function(allData, error){
	
					if(typeof allData !== 'undefined' && allData !== null && parseInt(allData.length) > 0)
					{

						methods.convertMultiMetaArrayCompany(allData, function(retConverted){	

							var counter = retConverted.length;		
							var jsonItems = Object.values(retConverted);

							promiseForeach.each(jsonItems,[function (val) {
								return new Promise(function (resolve, reject) {		
							
							  		var default_logo = '';

									var newObj = {};

							  		if(parseInt(data[0].account_type) === 1)
							  		{
							  			if(val.profile_logo){
											default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + val.profile_logo + '';
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : (val.fname +" "+val.lname).toString(),
											logo		  : default_logo,
											contact_no    : val.mobile,
											contact_phone_code    : val.mobile_country_code_id,
											category_id   : val.category_id,
											country_id    : val.country_id,
											state_id      : val.state_id,
											city_id       : val.city_id,
											area_id       : val.area_id
										}	

							  		}
							  		else if(parseInt(data[0].account_type) === 2)
							  		{

			  							if(val.professional_logo){
											default_logo = G_STATIC_IMG_URL + "uploads/jazenet/professional_account/"+val.account_id+"/logo_thumb/thumb_" + val.professional_logo;
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : (val.fname +" "+val.lname).toString(),
											logo		  : default_logo,
											contact_no    : val.mobile,
											contact_phone_code    : val.mobile_country_code_id,
											category_id   : val.category_id,
											country_id    : val.country_id,
											state_id      : val.state_id,
											city_id       : val.city_id,
											area_id       : val.area_id
										}	

							  		}
						  			else if(parseInt(data[0].account_type) === 3)
					  				{

  				 						if(val.listing_images){
											default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/logo/' + val.company_logo + '';
										}else{
											default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/listing_images/' + val.listing_images + '';
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : val.company_name,
											logo		  : default_logo,
											contact_no    : val.company_phone,
											contact_phone_code : val.company_phone_code,
											category_id   : val.category_id,
											country_id    : val.country_id,
											state_id      : val.state_id,
											city_id       : val.city_id,
											area_id       : val.area_id
										}	
					  				}

									counter -= 1;
									if (counter === 0){ 
										resolve(newObj);
									}
									
								})
							}],
							function (result, current) {
								if (counter === 0){ 
									return_result(result[0]);
								}
							});

						});

					}
					else
					{
						return_result(null);
					}
				});

		
			}
			else
			{
				return_result(null);
			}
		});

	}
	catch(err)
	{ console.log("getCompanyProfile",err); return_result(null);}
};


// ---------- property details end -------------- //

// ---------- auto details start --------------- //

methods.getAutoDetails =  function(arrParams,return_result){
	try
	{
		var objProduct = {};
		var arrProduct = [];

		var obj_basic_details = {};
		var obj_company_details = {};

		var arr_obj_specification = [];
			var obj_specification = {};
			var arr_inner_obj_specification = [];
				var inner_obj_specification = {};

		var query = "SELECT * FROM jaze_products where id =?";

		DB.query(query,[arrParams.product_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				DB.query("SELECT * FROM jaze_products_meta WHERE product_id=?",[data[0].id], function(inData, error){

					const IMG_URL_PROPERTY = G_STATIC_IMG_URL+'uploads/jazenet/company/'+data[0].account_id+'/products/'+arrParams.product_id+'/thumb/';

					var accParams = {account_id:data[0].account_id};
					methods.getCompanyProfile(accParams, function(retCompanyDetails){	
							
						if(retCompanyDetails!=null)
						{
							methods.convertMultiMetaArrayProduct(inData, function(retProductDetails){	

								var mobParams = {account_type:retCompanyDetails.account_type,phonecode:retCompanyDetails.contact_phone_code};
								
								methods.__getMobileCode(mobParams, function(retMobileCode){	

									methods.getCurrency(data[0].currency_id, function(retCurrency){		

										methods.getCompanyCategory(retCompanyDetails.category_id, function(retCategory){		

											var counter = retProductDetails.length;		
											var jsonItems = Object.values(retProductDetails);

											var arrImages = [];
											var arrDefaultAttri = [];

											var price      = "";
											var year_model = "";
											var image      = "";
											var seller_comments = "";
											var additional_info = [];

											promiseForeach.each(jsonItems,[function (jsonItems) {
												return new Promise(function (resolve, reject) {		

													if(jsonItems.gallery_images){
														var photo_serialize = unserialize(jsonItems.gallery_images);
														Object.keys(photo_serialize).forEach(function(key) {
															arrImages.push(IMG_URL_PROPERTY+photo_serialize[key]);
														});
													}

													if(jsonItems.default_attributes){
														var default_attributes = unserialize(jsonItems.default_attributes);
														Object.keys(default_attributes).forEach(function(key) {
															arrDefaultAttri.push(default_attributes[key]);
														});

														arrDefaultAttri.forEach(function(values) {
												   			if(values.name){
																for(const [key, value] of Object.entries(values.attributes)) {
																	inner_obj_specification[values.name] = value;
																}
																arr_inner_obj_specification.push(inner_obj_specification);
												   			}
														});
													}
									
													if(jsonItems.sale_price && parseInt(jsonItems.sale_price) !== 0 ){
														price = jsonItems.sale_price;
													}else{
														price = jsonItems.regular_price;
													}

													if(jsonItems.featured_image){
														image = IMG_URL_PROPERTY+jsonItems.featured_image;
													}else{
														image = arrImages[0];
													}

													if(jsonItems.attribute_filter_66){
														year_model = jsonItems.attribute_filter_66;
													}

													obj_basic_details = {
														product_id    : data[0].id.toString(),
														name          : data[0].name,
														title         : data[0].title,
														price         : price,
														currency      : retCurrency,
														year_model    : year_model,
														image         : image,
														gallery       : arrImages,
														description   : data[0].content
													};

													Object.keys(arr_inner_obj_specification[0]).forEach(function(key) {
													  	obj_specification  = {
													  		label:key,
													  		value:arr_inner_obj_specification[0][key]
													    };
													    arr_obj_specification.push(obj_specification);
													});

													obj_company_details = {
														account_id  : retCompanyDetails.account_id,
														name        : retCompanyDetails.name,
														logo        : retCompanyDetails.logo,
														contact_country_code:retMobileCode,
														contact_no  : retCompanyDetails.contact_no,
														category_name :retCategory
													};

													if(jsonItems.product_sections!=""){
														additional_info = jsonItems.product_sections.split("~");
													}

													if(jsonItems.seller_comments){
														seller_comments = jsonItems.seller_comments;
													}

												  	objProduct = {
										        		basic_details   : obj_basic_details,
										        		company_details : obj_company_details,
					        			        		about_details   : {
					        			        			specification   : arr_obj_specification,
					        			        			additional_info : additional_info,
				        			        				seller_comments : {
				        			        					description:seller_comments
				        			        				}
					        			        		},
				        			        			financial_calc  : {},
	        			        			      		finance_prov    : {}
					        		
													};

													counter -= 1;
													if (counter === 0){ 
														resolve(objProduct);
													}
								
												})
											}],
											function (result, current) {
												if (counter === 0){ 
													return_result(result[0]);
												}
											});
										});
									});

								});
							});
						}

					});

				});

			}
			else
			{ return_result(null); }

		});

	}
	catch(err)
	{ console.log("getAutoDetails",err); return_result(null);}
};

// ---------- auto details end --------------- //



// ---------- product details start --------------- //
methods.getProductInfo =  function(arrParams,return_result){
	try
	{
		var objProduct = {};
		var arrProduct = [];

		var obj_basic_details = {};
		var obj_company_details = {};

		var query = "SELECT * FROM jaze_products where id =?";

		DB.query(query,[arrParams.product_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				DB.query("SELECT * FROM jaze_products_meta WHERE product_id=?",[data[0].id], function(inData, error){

					const IMG_URL_PROPERTY = G_STATIC_IMG_URL+'uploads/jazenet/company/'+data[0].account_id+'/products/'+arrParams.product_id+'/thumb/';

					var accParams = {account_id:data[0].account_id};
					methods.getCompanyProfile(accParams, function(retCompanyDetails){	
							
						if(retCompanyDetails!=null)
						{
							methods.convertMultiMetaArrayProduct(inData, function(retProductDetails){

								var mobParams = {account_type:retCompanyDetails.account_type,phonecode:retCompanyDetails.contact_phone_code};
								
								methods.__getMobileCode(mobParams, function(retMobileCode){	

									methods.getCurrency(data[0].currency_id, function(retCurrency){	

										var revParams = {product_id:data[0].id, flag:1};
										methods.getProductReviews(revParams, function(retReviews){	

											methods.getProductBrand(data[0].jaze_brand_id, function(retBrand){	

												var raitings = [];
												if(retReviews!=null){
													raitings = retReviews;
												}	

												var counter = retProductDetails.length;		
												var jsonItems = Object.values(retProductDetails);

												var arrImages     = [];
												var arrProdSpec   = [];
												var arrProdConfig = [];

												var price       = "";
												var sale_price  = "";
												var image       = "";
								
												var inner_obj_product_attriObj = {};
					
												promiseForeach.each(jsonItems,[function (jsonItems) {
													return new Promise(function (resolve, reject) {		

														methods.processProductSpecs(data[0].id,function(retSpecs){

															methods.processProductConfig(data[0].id,function(retConfig){

																methods.processProducVariants(data[0].id,function(retVariants){

																	if(jsonItems.gallery_images){
																		var photo_serialize = unserialize(jsonItems.gallery_images);
																		Object.keys(photo_serialize).forEach(function(key) {
																			arrImages.push(IMG_URL_PROPERTY+photo_serialize[key]);
																		});
																	}

										
																	var obj_price = {
																		is_offer_available : jsonItems.sale_onsale,
																		original_price     : jsonItems.regular_price,
																		offer_price        : jsonItems.sale_price,
																		price_unit         : retCurrency,
																		offer_expiration   : jsonItems.sale_price_start_to,
																	};

																	obj_basic_details = {
																		id              : data[0].id.toString(),
																		master_id       : data[0].parent_id.toString(),										
																		title           : data[0].title,
																		model           : '',
																		brand           : retBrand,
																		prod_short_desc : data[0].ribbon,
																		seller_name            : retCompanyDetails.name,
																		seller_logo            : retCompanyDetails.logo,
																		prod_long_description  : jsonItems.product_sections,
																		price_details          : obj_price,
																		buy_status     : '1',
																		store_status   : '1'
																	};

																  	objProduct = {
														        		product_details           : obj_basic_details,
														        		product_specifications    : retSpecs,
														        		product_variants          : retVariants,
														        		ratings_and_reviews       : raitings,
																		image_url                 : arrImages,
														        		product_configurations    : retConfig					        	
									        			        	
																	};

																	counter -= 1;
																	if (counter === 0){ 
																		resolve(objProduct);
																	}


																});

															});

														});
									
													})
												}],
												function (result, current) {

													if (counter === 0){ 
														return_result(result[0]);
													}

												});

											});

										});
									});

								});
							});
						}

					});

				});

			}
			else
			{ return_result(null); }

		});

	}
	catch(err)
	{ console.log("getProductInfo",err); return_result(null);}
};

methods.processProducVariants = function(group_id,return_result){

	try{

		var arrProdConfig = [];

		let query  = "SELECT * FROM jaze_products_meta WHERE product_id=? ";

		DB.query(query,[group_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArrayProduct(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							if(jsonItems.default_attributes){

								var product_configurations = unserialize(jsonItems.default_attributes);

								Object.keys(product_configurations).forEach(function(key) {

									var arrValues = [];
									var product_attri = product_configurations[key].attributes;
							

									Object.keys(product_attri).forEach(function(attri) {
							
										arrValues.push(product_attri[attri]);

									});

									
									var inner_obj_product_configurationObj = {
										title       : product_configurations[key].name,
										value       : arrValues.toString()
									}

									arrProdConfig.push(inner_obj_product_configurationObj);

								});

							}
									
							counter -= 1;
							if (counter === 0){ 
								resolve(arrProdConfig);
							}

						})
					}],
					function (result, current) {
						if (counter === 0){ 

							return_result(result[0]);
									
						}
					});

				});
			}
			else
			{return_result([])}
		});
	}
	catch(err)
	{
		return_result([]);
	}
};


methods.processProductSpecs = function(group_id,return_result){

	try{

		var arrProdSpec = [];
		let query  = "SELECT * FROM jaze_products_meta WHERE product_id=? ";

		DB.query(query,[group_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArrayProduct(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							if(jsonItems.attribute_filters){

								var product_specification = unserialize(jsonItems.attribute_filters);

								Object.keys(product_specification).forEach(function(key) {

						
									var inner_obj_product_attriObj = {
										label:product_specification[key],
										value:jsonItems['attribute_filter_'+key]
									}
							    
									arrProdSpec.push(inner_obj_product_attriObj);
									
								});

							}

							counter -= 1;
							if (counter === 0){ 
								resolve(arrProdSpec);
							}

						})
					}],
					function (result, current) {
						if (counter === 0){ 

							return_result(result[0]);
									
						}
					});

				});
			}
			else
			{return_result([])}
		});
	}
	catch(err)
	{
		return_result([]);
	}
};

methods.processProductConfig = function(group_id,return_result){

	try{

		var arrProdConfig = [];

		let query  = "SELECT * FROM jaze_products_meta WHERE product_id=? ";

		DB.query(query,[group_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArrayProduct(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							if(jsonItems.default_attributes){

								var product_configurations = unserialize(jsonItems.default_attributes);

								Object.keys(product_configurations).forEach(function(key) {


									var arrInnerSpec = [];
									var product_attri = product_configurations[key].attributes;


									Object.keys(product_attri).forEach(function(attri) {
							
										var inner_object_config = {
								        	title         : product_attri[attri],
							              	is_selected   : "0",
							             	item_id       : product_attri[attri].replace(/ /g, "_")+"_id",
							              	active_status : "1"
										}
										arrInnerSpec.push(inner_object_config);

									});

									var inner_obj_product_configurationObj = {
										attribute_label       : product_configurations[key].name,
										selected_config_value : '',
										attribute_items       : arrInnerSpec,
										config_id             : product_configurations[key].name+'_id'
									}
						    
									arrProdConfig.push(inner_obj_product_configurationObj);
								
								});

							}
									
							counter -= 1;
							if (counter === 0){ 
								resolve(arrProdConfig);
							}

						})
					}],
					function (result, current) {
						if (counter === 0){ 

							return_result(result[0]);
									
						}
					});

				});
			}
			else
			{return_result([])}
		});
	}
	catch(err)
	{
		return_result([]);
	}
};

methods.getProductBrand = function(brand_id,return_result){

	try{
			let query  = "SELECT * FROM jaze_jazestore_brand_list WHERE group_id=? ";

			DB.query(query,[brand_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					methods.convertMultiMetaArray(data, function(retConverted){		

						return_result(retConverted[0].name);

					});
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('');
	}
};


methods.getProductReviews =  function(arrParams,return_result){
	try
	{
		var query = `SELECT * FROM jaze_products_reviews WHERE group_id IN (SELECT group_id FROM jaze_products_reviews WHERE meta_key = 'product_id' AND meta_value =?)`;

		var arrResults = [];
		DB.query(query,[arrParams.product_id], function(data, error){
	 		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
							
							var accParams = {account_id:jsonItems.account_id};

							methods.getCompanyProfile(accParams,function(retAccount){

								if(retAccount!=null){

									methods.__getCountryDetails(retAccount.country_id,function(retCountry){

										methods.__getStateDetails(retAccount.state_id,function(retState){

											methods.__getCityDetails(retAccount.city_id,function(retCity){

												methods.__getAreaDetails(retAccount.area_id,function(retArea){

													arrObjects = {
														//id           : jsonItems.group_id.toString(),
														name         : retAccount.name,
														logo         : retAccount.logo,
														//country_id   : retAccount.country_id,
														location     : retCountry,
														//state_id     : retAccount.state_id,
														//state_name   : retState,
														//city_id      : retAccount.city_id,
														//city_name    : retCity,
														//area_id      : retAccount.area_id,
														//area_name    : retArea,
														rating       : jsonItems.rating.toString(),
														message      : jsonItems.comment,
														date         : jsonItems.created_date
													};


													counter -= 1;
													arrResults.push(arrObjects);
												
													if (counter === 0){ 
														resolve(arrResults);
													}

												});
											});
										});
									});
								}

						
							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							result[0].sort(function(a, b) {
								a = new Date(a.date);
								b = new Date(b.date);
								return a>b ? -1 : a<b ? 1 : 0;
							});

							if(parseInt(arrParams.flag) === 1){
								return_result(result[0].slice(0,3));
							}else{
								return_result(result[0]);
							}
							
						}
					});

				});

	 	   	}
	 	   	else
	 	   	{
				return_result(null);
	 	   	}
 	 	});

	}
	catch(err)
	{ console.log("getProductReviews",err); return_result(null);}
};

// ---------- product details end --------------- //


// ------------------ product filter --------------------------- //

methods.getAreaForFilter =  function(arrParams,return_result){
	try
	{

		if(parseInt( arrParams.city_id ) === 0 && parseInt( arrParams.state_id ) !== 0)
		{	
			DB.query(`SELECT * FROM cities WHERE state_id =?`,[arrParams.state_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					DB.query(`SELECT * FROM area WHERE city_id =?`,[data[0].id], function(cdata, error){

						if(typeof cdata !== 'undefined' && cdata !== null && parseInt(cdata.length) > 0){

							var counter = cdata.length;		
							var jsonItems = Object.values(cdata);
							var obj_area = {};
							var ret_array = [];

							promiseForeach.each(jsonItems,[function (jsonItems) {
								return new Promise(function (resolve, reject) {	

								
									counter -= 1;
									ret_array.push(jsonItems.area_id);

									if (counter === 0){ 
										resolve(ret_array);
									}
								})
							}],
							function (result, current) {
								if (counter === 0){ 

									return_result(result[0].sort(sortingArrayObject('area_name')));
						
											
								}
							});

						}
						else
						{
							return_result([]);
						}

					});

				}
				else
				{
					return_result([]);
				}

			});
		}
		else
		{
			DB.query(`SELECT * FROM area WHERE city_id =?`,[arrParams.city_id], function(cdata, error){

				if(typeof cdata !== 'undefined' && cdata !== null && parseInt(cdata.length) > 0){

					var counter = cdata.length;		
					var jsonItems = Object.values(cdata);
					var obj_area = {};
					var ret_array = [];

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {						

							counter -= 1;
							ret_array.push(jsonItems.area_id);

							if (counter === 0){ 
								resolve(ret_array);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							return_result(result[0].sort(sortingArrayObject('area_name')));
				
									
						}
					});

				}
				else
				{
					return_result([]);
				}

			});
		}
	}
	catch(err)
	{ console.log("getAreaForFilter",err); return_result([]);}
};

methods.getProductFilter =  function(arrParams,return_result){
	try
	{
		methods.getAreaForFilter(arrParams,function(retArea){

			var objProduct = {};
			var arrProducts = [];
			var arrProduct = [];

			var obj_basic_details = {};
			var obj_company_details = {};


			var product_configurations = arrParams.product_config.length;

			if(parseInt(product_configurations) > 0)
			{

				var query = "SELECT * FROM jaze_products WHERE id ='"+arrParams.product_id+"' OR parent_id='"+arrParams.master_id+"' ";

				DB.query(query,null, function(data, error){
						

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{

						var counter = data.length;		
						var jsonItems = Object.values(data);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		

								if(parseInt(jsonItems.parent_id) !== parseInt(jsonItems.product_id) ){
									arrProduct.push(jsonItems.id);
								}
										
								counter -= 1;
								if (counter === 0){ 
									resolve(arrProduct);
								}

							})
						}],
						function (result, current) {

							if (counter === 0)
							{ 
		
								var queryIn = "SELECT * FROM jaze_products_meta WHERE product_id IN("+result[0]+") ";
								var obj_filters = {};
								var arr_filters = [];
								arrParams.product_config.forEach(function(val){

									var config_id = val.config_id.split("_");
									var meta_key = "attribute_"+config_id[0];

									var string =  val.item_id;
									var replaced = string.slice(0, string.lastIndexOf('_'));
									var item_id = replaced.replace(/_/g, " ")+"_id";

					
									var new_item_id = item_id.split("_");
									var meta_value = new_item_id[0];

									obj_filters[val.config_id] = val.item_id;
									arr_filters.push(obj_filters);

									queryIn += "  AND product_id IN (SELECT product_id FROM jaze_products_meta WHERE meta_key = '"+meta_key+"' AND meta_value ='"+meta_value+"') ";

								});


							

								var status = "0";
								DB.query(queryIn,null, function(data, error){

									if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
									{		
										
										status = "1";
										methods.convertMultiMetaArrayProduct(data, function(retProductDetails){

											methods.processProductFilter(arr_filters[0],retProductDetails[0].product_id,arrParams,status,function(retProcess){
				 								return_result(retProcess);
											});
										});
									}
									else
									{
									
										methods.processProductFilter([],arrParams.master_id,arrParams,status,function(retProcess){
			 								return_result(retProcess);
										});
									}

								});
					

							}
						});
					}
					else
					{ return_result(null); }

				});



			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getProductFilter",err); return_result(null);}
};


methods.processProductFilter =  function(arr_filters,product_id,arrParams,status,return_result){
	try
	{


		var query = "SELECT * FROM jaze_products where id =?";

		DB.query(query,[product_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				DB.query("SELECT * FROM jaze_products_meta WHERE product_id=?",[arrParams.master_id], function(inData, error){

					const IMG_URL_PROPERTY = 'https://www.img.chaaby.com/images/master.resources/uploads/jazenet/company/'+data[0].account_id+'/products/'+arrParams.product_id+'/thumb/';

					var accParams = {account_id:data[0].account_id};
					methods.getCompanyProfile(accParams, function(retCompanyDetails){	
							
						if(retCompanyDetails!=null)
						{
							methods.convertMultiMetaArrayProduct(inData, function(retProductDetails){

								var mobParams = {account_type:retCompanyDetails.account_type,phonecode:retCompanyDetails.contact_phone_code};
								
								methods.__getMobileCode(mobParams, function(retMobileCode){	

									methods.getCurrency(data[0].currency_id, function(retCurrency){	

										var revParams = {product_id:arrParams.master_id, flag:1};
										methods.getProductReviews(revParams, function(retReviews){	

											methods.getProductBrand(data[0].jaze_brand_id, function(retBrand){	

												var raitings = [];
												if(retReviews!=null){
													raitings = retReviews;
												}	

												var counter = retProductDetails.length;		
												var jsonItems = Object.values(retProductDetails);

												var arrImages     = [];
												var arrProdSpec   = [];
												var arrProdConfig = [];

												var price       = "";
												var sale_price  = "";
												var image       = "";
								
												var inner_obj_product_attriObj = {};
					
												promiseForeach.each(jsonItems,[function (jsonItems) {
													return new Promise(function (resolve, reject) {		

														methods.processProductSpecs(arrParams.master_id,function(retSpecs){


															methods.processProductConfigFilter(arrParams.master_id,arr_filters,function(retConfig){

																methods.processProducVariantsFilter(arrParams.master_id,function(retVariants){


																	if(jsonItems.gallery_images){
																		var photo_serialize = unserialize(jsonItems.gallery_images);
																		Object.keys(photo_serialize).forEach(function(key) {
																			arrImages.push(IMG_URL_PROPERTY+photo_serialize[key]);
																		});
																	}
										
																	var obj_price = {
																		is_offer_available : jsonItems.sale_onsale,
																		original_price     : jsonItems.regular_price,
																		offer_price        : jsonItems.sale_price,
																		price_unit         : retCurrency
																	};

																	obj_basic_details = {
																		id              : data[0].id.toString(),												
																		master_id       : arrParams.master_id.toString(),												
																		title           : data[0].title,
																		model           : '',
																		brand           : retBrand,
																		prod_short_desc : data[0].ribbon,
																		seller_name            : retCompanyDetails.name,
																		seller_logo            : retCompanyDetails.logo,
																		prod_long_description  : jsonItems.product_sections,
																		offer_expiration       : jsonItems.sale_price_start_to,
																		price_details  : obj_price,
																		buy_status     : '1',
																		store_status   : '1'
																	};

																  	objProduct = {
																  		search_status             : status,
														        		product_details           : obj_basic_details,
														        		product_specifications    : retSpecs,
												        		   		product_variants          : retVariants,
														        		ratings_and_reviews       : raitings,
																		image_url                 : arrImages,
														        		product_configurations    : retConfig					        	
									        			        	
																	};

																	counter -= 1;
																	if (counter === 0){ 
																		resolve(objProduct);
																	}

																});
															});

														});
									
													})
												}],
												function (result, current) {

													if (counter === 0){ 
														return_result(result[0]);
													}

												});

											});

										});
									});

								});
							});
						}

					});

				});

			}
			else
			{
			 	return_result(null); 
			}

		});

	}
	catch(err)
	{ console.log("processProductFilter",err); return_result(null);}
};



methods.processProducVariantsFilter = function(group_id,return_result){

	try{

		var arrProdConfig = [];

		let query  = "SELECT * FROM jaze_products_meta WHERE product_id=? ";

		DB.query(query,[group_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArrayProduct(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							if(jsonItems.default_attributes){

								var product_configurations = unserialize(jsonItems.default_attributes);

								Object.keys(product_configurations).forEach(function(key) {

									var arrValues = [];
									var product_attri = product_configurations[key].attributes;
							

									Object.keys(product_attri).forEach(function(attri) {
							
										arrValues.push(product_attri[attri]);

									});

									
									var inner_obj_product_configurationObj = {
										title       : product_configurations[key].name,
										value       : arrValues.toString()
									}

									arrProdConfig.push(inner_obj_product_configurationObj);

								});

							}
									
							counter -= 1;
							if (counter === 0){ 
								resolve(arrProdConfig);
							}

						})
					}],
					function (result, current) {
						if (counter === 0){ 

							return_result(result[0]);
									
						}
					});

				});
			}
			else
			{return_result([])}
		});
	}
	catch(err)
	{
		return_result([]);
	}
};



methods.processProductConfigFilter = function(group_id,arr_filters,return_result){

	try{

		var arrProdConfig = [];
		let query  = "SELECT * FROM jaze_products_meta WHERE product_id=? ";
		DB.query(query,[group_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArrayProduct(data, function(retConverted){	

					if(retConverted[0].default_attributes){

						var product_configurations = unserialize(retConverted[0].default_attributes);

						Object.keys(product_configurations).forEach(function(key) {

							var arrInnerSpec = [];
							var product_attri = product_configurations[key].attributes;

							var clean_string;

							Object.keys(product_attri).forEach(function(attri) {

								var to_check = arr_filters[product_configurations[key].name+'_id'];
		
								if(typeof to_check !== 'undefined' && to_check !== null && parseInt(to_check.length) > 0){

									if(parseInt(Object.keys(arr_filters).length) > 0){

										var string_toclean = arr_filters[product_configurations[key].name+'_id'];
										var replaced = string_toclean.slice(0, string_toclean.lastIndexOf('_'));
										clean_string = replaced.replace(/_/g, " ")+"_id";
									}
								}

								var is_selected   = "0";
								var active_status = "0";
								if(clean_string == product_attri[attri]+"_id" )
								{
									active_status = "1"
									is_selected   = "1";
								}
					
								var inner_object_config = {
						        	title         : product_attri[attri],
					              	is_selected   : is_selected,
					             	item_id       : product_attri[attri].replace(/ /g, "_")+"_id",
					              	active_status : "1"
								}

								arrInnerSpec.push(inner_object_config);

							});

							var inner_obj_product_configurationObj = {
								attribute_label       : product_configurations[key].name,
								selected_config_value : '',
								attribute_items       : arrInnerSpec,
								config_id             : product_configurations[key].name+'_id'
							}
				    
							arrProdConfig.push(inner_obj_product_configurationObj);

						});
				
					}

					setTimeout(function(){ 
						return_result(arrProdConfig);								
				 	}, 150);

				});
			}
			else
			{return_result([])}
		});
	}
	catch(err)
	{
		return_result([]);
	}
};


// methods.processProductConfigFilter = function(group_id,arr_filters,return_result){

// 	try{

// 		var arrProdConfig = [];

// 		let query  = "SELECT * FROM jaze_products_meta WHERE product_id=? ";

// 		DB.query(query,[group_id], function(data, error){

// 			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

// 				methods.convertMultiMetaArrayProduct(data, function(retConverted){	

// 					var counter = retConverted.length;		
// 					var jsonItems = Object.values(retConverted);

// 					promiseForeach.each(jsonItems,[function (jsonItems) {
// 						return new Promise(function (resolve, reject) {		

// 							if(jsonItems.default_attributes){

// 								var product_configurations = unserialize(jsonItems.default_attributes);

// 								Object.keys(product_configurations).forEach(function(key) {

// 									var arrInnerSpec = [];
// 									var product_attri = product_configurations[key].attributes;

// 									Object.keys(product_attri).forEach(function(attri) {

// 										var is_selected   = "0";
// 										var active_status = "0";
// 										if(arr_filters[product_configurations[key].name+'_id'] == product_attri[attri]+"_id" )
// 										{
// 											active_status = "1"
// 											is_selected   = "1";
// 										}
							
// 										var inner_object_config = {
// 								        	title         : product_attri[attri],
// 							              	is_selected   : is_selected,
// 							             	item_id       : product_attri[attri]+"_id",
// 							              	active_status : "1"
// 										}

// 										arrInnerSpec.push(inner_object_config);

// 									});

// 									var inner_obj_product_configurationObj = {
// 										attribute_label       : product_configurations[key].name,
// 										selected_config_value : '',
// 										attribute_items       : arrInnerSpec,
// 										config_id             : product_configurations[key].name+'_id'
// 									}
						    
// 									arrProdConfig.push(inner_obj_product_configurationObj);
								
// 								});

// 							}
									
// 							counter -= 1;
// 							if (counter === 0){ 
// 								resolve(arrProdConfig);
// 							}

// 						})
// 					}],
// 					function (result, current) {
// 						if (counter === 0){ 
// 							return_result(result[0]);								
// 						}
// 					});

// 				});
// 			}
// 			else
// 			{return_result([])}
// 		});
// 	}
// 	catch(err)
// 	{
// 		return_result([]);
// 	}
// };


// ------------------------------ product filter end ----------------------------//


methods.getCompanyCategory =  function(company_category,return_result){
	try
	{

		var arrResults = [];
		var query = "SELECT * FROM jaze_category_list WHERE group_id =? ";
		DB.query(query,[company_category], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
				
							arrResults.push(jsonItems.cate_name);
							counter -= 1;
							if (counter === 0){ 
								resolve(arrResults[0]);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							return_result(result[0]);
						}
					});
				});
			}
			else
			{
				return_result("");
			}

		});
	}
	catch(err)
	{ console.log("getCompanyCategory",err); return_result("");}
};


methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].group_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}

methods.convertMultiMetaArrayProduct =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndexProduct(list,arrvalues[i].product_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							product_id : arrvalues[i].product_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMultiMetaArrayProduct",err); return_result(null);}
};


function __getGroupIndexProduct(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].product_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}

methods.convertMultiMetaArrayCompany =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndexCompany(list,arrvalues[i].account_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndexCompany(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].account_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}



function sortingArrayObject(key, order='asc') {

	return function(a, b) {
	  if(!a.hasOwnProperty(key) || 
		 !b.hasOwnProperty(key)) {
		  return 0; 
	  }
	  
	  const varA = (typeof a[key] === 'string') ? 
		a[key].toUpperCase() : a[key];
	  const varB = (typeof b[key] === 'string') ? 
		b[key].toUpperCase() : b[key];
		
	  let comparison = 0;
	  if (varA > varB) {
		comparison = 1;
	  } else if (varA < varB) {
		comparison = -1;
	  }
	  return (
		(order == 'desc') ? 
		(comparison * -1) : comparison
	  );
	};
}

function removeDuplicates(array) {
  return array.filter((a, b) => array.indexOf(a) === b)
};



function unserialize(data){  
    // Takes a string representation of variable and recreates it    
    //   
    // version: 810.114  
    // discuss at: http://phpjs.org/functions/unserialize  
    // +     original by: Arpad Ray (mailto:arpad@php.net)  
    // +     improved by: Pedro Tainha (http://www.pedrotainha.com)  
    // +     bugfixed by: dptr1988  
    // +      revised by: d3x  
    // +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)  
    // %            note: We feel the main purpose of this function should be to ease the transport of data between php & js  
    // %            note: Aiming for PHP-compatibility, we have to translate objects to arrays   
    // *       example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}');  
    // *       returns 1: ['Kevin', 'van', 'Zonneveld']  
    // *       example 2: unserialize('a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}');  
    // *       returns 2: {firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'}  
      
    var error = function (type, msg, filename, line){throw new window[type](msg, filename, line);};  
    var read_until = function (data, offset, stopchr){  
        var buf = [];  
        var chr = data.slice(offset, offset + 1);  
        var i = 2;  
        while(chr != stopchr){  
            if((i+offset) > data.length){  
                error('Error', 'Invalid');  
            }  
            buf.push(chr);  
            chr = data.slice(offset + (i - 1),offset + i);  
            i += 1;  
        }  
        return [buf.length, buf.join('')];  
    };  
    var read_chrs = function (data, offset, length){  
        buf = [];  
        for(var i = 0;i < length;i++){  
            var chr = data.slice(offset + (i - 1),offset + i);  
            buf.push(chr);  
        }  
        return [buf.length, buf.join('')];  
    };  
    var _unserialize = function (data, offset){  
        if(!offset) offset = 0;  
        var buf = [];  
        var dtype = (data.slice(offset, offset + 1)).toLowerCase();  
          
        var dataoffset = offset + 2;  
        var typeconvert = new Function('x', 'return x');  
        var chrs = 0;  
        var datalength = 0;  
          
        switch(dtype){  
            case "i":  
                typeconvert = new Function('x', 'return parseInt(x)');  
                var readData = read_until(data, dataoffset, ';');  
                var chrs = readData[0];  
                var readdata = readData[1];  
                dataoffset += chrs + 1;  
            break;  
            case "b":  
                typeconvert = new Function('x', 'return (parseInt(x) == 1)');  
                var readData = read_until(data, dataoffset, ';');  
                var chrs = readData[0];  
                var readdata = readData[1];  
                dataoffset += chrs + 1;  
            break;  
            case "d":  
                typeconvert = new Function('x', 'return parseFloat(x)');  
                var readData = read_until(data, dataoffset, ';');  
                var chrs = readData[0];  
                var readdata = readData[1];  
                dataoffset += chrs + 1;  
            break;  
            case "n":  
                readdata = null;  
            break;  
            case "s":  
                var ccount = read_until(data, dataoffset, ':');  
                var chrs = ccount[0];  
                var stringlength = ccount[1];  
                dataoffset += chrs + 2;  
                  
                var readData = read_chrs(data, dataoffset+1, parseInt(stringlength));  
                var chrs = readData[0];  
                var readdata = readData[1];  
                dataoffset += chrs + 2;  
                if(chrs != parseInt(stringlength) && chrs != readdata.length){  
                    error('SyntaxError', 'String length mismatch');  
                }  
            break;  
            case "a":  
                var readdata = {};  
                  
                var keyandchrs = read_until(data, dataoffset, ':');  
                var chrs = keyandchrs[0];  
                var keys = keyandchrs[1];  
                dataoffset += chrs + 2;  
                  
                for(var i = 0;i < parseInt(keys);i++){  
                    var kprops = _unserialize(data, dataoffset);  
                    var kchrs = kprops[1];  
                    var key = kprops[2];  
                    dataoffset += kchrs;  
                      
                    var vprops = _unserialize(data, dataoffset);  
                    var vchrs = vprops[1];  
                    var value = vprops[2];  
                    dataoffset += vchrs;  
                      
                    readdata[key] = value;  
                }  
                  
                dataoffset += 1;  
            break;  
            default:  
                error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);  
            break;  
        }  
        return [dtype, dataoffset - offset, typeconvert(readdata)];  
    };  
    return _unserialize(data, 0)[2];  
} 

methods.__getMobileCode= function (account_list,return_result){
	try
	{
		if(account_list !== null)
		{
			if(parseInt(account_list.account_type) === 1 || parseInt(account_list.account_type) === 2)
			{
				var query = " SELECT `phonecode` FROM `country` WHERE `id` = '"+account_list.phonecode+"'; ";
		
				DB.query(query, null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						return_result("+"+data[0].phonecode);
					}
					else
					{ return_result(''); }
				});
			}
			else if(parseInt(account_list.account_type) === 3)
			{
				if(parseInt(account_list.company_phone_code) !== 0)
				{
					var query = " SELECT `phonecode` FROM `country` WHERE `id` = '"+account_list.phonecode+"'; ";
				
					DB.query(query, null, function(data, error){
						
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_result("+"+data[0].phonecode);
						}
						else
						{ return_result(''); }

					});
				}
				else
				{
					return_result('');
				}
			}
			else
			{
				return_result('');
			}
			
		}
		else
		{
			return_result('');
		}
	}
	catch(err)
	{ console.log("getMobileCode",err); return_result(''); }	
};

methods.__getCountryDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT country_name FROM country  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getStateDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT state_name FROM states  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].state_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getCityDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT city_name FROM cities  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].city_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getAreaDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT area_name FROM area  WHERE area_id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].area_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

module.exports = methods;
