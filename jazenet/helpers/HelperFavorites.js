const methods = {};
const DB = require('../../helpers/db_base.js');
const HelperGeneralJazenet = require('../helpers/HelperGeneralJazenet.js');


const dateFormat = require('dateformat');
const now = new Date();
const standardDT = dateFormat(now,'yyyy-mm-dd HH:MM:ss');

// const express = require('express');
// const router = express.Router();


methods.checkAccount = function(arrParams,return_result){

	try
	{
		DB.query("select * from jaze_user_credential_details where account_id=? and status = '1'", [arrParams.fav_account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
				return_result(data);
			}else{
				return_result(null);
			}
		});
	}
	catch(err)
	{ 
		console.log("checkCredentialDetails",err); 
		return_result(null); 
	}	
};


methods.getFavoritesListAllDetails = function(arrParams,return_result){

	var arrRows = [];
	var newObj = {};
	var count = 500;

	var quer = `SELECT t1.* FROM(
			SELECT id,group_id,
			(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
			(SELECT b.meta_value FROM jaze_user_favorite_list b WHERE b.meta_key = 'fav_account_id' AND b.group_id = jaze_user_favorite_list.group_id) AS fav_account_id,
			(SELECT c.meta_value FROM jaze_user_favorite_list c WHERE c.meta_key = 'fav_account_type' AND c.group_id = jaze_user_favorite_list.group_id) AS fav_account_type,
			(SELECT d.meta_value FROM jaze_user_favorite_list d WHERE d.meta_key = 'fav_type' AND d.group_id = jaze_user_favorite_list.group_id) AS reminder_mins,
			(SELECT f.meta_value FROM jaze_user_favorite_list f WHERE f.meta_key = 'create_date' AND f.group_id = jaze_user_favorite_list.group_id) AS create_date
			FROM  jaze_user_favorite_list
	WHERE group_id IN (SELECT group_id FROM  jaze_user_favorite_list  WHERE meta_key = 'account_id' AND meta_value =? )
	) AS t1 WHERE account_id != ""`;

	DB.query(quer,[arrParams.account_id], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

			if(parseInt(data.length) > 5){
				count = parseInt(data.length)*100;
			}

			function delay() {
			  return new Promise(resolve => setTimeout(resolve, count));
			}

			async function delayedLog(item) {
		 	 	await delay();
				return_result(arrRows.sort(sortingArrayObject('fav_name')));
			}

			async function processArray(array) {
			 	array.forEach(async (val) => {

					var favParam = {account_id:val.fav_account_id };
					HelperGeneralJazenet.getAccountDetails(favParam, function(favRes){	
		
						if(favRes !== null)
						{
							newObj = {
						
								fav_id:val.group_id.toString(),
								fav_name:favRes.name,
								fav_logo:favRes.logo,
								fav_category:favRes.category,
								fav_email_address:favRes.jazemail_id,
								fav_account_id:val.fav_account_id,
								fav_account_type:val.fav_account_type,
								fav_type:val.fav_type,
								create_date:val.create_date
	
							};
							
							arrRows.push(newObj);
						}
					});


				    await delayedLog(arrRows);
			  	})
			 
			}

			processArray(data);

        }
        else
        { 
        	return_result(null);
        }

	});
};

methods.removeToFavorites = function(arrParams,return_result){

	try
	{

		var checkFav = `SELECT t1.* FROM(
		SELECT id,group_id
			FROM  jaze_user_favorite_list
		WHERE group_id IN (SELECT group_id FROM  jaze_user_favorite_list  WHERE meta_key = 'account_id' AND meta_value =? )
		AND group_id IN (SELECT group_id FROM  jaze_user_favorite_list  WHERE meta_key = 'fav_account_id' AND meta_value =? )
		) AS t1 LIMIT 1`;

		DB.query(checkFav,[arrParams.account_id,arrParams.fav_account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				DB.query(`DELETE FROM jaze_user_favorite_list where group_id=?`, [data[0].group_id], function(rData, error){
			 		if(rData !== null){
		 				return_result(1);
			 	   	}else{
						return_result(2);
			 	   	}
				});

	 	   	}
	 	   	else
	 	   	{
	 	   		return_result(0);
	 	   	}

		});
	}
	catch(err)
	{ console.log("removeToFavorites",err); return_result(2); }	
};



methods.addToFavorites = function(arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{	
						
			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_user_favorite_list";
  			var lastGrpId = 0;

			methods.getLastGroupId(queGroupMax, function(queGrpRes){

			  	queGrpRes.forEach(function(row) {
					lastGrpId = row.lastGrpId;
			  	});


				var arrInsert = {
					account_id:arrParams.account_id,
					fav_account_id:arrParams.fav_account_id,
					fav_account_type:arrParams.fav_account_type,
					fav_type:arrParams.fav_type,
					create_date:standardDT,
					status:1
				};
			
				var plusOneGroupID = lastGrpId+1;
		  		var querInsert = `insert into jaze_user_favorite_list (group_id, meta_key, meta_value) VALUES (?,?,?)`;

			 	for (var key in arrInsert) 
			 	{
					DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){

			 		 	if(data!==null){
							return_result(data.affectedRows)
				 	   	}else{
			   				return_result(0);
				 	   	}
			 	 	});
			  	};


			});
		}
		else
		{ 
			return_result(0); 
		}
	}
	catch(err)
	{ 
		console.log("addToFavorites",err); return_result(0); 
	}	
};


methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
}




function sortingArrayObject(key, order='asc') {

	return function(a, b) {
	  if(!a.hasOwnProperty(key) || 
		 !b.hasOwnProperty(key)) {
		  return 0; 
	  }
	  
	  const varA = (typeof a[key] === 'string') ? 
		a[key].toUpperCase() : a[key];
	  const varB = (typeof b[key] === 'string') ? 
		b[key].toUpperCase() : b[key];
		
	  let comparison = 0;
	  if (varA > varB) {
		comparison = 1;
	  } else if (varA < varB) {
		comparison = -1;
	  }
	  return (
		(order == 'desc') ? 
		(comparison * -1) : comparison
	  );
	};
}

module.exports = methods;