const methods = {};
const DB = require('../../helpers/db_base.js');
const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperGeneral = require('../../helpers/HelperGeneral.js');
var promiseForeach = require('promise-foreach');
const fast_sort = require("fast-sort");
//var str_replace = require('str_replace');
var rtrim = require('rtrim');
var ltrim = require('ltrim');

const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

//const array_unique = require('array-unique');

// ------------------------------------------------------  Home City Module Start  --------------------------------------------------------------//  

methods.getHomeCity = function(arrParams,return_result)
{
	try
	{
		var query  = " SELECT  con.id as country_id,con.country_name ,con.country_short_code,con.phonecode, st.id as state_id,st.state_name,city.id as city_id,city.city_name , ";
			query += " (select img_url from jaze_master_geo_bg_img_details where jaze_master_geo_bg_img_details.flag = 3 and jaze_master_geo_bg_img_details.status = 1 and jaze_master_geo_bg_img_details.geo_id = city.id ) as city_bg_image  ";
			query += " FROM cities city  ";
			query += " INNER JOIN states st ON  st.id = city.`state_id`  ";
			query += " INNER JOIN country con ON con.id = st.country_id  ";
			query += " WHERE city.status = 1 AND st.status = 1 AND con.status = 1 ";

			if(parseInt(arrParams.flag) === 0)
			{
				query += " and city.id in (34917,30611,32870,34250,31654,34732,32618,33437,34639,34952) ";
			}
			else if(parseInt(arrParams.flag) === 1)
			{
				query += " and city.id in (34919,34918,34959,34917,34920,34922,34921,34923) ";
			}

			DB.query(query, null, function (data, error) {
	
				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{	
					var counter = data.length;
										
					var array_rows = [];
									
					if(parseInt(counter) !== 0)
					{
						promiseForeach.each(data,[function (list) {
				
							return new Promise(function (resolve, reject) {

								var city_bg_image = "";

								var city_thumb_img = "";

								var city_tab_landscape_image = "";

								var city_tab_portrait_image = "";

								if(list.city_bg_image !== "" && list.city_bg_image !== null)
								{ 
									city_bg_image =  G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/'+list.city_bg_image.toString(); 

									city_thumb_img =  G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/thumb/'+ list.city_bg_image.toString(); 

									city_tab_landscape_image =  G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/tab_landscape/'+list.city_bg_image.toString(); 

									city_tab_portrait_image =  G_STATIC_IMG_URL +'uploads/homepage_bg/mob_bg/tab_portrait/'+list.city_bg_image.toString(); 
								}

								var newObj = {
										country_id	  				: list.country_id.toString(),
										country_name  				: list.country_name.toString(),
										state_id	  				: list.state_id.toString(),
										state_name    				: list.state_name.toString(),
										city_id	  					: list.city_id.toString(),
										city_name    				: list.city_name.toString(),
										city_bg_img  				: city_bg_image,
										city_thumb_img  			: city_thumb_img,
										city_tab_landscape_image  	: city_tab_landscape_image,
										city_tab_portrait_image  	: city_tab_portrait_image,
										area_id	  					: "",
										area_name    				: ""
									};

								array_rows.push(newObj);
										
								counter -= 1;
										
								resolve(array_rows);
							})
						}],
						function (result, current) {
														
							if (counter === 0)
							{ 
								fast_sort(result[0]).asc('city_name');

								return_result(result[0]); 
							}
						});
					}
					else
					{ return_result(null); }	
				}
				else
				{ return_result(null); }
			});
	}
	catch(err)
	{
		console.log(err);
		return_result(null);
	}
}

// ------------------------------------------------------  Home City Module End  --------------------------------------------------------------//  

// ------------------------------------------------------  Search Company  Module Start  --------------------------------------------------------------//   

methods.getAutofillCompanyResults = function(arrParams,return_result)
{
	var return_obj = {
		result : null,
		tot_count : "0",
		tot_page : "0"
	};

	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 1 && (parseInt(arrParams.geo_type) === 3 || parseInt(arrParams.geo_type) === 4 || parseInt(arrParams.geo_type) === 5 || parseInt(arrParams.geo_type) === 6))
		{	
				methods.__getCountCompany(arrParams.geo_type,arrParams,0,function(comp_loc_count){
					
					if(comp_loc_count.company_id != "")
					{
						var start = '0';

						if(parseInt(arrParams.limit) !== 0)
						{  start = parseInt(arrParams.limit) * 30; }
						
						var company_array = (comp_loc_count.company_id.split(",")).splice(start, 30); 

						var query  = " SELECT * FROM jaze_user_basic_details WHERE ( account_type = 3 or account_type = 5)  and account_id in ("+company_array.join()+") and  meta_key IN ('company_name','company_logo','category_id','city_id','area_id','is_merchant_type','latitude','longitude','is_request_type') ";
						
						DB.query(query, null, function (data, error) {

							if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
							{ 
								methods.convertAccountMultiMetaArray(data, function(data_result){
									
									var counter = data_result.length;
									
									var array_rows = [];

									if(parseInt(counter) !== 0)
									{
										promiseForeach.each(data_result,[function (list) {
							
											return new Promise(function (resolve, reject) {
				
												methods.___getCategoryJazecomDetails(list, function(company_category){	

													HelperGeneral.__getLocationDetails(list.area_id,list.city_id, function(location_details){	

														HelperGeneral.__getAccountJazenetId(list.account_id,list.account_type, function(jazenet_id){

															methods.__getCompanyQuotationProductStatus(list.account_id,company_category.pay_status, function(quotation_status){

																var default_logo = '' + G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png'+'';

																if(list.company_logo !== "" && list.company_logo !== null)
																{
																	default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+list.account_id+'/logo/' + list.company_logo + '';
																}

																var detail_flag = '1';

																if(parseInt(list.account_type) === 5)
																{ detail_flag = '8'; }

																var newObj = {
																	account_id	  : list.account_id.toString(),
																	account_type  : list.account_type.toString(),
																	name		  : list.company_name.toString(),
																	logo		  : default_logo.toString(),
																	jazenet_id 	  : jazenet_id.toString(),
																	geo_latitude  : list.latitude.toString(),
																	geo_longitude : list.longitude.toString(),
																	quotation_status   : quotation_status.toString(),
																	vacancies_status   : '1',
																	area_name	  : location_details.area_name,
																	city_name	  : location_details.city_name,
																	country_name  : location_details.country_name,
																	detail_flag   : detail_flag
																};
										
																Object.assign(newObj, company_category);

																array_rows.push(newObj);
																										
																counter -= 1;
																										
																resolve(array_rows);
															});
														});
													});
												});
											});
										}],
										function (result, current) {
											
											if (counter === 0)
											{  
												var obj = {
													result : result[0],
													tot_count : comp_loc_count.company_count.toString(),
													tot_page  : Math.ceil(parseInt(comp_loc_count.company_count) / 30)
												}

												return_result(obj); 
											}
										});
									}
									else
									{ return_result(return_obj); }	
								});
							}
							else
							{ return_result(return_obj); }
						});
					}
					else
					{ return_result(return_obj); }
				});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getCompanyLocalCount = function(arrParams,return_result)
{
	var return_obj = {
			local_count      	 : "0",
			national_count       : "0",
			international_count  : "0"					                        
		};

	try
	{
		//company count
		methods.__getCountCompany(3,arrParams,0,function(comp_loc_count){
			
			methods.__getCountCompany(2,arrParams,0,function(comp_nat_count){

				methods.__getCountCompany(5,arrParams,0,function(comp_inter_count){

					return_obj = {
						local_count             : comp_loc_count.company_count.toString(),
						national_count          : comp_nat_count.company_count.toString(),
						international_count     : comp_inter_count.company_count.toString()				                        
					};
								
					return_result(return_obj);

				});							
			});	
		});
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getAutofillCompanyInternationalNationalResults = function(arrParams,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 1 && (parseInt(arrParams.geo_type) === 1 || parseInt(arrParams.geo_type) === 2))
		{
			if(parseInt(arrParams.geo_type) === 1)
			{
				//International Result
				methods.__getCountCompany(5,arrParams,0,function(comp_count){

					if(comp_count.company_id != "")
					{
						methods.__getCompanyCountryList(comp_count.company_id,function(comp_inter_count){

							if(comp_inter_count !== null)
							{ 
								return_result(comp_inter_count,[]);
							}
							else
							{ return_result([],[]); }
						});
					}
					else
					{ return_result([],[]); }
				});
			}
			else if(parseInt(arrParams.geo_type) === 2)
			{
				//National Result
				methods.__getCountCompany(2,arrParams,0,function(comp_count){

					if(comp_count.company_id != "")
					{
						methods.__getCompanyStateList(comp_count.company_id,arrParams.country,arrParams.city,1,function(comp_nation_count){

							if(comp_nation_count !== null)
							{ 
								return_result([],comp_nation_count);
							}
							else
							{ return_result([],[]); }
						});
					}
					else
					{ return_result([],[]); }
				});
			}
			else
			{ return_result([],[]); }
		}
		else
		{ return_result([],[]); }
	}
	catch(err)
	{
		console.log(err);
		return_result([],[]);
	}	
}

//Get Company Country List
methods.__getCompanyCountryList = function (account_list,return_result){
	
	try
	{
		if(account_list !== null)
		{
			var query  = " SELECT meta_value as country_id, (SELECT `country_name` FROM `country` WHERE `status` = '1' and `id` = meta_value) as country_name  FROM jaze_user_basic_details WHERE  account_id in ("+account_list+") and meta_key in ('country_id','team_country_id') ";
			    query += " and account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key in ('status','team_status') and meta_value = '1') ";
				
				DB.query(query, null, function(arrvalues, error){
					
					if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0){

						var array_rows = [];

						var counter = arrvalues.length;
						
						if(parseInt(counter) !== 0)
						{
								promiseForeach.each(arrvalues,[function (list) {
									
									return new Promise(function (resolve, reject) {

										methods.__getCompanyStateList(account_list,list.country_id,0,0,function(comp_state_list){

											if(list.country_name !== "" && list.country_name !== null)
											{
												var index = __getCountryGroupIndex(array_rows,list.country_id,'country_id');

												if(index != ""){

													var count = parseInt(array_rows[index].country_count) + 1;
							
													Object.assign(array_rows[index], {country_count :  count.toString()});
												}
												else {
							
													var obj_return = {
															country_id    :  list.country_id.toString(),
															country_name  :  list.country_name,
															country_count :  "1",
															state_list	  : comp_state_list
														};
																	
													array_rows.push(obj_return);
												}
											}

											counter -= 1;

											resolve(array_rows);
										
										});	
									});
								}],
								function (result, current) {
									
									if (counter === 0)
									{ 
										var resul = result[0];

											fast_sort(resul).asc('country_name'); 

											var obj_return = {
												country_id    :  "0",
												country_name  :  "world",
												country_count :  arrvalues.length.toString(),
												state_list	  : []
												};
				
											resul.unshift(obj_return);

											return_result(resul); 
									}
								});	
						}
						else
						{return_result(null)}
					}
					else
					{ return_result(null); }
				});
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("__getCompanyCountryList",err); return_result(null); }	
};

//Get Company State List
methods.__getCompanyStateList = function (account_list,country_id,city_id,all_status,return_result){

	try
	{
		if(account_list !== null)
		{
			var query  = " SELECT meta_value as state_id, (SELECT `state_name` FROM `states` WHERE `status` = '1' and `id` = meta_value) as state_name  FROM jaze_user_basic_details WHERE  account_id in ("+account_list+") and meta_key in ('state_id','team_state_id') ";
			    query += " and account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key in ('status','team_status')  and meta_value = '1') ";
				
				if(parseInt(country_id) !== 0)
				{
					query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key in ('country_id','team_country_id')  AND meta_value = '"+country_id+"') ";
				}

				if(parseInt(city_id) !== 0)
				{
					query += " and  account_id NOT IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key in ('city_id','team_city_id')  AND meta_value = '"+city_id+"') ";
				}

				DB.query(query, null, function(arrvalues, error){
					
					if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0){

						if(parseInt(arrvalues[0].state_id) !== 0)
						{
							var list = [];

							for (var i in arrvalues) 
							{
								var index = __getCountryGroupIndex(list,arrvalues[i].state_id,'state_id');

								if(index != ""){

									var count = parseInt(list[index].state_count) + 1;

									Object.assign(list[index], {state_count :  count.toString()});
								}
								else {

									var obj_return = {
										state_id :  arrvalues[i].state_id.toString(),
										state_name :  arrvalues[i].state_name,
										state_count :  "1"
										};

									list.push(obj_return);
								}								
							}

							fast_sort(list).asc('state_name');

							if(parseInt(all_status) === 1)
							{
								var obj_return = {
										state_id    :  "0",
										state_name  :  "all",
										state_count :  arrvalues.length.toString()
								};

								list.unshift(obj_return);
							}

							return_result(list);
						}
						else
						{ return_result([]); }
					}
					else
					{ return_result([]); }
				});
		}
		else
		{
			return_result([]);
		}
	}
	catch(err)
	{ console.log("__getCompanyStateList",err); return_result([]); }	
};

function __getCountryGroupIndex(list_array,index_value,fild_key)
{
	var retern_index = "";

		if(list_array !== null)
		{
			Object.keys(list_array).forEach(function(prop) {

				var value = "";

				if(fild_key === "country_id")
				{ value = list_array[prop].country_id; }
				else if(fild_key === "state_id")
				{ value = list_array[prop].state_id; }
				else if(fild_key === "keyword")
				{ value = list_array[prop].keyword; }
				
 
				if(parseInt(value) === parseInt(index_value))
				{
					retern_index = prop;
				}
			  });
		}
		
		return retern_index;
}

//Get Company Category Name 
methods.__getCompanyCategoryName =  function (account_list,return_result){
	
	var return_obj = {
			account_type  : "",
			title_name 	  : "",
			category_name : ""
		};

	try
	{
		if(account_list !== null)
		{
			if(parseInt(account_list.account_type) === 1)
			{
				return_obj = {
					account_type  : "1",
					title_name 	  : "personal",
					category_name : "personal"
				};

				return_result(return_obj);
			}
			else if(parseInt(account_list.account_type) === 2)
			{
				return_obj = {
					account_type  : "2",
					title_name 	  : "professional",
					category_name : "professional"
				};

				return_result(return_obj);
			}
			else if(parseInt(account_list.account_type) === 3)
			{
				if((Object.keys(account_list)).indexOf("category_id") !== -1)
				{
					var query = "  SELECT meta_value  FROM jaze_category_list WHERE  meta_key = 'cate_name' and group_id = '"+account_list.category_id+"'; ";
					
					DB.query(query, null, function(data, error){
					
						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
							return_obj = {
								account_type  : "3",
								title_name 	  : "company",
								category_name : data[0].meta_value
							};

							return_result(return_obj);
						}
						else
						{
							return_obj = {
								account_type  : "3",
								title_name 	  : "company",
								category_name : "company"
							};

							return_result(return_obj);
						}
					});
				}
				else
				{
					return_obj = {
						account_type  : "3",
						title_name 	  : "company",
						category_name : "company"
					};

					return_result(return_obj);
				}
			}
			else if(parseInt(account_list.account_type) === 5)
			{
				return_obj = {
					account_type  : "5",
					title_name 	  : "government",
					category_name : "government"
				};

				return_result(return_obj);
			}
		}
		else
		{
			return_result(return_obj);
		}
	}
	catch(err)
	{ console.log("__getCompanyCategoryName",err); return_result(return_obj); }	
};

//Get Company Category Name and jazecom status
methods.___getCategoryJazecomDetails = function(account_list, return_result){

		var return_obj = {
			account_type  	  : "0",
			category  	  	  : '',
			category_type  	  : '0',
			title	  	      : '',
			online_status	  : "0",
			chat_status	  	  : "0",
			mail_status	  	  : "0",
			call_status	  	  : "0",
			feed_status	  	  : "0",
			diary_status	  : "0",
			reservation_status : "2"
		};

	try
	{
		if(account_list !== null)
		{
			if(parseInt(account_list.account_type) === 1)
			{
				return_obj.account_type = "1";
				return_obj.title 	  = "personal";
				return_obj.category  = "personal";

				return_result(return_obj);
			}
			else if(parseInt(account_list.account_type) === 2)
			{
				return_obj.account_type = "2";
				return_obj.title 	  = "professional";
				return_obj.category  = "professional";

				return_result(return_obj);
			}
			else if(parseInt(account_list.account_type) === 3)
			{
				return_obj.account_type = "3";

				if(parseInt(account_list.is_merchant_type) === 2 || parseInt(account_list.is_merchant_type) === 3)
				{ // jazefood
					var array_order =  { '1838' : '1','1839' : '2','1331' : '3','1354' : '4','1355' : '5'};

					return_obj.online_status = "1"; 

					return_obj.reservation_status = "1"; 

					return_obj.category_type   = array_order[account_list.category_id].toString();
				}
				else if(parseInt(account_list.is_merchant_type) === 1 )
				{ // jazestore
					return_obj.online_status = "2"; 
				}

				//get jazecom status
				methods.___getJazecomStatus(account_list.account_id, function(jazecom_status){ 

					if (typeof jazecom_status !== 'undefined' && jazecom_status !== null)
					{
						Object.assign(return_obj, jazecom_status);
					}

					//get category
					if((Object.keys(account_list)).indexOf("category_id") !== -1)
					{
						var query = "  SELECT meta_value  FROM jaze_category_list WHERE  meta_key = 'cate_name' and group_id = '"+account_list.category_id+"'; ";

						DB.query(query, null, function(data, error){

							if (typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
							{
								return_obj.title 	  = "company";
								return_obj.category  = data[0].meta_value;

								return_result(return_obj);
							}
							else
							{ 
								return_obj.title 	  = "company";
								return_obj.category  = "company";

								return_result(return_obj);
							}
						});
					}
					else
					{ 
						return_obj.title 	  = "company";
						return_obj.category  = "company";

						return_result(return_obj);
					}
				});
			}
			else if(parseInt(account_list.account_type) === 5)
			{
				return_obj.account_type = "1";
				return_obj.title 	  = "government";
				return_obj.category  = "government";

				return_result(return_obj);
			}
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{ console.log("___getJazefoodCategoryDetails",err); return_result(return_obj); }
};

methods.___getJazecomStatus= function(account_id,return_result)
{ 
	var return_obj = {
		chat_status	  	  : "0",
		mail_status	  	  : "0",
		call_status	  	  : "0",
		feed_status	  	  : "0",
		diary_status	  : "0",
		pay_status	  	  : "0"
	};

	try
	{
		if(parseInt(account_id) !== 0)
		{
			var query = "SELECT * FROM jaze_user_jazecome_status WHERE  meta_key not in ('account_id')";
				query += " and `group_id` IN (SELECT `group_id`  FROM jaze_user_jazecome_status WHERE `meta_key` = 'account_id' AND meta_value = '"+account_id+"' ) ";
					
				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
						HelperRestriction.convertMetaArray(data, function(statusRes){
							
							if(typeof statusRes !== 'undefined' && statusRes !== null )
							{
								return_obj = { 
										chat_status	  	  : statusRes.jaze_chat,
										mail_status	  	  : statusRes.jaze_mail,
										call_status	  	  : statusRes.jaze_call,
										feed_status	  	  : statusRes.jaze_feed,
										diary_status	  : statusRes.jaze_diary,
										pay_status	  	  : statusRes.jaze_pay,
									};

								return_result(return_obj);
							}
							else
							{ return_result(return_obj); }
						});
					}
					else
					{ return_result(return_obj); }
				});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{ console.log("___getJazecomStatus",err); return_result(return_obj); }	
}

methods.__getCompanyQuotationProductStatus = function(account_id,flag,return_result)
{
	try
	{
		if(parseInt(account_id) !== 0 && typeof flag !== 'undefined' && flag !== null && parseInt(flag) !== 0)
		{
			if(parseInt(flag) === 1)
			{
				var query = " SELECT count(id) as count  FROM `jaze_products` WHERE `account_id` = '"+account_id+"' ";

				DB.query(query, null, function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{ 
						if (parseInt(data[0].count) > 0)
						{  return_result('1'); }
						else
						{ return_result('2'); }
					}
					else
					{ return_result('2'); }
				});
			}
			else
			{ return_result('0'); }
		}
		else
		{ return_result('0'); }
	}
	catch(err)
	{
		console.log('__getCompanyQuotationProductStatus',err);
		return_result('0');
	}
}

// ------------------------------------------------------  Search Company  Module End  --------------------------------------------------------------//   

// ------------------------------------------------------  Search Profile  Module Start  --------------------------------------------------------------// 

methods.getAutofillProfileResults = function(arrParams,return_result)
{
	var return_obj = {
		result : null,
		tot_count : "0",
		tot_page : "0"
	};

	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 2 && (parseInt(arrParams.geo_type) === 3 || parseInt(arrParams.geo_type) === 4 || parseInt(arrParams.geo_type) === 5 || parseInt(arrParams.geo_type) === 6))
		{	
				methods.__getCountProfile(arrParams.geo_type,arrParams,function(prof_loc_count){
					
					if(prof_loc_count.profile_id != "")
					{
						var start = '0';

						if(parseInt(arrParams.limit) !== 0)
						{  start = parseInt(arrParams.limit) * 30; }
						
						var prof_array = (prof_loc_count.profile_id.split(",")).splice(start, 30); 

						var counter = prof_array.length;
									
						var array_rows = [];

						if(parseInt(counter) !== 0)
						{
							promiseForeach.each(prof_array,[function (list) {
							
								return new Promise(function (resolve, reject) {

									var obj = { account_id :list };
 
									HelperGeneral.getAccountDetails(obj,function(account_details){	

										methods.___getProfileJazecomDetails(account_details,function(jazecom_status){	
									
											var newObj = {
													account_id	  		: account_details.account_id.toString(),
													account_type  		: account_details.account_type.toString(),
													name		  		: account_details.name,
													logo		  		: account_details.logo,
													jazenet_id 	  		: account_details.jazenet_id,
													area_name	  		: account_details.area_name,
													city_name	  		: account_details.city_name,
													country_name  		: account_details.country_name,
												};
														
											Object.assign(newObj, jazecom_status);

											array_rows.push(newObj);

											counter -= 1;
																										
											resolve(array_rows);
										});
									});
								});
							}],
							function (result, current) {
								
								if (counter === 0)
								{  
									var obj = {
										result : result[0],
										tot_count : prof_loc_count.profile_count.toString(),
										tot_page  : Math.ceil(parseInt(prof_loc_count.profile_count) / 30)
									}

									return_result(obj); 
								}
							});
						}
						else
						{ return_result(return_obj); }
					}
					else
					{ return_result(return_obj); }
				});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getProfileLocalCount = function(arrParams,return_result)
{
	var return_obj = {
			local_count      	 : "0",
			national_count       : "0",
			international_count  : "0"					                        
		};

	try
	{
		//profile count
		methods.__getCountProfile(3,arrParams,function(prof_loc_count){
			
			methods.__getCountProfile(2,arrParams,function(prof_nat_count){

				methods.__getCountProfile(5,arrParams,function(prof_inter_count){

					return_obj = {
						local_count             : prof_loc_count.profile_count.toString(),
						national_count          : prof_nat_count.profile_count.toString(),
						international_count     : prof_inter_count.profile_count.toString()				                        
					};
						
					return_result(return_obj);

				});							
			});	
		});
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getAutofillProfileInternationalNationalResults = function(arrParams,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 2 && (parseInt(arrParams.geo_type) === 1 || parseInt(arrParams.geo_type) === 2))
		{
			if(parseInt(arrParams.geo_type) === 1)
			{
				//International Result
				methods.__getCountProfile(5,arrParams,function(prof_count){

					if(prof_count.profile_id != "")
					{
						methods.__getCompanyCountryList(prof_count.profile_id,function(prof_inter_count){

							if(prof_inter_count !== null)
							{ 
								return_result(prof_inter_count,[]);
							}
							else
							{ return_result([],[]); }
						});
					}
					else
					{ return_result([],[]); }
				});
			}
			else if(parseInt(arrParams.geo_type) === 2)
			{
				//National Result
				methods.__getCountProfile(2,arrParams,function(prof_count){

					if(prof_count.profile_id != "")
					{
						methods.__getCompanyStateList(prof_count.profile_id,arrParams.country,arrParams.city,1,function(prof_nation_count){

							if(prof_nation_count !== null)
							{ 
								return_result([],prof_nation_count);
							}
							else
							{ return_result([],[]); }
						});
					}
					else
					{ return_result([],[]); }
				});
			}
			else
			{ return_result([],[]); }
		}
		else
		{ return_result([],[]); }
	}
	catch(err)
	{
		console.log(err);
		return_result([],[]);
	}	
}

methods.___getProfileJazecomDetails = function(arrParams,return_result)
{ 
	var return_obj = {
		chat_status	  	  : "0",
		mail_status	  	  : "0",
		call_status	  	  : "0",
		diary_status	  : "0",
		category 		  : "professional",
		title 			  : "",
		show_profile	  : "0",
		profile_about	  : "",
		detail_flag 	  : "0",
		professional_id	  : "0"
	};

	try
	{
		if(parseInt(arrParams.account_type) === 4)
		{
			var query = " SELECT *  FROM jaze_user_basic_details WHERE  meta_key in ('team_feed_status','team_position','team_chat_status','team_diary_status','team_mail_status','team_call_status','team_use_proff_profile','team_user_link_account_id') and account_id = '" + arrParams.account_id + "' " ;
				query += "and account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key = 'team_status' and meta_value = '1') ";

					DB.query(query, null, function(data, error){

						if (typeof data !== undefined && data !== null && parseInt(data.length) > 0)
						{
							var list = [];
									
							for (var i in data)
							{
								Object.assign(list, {[data[i].meta_key]:  data[i].meta_value});
							}

							methods.___getProfProfileAboutDetails(list.team_user_link_account_id,function(profile_about){	

							HelperGeneral.getAccountTypeDetails(list.team_user_link_account_id,function(professional_id_type){	

								var show_profile = list.team_use_proff_profile;

								if(parseInt(professional_id_type) === 1)
								{ show_profile = '0'; }

									return_obj = {
										chat_status	  	  : list.team_chat_status.toString(),
										mail_status	  	  : list.team_mail_status.toString(),
										call_status	  	  : list.team_call_status.toString(),
										diary_status	  : list.team_diary_status.toString(),
										category 		  : list.team_position,
										title 			  : arrParams.company_name,
										show_profile	  : show_profile.toString(),
										professional_id   : list.team_user_link_account_id.toString(),
										profile_about	  : profile_about,
										detail_flag 	  : "15"
									};
									
									return_result(return_obj); 
								});
							});
						}
						else
						{ return_result(return_obj); }
					});
		}
		else
		{ 
			var query = "SELECT * FROM jaze_user_jazecome_status WHERE  meta_key not in ('account_id')";
				query += " and `group_id` IN (SELECT `group_id`  FROM jaze_user_jazecome_status WHERE `meta_key` = 'account_id' AND meta_value = '"+arrParams.account_id+"' ) ";
					
				DB.query(query, null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
						HelperRestriction.convertMetaArray(data, function(statusRes){
							
							if(typeof statusRes !== 'undefined' && statusRes !== null )
							{
								return_obj = { 
										chat_status	  	  : statusRes.jaze_chat,
										mail_status	  	  : statusRes.jaze_mail,
										call_status	  	  : statusRes.jaze_call,
										diary_status	  : statusRes.jaze_diary,
										category 		  : arrParams.title, 
										title 			  : arrParams.category,
										show_profile	  : "0",
										professional_id   : "0",
										profile_about	  : "",
										detail_flag 	  : "2"
									};

								return_result(return_obj);
							}
							else
							{ return_result(return_obj); }
						});
					}
					else
					{ return_result(return_obj); }
				});
		}
	}
	catch(err)
	{ console.log("___getWorkProfileJazecomStatus",err); return_result(return_obj); }	
}

methods.___getProfProfileAboutDetails = function(account_id,return_result)
{ 
	try
	{
		if(parseInt(account_id) !== 0)
		{
			var query = " SELECT meta_value  FROM jaze_user_basic_details WHERE  meta_key in ('professional_about') and account_id = '" + account_id + "' and account_type = '2' " ;
				query += "and account_id in (SELECT account_id  FROM jaze_user_basic_details WHERE  meta_key = 'status' and meta_value = '1') ";

					DB.query(query, null, function(data, error){

						if (typeof data !== undefined && data !== null && parseInt(data.length) > 0)
						{
							return_result(entities.encode(data[0].meta_value).substr(0,200) + "..."); 
						}
						else
						{ return_result(''); }
					});
		}
		else
		{ return_result(''); }
	}
	catch(err)
	{ console.log("___getProfProfileAboutDetails",err); return_result(''); }	
}

// ------------------------------------------------------  Search Profile  Module End  --------------------------------------------------------------// 

// ------------------------------------------------------  Search Products  Module Start  --------------------------------------------------------------// 

methods.__getCountProductsCategory  = function(arrParams,return_result){

	try{

		var query  = " SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key = 'status'  ";
			query += " and  group_id IN  (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key = 'status'  AND meta_value = '1') ";
			query += " and  group_id IN  (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key IN ('cate_name')  AND LOWER(meta_value) like '%"+arrParams.keyword.toLowerCase()+"%') ";
			query += " group by group_id  ";

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						return_result(data.length);
					}
					else
					{return_result('0')}
				});
	}
	catch(err)
	{
		return_result('0')
	}
};

methods.__getCountProducts  = function(flag,arrParams,return_result){

	// flag  :  0 = total, 1= international,  2 = national,  3 = local, 4 = selected state result, 5 = international company , 6 = national company 

		var return_obj = {
				prod_count      : "0",
				prod_id         : ""					                        
			};

		try
		{
			var keyword = rtrim(ltrim(arrParams.keyword.toLowerCase()));

			var query  = " SELECT group_concat(id) as id FROM jaze_master_search_keyword_list WHERE type = 6 and  status  = 1 ";
				query += " and (keyword like '%"+ keyword+"%' or keyword like '%"+ keyword.replace(' ', '')+"%' or  Replace(coalesce(keyword,''), ' ','') LIKE '%"+keyword+"%') ";


			if(parseInt(flag) !== 0)
			{
				if(parseInt(arrParams.country) !== 0 &&  parseInt(arrParams.city) !== 0 && parseInt(flag) === 1)
				{
					query += " and  country_id = '"+arrParams.country+"' ";
					query += " and  city_id != '"+arrParams.city+"' ";
				}
				else if(parseInt(arrParams.country) !== 0 && parseInt(arrParams.city) !== 0 && parseInt(flag) === 2)
				{
					query += " and  country_id = '"+arrParams.country+"' ";
					query += " and  city_id != '"+arrParams.city+"' ";
				}
				else if(parseInt(arrParams.city) !== 0 && parseInt(flag) === 3)
				{
					query += " and  city_id = '"+arrParams.city+"' ";
				}
				else if(parseInt(arrParams.geo_child_id) !== 0 &&  parseInt(flag) === 4)
				{
					query += " and  state_id = '"+arrParams.geo_child_id+"' ";
				}
				else if(parseInt(arrParams.country) !== 0 &&  parseInt(flag) === 5)
				{
					query += " and  country_id != '"+arrParams.country+"' ";
				}
				else if(parseInt(arrParams.state) !== 0 &&  parseInt(flag) === 6)
				{
					query += " and  country_id = '"+arrParams.country+"' ";
					query += " and  state_id != '"+arrParams.state+"' ";
				}
				else 
				{
					query += " and  id  = 0 ";
				}
			}

				DB.query(query,null, function(data, error){

					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						if(data[0].id !== '' && data[0].id !== null){

							return_obj = {
								prod_count      : (__convert_array(data[0].id)).length,
								prod_id         : data[0].id				                        
								};

							return_result(return_obj);
						}
						else
						{ return_result(return_obj); }
					}
					else
					{ return_result(return_obj); }
				});
		}
		catch(err)
		{ return_result(return_obj); }
};

methods.getProductLocalCount = function(arrParams,return_result)
{
	var return_obj = {
			local_count      	 : "0",
			national_count       : "0",
			international_count  : "0"					                        
		};

	try
	{
		//profile count
		methods.__getCountProductsCategory(arrParams,function(p_category_t_count){

			methods.__getCountProducts(3,arrParams,function(prod_loc_count){
				
				methods.__getCountProducts(2,arrParams,function(prod_nat_count){

					methods.__getCountProducts(5,arrParams,function(prod_inter_count){

						var local_count = parseInt(p_category_t_count) + parseInt(prod_loc_count.prod_count);

						return_obj = {
							local_count             : local_count.toString(),
							national_count          : prod_nat_count.prod_count.toString(),
							international_count     : prod_inter_count.prod_count.toString()				                        
						};
							
						return_result(return_obj);

					});							
				});	
			});
		});
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}


methods.getAutofillProductCategoryResults = function(arrParams,return_result)
{
	var return_obj = {
		result : null,
		tot_count : "0",
		tot_page : "0"
	};

	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 3 && (parseInt(arrParams.geo_type) === 3 || parseInt(arrParams.geo_type) === 4 || parseInt(arrParams.geo_type) === 5 || parseInt(arrParams.geo_type) === 6))
		{	
			methods.getAutofillJazeProductCategoryResults(arrParams, function(jaze_cat_list){	

				methods.getAutofillJazeProductResults(arrParams, function(jaze_prod_list){

					var result_result = [];

					if(jaze_cat_list.result !== "" && jaze_cat_list.result !== null)
					{ result_result = result_result.concat(jaze_cat_list.result); }
					
					if(jaze_prod_list.result !== "" && jaze_prod_list.result !== null)
					{ result_result = result_result.concat(jaze_prod_list.result); }

					if (result_result !== null && parseInt(result_result.length) !== 0)
					{ 
						return_obj.result = result_result; // sort_name(result_result,arrParams.keyword);

						return_obj.tot_count = result_result.length.toString();
					}

					return_result(return_obj);

				});
			});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getAutofillJazeProductCategoryResults = function(arrParams,return_result)
{
	var return_obj = {
		result : null,
		tot_count : "0"
	};

	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 3)
		{	
			var query = "  SELECT  * FROM  jaze_jazestore_category_list where ";
				query += " group_id IN  (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key = 'status'  AND  meta_value = '1') ";
				query += " and group_id IN  (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key IN ('cate_name')  AND LOWER(meta_value) like '%"+arrParams.keyword.toLowerCase()+"%') ";

				DB.query(query, null, function (data, error) {
					
					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{
						HelperRestriction.convertMultiMetaArray(data, function (data_result) {

							var counter = data_result.length;
						
							var array_rows = [];

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data_result,[function (list) {
							
									return new Promise(function (resolve, reject) {

										methods.__getProductsCategoryParentName(list.parent_id, function(parent_cat_list){	

											methods.__getProductsCategoryChildStatus(1,list, function(detail_flag){	

												var parent_name = "jazestore";

												if(parent_cat_list !== null)
												{ parent_name = parent_cat_list[0].cate_name.toString(); }

												var newObj = {
														id	  	      : list.group_id.toString(),
														name	      : list.cate_name.toString(),
														logo	      : G_STATIC_IMG_URL +'uploads/jazenet/category/jazestore/'+list.image_url,
														cat_name	  : parent_name.toString(),
														detail_flag   : detail_flag.toString()
												};
																														
												array_rows.push(newObj);
																																													
												counter -= 1;
																	
												if(parseInt(counter) === 0)
												{ resolve(array_rows); }	

											});
										});
									});
								}],
								function (result, current) {
																			
									if (counter === 0)
									{ 	 
										var obj = {
												result : result[0],
												tot_count : data_result.length.toString(),
										}
								
										return_result(obj); 
									}
								});
							}
							else
							{ return_result(return_obj); }
						});
					}
					else
					{ return_result(return_obj); }
				});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getAutofillJazeProductResults = function(arrParams,return_result)
{
	var return_obj = {
		result : null,
		tot_count : "0"
	};

	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 3 && (parseInt(arrParams.geo_type) === 3 || parseInt(arrParams.geo_type) === 4 || parseInt(arrParams.geo_type) === 5 || parseInt(arrParams.geo_type) === 6))
		{	
			methods.__getCountProducts(arrParams.geo_type,arrParams,function(prod_loc_count){
					
				if(prod_loc_count.prod_id != "")
				{					
					var query  =" SELECT id,account_id,title,jaze_category_id,jaze_brand_id FROM jaze_products   WHERE id in ("+prod_loc_count.prod_id+") ";

					DB.query(query, null, function (data, error) {
					
						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
						{
							var counter = data.length;
										
							var array_rows = [];

							if(parseInt(counter) !== 0)
							{
								promiseForeach.each(data,[function (list) {
									
									return new Promise(function (resolve, reject) {
																		
										methods.__getProductsBrandName(list.jaze_brand_id, function(brand_name){	

											methods.__getProductsFeaturedImage(list.account_id,list.id, function(featured_image){	
													
												methods.__getProductsCategoryChildStatus(2,list, function(detail_flag){	

													var newObj = {
															id	  	      : list.id.toString(),
															name	      : list.title.toString(),
															logo	      : featured_image,
															cat_name	  : brand_name,
															detail_flag   : detail_flag.toString()
													};
																							
													array_rows.push(newObj);
																																						
													counter -= 1;
																																						
													resolve(array_rows);
												});
											});
										});
									});
								}],
								function (result, current) {
																	
									if (counter === 0)
									{ 
										var obj = {
											result : result[0],
											tot_count : data.length.toString(),
										}
						
										return_result(obj); 
									}
								});
							}
							else
							{ return_result(return_obj); }	
						}
						else
						{ return_result(return_obj); }
					});
				}
				else
				{ return_result(return_obj); }
			});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getAutofillProductInternationalNationalResults = function(arrParams,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 3 && (parseInt(arrParams.geo_type) === 1 || parseInt(arrParams.geo_type) === 2))
		{
			if(parseInt(arrParams.geo_type) === 1)
			{
				//International Result
				methods.__getCountProducts(5,arrParams,function(prod_list){

					if(prod_list.prod_id != "")
					{
						methods.__getProductCountryList(prod_list.prod_id,function(prod_inter_count){

							if(prod_inter_count !== null)
							{ 
								return_result(prod_inter_count,[]);
							}
							else
							{ return_result([],[]); }
						});
					}
					else
					{ return_result([],[]); }
				});
			}
			else if(parseInt(arrParams.geo_type) === 2)
			{
				//National Result
				methods.__getCountProducts(2,arrParams,function(prod_list){

					if(prod_list.prod_id != "")
					{
						methods.__getProductStateList(prod_list.prod_id,arrParams.country,arrParams.city,1,function(prod_nation_count){

							if(prod_nation_count !== null)
							{ 
								return_result([],prod_nation_count);
							}
							else
							{ return_result([],[]); }
						});
					}
					else
					{ return_result([],[]); }
				});
			}
			else
			{ return_result([],[]); }
		}
		else
		{ return_result([],[]); }
	}
	catch(err)
	{
		console.log(err);
		return_result([],[]);
	}	
}

//Get Product Country List
methods.__getProductCountryList = function (prod_list,return_result){

	try
	{
		if(prod_list !== null)
		{
			var query  =" SELECT pr.country_id, c.country_name FROM jaze_products pr  inner join country c on c.id = pr.country_id  WHERE c.status = '1' and pr.id in ("+prod_list+") ";

				DB.query(query, null, function(arrvalues, error){
					
					if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0){

						var array_rows = [];

					//	array_rows.push(obj_return);

						var counter = arrvalues.length;
						
						if(parseInt(counter) !== 0)
						{
								promiseForeach.each(arrvalues,[function (list) {

									return new Promise(function (resolve, reject) {

										methods.__getProductStateList(prod_list,list.country_id,0,0,function(prod_state_list){

											var index = __getCountryGroupIndex(array_rows,list.country_id,'country_id');

											if(index != ""){

												var count = parseInt(array_rows[index].country_count) + 1;
						
												Object.assign(array_rows[index], {country_count :  count.toString()});
											}
											else {
						
												var obj_return = {
														country_id    :  list.country_id.toString(),
														country_name  :  list.country_name,
														country_count :  "1",
														state_list	  : prod_state_list
													};
																
												array_rows.push(obj_return);
											}

											counter -= 1;

											resolve(array_rows);
										
										});	
									});
								}],
								function (result, current) {
									
									if (counter === 0)
									{ 
										var resul = result[0];

										fast_sort(resul).asc('country_name'); 

										var obj_return = {
											country_id    :  "0",
											country_name  :  "world",
											country_count :  arrvalues.length.toString(),
											state_list	  : []
											};
			
										resul.unshift(obj_return);

										return_result(resul);  
									}
								});	
						}
						else
						{return_result(null)}
					}
					else
					{ return_result(null); }
				});
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("__getProductCountryList",err); return_result(null); }	
};

//Get Product State List
methods.__getProductStateList = function (prod_list,country_id,city_id,all_status,return_result){

	try
	{
		if(prod_list !== null)
		{
			var query  =" SELECT pr.state_id, s.state_name FROM jaze_products pr  inner join states s on s.id = pr.state_id  WHERE s.status = '1' and pr.id in ("+prod_list+") ";

				if(parseInt(country_id) !== 0)
				{
					query += " and  pr.country_id = '"+country_id+"' ";
				}

				if(parseInt(city_id) !== 0)
				{
					query += " and  pr.city_id != '"+city_id+"' ";
				}

				DB.query(query, null, function(arrvalues, error){
					
					if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0){

						if(parseInt(arrvalues[0].state_id) !== 0)
						{
							var list = [];

							for (var i in arrvalues) 
							{
								var index = __getCountryGroupIndex(list,arrvalues[i].state_id,'state_id');

								if(index != ""){

									var count = parseInt(list[index].state_count) + 1;

									Object.assign(list[index], {state_count :  count.toString()});
								}
								else {

									var obj_return = {
										state_id :  arrvalues[i].state_id.toString(),
										state_name :  arrvalues[i].state_name,
										state_count :  "1"
										};

									list.push(obj_return);
								}								
							}

							fast_sort(list).asc('state_name');

							if(parseInt(all_status) === 1)
							{
								var obj_return = {
										state_id    :  "0",
										state_name  :  "all",
										state_count :  arrvalues.length.toString()
								};

								list.unshift(obj_return);
							}

							return_result(list);
						}
						else
						{ return_result([]); }
					}
					else
					{ return_result([]); }
				});
		}
		else
		{
			return_result([]);
		}
	}
	catch(err)
	{ console.log("__getProductStateList",err); return_result([]); }	
};

//Get Category Parent Name 
methods.__getProductsCategoryParentName =  function (category_id,return_result){

	try
	{
		var result_array = [];

		if(parseInt(category_id) === 0)
		{
			var obj_return = {
				cate_id 	:  '0',
				cate_name   :  'jazestore',
				parent_id   :  '0'
			};

			result_array.push(obj_return);

			return_result(result_array);
		}
		else if(parseInt(category_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_jazestore_category_list WHERE  meta_key IN ('cate_name','parent_id')  ";
				query += " and group_id IN  (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key = 'status'  AND meta_value = '1') ";
				query += " and group_id = '"+category_id+"' ";
				
				DB.query(query, null, function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{
						HelperRestriction.convertMultiMetaArray(data, function(data_result){
							
							if (typeof data_result !== 'undefined' && data_result !== null && parseInt(data_result.length) !== 0)
							{
								var obj_return = {
									cate_id 	:  data_result[0].group_id,
									cate_name   :  data_result[0].cate_name,
									parent_id  :  data_result[0].parent_id
								};

								result_array.push(obj_return);
									
								if(parseInt(data_result[0].parent_id) !== 0)
								{  
									methods.__getProductsCategoryParentName(data_result[0].parent_id, function(parent_result){	

										if(parent_result !== null)
										{ result_array = result_array.concat(parent_result); }

										return_result(result_array);
									});
								}
								else
								{ return_result(result_array); }
							}
							else
							{ return_result(null); }
						});
					}
					else
					{ return_result(null); }
				});
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("__getProductsCategoryParentName",err); return_result(null); }	
};

//Get Category Child Status 
methods.__getProductsCategoryChildStatus  =  function (flag,details,return_result){

	try
	{
		if(parseInt(flag) === 1)
		{
			//return_result : 6 = service category, 7 = service listing, 9 = auto, 10 = property 
			if(parseInt(details.group_id) !== 0)
			{
				var query  = " SELECT group_id FROM jaze_jazestore_category_list WHERE  ";
					query += " group_id IN  (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key = 'status'  AND meta_value = '1' ";
					query += " and group_id IN  (SELECT group_id FROM jaze_jazestore_category_list WHERE meta_key = 'parent_id'  AND meta_value = '"+details.group_id+"')) ";
					query += " group by  group_id ";
					
					DB.query(query, null, function (data, error) {

						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
						{ return_result('3'); }
						else
						{ return_result('4'); }
					});
			}
			else
			{ return_result('3'); }
		}
		else if(parseInt(flag) === 2)
		{ 
			var auto_cat = [57,58,59];

			var prod_cat = [209,210];

			var grocery_cat = [1841,155];

			var restaurants_cat = [1841];

			if(auto_cat.indexOf(parseInt(details.jaze_category_id)) !== -1)
			{  
				return_result('9'); 
			}
			else if(prod_cat.indexOf(parseInt(details.jaze_category_id)) !== -1)
			{  
				return_result('10'); 
			}
			else if(grocery_cat.indexOf(parseInt(details.jaze_category_id)) !== -1)
			{  
				return_result('16'); 
			}
			else if(restaurants_cat.indexOf(parseInt(details.jaze_category_id)) !== -1)
			{  
				return_result('18'); 
			}
			else
			{
				return_result('5'); 
			}
		}
		else
		{ return_result('0'); }
	}
	catch(err)
	{ console.log("__getProductsCategoryChildStatus",err); return_result('0'); }	
};

//Get Product image
methods.__getProductsFeaturedImage =  function (company_id,product_id,return_result){

	try
	{
		if(parseInt(company_id) !== 0 && parseInt(product_id) !== 0)
		{
			var query  = " SELECT `meta_value` FROM `jaze_products_meta` WHERE `meta_key` = 'featured_image' and `product_id` = '"+product_id+"' ";
				
				DB.query(query, null, function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{ 
						var default_logo = G_STATIC_IMG_URL +'uploads/jazenet/company/'+company_id+'/products/'+product_id+'/thumb/'+data[0].meta_value;

						return_result(default_logo.toString());
					}
					else
					{ return_result(''); }
				});
		}
		else
		{ return_result(''); }
	}
	catch(err)
	{ console.log("__getProductsFeaturedImage",err); return_result(''); }	
};

//Get Product Brand Name 
methods.__getProductsBrandName  =  function (brand_id,return_result){
	
	try
	{
		if(parseInt(brand_id) !== 0)
		{
			var query  = " SELECT `meta_value` FROM `jaze_jazestore_brand_list` WHERE `meta_key` = 'name'  ";
				query  += " and group_id IN  (SELECT group_id FROM jaze_jazestore_brand_list WHERE meta_key = 'status'  AND  meta_value = '1' and  `group_id` = '"+brand_id+"') ";
				
				DB.query(query, null, function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{ 
						return_result(data[0].meta_value);
					}
					else
					{ return_result(''); }
				});
		}
		else
		{ return_result(''); }
	}
	catch(err)
	{ console.log("__getProductsBrandName",err); return_result(''); }	
};

// ------------------------------------------------------  Search Products  Module End  --------------------------------------------------------------//

// ------------------------------------------------------  Search Services  Module Start  --------------------------------------------------------------// 

methods.getAutofillServicesResults = function(arrParams,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 4)
		{	
			methods.__getCountServiceCategory(arrParams,function(category_t_count){
				
				if(category_t_count.cat_id !== "")
				{
					var query  = " SELECT * FROM jaze_category_list WHERE  group_id IN  ("+category_t_count.cat_id.join()+") ";

					DB.query(query, null, function (data, error) {

						if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
						{ 
							HelperRestriction.convertMultiMetaArray(data, function(data_result){

								var counter = data_result.length;
										
								var array_rows = [];

								if(parseInt(counter) !== 0)
								{
									promiseForeach.each(data_result,[function (list) {
								
										return new Promise(function (resolve, reject) {
										
											methods.__getCategoryParentName(list.parent_id, function(parent_cat_list){	

												methods.__getCategoryChildStatus(list.group_id, function(detail_flag){	

													var default_logo = '';

													if(list.image_url !== "" && list.image_url !== null)
													{ default_logo = G_STATIC_IMG_URL + 'uploads/services_cat_images/'+list.image_url; }

													var parent_name = "";

													if(parent_cat_list !== null)
													{  parent_name = parent_cat_list[0].cate_name; }
													
													var newObj = {
															cat_id	  	  : list.group_id.toString(),
															cat_name	  : list.cate_name.toString(),
															cat_logo	  : default_logo.toString(),
															parent_name	  : parent_name.toString(),
															detail_flag   : detail_flag.toString()
													};
																	
													array_rows.push(newObj);

													resolve(array_rows);

													counter -= 1;
													
												});
											});
										});
									}],
									function (result, current) {
																	
										if (counter === 0)
										{ return_result(result[0]);  }
									});
								}
								else
								{ return_result(null); }
							});
						}
						else
						{ return_result(null); }
					});
				}
				else
				{ return_result(null); }	
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("getAutofillServicesResults",err); return_result(null); }	
}

//Get Category Parent Name 
methods.__getCategoryParentName =  function (category_id,return_result){

	try
	{
		var result_array = [];

		if(parseInt(category_id) !== 0)
		{
			var query  = " SELECT * FROM jaze_category_list WHERE  meta_key IN ('cate_name','parent_id')  ";
				query += " and group_id IN  (SELECT group_id FROM jaze_category_list WHERE meta_key = 'status'  AND meta_value = '1') ";
				query += " and group_id = '"+category_id+"' ";
				
				DB.query(query, null, function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{
						HelperRestriction.convertMultiMetaArray(data, function(data_result){

							if (typeof data_result !== 'undefined' && data_result !== null && parseInt(data_result.length) !== 0)
							{
								var obj_return = {
									cate_id 	:  data_result[0].group_id,
									cate_name   :  data_result[0].cate_name,
									parent_id  :  data_result[0].parent_id
								};

								result_array.push(obj_return);
									
								if(parseInt(data_result[0].parent_id) !== 0)
								{  
									methods.__getCategoryParentName(data_result[0].parent_id, function(parent_result){	

										if(parent_result !== null)
										{ result_array = result_array.concat(parent_result); }

										return_result(result_array);
									});
								}
								else
								{ return_result(result_array); }
							}
							else
							{ return_result(null); }
						});
					}
					else
					{ return_result(null); }
				});
		}
		else
		{
			return_result(null);
		}
	}
	catch(err)
	{ console.log("__getCategoryParentName",err); return_result(null); }	
};

//Get Category Child Status 
methods.__getCategoryChildStatus  =  function (category_id,return_result){

	try
	{
		//return_result : 6 = service category, 7 = service listing, 
		
		if(parseInt(category_id) !== 0)
		{
			var query  = " SELECT group_id FROM jaze_category_list WHERE  ";
				query += " group_id IN  (SELECT group_id FROM jaze_category_list WHERE meta_key = 'status'  AND meta_value = '1' ";
				query += " and group_id IN  (SELECT group_id FROM jaze_category_list WHERE meta_key = 'parent_id'  AND meta_value = '"+category_id+"')) ";
				query += " group by  group_id ";
				
				DB.query(query, null, function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{ return_result('6'); }
					else
					{ return_result('7'); }
				});
		}
		else
		{ return_result('0'); }
	}
	catch(err)
	{ console.log("__getCategoryChildStatus",err); return_result('0'); }	
};

// ------------------------------------------------------  Search Services  Module End  --------------------------------------------------------------// 

// ------------------------------------------------------  Search Keyword  Module Start  --------------------------------------------------------------// 

methods.__getCountCompanyProductKeyword = function(flag,arrParams,return_result){

	var return_obj = {
		keyword_count      : "0",
		keywords         : ""					                        
	};

	try{

			// flag  :  0 = total, 1= international,  2 = national,  3 = local, 4 = selected state result, 5 = international company , 6 = national company 

			var keyword = rtrim(ltrim(arrParams.keyword.toLowerCase()));

			var query  = " SELECT * FROM jaze_master_search_keyword_list WHERE  status  = 1  and (type = 7 or type = 8) ";
				query += " and (keyword like '%"+ keyword+"%' or keyword like '%"+ keyword.replace(' ', '')+"%' or  Replace(coalesce(keyword,''), ' ','') LIKE '%"+keyword+"%') ";


				if(parseInt(flag) !== 0)
				{
					if(parseInt(arrParams.country) !== 0 &&  parseInt(arrParams.city) !== 0 && parseInt(flag) === 1)
					{
						query += " and  country_id = '"+arrParams.country+"' ";
						query += " and  city_id != '"+arrParams.city+"' ";
					}
					else if(parseInt(arrParams.country) !== 0 && parseInt(arrParams.city) !== 0 && parseInt(flag) === 2)
					{
						query += " and  country_id = '"+arrParams.country+"' ";
						query += " and  city_id != '"+arrParams.city+"' ";
					}
					else if(parseInt(arrParams.city) !== 0 && parseInt(flag) === 3)
					{
						query += " and  city_id = '"+arrParams.city+"' ";
					}
					else if(parseInt(arrParams.geo_child_id) !== 0 &&  parseInt(flag) === 4)
					{
						query += " and  state_id = '"+arrParams.geo_child_id+"' ";
					}
					else if(parseInt(arrParams.country) !== 0 &&  parseInt(flag) === 5)
					{
						query += " and  country_id != '"+arrParams.country+"' ";
					}
					else if(parseInt(arrParams.state) !== 0 &&  parseInt(flag) === 6)
					{
						query += " and  country_id = '"+arrParams.country+"' ";
						query += " and  state_id != '"+arrParams.state+"' ";
					}
					else 
					{
						query += " and  id  = 0 ";
					}
				}

				DB.query(query,null, function(data, error){
				
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

							var keywords_id_list = [];

							var keywords_list = [];
							
							data.forEach(function(row){

								var list = __convert_array(row.keyword);

								for (var i in list) 
								{ 
									var keyword  = list[i];
									
									if(keyword.includes(arrParams.keyword.toLowerCase()))
									{ 
										var index = __getKeywordGroupIndex(keywords_list,keyword);

										if(index !== ""){
											//double check type
											var key_flag = keywords_list[index].type;
	
											if(parseInt(row.type) !== parseInt(key_flag))
											{ index = ""; }
										}

										if(index != ""){

											var count = parseInt(keywords_list[index].result_count) + 1;

											Object.assign(keywords_list[index], {result_count : count.toString()});

											keywords_id_list.push(row.id);
										}
										else {

											var obj_return = {
												keyword :  keyword,
												type : row.type.toString(),
												result_count :  "1"
												};

												keywords_list.push(obj_return);

												keywords_id_list.push(row.id);
										}
									}
								}
							});

							return_obj = {
								keyword_count    : keywords_list.length,
								keywords_id_list : keywords_id_list,
								keywords         : keywords_list					                        
							};

							return_result(return_obj);
					}
					else
					{ return_result(return_obj); }
				});
	}
	catch(err)
	{
		return_result(return_obj)
	}
};

methods.getKeywordLocalCount = function(arrParams,return_result)
{
	var return_obj = {
			local_count      	 : "0",
			national_count       : "0",
			international_count  : "0"					                        
		};

	try
	{
		//company count
		methods.__getCountCompanyProductKeyword(3,arrParams,function(keyword_loc_count){
			
			methods.__getCountCompanyProductKeyword(2,arrParams,function(keyword_nat_count){

				methods.__getCountCompanyProductKeyword(5,arrParams,function(keyword_inter_count){

					return_obj = {
						local_count             : keyword_loc_count.keyword_count.toString(),
						national_count          : keyword_nat_count.keyword_count.toString(),
						international_count     : keyword_inter_count.keyword_count.toString()				                        
					};
								
					return_result(return_obj);
				});							
			});	
		});
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getAutofillKeywordResults = function(arrParams,return_result)
{
	var return_obj = {
		result : [],
		tot_count : "0",
		tot_page : "0"
	};

	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 5 && (parseInt(arrParams.geo_type) === 3 || parseInt(arrParams.geo_type) === 4 || parseInt(arrParams.geo_type) === 5 || parseInt(arrParams.geo_type) === 6))
		{
			var keyword = rtrim(ltrim(arrParams.keyword.toLowerCase()));

			var query  = " SELECT * FROM jaze_master_search_keyword_list WHERE  status  = 1  and (type = 7 or type = 8)  ";
				query += " and (keyword like '%"+ keyword+"%' or keyword like '%"+ keyword.replace(' ', '')+"%' or  Replace(coalesce(keyword,''), ' ','') LIKE '%"+keyword+"%') ";

				var flag = arrParams.geo_type;

				if(parseInt(flag) !== 0)
				{
					if(parseInt(arrParams.country) !== 0 &&  parseInt(arrParams.city) !== 0 && parseInt(flag) === 1)
					{
						query += " and  country_id = '"+arrParams.country+"' ";
						query += " and  city_id != '"+arrParams.city+"' ";
					}
					else if(parseInt(arrParams.country) !== 0 && parseInt(arrParams.city) !== 0 && parseInt(flag) === 2)
					{
						query += " and  country_id = '"+arrParams.country+"' ";
						query += " and  city_id != '"+arrParams.city+"' ";
					}
					else if(parseInt(arrParams.city) !== 0 && parseInt(flag) === 3)
					{
						query += " and  city_id = '"+arrParams.city+"' ";
					}
					else if(parseInt(arrParams.geo_child_id) !== 0 &&  parseInt(flag) === 4)
					{
						query += " and  state_id = '"+arrParams.geo_child_id+"' ";
					}
					else if(parseInt(arrParams.country) !== 0 &&  parseInt(flag) === 5)
					{
						query += " and  country_id != '"+arrParams.country+"' ";
					}
					else if(parseInt(arrParams.state) !== 0 &&  parseInt(flag) === 6)
					{
						query += " and  country_id = '"+arrParams.country+"' ";
						query += " and  state_id != '"+arrParams.state+"' ";
					}
					else 
					{
						query += " and  id  = 0 ";
					}
				}

				DB.query(query, null, function (data, error) {
										
					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{
						var keywords_list = [];
						
						data.forEach(function(row){

							var list = __convert_array(row.keyword);

							for (var i in list) 
							{ 
								var keyword  = list[i];

								if(keyword.includes(arrParams.keyword.toLowerCase()))
								{ 
									var index = __getKeywordGroupIndex(keywords_list,keyword);

									if(index !== ""){
										//double check type
										var key_flag = keywords_list[index].flag;

										if(parseInt(row.type) === 7 && (parseInt(key_flag) === 13 ||parseInt(key_flag) === 14))
										{ }
										else if(parseInt(row.type) === 8 && (parseInt(key_flag) === 11 ||parseInt(key_flag) === 12))
										{ }
										else
										{ index = ""; }
									}

									if(index !== ""){

										var count = parseInt(keywords_list[index].result_count) + 1;

										var  flag = (parseInt(row.type) === 8) ? "11": "13";

										Object.assign(keywords_list[index], {result_count : count.toString(),link_id : "0",flag : flag});
									}
									else 
									{
										var flag = "0" , logo = "";

										if(parseInt(row.type) === 8)
										{
											flag = "12";
											logo = G_STATIC_IMG_URL + 'uploads/jazenet/others/mob_search_company_icon.png';
										}
										else if(parseInt(row.type) === 7)
										{
											flag = "14";
											logo = G_STATIC_IMG_URL + 'uploads/jazenet/others/mob_search_keyword_icon.png';
										}

										var obj_return = {
												keyword 	 :  keyword,
												result_count :  "1",
												link_id 	 :  row.id.toString(),
												flag 		 :  flag,
												logo		 : logo
										};

										keywords_list.push(obj_return);
									}
								}
							}
						});

						keywords_list = keywords_list.splice(0, 50);

						return_obj.tot_count = keywords_list.length;

						return_obj.result = sort_keyword(keywords_list,arrParams.keyword)

						return_result(return_obj);
					}
					else
					{ return_result(return_obj); }
			});
		}
		else
		{ return_result(return_obj); }
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.getAutofillKeywordInternationalNationalResults = function(arrParams,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(arrParams.category) === 5 && (parseInt(arrParams.geo_type) === 1 || parseInt(arrParams.geo_type) === 2))
		{ 
			if(parseInt(arrParams.geo_type) === 1)
			{
				//International Result
				methods.__getCountCompanyProductKeyword(5,arrParams,function(keywordList){

						if(keywordList.keywords != "")
						{
							methods.__getKeywordCountryList(keywordList.keywords_id_list,arrParams,function(keyword_inter_count){

								if(keyword_inter_count !== null)
								{ 
									return_result(keyword_inter_count,[]);
								}
								else
								{ return_result([],[]); }
							});
						}
						else
						{ return_result([],[]); }
				});
			}
			else if(parseInt(arrParams.geo_type) === 2)
			{ 
				//National Result
				methods.__getCountCompanyProductKeyword(2,arrParams,function(keywordList){
					
					if(keywordList.keywords != "")
					{
						methods.__getKeywordStateList(keywordList.keywords_id_list,arrParams.country,0,1,function(keyword_nation_count){
						
							if(keyword_nation_count.result !== null)
							{ 
								return_result([],keyword_nation_count.result);
							}
							else
							{ return_result([],[]); }
						});
					}
					else
					{ return_result([],[]); }
				});
			}
			else
			{ return_result([],[]); }
		}
		else
		{ return_result([],[]); }
	}
	catch(err)
	{
		console.log(err);
		return_result([],[]);
	}	
}

//Get Keyword Country List
methods.__getKeywordCountryList = function (keywordList,arrParams,return_result){
	
	try
	{
		var query  = " SELECT country_id,(SELECT `country_name` FROM `country` WHERE `status` = '1' and `id` = country_id) as country_name FROM jaze_master_search_keyword_list WHERE  status  = 1  and (type = 7 or type = 8) AND id in ("+keywordList+") ";

			DB.query(query, null, function(data, error){
				
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
					var array_rows = [];

					var counter = data.length;

					var total_count = '0';
				
					if(parseInt(counter) !== 0)
					{
						promiseForeach.each(data,[function (list) {
												
							return new Promise(function (resolve, reject) {
								
								methods.__getKeywordStateList(keywordList,list.country_id,0,0,function(key_state_list){

										if(list.country_name !== "" && list.country_name !== null)
										{
											var index = __getCountryGroupIndex(array_rows,list.country_id,'country_id');

											if(index != ""){

												var count = parseInt(array_rows[index].country_count) + parseInt(key_state_list.count);
						
											//	Object.assign(array_rows[index], {country_count :  count.toString()});
											}
											else {
						
												var obj_return = {
														country_id    : list.country_id.toString(),
														country_name  : list.country_name,
														country_count : key_state_list.count.toString(),
														state_list	  : key_state_list.result
													};
																
												array_rows.push(obj_return);

												total_count = parseInt(total_count) + parseInt(key_state_list.count);
											}
										}

									counter -= 1;
									
									if(parseInt(counter) === 0)
									{ resolve(array_rows); }
								});	
							});
						}],
						function (result, current) {
												
							if (counter === 0)
							{   	
								var resul = result[0];

								fast_sort(resul).asc('country_name'); 

								var obj_return = {
										country_id    :  "0",
										country_name  :  "world",
										country_count :  total_count.toString(),
										state_list	  : []
									};
				
								resul.unshift(obj_return);

								return_result(resul); 
							}
						});	
					}
					else
					{return_result(null)}
				}
				else
				{return_result(null)}
			});
	}
	catch(err)
	{ console.log("__getKeywordCountryList",err); return_result(null); }	
};

methods.__getKeywordStateList = function (keyword_list,country_id,city_id,all_status,return_result){
	
	var obj_return = {
		result : [],
		count : '0'
	};

	try
	{
		if(keyword_list !== null && parseInt(keyword_list.length) > 0)
		{
			var counter = keyword_list.length;

			var list = [];

			var total_count = '0';

			if (parseInt(counter) !== 0) {
										
				promiseForeach.each(keyword_list, [function (keyword_id_array) {

					return new Promise(function (resolve, reject) {

						var query  = " SELECT `state_id`, (SELECT `state_name` FROM `states` WHERE `status` = '1' and `id` = state_id) as state_name FROM `jaze_master_search_keyword_list` WHERE (`type` = '7' or `type` = '8') and id in ("+keyword_id_array+")";
		
						if(parseInt(country_id) !== 0)
						{
							query += " and  country_id = '"+country_id+"' ";
						}

						if(parseInt(city_id) !== 0)
						{
							query += " and  city_id = '"+city_id+"' ";
						}
					
						DB.query(query, null, function(arrvalues, error){
							
							if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0){

								if(parseInt(arrvalues[0].state_id) !== 0)
								{
									for (var i in arrvalues) 
									{
										total_count ++;

										var index = __getCountryGroupIndex(list,arrvalues[i].state_id,'state_id');

										if(index != ""){

											var count = parseInt(list[index].state_count) + 1;

											Object.assign(list[index], {state_count :  count.toString()});
										}
										else {

											var obj_return = {
												state_id :  arrvalues[i].state_id.toString(),
												state_name :  arrvalues[i].state_name,
												state_count :  "1"
												};

											list.push(obj_return);
										}								
									}

									counter -= 1;

									if (counter === 0) 
									{ resolve(list); }
								}
								else
								{ 
									counter -= 1;

									if (counter === 0) 
									{ resolve(list); }
								}
							}
							else
							{ 
								counter -= 1;

								if (counter === 0) 
								{ resolve(list); }
							}
						});	
					})
				}],
				function (result, current) {
					if (counter === 0) 
					{
						fast_sort(result[0]).asc('state_name');

						if(parseInt(all_status) === 1)
						{
							var obj_return = {
									state_id    :  "0",
									state_name  :  "all",
									state_count :  keyword_list.length.toString()
								};

								result[0].unshift(obj_return);
						}

						obj_return = {
							result : result[0],
							count : total_count.toString()
						};

						return_result(obj_return); 
					}
				});	
			} 
			else
			{ return_result(obj_return); }
		} 
		else
		{ return_result(obj_return); }
	}
	catch(err)
	{ console.log("__getKeywordStateList",err); return_result(obj_return); }		
};

function __getKeywordGroupIndex(list_array,index_value)
{
	var retern_index = "";

		if(list_array !== null)
		{
			Object.keys(list_array).forEach(function(prop) {

				var value = list_array[prop].keyword;
				
				if(value === index_value)
				{
					retern_index = prop;
				}
			  });
		}
		
		return retern_index;
}

// ------------------------------------------------------  Search Keyword  Module End  --------------------------------------------------------------// 




// ------------------------------------------------------ Main Count Module Start  ------------------------------------------------------ 

methods.getCountResult = function(arrParams,return_result)
{
	var return_obj = {
			company_count      : "0",
			prof_count         : "0",
			prod_count         : "0",
			category_count     : "0",
			keywords_count     : "0"					                        
		};

	try
	{
		//company count
		methods.__getCountCompany(0,arrParams,0,function(comp_t_count){
			
			methods.__getCountProfile(0,arrParams,function(prof_t_count){
				
				methods.__getCountProducts(0,arrParams,function(prod_t_count){

					methods.__getCountProductsCategory(arrParams,function(p_category_t_count){

						methods.__getCountServiceCategory(arrParams,function(category_t_count){

							methods.__getCountCompanyProductKeyword(0,arrParams,function(compKeywords){

								var prod_count = parseInt(p_category_t_count) + parseInt(prod_t_count.prod_count);

									return_obj = {
										company_count       : comp_t_count.company_count.toString(),
										prof_count          : prof_t_count.profile_count.toString(),
										prod_count          : prod_count.toString(),
										service_count   	: category_t_count.cat_count.toString(),
										keywords_count      : compKeywords.keyword_count.toString()				                        
									};
									
									return_result(return_obj);
							});
						});
					});
				});		
			});	
		});
	}
	catch(err)
	{
		console.log(err);
		return_result(return_obj);
	}
}

methods.__getCountCompany = function(flag,arrParams,keyword_flag,return_result){

	var return_obj = {
		company_count      : "0",
		company_id         : ""					                        
	};

	try{
		
		// flag  :  0 = total, 1= international,  2 = national,  3 = local, 4 = selected state result, 5 = international company , 6 = national company 

				var query  = " SELECT group_concat(id) as account_id FROM jaze_master_search_keyword_list WHERE (type = 3 or type = 5) and  status  = 1   ";
					
					if(parseInt(keyword_flag) === 0)
					{
						var keyword = rtrim(ltrim(arrParams.keyword.toLowerCase()));

						query += " and (keyword like '%"+ keyword+"%' or keyword like '%"+ keyword.replace(' ', '')+"%' or  Replace(coalesce(keyword,''), ' ','') LIKE '%"+keyword+"%') ";
					}
					
					if(parseInt(flag) !== 0)
					{
						if(parseInt(arrParams.country) !== 0 && parseInt(arrParams.city) !== 0 &&  parseInt(flag) === 1)
						{
							query += " and country_id = '"+arrParams.country+"' ";
							query += " and state_id != '"+arrParams.state+"' ";
							query += " and city_id != '"+arrParams.city+"' ";
							query += " and city_id != '0' ";
						}
						else if(parseInt(arrParams.country) !== 0 && parseInt(arrParams.city) !== 0 && parseInt(flag) === 2)
						{
							query += " and country_id = '"+arrParams.country+"' ";
							query += " and city_id != '"+arrParams.city+"' ";
							query += " and city_id != '0' ";
						}
						else if(parseInt(arrParams.city) !== 0 && parseInt(flag) === 3)
						{
							query += " and city_id = '"+arrParams.city+"' ";
						}
						else if(parseInt(arrParams.geo_child_id) !== 0 &&  parseInt(flag) === 4)
						{
							query += " and state_id = '"+arrParams.geo_child_id+"' ";
							query += " and city_id != '0' ";
						}
						else if(parseInt(arrParams.country) !== 0 &&  parseInt(flag) === 5)
						{
							query += " and country_id != '"+arrParams.country+"' ";
							query += " and city_id != '0' ";
						}
						else if(parseInt(arrParams.state) !== 0 &&  parseInt(flag) === 6)
						{
							query += " and country_id = '"+arrParams.country+"' ";
							query += " and state_id != '"+arrParams.state+"' ";
							query += " and city_id != '0' ";
						}
						else 
						{
							query += " and  id  = 0 ";
						}
					}
				
				DB.query(query,null, function(data, error){
					
					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
						if(data[0].account_id !== '' && data[0].account_id !== null){

							return_obj = {
								company_count      : (__convert_array(data[0].account_id)).length,
								company_id         : data[0].account_id					                        
							};

							return_result(return_obj);
						}
						else
						{return_result(return_obj)}
					}
					else
					{return_result(return_obj)}
				});
	}
	catch(err)
	{
		return_result(return_obj)
	}
};

methods.__getCountProfile = function(flag,arrParams,return_result){

	var return_obj = {
		profile_count      : "0",
		profile_id         : ""					                        
	};

	try{
		
		// flag  :  0 = total, 1= international,  2 = national,  3 = local, 4 = selected state result, 5 = international company , 6 = national company 

		var keyword = rtrim(ltrim(arrParams.keyword.toLowerCase()));

		var query  = " SELECT group_concat(id) as account_id FROM jaze_master_search_keyword_list WHERE (type = 2 or type = 4) and  status  = 1  ";
			query += " and (keyword like '%"+ keyword+"%' or keyword like '%"+ keyword.replace(' ', '')+"%' or  Replace(coalesce(keyword,''), ' ','') LIKE '%"+keyword+"%') ";

			if(parseInt(flag) !== 0)
			{	
				if(parseInt(arrParams.country) !== 0 &&  parseInt(arrParams.city) !== 0 && parseInt(flag) === 1)
				{
					query += " and  country_id = '"+arrParams.country+"' ";
					query += " and  city_id != '"+arrParams.city+"' ";
				}
				else if(parseInt(arrParams.country) !== 0 && parseInt(arrParams.city) !== 0 && parseInt(flag) === 2)
				{
					query += " and  country_id = '"+arrParams.country+"' ";
					query += " and  city_id != '"+arrParams.city+"' ";
				}
				else if(parseInt(arrParams.city) !== 0 && parseInt(flag) === 3)
				{
					query += " and  city_id = '"+arrParams.city+"' ";
				}
				else if(parseInt(arrParams.geo_child_id) !== 0 &&  parseInt(flag) === 4)
				{
					query += " and  state_id = '"+arrParams.geo_child_id+"' ";
				}
				else if(parseInt(arrParams.country) !== 0 &&  parseInt(flag) === 5)
				{
					query += " and  country_id != '"+arrParams.country+"' ";
				}
				else if(parseInt(arrParams.state) !== 0 &&  parseInt(flag) === 6)
				{
					query += " and  country_id = '"+arrParams.country+"' ";
					query += " and  state_id != '"+arrParams.state+"' ";
				}
				else 
				{
					query += " and  id  = 0 ";
				}
			}

			DB.query(query,null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
						
				if(data[0].account_id !== '' && data[0].account_id !== null){

					return_obj = {
						profile_count      : (__convert_array(data[0].account_id)).length,
						profile_id         : data[0].account_id					                        
					};

					return_result(return_obj);
				}
				else
				{ return_result(return_obj); }
			}
			else
			{  return_result(return_obj); }
		});
	}
	catch(err)
	{ return_result(return_obj); }
};




methods.__getCountServiceCategory  = function(arrParams,return_result){

	var return_obj = {
		cat_count      : "0",
		cat_id         : ""					                        
	};

	try{

		var parent_cat_array = [2,4,6,8,9,10,11,1834,1833];

		var query  = " SELECT group_id,meta_value FROM jaze_category_list WHERE  meta_key IN ('parent_id')  ";
			query += " and group_id IN  (SELECT group_id FROM jaze_category_list WHERE meta_key = 'status'  AND meta_value = '1') ";
			query += " and  group_id NOT IN  (SELECT group_id FROM jaze_category_list WHERE meta_key = 'parent_id'  AND meta_value IN (2,4,6,8,9,10,11,1834,1833)) ";
			query += " and  group_id IN  (SELECT group_id FROM jaze_category_list WHERE meta_key IN ('cate_name')  AND LOWER(meta_value) like '%"+arrParams.keyword.toLowerCase()+"%') ";
			query += " group by  group_id ";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					var counter = data.length;
										
					var array_rows = [];

					var cat_id_count = 0;

					if(parseInt(counter) !== 0)
					{
						promiseForeach.each(data,[function (list) {
					
							return new Promise(function (resolve, reject) {

								methods.__getCategoryParentName(list.meta_value, function(parent_cat_list){

									if(parent_cat_list !== null)
									{ 
										parent_cat_list = parent_cat_list.reverse(); 
					
										if(parent_cat_array.indexOf(parseInt(parent_cat_list[0].cate_id)) === -1)
										{  
											array_rows.push(list.group_id);  
											cat_id_count += 1; 
										}
									}
									resolve(array_rows);

									counter -= 1;

								});
							});
						}],
						function (result, current) {
														
							if (counter === 0)
							{  
								return_obj = {
									cat_count      : cat_id_count.toString(),
									cat_id         : result[0]					                        
								};

								return_result(return_obj);  
							}
						});	
					}
					else
					{ return_result(return_obj); }	

				}
				else
				{ return_result(return_obj); }		
			});
	}
	catch(err)
	{
		return_result(return_obj)
	}
};



// ------------------------------------------------------ Main Count Module End  ------------------------------------------------------ 

//------------------------------------------------------------------- General Module Start ---------------------------------------------------------------------------//


function __convert_array(array_val) {

	var list = [];

	if(typeof array_val !== 'undefined' && array_val !== null && array_val !== "")
	{
		if(array_val.toString().indexOf(',') != -1 ){
			list = array_val.split(',');
			list = list.map(v => v.toLowerCase());
		}
		else {
			list.push(array_val.toLowerCase());
		}
	}	
	return list;
}

methods.convertAccountMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getAccountGroupIndex(list,arrvalues[i].account_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							account_type : arrvalues[i].account_type,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};

function __getAccountGroupIndex(list_array,index_value)
{
	var retern_index = "";

		if(list_array !== null)
		{
			Object.keys(list_array).forEach(function(prop) {

				var value = list_array[prop].account_id;
 
				if(parseInt(value) === parseInt(index_value))
				{
					retern_index = prop;
				}
			  });
		}
		
		return retern_index;
}

function sort_keyword(array,keyword)
{
	return array.filter(o => o.keyword.toLowerCase().includes(keyword.toLowerCase()))
			.sort((a, b) => a.keyword.toLowerCase().indexOf(keyword.toLowerCase()) - b.keyword.toLowerCase().indexOf(keyword.toLowerCase()));
} 

function sort_name(array,keyword)
{
	return array.filter(o => o.name.toLowerCase().includes(keyword.toLowerCase()))
			.sort((a, b) => a.name.toLowerCase().indexOf(keyword.toLowerCase()) - b.name.toLowerCase().indexOf(keyword.toLowerCase()));
} 

//------------------------------------------------------------------- General Module End ---------------------------------------------------------------------------//



module.exports = methods;
