const methods = {};
const DB = require('../../helpers/db_base.js');
const promiseForeach = require('promise-foreach');
const dateFormat = require('dateformat');
const now = new Date();
const standardDT = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
const newdateForm = dateFormat(new Date(standardDT), 'yyyy-mm-dd HH:MM:ss');
const moment = require("moment");
const HelperRestriction = require('../helpers/HelperRestriction.js');

methods.getCompanyProfile =  function(arrParams,return_result){
	try
	{
			
		var arrAccounts = [];

		var queryAccountType = `SELECT account_id,account_type FROM jaze_user_basic_details WHERE account_id =?`;

		DB.query(queryAccountType,[arrParams.account_id], function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var queryAccDetails = `SELECT * FROM jaze_user_basic_details WHERE account_id =? and account_type=?`;

				DB.query(queryAccDetails,[data[0].account_id,data[0].account_type], function(allData, error){
	
					if(typeof allData !== 'undefined' && allData !== null && parseInt(allData.length) > 0)
					{

						methods.convertMultiMetaArrayCompany(allData, function(retConverted){	

							var counter = retConverted.length;		
							var jsonItems = Object.values(retConverted);

							promiseForeach.each(jsonItems,[function (val) {
								return new Promise(function (resolve, reject) {		
						
							  		var default_logo = '';

									var newObj = {};

							  		if(parseInt(data[0].account_type) === 1)
							  		{
							  			if(val.profile_logo !== "" && val.profile_logo !== null){
											default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + val.profile_logo + '';
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : (val.fname +" "+val.lname).toString(),
											logo		  : default_logo
										}	

							  		}
							  		else if(parseInt(data[0].account_type) === 2)
							  		{

				  						if(val.profile_logo !== "" && val.profile_logo !== null){
											default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + val.profile_logo + '';
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : (val.fname +" "+val.lname).toString(),
											logo		  : default_logo
										}	

							  		}
						  			else if(parseInt(data[0].account_type) === 3)
					  				{

				  						if(val.company_logo !== "" && val.company_logo !== null){
											default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/logo/' + val.company_logo + '';
										}else{
											default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/logo/' + val.listing_images + '';
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : val.company_name,
											logo		  : default_logo
										}	

					  				}

									counter -= 1;
									if (counter === 0){ 
										resolve(newObj);
									}
									
								})
							}],
							function (result, current) {
								if (counter === 0){ 
									return_result(result[0]);
								}
							});

						});


					}
					else
					{
						return_result(null);
					}
				});

		
			}
			else
			{
				return_result(null);
			}
		});

	}
	catch(err)
	{ console.log("getCompanyProfile",err); return_result(null);}
};

methods.getEventsList =  function(arrParams,return_result){
	try
	{
	
		var arrCategory = [];

		var dateNow = dateFormat(new Date(standardDT), 'yyyy-mm-dd');
		if(arrParams.date !=''){
			dateNow = arrParams.date;
		}

		var query = `SELECT * FROM jaze_events_category`;

		if(parseInt(arrParams.category_id) !==0 ){
			query += " WHERE group_id='"+arrParams.category_id+"' ";
		}

		DB.query(query,null, function(pdata, error){

			if(typeof pdata !== 'undefined' && pdata !== null && parseInt(pdata.length) > 0){

				methods.convertMultiMetaArray(pdata, function(retPdata){	

					var counter = retPdata.length;		
					var jsonItemsMain = Object.values(retPdata);

					promiseForeach.each(jsonItemsMain,[function (jsonItemsMain) {
						return new Promise(function (resolve, reject) {

							arrCategory.push(jsonItemsMain.group_id);
							counter -= 1;

							if (counter === 0){ 
								resolve(arrCategory);
							}
						})
					}],
					function (result, current) {

						if (counter === 0){ 
							
							var arrObjects = {};
							var arrResults = [];

							var dateTo = new Date();
							var dateFrom = new Date();
							dateTo.setDate(dateTo.getDate() + 30);
							dateFrom.setDate(dateFrom.getDate() - 30);

							var dateTo = dateTo.toISOString().split('T')[0];
							var dateNow = dateFrom.toISOString().split('T')[0];

							var cquery = "SELECT * FROM jaze_events_details WHERE group_id IN ( SELECT group_id FROM jaze_events_details WHERE meta_key ='event_cat_id' AND meta_value IN(?) )";

							if(arrParams.date !=''){
								dateNow = arrParams.date;

								cquery += "AND group_id IN ( SELECT group_id FROM jaze_events_details WHERE meta_key LIKE 'end_date' AND meta_value >= '"+dateNow+"' AND group_id IN (SELECT group_id FROM jaze_events_details WHERE meta_key LIKE 'event_date' AND meta_value <= '"+dateNow+"')) ";
								// cquery += " AND group_id IN ( SELECT group_id FROM jaze_events_details WHERE (meta_key ='event_date' OR meta_key ='end_time') AND DATE(meta_value) = '"+dateNow+"' )";

							}else{
								cquery += " AND group_id IN ( SELECT group_id FROM jaze_events_details WHERE meta_key ='event_date' AND DATE(meta_value) BETWEEN '"+dateNow+"' AND '"+dateTo+"' )";
							}

							DB.query(cquery,[result[0]], function(cdata, error){
							
								if(typeof cdata !== 'undefined' && cdata !== null && parseInt(cdata.length) > 0)
								{

									methods.convertMultiMetaArray(cdata, function(retConverted){

										var counter = retConverted.length;		
										var jsonItemsMain = Object.values(retConverted);

										promiseForeach.each(jsonItemsMain,[function (jsonItemsMain) {
											return new Promise(function (resolve, reject) {

												methods.getCategoryName(jsonItemsMain.event_cat_id, function(retCategory){

													methods.getDiaryStatus(arrParams.account_id, function(retDiaryStatus){

														var media = "";
														if(jsonItemsMain.featured_media){

															if(jsonItemsMain.featured_media.includes("."))
															{
																media = G_STATIC_IMG_URL+"uploads/event_images/"+jsonItemsMain.featured_media; 
															}
															else
															{
																media = 'https://img.youtube.com/vi/'+jsonItemsMain.featured_media+'/hqdefault.jpg';
															}
														}

														arrObjects = {
															category_id   : jsonItemsMain.event_cat_id.toString(),
															category_name : retCategory,
															event_id      : jsonItemsMain.group_id.toString(),
															event_title   : jsonItemsMain.title,
															event_image   : media,
															event_date    : jsonItemsMain.event_date,
															date_from     : jsonItemsMain.start_time,
															date_to       : jsonItemsMain.end_time,
															diary_status  : retDiaryStatus.toString()
														};

														arrResults.push(arrObjects);
														counter -= 1;

														if (counter === 0){ 
															resolve(arrResults);
														}
													});

												});
									
											})
										}],
										function (result, current) {
											
											if (counter === 0){ 

												result[0].sort(function(a, b) {
													a = new Date(a.date_to);
													b = new Date(b.date_to);
													return a>b ? -1 : a<b ? 1 : 0;
												});

												if(parseInt(arrParams.flag) === 0)
												{
													methods.getEventCalendar(arrParams, function(retEventCalendar){

														return_result(result[0],retEventCalendar);
													});
												}
												else
												{
													return_result(result[0],[]);
												}
											}
										});
									});
								}
								else
								{
									if(parseInt(arrParams.flag) === 0)
									{
										methods.getEventCalendar(arrParams, function(retEventCalendar){

											return_result([],retEventCalendar);
										});
									}
									else
									{
										return_result([],[]);
									}
								}
							});
						}
					});
				});

			}
			else
			{
				return_result(null,[],[]);
			}

		});

	}
	catch(err)
	{ console.log("getEventsList",err); return_result(null);}
};

methods.getEventCalendar =  function(arrParams,return_result){
	try
	{
	
		var arrCategory = [];

		var query = `SELECT * FROM jaze_events_category`;

		if(parseInt(arrParams.category_id) !==0 ){
			query += " WHERE group_id='"+arrParams.category_id+"' ";
		}

		DB.query(query,null, function(pdata, error){

			if(typeof pdata !== 'undefined' && pdata !== null && parseInt(pdata.length) > 0){

				methods.convertMultiMetaArray(pdata, function(retPdata){	

					var counter = retPdata.length;		
					var jsonItemsMain = Object.values(retPdata);

					promiseForeach.each(jsonItemsMain,[function (jsonItemsMain) {
						return new Promise(function (resolve, reject) {

							arrCategory.push(jsonItemsMain.group_id);
							counter -= 1;

							if (counter === 0){ 
								resolve(arrCategory);
							}
								
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
							var arrObjects = {};
							var arrResults = [];

							var date = new Date();
							var yearNow = date.getFullYear();

							var cquery = `SELECT * FROM jaze_events_details WHERE group_id IN ( SELECT group_id FROM jaze_events_details WHERE meta_key ='event_cat_id' AND meta_value IN(?) )`;
								cquery += " AND  group_id IN ( SELECT group_id FROM jaze_events_details WHERE meta_key ='event_date' AND  YEAR(meta_value) ='"+yearNow+"' )"
							
							DB.query(cquery,[result[0]], function(cdata, error){

								if(typeof cdata !== 'undefined' && cdata !== null && parseInt(cdata.length) > 0)
								{

									methods.convertMultiMetaArray(cdata, function(retConverted){

										var counter = retConverted.length;		
										var jsonItemsMain = Object.values(retConverted);

										promiseForeach.each(jsonItemsMain,[function (jsonItemsMain) {
											return new Promise(function (resolve, reject) {
												
												arrResults.push(jsonItemsMain.event_date);
												arrResults.push(dateFormat(new Date(jsonItemsMain.end_time), 'yyyy-mm-dd'));

												counter -= 1;

												if (counter === 0){ 
													resolve(arrResults);
												}
									
											})
										}],
										function (result, current) {
											if (counter === 0){ 

												var	filteredArray = removeDuplicates(result[0]);
												filteredArray.sort();
												
												var min = filteredArray.reduce(function (a, b) { return a < b ? a : b; }); 
												var max = filteredArray.reduce(function (a, b) { return a > b ? a : b; });

												methods.getAllDates(min,max,function(retAllDates){

													return_result(retAllDates);

												});
											}
											
										});
						
									});

								}
								else
								{
									return_result([]);
								}

							});
						}

					});

				});

			}
			else
			{
				return_result([]);
			}

		});

	}
	catch(err)
	{ console.log("getEventCalendar",err); return_result([]);}
};


methods.getEventDetails =  function(arrParams,return_result){
	try
	{
		var arrObjects = {};
		var arrResults = [];
		var query = `SELECT * FROM jaze_events_details WHERE group_id=?`;

		DB.query(query,[arrParams.event_id], function(data, error){
	
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){	

					methods.getCategoryName(retConverted[0].event_cat_id, function(retCategory){

						var arrDiaryParam = {account_id:arrParams.account_id,event_id:retConverted[0].group_id};

						methods.getDiaryStatus(arrDiaryParam, function(retDiaryStatus){

							methods.__getCountryDetails(retConverted[0].country_id, function(country){	

								methods.__getStateDetails(retConverted[0].state_id, function(state){	

									methods.__getCityDetails(retConverted[0].city_id, function(city){	

										methods.__getAreaDetails(retConverted[0].area_id, function(area){	

											var accParam = {account_id:retConverted[0].account_id};
											methods.getCompanyProfile(accParam, function(accRet){	

												methods.__getgeoDetails(arrParams.city_id, function(retGeoDetails){

													methods.getJazeComStatus(retConverted[0].account_id, function(retjazecom){

														var media = "";
														if(retConverted[0].featured_media){
															if(retConverted[0].featured_media.includes("."))
															{
																media = G_STATIC_IMG_URL+"uploads/event_images/"+retConverted[0].featured_media; 
															}
															else
															{
																media = 'https://img.youtube.com/vi/'+retConverted[0].featured_media+'/hqdefault.jpg';
															}
														}

														var geo_distance = 0;
														var latitude = 0;
														var longitude = 0;


														if(parseInt(arrParams.latitude) !==0 && parseInt(arrParams.longitude) !==0){

															if(retConverted[0].latitude){
																latitude = retConverted[0].latitude;
															}

															if(retConverted[0].longitude){
																longitude = retConverted[0].longitude;
															}

														  	geo_distance = distance(parseFloat(arrParams.latitude), parseFloat(arrParams.longitude), parseFloat(latitude), parseFloat(longitude),"K");
														}

														arrObjects = {

															event_basic_details : {
																event_id       : retConverted[0].group_id.toString(),
																category_id    : retConverted[0].event_cat_id.toString(),
																category_name  : retCategory,
																event_title    : retConverted[0].title,
																event_date  : retConverted[0].event_date,
																date_from   : retConverted[0].start_time,
																date_to     : retConverted[0].end_time,
																description : retConverted[0].content,
																event_link  : retConverted[0].event_link,
																featured_media : media
															},
															event_address_details : {
																street_no     : retConverted[0].street_no,
																street_name   : retConverted[0].street_name,
																building_no   : retConverted[0].building_no,
																building_name : retConverted[0].building_name,
																venue         : retConverted[0].venue,
																country_name  : country,
																state_name    : state,
																city_name     : city,
																area_name     : area,
																latitude      : retConverted[0].latitude,
																longitude     : retConverted[0].longitude,
																distance      : Math.ceil(geo_distance).toString()
													
															},
															event_contact_details : {
																contact_account_id : retConverted[0].account_id.toString(),
																contact_name       : retConverted[0].contact_name,
																contact_logo       : accRet.logo,
																contact_address    : retConverted[0].contact_address,
																contact_no         : retConverted[0].contact_no,
																contact_email      : retConverted[0].contact_email,
																contact_website    : retConverted[0].contact_website
															},
															jazecom_status : retjazecom,
															diary_status   : retDiaryStatus.toString()
														};

														return_result(arrObjects);

													});
												});
											});
										});
									});
								});
							});
						});
					});

				});
		
			}else{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getEventDetails",err); return_result(null);}
};


methods.getJazeComStatus =  function(account_id,return_result){

	var jazecom = {
		chat: '0',
		mail: '0',
	};


	try
	{
		var query = `SELECT * FROM jaze_user_jazecome_status WHERE meta_key in('jaze_chat','jaze_mail')
			 and group_id IN (SELECT group_id FROM jaze_user_jazecome_status WHERE meta_key = 'account_id' AND meta_value =?)`;

		DB.query(query,[account_id], function(data, error){
			
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				HelperRestriction.convertMultiMetaArray(data, function(retConverted){		
					
					jazecom = {
						chat  : retConverted[0].jaze_chat,
						mail  : retConverted[0].jaze_mail
					};
					
					return_result(jazecom);
				});				
			}
			else
			{
				return_result(jazecom);
			}

		});
	   
	}
	catch(err)
	{ console.log("getJazeComStatus",err); return_result(jazecom);}
};


methods.getAllDates =  function(from_date,to_date,return_result){
	try
	{

	    var dateArray = [];
	    var currentDate = moment(from_date);
	    var stopDate = moment(to_date);
	    while (currentDate <= stopDate) {
	        dateArray.push( moment(currentDate).format('YYYY-MM-DD') )
	        currentDate = moment(currentDate).add(1, 'days');
	    }

	    if(dateArray.length > 0){
    	 	return_result(dateArray);
	    }else{
    		return_result([]);
	    }
	   
	}
	catch(err)
	{ console.log("getAllDates",err); return_result(null);}
};


methods.getDiaryStatus =  function(arrParams,return_result){
	try
	{

		var query = `SELECT * FROM jaze_dairy_appointment_info WHERE group_id IN (SELECT group_id FROM jaze_dairy_appointment_info WHERE meta_key = 'appointment_type' AND meta_value = '4' )
			AND group_id IN (SELECT group_id FROM jaze_dairy_appointment_info WHERE meta_key = 'event_id' AND meta_value =? )
			AND group_id IN (SELECT group_id FROM jaze_dairy_appointment_info WHERE meta_key = 'account_id' AND meta_value =? )`;
		DB.query(query,[arrParams.event_id,arrParams.account_id], function(data, error){
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				return_result(1);	
			}
			else
			{
				return_result(0);
			}

		});

	}
	catch(err)
	{ console.log("getDiaryStatus",err); return_result(0);}
};


methods.__getgeoDetails =  function(city_id,return_result){
	try
	{
		var query = `SELECT co.country_name FROM country AS co
			LEFT JOIN states AS s ON s.country_id = co.id
			LEFT JOIN cities AS c ON c.state_id = s.id
			WHERE c.id =?`;

		DB.query(query,[city_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				return_result(data[0].country_name);	
			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("__getgeoDetails",err); return_result(null);}
};


methods.getCategoryName =  function(group_id,return_result){
	try
	{

		var query = `SELECT * FROM jaze_events_category WHERE group_id=?`;
		DB.query(query,[group_id], function(data, error){
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				methods.convertMultiMetaArray(data, function(retConverted){	

					return_result(retConverted[0].category_name);
				});
			
			}
			else
			{
				return_result('');
			}

		});

	}
	catch(err)
	{ console.log("getCategoryName",err); return_result('');}
};

methods.getSorting =  function(arrParams,return_result){
	try
	{

		var query = `SELECT categories FROM jaze_events_category_priority WHERE account_id=?`;
		DB.query(query,[arrParams.account_id], function(pdata, error){
			if(typeof pdata !== 'undefined' && pdata !== null && parseInt(pdata.length) > 0)
			{
				return_result(pdata[0].categories.split(","));	
			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getSorting",err); return_result(null);}
};


methods.addToDiaryEvents = function(arrParams,return_result){
	try
	{

		if(parseInt(arrParams.flag) == 1)
		{

			var query = `SELECT * FROM jaze_dairy_appointment_info WHERE group_id IN (SELECT group_id FROM jaze_dairy_appointment_info WHERE meta_key = 'appointment_type' AND meta_value = '4' )
				AND group_id IN (SELECT group_id FROM jaze_dairy_appointment_info WHERE meta_key = 'event_id' AND meta_value =? )
				AND group_id IN (SELECT group_id FROM jaze_dairy_appointment_info WHERE meta_key = 'account_id' AND meta_value =? )`;

			DB.query(query,[arrParams.event_id,arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					return_result(2);	
				}
				else
				{

					var equery = `SELECT * FROM jaze_events_details WHERE group_id=?`;
					DB.query(equery,[arrParams.event_id], function(edata, error){

						if(typeof edata !== 'undefined' && edata !== null && parseInt(edata.length) > 0)
						{
							methods.convertMultiMetaArray(edata, function(retConverted){	

								var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_dairy_appointment_info";
	  							var lastGrpId = 0;

								methods.getLastGroupId(queGroupMax, function(queGrpRes){

								  	queGrpRes.forEach(function(row) {
										lastGrpId = row.lastGrpId;
								  	});

									var arrInsert = {
										contact_name         : retConverted[0].contact_name,
										company_account_id   : retConverted[0].account_id,
										venues               : retConverted[0].venue,
										contact_no           : retConverted[0].contact_no,
										contact_email        : retConverted[0].contact_email,
										reminder_mins        : 0,
										repetition_type      : 0,
										no_people            : 1,
										end_time             : dateFormat(new Date(retConverted[0].end_time), 'HH:MM:ss'),
										start_time           : dateFormat(new Date(retConverted[0].start_time), 'HH:MM:ss'),
										appointment_status   : 1,
										appointment_date     : retConverted[0].event_date,
										appointment_type     : 4,
										remarks              : '',
										update_date          : newdateForm,
										event_id             : retConverted[0].group_id,
										create_date          : newdateForm,
										status               : 1,
										account_id           : arrParams.account_id
									};

									var plusOneGroupID = lastGrpId+1;
							  		var querInsert = `insert into jaze_dairy_appointment_info (group_id, meta_key, meta_value) VALUES (?,?,?)`;

							  		var returnFunction;
								 	for (var key in arrInsert) 
								 	{
										DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
								 		 	if(parseInt(data.affectedRows) > 0){
						 						return_result(1);
									 	   	}else{
							   					return_result(0);
									 	   	}
								 	 	});
								  	};

								});

							});
						}
						else
						{
							return_result(3);	
						}

					});
				}

			});
		}
		else if(parseInt(arrParams.flag) == 2)
		{

			var queLib = `SELECT * FROM jaze_dairy_appointment_info WHERE group_id IN (SELECT group_id FROM jaze_dairy_appointment_info WHERE meta_key = 'appointment_type' AND meta_value = '4' )
				AND group_id IN (SELECT group_id FROM jaze_dairy_appointment_info WHERE meta_key = 'event_id' AND meta_value =? )
				AND group_id IN (SELECT group_id FROM jaze_dairy_appointment_info WHERE meta_key = 'account_id' AND meta_value =? )`;

			DB.query(queLib,[arrParams.event_id,arrParams.account_id], function(data, error){

						
				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{

					methods.convertMultiMetaArray(data, function(retConverted){

						var queDel = `DELETE FROM jaze_dairy_appointment_info where group_id =? `;
						DB.query(queDel,[retConverted[0].group_id], function(data, error){

						 	if(parseInt(data.affectedRows) > 0){
		 						return_result(5);
					 	   	}else{
			   					return_result(0);
					 	   	}

						});
			

					});

			   	}
		 	   	else
		 	   	{
		 	   		return_result(3);
		 	   	}

			});

		}
		else
		{
			return_result(4);
		}


	}
	catch(err)
	{ console.log("addToDiaryEvents",err); return_result(0);}
};



// ---- event category list ------ //


methods.getEventCategoryList =  function(arrParams,return_result){
	try
	{

		var arrObj = {};
		var arrResult = [];


		var query = `SELECT * FROM jaze_events_category`;
		if(parseInt(arrParams.category_id) !==0 ){

			query = "SELECT * FROM jaze_events_category WHERE group_id='"+arrParams.category_id+"' ";
		}

		
		DB.query(query,null, function(data, error){
			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);


					promiseForeach.each(jsonItems,[function (val) {
						return new Promise(function (resolve, reject) {		
				
					  		arrObj = {
					  			category_id : val.group_id.toString(),
					  			name        : val.category_name,
					  			logo        : G_STATIC_IMG_URL+"uploads/services_cat_images/"+val.image_url
					  		}
				  		
							counter -= 1;
					  		arrResult.push(arrObj);

							if (counter === 0){ 
								resolve(arrResult);
							}
							
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							if(parseInt(arrParams.account_id) !==0){
								methods.getEventSortOrder(arrParams, function(retSort){	
									if(retSort!=null){
										var ordered_array = mapOrder(result[0], retSort, 'category_id');
										return_result(ordered_array);
									}else{
										return_result(result[0]);
									}

									
								});
							}else{
								return_result(result[0]);
							}

							
						}
					});

				});
			
			}
			else
			{
				return_result('');
			}

		});

	}
	catch(err)
	{ console.log("getEventCategoryList",err); return_result('');}
};


methods.getEventSortOrder =  function(arrParams,return_result){
	try
	{
			
		var arrObj = {};
		var arrResult = [];

		var query = `SELECT * FROM jaze_events_category_priority WHERE account_id=?`;
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
				return_result(data[0].categories.split(","));

			}else{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getEventSortOrder",err); return_result(null);}
};


methods.updateEventCategorySorting =  function(arrParams,return_result){
	try
	{
			

		var arrObj = {};
		var arrResult = [];

		var query = `SELECT * FROM jaze_events_category_priority WHERE account_id=?`;
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				var updateQue = `update jaze_events_category_priority SET categories =? WHERE id=?`;
				
				DB.query(updateQue, [arrParams.category_id,data[0].id], function(udata, error){
		 		 	if(parseInt(udata.affectedRows) > 0){
		 				return_result(1);
			 	   	}else{
		 	   			return_result(0);
			 	   	}
				});
			

		
			}else{
				
	  			var mainQuer = `INSERT INTO jaze_events_category_priority (account_id, categories) VALUES (?,?)`;
  				DB.query(mainQuer,[arrParams.account_id,arrParams.category_id], function(idata, error){	
	 				if(parseInt(idata.affectedRows) > 0){
		 				return_result(1);
			 	   	}else{
		 	   			return_result(0);
			 	   	}
  				});
			}

		});

	}
	catch(err)
	{ console.log("updateEventCategorySorting",err); return_result(null);}
};


methods.getEventsListYear =  function(arrParams,return_result){
	try
	{
	
		var arrCategory = [];
		var query = `SELECT * FROM jaze_events_category`;

		DB.query(query,null, function(pdata, error){

			if(typeof pdata !== 'undefined' && pdata !== null && parseInt(pdata.length) > 0){

				methods.convertMultiMetaArray(pdata, function(retPdata){	

					var counter = retPdata.length;		
					var jsonItemsMain = Object.values(retPdata);

					promiseForeach.each(jsonItemsMain,[function (jsonItemsMain) {
						return new Promise(function (resolve, reject) {

							arrCategory.push(jsonItemsMain.group_id);
							counter -= 1;

							if (counter === 0){ 
								resolve(arrCategory);
							}
								
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
							var arrObjects = {};
							var arrResults = [];

							var date = new Date();
							var yearNow = date.getFullYear();

							var cquery = `SELECT * FROM jaze_events_details WHERE group_id IN ( SELECT group_id FROM jaze_events_details WHERE meta_key ='event_cat_id' AND meta_value IN(?) )`;
cquery += "AND group_id IN ( SELECT group_id FROM jaze_events_details WHERE meta_key LIKE 'end_date' AND YEAR(meta_value) >= '"+arrParams.year+"' AND group_id IN (SELECT group_id FROM jaze_events_details WHERE meta_key LIKE 'event_date' AND YEAR(meta_value) <= '"+arrParams.year+"')) ";
							if(parseInt(arrParams.city_id) !==0){
								cquery += " AND group_id IN (SELECT group_id FROM jaze_events_details WHERE meta_key='city_id' AND meta_value ='"+arrParams.city_id+"') ";
							}

							DB.query(cquery,[result[0]], function(cdata, error){

								if(typeof cdata !== 'undefined' && cdata !== null && parseInt(cdata.length) > 0)
								{

									methods.convertMultiMetaArray(cdata, function(retConverted){

										var counter = retConverted.length;		
										var jsonItemsMain = Object.values(retConverted);

										promiseForeach.each(jsonItemsMain,[function (jsonItemsMain) {
											return new Promise(function (resolve, reject) {
												
												arrResults.push(jsonItemsMain.event_date);
												arrResults.push(dateFormat(new Date(jsonItemsMain.end_time), 'yyyy-mm-dd'));

												counter -= 1;

												if (counter === 0){ 
													resolve(arrResults);
												}
									
											})
										}],
										function (result, current) {
											if (counter === 0){ 

												var	filteredArray = removeDuplicates(result[0]);
												filteredArray.sort();
												
												var min = filteredArray.reduce(function (a, b) { return a < b ? a : b; }); 
												var max = filteredArray.reduce(function (a, b) { return a > b ? a : b; });

												methods.getAllDates(min,max,function(retAllDates){

													return_result(retAllDates);

												});
											}
											
										});
						
									});

								}
								else
								{
									return_result(null);
								}

							});
						}

					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getEventCalendar",err); return_result(null);}
};


// ----- event category list end ----///

methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
};


methods.__getCountryDetails = function(arrParams,return_result){

	try{	

			if(arrParams ==''){
				arrParams = 0;
			}

			let query  = " SELECT country_name FROM country  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}

};

methods.__getStateDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT state_name FROM states  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].state_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getCityDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT city_name FROM cities  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].city_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};


methods.__getAreaDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT area_name FROM area  WHERE area_id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].area_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].group_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}


methods.convertMultiMetaArrayCompany =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndexCompany(list,arrvalues[i].account_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndexCompany(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].account_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}

function sortingArrayObject(key, order='asc') {

	return function(a, b) {
	  if(!a.hasOwnProperty(key) || 
		 !b.hasOwnProperty(key)) {
		  return 0; 
	  }
	  
	  const varA = (typeof a[key] === 'string') ? 
		a[key].toUpperCase() : a[key];
	  const varB = (typeof b[key] === 'string') ? 
		b[key].toUpperCase() : b[key];
		
	  let comparison = 0;
	  if (varA > varB) {
		comparison = 1;
	  } else if (varA < varB) {
		comparison = -1;
	  }
	  return (
		(order == 'desc') ? 
		(comparison * -1) : comparison
	  );
	};
}


function distance(lat1, lon1, lat2, lon2, unit) {

	if ((lat1 == lat2) && (lon1 == lon2)) {
		return 0;
	}
	else 
	{
		var radlat1 = Math.PI * lat1/180;
		var radlat2 = Math.PI * lat2/180;
		var theta = lon1-lon2;
		var radtheta = Math.PI * theta/180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist);
		dist = dist * 180/Math.PI;
		dist = dist * 60 * 1.1515;
		if (unit.toString()=="K") { dist = dist * 1.609344 }
		if (unit.toString()=="N") { dist = dist * 0.8684 }
		return dist;
	}
}



methods.getFileTypes = function(ext,return_result){

	var type;

	var arrImages = ['.jpeg','.jpg','.gif','.png','.tif'];
	var arrVideos = ['.mp4','.avi','.mov','.flv','.wmv'];
	var arrAudio = ['.pcm','.wav','.aiff','.mp3','.aac','.wma','.flac','.alac'];
	var arrDocs = ['.odt','.xls','.xlsx','.ods','.ppt','.pptx','.txt','.doc'];
	var arrPdf = ['.pdf'];

	if(arrImages.includes(ext.toLowerCase())){
		type = '2';
	}else if(arrAudio.includes(ext.toLowerCase())){
		type = '3';
	}else if(arrVideos.includes(ext.toLowerCase())){
		type = '4';
	}else if(arrDocs.includes(ext.toLowerCase())){
		type = '5';
	}else if(arrPdf.includes(ext.toLowerCase())){
		type = '6';
	}else{
		type = '7';
	}

	return_result(type);
};


function mapOrder (array, order, key) {
  
  array.sort( function (a, b) {
    var A = a[key], B = b[key];
    
    if (order.indexOf(A) > order.indexOf(B)) {
      return 1;
    } else {
      return -1;
    }
    
  });
  
  return array;
};


function removeDuplicates(array) {
  return array.filter((a, b) => array.indexOf(a) === b)
};



module.exports = methods;
