const methods = {};
const DB = require('../../helpers/db_base.js');
const HelperGeneral = require('../../helpers/HelperGeneral.js');
const HelperGeneralJazenet = require('../helpers/HelperGeneralJazenet.js');
const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperNotification = require('../../jazecom/helpers/HelperNotification.js');
var async = require('async');

const promiseForeach = require('promise-foreach');
const fast_sort = require("fast-sort");
var rtrim = require('rtrim');
var ltrim = require('ltrim');


const dateFormat = require('dateformat');
const now = new Date();
const standardDT = dateFormat(now,'yyyy-mm-dd HH:MM:ss');
const moment = require("moment-timezone");


const standardDTNew = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
var newdateForm = dateFormat(new Date(standardDTNew), 'yyyy-mm-dd HH:MM:ss');

const STATIC_IMG_EXTERNAL = G_STATIC_IMG_URL+"uploads/jazenet/jazepay/external_system_partner/";

const IMG_URL_PRODUCTS = G_STATIC_IMG_URL+"uploads/jazenet/company/";


//------------------------------------------------------------------- Search Module Start ---------------------------------------------------------------------------//

methods.searchAccounts = function(arrParams,return_result)
{
	try
	{
		if(arrParams !== null)
		{	
			var query  = "";
			var meta_key = '';
			var account_type = '';

				if(parseInt(arrParams.account_type) === 1)
				{ meta_key = "'fname','lname'"; account_type = "1"; }
				else if(parseInt(arrParams.account_type) === 3)
				{ meta_key = "'fname','lname','company_name'"; account_type = "1,3"; }

			if(parseInt(arrParams.flag) === 1)
			{
				query += " SELECT group_concat(account_id) as account_id FROM jaze_user_basic_details WHERE  account_type IN ("+account_type+") and meta_key IN ('status')";
				query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1') ";
				query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country_id+"')  ";
				query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state_id+"' ) ";
				query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city_id+"')  ";
				query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ("+meta_key+")  AND LOWER(meta_value) like '%"+rtrim(ltrim(arrParams.keyword.toLowerCase()))+"%' ) ";
				query += " ORDER BY  CASE WHEN meta_key IN ("+meta_key+") THEN meta_value END asc ";
				query += " limit 20";
			}
			else if(parseInt(arrParams.flag) === 2)
			{
				query += " SELECT account_id FROM jaze_user_jazenet_id WHERE  category IN ("+account_type+") and concat(jazenet_id,unique_id)  = '"+rtrim(ltrim(arrParams.keyword.toLowerCase()))+"' ";
			}
			else if(parseInt(arrParams.flag) === 3)
			{
				query += " SELECT group_concat(account_id) as account_id FROM jaze_user_basic_details WHERE  account_type IN (3) and meta_key IN ('status')";
				query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'status'  AND meta_value = '1') ";
				query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country_id+"')  ";
				query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'state_id'  AND meta_value = '"+arrParams.state_id+"')  ";
				query += " and  account_id  IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key = 'city_id'  AND meta_value = '"+arrParams.city_id+"')  ";
				query += " and  account_id IN  (SELECT account_id FROM jaze_user_basic_details WHERE meta_key IN ('company_name')  AND LOWER(meta_value) like '%"+rtrim(ltrim(arrParams.keyword.toLowerCase()))+"%' ) ";
				query += " ORDER BY  CASE WHEN meta_key IN ('company_name') THEN meta_value END asc ";
				query += " limit 20";
			}
		
			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					if (typeof data[0].account_id !== 'undefined' && data[0].account_id !== null)
					{ 
						var data_list = __convert_array(data[0].account_id);
						
						var counter = data_list.length;

						if(parseInt(counter) !== 0)
						{	
							var array_rows = [];
							
							promiseForeach.each(data_list,[function (list) {
									
								return new Promise(function (resolve, reject) {

									var reqParams = {
											account_id : list
										}; 
										
									HelperGeneral.getAccountDetails(reqParams, function(account_result){

										if(account_result!=null){

											methods.__getCompanyProductStatus(arrParams,account_result, function(product_status){
										
												var objRres = {
														account_id	    	: account_result.account_id,
														account_type    	: account_result.account_type,
														account_name		: account_result.name,
														account_logo		: account_result.logo,
														account_title		: account_result.title,
														account_category	: account_result.category,
														area_name			: account_result.area_name,
														city_name			: account_result.city_name,
														product_status		: product_status.toString()    // 1 : have products, 2 : no products, 0: other accounts
													};

												array_rows.push(objRres);
												
												counter -= 1;
												
												if (counter === 0)
												{ resolve(array_rows); }
											});

										}else{counter -= 1;}

					
									});
								});
							}],
							function (result, current) {

								if (counter === 0)
								{  
									if(parseInt(arrParams.flag) === 1 || parseInt(arrParams.flag) === 3)
									{ return_result(sort_name((result[0]),arrParams.keyword));  }
									else if(parseInt(arrParams.flag) === 2)
									{ return_result(result[0]); } 
								}
							});
						}
						else
						{ return_result(null); }
					}
					else
					{ return_result(null); }
				}
				else
				{ return_result(null); }
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log('searchAccounts',err);
		return_result(null);
	}
}

methods.__getCompanyProductStatus = function(arrParams,account_result,return_result)
{
	try
	{
		if(arrParams !== null && parseInt(account_result.account_type) === 3)
		{
			var query = " SELECT count(id) as count  FROM `jaze_products` WHERE `account_id` = '"+account_result.account_id+"' ";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					if (parseInt(data[0].count) > 0)
					{  return_result('1'); }
					else
					{ return_result('2'); }
				}
				else
				{ return_result('2'); }
			});
		}
		else
		{ return_result('0'); }
	}
	catch(err)
	{
		console.log('__getCompanyProductStatus',err);
		return_result('0');
	}
}
//------------------------------------------------------------------- Search Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- System Partners Payment Module Start ---------------------------------------------------------------------------//

methods.getExternalSystemPartners = function(arrParams,company_id,return_result)
{
	try
	{
		if(arrParams !== null)
		{
			var query = " SELECT *  FROM jaze_master_external_system_partner WHERE meta_key in ('company_name','logo_path')";
				query += " and  group_id in (SELECT group_id  FROM jaze_master_external_system_partner WHERE  meta_key = 'status' and meta_value = '1' ";

				if(parseInt(arrParams.country_id) !== 0)
				{
					query += " and  group_id  IN  (SELECT group_id FROM jaze_master_external_system_partner WHERE meta_key = 'country_id'  AND meta_value = '"+arrParams.country_id+"') ";
				}

				if(parseInt(arrParams.state_id) !== 0)
				{
					query += " and  group_id  IN  (SELECT group_id FROM jaze_master_external_system_partner WHERE meta_key = 'state_id'  AND (meta_value = '"+arrParams.state_id+"' or meta_value = '0')) ";
				}

				if(parseInt(arrParams.city_id) !== 0)
				{
					query += " and  group_id  IN  (SELECT group_id FROM jaze_master_external_system_partner WHERE meta_key = 'city_id'  AND (meta_value = '"+arrParams.city_id+"' or meta_value = '0')) ";
				}

				if(parseInt(company_id) !== 0)
				{
					query += " and  group_id = '"+company_id+"' ";
				}
				
				query += " )";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					HelperRestriction.convertMultiMetaArray(data, function(data_result){
	
						var counter = data_result.length;
										
						if(parseInt(counter) !== 0)
						{
							var array_rows = [];
							
							promiseForeach.each(data_result,[function (list) {
									
								return new Promise(function (resolve, reject) {

									var objRres = {
											company_id	    	: list.group_id.toString(),
											company_name		: list.company_name,
											company_logo		: G_STATIC_IMG_URL+'uploads/jazenet/jazepay/external_system_partner/'+list.logo_path,
										}

									array_rows.push(objRres);
											
									counter -= 1;
											
									if (counter === 0)
									{ resolve(array_rows); }

								});
							}],
							function (result, current) {
								if (counter === 0)
								{ return_result(result[0]); }
							});
						}
						else
						{ return_result(null); }
					});
				}
				else
				{ return_result(null); }
			});
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{
		console.log('getExternalSystemPartners',err);
		return_result(null);
	}
}

//------------------------------------------------------------------- System Partners Payment Module End ---------------------------------------------------------------------------//



//------------------------------------------------------------------- Intra transfer Module Start ----------------------------------------------------------------------------------//


methods.procInstaTransfer =  function (arrParams,return_result){
	
	try
	{			
		if(parseInt(arrParams.account_id) !== 0 )
		{	

			methods.getJazePayBalance(arrParams.account_id,function(retBalance){

				if(parseInt(retBalance) > parseInt(arrParams.amount))
				{
					var compute_charges = parseFloat(parseFloat(arrParams.amount) * parseFloat(0.05)).toFixed(2);

					var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_jazenet_transfer_transaction_list";
					var lastGrpId = 0;

					var arrInsert = {
						account_id  : arrParams.account_id,
						profile_id  : arrParams.request_account_id,
						amount      : arrParams.amount,
						charges     : compute_charges,
						remarks     : arrParams.remarks,
						date        : newdateForm,
						status      : 1
					}

					var arrLength = Object.keys(arrInsert).length;
					var num_rows_insert = 0;

					var transParam = {account_id:arrParams.account_id,request_account_id:arrParams.request_account_id,amount:arrParams.amount};
					methods.getLastGroupId(queGroupMax, function(queGrpRes){

						lastGrpId = queGrpRes[0].lastGrpId;
						var plusOneGroupID = lastGrpId+1;

						var querInsert = `insert into jaze_jazepay_jazenet_transfer_transaction_list (group_id, meta_key, meta_value) VALUES (?,?,?)`;
						for (var key in arrInsert) 
					 	{
							DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
								num_rows_insert += data.affectedRows;
								if(parseInt(num_rows_insert) === arrLength){

								  	//sender record
									var queGroupMaxS= "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_statements";
									var lastGrpIdS = 0;

									var arrInsertS = {
										account_id    : arrParams.account_id,
										amount		  : arrParams.amount,
										type          : 3,
										category_id   : plusOneGroupID,
										category_type : 1,
										charges       : compute_charges,
										date_created  : newdateForm,
										status        : 1
									}

									methods.getLastGroupId(queGroupMaxS, function(queGrpResS){

										lastGrpIdS = queGrpResS[0].lastGrpId;
										var plusOneGroupIDS = lastGrpIdS+1;

										var querInsertS = `insert into jaze_jazepay_statements (group_id, meta_key, meta_value) VALUES (?,?,?)`;
										for (var keyS in arrInsertS) 
									 	{
											DB.query(querInsertS,[plusOneGroupIDS,keyS,arrInsertS[keyS]], function(dataS, error){});

									  	};

									});


									methods.procJazePayAccount(transParam,function(retStatus){
										return_result(retStatus);
									});
									
								}
							});

					  	};


					  	//receiver record
			  			var queGroupMaxS= "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_statements";
						var lastGrpIdS = 0;

						var arrInsertS = {
							account_id    : arrParams.request_account_id,
							amount		  : arrParams.amount,
							type          : 3,
							category_id   : plusOneGroupID,
							category_type : 2,
							charges       : compute_charges,
							date_created  : newdateForm,
							status        : 1
						}

						methods.getLastGroupId(queGroupMaxS, function(queGrpResS){

							lastGrpIdS = queGrpResS[0].lastGrpId;
							var plusOneGroupIDS = lastGrpIdS+1;

							var querInsertS = `insert into jaze_jazepay_statements (group_id, meta_key, meta_value) VALUES (?,?,?)`;
							for (var keyS in arrInsertS) 
						 	{
								DB.query(querInsertS,[plusOneGroupIDS,keyS,arrInsertS[keyS]], function(dataS, error){});

						  	};

						});



							// 150 notification type
							//send notification starts here
							// var senParam = {account_id:arrParams.account_id};
							// HelperGeneralJazenet.getAccountDetails(senParam, function(retSender){

							// 	var recParam = {account_id:arrParams.request_account_id};
							// 	HelperGeneralJazenet.getAccountDetails(recParam, function(retReceiver){	


							// 		var notiParams = {	
							// 			group_id             : plusOneGroupID,
							// 			account_id           : retSender.account_id,
							// 			account_type         : retSender.account_type,
							// 			request_account_id   : retReceiver.account_id,
							// 			request_account_type : retReceiver.account_type,
							// 			message              : 'Jazepay Intra Transfer received.,
							// 			flag                 :'150'
							// 		};

								
							// 		HelperNotification.sendPushNotification(notiParams, function(retNoti){});

							// 	});
							// });
					});

					if(parseInt(arrParams.is_favorites) === 1)
					{

						var querCheck = `SELECT * FROM jaze_jazepay_jazenet_transfer_account_list WHERE group_id 
						IN (select group_id from jaze_jazepay_jazenet_transfer_account_list where meta_key = 'account_id' and meta_value =?)
						AND group_id IN (SELECT group_id FROM jaze_jazepay_jazenet_transfer_account_list WHERE meta_key = 'profile_id' AND meta_value =?)
						AND group_id IN (SELECT group_id FROM jaze_jazepay_jazenet_transfer_account_list WHERE meta_key = 'fav_status' AND meta_value ='1')`;

						DB.query(querCheck,[arrParams.account_id,arrParams.request_account_id], function(dataC, error){

							if( parseInt(dataC.length) === 0 )
							{
								
								var queGroupMaxIn = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_jazenet_transfer_account_list";
								var lastGrpIdIn = 0;

								var arrInsertIn = {
									account_id  : arrParams.account_id,
									profile_id  : arrParams.request_account_id,
									date        : newdateForm,
									fav_status  : 1,
									status      : 1
								}

								methods.getLastGroupId(queGroupMaxIn, function(queGrpResIn){

									lastGrpIdIn = queGrpResIn[0].lastGrpId;
									var plusOneGroupIDIn = lastGrpId+1;

									var querInsertIn = `insert into jaze_jazepay_jazenet_transfer_account_list (group_id, meta_key, meta_value) VALUES (?,?,?)`;
									for (var keyIn in arrInsertIn) 
								 	{
										DB.query(querInsertIn,[plusOneGroupIDIn,keyIn,arrInsertIn[keyIn]], function(dataIn, error){});

								  	};

								});

							}
							
						});


					}




				}
				else
				{
					return_result(2); 
				}

			});





		}
		else
		{
			return_result(0); 
		}

	}
	catch(err)
	{ 
		console.log("procInstaTransfer",err); return_result(0); 
	}

};


methods.procJazePayAccount =  function (arrParams,return_result){
	
	try
	{				
		var compute_charges = parseFloat(parseFloat(arrParams.amount) * parseFloat(0.05)).toFixed(2);

		methods.getJazePayBalance(arrParams.account_id,function(retSenBal){

			var new_sender_balance = parseFloat( parseFloat(retSenBal)-parseFloat(arrParams.amount) ).toFixed(2);

			methods.getJazePayBalance(arrParams.request_account_id,function(retRecBal){

				var new_receiver_balance = parseFloat( parseFloat(retRecBal)+parseFloat(arrParams.amount) ).toFixed(2);

				var total_receiver_balance = parseFloat( parseFloat(new_receiver_balance) - parseFloat(compute_charges) ).toFixed(2);

				var updateQuer = `update jaze_jazepay_accounts SET meta_value =? WHERE meta_key =? AND group_id =?`;

				var query = `SELECT *  FROM jaze_jazepay_accounts WHERE 
					group_id in (SELECT group_id  FROM jaze_jazepay_accounts WHERE  meta_key = 'account_id' and meta_value =?)`;
	
				DB.query(query, [arrParams.account_id], function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{ 
						
						var	arrSender = {
							current_balance  : new_sender_balance,
							date_updated     : standardDT
						};

						var	arrRec = {
							current_balance  : total_receiver_balance,
							date_updated     : standardDT
						};

						var arrLengthS = Object.keys(arrSender).length;
						var num_rows_sender = 0;
						for (var keyS in arrSender) 
					 	{	
							DB.query(updateQuer, [arrSender[keyS],keyS,data[0].group_id], function(dataS, error){
					 		num_rows_sender += dataS.affectedRows;
								if(parseInt(num_rows_sender) === arrLengthS){
									
									DB.query(query, [arrParams.request_account_id], function (dataIn, error) {

										if (typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) !== 0)
										{ 
											//update if receiver has a account
											var arrLengthR = Object.keys(arrRec).length;
											var num_rows_rec = 0;
											for (var keyR in arrRec) 
										 	{	
												DB.query(updateQuer, [arrRec[keyR],keyR,dataIn[0].group_id], function(dataR, error){
										 		num_rows_rec += dataR.affectedRows;
													if(parseInt(num_rows_rec) === arrLengthR){
								
														return_result(1);
														
													}
												});
											
											}

										}
										else
										{

											//insert if receiver has no account
											var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_accounts";
											var lastGrpId = 0;

											var arrInsert = {
												account_id       : arrParams.request_account_id,
												current_balance  : new_receiver_balance,
												date_created     : standardDT,
												date_updated     : standardDT,
												status           : 1
											}

											var arrLength = Object.keys(arrInsert).length;
											var num_rows_insert = 0;

											methods.getLastGroupId(queGroupMax, function(queGrpRes){

												lastGrpId = queGrpRes[0].lastGrpId;
												var plusOneGroupID = lastGrpId+1;

												var querInsert = `insert into jaze_jazepay_accounts (group_id, meta_key, meta_value) VALUES (?,?,?)`;
												for (var key in arrInsert) 
											 	{
													DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
														num_rows_insert += data.affectedRows;
														if(parseInt(num_rows_insert) === arrLength){
															return_result(1);
														}
													});

											  	};

											});
										}

									});
									
								}

							});
						
						}

					}
			
				});

	
			});

		});
	
	}
	catch(err)
	{ 
		console.log("procJazePayAccount",err); return_result(0); 
	}

};


//------------------------------------------------------------------- Intra transfer Module End ------------------------------------------------------------------------------------//

//------------------------------------------------------------------- Intra transfer History Start ---------------------------------------------------------------------------------//

methods.getIntraTransferHistory =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var current_m = now.getMonth()+1;
		var current_y = now.getFullYear();

		var query = "SELECT * FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE group_id IN (SELECT group_id FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE (meta_key = 'account_id' OR meta_key='profile_id') AND meta_value=?) ";
			
			
		if((arrParams.month !="" && arrParams.year !="") && (arrParams.from_date =="" && arrParams.to_date =="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE meta_key = 'date' AND MONTH(meta_value)='"+arrParams.month+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE meta_key = 'date' AND YEAR(meta_value)='"+arrParams.year+"') ";
		}
		else if((arrParams.month =="" && arrParams.year =="") && (arrParams.from_date !="" && arrParams.to_date !="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE meta_key = 'date' AND DATE(meta_value) >='"+arrParams.from_date+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE meta_key = 'date' AND DATE(meta_value) <='"+arrParams.to_date+"') ";
		}
		else 
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE meta_key = 'date' AND MONTH(meta_value)='"+current_m+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE meta_key = 'date' AND YEAR(meta_value)='"+current_y+"') ";
		}


		DB.query(query,[arrParams.account_id], function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
							
							//1 sent
							//2 receive
							var accParams = {account_id:jsonItems.profile_id};

							var trans_type = 1;
							if( jsonItems.profile_id == arrParams.account_id ){
								accParams.account_id = jsonItems.account_id;
								trans_type = 2;
							}

							HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

								if(retAccountDetails!=null)
								{	

									var tran_no = parseInt(jsonItems.group_id)+100;

									objvalues = {
										tran_no    : 'JT'+pad_with_zeroes(tran_no,5).toString(),
										group_id   : jsonItems.group_id.toString(),
										account_id : retAccountDetails.account_id,
										name       : retAccountDetails.name,
										logo       : retAccountDetails.logo,
										intra_type : trans_type.toString(),
										amount     : jsonItems.amount,
										charges    : jsonItems.charges,
										remarks    : jsonItems.remarks,
										date       : jsonItems.date
									}

									arrValues.push(objvalues);
								}
			
								counter -= 1;

								if (counter === 0){ 
									resolve(arrValues);
								}							
							
							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getIntraTransferHistory",err); return_result(null); }	
};




methods.getIntraTranferDetailsForNotification =  function (arrParams,return_result){
	
	try
	{			
	 


		query = `SELECT * FROM jaze_jazepay_statements WHERE group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'account_id' AND meta_value=?)
		AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'type' AND meta_value='3') 
		AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'category_id' AND meta_value=?) `;

		var objvalues = {};

		DB.query(query, [arrParams.account_id,arrParams.id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							var receipt_id = parseFloat(jsonItems.category_id);
							var receipt_no = 'AD'+pad_with_zeroes(receipt_id,5).toString();

							objvalues = {
								id             : jsonItems.group_id.toString(),
								receipt_no     : receipt_no,
								charges        : parseFloat(jsonItems.charges).toFixed(2),
								amount         : parseFloat(jsonItems.amount).toFixed(2),
							};

							counter -= 1;	
							if (counter === 0){ 
								resolve(objvalues);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 
											
			  				return_result(result[0]);					
						}
					});

				});

			}
			else{return_result(null); }

		});
	
	}
	catch(err)
	{ 
		console.log("getIntraTranferDetails",err); return_result(null); 
	}

};

//------------------------------------------------------------------- Intra transfer History Start ---------------------------------------------------------------------------------//


//------------------------------------------------------------------- Intra transfer Favorties List Start --------------------------------------------------------------------------//


methods.getIntraFavoritesList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};



		var query = "SELECT * FROM jaze_jazepay_jazenet_transfer_account_list WHERE group_id IN (SELECT group_id FROM jaze_jazepay_jazenet_transfer_account_list WHERE meta_key = 'account_id' AND meta_value =?)";
			
		DB.query(query,[arrParams.account_id], function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
							
							var accParams = {account_id:jsonItems.profile_id };
							HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

								if(retAccountDetails!=null)
								{	

									var queryIn = `SELECT * FROM jaze_user_jazenet_id WHERE account_id=?`;
			
									DB.query(queryIn,[retAccountDetails.account_id], function(dataIn, error){

										if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
										{
											

											objvalues = {
												group_id     : jsonItems.group_id.toString(),
												jazenet_id   : dataIn[0].jazenet_id,
												unique_id    : dataIn[0].unique_id,
												account_id   : retAccountDetails.account_id,
												account_type : retAccountDetails.account_type,
												name         : retAccountDetails.name,
												logo         : retAccountDetails.logo,
												date         : jsonItems.date
											}

											arrValues.push(objvalues);

										}

										counter -= 1;

										if (counter === 0){ 
											resolve(arrValues);
										}	


									});
								}
			
										
							
							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getIntraFavoritesList",err); return_result(null); }	
};


//------------------------------------------------------------------- Intra transfer Favorties List End ----------------------------------------------------------------------------//

//------------------------------------------------------------------ Intra transfer Remove Favorties Start -------------------------------------------------------------------------//
methods.removeIntraFavorites =  function (arrParams,return_result){
	
	try
	{				

		var query = "SELECT * FROM jaze_jazepay_jazenet_transfer_account_list WHERE group_id IN("+arrParams.favorite_id+")";
		DB.query(query,null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				var queDelC = "DELETE FROM jaze_jazepay_jazenet_transfer_account_list WHERE group_id IN("+arrParams.favorite_id+")";
				DB.query(queDelC,null, function(dataDel, error){
				 	if(parseInt(dataDel.affectedRows) > 0)
					{
						return_result(1);
					}
					else
					{
						return_result(0);
					}
				});
			}
			else
			{
				return_result(2);
			}

		});

	}
	catch(err)
	{ console.log("removeIntraFavorites",err); return_result(0); }	
};

//------------------------------------------------------------------ Intra transfer Remove Favorties End -------------------------------------------------------------------------//



//------------------------------------------------------------------ Jazepay Add Funds Start -------------------------------------------------------------------------//

methods.jazePayAddFunds =  function (arrParams,return_result){
	
	try
	{			
	    var creditCardType = require('credit-card-type');
		var card_obj = {
		    name       : '',
		    logo       : ''
		};

		var query = `SELECT * FROM jaze_user_credit_card WHERE group_id =?`;
		DB.query(query,[arrParams.payment_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
					
				methods.convertMultiMetaArray(data, function(retConverted){

	            	var visaCards = creditCardType(retConverted[0].card_no)[0];


		            if (typeof visaCards !== 'undefined' && visaCards !== null && parseInt(visaCards.length) !== 0)
		            {
						card_obj.name = visaCards.niceType;
						card_obj.logo = G_STATIC_IMG_URL+"uploads/jazenet/creditcard_logos/"+visaCards.type+".png";
		            }
		

					var obj_return = {
						payment_type : '2',
						card_name    : retConverted[0].card_name,
						card_no      : retConverted[0].card_no,
						card_details : card_obj,
						amount       : parseFloat(arrParams.amount).toFixed(2)
					}


					var charges = parseFloat(parseFloat(arrParams.amount) * parseFloat(0.05)).toFixed(2);

					var computed_amount = parseFloat(parseFloat(arrParams.amount) - parseFloat(charges)).toFixed(2);

					var queGroupMaxS= "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_statements";
					var lastGrpIdS = 0;

					var arrInsertS = {
						account_id    : arrParams.account_id,
						amount		  : computed_amount,
						type          : 1,
						category_id   : data[0].group_id,
						category_type : arrParams.payment_type,
						charges       : charges,
						date_created  : standardDT,
						status        : 1
					}

					var arrLengthS = Object.keys(arrInsertS).length;
					var num_rows_insertS = 0;

					methods.getLastGroupId(queGroupMaxS, function(queGrpResS){

						lastGrpIdS = queGrpResS[0].lastGrpId;
						var plusOneGroupIDS = lastGrpIdS+1;

						var querInsertS = `insert into jaze_jazepay_statements (group_id, meta_key, meta_value) VALUES (?,?,?)`;
						for (var keyS in arrInsertS) 
					 	{
							DB.query(querInsertS,[plusOneGroupIDS,keyS,arrInsertS[keyS]], function(dataS, error){
								num_rows_insertS += dataS.affectedRows;
								if(parseInt(num_rows_insertS) === arrLengthS){

									methods.procJazePayAccountForAddFunds(arrParams,function(returnStatus){
										if(parseInt(returnStatus)===1){
											return_result(returnStatus,obj_return);
										}
							
									});
								}

							});
					  	};

					});
				});

			}	
			else
			{
				return_result(2);
			}

		});

	}
	catch(err)
	{ console.log("jazePayAddFunds",err); return_result(0); }	
};



methods.procJazePayAccountForAddFunds =  function (arrParams,return_result){
	
	try
	{				
		var charges = parseFloat(parseFloat(arrParams.amount) * parseFloat(0.05)).toFixed(2);

		methods.getJazePayBalance(arrParams.account_id,function(retBal){

			var computed_amount = parseFloat(parseFloat(arrParams.amount) - parseFloat(charges)).toFixed(2);
			var new_balance = parseFloat( parseFloat(retBal)+parseFloat(computed_amount) ).toFixed(2);

			if(retBal !='0.00')
			{

				//update if has account
				var updateQuer = `update jaze_jazepay_accounts SET meta_value =? WHERE meta_key =? AND group_id =?`;
				var query = `SELECT *  FROM jaze_jazepay_accounts WHERE 
					group_id in (SELECT group_id  FROM jaze_jazepay_accounts WHERE  meta_key = 'account_id' and meta_value =?)`;

				DB.query(query, [arrParams.account_id], function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{ 
						
						var	arrSender = {
							current_balance  : new_balance,
							date_updated     : standardDT
						};

						var arrLengthS = Object.keys(arrSender).length;
						var num_rows_sender = 0;
						for (var keyS in arrSender) 
					 	{	
							DB.query(updateQuer, [arrSender[keyS],keyS,data[0].group_id], function(dataS, error){
					 		num_rows_sender += dataS.affectedRows;
								if(parseInt(num_rows_sender) === arrLengthS){
									
									return_result(1);
									
								}

							});
						
						}

					}
			
				});
			}
			else
			{		

				//insert if no account
				var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_accounts";
				var lastGrpId = 0;

				var arrInsert = {
					account_id       : arrParams.account_id,
					current_balance  : new_balance,
					date_created     : standardDT,
					date_updated     : standardDT,
					status           : 1
				}

				var arrLength = Object.keys(arrInsert).length;
				var num_rows_insert = 0;

				methods.getLastGroupId(queGroupMax, function(queGrpRes){

					lastGrpId = queGrpRes[0].lastGrpId;
					var plusOneGroupID = lastGrpId+1;

					var querInsert = `insert into jaze_jazepay_accounts (group_id, meta_key, meta_value) VALUES (?,?,?)`;
					for (var key in arrInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
							num_rows_insert += data.affectedRows;
							if(parseInt(num_rows_insert) === arrLength){
								return_result(1);
							}
						});

				  	};

				});

			}
		

		});
	
	}
	catch(err)
	{ 
		console.log("procJazePayAccountForAddFunds",err); return_result(0); 
	}

};


//------------------------------------------------------------------ Jazepay Add Funds End  -------------------------------------------------------------------------//



methods.procSystemPartnersPayment =  function (arrParams,return_result){
	
	try
	{			
		if(parseInt(arrParams.account_id) !== 0 )
		{	

			methods.getJazePayBalance(arrParams.account_id,function(retBalance){

				if(parseInt(retBalance) > parseInt(arrParams.amount))
				{
				
					var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_system_partner_transaction_list";
					var lastGrpId = 0;

					var arrInsert = {
						account_id  : arrParams.account_id,
						partner_id  : arrParams.request_account_id,
						amount      : arrParams.amount,
						remarks     : arrParams.remarks,
						date        : standardDT,
						status      : 1
					}

					var arrLength = Object.keys(arrInsert).length;
					var num_rows_insert = 0;

					var transParam = {account_id:arrParams.account_id,request_account_id:arrParams.request_account_id,amount:arrParams.amount};
					methods.getLastGroupId(queGroupMax, function(queGrpRes){

						lastGrpId = queGrpRes[0].lastGrpId;
						var plusOneGroupID = lastGrpId+1;

						var querInsert = `insert into jaze_jazepay_system_partner_transaction_list (group_id, meta_key, meta_value) VALUES (?,?,?)`;
						for (var key in arrInsert) 
					 	{
							DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
								num_rows_insert += data.affectedRows;
								if(parseInt(num_rows_insert) === arrLength){


									var queGroupMaxS= "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_statements";
									var lastGrpIdS = 0;

									var arrInsertS = {
										account_id    : arrParams.account_id,
										amount		  : arrParams.amount,
										type          : 2,
										category_id   : plusOneGroupID,
										category_type : 1,
										charges       : 0,
										date_created  : standardDT,
										status        : 1
									}

									methods.getLastGroupId(queGroupMaxS, function(queGrpResS){

										lastGrpIdS = queGrpResS[0].lastGrpId;
										var plusOneGroupIDS = lastGrpIdS+1;

										var querInsertS = `insert into jaze_jazepay_statements (group_id, meta_key, meta_value) VALUES (?,?,?)`;
										for (var keyS in arrInsertS) 
									 	{
											DB.query(querInsertS,[plusOneGroupIDS,keyS,arrInsertS[keyS]], function(dataS, error){});

									  	};

									});


									methods.procJazePayAccountForSystemPartners(transParam,function(retStatus){
										return_result(retStatus);
									});
									
								}
							});

					  	};

					});


					var queGroupMaxIn = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_system_partner_account_list";
					var lastGrpIdIn = 0;

					var arrInsertIn = {
						account_id     : arrParams.account_id,
						partner_id     : arrParams.request_account_id,
						account_name   : arrParams.account_name,
						account_number : arrParams.account_number,
						fav_status     : arrParams.is_favorites,
						status         : 1
					}

					methods.getLastGroupId(queGroupMaxIn, function(queGrpResIn){

						lastGrpIdIn = queGrpResIn[0].lastGrpId;
						var plusOneGroupIDIn = lastGrpId+1;

						var querInsertIn = `insert into jaze_jazepay_system_partner_account_list (group_id, meta_key, meta_value) VALUES (?,?,?)`;
						for (var keyIn in arrInsertIn) 
					 	{
							DB.query(querInsertIn,[plusOneGroupIDIn,keyIn,arrInsertIn[keyIn]], function(dataIn, error){});

					  	};

					});


				}
				else
				{
					return_result(2); 
				}

			});


		}
		else
		{
			return_result(0); 
		}

	}
	catch(err)
	{ 
		console.log("procInstaTransfer",err); return_result(0); 
	}

};


methods.procJazePayAccountForSystemPartners =  function (arrParams,return_result){
	
	try
	{				
		var compute_charges = parseFloat(parseFloat(arrParams.amount) * parseFloat(0.05)).toFixed(2);

		methods.getJazePayBalance(arrParams.account_id,function(retSenBal){

			var new_sender_balance = parseFloat( parseFloat(retSenBal)-parseFloat(arrParams.amount) ).toFixed(2);

			var updateQuer = `update jaze_jazepay_accounts SET meta_value =? WHERE meta_key =? AND group_id =?`;

			var query = `SELECT *  FROM jaze_jazepay_accounts WHERE 
					group_id in (SELECT group_id  FROM jaze_jazepay_accounts WHERE  meta_key = 'account_id' and meta_value =?)`;
	
			DB.query(query, [arrParams.account_id], function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					
					var	arrSender = {
						current_balance  : new_sender_balance,
						date_updated     : standardDT
					};

					var arrLengthS = Object.keys(arrSender).length;
					var num_rows_sender = 0;
					for (var keyS in arrSender) 
				 	{	
						DB.query(updateQuer, [arrSender[keyS],keyS,data[0].group_id], function(dataS, error){
				 		num_rows_sender += dataS.affectedRows;
							if(parseInt(num_rows_sender) === arrLengthS){
								
								return_result(1); 
								
							}

						});
					
					}

				}
	

			});

		});
	
	}
	catch(err)
	{ 
		console.log("procJazePayAccountForSystemPartners",err); return_result(0); 
	}

};


//------------------------------------------------------------------- General Module Start ---------------------------------------------------------------------------//



//------------------------------------------------------------------ Jazepay Add Funds End  -------------------------------------------------------------------------//


//------------------------------------------------------------------ national and international transfers start  -------------------------------------------------------------------------//


methods.favoritesCheck =  function (arrParams,type,return_result){
	
	try
	{				

		var table = 'jaze_jazepay_national_account_list';
		if(parseInt(type) === 2){
			table = 'jaze_jazepay_international_account_list';
		}

		var query = "SELECT * FROM "+table+" WHERE group_id in (SELECT group_id FROM "+table+" WHERE meta_key = 'account_id' AND meta_value=?) ";
			query += "	AND group_id IN (select group_id from "+table+" WHERE meta_key = 'account_number' AND meta_value=?) ";
			query += "	AND group_id IN (SELECT group_id FROM "+table+" WHERE meta_key = 'branch_name' AND meta_value=?) ";
			query += "	AND group_id IN (SELECT group_id FROM "+table+" WHERE meta_key = 'fav_status' AND meta_value='1') ";

		DB.query(query, [arrParams.account_id,arrParams.account_number,arrParams.branch_name], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0){ 
				return_result(0); 
			}else{
				return_result(1); 
			}
		});
	
	}
	catch(err)
	{ 
		console.log("favoritesCheck",err); return_result(0); 
	}

};

methods.procNationalTransfers =  function (arrParams,return_result){
	
	try
	{			

		if(parseInt(arrParams.account_id) !== 0 )
		{	

			methods.getJazePayBalance(arrParams.account_id,function(retBalance){

				methods.favoritesCheck(arrParams,1,function(ret_is_fav){

					if(parseInt(retBalance) > parseInt(arrParams.amount))
					{
					
						var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_national_account_list";
						var lastGrpId = 0;

						var arrInsert = {
							account_id   : arrParams.account_id,
							account_name : arrParams.account_name,
							bank_name    : arrParams.bank_name,
							branch_name  : arrParams.branch_name,
							iban_number  : arrParams.iban_number,
							account_number : arrParams.account_number,
							fav_status     : ret_is_fav,
							status         : 1
						}

						var arrLength = Object.keys(arrInsert).length;
						var num_rows_insert = 0;

						methods.getLastGroupId(queGroupMax, function(queGrpRes){

							lastGrpId = queGrpRes[0].lastGrpId;
							var plusOneGroupID = lastGrpId+1;

							var querInsert = `insert into jaze_jazepay_national_account_list (group_id, meta_key, meta_value) VALUES (?,?,?)`;
							for (var key in arrInsert) 
						 	{
								DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
									num_rows_insert += data.affectedRows;
									if(parseInt(num_rows_insert) === arrLength){


										var queGroupMaxS= "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_statements";
										var lastGrpIdS = 0;

										var arrInsertS = {
											account_id    : arrParams.account_id,
											amount		  : arrParams.amount,
											type          : 2,
											category_id   : plusOneGroupID,
											category_type : 1,
											charges       : 0,
											date_created  : newdateForm,
											status        : 1
										}

										methods.getLastGroupId(queGroupMaxS, function(queGrpResS){

											lastGrpIdS = queGrpResS[0].lastGrpId;
											var plusOneGroupIDS = lastGrpIdS+1;

											var querInsertS = `insert into jaze_jazepay_statements (group_id, meta_key, meta_value) VALUES (?,?,?)`;
											for (var keyS in arrInsertS) 
										 	{
												DB.query(querInsertS,[plusOneGroupIDS,keyS,arrInsertS[keyS]], function(dataS, error){});

										  	};

										});


										methods.procJazePayAccountForNationalTransfer(arrParams,function(retStatus){
											return_result(retStatus);
										});

										var queGroupMaxIn = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_national_transaction_list";
										var lastGrpIdIn = 0;

										var arrInsertIn = {
											account_id   : arrParams.account_id,
											bank_id      : plusOneGroupID,
											amount       : arrParams.amount,
											remarks      : arrParams.remarks,
											date         : newdateForm,
											status       : 1
										}

										methods.getLastGroupId(queGroupMaxIn, function(queGrpResIn){

											lastGrpIdIn = queGrpResIn[0].lastGrpId;
											var plusOneGroupIDIn = lastGrpId+1;

											var querInsertIn = `insert into jaze_jazepay_national_transaction_list (group_id, meta_key, meta_value) VALUES (?,?,?)`;
											for (var keyIn in arrInsertIn) 
										 	{
												DB.query(querInsertIn,[plusOneGroupIDIn,keyIn,arrInsertIn[keyIn]], function(dataIn, error){});

										  	};

										});
										
									}
								});

						  	};

						});

					}
					else
					{
						return_result(2); 
					}

				});

			});


		}
		else
		{
			return_result(0); 
		}

	}
	catch(err)
	{ 
		console.log("procInstaTransfer",err); return_result(0); 
	}

};


methods.procJazePayAccountForNationalTransfer =  function (arrParams,return_result){
	
	try
	{				
		var compute_charges = parseFloat(parseFloat(arrParams.amount) * parseFloat(0.05)).toFixed(2);

		methods.getJazePayBalance(arrParams.account_id,function(retSenBal){

			var new_sender_balance = parseFloat( parseFloat(retSenBal)-parseFloat(arrParams.amount) ).toFixed(2);

			var updateQuer = `update jaze_jazepay_accounts SET meta_value =? WHERE meta_key =? AND group_id =?`;

			var query = `SELECT *  FROM jaze_jazepay_accounts WHERE 
					group_id in (SELECT group_id  FROM jaze_jazepay_accounts WHERE  meta_key = 'account_id' and meta_value =?)`;
	
			DB.query(query, [arrParams.account_id], function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					
					var	arrSender = {
						current_balance  : new_sender_balance,
						date_updated     : newdateForm
					};

					var arrLengthS = Object.keys(arrSender).length;
					var num_rows_sender = 0;
					for (var keyS in arrSender) 
				 	{	
						DB.query(updateQuer, [arrSender[keyS],keyS,data[0].group_id], function(dataS, error){
				 		num_rows_sender += dataS.affectedRows;
							if(parseInt(num_rows_sender) === arrLengthS){
								
								return_result(1); 
								
							}

						});
					
					}

				}
	

			});

		});
	
	}
	catch(err)
	{ 
		console.log("procJazePayAccountForNationalTransfer",err); return_result(0); 
	}

};



methods.procInternationalTransfer =  function (arrParams,return_result){
	
	try
	{			

		if(parseInt(arrParams.account_id) !== 0 )
		{	

			methods.getJazePayBalance(arrParams.account_id,function(retBalance){

				if(parseInt(retBalance) > parseInt(arrParams.amount))
				{
					
					methods.favoritesCheck(arrParams,2,function(ret_is_fav){

						var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_international_account_list";
						var lastGrpId = 0;

						var arrInsert = {
							account_id     : arrParams.account_id,
							account_name   : arrParams.account_name,
							account_number : arrParams.account_number,
							bank_name      : arrParams.bank_name,
							bank_address   : arrParams.bank_address,
							branch_name    : arrParams.branch_name,
							iban_number    : arrParams.iban_number,
							fav_status     : ret_is_fav,
							status         : 1
						}

						var arrLength = Object.keys(arrInsert).length;
						var num_rows_insert = 0;

						methods.getLastGroupId(queGroupMax, function(queGrpRes){

							lastGrpId = queGrpRes[0].lastGrpId;
							var plusOneGroupID = lastGrpId+1;

							var querInsert = `insert into jaze_jazepay_international_account_list (group_id, meta_key, meta_value) VALUES (?,?,?)`;
							for (var key in arrInsert) 
						 	{
								DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
									num_rows_insert += data.affectedRows;
									if(parseInt(num_rows_insert) === arrLength){


										var queGroupMaxS= "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_statements";
										var lastGrpIdS = 0;

										var arrInsertS = {
											account_id    : arrParams.account_id,
											amount		  : arrParams.amount,
											type          : 2,
											category_id   : plusOneGroupID,
											category_type : 1,
											charges       : 0,
											date_created  : newdateForm,
											status        : 1
										}

										methods.getLastGroupId(queGroupMaxS, function(queGrpResS){

											lastGrpIdS = queGrpResS[0].lastGrpId;
											var plusOneGroupIDS = lastGrpIdS+1;

											var querInsertS = `insert into jaze_jazepay_statements (group_id, meta_key, meta_value) VALUES (?,?,?)`;
											for (var keyS in arrInsertS) 
										 	{
												DB.query(querInsertS,[plusOneGroupIDS,keyS,arrInsertS[keyS]], function(dataS, error){});

										  	};

										});


										methods.procJazePayAccountForNationalTransfer(arrParams,function(retStatus){
											return_result(retStatus);
										});

										var queGroupMaxIn = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_international_transaction_list";
										var lastGrpIdIn = 0;

										var arrInsertIn = {
											account_id   : arrParams.account_id,
											bank_id      : plusOneGroupID,
											amount       : arrParams.amount,
											remarks      : arrParams.remarks,
											date         : newdateForm,
											status       : 1
										}

										methods.getLastGroupId(queGroupMaxIn, function(queGrpResIn){

											lastGrpIdIn = queGrpResIn[0].lastGrpId;
											var plusOneGroupIDIn = lastGrpId+1;

											var querInsertIn = `insert into jaze_jazepay_international_transaction_list (group_id, meta_key, meta_value) VALUES (?,?,?)`;
											for (var keyIn in arrInsertIn) 
										 	{
												DB.query(querInsertIn,[plusOneGroupIDIn,keyIn,arrInsertIn[keyIn]], function(dataIn, error){});

										  	};

										});
										
									}
								});

						  	};

						});

					});
				}
				else
				{
					return_result(2); 
				}

			});


		}
		else
		{
			return_result(0); 
		}

	}
	catch(err)
	{ 
		console.log("procInstaTransfer",err); return_result(0); 
	}

};



//------------------------------------------------------------------ national and international transfers end  -------------------------------------------------------------------------//



//------------------------------------------------------------------ jazepay transfers favorties list start  -------------------------------------------------------------------------//



methods.getTransfersFavorites =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var current_m = now.getMonth()+1;
		var current_y = now.getFullYear();



		var query = " SELECT * FROM jaze_jazepay_statements WHERE group_id IN (SELECT  group_id FROM jaze_jazepay_statements WHERE meta_key ='account_id' AND meta_value =?) ";
			query += " AND group_id IN (SELECT  group_id FROM jaze_jazepay_statements WHERE meta_key ='type' AND meta_value ='2') ";


		if((arrParams.month !="" && arrParams.year !="") && (arrParams.from_date =="" && arrParams.to_date =="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+arrParams.month+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+arrParams.year+"') ";
		}
		else if((arrParams.month =="" && arrParams.year =="") && (arrParams.from_date !="" && arrParams.to_date !="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND DATE(meta_value) >='"+arrParams.from_date+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND DATE(meta_value) <='"+arrParams.to_date+"') ";
		}
		else 
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+current_m+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+current_y+"') ";
		}

		if(parseInt(arrParams.flag) === 1)
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'category_type' AND meta_value='1') ";
		}
		else if(parseInt(arrParams.flag) === 2)
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'category_type' AND meta_value='2') ";
		}
		else if(parseInt(arrParams.flag) === 3)
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'category_type' AND meta_value='3') ";
		}


		DB.query(query,[arrParams.account_id], function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				

				methods.convertMultiMetaArray(data, function(retConverted){	

		
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
							

							methods.getTransfersFavoritesDetails(arrParams,jsonItems,function(retData){

								if(retData!=null){
									arrValues.push(retData);
								}
							
								counter -= 1;
								if (counter === 0){ 
									resolve(arrValues);
								}	

							});

						})
					}],
					function (result, current) {
						if (counter === 0){ 

						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getTransfersFavorites",err); return_result(null); }	
};



methods.getTransfersFavoritesDetails =  function (arrParams,arrResult,return_result){
	
	try
	{				
	
		var arrValues = [];
		var objvalues  = {};

		var query;

		if(parseInt(arrParams.flag) === 1)
		{
					
			query = `SELECT * FROM jaze_jazepay_system_partner_account_list WHERE group_id=?  
			AND group_id IN (SELECT group_id FROM jaze_jazepay_system_partner_account_list WHERE meta_key ='fav_status' AND meta_value ='1')`;
			DB.query(query,[arrResult.category_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

	
						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
				
								methods.getTransferFavortiesyAccountDetails(arrParams,jsonItems.partner_id, function(retAccount){	

									if(retAccount!=null)
									{

										var queryIn = `SELECT * FROM jaze_jazepay_system_partner_transaction_list WHERE group_id=?`;
										DB.query(queryIn,[arrResult.category_id], function(dataIn, error){

											if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
											{
												methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

													var plus = parseInt(arrResult.category_id)+100;
													var tran_no = 'SP'+pad_with_zeroes(plus,5).toString();

													objvalues = {
														id           : arrResult.group_id.toString(),
														tran_no      : tran_no,
														partner_id   : jsonItems.partner_id.toString(),
														name         : retAccount.name,
														logo         : retAccount.logo,
														amount       : arrResult.amount,
														account_name : jsonItems.account_name,
														account_no   : jsonItems.account_number,
														remarks      : retConvertedIn[0].remarks,
														date         : arrResult.date_created,
													};


													counter -= 1;
													if (counter === 0){ 
														resolve(objvalues);
													}

									
												});
											}

										});
							
									}

								});
							})
						}],
						function (result, current) {
							if (counter === 0){ 

				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null);
				}

			});


		}
		else if(parseInt(arrParams.flag) === 2)
		{
			query = `SELECT * FROM jaze_jazepay_national_account_list WHERE group_id=? 
			 AND group_id IN (SELECT group_id FROM jaze_jazepay_national_account_list WHERE meta_key ='fav_status' AND meta_value ='1')`

			DB.query(query,[arrResult.category_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

	
						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
					

								var queryIn = `SELECT * FROM jaze_jazepay_national_transaction_list WHERE group_id=?`

								DB.query(queryIn,[arrResult.category_id], function(dataIn, error){

									if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
									{
										methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

											var plus = parseInt(jsonItems.group_id)+100;
											var tran_no = 'NT'+pad_with_zeroes(plus,5).toString();

											objvalues = {
												id             : jsonItems.group_id.toString(),
												tran_no        : tran_no,
												bank_name      : jsonItems.bank_name,
												branch_name    : jsonItems.branch_name,
												account_name   : jsonItems.account_name,
												account_number : jsonItems.account_number,
												iban_number    : jsonItems.iban_number,
												amount         : arrResult.amount,
												remarks        : retConvertedIn[0].remarks,
												date           : arrResult.date_created,
												result_flag    : '2'
											};


								
											counter -= 1;
											if (counter === 0){ 
												resolve(objvalues);
											}

										});	
									}

								});


							})
						}],
						function (result, current) {
							if (counter === 0){ 

				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null);
				}

			});


		}
		else if(parseInt(arrParams.flag) === 3)
		{
			query = `SELECT * FROM jaze_jazepay_international_account_list WHERE group_id=? 
			AND group_id IN (SELECT group_id FROM jaze_jazepay_international_account_list WHERE meta_key ='fav_status' AND meta_value ='1')`;
			DB.query(query,[arrResult.category_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
				

								var queryIn = `SELECT * FROM jaze_jazepay_international_transaction_list WHERE group_id=?`

								DB.query(queryIn,[arrResult.category_id], function(dataIn, error){

									if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
									{
										methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

											var plus = parseInt(jsonItems.group_id)+100;
											var tran_no = 'IT'+pad_with_zeroes(plus,5).toString();

											objvalues = {
												id             : jsonItems.group_id.toString(),
												tran_no        : tran_no,
												bank_name      : jsonItems.bank_name,
												branch_name    : jsonItems.branch_name,
												account_name   : jsonItems.account_name,
												account_number : jsonItems.account_number,
												iban_number    : jsonItems.iban_number,
												amount         : arrResult.amount,
												swift_code     : jsonItems.swift_code,
												remarks        : retConvertedIn[0].remarks,
												date           : arrResult.date_created,
												result_flag    : '3'
											};

								
											counter -= 1;
											if (counter === 0){ 
												resolve(objvalues);
											}	
										});
									}	
								});
							})
						}],
						function (result, current) {
							if (counter === 0){ 

				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null);
				}

			});


		}
			



	}
	catch(err)
	{ console.log("getTransfersFavoritesDetails",err); return_result(null); }	
};


methods.getTransferFavortiesyAccountDetails =  function (arrParams,partner_id,return_result){
	
	try
	{

		var objvalues  = {};

		var queryIn = `SELECT * FROM jaze_master_external_system_partner WHERE group_id=?`;

		DB.query(queryIn,[partner_id], function(dataIn, error){

			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{
				
				methods.convertMultiMetaArray(dataIn, function(retConverted){	

			
					objvalues = {
						partner_id   : retConverted[0].group_id.toString(),
						name         : retConverted[0].company_name,
						logo         : STATIC_IMG_EXTERNAL+retConverted[0].logo_path,
					
					}

					return_result(objvalues);
				
				});
			}
			else{	return_result(null);}

		});
	
	}
	catch(err)
	{ 
		console.log("getTransferFavortiesyAccountDetails",err); return_result(null); 
	}

};


// methods.getTransfersFavorites =  function (arrParams,return_result){
	
// 	try
// 	{				

// 		var arrValues = [];
// 		var objvalues  = {};



// 		var query;

// 		if(parseInt(arrParams.flag) === 1)
// 		{
// 			query = `SELECT * FROM jaze_jazepay_system_partner_account_list WHERE group_id IN (SELECT  group_id FROM jaze_jazepay_system_partner_account_list WHERE meta_key ='account_id' AND meta_value =?)
// 			AND group_id IN (SELECT  group_id FROM jaze_jazepay_system_partner_account_list WHERE meta_key ='fav_status' AND meta_value ='1')`;
// 		}
// 		else if(parseInt(arrParams.flag) === 2)
// 		{
// 			query = `SELECT * FROM jaze_jazepay_national_account_list WHERE group_id IN (SELECT  group_id FROM jaze_jazepay_national_account_list WHERE meta_key ='account_id' AND meta_value =?)
// 			AND group_id IN (SELECT  group_id FROM jaze_jazepay_national_account_list WHERE meta_key ='fav_status' AND meta_value ='1')`;
// 		}
// 		else if(parseInt(arrParams.flag) === 3)
// 		{
// 			query = `SELECT * FROM jaze_jazepay_international_account_list WHERE group_id IN (SELECT  group_id FROM jaze_jazepay_international_account_list WHERE meta_key ='account_id' AND meta_value =?)
// 			AND group_id IN (SELECT  group_id FROM jaze_jazepay_international_account_list WHERE meta_key ='fav_status' AND meta_value ='1')`;
// 		}
			

// 		DB.query(query,[arrParams.account_id], function(data, error){


// 			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
// 			{
				
// 				methods.convertMultiMetaArray(data, function(retConverted){	
	
// 					var counter = retConverted.length;		
// 					var jsonItems = Object.values(retConverted);

// 					promiseForeach.each(jsonItems,[function (jsonItems) {
// 						return new Promise(function (resolve, reject) {		
							
// 							methods.getFavortiesAccountDetails(arrParams,jsonItems, function(retAccount){	

// 								if(retAccount!=null)
// 								{
// 									arrValues.push(retAccount);	
// 								}

// 								counter -= 1;
// 								if (counter === 0){ 
// 									resolve(arrValues);
// 								}	

// 							});
// 						})
// 					}],
// 					function (result, current) {
// 						if (counter === 0){ 
// 			  				return_result(result[0]);					
// 						}
// 					});

// 				});

// 			}
// 			else
// 			{
// 				return_result(null);
// 			}

// 		});

// 	}
// 	catch(err)
// 	{ console.log("getTransfersFavorites",err); return_result(null); }	
// };



// methods.getFavortiesAccountDetails =  function (arrParams,jsonItems,return_result){
	
// 	try
// 	{

// 		var query;
// 		var objvalues  = {};

// 		if(parseInt(arrParams.flag) === 1)
// 		{
			

// 			var queryIn = `SELECT * FROM jaze_master_external_system_partner WHERE group_id=?`;

// 			DB.query(queryIn,[jsonItems.partner_id], function(dataIn, error){

// 				if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
// 				{
					
// 					methods.convertMultiMetaArray(dataIn, function(retConverted){	

// 						// var accParams = { account_id:}
// 						// HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	
						
// 							objvalues = {
// 								id           : jsonItems.group_id.toString(),
// 								name         : retConverted[0].company_name,
// 								logo         : STATIC_IMG_EXTERNAL+retConverted[0].logo_path,
// 								partner_id   : retConverted[0].group_id.toString(),
// 								account_name : jsonItems.account_name,
// 								account_no   : jsonItems.account_number,
// 								result_flag  : '1'
// 							}

// 							return_result(objvalues);
// 						// });
// 					});
// 				}
// 				else{	return_result(null);}

// 			});


// 		}
// 		else if(parseInt(arrParams.flag) === 2 )
// 		{


// 			objvalues = {

// 				id             : jsonItems.group_id.toString(),
// 				bank_name      : jsonItems.bank_name,
// 				branch_name    : jsonItems.branch_name,
// 				account_name   : jsonItems.account_name,
// 				account_number : jsonItems.account_number,
// 				result_flag    : '2'
// 			};

// 			return_result(objvalues);


// 		}
// 		else if(parseInt(arrParams.flag) === 3 )
// 		{


// 			objvalues = {

// 				id             : jsonItems.group_id.toString(),
// 				bank_name      : jsonItems.bank_name,
// 				branch_name    : jsonItems.branch_name,
// 				account_name   : jsonItems.account_name,
// 				account_number : jsonItems.account_number,
// 				iban_number    : jsonItems.iban_number,
// 				result_flag    : '3'
// 			};



// 			return_result(objvalues);


// 		}
// 		else{	return_result(null);}
	
// 	}
// 	catch(err)
// 	{ 
// 		console.log("getFavortiesAccountDetails",err); return_result(null); 
// 	}

// };


//------------------------------------------------------------------ jazepay transfers favorties list end  ---------------------------------------------------------------------------//


//------------------------------------------------------------------ jazepay transfers history list start  -------------------------------------------------------------------------//




methods.getTransferHistoryStatements =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var current_m = now.getMonth()+1;
		var current_y = now.getFullYear();



		var query = " SELECT * FROM jaze_jazepay_statements WHERE group_id IN (SELECT  group_id FROM jaze_jazepay_statements WHERE meta_key ='account_id' AND meta_value =?) ";
			query += " AND group_id IN (SELECT  group_id FROM jaze_jazepay_statements WHERE meta_key ='type' AND meta_value ='2') ";


		if((arrParams.month !="" && arrParams.year !="") && (arrParams.from_date =="" && arrParams.to_date =="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+arrParams.month+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+arrParams.year+"') ";
		}
		else if((arrParams.month =="" && arrParams.year =="") && (arrParams.from_date !="" && arrParams.to_date !="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND DATE(meta_value) >='"+arrParams.from_date+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND DATE(meta_value) <='"+arrParams.to_date+"') ";
		}
		else 
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+current_m+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+current_y+"') ";
		}

		if(parseInt(arrParams.flag) === 1)
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'category_type' AND meta_value='1') ";
		}
		else if(parseInt(arrParams.flag) === 2)
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'category_type' AND meta_value='2') ";
		}
		else if(parseInt(arrParams.flag) === 3)
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'category_type' AND meta_value='3') ";
		}


		DB.query(query,[arrParams.account_id], function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				

				methods.convertMultiMetaArray(data, function(retConverted){	

		
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
							

							methods.getTransfersHistoryList(arrParams,jsonItems,function(retData){

								if(retData!=null){
									arrValues.push(retData);
								}
							
								counter -= 1;
								if (counter === 0){ 
									resolve(arrValues);
								}	

							});

						})
					}],
					function (result, current) {
						if (counter === 0){ 

						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getTransferHistoryStatements",err); return_result(null); }	
};



methods.getTransfersHistoryList =  function (arrParams,arrResult,return_result){
	
	try
	{				
	
		var arrValues = [];
		var objvalues  = {};

		var query;

		if(parseInt(arrParams.flag) === 1)
		{
					
			query = "SELECT * FROM jaze_jazepay_system_partner_account_list WHERE group_id=?";
			DB.query(query,[arrResult.category_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

	
						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
				
								methods.getTransferHistoryAccountDetails(arrParams,jsonItems.partner_id, function(retAccount){	

									if(retAccount!=null)
									{

										var queryIn = `SELECT * FROM jaze_jazepay_system_partner_transaction_list WHERE group_id=?`;
										DB.query(queryIn,[arrResult.category_id], function(dataIn, error){

											if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
											{
												methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

													var plus = parseInt(arrResult.category_id)+100;
													var tran_no = 'SP'+pad_with_zeroes(plus,5).toString();

													objvalues = {
														id           : arrResult.group_id.toString(),
														tran_no      : tran_no,
														partner_id   : jsonItems.partner_id.toString(),
														name         : retAccount.name,
														logo         : retAccount.logo,
														amount       : arrResult.amount,
														account_name : jsonItems.account_name,
														account_no   : jsonItems.account_number,
														remarks      : retConvertedIn[0].remarks,
														date         : arrResult.date_created,
													};


													counter -= 1;
													if (counter === 0){ 
														resolve(objvalues);
													}

									
												});
											}

										});
							
									}

								});
							})
						}],
						function (result, current) {
							if (counter === 0){ 

				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null);
				}

			});


		}
		else if(parseInt(arrParams.flag) === 2)
		{
			query = `SELECT * FROM jaze_jazepay_national_account_list WHERE group_id=?`

			DB.query(query,[arrResult.category_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

	
						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
					

								var queryIn = `SELECT * FROM jaze_jazepay_national_transaction_list WHERE group_id=?`

								DB.query(queryIn,[arrResult.category_id], function(dataIn, error){

									if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
									{
										methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

											var plus = parseInt(jsonItems.group_id)+100;
											var tran_no = 'NT'+pad_with_zeroes(plus,5).toString();

											objvalues = {
												id             : jsonItems.group_id.toString(),
												tran_no        : tran_no,
												bank_name      : jsonItems.bank_name,
												branch_name    : jsonItems.branch_name,
												account_name   : jsonItems.account_name,
												account_number : jsonItems.account_number,
												iban_number    : jsonItems.iban_number,
												amount         : arrResult.amount,
												remarks        : retConvertedIn[0].remarks,
												date           : arrResult.date_created,
												result_flag    : '2'
											};


											console.log(objvalues);

								
											counter -= 1;
											if (counter === 0){ 
												resolve(objvalues);
											}

										});	
									}

								});


							})
						}],
						function (result, current) {
							if (counter === 0){ 

				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null);
				}

			});


		}
		else if(parseInt(arrParams.flag) === 3)
		{
			query = `SELECT * FROM jaze_jazepay_international_account_list WHERE group_id=?`;
			DB.query(query,[arrResult.category_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						console.log(retConverted);
	
						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
				

								var queryIn = `SELECT * FROM jaze_jazepay_international_transaction_list WHERE group_id=?`

								DB.query(queryIn,[arrResult.category_id], function(dataIn, error){

									if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
									{
										methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

											var plus = parseInt(jsonItems.group_id)+100;
											var tran_no = 'IT'+pad_with_zeroes(plus,5).toString();

											objvalues = {
												id             : jsonItems.group_id.toString(),
												tran_no        : tran_no,
												bank_name      : jsonItems.bank_name,
												branch_name    : jsonItems.branch_name,
												account_name   : jsonItems.account_name,
												account_number : jsonItems.account_number,
												iban_number    : jsonItems.iban_number,
												amount         : arrResult.amount,
												remarks        : retConvertedIn[0].remarks,
												date           : arrResult.date_created,
												result_flag    : '3'
											};

								
											counter -= 1;
											if (counter === 0){ 
												resolve(objvalues);
											}	
										});
									}	
								});
							})
						}],
						function (result, current) {
							if (counter === 0){ 

				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null);
				}

			});


		}
			



	}
	catch(err)
	{ console.log("getTransfersHistoryList",err); return_result(null); }	
};


methods.getTransferHistoryAccountDetails =  function (arrParams,partner_id,return_result){
	
	try
	{

		var objvalues  = {};

		var queryIn = `SELECT * FROM jaze_master_external_system_partner WHERE group_id=?`;

		DB.query(queryIn,[partner_id], function(dataIn, error){

			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{
				
				methods.convertMultiMetaArray(dataIn, function(retConverted){	

			
					objvalues = {
						partner_id   : retConverted[0].group_id.toString(),
						name         : retConverted[0].company_name,
						logo         : STATIC_IMG_EXTERNAL+retConverted[0].logo_path,
					
					}

					return_result(objvalues);
				
				});
			}
			else{	return_result(null);}

		});
	
	}
	catch(err)
	{ 
		console.log("getTransferHistoryAccountDetails",err); return_result(null); 
	}

};


//------------------------------------------------------------------ jazepay transfers history list end  ---------------------------------------------------------------------------//


//------------------------------------------------------------------ jazepay transfers remove favorties start ------------------------------------------------------------------------//


methods.removeTransfersFavorties =  function (arrParams,return_result){
	
	try
	{				

		var query = "";
		var updateQue = "";

		var arrGroupID = arrParams.favorite_id.split(',');

		if(parseInt(arrParams.flag) === 1)
		{	
			query = "SELECT * FROM jaze_jazepay_system_partner_account_list WHERE group_id IN("+arrParams.favorite_id+") ";
			updateQue = "update jaze_jazepay_system_partner_account_list SET meta_value =? WHERE meta_key =? AND group_id =?";
		}
		else if(parseInt(arrParams.flag) === 2)
		{
			query = "SELECT * FROM jaze_jazepay_national_account_list WHERE group_id IN("+arrParams.favorite_id+") ";
			updateQue = "update jaze_jazepay_national_account_list SET meta_value =? WHERE meta_key =? AND group_id =?";
		}
		else if(parseInt(arrParams.flag) === 3)
		{
			query = "SELECT * FROM jaze_jazepay_international_account_list WHERE group_id IN("+arrParams.favorite_id+") ";
			updateQue = "update jaze_jazepay_international_account_list SET meta_value =? WHERE meta_key =? AND group_id =?";
		}

		
		DB.query(query,null, function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
					
				arrGroupID.forEach(function(row) {

					DB.query(updateQue, [0,'fav_status',row], function(data, error){

 				 		if(data !== null){
							return_result(1);
				 	   	}else{
			 	   			return_result(0);
				 	   	}
					});
		
			  	});

			}
			else
			{
				return_result(2);
			}

		});

	}
	catch(err)
	{ console.log("removeTransfersFavorties",err); return_result(0); }	
};

//------------------------------------------------------------------ jazepay transfers remove favorties end --------------------------------------------------------------------------//

//------------------------------------------------------------------ jazepay payment history start ----------------------------------------------------------------------------------//


methods.getPaymentHistory =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var current_m = now.getMonth()+1;
		var current_y = now.getFullYear();

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_statements WHERE group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'account_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'type' AND meta_value IN ('2','3','4')) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'category_type' AND meta_value IN ('1','2') ) ";
	
		if((arrParams.month !="" && arrParams.year !="") && (arrParams.from_date =="" && arrParams.to_date =="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+arrParams.month+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+arrParams.year+"') ";
		}
		else if((arrParams.month =="" && arrParams.year =="") && (arrParams.from_date !="" && arrParams.to_date !="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+arrParams.from_date+"' AND '"+arrParams.to_date+"')  ";
		}
		else 
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+current_m+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+current_y+"') ";
		}



		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.checkTransactionType(jsonItems, function(retDetails){	

		
								if(retDetails!=null && retDetails.trans_type!=null)
								{
// type = 2
// category_id = payment_id
// category_type = 1- partner, 2 - national, 3 - international
// #jazenet transfer
// type = 3
// category_id = payment_id
// category_type = 1
// #order invoice / payment
// type = 4
// category_id = payment_id
// category_type = 1 - payment / 2 received
// #order cart


									objvalues = {
										ref_no       : retDetails.group_id,
										trans_type   : jsonItems.type.toString(),
										id           : jsonItems.group_id.toString(),
										account_id   : retDetails.account_id,
										account_type : retDetails.account_type,
										name         : retDetails.name,
										logo         : retDetails.logo,
										payment_type : retDetails.trans_type.toString(),
							
										amount       : parseFloat(retDetails.amount).toFixed(2),
										date         : jsonItems.date_created
									};

									total_amount += parseFloat(retDetails.amount);							
									arrValues.push(objvalues);

								}

								counter -= 1;	
								if (counter === 0){ 
									resolve(arrValues);
								}			
							
							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0],total_amount);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getPaymentHistory",err); return_result(null); }	
};


methods.checkTransactionType =  function (tranParam,return_result){
	
	try
	{			

		// console.log(tranParam.type+'  '+ tranParam.category_type);

		var objvalues = {
			group_id      : '',
			account_id    : '',
			account_type  : '',
			name          : '',
			logo          : '',
			trans_type    : '',
			amount        : 0
		};
		var accParams = {account_id: '',group_id:''};

		

		var ref = '';
		var table = '';
		if(parseInt(tranParam.type) === 2 && parseInt(tranParam.category_type) === 1 )
		{


			ref = 'SPP';
			var query = "SELECT * FROM jaze_jazepay_system_partner_transaction_list WHERE group_id =?";

			DB.query(query, [tranParam.category_id], function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					
					methods.convertMultiMetaArray(data, function(retConverted){	



						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
										

								var tran_no = parseInt(tranParam.group_id)+100;
							
								accParams.account_id = jsonItems.partner_id;
								accParams.group_id   = jsonItems.category_id;

								methods.getSystemPartnerDetails(accParams, function(retAccountDetails){	

							

									objvalues.group_id   = ref+pad_with_zeroes(tran_no,5).toString();

									objvalues.account_id = retAccountDetails.account_id;
									objvalues.name       = retAccountDetails.name;
									objvalues.logo       = retAccountDetails.logo;
									objvalues.amount     = jsonItems.amount;
						
					

									counter -= 1;
									resolve(objvalues);

								});
					
							})
						}],
						function (result, current) {
							if (counter === 0){ 								
				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
				
					return_result(null); 
				}
			});


		}
		else if(parseInt(tranParam.type) === 3)
		{
		
			ref = 'JT';

			var query = "SELECT * FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE group_id =?";

			DB.query(query, [tranParam.category_id], function (data, error) {

	

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
										
					
								var tran_no = parseInt(tranParam.category_id)+100;

								accParams.account_id = jsonItems.profile_id;
								accParams.group_id   = jsonItems.category_id;


								objvalues.group_id   = ref+pad_with_zeroes(tran_no,5).toString();
								objvalues.amount     = jsonItems.amount;
								objvalues.trans_type = 2;


								HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

									if(retAccountDetails!=null)
									{
								
										objvalues.account_id   = retAccountDetails.account_id;
										objvalues.account_type = retAccountDetails.account_type;
										objvalues.name       = retAccountDetails.name;
										objvalues.logo       = retAccountDetails.logo;
									}

									counter -= 1;
									resolve(objvalues);

								});
					
							
							})
						}],
						function (result, current) {
							if (counter === 0){ 								
				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
									
					return_result(null); 
				}
			});

		}
		else if(parseInt(tranParam.type) === 4)
		{

			ref = 'P';
		
			var query = "SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id =?";

			DB.query(query, [tranParam.category_id], function (data, error) {
			
				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
										

								var tran_no = parseInt(tranParam.group_id)+100;

								accParams.account_id = jsonItems.profile_id;
								accParams.group_id   = jsonItems.category_id;


								objvalues.group_id   = ref+pad_with_zeroes(jsonItems.payment_transaction_id,5).toString();
						 		objvalues.trans_type = jsonItems.payment_type;
						 		objvalues.amount     = jsonItems.total;


								HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

					

									if(retAccountDetails!=null)
									{
								
										objvalues.account_id   = retAccountDetails.account_id;
										objvalues.account_type = retAccountDetails.account_type;
										objvalues.name       = retAccountDetails.name;
										objvalues.logo       = retAccountDetails.logo;
									}


									counter -= 1;
									resolve(objvalues);

								});
					
							
							})
						}],
						function (result, current) {
							if (counter === 0){ 
								//console.log(result[0]);								
				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					
					return_result(null); 
				}
			});



		}
		else
		{
			return_result(null)
		}

	
	
	}
	catch(err)
	{ 
		console.log("checkTransactionType",err); return_result(null); 
	}

};


methods.getSystemPartnerDetails =  function (tranParam,return_result){
	
	try
	{			

		var obj_return = {
			account_id : '',
			name       : '',
			logo       : '',
		}

		var query = "SELECT * FROM jaze_master_external_system_partner WHERE group_id=?";

		DB.query(query, [tranParam.account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var obj_return = {
						account_id : retConverted[0].account_id,
						name       : retConverted[0].company_name,
						logo       : STATIC_IMG_EXTERNAL+retConverted[0].logo_path,

					}

					return_result(obj_return); 

				});

			}
			else
			{
				return_result(obj_return); 
			}
		});
	
	}
	catch(err)
	{ 
		console.log("getSystemPartnerDetails",err); return_result(obj_return); 
	}

};

//------------------------------------------------------------------ jazepay payment history end --------------------------------------------------------------------------------//


//------------------------------------------------------------------ jazepay search payment history start ----------------------------------------------------------------------------------//


methods.searchPaymentHistory =  function (arrParams,return_result){
	


	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var total_amount = 0;

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var query = "SELECT * FROM jaze_jazepay_statements WHERE group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'account_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'type' AND meta_value IN ('2','3','4')) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'category_type' AND meta_value IN ('1','2')) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";


		DB.query(query,[arrParams.account_id], function(data, error){


			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.checkTransactionTypeForSearchPayment(jsonItems, function(retDetails){	
			
								if(retDetails!=null)
								{
	
									objvalues = {
										id             : jsonItems.group_id.toString(),
										account_id     : retDetails.account_id,
										ref_no         : retDetails.group_id,
										ref_id         : retDetails.ref_id,
										trans_type     : retDetails.trans_type,
										trans_type_ref : retDetails.trans_type_ref,										
										account_type   : retDetails.account_type,
										name           : retDetails.name,
										logo           : retDetails.logo,
										payment_type   : retDetails.payment_type,
										amount         : parseFloat(retDetails.amount).toFixed(2),
										date           : jsonItems.date_created
									};

									total_amount += parseFloat(retDetails.amount);							
									arrValues.push(objvalues);

								}

								counter -= 1;	
								if (counter === 0){ 
									resolve(arrValues);
								}			
							
							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 
					

						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});
		                	
			  				return_result(result[0],total_amount);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("searchPaymentHistory",err); return_result(null); }	
};


methods.checkTransactionTypeForSearchPayment =  function (tranParam,return_result){
	
	try
	{			

		var objvalues = {
			ref_id        : '',
			group_id      : '',
			account_id    : '',
			account_type  : '',
			name          : '',
			logo          : '',
			trans_type    : '',
			trans_type_ref    : '',
			payment_type  : '',
			amount        : 0
		};
		var accParams = {account_id: '',group_id:''};

		

		var ref = '';
		var table = '';
		if(parseInt(tranParam.type) === 2 && parseInt(tranParam.category_type) === 1 )
		{


			ref = 'SPP';
			var query = "SELECT * FROM jaze_jazepay_system_partner_transaction_list WHERE group_id =?";

			DB.query(query, [tranParam.category_id], function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
										

								var tran_no = parseInt(tranParam.category_id)+100;
							
								accParams.account_id = jsonItems.partner_id;
								accParams.group_id   = jsonItems.category_id;

								methods.getSystemPartnerDetailsForSearch(accParams, function(retAccountDetails){					

									objvalues.group_id      = ref+pad_with_zeroes(tran_no,5).toString();
									objvalues.account_id    = retAccountDetails.account_id;
									objvalues.name          = retAccountDetails.name;
									objvalues.logo          = retAccountDetails.logo;
									objvalues.amount        = jsonItems.amount;
									objvalues.payment_type  = '1';
									objvalues.trans_type_ref  = 'system partners payment';
									objvalues.trans_type    = '2';
											

									counter -= 1;
									resolve(objvalues);

								});
					
							})
						}],
						function (result, current) {
							if (counter === 0){ 								
				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null); 
				}
			});


		}
		else if(parseInt(tranParam.type) === 3)
		{
	
			ref = 'JT';

			var query = "SELECT * FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE group_id =?";

			DB.query(query, [tranParam.category_id], function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
										

								var tran_no = parseInt(tranParam.category_id)+100;

								accParams.account_id = jsonItems.profile_id;
								accParams.group_id   = jsonItems.category_id;


								objvalues.group_id   = ref+pad_with_zeroes(tran_no,5).toString();
								objvalues.amount     = jsonItems.amount;
								objvalues.trans_type = 2;


								HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

									if(retAccountDetails!=null)
									{
								
										objvalues.account_id   = retAccountDetails.account_id;
										objvalues.account_type = retAccountDetails.account_type;
										objvalues.name         = retAccountDetails.name;
										objvalues.logo         = retAccountDetails.logo;
										objvalues.payment_type = '1';
										objvalues.trans_type_ref  = 'jazenet transfer';
										objvalues.trans_type    = '3';
									}

									counter -= 1;
									resolve(objvalues);

								});
					
							
							})
						}],
						function (result, current) {
							if (counter === 0){ 								
				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null); 
				}
			});

		}
		else if(parseInt(tranParam.type) === 4)
		{

			ref = 'P';
		
			var query = "SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id =?";

			DB.query(query, [tranParam.category_id], function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
										

								var tran_no = parseInt(tranParam.group_id)+100;

								accParams.account_id = jsonItems.profile_id;
								accParams.group_id   = jsonItems.category_id;

								objvalues.ref_id     = jsonItems.group_id.toString();
								objvalues.group_id   = ref+pad_with_zeroes(jsonItems.payment_transaction_id,5).toString();
						 		objvalues.payment_type = jsonItems.payment_type;
						 		objvalues.amount     = jsonItems.total;
				 				objvalues.trans_type_ref  = 'invoice payment';
								objvalues.trans_type      = '4';


								HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

									if(retAccountDetails!=null)
									{
								
										objvalues.account_id   = retAccountDetails.account_id;
										objvalues.account_type = retAccountDetails.account_type;
										objvalues.name       = retAccountDetails.name;
										objvalues.logo       = retAccountDetails.logo;
								
									}

									counter -= 1;
									resolve(objvalues);

								});
					
							
							})
						}],
						function (result, current) {
							if (counter === 0){ 								
				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null); 
				}
			});



		}
		else
		{
			return_result(null)
		}

	
	
	}
	catch(err)
	{ 
		console.log("checkTransactionTypeForSearchPayment",err); return_result(null); 
	}

};


methods.getSystemPartnerDetailsForSearch =  function (tranParam,return_result){
	
	try
	{			

		var obj_return = {
			account_id : '',
			name       : '',
			logo       : '',
		}

		var query = "SELECT * FROM jaze_master_external_system_partner WHERE group_id=?";

		DB.query(query, [tranParam.account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var obj_return = {
						account_id : retConverted[0].account_id,
						name       : retConverted[0].company_name,
						logo       : STATIC_IMG_EXTERNAL+retConverted[0].logo_path,

					}

					return_result(obj_return); 

				});

			}
			else
			{
				return_result(obj_return); 
			}
		});
	
	}
	catch(err)
	{ 
		console.log("getSystemPartnerDetailsForSearch",err); return_result(obj_return); 
	}

};

//------------------------------------------------------------------ jazepay search payment history end --------------------------------------------------------------------------------//
//------------------------------------------------------------------ jazepay statement history start ----------------------------------------------------------------------------------//


methods.getStatementHistroy =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

	  	var obj_balance = {
	  		debit   : 0,
	  		credit  : 0,
	  		charges : 0,
	  		balance : 0,
	  	}

		var total_amount = 0;

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var query = "SELECT * FROM jaze_jazepay_statements WHERE group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'account_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'type' AND meta_value IN ('1','2','3','4')) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'category_type' AND meta_value IN ('1','2','3')) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
		
		DB.query(query,[arrParams.account_id,arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	



					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.checkTransactionTypeForStatment(jsonItems, function(retDetails){	
						
								if( retDetails.trans_type != null){


									var credit = '0.00';
									var debit = '0.00';

									if(parseInt(jsonItems.type) === 1)
									{
										credit = parseFloat( parseFloat(jsonItems.amount)+parseFloat(jsonItems.charges) ).toFixed(2);
									}
								
									var type = '';
									if(parseInt(jsonItems.type) === 1 ){

										if(parseInt(retDetails.trans_type) === 1){
											type = 'add fund / Bank card';
										}else{
											type = 'add fund / credit card';
										}

									}else if(parseInt(jsonItems.type) === 2 ){

										if(parseInt(jsonItems.category_type) === 1 ){
											type = 'system partners payment';
										}else if(parseInt(jsonItems.category_type) === 2){
											type = 'national transfers';
										}else if(parseInt(jsonItems.category_type) === 3){
											type = 'international transfers';
										}

										debit = retDetails.amount;

									}else if(parseInt(jsonItems.type) === 3){

										type = 'jazenet transfer';	
										if(parseInt(jsonItems.category_type) === 1){
											
											debit = retDetails.amount;
										}else{
											credit = retDetails.amount;
										}

									
									}else if(parseInt(jsonItems.type) === 4){

										if(parseInt(jsonItems.category_type) === 1)
										{	
											type = 'jazepay';	
										}
										else
										{
											type = 'credit card';	
										}

										debit = retDetails.amount;
									}


									objvalues = {
										ref_no       : retDetails.group_id,
										trans_type   : type,
										group_id     : jsonItems.group_id.toString(),
										account_id   : retDetails.account_id,
										account_type : retDetails.account_type,
										name         : retDetails.name,
										logo         : retDetails.logo,
										payment_type : retDetails.trans_type.toString(),
										debit        : debit,
										credit       : credit,
										charges      : parseFloat(jsonItems.charges).toFixed(2),
										date         : jsonItems.date_created
									};
							
									counter -= 1;	
									arrValues.push(objvalues);
									if (counter === 0){ 
										resolve(arrValues);
									}	
								

								}else{ counter -= 1;	}	

											
						
							});
				
							
						})
					}],
					function (result, current) {
						if (counter === 0){ 


							
							methods.getAccountCredits(arrParams, function(objvalues){	

								if(objvalues!=null){result[0].push(objvalues[0]);}

							  	result[0].sort(function(a, b){ 
			                   	 	return new Date(b.date) - new Date(a.date); 
			                	});

		                		return_result(result[0]);					

		                	});

			  			
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getStatementHistroy",err); return_result(null); }	
};


methods.checkTransactionTypeForStatment =  function (tranParam,return_result){
	
	try
	{			

		var objvalues = {
			group_id      : '',
			account_id    : '',
			account_type  : '',
			name          : '',
			logo          : '',
			trans_type    : '',
			amount        : 0
		};
		var accParams = {account_id: '',group_id:'',category_type:''};

		var ref = '';
		var table = '';

		if(parseInt(tranParam.type) === 1)
		{


			var query = "SELECT * FROM jaze_jazepay_statements WHERE group_id=?";
			DB.query(query, [tranParam.group_id], function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					methods.convertMultiMetaArray(data, function(retConverted){	

						var accParams = {account_id: tranParam.account_id};
						HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

							if(retAccountDetails!=null)
							{
								var trans_type = 2;
								if(parseInt(tranParam.category_type) === 1){
									trans_type = 1;
								}
								

								var tran_no = parseInt(tranParam.group_id)+100;
								objvalues.group_id   = 'AD'+pad_with_zeroes(tran_no,5).toString();

								objvalues.account_id   = retAccountDetails.account_id;
								objvalues.account_type = retAccountDetails.account_type;
								objvalues.name       = 'Me';
								objvalues.logo       = retAccountDetails.logo;
								objvalues.amount     = retConverted[0].amount;
								objvalues.trans_type = trans_type;

								return_result(objvalues);



							}						
						
						});

					});

				}

			});

		}
		else
		{

			if(parseInt(tranParam.type) === 2)
			{
			
				if(parseInt(tranParam.category_type) === 1)
				{
					table = 'jaze_jazepay_system_partner_transaction_list';
					ref = 'SPP';
				}
				else if(parseInt(tranParam.category_type) === 2)
				{
					table = 'jaze_jazepay_national_transaction_list';
					ref = 'NT';
				}
				else if(parseInt(tranParam.category_type) === 3)
				{
					table = 'jaze_jazepay_international_transaction_list';
					ref = 'IT';					
				}

			}
			else if(parseInt(tranParam.type) === 3)
			{				
				table = 'jaze_jazepay_jazenet_transfer_transaction_list';
				ref = 'JT';

							console.log(tranParam.group_id);
	

			}
			else if(parseInt(tranParam.type) === 4)
			{
				ref = 'P';
				table ='jaze_jazepay_invoice_payments';
			}

			var query = "SELECT * FROM "+table+" WHERE group_id =?";

			DB.query(query, [tranParam.category_id], function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
										
								var tran_no = parseInt(tranParam.group_id)+100;

								if(parseInt(tranParam.type) === 3 || parseInt(tranParam.type) === 4)
								{
								
									accParams.group_id   = jsonItems.category_id;

									if(parseInt(tranParam.type) === 4)
									{
										accParams.account_id = jsonItems.profile_id;
										objvalues.group_id   = ref+pad_with_zeroes(jsonItems.payment_transaction_id,5).toString();
										objvalues.trans_type = jsonItems.payment_type;
										objvalues.amount     = jsonItems.total;
									}
									else
									{
										if(parseInt(tranParam.category_type) === 1){
											accParams.account_id = jsonItems.profile_id;
										}else{
											accParams.account_id = jsonItems.account_id;
										}

										tran_no = parseInt(jsonItems.group_id)+100;
										objvalues.group_id   = ref+pad_with_zeroes(tran_no,5).toString();
										objvalues.amount     = jsonItems.amount;
										objvalues.trans_type = 2;
									}
									
									HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

										if(retAccountDetails!=null)
										{									
											objvalues.account_id   = retAccountDetails.account_id;
											objvalues.account_type = retAccountDetails.account_type;
											objvalues.name       = retAccountDetails.name;
											objvalues.logo       = retAccountDetails.logo;
										}

										counter -= 1;
										resolve(objvalues);
									});

								}
								else if(parseInt(tranParam.type) === 2)
								{	


									accParams.category_type = tranParam.category_type;

									if(parseInt(tranParam.category_type) === 1){
										accParams.account_id = jsonItems.partner_id;
									}else if(parseInt(tranParam.category_type) === 2 ){
										accParams.group_id   = jsonItems.bank_id;
									}else if(parseInt(tranParam.category_type) === 3){
										accParams.group_id   = jsonItems.bank_id;										
									}
									

									methods.getCategoryType2(accParams, function(retAccountDetails){	
										
										if(parseInt(tranParam.category_type) === 1){
											tran_no = parseInt(jsonItems.group_id)+100;
											objvalues.group_id   = ref+pad_with_zeroes(tran_no,5).toString();
										}else if(parseInt(tranParam.category_type) === 2 || parseInt(tranParam.category_type) === 3){
											objvalues.group_id   = ref+pad_with_zeroes(jsonItems.bank_id,5).toString();
										}

										objvalues.account_id = retAccountDetails.account_id;
										objvalues.name       = retAccountDetails.name;
										objvalues.logo       = retAccountDetails.logo;
										objvalues.amount     = jsonItems.amount;
										objvalues.trans_type = 2;
							
										counter -= 1;
										resolve(objvalues);

									});
								}
										
							})
						}],
						function (result, current) {
							if (counter === 0){ 								
				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(objvalues); 
				}
			});

		}

	}
	catch(err)
	{ 
		console.log("checkTransactionTypeForStatment",err); return_result(objvalues); 
	}

};


methods.getCategoryType2 =  function (tranParam,return_result){
	
	try
	{			

		var obj_return = {
			account_id : '',
			name       : '',
			logo       : '',
		}

		if(parseInt(tranParam.category_type) === 1)
		{

			var query = "SELECT * FROM jaze_master_external_system_partner WHERE group_id=?";
			DB.query(query, [tranParam.account_id], function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var obj_return = {
							account_id : retConverted[0].account_id,
							name       : retConverted[0].company_name,
							logo       : STATIC_IMG_EXTERNAL+retConverted[0].logo_path,

						}

						return_result(obj_return); 

					});

				}
				else
				{
					return_result(obj_return); 
				}
			});

		}
		else if(parseInt(tranParam.category_type) === 2)
		{

			var queryIn = "SELECT * FROM jaze_jazepay_national_account_list WHERE group_id=?";
			DB.query(queryIn, [tranParam.group_id], function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 

					methods.convertMultiMetaArray(data, function(retConverted){	

						var obj_return = {
							account_id : '',
							name       : retConverted[0].bank_name,
							logo       : '',
						}

						return_result(obj_return); 

					});

				}
				else{return_result(obj_return); }

			});


		}
		else if(parseInt(tranParam.category_type) === 3)
		{
			var queryIn = "SELECT * FROM jaze_jazepay_international_account_list WHERE group_id=?";
			DB.query(queryIn, [tranParam.group_id], function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 

					methods.convertMultiMetaArray(data, function(retConverted){	

						var obj_return = {
							account_id : '',
							name       : retConverted[0].bank_name,
							logo       : '',
						}

						return_result(obj_return); 

					});

				}
				else{return_result(obj_return); }

			});
		}

	
	}
	catch(err)
	{ 
		console.log("getCategoryType2",err); return_result(obj_return); 
	}

};


methods.getAccountCredits =  function (arrParams,return_result){
	
	try
	{			

		var current_m = now.getMonth()+1;
		var current_y = now.getFullYear();

		query = " SELECT * FROM jaze_jazepay_credit_notes WHERE group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'profile_id' AND meta_value=?)  ";

		if((arrParams.month !="" && arrParams.year !="") && (arrParams.from_date =="" && arrParams.to_date =="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+arrParams.month+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+arrParams.year+"') ";
		}
		else if((arrParams.month =="" && arrParams.year =="") && (arrParams.from_date !="" && arrParams.to_date !="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+arrParams.from_date+"' AND '"+arrParams.to_date+"')  ";
		}
		else 
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+current_m+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+current_y+"') ";
		}

		var arrValues = [];
		var objvalues = {};

		DB.query(query, [arrParams.account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 

				methods.convertMultiMetaArray(data, function(retConverted){	
	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		
							
							var accParams = {account_id: jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

								if(retAccountDetails!=null)
								{
									var tran_no = parseInt(jsonItems.group_id)+100;
									var ref_no = 'CN'+pad_with_zeroes(tran_no,5).toString();

									objvalues = {
										ref_no       : ref_no,
										trans_type   : 'jazepay',
										group_id     : jsonItems.group_id.toString(),
										account_id   : retAccountDetails.account_id,
										account_type : retAccountDetails.account_type,
										name         : retAccountDetails.name,
										logo         : retAccountDetails.logo,
										payment_type : '2',
										debit        : '0',
										credit       : parseFloat(jsonItems.total).toFixed(2),
										charges      : '0',
										date         : jsonItems.date_created
									};

									counter -= 1;	
									arrValues.push(objvalues);
								}

							
								if (counter === 0){ 
									resolve(arrValues);
								}	

							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 

			

			  				return_result(result[0]);					
						}
					});

				});

			}
			else{return_result(null); }

		});
	
	}
	catch(err)
	{ 
		console.log("getAccountCredits",err); return_result(null); 
	}

};


//------------------------------------------------------------------ jazepay statement history end --------------------------------------------------------------------------------//


//------------------------------------------------------------------ jazepay payables creditors list start  --------------------------------------------------------------------------------//


methods.getPayablesCreditorsList =  function (arrParams,return_result){
	
	try
	{			

		var current_m = now.getMonth()+1;
		var current_y = now.getFullYear();

		query = `SELECT t1.* FROM(
				SELECT group_id,
					(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
					(SELECT c.meta_value FROM jaze_jazepay_quotations c WHERE c.meta_key = 'profile_id' AND c.group_id = jaze_jazepay_quotations.group_id) AS profile_id,
					(SELECT c.meta_value FROM jaze_jazepay_quotations c WHERE c.meta_key = 'date_created' AND c.group_id = jaze_jazepay_quotations.group_id) AS date_created
				FROM  jaze_jazepay_quotations
			) AS t1 WHERE account_id != '' AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?) `;

		if((arrParams.month !="" && arrParams.year !="") && (arrParams.from_date =="" && arrParams.to_date =="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+arrParams.month+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+arrParams.year+"') ";
		}
		else if((arrParams.month =="" && arrParams.year =="") && (arrParams.from_date !="" && arrParams.to_date !="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+arrParams.from_date+"' AND '"+arrParams.to_date+"')  ";
		}
		else 
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+current_m+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+current_y+"') ";
		}

		query += " GROUP BY account_id ORDER BY date_created DESC";


		var objvalues = {};
		var arrValues = [];

		var total_amount = 0;

		DB.query(query, [arrParams.account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 

	
				var counter = data.length;		
				var jsonItems = Object.values(data);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	

			
						var creParams = {profile_id: jsonItems.profile_id, account_id: jsonItems.account_id};
						methods.getCreditorsTransactions(creParams, function(date,retSummary){

							var accParams = {account_id: jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

								if(retAccountDetails!=null)
								{
									total_amount += parseFloat(retSummary);

									objvalues = {
				
										account_id   : retAccountDetails.account_id,
										account_type : retAccountDetails.account_type,
										name         : retAccountDetails.name,
										logo         : retAccountDetails.logo,
										amount       : retSummary.toFixed(2),
										date         : date

									};

									counter -= 1;	
									arrValues.push(objvalues);
					

								}else{	counter -= 1;}

								if (counter === 0){ 
									resolve(arrValues);
								}	

							});
						});
					})
				}],
				function (result, current) {
					if (counter === 0){

					  	result[0].sort(function(a, b){ 
	                   	 	return new Date(a.date) - new Date(b.date); 
	                	});

			
		  				return_result(result[0],total_amount);					
					}
				});

			}
			else{return_result(null,0); }

		});
	
	}
	catch(err)
	{ 
		console.log("getPayablesCreditorsList",err); return_result(null,0); 
	}

};



methods.getCreditorsTransactions =  function (arrParams,return_result){
	
	try
	{			

		var current_m = now.getMonth()+1;
		var current_y = now.getFullYear();

		var total_amount = 0;
		var arrTotal = [];
		var date = "";

		var query = `SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?) 
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'account_id' AND meta_value=?) `;

		DB.query(query, [arrParams.profile_id,arrParams.account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							total_amount += parseFloat(jsonItems.balance);
							date = jsonItems.date_created;
							counter -= 1;	
							if (counter === 0){ 
								resolve(date);
							}
					
						})
					}],
					function (result, current) {
						if (counter === 0){ 

			  				return_result(result[0],total_amount);					
						}
					});

				});

			}
			else{return_result(0); }

		});
	
	}
	catch(err)
	{ 
		console.log("getCreditorsTransactions",err); return_result(0); 
	}

};

//------------------------------------------------------------------ jazepay payables creditors list end    --------------------------------------------------------------------------------//


//------------------------------------------------------------------ jazepay payables creditor transaction list start    --------------------------------------------------------------------------------//


methods.getPayablesCreditorTransactions =  function (arrParams,return_result){
	
	try
	{	
		
		methods.getCreditorsInvTransactions(arrParams,function(arrInvoice){

			methods.getCreditorsPayTransactions(arrParams,function(arrPayments){

				methods.getCreditorCashPurchase(arrParams,function(arrCashPur){

					var retArr = arrPayments.concat(arrInvoice);
					var mainArr = retArr.concat(arrCashPur);
				  	mainArr.sort(function(a, b){ 
	               	 	return new Date(a.date) - new Date(b.date); 
	            	});

					var accParams = {account_id: arrParams.creditor_account_id};
					HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

						if(retAccountDetails!=null)
						{
							var ret_obj = {

								account_id   : retAccountDetails.account_id,
								account_type : retAccountDetails.account_type,
								name         : retAccountDetails.name,
								logo         : retAccountDetails.logo,
								transactions : mainArr
							}

						  	return_result(ret_obj);
						}else{return_result(null);}				
					
					});

				});
			});
		});

	}
	catch(err)
	{ 
		console.log("getPayablesCreditorTransactions",err); return_result(null); 
	}

};


methods.getCreditorsInvTransactions =  function (arrParams,return_result){
	
	try
	{			

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		query = `SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?)
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'account_id' AND meta_value=?)
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'invoice_id' AND meta_value !='0')
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'order_id' AND meta_value ='0') `;

		query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";


		var objvalues = {};
		var arrValues = [];

		DB.query(query, [arrParams.account_id,arrParams.creditor_account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							var debit = Math.round(jsonItems.total * 100) / 100;

							var ref_no = 'I'+pad_with_zeroes(jsonItems.invoice_id,5).toString();

							objvalues = {
		
								id           : jsonItems.group_id.toString(),
								ref_no       : ref_no,
								trans_type   : '1',
								debit        : debit.toFixed(2),
								credit       : '0.00',
								date         : jsonItems.date_created

							};

							counter -= 1;	
							arrValues.push(objvalues);

							if (counter === 0){ 
								resolve(arrValues);
							}	
			
						})
					}],
					function (result, current) {
						if (counter === 0){ 

			  				return_result(result[0]);					
						}
					});

				});

			}
			else{return_result([]); }

		});
	
	}
	catch(err)
	{ 
		console.log("getCreditorsInvTransactions",err); return_result([]); 
	}

};


methods.getCreditorsPayTransactions =  function (arrParams,return_result){
	
	try
	{			

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		query = `SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'profile_id' AND meta_value=?)
		AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'account_id' AND meta_value=?) `;


		query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";

		var objvalues = {};
		var arrValues = [];

		DB.query(query, [arrParams.creditor_account_id,arrParams.account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 

				methods.convertMultiMetaArray(data, function(retConverted){	


					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							var credit = Math.round(jsonItems.total * 100) / 100;

							var ref_no = 'P'+pad_with_zeroes(jsonItems.payment_transaction_id,5).toString();

							objvalues = {
		
								id           : jsonItems.group_id.toString(),
								ref_no       : ref_no,
								trans_type   : '2',
								debit        : '0.00',
								credit       : credit.toFixed(2),
								date         : jsonItems.date_created

							};

							counter -= 1;	
							arrValues.push(objvalues);

							if (counter === 0){ 
								resolve(arrValues);
							}	
			
						})
					}],
					function (result, current) {
						if (counter === 0){ 

			  				return_result(result[0]);					
						}
					});

				});

			}
			else{return_result([]); }

		});
	
	}
	catch(err)
	{ 
		console.log("getCreditorsPayTransactions",err); return_result([]); 
	}

};


methods.getCreditorCashPurchase =  function (arrParams,return_result){
	
	try
	{			

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		query = `SELECT * FROM jaze_jazepay_cash_sale WHERE group_id IN (SELECT group_id FROM jaze_jazepay_cash_sale WHERE meta_key = 'account_id' AND meta_value=?)
		AND group_id IN (SELECT group_id FROM jaze_jazepay_cash_sale WHERE meta_key = 'profile_id' AND meta_value=?)
		AND group_id IN (SELECT group_id FROM jaze_jazepay_cash_sale WHERE meta_key = 'paid_status' AND meta_value='1')`;

		query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_cash_sale WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";		


		var objvalues = {};
		var arrValues = [];

		DB.query(query, [arrParams.creditor_account_id,arrParams.account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 

				methods.convertMultiMetaArray(data, function(retConverted){	


					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							var credit = Math.round(jsonItems.total * 100) / 100;

							var ref_no = 'CS'+pad_with_zeroes(jsonItems.cashsale_id,5).toString();

							objvalues = {
		
								id           : jsonItems.group_id.toString(),
								ref_no       : ref_no,
								trans_type   : '3',
								debit        : parseFloat(jsonItems.total).toFixed(2),
								credit       : '0.00',
								date         : jsonItems.date_created

							};

							counter -= 1;	
							arrValues.push(objvalues);

							if (counter === 0){ 
								resolve(arrValues);
							}	
			
						})
					}],
					function (result, current) {
						if (counter === 0){ 

			  				return_result(result[0]);					
						}
					});

				});

			}
			else{return_result([]); }

		});
	
	}
	catch(err)
	{ 
		console.log("getCreditorCashPurchase",err); return_result([]); 
	}

};


//------------------------------------------------------------------ jazepay payables creditor transaction list end     --------------------------------------------------------------------------------//


//------------------------------------------------------------------ jazepay add funds history start    --------------------------------------------------------------------------------//


methods.getAddFundsHistory =  function (arrParams,return_result){
	
	try
	{			
	 
		var current_m = now.getMonth()+1;
		var current_y = now.getFullYear();

		query = `SELECT * FROM jaze_jazepay_statements WHERE group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'account_id' AND meta_value=?)
		AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'type' AND meta_value='1') `;


		if((arrParams.month !="" && arrParams.year !="") && (arrParams.from_date =="" && arrParams.to_date =="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+arrParams.month+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+arrParams.year+"') ";
		}
		else if((arrParams.month =="" && arrParams.year =="") && (arrParams.from_date !="" && arrParams.to_date !="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+arrParams.from_date+"' AND '"+arrParams.to_date+"')  ";
		}
		else 
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+current_m+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+current_y+"') ";
		}

		var objvalues = {};
		var arrValues = [];

		DB.query(query, [arrParams.account_id], function (data, error) {


			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							var payParams = {group_id: jsonItems.category_id, type:jsonItems.category_type };
							methods.getAccountPayments(payParams,function(retPayment){

								var receipt_id = parseFloat(jsonItems.group_id)+100;
								var receipt_no = 'AD'+pad_with_zeroes(receipt_id,5).toString();

								objvalues = {
			
									id             : jsonItems.group_id.toString(),
									receipt_no     : receipt_no,
									payment_id     : retPayment.payment_id,
									payment_type   : retPayment.payment_type,
									account_name   : retPayment.account_name,
									account_number : retPayment.account_number,
									bank_name      : retPayment.bank_name,
									expiry_date    : retPayment.expiry_date,
									card_details   : retPayment.card_details,
									charges        : parseFloat(jsonItems.charges).toFixed(2),
									amount         : parseFloat(jsonItems.amount).toFixed(2),
									date           : jsonItems.date_created

								};

								counter -= 1;	
								arrValues.push(objvalues);

								if (counter === 0){ 
									resolve(arrValues);
								}

							});		
			
						})
					}],
					function (result, current) {
						if (counter === 0){ 
											
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else{return_result(null); }

		});
	
	}
	catch(err)
	{ 
		console.log("getAddFundsHistory",err); return_result(null); 
	}

};


methods.getAccountPayments =  function (arrParams,return_result){
	
	try
	{			

		var creditCardType = require('credit-card-type');

		var query = `SELECT * FROM jaze_user_credit_card WHERE group_id=? `;

		if(parseInt(arrParams.type) === 1){
			query = `SELECT * FROM jaze_user_bank_details WHERE group_id=? `;
		}

		var ret_obj = {

			payment_id     : '',
			account_name   : '',
			account_number : '',
			bank_name      : '',
			expiry_date    : '',
			payment_type   : '',
			card_details   : ''
		};

		var obj_card = {
			name : '',
			logo : ''
		}

		DB.query(query, [arrParams.group_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							if(parseInt(arrParams.type) === 1){

								ret_obj.payment_id     = jsonItems.group_id.toString();
								ret_obj.account_name   = jsonItems.holder_name;
								ret_obj.account_number = jsonItems.account_no;
								ret_obj.bank_name      = jsonItems.bank_name;
								ret_obj.payment_type   = '1';
								ret_obj.card_details   = obj_card;

							}
							else
							{

								var remove = jsonItems.card_no.replace(/\s/g, '')
								var visaCards = creditCardType(remove)[0];

					            if (typeof visaCards !== 'undefined' && visaCards !== null && parseInt(visaCards.length) !== 0)
					            {
									obj_card.name = visaCards.niceType;
									obj_card.logo = G_STATIC_IMG_URL+"uploads/jazenet/creditcard_logos/"+visaCards.type+".png";
					            }
					    
								ret_obj.payment_id     = jsonItems.group_id.toString();
								ret_obj.account_name   = jsonItems.card_name;
								ret_obj.account_number = jsonItems.card_no;
								ret_obj.bank_name      = jsonItems.bank_name;
								ret_obj.payment_type   = '2';
								ret_obj.expiry_date    = jsonItems.exp_month+'/'+jsonItems.exp_year;
								ret_obj.card_details   = obj_card;
							}

							counter -= 1;	
							if (counter === 0){ 
								resolve(ret_obj);
							}
					
						})
					}],
					function (result, current) {
						if (counter === 0){ 

					
			  				return_result(result[0]);					
						}
					});

				});

			}
			else{return_result(null); }

		});
	
	}
	catch(err)
	{ 
		console.log("getAccountPayments",err); return_result(null); 
	}

};

//------------------------------------------------------------------  jazepay add funds history end    --------------------------------------------------------------------------------//


//------------------------------------------------------------------  jazepay payables creditor maintenance start   --------------------------------------------------------------------------------//



methods.getPayablesCreditorsMaintenance =  function (arrParams,return_result){
	
	try
	{			

		var current_m = now.getMonth()+1;
		var current_y = now.getFullYear();

		query = `SELECT t1.* FROM(
				SELECT group_id,
					(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
					(SELECT c.meta_value FROM jaze_jazepay_quotations c WHERE c.meta_key = 'profile_id' AND c.group_id = jaze_jazepay_quotations.group_id) AS profile_id,
					(SELECT c.meta_value FROM jaze_jazepay_quotations c WHERE c.meta_key = 'date_created' AND c.group_id = jaze_jazepay_quotations.group_id) AS date_created
				FROM  jaze_jazepay_quotations
			) AS t1 WHERE account_id != '' AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?) `;

		if((arrParams.month !="" && arrParams.year !="") && (arrParams.from_date =="" && arrParams.to_date =="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+arrParams.month+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+arrParams.year+"') ";
		}
		else if((arrParams.month =="" && arrParams.year =="") && (arrParams.from_date !="" && arrParams.to_date !="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+arrParams.from_date+"' AND '"+arrParams.to_date+"')  ";
		}
		else 
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+current_m+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+current_y+"') ";
		}

		query += " GROUP BY account_id ORDER BY date_created DESC";


		var objvalues = {};
		var arrValues = [];

		DB.query(query, [arrParams.account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
	
				var counter = data.length;		
				var jsonItems = Object.values(data);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {		

						var creParams = {profile_id: jsonItems.profile_id, account_id: jsonItems.account_id};
						methods.getFirstCreditorTransactionDate(creParams, function(lastTranDate){	

							var accParams = {account_id: jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

								var regParam = {profile_id: jsonItems.account_id, account_id: jsonItems.profile_id};
								methods.getCreditorRegistration(regParam, function(retRegistration){	

									var addParam1 = {profile_id: jsonItems.account_id, account_id: jsonItems.profile_id, type: 1};
									methods.getCreditorAddressDetails(addParam1, function(retAdd1){	

							
										var addParam2 = {profile_id: jsonItems.account_id, account_id: jsonItems.profile_id, type: 2};
										methods.getCreditorAddressDetails(addParam2, function(retAdd2){


											var addParam3 = {profile_id: jsonItems.account_id, account_id: jsonItems.profile_id, type: 3};
											methods.getCreditorAddressDetails(addParam3, function(retAdd3){

												if(retAccountDetails!=null)
												{

													var ret_reg = {};
													var ret_status = '0';
													if(retRegistration!=null){
														ret_status = '1';
														ret_reg = retRegistration;
													}

													var ret_add1 = {};
													var ret_add1_status = '0';
													if(retAdd1!=null){
														ret_add1_status = '1';
														ret_add1 = retAdd1;
													}

													var ret_add2 = {};
													var ret_add2_status = '0';
													if(retAdd2!=null){
														ret_add2_status = '1';
														ret_add2 = retAdd2;
													}

													var ret_add3 = {};
													var ret_add3_status = '0';
													if(retAdd3!=null){
														ret_add3_status = '1';
														ret_add3 = retAdd3;
													}

													objvalues = {
								
														account_id   : retAccountDetails.account_id,
														account_type : retAccountDetails.account_type,
														name         : retAccountDetails.name,
														logo         : retAccountDetails.logo,
														date         : lastTranDate,
														registration_status     : ret_status,
														physical_address_status : ret_add1_status,
														delivery_address_status : ret_add2_status,
														postal_address_status   : ret_add3_status,
														registration     : ret_reg,
														physical_address : ret_add1,
														delivery_address : ret_add2,
														postal_address   : ret_add3

													};

													counter -= 1;	
													arrValues.push(objvalues);
									

												}else{	counter -= 1;}

												if (counter === 0){ 
													resolve(arrValues);
												}	

											});

										});

									});

								});

							});

						});

					})
				}],
				function (result, current) {
					if (counter === 0){ 

					  	result[0].sort(function(a, b){ 
	                   	 	return new Date(b.date) - new Date(a.date); 
	                	});

		  				return_result(result[0]);					
					}
				});

			}
			else{return_result(null); }

		});
	
	}
	catch(err)
	{ 
		console.log("getPayablesCreditorsMaintenance",err); return_result(null); 
	}

};



methods.getFirstCreditorTransactionDate =  function (arrParams,return_result){
	
	try
	{			

		var query = `SELECT t1.* FROM(
				SELECT group_id,
					(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
					(SELECT c.meta_value FROM jaze_jazepay_quotations c WHERE c.meta_key = 'date_created' AND c.group_id = jaze_jazepay_quotations.group_id) AS date_created
				FROM  jaze_jazepay_quotations
			) AS t1 WHERE account_id != '' 
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?)
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'account_id' AND meta_value=?)
			ORDER BY DATE(date_created) DESC LIMIT 1`;

		DB.query(query, [arrParams.profile_id,arrParams.account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				return_result(data[0].date_created);
			}
			else{return_result(''); }

		});
	
	}
	catch(err)
	{ 
		console.log("getFirstCreditorTransactionDate",err); return_result(''); 
	}

};




methods.getCreditorRegistration =  function (arrParams,return_result){
	
	try
	{			

		var query = `SELECT * FROM jaze_jazepay_creditor_debtor_settings WHERE group_id IN (SELECT group_id FROM jaze_jazepay_creditor_debtor_settings WHERE meta_key = 'account_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_jazepay_creditor_debtor_settings WHERE meta_key = 'profile_id' AND meta_value =?)`;

		DB.query(query, [arrParams.account_id,arrParams.profile_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var ret_obj = {
						company_registration : retConverted[0].company_registration,
						credit_limit         : retConverted[0].credit_limit,
						discount             : retConverted[0].discount,
						vat_number           : retConverted[0].vat_number,
						payment_terms        : retConverted[0].payment_terms,
						contact_person       : retConverted[0].contact_person,
						discount             : retConverted[0].discount

					};
		
					return_result(ret_obj);
	
				});

			}
			else
			{ 
				return_result(null);

			}

		});
	
	}
	catch(err)
	{ 
		console.log("getCreditorRegistration",err); return_result(null); 
	}

};



methods.getCreditorAddressDetails =  function (arrParams,return_result){
	
	try
	{			

		var query = `SELECT * FROM jaze_jazepay_address WHERE group_id IN (SELECT group_id FROM jaze_jazepay_address WHERE meta_key = 'account_id' AND meta_value =?)
		AND group_id IN (SELECT group_id FROM jaze_jazepay_address WHERE meta_key = 'profile_id' AND meta_value =?)
		AND group_id IN (SELECT group_id FROM jaze_jazepay_address WHERE meta_key = 'type_id' AND meta_value =?)`;

		DB.query(query, [arrParams.account_id,arrParams.profile_id,arrParams.type], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 

				
				methods.convertMultiMetaArray(data, function(retConverted){	

					methods.__getCountryDetails(retConverted[0].country_id, function(retCountry){	

						methods.__getStateDetails(retConverted[0].state_id, function(retState){	

							methods.__getCityDetails(retConverted[0].city_id, function(retCity){	

								methods.__getAreaDetails(retConverted[0].area_id, function(retArea){	

									var obj_return = {
										id              : retConverted[0].group_id.toString(),
										country_id      : retConverted[0].country_id,
										country_name    : retCountry,
										state_id        : retConverted[0].state_id,
										state_name      : retState,
										city_id         : retConverted[0].city_id,
										city_name       : retCity,
										area_id         : retConverted[0].area_id,
										area_name       : retArea,
										street_name     : retConverted[0].street_name,
										street_number   : retConverted[0].street_number,
										building_name   : retConverted[0].building_name,
										building_number : retConverted[0].building_number,
										result_flag     : retConverted[0].type_id,
					
									};


									return_result(obj_return); 

								});

							});

						});

					});

				});

			}
			else
			{
				return_result(null); 
			}

		});
	
	}
	catch(err)
	{ 
		console.log("getCreditorAddressDetails",err); return_result(null); 
	}

};



//------------------------------------------------------------------  jazepay payables creditor maintenance end   --------------------------------------------------------------------------------//


//------------------------------------------------------------------  jazepay payables creditor maintenance registration start   --------------------------------------------------------------------------------//


methods.procCreditorRegistration =  function (arrParams,return_result){
	
	try
	{			

		var query = `SELECT * FROM jaze_jazepay_creditor_debtor_settings WHERE group_id IN (SELECT group_id FROM jaze_jazepay_creditor_debtor_settings WHERE meta_key = 'account_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_jazepay_creditor_debtor_settings WHERE meta_key = 'profile_id' AND meta_value =?)`;

		DB.query(query, [arrParams.account_id,arrParams.creditor_account_id], function (data, error) {

			console.log(data);
			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				
				var updateQuer = `update jaze_jazepay_creditor_debtor_settings SET meta_value =? WHERE meta_key =? AND group_id =?`;
				var	arrUpdate = {
					company_registration : arrParams.registration_no,
					vat_number           : arrParams.vat_no,
					credit_limit         : arrParams.credit_limit,
					payment_terms        : arrParams.payment_terms,
					discount             : arrParams.discount,
					contact_person       : arrParams.contact_person,
					date_modified        : standardDT
				};

				for (var keyS in arrUpdate) 
			 	{	
 					if(arrUpdate[keyS] !='')
 					{
						DB.query(updateQuer, [arrUpdate[keyS],keyS,data[0].group_id], function(dataS, error){
			
							if(parseInt(dataS.affectedRows) > 0){					
								return_result(1);
							}

						});
 					}
				
				}

			}
			else
			{ 

				var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_creditor_debtor_settings";
				var lastGrpId = 0;

				var arrInsert = {
					account_id           : arrParams.account_id,
					profile_id           : arrParams.creditor_account_id,
					company_registration : arrParams.registration_no,
					vat_number           : arrParams.vat_no,
					credit_limit         : arrParams.credit_limit,
					payment_terms        : arrParams.payment_terms,
					discount             : arrParams.discount,
					contact_person       : arrParams.contact_person,
					date_modified        : standardDT,
					date_created         : standardDT,
					status               : 1
				}

				var arrLength = Object.keys(arrInsert).length;
				var num_rows_insert = 0;

				methods.getLastGroupId(queGroupMax, function(queGrpRes){

					lastGrpId = queGrpRes[0].lastGrpId;
					var plusOneGroupID = lastGrpId+1;

					var querInsert = `insert into jaze_jazepay_creditor_debtor_settings (group_id, meta_key, meta_value) VALUES (?,?,?)`;
					for (var key in arrInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
							num_rows_insert += data.affectedRows;
							if(parseInt(num_rows_insert) === arrLength){
								return_result(1);
								
							}
						});						

				  	};


				});

			}

		});
	
	}
	catch(err)
	{ 
		console.log("procCreditorRegistration",err); return_result(null); 
	}

};



//------------------------------------------------------------------  jazepay payables creditor maintenance registration end   --------------------------------------------------------------------------------//


//------------------------------------------------------------------  jazepay payables creditor maintenance address update start   --------------------------------------------------------------------------------//



methods.procCreditorAddress =  function (arrParams,return_result){
	
	try
	{			

		var query = `SELECT * FROM jaze_jazepay_address WHERE group_id IN (SELECT group_id FROM jaze_jazepay_address WHERE meta_key = 'account_id' AND meta_value =?)
		AND group_id IN (SELECT group_id FROM jaze_jazepay_address WHERE meta_key = 'profile_id' AND meta_value =?)
		AND group_id IN (SELECT group_id FROM jaze_jazepay_address WHERE meta_key = 'type_id' AND meta_value =?)`;

		DB.query(query, [arrParams.account_id,arrParams.creditor_account_id,arrParams.flag], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				
				var updateQuer = `update jaze_jazepay_address SET meta_value =? WHERE meta_key =? AND group_id =?`;
				var	arrUpdate = {

					country_id       : arrParams.country_id,
					state_id         : arrParams.state_id,
					city_id          : arrParams.city_id,
					area_id          : arrParams.area_id,
					street_name      : arrParams.street_name,
					street_number    : arrParams.street_number,
					building_name    : arrParams.building_name,
					building_number  : arrParams.building_number
				};



				for (var keyS in arrUpdate) 
			 	{	
 					if(arrUpdate[keyS] !='')
 					{
						DB.query(updateQuer, [arrUpdate[keyS],keyS,data[0].group_id], function(dataS, error){
			
							if(parseInt(dataS.affectedRows) > 0){					
								return_result(1);
							}

						});
 					}
				
				}

			}
			else
			{ 

				var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_address";
				var lastGrpId = 0;

				var arrInsert = {
					account_id       : arrParams.account_id,
					profile_id       : arrParams.creditor_account_id,
					country_id       : arrParams.country_id,
					state_id         : arrParams.state_id,
					city_id          : arrParams.city_id,
					area_id          : arrParams.area_id,
					street_name      : arrParams.street_name,
					street_number    : arrParams.street_number,
					building_name    : arrParams.building_name,
					building_number  : arrParams.building_number,
					type_id          : arrParams.flag
				}


				var arrLength = Object.keys(arrInsert).length;
				var num_rows_insert = 0;

				methods.getLastGroupId(queGroupMax, function(queGrpRes){

					lastGrpId = queGrpRes[0].lastGrpId;
					var plusOneGroupID = lastGrpId+1;

					var querInsert = `insert into jaze_jazepay_address (group_id, meta_key, meta_value) VALUES (?,?,?)`;
					for (var key in arrInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
							num_rows_insert += data.affectedRows;
							if(parseInt(num_rows_insert) === arrLength){
								return_result(1);
								
							}
						});

				  	};


				});

			}

		});
	
	}
	catch(err)
	{ 
		console.log("procCreditorAddress",err); return_result(null); 
	}

};




//------------------------------------------------------------------  jazepay payables creditor maintenance address update end   --------------------------------------------------------------------------------//


//------------------------------------------------------------------  jazepay payables request quotation start   --------------------------------------------------------------------------------//


methods.reqestQuotation =  function (arrParams,return_result){
	
	try
	{			

		var arr_val_len = arrParams.product_values.length;
		var arr_val = arrParams.product_values;

		if(parseInt(arr_val_len) > 0)
		{


			var refParams = {account_id:arrParams.account_id, flag:1};
			methods.getTransactionNumber(refParams,function(retReferenceNumber){

				var quotation_request_no = retReferenceNumber.quo_req_id;
				if(parseInt(retReferenceNumber.quo_req_id) == parseInt(arrParams.quotation_request_no)){
					quotation_request_no = arrParams.quotation_request_no;
				}

				var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_quotation_request";
				var queGroupMaxMeta = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_quotation_request";
				var lastGrpIdM = 0;
				var lastGrpId = 0;

		 		var arrInsert = {
	 				ref_no               : arrParams.reference,
		 			account_id           : arrParams.account_id,
		 			profile_id           : arrParams.request_account_id,
		 			quotation_request_id : quotation_request_no,
		 			quotation_id         : '0', 
		 			type                 : arrParams.flag,
		 			date_created         : newdateForm,
		 			date_updated         : newdateForm,
		 			status               : '1'
		 		}

				var arrLength = Object.keys(arrInsert).length;
				var num_rows_insert = 0;

				methods.getLastGroupId(queGroupMax, function(queGrpRes){

					lastGrpId = queGrpRes[0].lastGrpId;
					var plusOneGroupID = lastGrpId+1;

					var querInsert = `insert into jaze_jazepay_quotation_request (group_id, meta_key, meta_value) VALUES (?,?,?)`;
					for (var key in arrInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
							num_rows_insert += data.affectedRows;
							if(parseInt(num_rows_insert) === arrLength){

								methods.processRequestQuotation(quotation_request_no,arr_val, function(retStatus){		

									return_result(retStatus);

								});

							}
						});						

				  	};


				});
			});

		}
		else
		{
			return_result(0); 
		}

	
	}
	catch(err)
	{ 
		console.log("reqestQuotation",err); return_result(0); 
	}

};



methods.processRequestQuotation =  function(quotation_request_no,arrObjects,return_result){
	try
	{	

		var counter   = arrObjects.length;		
		var jsonItems = Object.values(arrObjects);

		var querInsert = `insert into jaze_jazepay_quotation_request_meta (group_id, meta_key, meta_value) VALUES (?,?,?)`;
		var queGroupMaxMeta = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_quotation_request_meta";


		var lastGrpId  = 0;

		var ctr = 1;
		for (let i = 0; i < counter; i++) {

			setTimeout( function timer(){

				methods.getLastGroupId(queGroupMaxMeta, function(queGrpResMeta){	


					lastGrpId = queGrpResMeta[0].lastGrpId;
					var plusOneGroupID = lastGrpId+1;

					var prod_id = 0;
					var qty = 0;	

			
					if(parseInt(jsonItems[i].id) !== 0 || jsonItems[i].id !=="")
					{
						prod_id = jsonItems[i].id;
						qty     = jsonItems[i].qty;
					}

					var objInsert = {
						quotation_id : quotation_request_no,
						product_id   : prod_id,
						code         : jsonItems[i].code,
						description  : jsonItems[i].desc,
						quantity     : qty,
						date_created : newdateForm,
						status       : '1'
					};

					for (var key in objInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,objInsert[key]], function(data, error){});	

				  	};

			  		if(parseInt(counter) === parseInt(ctr)){
					  	return_result(1);
					}
			
				  	ctr++;

				});

			}, i*3000 );

		}

	}
	catch(err)
	{ console.log("processRequestQuotation",err); return_result(0); }	

};


//------------------------------------------------------------------  jazepay payables request quotation end   ----------------------------------------------------------------------------------//


//------------------------------------------------------------------  generate transaction number start   ----------------------------------------------------------------------------------//




methods.getTransactionNumber =  function (arrParams,return_result){
	
	try
	{				
		var query = ''; 	
		if(parseInt(arrParams.flag) === 1)
		{
			query = `SELECT t1.* FROM(
			SELECT group_id,
					(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id
					FROM  jaze_jazepay_quotation_request
				) AS t1 WHERE account_id != '' 
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotation_request WHERE meta_key = 'account_id' AND meta_value=?)
			AND account_id !=''`;
		}
		else if(parseInt(arrParams.flag) === 2)
		{

			query =`SELECT t1.* FROM(
			SELECT group_id,
					(CASE WHEN meta_key = 'profile_id' THEN meta_value END) AS profile_id
					FROM  jaze_jazepay_quotations	
				) AS t1 WHERE profile_id != '' 
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?)
			AND group_id IN ( SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'type' AND meta_value IN('1','2') )
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'order_id' AND meta_value!='0')
			AND profile_id !=''`;
		}
		else if(parseInt(arrParams.flag) === 3)
		{
			query = `SELECT t1.* FROM(
			SELECT group_id,
					(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id
					FROM  jaze_jazepay_invoice_payments	
				) AS t1 WHERE account_id != '' 
			AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'account_id' AND meta_value=?)`;

		}
		else if(parseInt(arrParams.flag) === 4)
		{
			query = `SELECT t1.* FROM(
			SELECT group_id,
					(CASE WHEN meta_key = 'profile_id' THEN meta_value END) AS profile_id
					FROM  jaze_jazepay_quotations	
				) AS t1 WHERE profile_id != '' 
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?)
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'order_id' AND meta_value !='0')`;

		}


		var ret_obj = {
			quo_req_no : '',
			quo_req_no : ''
		};


		DB.query(query, [arrParams.account_id], function (data, error) {

	
			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				var quo_req_no = '';
				var data_len = data.length;

				var quo_req_id = parseInt(data_len)+1;

				if(parseInt(arrParams.flag) === 1)
				{
			 		quo_req_no = 'QR'+pad_with_zeroes(quo_req_id,5).toString();
				}
				else if(parseInt(arrParams.flag) === 2)
				{
					quo_req_no = 'Q'+pad_with_zeroes(quo_req_id,5).toString();
				}
				else if(parseInt(arrParams.flag) === 3)
				{
					quo_req_no = 'P'+pad_with_zeroes(quo_req_id,5).toString();
				}
				else if(parseInt(arrParams.flag) === 4)
				{
					quo_req_no = 'P'+pad_with_zeroes(quo_req_id,5).toString();
				}


				ret_obj.quo_req_id = quo_req_id.toString();
				ret_obj.quo_req_no = quo_req_no;

			 	return_result(ret_obj); 

			}
			else
			{

				var quo_req_no;

				if(parseInt(arrParams.flag) === 1)
				{
			 		quo_req_no = 'QR'+pad_with_zeroes(1,5).toString();
				}
				else if(parseInt(arrParams.flag) === 2)
				{
					quo_req_no = 'Q'+pad_with_zeroes(1,5).toString();
				}
				else if(parseInt(arrParams.flag) === 3)
				{
					quo_req_no = 'P'+pad_with_zeroes(1,5).toString();
				}
				else if(parseInt(arrParams.flag) === 4)
				{
					quo_req_no = 'O'+pad_with_zeroes(1,5).toString();
				}


				ret_obj.quo_req_id = '1';
				ret_obj.quo_req_no = quo_req_no;


				return_result(ret_obj); 
			}

		});
	
	}
	catch(err)
	{ 
		console.log("getTransactionNumber",err); return_result(null); 
	}

};


//------------------------------------------------------------------  generate transaction number end   ------------------------------------------------------------------------------------//


methods.getQuotationRequest =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_quotation_request WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotation_request WHERE meta_key = 'account_id' AND meta_value=?) ";
	
		query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotation_request WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";

		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParam = {account_id:jsonItems.profile_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){

								var type = 0;
								if(parseInt(jsonItems.quotation_id) !==0){
									type = 1;
								}

								var quotParam = {request_id:jsonItems.quotation_request_id,type:type }

								methods.getQuotationNumber(quotParam,function(retQuoNumber){

									var ref_no = 'QR'+pad_with_zeroes(jsonItems.quotation_request_id,5).toString();

							

									objvalues = {
										id           : jsonItems.group_id.toString(),
										quotation_request_no   : ref_no,
										quotation_no           : retQuoNumber,
										account_id   : retAccountDetails.account_id,
										account_type : retAccountDetails.account_type,
										name         : retAccountDetails.name,
										logo         : retAccountDetails.logo,
										quotation_status : jsonItems.status.toString(),
										date         : jsonItems.date_created
									};

									counter -= 1;	
									arrValues.push(objvalues);
									if (counter === 0){ 
										resolve(arrValues);
									}			


								});				
							});				
						
				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getQuotationRequest",err); return_result(null); }	
};






methods.getQuotationNumber =  function (quoParam,return_result){
	
	try
	{				

		if(parseInt(quoParam.type) === 1)
		{

			var query = `SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key ='request_id' AND meta_value=?)`;
			DB.query(query, [quoParam.request_id], function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					methods.convertMultiMetaArray(data, function(retConverted){	

					var order_num = pad_with_zeroes(retConverted[0].quotation_id,6).toString();	
						return_result(order_num);
					});

					
				}
				else{ return_result(''); }

			});
		}
		else
		{
			return_result('');
		}

	
	}
	catch(err)
	{ 
		console.log("getQuotationNumber",err); return_result(''); 
	}

};


//------------------------------------------------------------------- payables quotation details Module Start ---------------------------------------------------------------------------//

methods.getQuotationRequestDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var item_count = 0;
		var query = "SELECT * FROM jaze_jazepay_quotation_request WHERE group_id=? ";
	

		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getQuotationRequestDetailsMeta(jsonItems.group_id,function(retItems){

								var recParam = {account_id:jsonItems.profile_id};
								HelperGeneralJazenet.getAccountDetails(recParam,function(retReceiver){

									var senParam = {account_id:jsonItems.account_id};
									HelperGeneralJazenet.getAccountDetails(senParam,function(retSender){

										var type = 0;
										if(parseInt(jsonItems.quotation_id) !==0){
											type = 1;
										}

										var tranParam = { request_id:jsonItems.group_id, type:type };
										methods.getQuotationNumber(tranParam,function(retQuoNumber){

											var ref_no = 'QR'+pad_with_zeroes(jsonItems.quotation_request_id,5).toString();
											var quotation_no = "";

											if(parseInt(jsonItems.quotation_id) !==0){
												quotation_no = 'Q'+pad_with_zeroes(retQuoNumber,4).toString();

											}

											var obj_rec = {
												account_id     : retReceiver.account_id,
												account_type   : retReceiver.account_type,								
												category       : retReceiver.category,
												name           : retReceiver.name,
												logo           : retReceiver.logo
											};

											var obj_sen = {
												account_id   : retSender.account_id,
												account_type : retSender.account_type,
												category     : retSender.category,
												name         : retSender.name,
												logo         : retSender.logo
											};

											var tot_qty = "0";
											retItems.forEach( function (value){	

												if(value.qty !=""){
													item_count += parseInt(value.qty);
												}						
										   		
											});

											tot_qty = item_count.toString();
												
									
										
											var quot_request = {

												id                   : jsonItems.group_id.toString(),
												ref_no               : jsonItems.ref_no,
												quotation_request_no : ref_no,
												receiver_details     : obj_rec,
												sender_details       : obj_sen,
												date                 : jsonItems.date_created,
												type                 : jsonItems.type.toString(),
												items                : retItems,
												total_qty            : tot_qty,
												status               : jsonItems.status,
												quotation_no         : quotation_no

											};

			 								counter -= 1;	
					
											if (counter === 0){ 
												resolve(quot_request);
											}

										});
									});
								});

							});			
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getQuotationRequestDetails",err); return_result(null); }	
};



methods.getQuotationRequestDetailsMeta =  function (quotation_id,return_result){
	
	try
	{				

		var arrValues = [];
		var obj_return = {};

		var obj_return = {

			product_id  : '',
			code        : '',
			description : '',

		}

		var query = `SELECT * FROM jaze_jazepay_quotation_request_meta WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotation_request_meta WHERE meta_key = 'quotation_id' AND meta_value=?)`;
		DB.query(query, [quotation_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	

	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getProductDetails(jsonItems.product_id,function(retProdDetails){

								if(parseInt(jsonItems.product_id) !==0 )
								{

									obj_return = {
										id          : jsonItems.product_id,
										sku         : jsonItems.code,
										description : jsonItems.description,
										logo        : retProdDetails.logo,
										brand       : retProdDetails.brand,
										price       : retProdDetails.price,
										qty         : jsonItems.quantity
									};

								}
								else
								{
									obj_return = {
										id          : jsonItems.product_id,
										sku         : jsonItems.code,
										description : jsonItems.description,
										logo        : '',
										brand       : '',
										price       : '',
										qty         : jsonItems.quantity

									};					
								}

								counter -= 1;	
								arrValues.push(obj_return);
								if (counter === 0){ 
									resolve(arrValues);
								}	
					
							});
				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
							return_result(result[0].sort(sortingArrayObject('description')));

						}
					});


				});

			}
			else{ return_result([]); }

		});

	
	}
	catch(err)
	{ 
		console.log("getQuotationRequestDetailsMeta",err); return_result([]); 
	}

};



methods.getProductDetails =  function (product_id,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var query = "SELECT * FROM jaze_products WHERE id=?";

		DB.query(query,[product_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				var counter = data.length;		
				var jsonItems = Object.values(data);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	

						methods.getCompanyProductsMeta(jsonItems.id,jsonItems.account_id,function(retMeta){

							methods.getProductBrand(jsonItems.jaze_brand_id,function(retBrand){

								if(retMeta!=null){

									objvalues = {
										id         : jsonItems.id.toString(),
										sku        : retMeta.sku,
										name       : jsonItems.title,
										logo       : retMeta.logo,
										brand      : retBrand, 
										price      : retMeta.price
						
									};

							
								}

								counter -= 1;	
								if (counter === 0){ 
									resolve(objvalues);
								}	

							});
						});
				
					})
				}],
				function (result, current) {
					if (counter === 0){ 
						return_result(result[0]);				
					}
				});

	

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getCompanyProducts",err); return_result(null); }	
};

//------------------------------------------------------------------- payables quotation details Module End  ----------------------------------------------------------------------------//

//------------------------------------------------------------------- payables update quotation Module Start  ----------------------------------------------------------------------------//

methods.updateQuotationRequest =  function (arrParams,return_result){
	
	try
	{							


		var query = "SELECT * FROM jaze_jazepay_quotation_request WHERE group_id=? ";

		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){			

					if(parseInt(arrParams.flag) === 1)
					{

						var queryIn = `SELECT * FROM jaze_jazepay_quotation_request_meta WHERE 
								group_id IN (SELECT group_id FROM jaze_jazepay_quotation_request_meta WHERE meta_key = 'quotation_id' AND meta_value =?) GROUP BY group_id`

						DB.query(queryIn,[retConverted[0].group_id], function(dataIn, error){

							if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
							{
								
								methods.convertMultiMetaArray(dataIn, function(retConvertedIn){		
				
									var arrGroupID = [];
									retConvertedIn.forEach(function(val){
										arrGroupID.push(val.group_id);
									});
					
									var queDelC = "DELETE FROM jaze_jazepay_quotation_request_meta WHERE group_id IN("+arrGroupID+") ";
									DB.query(queDelC,null, function(dataDel, error){
									 	if(parseInt(dataDel.affectedRows) > 0)
			 							{
											
		 									var arrObjects = arrParams.product_values;
											var counter   = arrObjects.length;		
											var jsonItems = Object.values(arrObjects);

											var querInsert = `insert into jaze_jazepay_quotation_request_meta (group_id, meta_key, meta_value) VALUES (?,?,?)`;
											var queGroupMaxMeta = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_quotation_request_meta";

											var lastGrpId  = 0;
											var ctr = 1;
 									
 											for (let i = 0; i < counter; i++) {

												setTimeout( function timer(){

													methods.getLastGroupId(queGroupMaxMeta, function(queGrpResMeta){	

														lastGrpId = queGrpResMeta[0].lastGrpId;
														var plusOneGroupID = lastGrpId+1;

														var prod_id = 0;
														var qty = 0;	

														if(parseInt(jsonItems[i].id) !== 0 || jsonItems[i].id !=="")
														{
															prod_id = jsonItems[i].id;
															qty     = jsonItems[i].qty;
														}

														var objInsert = {
															quotation_id : retConverted[0].group_id,
															product_id   : prod_id,
															code         : jsonItems[i].code,
															description  : jsonItems[i].desc,
															quantity     : qty,
															date_created : newdateForm,
															status       : '1'
														};	

														for (var key in objInsert) 
													 	{
															DB.query(querInsert,[plusOneGroupID,key,objInsert[key]], function(data, error){});	

													  	};

												  		if(parseInt(counter) === parseInt(ctr)){
														  	return_result(1);
														}
												
													  	ctr++;

													});

												}, i*3000 );

											}

		 								}
		 								else
		 								{
											return_result(0);
		 								}
									});

								});
							}
							else
							{

								var arrObjects = arrParams.product_values;
								var counter   = arrObjects.length;		
								var jsonItems = Object.values(arrObjects);

								var querInsert = `insert into jaze_jazepay_quotation_request_meta (group_id, meta_key, meta_value) VALUES (?,?,?)`;
								var queGroupMaxMeta = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_quotation_request_meta";


								var lastGrpId  = 0;

								var ctr = 1;
								for (let i = 0; i < counter; i++) {

									setTimeout( function timer(){

										methods.getLastGroupId(queGroupMaxMeta, function(queGrpResMeta){	

											lastGrpId = queGrpResMeta[0].lastGrpId;
											var plusOneGroupID = lastGrpId+1;

											var prod_id = 0;
											var qty = 0;	

											if(parseInt(jsonItems[i].id) !== 0 || jsonItems[i].id !=="")
											{
												prod_id = jsonItems[i].id;
												qty     = jsonItems[i].qty;
											}

											var objInsert = {
												quotation_id : retConverted[0].group_id,
												product_id   : prod_id,
												code         : jsonItems[i].code,
												description  : jsonItems[i].desc,
												quantity     : qty,
												date_created : newdateForm,
												status       : '1'
											};	

											for (var key in objInsert) 
										 	{
												DB.query(querInsert,[plusOneGroupID,key,objInsert[key]], function(data, error){});	

										  	};

									  		if(parseInt(counter) === parseInt(ctr)){
											  	return_result(1);
											}
									
										  	ctr++;

										});

									}, i*3000 );

								}

							}
						});

						//amenned


					}
					else if(parseInt(arrParams.flag) === 2)
					{
						//cancel
						var updateQue = `update jaze_jazepay_quotation_request SET meta_value =? WHERE meta_key =? AND group_id =?`;
						DB.query(updateQue, ['3','status',retConverted[0].group_id], function(data, error){
							if(data!=null){
								return_result(1);
							}else{
								return_result(0);
							}
							
						});
					}

				});

			}
			else
			{
				return_result(2);
			}

		})

	}
	catch(err)
	{ 
		console.log("updateQuotationRequest",err); return_result(0); 
	}

};


//------------------------------------------------------------------- payables update quotation Module End  ----------------------------------------------------------------------------//


methods.getQuotationReceivedList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?) ";
	
		query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'status' AND ( meta_value='1' OR meta_value='0' ) ) ";
		query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";


		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParam = {account_id:jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){


								var quotation_no = pad_with_zeroes(jsonItems.order_ref_no,6).toString();
								var ord_no = "";

								var status = "0";
								if(jsonItems.status!=""){
									status = jsonItems.status;
								}

								if(parseInt(jsonItems.order_id)!==0){
									ord_no = pad_with_zeroes(jsonItems.order_id,6).toString();
								}

								objvalues = {
									id           : jsonItems.group_id.toString(),
									quotation_no : quotation_no,
									order_no     : ord_no,
									account_id   : retAccountDetails.account_id,
									account_type : retAccountDetails.account_type,
									name         : retAccountDetails.name,
									logo         : retAccountDetails.logo,
									amount       : jsonItems.total,
									quotation_status : status,
									date         : jsonItems.date_created
								};

								counter -= 1;	
								arrValues.push(objvalues);
								if (counter === 0){ 
									resolve(arrValues);
								}			
		
							});				
					
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getQuotationReceivedList",err); return_result(null); }	
};


methods.getQuotationReceiveDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var item_count = 0;
		var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id=? ";
	

		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getQuotationReceiveDetailsMeta(jsonItems.group_id,function(retItems){

								var recParam = {account_id:jsonItems.account_id};
								HelperGeneralJazenet.getAccountDetails(recParam,function(retReceiver){

									var senParam = {account_id:jsonItems.profile_id};
									HelperGeneralJazenet.getAccountDetails(senParam,function(retSender){

										var type = 0;
										if(parseInt(jsonItems.quotation_id) !==0){
											type = 1;
										}

			
										var ref_no = 'QR'+pad_with_zeroes(jsonItems.quotation_request_id,5).toString();
										var quo_no = 'Q'+pad_with_zeroes(jsonItems.quotation_id,5).toString();

										var ord_no = "";

										if(parseInt(jsonItems.order_id) !==0){
											ord_no = 'O'+pad_with_zeroes(jsonItems.order_id,5).toString();
										}

										var obj_rec = {
											account_id   : retReceiver.account_id,
											account_type : retReceiver.account_type,
											name         : retReceiver.name,
											logo         : retReceiver.logo
										};

										var obj_sen = {
											account_id   : retSender.account_id,
											account_type : retSender.account_type,
											name         : retSender.name,
											logo         : retSender.logo
										};

										var tot_qty = "0";
										var sub_total = 0;
										retItems.forEach( function (value){	

											if(value.qty !=""){
												item_count += parseInt(value.qty);
												sub_total  += parseFloat(value.total);
											}						
									   		
										});

										var cur_tax   = '5.00';
										var tax       = 5/100;
										var cur_disc  = '0.00';
										var discount  = 0;
										var computed_disc = sub_total;

										var comp_disc = 0;

										if(jsonItems.tax!=""){
											tax = parseFloat(jsonItems.tax)/100;
											cur_tax = jsonItems.tax;
										}

										//discounted_price = original_price - (original_price * discount / 100)
								
										if(jsonItems.discount!=""){
											cur_disc  = parseFloat(jsonItems.discount).toFixed(2);
											discount  = parseFloat(jsonItems.discount)/100;
											comp_disc = parseFloat(sub_total)*parseFloat(discount);
											computed_disc = parseFloat(sub_total)-parseFloat(comp_disc);
										}

										tot_qty = item_count.toString();
										var tax_compute = parseFloat(tax)*parseFloat(computed_disc);
										var total = parseFloat(tax_compute)+parseFloat(computed_disc);
										
										var obj_acc = {
											sub_total      : sub_total.toFixed(2),
											discount       : cur_disc,
											total_discount : comp_disc.toFixed(2),
											tax            : cur_tax,
											total_tax      : tax_compute.toFixed(2),
											total          : total.toFixed(2)
										};
								
									
										var quot_request = {

											id                   : jsonItems.group_id.toString(),
											quotation_no         : quo_no,
											ref_no               : ref_no,
											receiver_details     : obj_rec,
											sender_details       : obj_sen,
											date                 : jsonItems.date_created,
											items                : retItems,
											total_qty            : tot_qty,
											status               : jsonItems.status,
											order_no             : ord_no,
											accounting           : obj_acc

										};

		 								counter -= 1;	
				
										if (counter === 0){ 
											resolve(quot_request);
										}

									
									});
								});

							});			
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getQuotationReceiveDetails",err); return_result(null); }	
};



methods.getQuotationReceiveDetailsMeta =  function (quotation_id,return_result){
	
	try
	{				

		var arrValues = [];
		var obj_return = {};

		var total = 0;

		var query = `SELECT * FROM jaze_jazepay_quotations_meta WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations_meta WHERE meta_key = 'quotation_id' AND meta_value=?)`;
		DB.query(query, [quotation_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	
	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							if(parseInt(jsonItems.product_id) !== 0)
							{
								methods.getProductDetails(jsonItems.product_id,function(retBrand){
			
									total = parseFloat(jsonItems.amount)*parseFloat(jsonItems.quantity);

									obj_return = {
										id          : jsonItems.group_id.toString(),
										product_id  : jsonItems.product_id,
										code        : jsonItems.code,
										description : jsonItems.description,
										brand       : retBrand.brand,
										amount      : jsonItems.amount,
										qty         : jsonItems.quantity,
										total       : total.toFixed(2)
									};

									counter -= 1;	
									arrValues.push(obj_return);
									if (counter === 0){ 
										resolve(arrValues);
									}	
								});

							}
							else
							{
								total = parseFloat(jsonItems.amount)*parseFloat(jsonItems.quantity);

								obj_return = {
									id          : jsonItems.group_id.toString(),
									product_id  : jsonItems.product_id,
									code        : jsonItems.code,
									description : jsonItems.description,
									brand       : '',
									amount      : jsonItems.amount,
									qty         : jsonItems.quantity,
									total       : total.toFixed(2)
								};

								counter -= 1;	
								arrValues.push(obj_return);
								if (counter === 0){ 
									resolve(arrValues);
								}	

							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return a.id - b.id; 
		                	});

 							return_result(result[0]);
						}
					});


				});

			}
			else{ return_result([]); }

		});

	
	}
	catch(err)
	{ 
		console.log("getQuotationReceiveDetailsMeta",err); return_result([]); 
	}

};





methods.acceptRejectQuotationReceived =  function (arrParams,return_result){
	
	try
	{				

		var query = `SELECT * FROM jaze_jazepay_quotations WHERE group_id=?`;
		DB.query(query, [arrParams.id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 	

				methods.convertMultiMetaArray(data, function(retConverted){	

					var recParam = {account_id:retConverted[0].account_id};
					HelperGeneralJazenet.getAccountDetails(recParam,function(retReceiver){

						var senParam = {account_id:retConverted[0].profile_id};
						HelperGeneralJazenet.getAccountDetails(senParam,function(retSender){
			
							var tranParam = { account_id:arrParams.account_id, flag:4 };
							methods.getTransactionNumber(tranParam,function(retorder_id){

								var updateQuer = `update jaze_jazepay_quotations SET meta_value =? WHERE meta_key =? AND group_id =?`;

								var	arrUpdate = {
									date_updated         : newdateForm,
									status               : 2
								};

								if(parseInt(arrParams.flag) === 1) {

									arrUpdate = {

										order_status         : '0',
										order_ref_no         : retConverted[0].quotation_id,
										order_id             : retorder_id.quo_req_id,
										order_date_updated   : newdateForm,
										order_date_created   : newdateForm,
										date_updated         : newdateForm,
										status               : '1'
									};

								}


								for (var keyS in arrUpdate) 
							 	{	
				 					if(arrUpdate[keyS] !='')
				 					{
										DB.query(updateQuer, [arrUpdate[keyS],keyS,arrParams.id], function(dataS, error){
							
											if(parseInt(dataS.affectedRows) > 0){					
												return_result(1);
											}

										});
				 					}
								
								}

								var successMessage = "Order sent.";
								if(parseInt(arrParams.flag) == 2){
									successMessage = "Quotation rejected.";
								}




								var notiParams = {	
									group_id             : retorder_id.quo_req_id,
									account_id           : retSender.account_id,
									account_type         : retSender.account_type,
									request_account_id   : retReceiver.account_id,
									request_account_type : retReceiver.account_type,
									message              : successMessage,
									flag                 :'55'
								};

								//send notification
								//HelperNotification.sendPushNotification(notiParams, function(retNoti){});

							});
						});
					});
				});
			}
			else
			{ 
				return_result(2);

			}

		});

	}
	catch(err)
	{ console.log("acceptRejectQuotationReceived",err); return_result(0); }	
};



//------------------------------------------------------------------- Company Products Module Start ---------------------------------------------------------------------------//

methods.getCompanyProducts =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var query = "SELECT * FROM jaze_products WHERE account_id =?";

		DB.query(query,[arrParams.request_account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				var counter = data.length;		
				var jsonItems = Object.values(data);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	

						methods.getCompanyProductsMeta(jsonItems.id,jsonItems.account_id,function(retMeta){

							methods.getProductBrand(jsonItems.jaze_brand_id,function(retBrand){

								if(retMeta!=null){

									objvalues = {
										id         : jsonItems.id.toString(),
										sku        : retMeta.sku,
										name       : jsonItems.title,
										logo       : retMeta.logo,
										brand      : retBrand, 
										price      : retMeta.price
						
									};

									arrValues.push(objvalues);
								}

								counter -= 1;	
								if (counter === 0){ 
									resolve(arrValues);
								}	

							});
						});
				
					})
				}],
				function (result, current) {
					if (counter === 0){ 
						return_result(result[0].sort(sortingArrayObject('name')));				
					}
				});

	

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getCompanyProducts",err); return_result(null); }	
};


methods.getProductBrand = function(product_id,return_result){

	try
	{	

		var queryIn = "SELECT * FROM jaze_jazestore_brand_list WHERE group_id =?";
		DB.query(queryIn,[product_id], function(dataIn, error){
		
			if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
			{		
				methods.convertMultiMetaArray(dataIn, function(retConverted){		
	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							counter -= 1;
							if (counter === 0){ 
								resolve(jsonItems.name);
							}
						})
					}],
					function (result, current) {
						if (counter === 0){ 

							return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result('');
			}
			
		});
	}
	catch(err)
	{ 
		console.log("getProductBrand",err); return_result(''); 
	}	
};


methods.getCompanyProductsMeta =  function (product_id,account_id,return_result){
	
	try
	{				

		var objvalues  = {};

		var query = "SELECT * FROM jaze_products_meta WHERE product_id=?";

		DB.query(query,[product_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArrayProduct(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var price = jsonItems.regular_price;
							if(jsonItems.sale_price && parseInt(jsonItems.sale_price) !== 0 ){
								price = jsonItems.sale_price;
							}

							var logo = "";
							if(jsonItems.featured_image!=""){
								logo =  IMG_URL_PRODUCTS+account_id+'/products/'+product_id+'/thumb/'+jsonItems.featured_image;
							}
							objvalues = {
								sku     : jsonItems.sku.toString(),
								logo    : logo,
								price   : price
							};

							counter -= 1;	
						
							if (counter === 0){ 
								resolve(objvalues);
							}			
					
						})
					}],
					function (result, current) {
						if (counter === 0){ 
			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getCompanyProductsMeta",err); return_result(null); }	
};


//------------------------------------------------------------------- Company Products Module End ---------------------------------------------------------------------------//




//------------------------------------------------------------------  jazepay payables request order start   ----------------------------------------------------------------------------------//

methods.reqestOrderQuotation =  function (arrParams,return_result){
	
	try
	{			

		var arr_val_len = arrParams.product_values.length;
		var arr_val = arrParams.product_values;

		if(parseInt(arr_val_len) > 0)
		{

			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_quotations";

			var lastGrpIdM = 0;
			var lastGrpId = 0;

			var total_price = 0;
			arr_val.forEach(function(val){
				total_price += parseFloat(parseFloat(val.price) * val.qty);
			});

			var tax = 5/100;

			var tax_compute = parseFloat(parseFloat(tax)*parseFloat(total_price));
			var total = parseFloat(tax_compute)+parseFloat(total_price);

	 		var arrInsert = {
 			
	 			account_id           : arrParams.request_account_id,
	 			profile_id           : arrParams.account_id,
	 			request_id           : '0',
	 			type                 : '2',
	 			total                : total.toFixed(2),
	 			discount             : '1',
	 			tax                  : '5',
	 			ref_no               : '',
	 			order_status         : '0',
	 			order_ref_no         : '',
	 			invoice_status       : '',
	 			invoice_ref_no       : '',
	 			balance              : total.toFixed(2),
	 			payment_id           : '0',
	 			quotation_id         : '0',
	 			order_id         	 : arrParams.quotation_request_no,
	 			invoice_id           : '0',
	 			balance_date_updated : newdateForm,
	 			order_date_created   : newdateForm,
	 			order_date_updated   : newdateForm,
	 			invoice_date_created : '',
	 			invoice_date_updated : '',
	 			date_created         : newdateForm,
	 			date_updated         : newdateForm,
	 			status               : ''

	 		}

			var arrLength = Object.keys(arrInsert).length;
			var num_rows_insert = 0;

			methods.getLastGroupId(queGroupMax, function(queGrpRes){

				lastGrpId = queGrpRes[0].lastGrpId;
				var plusOneGroupID = lastGrpId+1;

				var querInsert = `insert into jaze_jazepay_quotations (group_id, meta_key, meta_value) VALUES (?,?,?)`;
				for (var key in arrInsert) 
			 	{
					DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
						num_rows_insert += data.affectedRows;
						if(parseInt(num_rows_insert) === arrLength){

							methods.processRequestOrderQuotation(arrParams,arr_val,plusOneGroupID, function(retStatus){		

								return_result(retStatus);

							});

						}
					});						

			  	};


			});

		}
		else
		{
			return_result(0); 
		}

	
	}
	catch(err)
	{ 
		console.log("reqestOrderQuotation",err); return_result(0); 
	}

};



methods.processRequestOrderQuotation =  function(arrParams,arrObjects,group_id,return_result){
	try
	{	

		var counter   = arrObjects.length;		
		var jsonItems = Object.values(arrObjects);

		var querInsert = `insert into jaze_jazepay_quotations_meta (group_id, meta_key, meta_value) VALUES (?,?,?)`;
		var queGroupMaxMeta = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_quotations_meta";


		var lastGrpId  = 0;

		var ctr = 1;
		for (let i = 0; i < counter; i++) {

			setTimeout( function timer(){

				methods.getLastGroupId(queGroupMaxMeta, function(queGrpResMeta){	

					lastGrpId = queGrpResMeta[0].lastGrpId;
					var plusOneGroupID = lastGrpId+1;

					var objInsert = {
						quotation_id : group_id,
						product_id   : jsonItems[i].id,
						code         : jsonItems[i].code,
						description  : jsonItems[i].desc,
						quantity     : jsonItems[i].qty,
						amount       : jsonItems[i].price,
						date_created : newdateForm,
						status       : '1'
					};

					for (var key in objInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,objInsert[key]], function(data, error){});	

				  	};

			  		if(parseInt(counter) === parseInt(ctr)){
					  	return_result(1);
					}
			
				  	ctr++;

				});

			}, i*3000 );

		}

	}
	catch(err)
	{ console.log("processRequestOrderQuotation",err); return_result(0); }	

};


//------------------------------------------------------------------  jazepay payables request order end   ----------------------------------------------------------------------------------//

//------------------------------------------------------------------  jazepay payables request order list start   ----------------------------------------------------------------------------------//


methods.getOrderQuotationRequestList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'type' AND meta_value ='2' ) ";
			// query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'order_id' AND meta_value !='0' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
	
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParam = {account_id:jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){

								methods.getOrderQuotationRequestTotalAmount(jsonItems.group_id,function(metaVal){
							
									var order_id = 'O'+pad_with_zeroes(jsonItems.order_id,5).toString();

									var quotation_no ="";
									if(jsonItems.quotation_id !=="" || typeof jsonItems.quotation_id !== 'undefined' ){
										quotation_no = pad_with_zeroes(jsonItems.quotation_id,5).toString();
									}


									var invoice_ref_no ="";
									if( jsonItems.invoice_id !=="" || typeof  jsonItems.invoice_id !== 'undefined'){
										invoice_ref_no = pad_with_zeroes(jsonItems.invoice_id,5).toString();
									}
									

									var sub_total = 0;
									var tax = 5/100;
									var tax_compute = 0;
									var total = 0;

									metaVal.forEach(function(val){
										sub_total += parseFloat(val.total);

									});

									var status = '0';
									if(jsonItems.status!=""){
										status = jsonItems.status;
									}

									tax_compute = parseFloat(tax)*parseFloat(sub_total);
									total = parseFloat(tax_compute)+parseFloat(sub_total);


									objvalues = {
										id           : jsonItems.group_id.toString(),
										order_no     : order_id,
										quotation_no : quotation_no,
										invoice_no   : invoice_ref_no,
										account_id   : retAccountDetails.account_id,
										account_type : retAccountDetails.account_type,
										name         : retAccountDetails.name,
										logo         : retAccountDetails.logo,
										amount       : total.toFixed(2),
										status       : status,
										date         : jsonItems.date_updated
									};

									counter -= 1;	
									arrValues.push(objvalues);
									if (counter === 0){ 
										resolve(arrValues);
									}			


								});				
							});				
						
				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getOrderQuotationRequestList",err); return_result(null); }	
};


methods.getOrderQuotationRequestTotalAmount =  function (quotation_id,return_result){
	
	try
	{				

		var arrValues = [];
		var obj_return = {};

		//var total = 0;
		//var tot_qty = 0;

		var query = `SELECT * FROM jaze_jazepay_quotations_meta WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations_meta WHERE meta_key = 'quotation_id' AND meta_value=?)`;
		DB.query(query, [quotation_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	
	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

						var total = parseFloat(jsonItems.amount)*parseFloat(jsonItems.quantity);


						obj_return = {
								id          : jsonItems.group_id.toString(),
								product_id  : jsonItems.product_id,
								code        : jsonItems.code,
								description : jsonItems.description,
								amount      : jsonItems.amount,
								qty         : jsonItems.quantity,
								total       : total
							};

							counter -= 1;	
							arrValues.push(obj_return);
							if (counter === 0){ 
								resolve(arrValues);
							}	
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
				

 							return_result(result[0]);
						}
					});


				});

			}
			else{ return_result([]); }

		});

	
	}
	catch(err)
	{ 
		console.log("getOrderQuotationRequestTotalAmount",err); return_result([]); 
	}

};


//------------------------------------------------------------------  jazepay payables request order lsit end   ----------------------------------------------------------------------------------//


//------------------------------------------------------------------  jazepay payables request order details start   ----------------------------------------------------------------------------------//

methods.getOrderRequestQuotationDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var item_count = 0;

		var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id=? ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'type' AND meta_value ='2' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'order_id' AND meta_value !='0' ) ";
	

		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getOrderRequestQuotationDetailsMeta(jsonItems.group_id,function(retItems){

								var recParam = {account_id:jsonItems.account_id};
								HelperGeneralJazenet.getAccountDetails(recParam,function(retReceiver){

									var senParam = {account_id:jsonItems.profile_id};
									HelperGeneralJazenet.getAccountDetails(senParam,function(retSender){

										var order_id = 'O'+pad_with_zeroes(jsonItems.order_id,5).toString();

										var quotation_no ="";
										if( parseInt(jsonItems.quotation_id)!==0 ){
											if(jsonItems.quotation_id !=="" || typeof jsonItems.quotation_id !== 'undefined')
											{
												quotation_no = 'Q'+pad_with_zeroes(jsonItems.quotation_id,5).toString();
											}
										}

										var invoice_ref_no ="";
										if( parseInt(jsonItems.invoice_id)!==0 )
										{
											if( jsonItems.invoice_id !=="" || typeof  jsonItems.invoice_id !== 'undefined'){
												invoice_ref_no = 'I'+pad_with_zeroes(jsonItems.invoice_id,5).toString();
											}
										}

										var obj_rec = {
											account_id   : retReceiver.account_id,
											account_type : retReceiver.account_type,
											name         : retReceiver.name,
											logo         : retReceiver.logo
										};

										var obj_sen = {
											account_id   : retSender.account_id,
											account_type : retSender.account_type,
											name         : retSender.name,
											logo         : retSender.logo
										};

										var tot_qty = "0";
										var sub_total = 0;
										retItems.forEach( function (value){	

											if(value.qty !=""){
												item_count += parseInt(value.qty);
												sub_total  += parseFloat(value.total);
											}						
									   		
										});

										var cur_tax   = '5.00';
										var tax       = 5/100;
										var cur_disc  = '0.00';
										var discount  = 0;
										var computed_disc = sub_total;

										var comp_disc = 0;

										if(jsonItems.tax!=""){
											tax = parseFloat(jsonItems.tax)/100;
											cur_tax = parseFloat(jsonItems.tax).toFixed(2);
										}
								
										if(jsonItems.discount!=""){
											cur_disc  = parseFloat(jsonItems.discount).toFixed(2);
											discount  = parseFloat(jsonItems.discount)/100;
											comp_disc = parseFloat(sub_total)*parseFloat(discount);
											computed_disc = parseFloat(sub_total)-parseFloat(comp_disc);
										}

										tot_qty = item_count.toString();
										var tax_compute = parseFloat(tax)*parseFloat(computed_disc);
										var total = parseFloat(tax_compute)+parseFloat(computed_disc);
										
										var obj_acc = {
											sub_total      : sub_total.toFixed(2),
											discount       : cur_disc,
											total_discount : comp_disc.toFixed(2),
											tax            : cur_tax,
											total_tax      : tax_compute.toFixed(2),
											total          : total.toFixed(2)
										};
								
									
										var quot_request = {

											id           		 : jsonItems.group_id.toString(),
											order_no     		 : order_id,
											ref_no 		 		 : quotation_no,
											invoice_no   	     : invoice_ref_no,
											receiver_details     : obj_rec,
											sender_details       : obj_sen,
											date                 : jsonItems.date_created,
											items                : retItems,
											total_qty            : tot_qty,
											status               : jsonItems.status,
											accounting           : obj_acc

										};

		 								counter -= 1;	
				
										if (counter === 0){ 
											resolve(quot_request);
										}

									
									});
								});

							});			
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getOrderRequestQuotationDetails",err); return_result(null); }	
};



methods.getOrderRequestQuotationDetailsMeta =  function (quotation_id,return_result){
	
	try
	{				

		var arrValues = [];
		var obj_return = {};

		var total = 0;

		var query = `SELECT * FROM jaze_jazepay_quotations_meta WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations_meta WHERE meta_key = 'quotation_id' AND meta_value=?)`;
		DB.query(query, [quotation_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	
	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

						total = parseFloat(jsonItems.amount)*parseFloat(jsonItems.quantity);

						obj_return = {
								id          : jsonItems.group_id.toString(),
								product_id  : jsonItems.product_id,
								code        : jsonItems.code,
								description : jsonItems.description,
								amount      : jsonItems.amount,
								qty         : jsonItems.quantity,
								total       : total.toString()
							};

							counter -= 1;	
							arrValues.push(obj_return);
							if (counter === 0){ 
								resolve(arrValues);
							}	
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return a.id - b.id; 
		                	});

		                	console.log(result[0]);

 							return_result(result[0]);
						}
					});


				});

			}
			else{ return_result([]); }

		});

	
	}
	catch(err)
	{ 
		console.log("getOrderRequestQuotationDetailsMeta",err); return_result([]); 
	}

};

//------------------------------------------------------------------  jazepay payables request order details end   ----------------------------------------------------------------------------------//




//------------------------------------------------------------------  jazepay payables invoice received list start   ----------------------------------------------------------------------------------//

methods.getInvoiceReceivedList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'type' AND meta_value ='2' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'invoice_id' AND meta_value !='0' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
	
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParam = {account_id:jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){

								methods.getInvoiceReceivedTotalAmount(jsonItems.group_id,function(metaVal){

									methods.getInvoicePaymentTransactionID(jsonItems.payment_id,function(RetPaymentTrans){
							
										var order_id = '';
										if( parseInt(jsonItems.order_id) !==0 ){
											order_id ='O'+pad_with_zeroes(jsonItems.order_id,5).toString();
										}

										var quotation_no ="";
										if(jsonItems.quotation_id !=="" || typeof jsonItems.quotation_id !== 'undefined' ){
											quotation_no = pad_with_zeroes(jsonItems.quotation_id,5).toString();
										}

										var invoice_ref_no ="";
										if( jsonItems.invoice_id !=="" || typeof  jsonItems.invoice_id !== 'undefined'){
											invoice_ref_no = 'I'+pad_with_zeroes(jsonItems.invoice_id,5).toString();
										}

										var status ="0";
										if( parseInt(jsonItems.payment_id) !==0 && jsonItems.status !=""){
											status = jsonItems.status;
										}
										
										
										var sub_total = 0;
										var tax = 5/100;
										var tax_compute = 0;
										var total = 0;

										metaVal.forEach(function(val){
											sub_total += parseFloat(val.total);
										});

										tax_compute = parseFloat(tax)*parseFloat(sub_total);
										total = parseFloat(tax_compute)+parseFloat(sub_total);


										objvalues = {
											id           : jsonItems.group_id.toString(),
											invoice_no   : invoice_ref_no,
											order_no     : order_id,
											payment_no   : RetPaymentTrans,									
											account_id   : retAccountDetails.account_id,
											account_type : retAccountDetails.account_type,
											name         : retAccountDetails.name,
											logo         : retAccountDetails.logo,
											amount       : total.toFixed(2),
											status       : status,
											date         : jsonItems.date_updated
										};

										counter -= 1;	
										arrValues.push(objvalues);
										if (counter === 0){ 
											resolve(arrValues);
										}			

									});				
								});				
							});				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getInvoiceReceivedList",err); return_result(null); }	
};


methods.getInvoicePaymentTransactionID =  function (payment_id,return_result){
	
	try
	{				

		var query = `SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id =?`;
		DB.query(query, [payment_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	
		
					var payment_id = 'P'+pad_with_zeroes(retConverted[0].payment_transaction_id,5).toString();
					return_result(payment_id);

				});

			}
			else{ return_result(''); }

		});

	
	}
	catch(err)
	{ 
		console.log("getInvoicePaymentTransactionID",err); return_result(''); 
	}

};


methods.getInvoiceReceivedTotalAmount =  function (quotation_id,return_result){
	
	try
	{				

		var arrValues = [];
		var obj_return = {};

		var query = `SELECT * FROM jaze_jazepay_quotations_meta WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations_meta WHERE meta_key = 'quotation_id' AND meta_value=?)`;
		DB.query(query, [quotation_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	
	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var total = parseFloat(jsonItems.amount)*parseFloat(jsonItems.quantity);

							obj_return = {
								id          : jsonItems.group_id.toString(),
								product_id  : jsonItems.product_id,
								code        : jsonItems.code,
								description : jsonItems.description,
								amount      : jsonItems.amount,
								qty         : jsonItems.quantity,
								total       : total
							};

							counter -= 1;	
							arrValues.push(obj_return);
							if (counter === 0){ 
								resolve(arrValues);
							}	
						})
					}],
					function (result, current) {
						if (counter === 0){ 
						
 							return_result(result[0]);
						}
					});


				});

			}
			else{ return_result([]); }

		});

	
	}
	catch(err)
	{ 
		console.log("getInvoiceReceivedTotalAmount",err); return_result([]); 
	}

};

//------------------------------------------------------------------  jazepay payables invoice received list end   -----------------------------------------------------------------------------------//

//------------------------------------------------------------------  jazepay payables invoice received details start   -----------------------------------------------------------------------------------//

methods.getInvoiceReceiveDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id=? ";
		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getInvoiceReceiveDetailsMeta(jsonItems.group_id,function(retItems){

								var recParam = {account_id:jsonItems.profile_id};
								HelperGeneralJazenet.getAccountDetails(recParam,function(retReceiver){

									var senParam = {account_id:jsonItems.account_id};
									HelperGeneralJazenet.getAccountDetails(senParam,function(retSender){

										methods.getInvoicePaymentTransactionDetails(jsonItems.payment_id,function(RetPaymentTrans,payment_no){

											console.log(RetPaymentTrans);
							
											var invoice_no ="";
											if( jsonItems.invoice_id !=="" || typeof  jsonItems.invoice_id !== 'undefined'){
												invoice_no = 'I'+pad_with_zeroes(jsonItems.invoice_id,5).toString();
											}
											
											var ref_no = "";
											if(parseInt(jsonItems.invoice_ref_no) !==0){
												ref_no = 'O'+pad_with_zeroes(jsonItems.invoice_ref_no,5).toString();
											}

											var obj_rec = {
												account_id   : retReceiver.account_id,
												account_type : retReceiver.account_type,
												name         : retReceiver.name,
												logo         : retReceiver.logo
											};

											var obj_sen = {
												account_id   : retSender.account_id,
												account_type : retSender.account_type,
												name         : retSender.name,
												logo         : retSender.logo
											};

											var status ="0";
											if( parseInt(jsonItems.payment_id) !==0 && jsonItems.status !=""){
												status = jsonItems.status;
											}
									
											var tot_qty = "0";
											var sub_total = 0;
											var item_count = 0;

											retItems.forEach( function (value){	

												if(value.qty !=""){
													item_count += parseInt(value.qty);
													sub_total  += parseFloat(value.total);
												}						
										   		
											});

											tot_qty = item_count.toString();
											var tax = 0;

											if( jsonItems.tax !=="" || typeof  jsonItems.tax !== 'undefined'){
												tax = parseFloat(jsonItems.tax)/100;
											}

											var tax_compute = parseFloat(tax)*parseFloat(sub_total);
											var total = parseFloat(tax_compute)+parseFloat(sub_total);
											
											var obj_acc = {
												sub_total   : sub_total.toFixed(2),
												discount    : '0.00',
												tax         : parseInt(jsonItems.tax).toFixed(2),
												total_tax   : tax_compute.toFixed(2),
												total       : total.toFixed(2)
											};
																			
											var quot_request = {

												id                   : jsonItems.group_id.toString(),
												invoice_no           : invoice_no,
												ref_no               : ref_no,										
												receiver_details     : obj_rec,
												sender_details       : obj_sen,
												date                 : jsonItems.date_created,
												items                : retItems,
												total_qty            : tot_qty,
												status               : status,
												accounting           : obj_acc,
												payment_no           : payment_no,
												payment_details      : RetPaymentTrans

											};

			 								counter -= 1;	
					
											if (counter === 0){ 
												resolve(quot_request);
											}
									
										});
									});
								});

							});			
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getInvoiceReceiveDetails",err); return_result(null); }	
};



methods.getInvoicePaymentTransactionDetails =  function (payment_id,return_result){
	
	try
	{				

		var obj_payment_details = {
			id           : '',
			payment_type : '',
			payment_desc : ''
		};

		// payment_type
		// 0 = cod
		// 1 = jazePay
		// 2 = card
		// 3 = bank

		var query = `SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id =?`;
		DB.query(query, [payment_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){
	
					var payment_no = 'P'+pad_with_zeroes(retConverted[0].payment_transaction_id,5).toString();
			
					if(parseInt(retConverted[0].payment_type) === 0)
					{
						obj_payment_details = {
							id           : retConverted[0].payment_id,
							payment_type : retConverted[0].payment_type,
							payment_desc : 'Cash on Delivery'
						};

						return_result(obj_payment_details,payment_no);
					}
					else if(parseInt(retConverted[0].payment_type) === 1)
					{

						obj_payment_details = {
							id           : retConverted[0].payment_id,
							payment_type : retConverted[0].payment_type,
							payment_desc : 'Jazepay'
						};

						return_result(obj_payment_details,payment_no);

					}
					else if(parseInt(retConverted[0].payment_type) === 2)
					{
						

	
						var queryIn = `SELECT * FROM jaze_user_credit_card WHERE group_id =?`;
						DB.query(queryIn, [retConverted[0].payment_id], function (dataIn, error) {

							if (typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) !== 0)
							{ 
								methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

									obj_payment_details = {
										id           : retConverted[0].payment_id,
										payment_type : retConverted[0].payment_type,
										payment_desc : 'Card',
										card_name    : retConvertedIn[0].card_name,
										bank_name    : retConvertedIn[0].bank_name,
										card_no      : retConvertedIn[0].card_no,
										exp_month    : retConvertedIn[0].exp_month,
										exp_year     : retConvertedIn[0].exp_year,
									};

									return_result(obj_payment_details,payment_no);

													console.log(obj_payment_details,payment_no);	

								});
							}
							else
							{
								return_result(obj_payment_details,'');
							}
						});

					}
					else if(parseInt(retConverted[0].payment_type) === 3)
					{
						
						var queryIn = `SELECT * FROM jaze_user_credit_card WHERE group_id =?`;
						DB.query(queryIn, [retConverted[0].payment_id], function (dataIn, error) {

							if (typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) !== 0)
							{ 
								methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

									obj_payment_details = {
										id           : retConverted[0].payment_id,
										payment_type : retConverted[0].payment_type,
										payment_desc : 'Bank',
										account_name : retConvertedIn[0].holder_name,
										account_no   : retConvertedIn[0].account_no,
										bank_name    : retConvertedIn[0].bank_name,
										branch_name  : retConvertedIn[0].branch_name,
										iban_no      : retConvertedIn[0].iban_no,
										swift_code   : retConvertedIn[0].swift_code,
									};

									return_result(obj_payment_details,payment_no);

								});
							}
							else
							{
								return_result(obj_payment_details,'');
							}
						});

					}

				});

			}
			else
			{ 

				console.log('hello');
				return_result(obj_payment_details,''); 
			}

		});

	
	}
	catch(err)
	{ 
		console.log("getInvoicePaymentTransactionDetails",err); return_result(obj_payment_details,''); 
	}

};



methods.getInvoiceReceiveDetailsMeta =  function (quotation_id,return_result){
	
	try
	{				

		var arrValues = [];
		var obj_return = {};

		var total = 0;

		var query = `SELECT * FROM jaze_jazepay_quotations_meta WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations_meta WHERE meta_key = 'quotation_id' AND meta_value=?)`;
		DB.query(query, [quotation_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	
	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							total = parseFloat(jsonItems.amount)*parseFloat(jsonItems.quantity);

							obj_return = {
								id          : jsonItems.group_id.toString(),
								product_id  : jsonItems.product_id,
								code        : jsonItems.code,
								description : jsonItems.description,
								amount      : jsonItems.amount,
								qty         : jsonItems.quantity,
								total       : total.toFixed(2)
							};

							counter -= 1;	
							arrValues.push(obj_return);
							if (counter === 0){ 
								resolve(arrValues);
							}	
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return a.id - b.id; 
		                	});

 							return_result(result[0]);
						}
					});


				});

			}
			else{ return_result([]); }

		});

	
	}
	catch(err)
	{ 
		console.log("getInvoiceReceiveDetailsMeta",err); return_result([]); 
	}

};

//------------------------------------------------------------------  jazepay payables invoice received details end   -------------------------------------------------------------------------------------//


//------------------------------------------------------------------  jazepay payables payments made list start   ----------------------------------------------------------------------------------//

methods.getPaymentsMadeList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'account_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'status' AND meta_value ='1' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
	
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParam = {account_id:jsonItems.profile_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){
				
								var payment_id = 'P'+pad_with_zeroes(jsonItems.payment_transaction_id,5).toString();

								objvalues = {
									id             : jsonItems.group_id.toString(),
									payment_no     : payment_id,									
									account_id     : retAccountDetails.account_id,
									account_type   : retAccountDetails.account_type,
									name           : retAccountDetails.name,
									logo           : retAccountDetails.logo,
									amount         : parseFloat(jsonItems.total).toFixed(2),
									charges        : parseFloat(jsonItems.charges).toFixed(2),
									charge_percent : jsonItems.charge_percent,
									payment_type   : jsonItems.payment_type,
									status         : jsonItems.status,
									date           : jsonItems.date_created
								};

								counter -= 1;	
								arrValues.push(objvalues);
								if (counter === 0){ 
									resolve(arrValues);
								}			
								
							});				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getPaymentsMadeList",err); return_result(null); }	
};


//------------------------------------------------------------------  jazepay payables payments made list end   -----------------------------------------------------------------------------------//


//------------------------------------------------------------------  jazepay payables payments made details start   -----------------------------------------------------------------------------------//

methods.getPaymentsMadeDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};


		var query = "SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id=? ";
	
		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var senParam = {account_id:jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(senParam,function(retAccountDetails){

								methods.getInvoicePaymentTransactionDetails(jsonItems.group_id,function(RetPaymentTrans){

									methods.getPaymentsMadeDetailsMeta(jsonItems.group_id,function(retInvoice){
									
										var payment_id = 'P'+pad_with_zeroes(jsonItems.payment_transaction_id,5).toString();

										var invoice_no ="";
										if( retInvoice!=null){
											invoice_no = 'I'+pad_with_zeroes(retInvoice.invoice_id,5).toString();
										}
										
										objvalues = {
											id              : jsonItems.group_id.toString(),
											invoice_no      : invoice_no,									
											payment_no      : payment_id,									
											account_id      : retAccountDetails.account_id,
											account_type    : retAccountDetails.account_type,
											name            : retAccountDetails.name,
											logo            : retAccountDetails.logo,
											amount          : parseFloat(jsonItems.total).toFixed(2),
											charges         : parseFloat(jsonItems.charges).toFixed(2),
											charge_percent  : jsonItems.charge_percent,
											status          : jsonItems.status,
											date            : jsonItems.date_created,
											payment_details : RetPaymentTrans
										};

			
		 								counter -= 1;	
				
										if (counter === 0){ 
											resolve(objvalues);
										}
							
									});
								});
							});
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getPaymentsMadeDetails",err); return_result(null); }	
};



methods.getPaymentsMadeTransactionDetails =  function (payment_id,return_result){
	
	try
	{				

		var obj_payment_details = {
			id           : '',
			payment_type : '',
			payment_desc : ''
		};

		// payment_type
		// 0 = cod
		// 1 = jazePay
		// 2 = card
		// 3 = bank

		var query = `SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id =?`;
		DB.query(query, [payment_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	
		
					var payment_no = 'P'+pad_with_zeroes(retConverted[0].payment_transaction_id,5).toString();
			
					if(parseInt(retConverted[0].payment_type) === 0)
					{
						obj_payment_details = {
							id           : retConverted[0].payment_id,
							payment_type : retConverted[0].payment_type,
							payment_desc : 'Cash on Delivery'
						};

						return_result(obj_payment_details,payment_no);
					}
					else if(parseInt(retConverted[0].payment_type) === 1)
					{

						obj_payment_details = {
							id           : retConverted[0].payment_id,
							payment_type : retConverted[0].payment_type,
							payment_desc : 'Jazepay'
						};

						return_result(obj_payment_details,payment_no);

					}
					else if(parseInt(retConverted[0].payment_type) === 2)
					{
						
						var queryIn = `SELECT * FROM jaze_user_credit_card WHERE group_id =?`;
						DB.query(queryIn, [payment_id], function (dataIn, error) {

							if (typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) !== 0)
							{ 
								methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

									obj_payment_details = {
										id           : retConverted[0].payment_id,
										payment_type : retConverted[0].payment_type,
										payment_desc : 'Card',
										card_name    : retConvertedIn[0].card_name,
										bank_name    : retConvertedIn[0].bank_name,
										card_no      : retConvertedIn[0].card_no,
										exp_month    : retConvertedIn[0].exp_month,
										exp_year     : retConvertedIn[0].exp_year,
									};

									return_result(obj_payment_details,payment_no);

								});
							}
							else
							{
								return_result(obj_payment_details,'');
							}
						});

					}
					else if(parseInt(retConverted[0].payment_type) === 3)
					{
						
						var queryIn = `SELECT * FROM jaze_user_credit_card WHERE group_id =?`;
						DB.query(queryIn, [payment_id], function (dataIn, error) {

							if (typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) !== 0)
							{ 
								methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

									obj_payment_details = {
										id           : retConverted[0].payment_id,
										payment_type : retConverted[0].payment_type,
										payment_desc : 'Bank',
										account_name : retConvertedIn[0].holder_name,
										account_no   : retConvertedIn[0].account_no,
										bank_name    : retConvertedIn[0].bank_name,
										branch_name  : retConvertedIn[0].branch_name,
										iban_no      : retConvertedIn[0].iban_no,
										swift_code   : retConvertedIn[0].swift_code,
									};

									return_result(obj_payment_details,payment_no);

								});
							}
							else
							{
								return_result(obj_payment_details,'');
							}
						});

					}

				});

			}
			else
			{ 
				return_result(obj_payment_details,''); 
			}

		});

	}
	catch(err)
	{ 
		console.log("getPaymentsMadeTransactionDetails",err); return_result(obj_payment_details,''); 
	}

};



methods.getPaymentsMadeDetailsMeta =  function (quotation_id,return_result){

	
	try
	{				

		var arrValues = [];
		var obj_return = {};

		var total = 0;

		var query = `SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'payment_id' AND meta_value=?)`;
		DB.query(query, [quotation_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	
	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							counter -= 1;	
		
							if (counter === 0){ 
								resolve(jsonItems);
							}	
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
 							return_result(result[0]);
						}
					});


				});

			}
			else{ return_result(null); }

		});

	
	}
	catch(err)
	{ 
		console.log("getPaymentsMadeDetailsMeta",err); return_result(null); 
	}

};


//------------------------------------------------------------------  jazepay payables payments made details end   -----------------------------------------------------------------------------------//

//------------------------------------------------------------------  jazepay payables cash purchase list start   -----------------------------------------------------------------------------------//

methods.getCashPurchaseList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_cash_sale WHERE group_id IN (SELECT group_id FROM jaze_jazepay_cash_sale WHERE meta_key = 'profile_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_cash_sale WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
	
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParam = {account_id:jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){
				
								var document_no = pad_with_zeroes(jsonItems.cashsale_id,5).toString();
								
								objvalues = {
									id             : jsonItems.group_id.toString(),
									document_no    : document_no,									
									account_id     : retAccountDetails.account_id,
									account_type   : retAccountDetails.account_type,
									name           : retAccountDetails.name,
									logo           : retAccountDetails.logo,
									amount         : parseFloat(jsonItems.total).toFixed(2),
									payment_type   : jsonItems.payment_type,
									status         : jsonItems.status,
									date           : jsonItems.date_created
								};

								counter -= 1;	
								arrValues.push(objvalues);
								if (counter === 0){ 
									resolve(arrValues);
								}			
								
							});				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getCashPurchaseList",err); return_result(null); }	
};



//------------------------------------------------------------------  jazepay payables cash purchase list end   -----------------------------------------------------------------------------------//

//------------------------------------------------------------------  jazepay payables cash purchase details start   -----------------------------------------------------------------------------------//


methods.getCashPurchaseDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_cash_sale WHERE group_id =? ";

	
		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getCashPurchaseDetailsMeta(jsonItems.cashsale_id,function(retItems){

								var senParam = {account_id:jsonItems.account_id};
								HelperGeneralJazenet.getAccountDetails(senParam,function(retRec){

									var recParam = {account_id:jsonItems.profile_id};
									HelperGeneralJazenet.getAccountDetails(recParam,function(retSen){
					
										var document_no = pad_with_zeroes(jsonItems.cashsale_id,5).toString();

										var cs_no = 'CS'+pad_with_zeroes(jsonItems.group_id,5).toString();

										var obj_rec = {
											account_id   : retSen.account_id,
											account_type : retSen.account_type,
											name         : retSen.name,
											logo         : retSen.logo
										};

										var obj_sen = {
											account_id   : retRec.account_id,
											account_type : retRec.account_type,
											name         : retRec.name,
											logo         : retRec.logo
										};


										var tot_qty = "0";
										var sub_total = 0;
										var item_count = 0;

										retItems.forEach( function (value){	

											if(value.qty !=""){
												item_count += parseInt(value.qty);
												sub_total  += parseFloat(value.total);
											}						
									   		
										});

										var disc = 0;
										if(jsonItems.discount!=""){
										 	disc = parseFloat(jsonItems.discount)/100;
										}

										var disc_compute_new_sub = parseFloat(sub_total)-parseFloat(parseFloat(sub_total)*parseFloat(disc));

										tot_qty = item_count.toString();
										var tax = parseFloat(jsonItems.tax)/100;

										var tax_compute = parseFloat(tax)*parseFloat(disc_compute_new_sub);
										var total = parseFloat(tax_compute)+parseFloat(disc_compute_new_sub);
										
										var obj_acc = {
											sub_total   : sub_total.toFixed(2),
											discount    : parseFloat(jsonItems.discount).toFixed(2),
											tax         : parseFloat(jsonItems.tax).toFixed(2),
											total_tax   : tax_compute.toFixed(2),
											total       : total.toFixed(2)
										};

										
										objvalues = {
											id             : jsonItems.group_id.toString(),
											cash_purcahse_no : cs_no,
											document_no    : document_no,											
											sender_details       : obj_sen,
											receiver_details     : obj_rec,
											amount         : parseFloat(jsonItems.total).toFixed(2),
											items                : retItems,
											total_qty            : tot_qty,
											payment_type   : jsonItems.payment_type,
											status         : jsonItems.status,
											date           : jsonItems.date_created,
											accounting     : obj_acc
										};

										counter -= 1;	
										arrValues.push(objvalues);
										if (counter === 0){ 
											resolve(arrValues);
										}			
										
									});				
								});				
							});				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getCashPurchaseDetails",err); return_result(null); }	
};



methods.getCashPurchaseDetailsMeta =  function (group_id,return_result){
	
	try
	{				

		var arrValues = [];
		var obj_return = {};

		var total = 0;

		var query = `SELECT * FROM jaze_jazepay_cash_sale_meta WHERE group_id IN (SELECT group_id FROM jaze_jazepay_cash_sale_meta WHERE meta_key = 'cashsale_group_id' AND meta_value =?)`;
		DB.query(query, [group_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	
	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							total = parseFloat(jsonItems.amount)*parseFloat(jsonItems.quantity);

							obj_return = {
								id          : jsonItems.group_id.toString(),
								product_id  : jsonItems.product_id,
								code        : jsonItems.code,
								description : jsonItems.description,
								amount      : jsonItems.amount,
								qty         : jsonItems.quantity,
								total       : total.toFixed(2)
							};

							counter -= 1;	
							arrValues.push(obj_return);
							if (counter === 0){ 
								resolve(arrValues);
							}	
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return a.id - b.id; 
		                	});

 							return_result(result[0]);
						}
					});


				});

			}
			else{ return_result([]); }

		});

	
	}
	catch(err)
	{ 
		console.log("getCashPurchaseDetailsMeta",err); return_result([]); 
	}

};


//------------------------------------------------------------------  jazepay payables cash purchase details end   -----------------------------------------------------------------------------------//


methods.getJazepayHomeBalance =  function (arrParams,return_result){
	
	try
	{						

		var obj_return = {
			payment_balance : '0.00',
			payable_balance : '0.00',
			receivable_balance : '0.00'
		};

		var accParams = {account_id: arrParams.account_id};
		HelperGeneralJazenet.getAccountDetails(accParams,function(retAccount){

			methods.getJazePayBalance(retAccount.account_id,function(ret_payment){

				methods.getPayableBalance(retAccount.account_id,function(ret_payables){

					methods.getReceivableBalance(retAccount.account_id,function(ret_rec){

						obj_return.payment_balance = ret_payment;
						obj_return.payable_balance = ret_payables.toFixed(2);

						if(parseInt(retAccount.account_type)===3 || parseInt(retAccount.account_type)===4){
							obj_return.receivable_balance = ret_rec.toFixed(2);
						}
	

						return_result(obj_return);

					});
				});
			});

		});

	
	}
	catch(err)
	{ 
		console.log("getJazepayHomeBalance",err); return_result(obj_return); 
	}

};


methods.getPayableBalance =  function (account_id,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};


		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'type' AND meta_value ='2' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'invoice_status' AND meta_value ='0' ) ";
	
		DB.query(query,[account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							total_amount += parseFloat(jsonItems.total);			
							counter -= 1;	
		
							if (counter === 0){ 
								resolve(total_amount);
							}	

						})
					}],
					function (result, current) {
						if (counter === 0){ 
						

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(total_amount);
			}

		});

	}
	catch(err)
	{ console.log("getPayableBalance",err); return_result(total_amount); }	
};


methods.getReceivableBalance =  function (account_id,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};


		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'account_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'type' AND meta_value ='2' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'invoice_status' AND meta_value ='0' ) ";
	
		DB.query(query,[account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							total_amount += parseFloat(jsonItems.total);			
							counter -= 1;	
		
							if (counter === 0){ 
								resolve(total_amount);
							}	

						})
					}],
					function (result, current) {
						if (counter === 0){ 
						

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(total_amount);
			}

		});

	}
	catch(err)
	{ console.log("getReceivableBalance",err); return_result(total_amount); }	
};


//------------------------------------------------------------------  jazepay payables Receipt list start   -----------------------------------------------------------------------------------//

methods.getPayablesReceiptList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'account_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'status' AND meta_value ='1' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'receipt_id' AND meta_value !='0' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
	
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParam = {account_id:jsonItems.profile_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){
				
								var payment_id = pad_with_zeroes(jsonItems.group_id,5).toString();
								var receipt_id = pad_with_zeroes(jsonItems.receipt_id,5).toString();

								objvalues = {
									id             : jsonItems.group_id.toString(),
									receipt_no     : receipt_id,									
									payment_no     : payment_id,									
									account_id     : retAccountDetails.account_id,
									account_type   : retAccountDetails.account_type,
									name           : retAccountDetails.name,
									logo           : retAccountDetails.logo,
									amount         : parseFloat(jsonItems.total).toFixed(2),
									charges        : parseFloat(jsonItems.charges).toFixed(2),
									charge_percent : jsonItems.charge_percent,
									payment_type   : jsonItems.payment_type,
									status         : jsonItems.status,
									date           : jsonItems.receipt_date_created
								};

								counter -= 1;	
								arrValues.push(objvalues);
								if (counter === 0){ 
									resolve(arrValues);
								}			
								
							});				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getPayablesReceiptList",err); return_result(null); }	
};



//------------------------------------------------------------------  jazepay payables Receipt list end   -----------------------------------------------------------------------------------//

//------------------------------------------------------------------  jazepay payables Receipt details start   -----------------------------------------------------------------------------------//


methods.getPayableReceiptDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id =? ";

	
		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getPaymentsMadeTransactionDetails(jsonItems.group_id,function(retPayment){

								var senParam = {account_id:jsonItems.profile_id};
								HelperGeneralJazenet.getAccountDetails(senParam,function(retRec){

									var recParam = {account_id:jsonItems.account_id};
									HelperGeneralJazenet.getAccountDetails(recParam,function(retSen){
					
										var payment_id = 'P'+pad_with_zeroes(jsonItems.group_id,5).toString();
										var receipt_id = 'R'+pad_with_zeroes(jsonItems.receipt_id,5).toString();

										var obj_rec = {
											account_id   : retSen.account_id,
											account_type : retSen.account_type,
											name         : retSen.name,
											logo         : retSen.logo
										};

										var obj_sen = {
											account_id   : retRec.account_id,
											account_type : retRec.account_type,
											name         : retRec.name,
											logo         : retRec.logo
										};
										
										objvalues = {
											id             : jsonItems.group_id.toString(),
											receipt_no     : receipt_id,									
											payment_no     : payment_id,										
											sender_details       : obj_sen,
											receiver_details     : obj_rec,
											amount         : parseFloat(jsonItems.total).toFixed(2),
											payment_type   : jsonItems.payment_type,
											status         : jsonItems.status,
											date           : jsonItems.receipt_date_created,
											payment_details : retPayment
										};

										counter -= 1;	
										arrValues.push(objvalues);
										if (counter === 0){ 
											resolve(arrValues);
										}			
										
									});				
								});				
											
							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getPayableReceiptDetails",err); return_result(null); }	
};


//------------------------------------------------------------------  jazepay payables Receipt details end   -----------------------------------------------------------------------------------//



//------------------------------------------------------------------  jazepay payables credit notes list start   -----------------------------------------------------------------------------------//

methods.getPayablesCreditNotesList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_credit_notes WHERE group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'profile_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'status' AND meta_value ='1' ) ";
			//query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'paid_status' AND meta_value ='0' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
	
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParam = {account_id:jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){
				
								var invoice_no = pad_with_zeroes(jsonItems.invoice_id,5).toString();
								var credit_no = pad_with_zeroes(jsonItems.credit_id,5).toString();

								objvalues = {
									id             : jsonItems.group_id.toString(),
									credit_no      : credit_no,									
									invoice_no     : invoice_no,									
									account_id     : retAccountDetails.account_id,
									account_type   : retAccountDetails.account_type,
									name           : retAccountDetails.name,
									logo           : retAccountDetails.logo,
									amount         : parseFloat(jsonItems.total).toFixed(2),
									discount       : parseFloat(jsonItems.discount).toFixed(2),
									tax            : parseFloat(jsonItems.tax).toFixed(2),
									payment_type   : jsonItems.payment_type,
									status         : jsonItems.paid_status,
									date           : jsonItems.date_created
								};

								counter -= 1;	
								arrValues.push(objvalues);
								if (counter === 0){ 
									resolve(arrValues);
								}			
								
							});				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getPayablesCreditNotesList",err); return_result(null); }	
};



//------------------------------------------------------------------  jazepay payables credit notes list end   -----------------------------------------------------------------------------------//



//------------------------------------------------------------------  jazepay payables credit notes details start   -----------------------------------------------------------------------------------//


methods.getPayablesCreditNotesDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_credit_notes WHERE group_id =? ";

	
		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getPayablesCreditNotesDetailsMeta(jsonItems.credit_id,function(retItems){

								var senParam = {account_id:jsonItems.account_id};
								HelperGeneralJazenet.getAccountDetails(senParam,function(retRec){

									var recParam = {account_id:jsonItems.profile_id};
									HelperGeneralJazenet.getAccountDetails(recParam,function(retSen){
					
										var invoice_no = 'I'+pad_with_zeroes(jsonItems.invoice_id,5).toString();

										var credit_no = 'CN'+pad_with_zeroes(jsonItems.credit_id,5).toString();

										var obj_rec = {
											account_id   : retSen.account_id,
											account_type : retSen.account_type,
											name         : retSen.name,
											logo         : retSen.logo
										};

										var obj_sen = {
											account_id   : retRec.account_id,
											account_type : retRec.account_type,
											name         : retRec.name,
											logo         : retRec.logo
										};


										var tot_qty = "0";
										var sub_total = 0;
										var item_count = 0;

										retItems.forEach( function (value){	
											if(value.qty !=""){
												item_count += parseInt(value.qty);
												sub_total  += parseFloat(value.total);
											}						
										});

										var disc = 0;
										if(jsonItems.discount!=""){
										 	disc = parseFloat(jsonItems.discount)/100;
										}

										var disc_compute_new_sub = parseFloat(sub_total)-parseFloat(parseFloat(sub_total)*parseFloat(disc));

										tot_qty = item_count.toString();
										var tax = parseFloat(jsonItems.tax)/100;

										var tax_compute = parseFloat(tax)*parseFloat(disc_compute_new_sub);
										var total = parseFloat(tax_compute)+parseFloat(disc_compute_new_sub);
										
										var obj_acc = {
											sub_total   : sub_total.toFixed(2),
											discount    : parseFloat(jsonItems.discount).toFixed(2),
											tax         : parseFloat(jsonItems.tax).toFixed(2),
											total_tax   : tax_compute.toFixed(2),
											total       : total.toFixed(2)
										};

										
										objvalues = {
											id            : jsonItems.group_id.toString(),
											credit_no     : credit_no,
											invoice_no    : invoice_no,											
											sender_details       : obj_sen,
											receiver_details     : obj_rec,
											amount         : parseFloat(jsonItems.total).toFixed(2),
											items                : retItems,
											total_qty            : tot_qty,
											payment_type   : jsonItems.payment_type,
											status         : jsonItems.status,
											date           : jsonItems.date_created,
											accounting     : obj_acc
										};

										counter -= 1;	
										arrValues.push(objvalues);
										if (counter === 0){ 
											resolve(arrValues);
										}			
										
									});				
								});				
							});				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getPayablesCreditNotesDetails",err); return_result(null); }	
};



methods.getPayablesCreditNotesDetailsMeta =  function (group_id,return_result){
	
	try
	{				

		var arrValues = [];
		var obj_return = {};

		var total = 0;

		var query = `SELECT * FROM jaze_jazepay_credit_notes_meta WHERE group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes_meta WHERE meta_key = 'credit_note_id' AND meta_value =?)`;
		DB.query(query, [group_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	
	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							total = parseFloat(jsonItems.amount)*parseFloat(jsonItems.quantity);

							obj_return = {
								id          : jsonItems.group_id.toString(),
								product_id  : jsonItems.product_id,
								code        : jsonItems.code,
								description : jsonItems.description,
								amount      : jsonItems.amount,
								qty         : jsonItems.quantity,
								total       : total.toFixed(2)
							};

							counter -= 1;	
							arrValues.push(obj_return);
							if (counter === 0){ 
								resolve(arrValues);
							}	
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return a.id - b.id; 
		                	});

 							return_result(result[0]);
						}
					});


				});

			}
			else{ return_result([]); }

		});

	
	}
	catch(err)
	{ 
		console.log("getPayablesCreditNotesDetailsMeta",err); return_result([]); 
	}

};


//------------------------------------------------------------------  jazepay payables credit notes details end   -----------------------------------------------------------------------------------//

//------------------------------------------------------------------  jazepay payables pay invoice details module start -----------------------------------------------------------------------------------//


methods.getPayablesInvoicePayDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var arrPayments = [];
		var objvalues  = {};

		var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id=? ";
		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getInvoiceReceiveDetailsMeta(jsonItems.group_id,function(retItems){

								var senParam = {account_id:jsonItems.profile_id};
								HelperGeneralJazenet.getAccountDetails(senParam,function(retSender){

									var senParam = {account_id:jsonItems.account_id};
									HelperGeneralJazenet.getAccountDetails(senParam,function(retReceiver){

										var payParam = {account_id:jsonItems.profile_id,flag:3};
										methods.getTransactionNumber(payParam,function(ret_payment_no){

											methods.getJazePayBalance(jsonItems.profile_id,function(retJazepay){

												methods.getInvoicePaymentCard(jsonItems.profile_id,function(retCard){

													methods.getInvoiceNumberForPayment(jsonItems.profile_id,function(retInvoiceNumber){


														var obj_jazepay = {
															id   : '0',
															type : '1',
															desc : 'jazepay',
															balance : retJazepay
														};

																																
														var obj_sen = {
															account_id   : retSender.account_id,
															account_type : retSender.account_type,
															name         : retSender.name,
															logo         : retSender.logo
														};

														var obj_rec = {
															account_id   : retReceiver.account_id,
															account_type : retReceiver.account_type,
															name         : retReceiver.name,
															logo         : retReceiver.logo
														};

														var status ="0";
														if( parseInt(jsonItems.payment_id) !==0 && jsonItems.status !=""){
															status = jsonItems.status;
														}
												
														var tot_qty = "0";
														var sub_total = 0;
													
														retItems.forEach( function (value){	
															if(value.qty !=""){	
																sub_total  += parseFloat(value.total);
															}															   	
														});

											
														var tax = 0;

														if( jsonItems.tax !=="" || typeof  jsonItems.tax !== 'undefined'){
															tax = parseFloat(jsonItems.tax)/100;
														}

														var tax_compute = parseFloat(tax)*parseFloat(sub_total);
														var total = parseFloat(tax_compute)+parseFloat(sub_total);
																		
										
														var payment_no = 'P'+pad_with_zeroes(ret_payment_no.quo_req_id,5).toString();

														var quot_request = {

															id                   : jsonItems.group_id.toString(),
															payment_no_req_id    : ret_payment_no.quo_req_id.toString(),
															payment_no           : payment_no,
															ref_no               : retInvoiceNumber,																					
															sender_details       : obj_sen,
															receiver_details     : obj_rec,
															total                : total.toFixed(2),
															date                 : jsonItems.date_created,								
															status               : status,								
															jaze_pay             : obj_jazepay,
															credit_card          : retCard,

														};

						 								counter -= 1;	
								
														if (counter === 0){ 
															resolve(quot_request);
														}
											
													});
												});
											});
										});

									});

								});
					

							});			
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getPayablesInvoicePayDetails",err); return_result(null); }	
};


methods.getInvoiceNumberForPayment =  function (account_id,return_result){
	
	try
	{				

		var queryIn = ` SELECT t1.* FROM(
		SELECT group_id,
				(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id
				FROM  jaze_jazepay_invoice_payments	
			) AS t1 WHERE account_id != '' 
		AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'account_id' AND meta_value=?)
		AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'payment_id' AND meta_value !='0')`;

		var inv_no = 'I'+pad_with_zeroes(1,5).toString();

		DB.query(queryIn, [account_id], function (dataIn, error) {

		

			if (typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) !== 0)
			{ 

				var invoices = parseFloat(dataIn.length)+1;

				inv_no = 'I'+pad_with_zeroes(invoices,5).toString();
				
				return_result(inv_no);

			
			}
			else
			{
				
				return_result(inv_no);
			}
		});

	
	}
	catch(err)
	{ 
		console.log("getInvoiceNumberForPayment",err); return_result(inv_no); 
	}

};

methods.getInvoicePaymentCard =  function (account_id,return_result){
	
	try
	{				


	var queryIn = ` SELECT * FROM jaze_user_credit_card WHERE group_id IN (SELECT group_id FROM jaze_user_credit_card WHERE meta_key='account_id' AND meta_value=?)
 		AND group_id IN (SELECT group_id FROM jaze_user_credit_card WHERE meta_key='is_default' AND meta_value='1')`;
		DB.query(queryIn, [account_id], function (dataIn, error) {

			if (typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) !== 0)
			{ 
				methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

					obj_payment_details = {
						id           : retConvertedIn[0].group_id.toString(),
						payment_type : '2',
						payment_desc : 'Card',
						card_name    : retConvertedIn[0].card_name,
						bank_name    : retConvertedIn[0].bank_name,
						card_no      : retConvertedIn[0].card_no,
						card_logo    : '',
						exp_month    : retConvertedIn[0].exp_month,
						exp_year     : retConvertedIn[0].exp_year,
						csv          : retConvertedIn[0].csv,
					};

					return_result(obj_payment_details);

				});
			}
			else
			{
				
				var queryIn = ` SELECT * FROM jaze_user_credit_card WHERE group_id IN (SELECT group_id FROM jaze_user_credit_card WHERE meta_key='account_id' AND meta_value=?)`;
				DB.query(queryIn, [account_id], function (dataIn, error) {

					if (typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) !== 0)
					{ 

						methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

							obj_payment_details = {
								id           : retConvertedIn[0].group_id.toString(),
								payment_type : '2',
								payment_desc : 'Card',
								card_name    : retConvertedIn[0].card_name,
								bank_name    : retConvertedIn[0].bank_name,
								card_no      : retConvertedIn[0].card_no,
								card_logo    : '',
								exp_month    : retConvertedIn[0].exp_month,
								exp_year     : retConvertedIn[0].exp_year,
								csv          : retConvertedIn[0].csv,
							};

							return_result(obj_payment_details);

						});
					}
					else{return_result({});}
				});
			}
		});

	
	}
	catch(err)
	{ 
		console.log("getInvoicePaymentCard",err); return_result({}); 
	}

};


//------------------------------------------------------------------  jazepay payables pay invoice details module end -----------------------------------------------------------------------------------//


//------------------------------------------------------------------  jazepay payables pay invoice module start -----------------------------------------------------------------------------------//

methods.payablesPayInvoice =  function (arrParams,return_result){
	
	try
	{				


		var queryQuotation = `SELECT *  FROM jaze_jazepay_quotations WHERE group_id =?`;
		
		DB.query(queryQuotation, [arrParams.id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 	

				methods.convertMultiMetaArray(data, function(retConverted){	

					var payParam = {account_id:retConverted[0].profile_id,flag:3};
					methods.getTransactionNumber(payParam,function(ret_payment_no){

						var payment_no = ret_payment_no.quo_req_id;
						if(parseInt(arrParams.payment_no_req_id) === parseInt(ret_payment_no.quo_req_id))
						{
							payment_no = arrParams.payment_no_req_id;
						}


						var queryInvPayment = " SELECT count(group_id) + 1 as  receipt_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'account_id'  AND meta_value = '"+retConverted[0].account_id+"' ";

						DB.query(queryInvPayment,null, function(receipt, error){ 

							if(typeof receipt !== 'undefined' && receipt !== null  && parseInt(receipt.length) > 0)
							{

								var charges = parseFloat(retConverted[0].total) * 5 / 100;
								var obj_insert_meta = {
									account_id       		: retConverted[0].profile_id,
									profile_id       		: retConverted[0].account_id,
									invoice_id       		: retConverted[0].group_id,
									total       			: retConverted[0].total,
									payment_type       		: arrParams.payment_type,
									payment_id       		: arrParams.payment_id,
									payment_transaction_id  : payment_no,
									charges       			: charges,
									charge_percent       	: '5',
									date_created	 		: newdateForm,
									status	 	 			: '1',
									// receipt_id	 	 		: receipt[0].receipt_id,
									// receipt_date_created	: moment_date
								};

								HelperRestriction.getNewGroupId('jaze_jazepay_invoice_payments', function (group_id) {

									if(parseInt(group_id) !== 0){

										HelperRestriction.insertTBQuery('jaze_jazepay_invoice_payments',group_id,obj_insert_meta, function (meta_id) {
											
											if(parseInt(meta_id) !== 0)
											{	
												var newArrParams = { account_id: retConverted[0].account_id, profile_id:retConverted[0].profile_id, amount:retConverted[0].total,charges:charges, payment_type:arrParams.payment_type};
												methods.processUpdateQUotation(newArrParams,group_id,retConverted[0].group_id,function(retStatus){
													return_result(retStatus); 

												});							
											}
											else
											{ return_result(0); }
										});
									}	
									else
									{ return_result(0); }
								});
							}
							else
							{ return_result(0); }
						});

					});
				});

			}	
			else
			{
		 		return_result(2);
		 	}

		});





	}
	catch(err)
	{ 
		console.log("payablesPayInvoice",err); return_result(0); 
	}

};



methods.processUpdateQUotation  = function(arrParams,payment_id,quotation_id,return_result){
	try
	{



		var updateQuer = `update jaze_jazepay_quotations SET meta_value =? WHERE meta_key =? AND group_id =?`;

		var	arrSender = {
			invoice_status       : 1,
			invoice_date_updated : newdateForm,
			balance              : 0,
			balance_date_updated : newdateForm,
			payment_id           : payment_id,
			status               : 1
		};

		var arrLengthS = Object.keys(arrSender).length;
		var num_rows_sender = 0;
		for (var keyS in arrSender) 
	 	{	
			DB.query(updateQuer, [arrSender[keyS],keyS,quotation_id], function(dataS, error){
	 		num_rows_sender += dataS.affectedRows;
				if(parseInt(num_rows_sender) === arrLengthS){
					
					//insert statment.
					methods.___insertStatement(arrParams,payment_id,function(retStatus){

						return_result(retStatus);
					});


					if(parseInt(arrParams.payment_type) === 1)
					{
						console.log(arrParams);
						methods.procJazePayAccountForPayment(arrParams,function(retPayStats){});
					}
			
				}

			});
		
		}


	}
	catch(err)
	{ console.log("processUpdateQUotation",err); return_result(0); }
};


methods.___insertStatement  = function(arrParams,payment_id, return_result){
	try
	{
		if(arrParams !== null)
		{
			var data_list = [];

			var obj_insert_meta = {
					account_id       : arrParams.account_id,
					type       		: '4',
					category_id     : payment_id,
					category_type   : '1',
					charges       	: arrParams.charges,
					amount       	: arrParams.amount,
					date_created	: newdateForm,
					status	 	 	: '1'
				};

				data_list.push(obj_insert_meta);

				var obj_insert_meta = {
					account_id       : arrParams.profile_id,
					type       		: '4',
					category_id     : payment_id,
					category_type   : '2',
					charges       	: arrParams.charges,
					amount       	: arrParams.amount,
					date_created	: newdateForm,
					status	 	 	: '1'
				};

				data_list.push(obj_insert_meta);

				async.forEach(Object.keys(data_list), function (i, callback){ 
						
					setTimeout(() => {

						HelperRestriction.getNewGroupId('jaze_jazepay_statements', function (group_id) {

							if(parseInt(group_id) !== 0){

								HelperRestriction.insertTBQuery('jaze_jazepay_statements',group_id,data_list[i], function (meta_id) {
									
									if(parseInt(meta_id) !== 0)
									{ callback(); }
								});
							}
						});

					}, 1000 * i );
				},function(err) {
					return_result(1);
				});
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{ console.log("___insertInvoicePayments",err); return_result(0); }
};



methods.procJazePayAccountForPayment =  function (arrParams,return_result){
	
	try
	{				
		

		methods.getJazePayBalance(arrParams.profile_id,function(retSenBal){

			var new_sender_balance = parseFloat( parseFloat(retSenBal)-parseFloat(arrParams.amount) ).toFixed(2);

			methods.getJazePayBalance(arrParams.account_id,function(retRecBal){

				var new_receiver_balance = parseFloat( parseFloat(retRecBal)+parseFloat(arrParams.amount) ).toFixed(2);

				var updateQuer = `update jaze_jazepay_accounts SET meta_value =? WHERE meta_key =? AND group_id =?`;

				var query = `SELECT *  FROM jaze_jazepay_accounts WHERE 
					group_id in (SELECT group_id  FROM jaze_jazepay_accounts WHERE  meta_key = 'account_id' and meta_value =?)`;
	
				DB.query(query, [arrParams.profile_id], function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{ 
						
						var	arrSender = {
							current_balance  : new_sender_balance,
							date_updated     : newdateForm
						};

						var	arrRec = {
							current_balance  : new_receiver_balance,
							date_updated     : newdateForm
						};

						var arrLengthS = Object.keys(arrSender).length;
						var num_rows_sender = 0;
						for (var keyS in arrSender) 
					 	{	
							DB.query(updateQuer, [arrSender[keyS],keyS,data[0].group_id], function(dataS, error){
					 		num_rows_sender += dataS.affectedRows;
								if(parseInt(num_rows_sender) === arrLengthS){
									
									DB.query(query, [arrParams.account_id], function (dataIn, error) {

										if (typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) !== 0)
										{ 
											//update if receiver has a account
											var arrLengthR = Object.keys(arrRec).length;
											var num_rows_rec = 0;
											for (var keyR in arrRec) 
										 	{	
												DB.query(updateQuer, [arrRec[keyR],keyR,dataIn[0].group_id], function(dataR, error){
										 		num_rows_rec += dataR.affectedRows;
													if(parseInt(num_rows_rec) === arrLengthR){
								
														return_result(1);
														
													}
												});
											
											}

										}
										else
										{

											//insert if receiver has no account
											var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_accounts";
											var lastGrpId = 0;

											var arrInsert = {
												account_id       : arrParams.account_id,
												current_balance  : new_receiver_balance,
												date_created     : standardDT,
												date_updated     : standardDT,
												status           : 1
											}

											var arrLength = Object.keys(arrInsert).length;
											var num_rows_insert = 0;

											methods.getLastGroupId(queGroupMax, function(queGrpRes){

												lastGrpId = queGrpRes[0].lastGrpId;
												var plusOneGroupID = lastGrpId+1;

												var querInsert = `insert into jaze_jazepay_accounts (group_id, meta_key, meta_value) VALUES (?,?,?)`;
												for (var key in arrInsert) 
											 	{
													DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
														num_rows_insert += data.affectedRows;
														if(parseInt(num_rows_insert) === arrLength){
															return_result(1);
														}
													});

											  	};

											});
										}

									});
									
								}

							});
						
						}

					}
			
				});

	
			});

		});
	
	}
	catch(err)
	{ 
		console.log("procJazePayAccountForPayment",err); return_result(0); 
	}

};

//------------------------------------------------------------------  jazepay payables pay invoice module end -----------------------------------------------------------------------------------//



methods.getJazepayUnpaidPayments =  function (arrParams,return_result){
	
	try
	{				

		var arrAccountID = [];
		var objvalues  = {};

		var query = `SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key='profile_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key='payment_id' AND meta_value ='0')
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key='invoice_status' AND meta_value ='0')
			GROUP BY group_id`;

		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							if(jsonItems.account_id){
								arrAccountID.push(jsonItems.account_id);
							}
					
							counter -= 1;	
							if (counter === 0){ 
								resolve(arrAccountID);
							}			
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 



							var filteredArray = removeDuplicates(result[0]);

							if(filteredArray.length > 0)
							{

								var counterInn = filteredArray.length;		
								var jsonItemsInn = Object.values(filteredArray);

								var inv_count = 0;
								
								var arrResult = [];

								promiseForeach.each(jsonItemsInn,[function (jsonItemsInn) {
									return new Promise(function (resolveInn, reject) {						

										var accParam = {account_id: jsonItemsInn};
										HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){

											var querParam = { account_id:jsonItemsInn, profile_id:arrParams.account_id };

											methods.getJazepayUnpaidPaymentsTransactions(querParam,function(retTransactions,count){

												var balance   = 0;
												if(retTransactions!=null){
													retTransactions.forEach(function(val){
														balance += parseFloat(val.balance);
													});
												}
								
												objvalues = {
													account_id     : retAccountDetails.account_id,
													account_type   : retAccountDetails.account_type,
													name           : retAccountDetails.name,
													logo           : retAccountDetails.logo,
													category       : retAccountDetails.category,
													total_invoices : count.toString(),
													total          : balance.toFixed(2),
													amount         : balance.toFixed(2),
													transactions   : retTransactions,
												}

												arrResult.push(objvalues);

												counterInn -= 1;	
												if (counterInn === 0){ 
													resolveInn(arrResult);
												}

											});			
										});

									})
								}],
								function (resultInn, current) {
									if (counterInn === 0){ 							
										return_result(resultInn[0]);
									}
								});
							}
							else
							{
								return_result(null);
							}				

						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getJazepayUnpaidPayments",err); return_result(null); }	
};


methods.getJazepayUnpaidPaymentsTransactions =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};
		var count = 0;

		var query = `SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key='account_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key='profile_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key='payment_id' AND meta_value ='0')
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key='invoice_status' AND meta_value ='0')`;

		DB.query(query,[arrParams.account_id,arrParams.profile_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							count++;
							var inv_no = 'I'+pad_with_zeroes(jsonItems.invoice_id,5).toString();

							var status = '0';
							if(jsonItems.status !=""){
								status = jsonItems.status.toString();
							}
							objvalues = {
								id         : jsonItems.group_id.toString(),
								invoice_no : inv_no,
								amount     : jsonItems.total,
								balance    : jsonItems.balance,
								status     : status
							}

							arrValues.push(objvalues);
							counter -= 1;	
							if (counter === 0){ 
								resolve(arrValues);
							}

						
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
					
			  				return_result(result[0],count);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getJazepayUnpaidPaymentsTransactions",err); return_result(null); }	
};




//------------------------------------------------------------------  jazepay receivable debtor maintenance start   --------------------------------------------------------------------------------//

methods.receivableDebtorsMaintenance =  function (arrParams,return_result){
	
	try
	{			

		var current_m = now.getMonth()+1;
		var current_y = now.getFullYear();


		query = `SELECT t1.* FROM(
				SELECT group_id,
					(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
					(SELECT c.meta_value FROM jaze_jazepay_quotations c WHERE c.meta_key = 'profile_id' AND c.group_id = jaze_jazepay_quotations.group_id) AS profile_id,
					(SELECT c.meta_value FROM jaze_jazepay_quotations c WHERE c.meta_key = 'date_created' AND c.group_id = jaze_jazepay_quotations.group_id) AS date_created
				FROM  jaze_jazepay_quotations
			) AS t1 WHERE account_id != '' AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'account_id' AND meta_value=?) `;

		if((arrParams.month !="" && arrParams.year !="") && (arrParams.from_date =="" && arrParams.to_date =="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+arrParams.month+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+arrParams.year+"') ";
		}
		else if((arrParams.month =="" && arrParams.year =="") && (arrParams.from_date !="" && arrParams.to_date !="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+arrParams.from_date+"' AND '"+arrParams.to_date+"')  ";
		}
		else 
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+current_m+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+current_y+"') ";
		}

		query += " GROUP BY profile_id ORDER BY date_created DESC";



		var arrAccountID = [];

		DB.query(query, [arrParams.account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 

				var counter = data.length;		
				var jsonItems = Object.values(data);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {		

						arrAccountID.push(jsonItems.profile_id);
						counter -= 1;	
						if (counter === 0){ 
							resolve(arrAccountID);
						}

					})
				}],
				function (result, current) {
					if (counter === 0){ 
	
						var objvalues = {};
						var arrValues = [];
	
						if(result[0].length > 0)
						{

							var counterInn = result[0].length;		
							var jsonItemsInn = Object.values(result[0]);

							var inv_count = 0;
							
							var arrResult = [];



							promiseForeach.each(jsonItemsInn,[function (jsonItemsInn) {
								return new Promise(function (resolveInn, reject) {	



									var creParams = {profile_id: jsonItemsInn, account_id: arrParams.account_id};
									methods.getFirstCreditorTransactionDate(creParams, function(lastTranDate){	

										var accParams = {account_id: jsonItemsInn};
										HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

											var regParam = {profile_id: jsonItemsInn, account_id: arrParams.account_id};
											methods.getCreditorRegistration(regParam, function(retRegistration){	

												var addParam1 = {profile_id: jsonItemsInn, account_id: arrParams.account_id, type: 1};
												methods.getCreditorAddressDetails(addParam1, function(retAdd1){	

										
													var addParam2 = {profile_id: jsonItemsInn, account_id: arrParams.account_id, type: 2};
													methods.getCreditorAddressDetails(addParam2, function(retAdd2){


														var addParam3 = {profile_id: jsonItemsInn, account_id: arrParams.account_id, type: 3};
														methods.getCreditorAddressDetails(addParam3, function(retAdd3){

															if(retAccountDetails!=null)
															{

																var ret_reg = {};
																var ret_status = '0';
																if(retRegistration!=null){
																	ret_status = '1';
																	ret_reg = retRegistration;
																}

																var ret_add1 = {};
																var ret_add1_status = '0';
																if(retAdd1!=null){
																	ret_add1_status = '1';
																	ret_add1 = retAdd1;
																}

																var ret_add2 = {};
																var ret_add2_status = '0';
																if(retAdd2!=null){
																	ret_add2_status = '1';
																	ret_add2 = retAdd2;
																}

																var ret_add3 = {};
																var ret_add3_status = '0';
																if(retAdd3!=null){
																	ret_add3_status = '1';
																	ret_add3 = retAdd3;
																}

																objvalues = {
											
																	account_id   : retAccountDetails.account_id,
																	account_type : retAccountDetails.account_type,
																	name         : retAccountDetails.name,
																	logo         : retAccountDetails.logo,
																	date         : lastTranDate,
																	registration_status     : ret_status,
																	physical_address_status : ret_add1_status,
																	delivery_address_status : ret_add2_status,
																	postal_address_status   : ret_add3_status,
																	registration     : ret_reg,
																	physical_address : ret_add1,
																	delivery_address : ret_add2,
																	postal_address   : ret_add3

																};

																counterInn -= 1;	
																arrValues.push(objvalues);
												

															}else{	counterInn -= 1;}


																			

															if (counterInn === 0){ 
																resolveInn(arrValues);
															}	

														});

													});

												});

											});

										});

									});

								})
							}],
							function (resultInn, current) {
								if (counterInn === 0){ 							
					
									resultInn[0].sort(function(a, b){ 
										return new Date(b.date) - new Date(a.date); 
									});

									return_result(resultInn[0]);	
								}
							});
						}
						else
						{
							return_result(null);
						}
					}
				});

			}
			else{return_result(null); }

		});
	
	}
	catch(err)
	{ 
		console.log("receivableDebtorsMaintenance",err); return_result(null); 
	}

};

//------------------------------------------------------------------  jazepay receivable debtor maintenance end   --------------------------------------------------------------------------------//

//------------------------------------------------------------------  jazepay receivable debtor maintenance registration start   --------------------------------------------------------------------------------//

methods.procDebtorRegistration =  function (arrParams,return_result){
	
	try
	{			

		var query = `SELECT * FROM jaze_jazepay_creditor_debtor_settings WHERE group_id IN (SELECT group_id FROM jaze_jazepay_creditor_debtor_settings WHERE meta_key = 'profile_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_jazepay_creditor_debtor_settings WHERE meta_key = 'account_id' AND meta_value =?)`;

		DB.query(query, [arrParams.debtor_account_id,arrParams.account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				
				var updateQuer = `update jaze_jazepay_creditor_debtor_settings SET meta_value =? WHERE meta_key =? AND group_id =?`;
				var	arrUpdate = {
					company_registration : arrParams.registration_no,
					vat_number           : arrParams.vat_no,
					credit_limit         : arrParams.credit_limit,
					payment_terms        : arrParams.payment_terms,
					discount             : arrParams.discount,
					contact_person       : arrParams.contact_person,
					date_modified        : standardDT
				};

				for (var keyS in arrUpdate) 
			 	{	
 					if(arrUpdate[keyS] !='')
 					{
						DB.query(updateQuer, [arrUpdate[keyS],keyS,data[0].group_id], function(dataS, error){
			
							if(parseInt(dataS.affectedRows) > 0){					
								return_result(1);
							}

						});
 					}
				
				}

			}
			else
			{ 

				var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_creditor_debtor_settings";
				var lastGrpId = 0;

				var arrInsert = {
					account_id           : arrParams.account_id,
					profile_id           : arrParams.debtor_account_id,
					company_registration : arrParams.registration_no,
					vat_number           : arrParams.vat_no,
					credit_limit         : arrParams.credit_limit,
					payment_terms        : arrParams.payment_terms,
					discount             : arrParams.discount,
					contact_person       : arrParams.contact_person,
					date_modified        : standardDT,
					date_created         : standardDT,
					statis               : 1
				}

				var arrLength = Object.keys(arrInsert).length;
				var num_rows_insert = 0;

				methods.getLastGroupId(queGroupMax, function(queGrpRes){

					lastGrpId = queGrpRes[0].lastGrpId;
					var plusOneGroupID = lastGrpId+1;

					var querInsert = `insert into jaze_jazepay_creditor_debtor_settings (group_id, meta_key, meta_value) VALUES (?,?,?)`;
					for (var key in arrInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
							num_rows_insert += data.affectedRows;
							if(parseInt(num_rows_insert) === arrLength){
								return_result(1);
								
							}
						});						

				  	};


				});

			}

		});
	
	}
	catch(err)
	{ 
		console.log("procDebtorRegistration",err); return_result(null); 
	}

};

//------------------------------------------------------------------   jazepay receivable debtor maintenance registration end   --------------------------------------------------------------------------------//


//------------------------------------------------------------------ Jazepay receivable debtors address update start  -----------------------------------------------------------------------------------------------//

methods.procDebtorAddress =  function (arrParams,return_result){
	
	try
	{			

		var query = `SELECT * FROM jaze_jazepay_address WHERE group_id IN (SELECT group_id FROM jaze_jazepay_address WHERE meta_key = 'account_id' AND meta_value =?)
		AND group_id IN (SELECT group_id FROM jaze_jazepay_address WHERE meta_key = 'profile_id' AND meta_value =?)
		AND group_id IN (SELECT group_id FROM jaze_jazepay_address WHERE meta_key = 'type_id' AND meta_value =?)`;

		DB.query(query, [arrParams.account_id,arrParams.debtor_account_id,arrParams.flag], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				
				var updateQuer = `update jaze_jazepay_address SET meta_value =? WHERE meta_key =? AND group_id =?`;
				var	arrUpdate = {

					country_id       : arrParams.country_id,
					state_id         : arrParams.state_id,
					city_id          : arrParams.city_id,
					area_id          : arrParams.area_id,
					street_name      : arrParams.street_name,
					street_number    : arrParams.street_number,
					building_name    : arrParams.building_name,
					building_number  : arrParams.building_number
				};

				for (var keyS in arrUpdate) 
			 	{	
 					if(arrUpdate[keyS] !='')
 					{
						DB.query(updateQuer, [arrUpdate[keyS],keyS,data[0].group_id], function(dataS, error){
			
							if(parseInt(dataS.affectedRows) > 0){					
								return_result(1);
							}

						});
 					}
				}

			}
			else
			{ 

				var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_address";
				var lastGrpId = 0;

				var arrInsert = {
					account_id       : arrParams.account_id,
					profile_id       : arrParams.debtor_account_id,
					country_id       : arrParams.country_id,
					state_id         : arrParams.state_id,
					city_id          : arrParams.city_id,
					area_id          : arrParams.area_id,
					street_name      : arrParams.street_name,
					street_number    : arrParams.street_number,
					building_name    : arrParams.building_name,
					building_number  : arrParams.building_number,
					type_id          : arrParams.flag
				}


				var arrLength = Object.keys(arrInsert).length;
				var num_rows_insert = 0;

				methods.getLastGroupId(queGroupMax, function(queGrpRes){

					lastGrpId = queGrpRes[0].lastGrpId;
					var plusOneGroupID = lastGrpId+1;

					var querInsert = `insert into jaze_jazepay_address (group_id, meta_key, meta_value) VALUES (?,?,?)`;
					for (var key in arrInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
							num_rows_insert += data.affectedRows;
							if(parseInt(num_rows_insert) === arrLength){
								return_result(1);
								
							}
						});

				  	};


				});

			}

		});
	
	}
	catch(err)
	{ 
		console.log("procDebtorAddress",err); return_result(null); 
	}

};

//------------------------------------------------------------------ Jazepay receivable debtors address update end   -----------------------------------------------------------------------------------------------//

//------------------------------------------------------------------ jazepay receivable debtors list start  --------------------------------------------------------------------------------//

methods.getReceivableDebtorsList =  function (arrParams,return_result){
	
	try
	{			

		var current_m = now.getMonth()+1;
		var current_y = now.getFullYear();

		query = `SELECT t1.* FROM(
				SELECT group_id,
					(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
					(SELECT c.meta_value FROM jaze_jazepay_quotations c WHERE c.meta_key = 'profile_id' AND c.group_id = jaze_jazepay_quotations.group_id) AS profile_id,
					(SELECT c.meta_value FROM jaze_jazepay_quotations c WHERE c.meta_key = 'date_created' AND c.group_id = jaze_jazepay_quotations.group_id) AS date_created
				FROM  jaze_jazepay_quotations
			) AS t1 WHERE account_id != '' AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'account_id' AND meta_value=?) `;

		if((arrParams.month !="" && arrParams.year !="") && (arrParams.from_date =="" && arrParams.to_date =="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+arrParams.month+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+arrParams.year+"') ";
		}
		else if((arrParams.month =="" && arrParams.year =="") && (arrParams.from_date !="" && arrParams.to_date !="") )
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+arrParams.from_date+"' AND '"+arrParams.to_date+"')  ";
		}
		else 
		{
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND MONTH(meta_value)='"+current_m+"') ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND YEAR(meta_value)='"+current_y+"') ";
		}

		query += " GROUP BY profile_id ORDER BY date_created DESC";


		var objvalues = {};
		var arrValues = [];

		var total_amount = 0;

		DB.query(query, [arrParams.account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 

	
				var counter = data.length;		
				var jsonItems = Object.values(data);

				promiseForeach.each(jsonItems,[function (jsonItems) {
					return new Promise(function (resolve, reject) {	

			
						var creParams = {profile_id: jsonItems.profile_id, account_id: jsonItems.account_id};
						methods.getDebtorsTransactions(creParams, function(date,retSummary){

							var accParams = {account_id: jsonItems.profile_id};
							HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	

								if(retAccountDetails!=null)
								{
									total_amount += parseFloat(retSummary);

									objvalues = {
				
										account_id   : retAccountDetails.account_id,
										account_type : retAccountDetails.account_type,
										name         : retAccountDetails.name,
										logo         : retAccountDetails.logo,
										amount       : retSummary,
										date         : date

									};

									counter -= 1;	
									arrValues.push(objvalues);
					

								}else{	counter -= 1;}

								if (counter === 0){ 
									resolve(arrValues);
								}	

							});
						});
					})
				}],
				function (result, current) {
					if (counter === 0){

					  	result[0].sort(function(a, b){ 
	                   	 	return new Date(a.date) - new Date(b.date); 
	                	});

			
		  				return_result(result[0],total_amount);					
					}
				});

			}
			else{return_result(null,0); }

		});
	
	}
	catch(err)
	{ 
		console.log("getReceivableDebtorsList",err); return_result(null,0); 
	}

};

methods.getDebtorsTransactions =  function (arrParams,return_result){
	
	try
	{			

		var total_amount = 0;
		var arrTotal = [];
		var date = "";

		var query = `SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?) 
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'account_id' AND meta_value=?) 
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'order_status' AND meta_value='1')`;

		DB.query(query, [arrParams.profile_id,arrParams.account_id], function (data, error) {


			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							total_amount += parseFloat(jsonItems.balance);

							date = jsonItems.date_created;
							counter -= 1;	
							if (counter === 0){ 
								resolve(date);
							}
					
						})
					}],
					function (result, current) {
						if (counter === 0){ 

			  				return_result(result[0],total_amount);					
						}
					});

				});

			}
			else{return_result(0); }

		});
	
	}
	catch(err)
	{ 
		console.log("getDebtorsTransactions",err); return_result(0); 
	}

};

//------------------------------------------------------------------ jazepay receivable debtor list end    --------------------------------------------------------------------------------//



//------------------------------------------------------------------ jazepay receivable debtor transaction list start    --------------------------------------------------------------------------------//


methods.getReceivableDebtorTransactions =  function (arrParams,return_result){
	
	try
	{	
		
		methods.getDebtorsInvTransactions(arrParams,function(arrInvoice){


			methods.getDebtorsPayTransactions(arrParams,function(arrPayments){

				methods.getDebtorsCashPurchase(arrParams,function(arrCashPur){

					var retArr = arrPayments.concat(arrInvoice);
					var mainArr = retArr.concat(arrCashPur);
				  	mainArr.sort(function(a, b){ 
	               	 	return new Date(a.date) - new Date(b.date); 
	            	});

					var accParams = {account_id: arrParams.debtor_account_id};
					HelperGeneralJazenet.getAccountDetails(accParams, function(retAccountDetails){	


						if(retAccountDetails!=null)
						{
							var ret_obj = {

								account_id   : retAccountDetails.account_id,
								account_type : retAccountDetails.account_type,
								name         : retAccountDetails.name,
								logo         : retAccountDetails.logo,
								transactions : mainArr
							}

						  	return_result(ret_obj);
						}else{return_result(null);}				
					
					});

				});
			});
		});

	}
	catch(err)
	{ 
		console.log("getReceivableDebtorTransactions",err); return_result(null); 
	}

};


methods.getDebtorsInvTransactions =  function (arrParams,return_result){
	
	try
	{			

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		query = `SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?)
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'account_id' AND meta_value=?)
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'invoice_id' AND meta_value !='0')
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'order_id' AND meta_value !='0')`;

		query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";


		var objvalues = {};
		var arrValues = [];

		DB.query(query, [arrParams.debtor_account_id,arrParams.account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 

				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							var debit = Math.round(jsonItems.total * 100) / 100;

							var ref_no = 'I'+pad_with_zeroes(jsonItems.invoice_id,5).toString();

							objvalues = {
		
								id           : jsonItems.group_id.toString(),
								ref_no       : ref_no,
								trans_type   : '1',
								debit        : debit.toFixed(2),
								credit       : '0.00',
								date         : jsonItems.date_created

							};

							counter -= 1;	
							arrValues.push(objvalues);

							if (counter === 0){ 
								resolve(arrValues);
							}	
			
						})
					}],
					function (result, current) {
						if (counter === 0){ 

			  				return_result(result[0]);					
						}
					});

				});

			}
			else{return_result([]); }

		});
	
	}
	catch(err)
	{ 
		console.log("getDebtorsInvTransactions",err); return_result([]); 
	}

};


methods.getDebtorsPayTransactions =  function (arrParams,return_result){
	
	try
	{			

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		query = `SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'profile_id' AND meta_value=?)
		AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'account_id' AND meta_value=?) `;


		query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";

		var objvalues = {};
		var arrValues = [];

		DB.query(query, [arrParams.debtor_account_id,arrParams.account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 

				methods.convertMultiMetaArray(data, function(retConverted){	


					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							var credit = Math.round(jsonItems.total * 100) / 100;

							var ref_no = 'P'+pad_with_zeroes(jsonItems.payment_transaction_id,5).toString();

							objvalues = {
		
								id           : jsonItems.group_id.toString(),
								ref_no       : ref_no,
								trans_type   : '2',
								debit        : '0.00',
								credit       : credit.toFixed(2),
								date         : jsonItems.date_created

							};

							counter -= 1;	
							arrValues.push(objvalues);

							if (counter === 0){ 
								resolve(arrValues);
							}	
			
						})
					}],
					function (result, current) {
						if (counter === 0){ 

			  				return_result(result[0]);					
						}
					});

				});

			}
			else{return_result([]); }

		});
	
	}
	catch(err)
	{ 
		console.log("getDebtorsPayTransactions",err); return_result([]); 
	}

};


methods.getDebtorsCashPurchase =  function (arrParams,return_result){
	
	try
	{			

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		query = `SELECT * FROM jaze_jazepay_cash_sale WHERE group_id IN (SELECT group_id FROM jaze_jazepay_cash_sale WHERE meta_key = 'account_id' AND meta_value=?)
		AND group_id IN (SELECT group_id FROM jaze_jazepay_cash_sale WHERE meta_key = 'profile_id' AND meta_value=?)
		AND group_id IN (SELECT group_id FROM jaze_jazepay_cash_sale WHERE meta_key = 'paid_status' AND meta_value='1')`;

		query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_cash_sale WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";		


		var objvalues = {};
		var arrValues = [];

		DB.query(query, [arrParams.account_id,arrParams.debtor_account_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 

				methods.convertMultiMetaArray(data, function(retConverted){	


					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {		

							var credit = Math.round(jsonItems.total * 100) / 100;

							var ref_no = 'CS'+pad_with_zeroes(jsonItems.cashsale_id,5).toString();

							objvalues = {
		
								id           : jsonItems.group_id.toString(),
								ref_no       : ref_no,
								trans_type   : '3',
								debit        : parseFloat(jsonItems.total).toFixed(2),
								credit       : '0.00',
								date         : jsonItems.date_created

							};

							counter -= 1;	
							arrValues.push(objvalues);

							if (counter === 0){ 
								resolve(arrValues);
							}	
			
						})
					}],
					function (result, current) {
						if (counter === 0){ 

			  				return_result(result[0]);					
						}
					});

				});

			}
			else{return_result([]); }

		});
	
	}
	catch(err)
	{ 
		console.log("getDebtorsCashPurchase",err); return_result([]); 
	}

};


//------------------------------------------------------------------ jazepay receivable debtor transaction list end     --------------------------------------------------------------------------------//

// ----------------------------------------------------------------- receivable quotation request received list start  ----------------------------------------------------------------------------- //


methods.getReceivableQuotationRequestReceivedList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_quotation_request WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotation_request WHERE meta_key = 'profile_id' AND meta_value=?) ";
	
		query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotation_request WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";

		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParam = {account_id:jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){

								var type = 0;
								if(parseInt(jsonItems.quotation_id) !==0){
									type = 1;
								}

								var quoParam = {account_id:arrParams.account_id,request_id:jsonItems.group_id};

								methods.getReceivableQuotationReference(quoParam,function(retQuoNumber){

									var quotation_no = "";
									if(retQuoNumber!=""){
										quotation_no = pad_with_zeroes(retQuoNumber,5).toString();
									}

									var ref_no = pad_with_zeroes(jsonItems.quotation_request_id,5).toString();							

									objvalues = {
										id                   : jsonItems.group_id.toString(),
										quotation_request_no : ref_no,
										quotation_no         : quotation_no,
										account_id           : retAccountDetails.account_id,
										account_type         : retAccountDetails.account_type,
										name                 : retAccountDetails.name,
										logo                 : retAccountDetails.logo,
										quotation_status     : jsonItems.status.toString(),
										date                 : jsonItems.date_created
									};

									counter -= 1;	
									arrValues.push(objvalues);
									if (counter === 0){ 
										resolve(arrValues);
									}			

								});				
							});				
						
				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getReceivableQuotationRequestReceivedList",err); return_result(null); }	
};



methods.getReceivableQuotationReference =  function (arrParams,return_result){
	
	try
	{				

		var quotation_id ="";

		var query = `SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'account_id' AND meta_value=?) 
					AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'request_id' AND meta_value=?) `;
		DB.query(query, [arrParams.account_id,arrParams.request_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	
	
					if(parseInt(retConverted[0].quotation_id) !== 0){
						quotation_id = retConverted[0].quotation_id.toString();
					}

					return_result(quotation_id);
				});

			}
			else{ return_result(quotation_id); }

		});

	}
	catch(err)
	{ 
		console.log("getReceivableQuotationReference",err); return_result(quotation_id); 
	}

};

// ------------------------------------------------------------------ receivable quotation request received list end  ----------------------------------------------------------------------------- //


// ------------------------------------------------------------------ receivable quotation request received details start  ----------------------------------------------------------------------------- //


methods.getReceivableQuotationRequestReceivedDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var item_count = 0;

		var query = "SELECT * FROM jaze_jazepay_quotation_request WHERE group_id=? ";		

		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getReceivableQuotationRequestReceivedDetailsMeta(jsonItems.group_id,function(retItems){

								var recParam = {account_id:jsonItems.account_id};
								HelperGeneralJazenet.getAccountDetails(recParam,function(retReceiver){

									var senParam = {account_id:jsonItems.profile_id};
									HelperGeneralJazenet.getAccountDetails(senParam,function(retSender){

										var quotation_request_no = 'QR'+pad_with_zeroes(jsonItems.quotation_request_id,5).toString();				

										var obj_rec = {
											account_id   : retReceiver.account_id,
											account_type : retReceiver.account_type,
											name         : retReceiver.name,
											logo         : retReceiver.logo
										};

										var obj_sen = {
											account_id   : retSender.account_id,
											account_type : retSender.account_type,
											name         : retSender.name,
											logo         : retSender.logo
										};

										var tot_qty = "0";						
										retItems.forEach( function (value){	
											if(value.qty !=""){
												item_count += parseInt(value.qty);											
											}															   	
										});

										tot_qty = item_count.toString();

										var quot_request = {

											id           		 : jsonItems.group_id.toString(),
											quotation_request_no : quotation_request_no,
											ref_no 		 		 : jsonItems.ref_no,									
											receiver_details     : obj_rec,
											sender_details       : obj_sen,
											date                 : jsonItems.date_created,
											items                : retItems,
											total_qty            : tot_qty,
											status               : jsonItems.status,
								
										};

		 								counter -= 1;					
										if (counter === 0){ 
											resolve(quot_request);
										}
									
									});
								});

							});			
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getReceivableQuotationRequestReceivedDetails",err); return_result(null); }	
};



methods.getReceivableQuotationRequestReceivedDetailsMeta =  function (quotation_id,return_result){
	
	try
	{				

		var arrValues = [];
		var obj_return = {};

		var total = 0;

		var query = `SELECT * FROM jaze_jazepay_quotation_request_meta WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotation_request_meta WHERE meta_key = 'quotation_id' AND meta_value=?) `;
		DB.query(query, [quotation_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	
	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	
				
							obj_return = {
								id          : jsonItems.group_id.toString(),
								product_id  : jsonItems.product_id,
								code        : jsonItems.code,
								description : jsonItems.description,						
								qty         : jsonItems.quantity,
			
							};

							counter -= 1;	
							arrValues.push(obj_return);
							if (counter === 0){ 
								resolve(arrValues);
							}	
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return a.id - b.id; 
		                	});
	
 							return_result(result[0]);
						}
					});


				});

			}
			else{ return_result([]); }

		});

	}
	catch(err)
	{ 
		console.log("getReceivableQuotationRequestReceivedDetailsMeta",err); return_result([]); 
	}

};



// ------------------------------------------------------------------ receivable quotation request received details end  ----------------------------------------------------------------------------- //




// ------------------------------------------------------------------ receivable quotations list start  ----------------------------------------------------------------------------- //


methods.getReceivableQuotationsList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');


		var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'account_id' AND meta_value=?) ";
	
		query += " 	AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key='status' AND meta_value ='1')  ";
		query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";

		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var total_amount = 0;
		
							var accParam = {account_id:jsonItems.profile_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){
					
								var quotation_id = "";
								var order_no = "";
								var type = 0;
								if(parseInt(jsonItems.quotation_id) !==0){
									type = 1;
									quotation_id = pad_with_zeroes(jsonItems.quotation_id,5).toString();	
								}

								if(jsonItems.order_id !=""){				
									order_no = pad_with_zeroes(jsonItems.order_id,5).toString();	
								}
		
								var quotParam = {request_id:jsonItems.quotation_request_id,type:type }
								var ref_no = pad_with_zeroes(jsonItems.quotation_request_id,5).toString();			

								objvalues = {
									id           : jsonItems.group_id.toString(),											
									quotation_no : quotation_id,
									order_no     : order_no,
									account_id   : retAccountDetails.account_id,
									account_type : retAccountDetails.account_type,
									name         : retAccountDetails.name,
									logo         : retAccountDetails.logo,
									quotation_status : jsonItems.status.toString(),
									amount       : parseFloat(jsonItems.total).toFixed(2),
									date         : jsonItems.date_created
								};

								counter -= 1;	
								arrValues.push(objvalues);
								if (counter === 0){ 
									resolve(arrValues);
								}			
		
							});												
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getReceivableQuotationsList",err); return_result(null); }	
};



// ------------------------------------------------------------------ receivable quotations list end  ----------------------------------------------------------------------------- //

// ------------------------------------------------------------------ receivable quotations details start  ----------------------------------------------------------------------------- //

methods.getReceivableQuotationDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var item_count = 0;

		var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id=? ";		

		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getReceivableQuotationDetailsMeta(jsonItems.group_id,function(retItems){

								console.log(retItems);

								var recParam = {account_id:jsonItems.account_id};
								HelperGeneralJazenet.getAccountDetails(recParam,function(retReceiver){

									var senParam = {account_id:jsonItems.profile_id};
									HelperGeneralJazenet.getAccountDetails(senParam,function(retSender){

										var quotation_request_no = 'QR'+pad_with_zeroes(jsonItems.quotation_request_id,5).toString();				

										var obj_rec = {
											account_id   : retReceiver.account_id,
											account_type : retReceiver.account_type,
											name         : retReceiver.name,
											logo         : retReceiver.logo
										};

										var obj_sen = {
											account_id   : retSender.account_id,
											account_type : retSender.account_type,
											name         : retSender.name,
											logo         : retSender.logo
										};

										var tot_qty = "0";
										var sub_total = 0;
										retItems.forEach( function (value){	

											if(value.qty !=""){
												item_count += parseInt(value.qty);
												sub_total  += parseFloat(value.total);
											}						
									   		
										});

										tot_qty = item_count.toString();
										var tax = 5/100;

										var tax_compute = parseFloat(tax)*parseFloat(sub_total);
										var total = parseFloat(tax_compute)+parseFloat(sub_total);
										
										var obj_acc = {
											sub_total   : sub_total.toFixed(2),
											discount    : '0.00',
											tax         : '5.00',
											total_tax   : tax_compute.toFixed(2),
											total       : total.toFixed(2)
										};


										var order_no = "";
										var quotation_no = "";
										if(jsonItems.order_id !=""){				
											order_no = 'O'+pad_with_zeroes(jsonItems.order_id,5).toString();	
										}

										if(jsonItems.order_id !=""){				
											quotation_no = 'Q'+pad_with_zeroes(jsonItems.quotation_id,5).toString();	
										}


										var quot_request = {

											id           		 : jsonItems.group_id.toString(),
											quotation_no         : quotation_no,
											ref_no 		 		 : quotation_request_no,	
											order_no             : order_no,						
											receiver_details     : obj_rec,
											sender_details       : obj_sen,
											date                 : jsonItems.date_created,
											accounting           : obj_acc,
											items                : retItems,
											total_qty            : tot_qty,
											status               : jsonItems.status,
								
										};

		 								counter -= 1;					
										if (counter === 0){ 
											resolve(quot_request);
										}
									
									});
								});

							});			
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getReceivableQuotationDetails",err); return_result(null); }	
};



methods.getReceivableQuotationDetailsMeta =  function (quotation_id,return_result){
	
	try
	{				

		var arrValues = [];
		var obj_return = {};

		var total = 0;

		var query = `SELECT * FROM jaze_jazepay_quotations_meta WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations_meta WHERE meta_key = 'quotation_id' AND meta_value=?) `;
		DB.query(query, [quotation_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	
	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	
				
							total = parseFloat(jsonItems.amount)*parseFloat(jsonItems.quantity);

							obj_return = {
								id          : jsonItems.group_id.toString(),
								product_id  : jsonItems.product_id,
								code        : jsonItems.code,
								description : jsonItems.description,
								amount      : jsonItems.amount,
								qty         : jsonItems.quantity,
								total       : parseFloat(total).toFixed(2)
							};

							counter -= 1;	
							arrValues.push(obj_return);
							if (counter === 0){ 
								resolve(arrValues);
							}	
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return a.id - b.id; 
		                	});
	
 							return_result(result[0]);
						}
					});


				});

			}
			else{ return_result([]); }

		});

	}
	catch(err)
	{ 
		console.log("getReceivableQuotationDetailsMeta",err); return_result([]); 
	}

};



// ------------------------------------------------------------------ receivable quotations details end  ----------------------------------------------------------------------------- //



//------------------------------------------------------------------  receivable order received list start   ----------------------------------------------------------//


methods.getReceivableOrderReceivedList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'account_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'type' AND meta_value ='2' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'order_id' AND meta_value !='0' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
	
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParam = {account_id:jsonItems.profile_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){

								methods.getReceivableOrderReceivedTotalAmount(jsonItems.group_id,function(metaVal){
							
									var order_id = 'O'+pad_with_zeroes(jsonItems.order_id,5).toString();

									var quotation_no ="";
									if(jsonItems.quotation_id !=="" || typeof jsonItems.quotation_id !== 'undefined' ){
										quotation_no = pad_with_zeroes(jsonItems.quotation_id,5).toString();
									}


									var invoice_ref_no ="";
									if( jsonItems.invoice_id !=="" || typeof  jsonItems.invoice_id !== 'undefined'){
										invoice_ref_no = pad_with_zeroes(jsonItems.invoice_id,5).toString();
									}
									

									var sub_total = 0;
									var tax = 5/100;
									var tax_compute = 0;
									var total = 0;

									metaVal.forEach(function(val){
										sub_total += parseFloat(val.total);

									});

									var status = '0';
									if(jsonItems.status!=""){
										status = jsonItems.status;
									}

									tax_compute = parseFloat(tax)*parseFloat(sub_total);
									total = parseFloat(tax_compute)+parseFloat(sub_total);


									objvalues = {
										id           : jsonItems.group_id.toString(),
										order_no     : order_id,
										quotation_no : quotation_no,
										invoice_no   : invoice_ref_no,
										account_id   : retAccountDetails.account_id,
										account_type : retAccountDetails.account_type,
										name         : retAccountDetails.name,
										logo         : retAccountDetails.logo,
										amount       : total.toFixed(2),
										status       : status,
										date         : jsonItems.date_updated
									};

									counter -= 1;	
									arrValues.push(objvalues);
									if (counter === 0){ 
										resolve(arrValues);
									}			


								});				
							});				
						
				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getReceivableOrderReceivedList",err); return_result(null); }	
};


methods.getReceivableOrderReceivedTotalAmount =  function (quotation_id,return_result){
	
	try
	{				

		var arrValues = [];
		var obj_return = {};

		//var total = 0;
		//var tot_qty = 0;

		var query = `SELECT * FROM jaze_jazepay_quotations_meta WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations_meta WHERE meta_key = 'quotation_id' AND meta_value=?)`;
		DB.query(query, [quotation_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	
	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

						var total = parseFloat(jsonItems.amount)*parseFloat(jsonItems.quantity);


						obj_return = {
								id          : jsonItems.group_id.toString(),
								product_id  : jsonItems.product_id,
								code        : jsonItems.code,
								description : jsonItems.description,
								amount      : jsonItems.amount,
								qty         : jsonItems.quantity,
								total       : total
							};

							counter -= 1;	
							arrValues.push(obj_return);
							if (counter === 0){ 
								resolve(arrValues);
							}	
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
				

 							return_result(result[0]);
						}
					});


				});

			}
			else{ return_result([]); }

		});

	
	}
	catch(err)
	{ 
		console.log("getReceivableOrderReceivedTotalAmount",err); return_result([]); 
	}

};


//------------------------------------------------------------------ receivable order received list end   ------------------------------------------------------------//

//------------------------------------------------------------------ receivable order details start   ------------------------------------------------------------//


methods.getReceivableOrderReceivedDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var item_count = 0;

		var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id=? ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'type' AND meta_value ='2' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'order_id' AND meta_value !='0' ) ";
	

		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getReceivableOrderReceivedDetailsMeta(jsonItems.group_id,function(retItems){

								console.log(retItems);

								console.log(retItems);

								var recParam = {account_id:jsonItems.account_id};
								HelperGeneralJazenet.getAccountDetails(recParam,function(retReceiver){

									var senParam = {account_id:jsonItems.profile_id};
									HelperGeneralJazenet.getAccountDetails(senParam,function(retSender){

										var order_id = 'O'+pad_with_zeroes(jsonItems.order_id,5).toString();

										var quotation_no ="";
										if( parseInt(jsonItems.quotation_id)!==0 ){
											if(jsonItems.quotation_id !=="" || typeof jsonItems.quotation_id !== 'undefined')
											{
												quotation_no = 'Q'+pad_with_zeroes(jsonItems.quotation_id,5).toString();
											}
										}

										var invoice_ref_no ="";
										if( parseInt(jsonItems.invoice_id)!==0 )
										{
											if( jsonItems.invoice_id !=="" || typeof  jsonItems.invoice_id !== 'undefined'){
												invoice_ref_no = 'I'+pad_with_zeroes(jsonItems.invoice_id,5).toString();
											}
										}

										var obj_rec = {
											account_id   : retReceiver.account_id,
											account_type : retReceiver.account_type,
											name         : retReceiver.name,
											logo         : retReceiver.logo
										};

										var obj_sen = {
											account_id   : retSender.account_id,
											account_type : retSender.account_type,
											name         : retSender.name,
											logo         : retSender.logo
										};

										var tot_qty = "0";
										var sub_total = 0;
										retItems.forEach( function (value){	

											if(value.qty !=""){
												item_count += parseInt(value.qty);
												sub_total  += parseFloat(value.total);
											}						
									   		
										});

										tot_qty = item_count.toString();
										var tax = 5/100;

										var tax_compute = parseFloat(tax)*parseFloat(sub_total);
										var total = parseFloat(tax_compute)+parseFloat(sub_total);
										
										var obj_acc = {
											sub_total   : sub_total.toFixed(2),
											discount    : '0.00',
											tax         : '5.00',
											total_tax   : tax_compute.toFixed(2),
											total       : total.toFixed(2)
										};
								
									
										var quot_request = {

											id           		 : jsonItems.group_id.toString(),
											order_no     		 : order_id,
											ref_no 		 		 : quotation_no,
											invoice_no   	     : invoice_ref_no,
											receiver_details     : obj_rec,
											sender_details       : obj_sen,
											date                 : jsonItems.date_created,
											items                : retItems,
											total_qty            : tot_qty,
											status               : jsonItems.status,
											accounting           : obj_acc

										};

		 								counter -= 1;	
				
										if (counter === 0){ 
											resolve(quot_request);
										}

									
									});
								});

							});			
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getReceivableOrderReceivedDetails",err); return_result(null); }	
};



methods.getReceivableOrderReceivedDetailsMeta =  function (quotation_id,return_result){
	
	try
	{				

		var arrValues = [];
		var obj_return = {};

		var total = 0;

		var query = `SELECT * FROM jaze_jazepay_quotations_meta WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations_meta WHERE meta_key = 'quotation_id' AND meta_value=?)`;
		DB.query(query, [quotation_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				methods.convertMultiMetaArray(data, function(retConverted){	
	
					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

						total = parseFloat(jsonItems.amount)*parseFloat(jsonItems.quantity);

						obj_return = {
								id          : jsonItems.group_id.toString(),
								product_id  : jsonItems.product_id,
								code        : jsonItems.code,
								description : jsonItems.description,
								amount      : jsonItems.amount,
								qty         : jsonItems.quantity,
								total       : total.toString()
							};

							counter -= 1;	
							arrValues.push(obj_return);
							if (counter === 0){ 
								resolve(arrValues);
							}	
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return a.id - b.id; 
		                	});
		       
 							return_result(result[0]);
						}
					});


				});

			}
			else{ return_result([]); }

		});

	
	}
	catch(err)
	{ 
		console.log("getReceivableOrderReceivedDetailsMeta",err); return_result([]); 
	}

};

//------------------------------------------------------------------- receivable order details end ---------------------------------------------------------------------------//


//------------------------------------------------------------------- receivable invoice list start ---------------------------------------------------------------------------//

methods.getReceivableInvoiceList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'account_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'type' AND meta_value ='2' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'invoice_id' AND meta_value !='0' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
	
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParam = {account_id:jsonItems.profile_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){

								methods.getInvoiceReceivedTotalAmount(jsonItems.group_id,function(metaVal){

									methods.getInvoicePaymentTransactionID(jsonItems.payment_id,function(RetPaymentTrans){
							
										var order_id = '';
										if( parseInt(jsonItems.order_id) !==0 ){
											order_id ='O'+pad_with_zeroes(jsonItems.order_id,5).toString();
										}

										var quotation_no ="";
										if(jsonItems.quotation_id !=="" || typeof jsonItems.quotation_id !== 'undefined' ){
											quotation_no = pad_with_zeroes(jsonItems.quotation_id,5).toString();
										}

										var invoice_ref_no ="";
										if( jsonItems.invoice_id !=="" || typeof  jsonItems.invoice_id !== 'undefined'){
											invoice_ref_no = 'I'+pad_with_zeroes(jsonItems.invoice_id,5).toString();
										}

										var status ="0";
										if( parseInt(jsonItems.payment_id) !==0 && jsonItems.status !=""){
											status = jsonItems.status;
										}
										
										
										var sub_total = 0;
										var tax = 5/100;
										var tax_compute = 0;
										var total = 0;

										metaVal.forEach(function(val){
											sub_total += parseFloat(val.total);
										});

										tax_compute = parseFloat(tax)*parseFloat(sub_total);
										total = parseFloat(tax_compute)+parseFloat(sub_total);


										objvalues = {
											id           : jsonItems.group_id.toString(),
											invoice_no   : invoice_ref_no,
											order_no     : order_id,
											payment_no   : RetPaymentTrans,									
											account_id   : retAccountDetails.account_id,
											account_type : retAccountDetails.account_type,
											name         : retAccountDetails.name,
											logo         : retAccountDetails.logo,
											amount       : total.toFixed(2),
											status       : status,
											date         : jsonItems.date_updated
										};

										counter -= 1;	
										arrValues.push(objvalues);
										if (counter === 0){ 
											resolve(arrValues);
										}			

									});				
								});				
							});				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getReceivableInvoiceList",err); return_result(null); }	
};


//------------------------------------------------------------------- receivable invoice list end ---------------------------------------------------------------------------//


//------------------------------------------------------------------  receivable invoice details start   -----------------------------------------------------------------------------------//

methods.getReceivableInvoiceDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id=? ";
		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getInvoiceReceiveDetailsMeta(jsonItems.group_id,function(retItems){

								var recParam = {account_id:jsonItems.profile_id};
								HelperGeneralJazenet.getAccountDetails(recParam,function(retReceiver){

									var senParam = {account_id:jsonItems.account_id};
									HelperGeneralJazenet.getAccountDetails(senParam,function(retSender){

										methods.getInvoicePaymentTransactionDetails(jsonItems.payment_id,function(RetPaymentTrans,payment_no){
							
											var invoice_no ="";
											if( jsonItems.invoice_id !=="" || typeof  jsonItems.invoice_id !== 'undefined'){
												invoice_no = 'I'+pad_with_zeroes(jsonItems.invoice_id,5).toString();
											}
											
											var ref_no = "";
											if(parseInt(jsonItems.invoice_ref_no) !==0){
												ref_no = 'O'+pad_with_zeroes(jsonItems.invoice_ref_no,5).toString();
											}

											var obj_rec = {
												account_id   : retReceiver.account_id,
												account_type : retReceiver.account_type,
												name         : retReceiver.name,
												logo         : retReceiver.logo
											};

											var obj_sen = {
												account_id   : retSender.account_id,
												account_type : retSender.account_type,
												name         : retSender.name,
												logo         : retSender.logo
											};

											var status ="0";
											if( parseInt(jsonItems.payment_id) !==0 && jsonItems.status !=""){
												status = jsonItems.status;
											}
									
											var tot_qty = "0";
											var sub_total = 0;
											var item_count = 0;

											retItems.forEach( function (value){	

												if(value.qty !=""){
													item_count += parseInt(value.qty);
													sub_total  += parseFloat(value.total);
												}						
										   		
											});

											tot_qty = item_count.toString();
											var tax = 0;

											if( jsonItems.tax !=="" || typeof  jsonItems.tax !== 'undefined'){
												tax = parseFloat(jsonItems.tax)/100;
											}

											var tax_compute = parseFloat(tax)*parseFloat(sub_total);
											var total = parseFloat(tax_compute)+parseFloat(sub_total);
											
											var obj_acc = {
												sub_total   : sub_total.toFixed(2),
												discount    : '0.00',
												tax         : parseInt(jsonItems.tax).toFixed(2),
												total_tax   : tax_compute.toFixed(2),
												total       : total.toFixed(2)
											};
																			
											var quot_request = {

												id                   : jsonItems.group_id.toString(),
												invoice_no           : invoice_no,
												ref_no               : ref_no,										
												receiver_details     : obj_rec,
												sender_details       : obj_sen,
												date                 : jsonItems.date_created,
												items                : retItems,
												total_qty            : tot_qty,
												status               : status,
												accounting           : obj_acc,
												payment_no           : payment_no,
												payment_details      : RetPaymentTrans

											};

			 								counter -= 1;	
					
											if (counter === 0){ 
												resolve(quot_request);
											}
									
										});
									});
								});

							});			
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getReceivableInvoiceDetails",err); return_result(null); }	
};



//------------------------------------------------------------------   receivable invoice details end   -------------------------------------------------------------------------------------//


//------------------------------------------------------------------  receivable credit notes list start   -----------------------------------------------------------------------------------//

methods.getReceivableCreditNotesList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_credit_notes WHERE group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'account_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'status' AND meta_value ='1' ) ";

			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
	
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParam = {account_id:jsonItems.profile_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){
				
								var invoice_no = pad_with_zeroes(jsonItems.invoice_id,5).toString();
								var credit_no = pad_with_zeroes(jsonItems.credit_id,5).toString();

								objvalues = {
									id             : jsonItems.group_id.toString(),
									credit_no      : credit_no,									
									invoice_no     : invoice_no,									
									account_id     : retAccountDetails.account_id,
									account_type   : retAccountDetails.account_type,
									name           : retAccountDetails.name,
									logo           : retAccountDetails.logo,
									amount         : parseFloat(jsonItems.total).toFixed(2),
									discount       : parseFloat(jsonItems.discount).toFixed(2),
									tax            : parseFloat(jsonItems.tax).toFixed(2),
									payment_type   : jsonItems.payment_type,
									status         : jsonItems.paid_status,
									date           : jsonItems.date_created
								};

								counter -= 1;	
								arrValues.push(objvalues);
								if (counter === 0){ 
									resolve(arrValues);
								}			
								
							});				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getPayablesCreditNotesList",err); return_result(null); }	
};

//------------------------------------------------------------------  receivable credit notes list end   -----------------------------------------------------------------------------------//

//------------------------------------------------------------------  receivable credit notes details start   ---------------------------------------------------------------------------------//

methods.getReceivableCreditNotesDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_credit_notes WHERE group_id =? ";

	
		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getPayablesCreditNotesDetailsMeta(jsonItems.credit_id,function(retItems){

								var senParam = {account_id:jsonItems.account_id};
								HelperGeneralJazenet.getAccountDetails(senParam,function(retRec){

									var recParam = {account_id:jsonItems.profile_id};
									HelperGeneralJazenet.getAccountDetails(recParam,function(retSen){
					
										var invoice_no = 'I'+pad_with_zeroes(jsonItems.invoice_id,5).toString();

										var credit_no = 'CN'+pad_with_zeroes(jsonItems.credit_id,5).toString();

										var obj_rec = {
											account_id   : retSen.account_id,
											account_type : retSen.account_type,
											name         : retSen.name,
											logo         : retSen.logo
										};

										var obj_sen = {
											account_id   : retRec.account_id,
											account_type : retRec.account_type,
											name         : retRec.name,
											logo         : retRec.logo
										};


										var tot_qty = "0";
										var sub_total = 0;
										var item_count = 0;

										retItems.forEach( function (value){	
											if(value.qty !=""){
												item_count += parseInt(value.qty);
												sub_total  += parseFloat(value.total);
											}						
										});

										var disc = 0;
										if(jsonItems.discount!=""){
										 	disc = parseFloat(jsonItems.discount)/100;
										}

										var disc_compute_new_sub = parseFloat(sub_total)-parseFloat(parseFloat(sub_total)*parseFloat(disc));

										tot_qty = item_count.toString();
										var tax = parseFloat(jsonItems.tax)/100;

										var tax_compute = parseFloat(tax)*parseFloat(disc_compute_new_sub);
										var total = parseFloat(tax_compute)+parseFloat(disc_compute_new_sub);
										
										var obj_acc = {
											sub_total   : sub_total.toFixed(2),
											discount    : parseFloat(jsonItems.discount).toFixed(2),
											tax         : parseFloat(jsonItems.tax).toFixed(2),
											total_tax   : tax_compute.toFixed(2),
											total       : total.toFixed(2)
										};

										
										objvalues = {
											id            : jsonItems.group_id.toString(),
											credit_no     : credit_no,
											invoice_no    : invoice_no,											
											sender_details       : obj_sen,
											receiver_details     : obj_rec,
											amount         : parseFloat(jsonItems.total).toFixed(2),
											items                : retItems,
											total_qty            : tot_qty,
											payment_type   : jsonItems.payment_type,
											status         : jsonItems.status,
											date           : jsonItems.date_created,
											accounting     : obj_acc
										};

										counter -= 1;	
										arrValues.push(objvalues);
										if (counter === 0){ 
											resolve(arrValues);
										}			
										
									});				
								});				
							});				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getReceivableCreditNotesDetails",err); return_result(null); }	
};

//------------------------------------------------------------------  receivable credit notes details end   -----------------------------------------------------------------------------------//

//------------------------------------------------------------------  receivable Payments Received List Stat -----------------------------------------------------------------------------------//
methods.getReceivablePaymentsMadeList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'account_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'status' AND meta_value ='1' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
	
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParam = {account_id:jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){
				
								var payment_id = 'P'+pad_with_zeroes(jsonItems.payment_transaction_id,5).toString();

								objvalues = {
									id             : jsonItems.group_id.toString(),
									payment_no     : payment_id,									
									account_id     : retAccountDetails.account_id,
									account_type   : retAccountDetails.account_type,
									name           : retAccountDetails.name,
									logo           : retAccountDetails.logo,
									amount         : parseFloat(jsonItems.total).toFixed(2),
									charges        : parseFloat(jsonItems.charges).toFixed(2),
									charge_percent : jsonItems.charge_percent,
									payment_type   : jsonItems.payment_type,
									status         : jsonItems.status,
									date           : jsonItems.date_created
								};

								counter -= 1;	
								arrValues.push(objvalues);
								if (counter === 0){ 
									resolve(arrValues);
								}			
								
							});				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getReceivablePaymentsMadeList",err); return_result(null); }	
};

//------------------------------------------------------------------  receivable Payments Received List end -----------------------------------------------------------------------------------//


//------------------------------------------------------------------  receivable Payments Received details start -----------------------------------------------------------------------------------//

methods.getReceivablePaymentsReceivedDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};


		var query = "SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id=? ";
	
		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var senParam = {account_id:jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(senParam,function(retAccountDetails){

								methods.getInvoicePaymentTransactionDetails(jsonItems.group_id,function(RetPaymentTrans){

									methods.getPaymentsMadeDetailsMeta(jsonItems.group_id,function(retInvoice){
									
										var payment_id = 'P'+pad_with_zeroes(jsonItems.payment_transaction_id,5).toString();

										var invoice_no ="";
										if( retInvoice!=null){
											invoice_no = 'I'+pad_with_zeroes(retInvoice.invoice_id,5).toString();
										}
										
										objvalues = {
											id              : jsonItems.group_id.toString(),
											invoice_no      : invoice_no,									
											payment_no      : payment_id,									
											account_id      : retAccountDetails.account_id,
											account_type    : retAccountDetails.account_type,
											name            : retAccountDetails.name,
											logo            : retAccountDetails.logo,
											amount          : parseFloat(jsonItems.total).toFixed(2),
											charges         : parseFloat(jsonItems.charges).toFixed(2),
											charge_percent  : jsonItems.charge_percent,
											status          : jsonItems.status,
											date            : jsonItems.date_created,
											payment_details : RetPaymentTrans
										};

			
		 								counter -= 1;	
				
										if (counter === 0){ 
											resolve(objvalues);
										}
							
									});
								});
							});
						
						})
					}],
					function (result, current) {
						if (counter === 0){ 
			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getReceivablePaymentsReceivedDetails",err); return_result(null); }	
};

//------------------------------------------------------------------  receivable Payments Received details end -----------------------------------------------------------------------------------//



methods.getReceivableReceiptList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'profile_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'status' AND meta_value ='1' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'receipt_id' AND meta_value !='0' ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
	
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParam = {account_id:jsonItems.account_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){
				
								var payment_id = pad_with_zeroes(jsonItems.group_id,5).toString();
								var receipt_id = pad_with_zeroes(jsonItems.receipt_id,5).toString();

								objvalues = {
									id             : jsonItems.group_id.toString(),
									receipt_no     : receipt_id,									
									payment_no     : payment_id,									
									account_id     : retAccountDetails.account_id,
									account_type   : retAccountDetails.account_type,
									category       : retAccountDetails.category,
									name           : retAccountDetails.name,
									logo           : retAccountDetails.logo,
									amount         : parseFloat(jsonItems.total).toFixed(2),
									charges        : parseFloat(jsonItems.charges).toFixed(2),
									charge_percent : jsonItems.charge_percent,
									payment_type   : jsonItems.payment_type,
									status         : jsonItems.status,
									date           : jsonItems.receipt_date_created
								};

								counter -= 1;	
								arrValues.push(objvalues);
								if (counter === 0){ 
									resolve(arrValues);
								}			
								
							});				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getPayablesReceiptList",err); return_result(null); }	
};


methods.getReceivableReceiptDetails =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id =? ";

	
		DB.query(query,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							methods.getPaymentsMadeTransactionDetails(jsonItems.group_id,function(retPayment){

								var senParam = {account_id:jsonItems.profile_id};
								HelperGeneralJazenet.getAccountDetails(senParam,function(retRec){

									var recParam = {account_id:jsonItems.account_id};
									HelperGeneralJazenet.getAccountDetails(recParam,function(retSen){
					
										var payment_id = 'P'+pad_with_zeroes(jsonItems.group_id,5).toString();
										var receipt_id = 'R'+pad_with_zeroes(jsonItems.receipt_id,5).toString();

										var obj_rec = {
											account_id   : retSen.account_id,
											account_type : retSen.account_type,
											category     : retSen.category,
											name         : retSen.name,
											logo         : retSen.logo
										};

										var obj_sen = {
											account_id   : retRec.account_id,
											account_type : retRec.account_type,
											category     : retSen.category,
											name         : retRec.name,
											logo         : retRec.logo
										};
										
										objvalues = {
											id             : jsonItems.group_id.toString(),
											receipt_no     : receipt_id,									
											payment_no     : payment_id,										
											sender_details       : obj_sen,
											receiver_details     : obj_rec,
											amount         : parseFloat(jsonItems.total).toFixed(2),
											payment_type   : jsonItems.payment_type,
											status         : jsonItems.status,
											date           : jsonItems.receipt_date_created,
											payment_details : retPayment
										};

										counter -= 1;	
								
										if (counter === 0){ 
											resolve(objvalues);
										}			
										
									});				
								});				
											
							});
						})
					}],
					function (result, current) {
						if (counter === 0){ 				

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getReceivableReceiptDetails",err); return_result(null); }	
};



methods.getReceivableCashSaleList =  function (arrParams,return_result){
	
	try
	{				

		var arrValues = [];
		var objvalues  = {};

		var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
		var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

		var total_amount = 0;

		var query = "SELECT * FROM jaze_jazepay_cash_sale WHERE group_id IN (SELECT group_id FROM jaze_jazepay_cash_sale WHERE meta_key = 'profile_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_cash_sale WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
	
		DB.query(query,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				
				methods.convertMultiMetaArray(data, function(retConverted){	

					var counter = retConverted.length;		
					var jsonItems = Object.values(retConverted);

					promiseForeach.each(jsonItems,[function (jsonItems) {
						return new Promise(function (resolve, reject) {	

							var accParam = {account_id:jsonItems.profile_id};
							HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){
				
								var document_no = pad_with_zeroes(jsonItems.cashsale_id,5).toString();
								
								objvalues = {
									id             : jsonItems.group_id.toString(),
									document_no    : document_no,									
									account_id     : retAccountDetails.account_id,
									account_type   : retAccountDetails.account_type,
									category       : retAccountDetails.category,
									name           : retAccountDetails.name,
									logo           : retAccountDetails.logo,
									amount         : parseFloat(jsonItems.total).toFixed(2),
									payment_type   : jsonItems.payment_type,
									status         : jsonItems.status,
									date           : jsonItems.date_created
								};

								counter -= 1;	
								arrValues.push(objvalues);
								if (counter === 0){ 
									resolve(arrValues);
								}			
								
							});				
						})
					}],
					function (result, current) {
						if (counter === 0){ 
							
						  	result[0].sort(function(a, b){ 
		                   	 	return new Date(b.date) - new Date(a.date); 
		                	});

			  				return_result(result[0]);					
						}
					});

				});

			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("getReceivableCashPurchaseList",err); return_result(null); }	
};



//------------------------------------------------------------------  jazepay pay multiple invoice  -----------------------------------------------------------------------------------//

methods.___insertMultipleStatement  = function(arrParams,payment_id, return_result){
	try
	{
		if(arrParams !== null)
		{

			var data_list = [];
			var obj_insert_meta = {
				account_id       : arrParams.account_id,
				type       		: '4',
				category_id     : payment_id,
				category_type   : '1',
				charges       	: arrParams.charges,
				amount       	: arrParams.amount,
				date_created	: newdateForm,
				status	 	 	: '1'
			};

			data_list.push(obj_insert_meta);

			var obj_insert_meta = {
				account_id       : arrParams.profile_id,
				type       		: '4',
				category_id     : payment_id,
				category_type   : '2',
				charges       	: arrParams.charges,
				amount       	: arrParams.amount,
				date_created	: newdateForm,
				status	 	 	: '1'
			};

			data_list.push(obj_insert_meta);

			async.forEach(Object.keys(data_list), function (i, callback){ 
					
				setTimeout(() => {

					HelperRestriction.getNewGroupId('jaze_jazepay_statements', function (group_id) {

						if(parseInt(group_id) !== 0){

							HelperRestriction.insertTBQuery('jaze_jazepay_statements',group_id,data_list[i], function (meta_id) {
								
								if(parseInt(meta_id) !== 0)
								{ callback(); }

							});
						}
					});

				}, 1000 * i );
			},function(err) {
				return_result(1);
			});
		}
		else
		{ return_result(0); }
	}
	catch(err)
	{ console.log("___insertMultipleStatement",err); return_result(0); }
};

methods.processUpdateQUotationMultiple  = function(arrParams,payment_id,quotation_id,counter,ctr,return_result){
	try
	{

		console.log(counter+'  '+ctr);

		var updateQuer = `update jaze_jazepay_quotations SET meta_value =? WHERE meta_key =? AND group_id =?`;

		var current_balance = parseFloat(arrParams.current_balance).toFixed(2);
		var payment_amount  = parseFloat(arrParams.amount).toFixed(2);

		var new_balance     = parseFloat(current_balance)-parseFloat(payment_amount);

		var invoice_status = 1;
		if(new_balance !=0){
			invoice_status = 0;
		}

		var	arrUpdate = {
			invoice_status       : invoice_status,
			invoice_date_updated : newdateForm,
			balance              : new_balance.toFixed(2),
			balance_date_updated : newdateForm,
			payment_id           : arrParams.payment_id,
			status               : 1
		};

		var arrLengthS = Object.keys(arrUpdate).length;
		var num_rows_update = 0;
		for (var keyS in arrUpdate) 
	 	{	
			DB.query(updateQuer, [arrUpdate[keyS],keyS,quotation_id], function(dataS, error){
	 		num_rows_update += dataS.affectedRows;
				if(parseInt(num_rows_update) === arrLengthS)
				{					
					//insert statment.
					methods.___insertMultipleStatement(arrParams,payment_id,function(retStatus){
						if(parseInt(counter) == parseInt(ctr)){
							return_result(1);
						}
						//return_result(retStatus);
					});

					//update if jazepay account
					if(parseInt(arrParams.payment_type) === 1)
					{
						methods.procJazePayAccountForPaymentMultiple(arrParams,function(retPayStats){});
					}
			
				}

			});
		
		}

	}
	catch(err)
	{ console.log("processUpdateQUotationMultiple",err); return_result(0); }
};

methods.payablesPayMultipleInvoice =  function (arrParams,return_result){
	
	try
	{				


		var arr_val_len = arrParams.payment_values.length;
		var arr_val = arrParams.payment_values;

		if(parseInt(arr_val_len) > 0)
		{

			var counter   = arr_val_len;		
			var jsonItems = Object.values(arr_val);

			var queryQuotation = `SELECT *  FROM jaze_jazepay_quotations WHERE group_id =?`;

			var ctr = 0;
			for (let i = 0; i < counter; i++) {

				setTimeout( function timer(){

					if(jsonItems[i].id !="" && jsonItems[i].payment_type !="" && jsonItems[i].payment_id !="" )
					{
					
						DB.query(queryQuotation, [jsonItems[i].id], function (data, error) {

							if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
							{ 	

								methods.convertMultiMetaArray(data, function(retConverted){	

									if(parseInt(retConverted[0].balance) !== 0)
									{

										var current_balance = parseFloat(retConverted[0].balance).toFixed(2);
										var current_payment = parseFloat(jsonItems[i].amount).toFixed(2);


										var payParam = {account_id:retConverted[0].profile_id,flag:3};
										methods.getTransactionNumber(payParam,function(ret_payment_no){

											var payment_no = ret_payment_no.quo_req_id;
											if(parseInt(arrParams.payment_no_req_id) === parseInt(ret_payment_no.quo_req_id))
											{
												payment_no = arrParams.payment_no_req_id;
											}


											var queryInvPayment = " SELECT count(group_id) + 1 as  receipt_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'account_id'  AND meta_value = '"+retConverted[0].account_id+"' ";

											DB.query(queryInvPayment,null, function(receipt, error){ 

												if(typeof receipt !== 'undefined' && receipt !== null  && parseInt(receipt.length) > 0)
												{

													var charges_percent = '5';
													var charges = parseFloat(current_payment) * 5 / 100;
													if(parseInt(jsonItems[i].payment_type) === 2){
														charges = 0;
														charges_percent = '0';
													}
												

													var obj_insert_meta = {
														account_id       		: retConverted[0].profile_id,
														profile_id       		: retConverted[0].account_id,
														invoice_id       		: retConverted[0].group_id,
														total       			: current_payment,
														payment_type       		: jsonItems[i].payment_type,
														payment_id       		: jsonItems[i].payment_id,
														payment_transaction_id  : payment_no,
														charges       			: charges,
														charge_percent       	: charges_percent,
														date_created	 		: newdateForm,
														status	 	 			: '1',
										
													};

													HelperRestriction.getNewGroupId('jaze_jazepay_invoice_payments', function (group_id) {

														if(parseInt(group_id) !== 0){

															HelperRestriction.insertTBQuery('jaze_jazepay_invoice_payments',group_id,obj_insert_meta, function (meta_id) {
																
																if(parseInt(meta_id) !== 0)
																{	
														
																	var updateParams = { 
																		account_id      : retConverted[0].account_id, 
																		profile_id      : retConverted[0].profile_id, 
																		amount          : current_payment,
																		charges         : charges, 
																		payment_type    : jsonItems[i].payment_type,
																		payment_id      : jsonItems[i].payment_id,
																		current_balance : current_balance
																	};


																	methods.processUpdateQUotationMultiple(updateParams,group_id,retConverted[0].group_id,counter,ctr,function(retStatus){
																		return_result(retStatus); 

																	});							
																}
																else
																{ return_result(0); }
															});
														}	
														else
														{ return_result(0); }
													});
												}
												else
												{ return_result(0); }
											});

										});
									}
								});

							}	
							else
							{
						 		return_result(2);
						 	}

						});

					}

					ctr++;
				}, i*1000 );

			}
		}
		else
		{
			return_result(0); 
		}

	}
	catch(err)
	{ 
		console.log("payablesPayMultipleInvoice",err); return_result(0); 
	}

};


methods.procJazePayAccountForPaymentMultiple =  function (arrParams,return_result){
	
	try
	{				
		
		var charges = parseFloat(arrParams.amount) * 5 / 100;

		methods.getJazePayBalance(arrParams.profile_id,function(retSenBal){

			var compute_charges = parseFloat( parseFloat(retSenBal)-parseFloat(charges) ).toFixed(2);

			var new_sender_balance = parseFloat( parseFloat(compute_charges)-parseFloat(arrParams.amount) ).toFixed(2);

			methods.getJazePayBalance(arrParams.account_id,function(retRecBal){

				var new_receiver_balance = parseFloat( parseFloat(retRecBal)+parseFloat(arrParams.amount) ).toFixed(2);

				var updateQuer = `update jaze_jazepay_accounts SET meta_value =? WHERE meta_key =? AND group_id =?`;

				var query = `SELECT *  FROM jaze_jazepay_accounts WHERE 
					group_id in (SELECT group_id  FROM jaze_jazepay_accounts WHERE  meta_key = 'account_id' and meta_value =?)`;
	
				DB.query(query, [arrParams.profile_id], function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{ 
						
						var	arrSender = {
							current_balance  : new_sender_balance,
							date_updated     : newdateForm
						};

						var	arrRec = {
							current_balance  : new_receiver_balance,
							date_updated     : newdateForm
						};

						var arrLengthS = Object.keys(arrSender).length;
						var num_rows_sender = 0;
						for (var keyS in arrSender) 
					 	{	
							DB.query(updateQuer, [arrSender[keyS],keyS,data[0].group_id], function(dataS, error){
					 		num_rows_sender += dataS.affectedRows;
								if(parseInt(num_rows_sender) === arrLengthS){
									
									DB.query(query, [arrParams.account_id], function (dataIn, error) {

										if (typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) !== 0)
										{ 
											//update if receiver has a account
											var arrLengthR = Object.keys(arrRec).length;
											var num_rows_rec = 0;
											for (var keyR in arrRec) 
										 	{	
												DB.query(updateQuer, [arrRec[keyR],keyR,dataIn[0].group_id], function(dataR, error){
										 		num_rows_rec += dataR.affectedRows;
													if(parseInt(num_rows_rec) === arrLengthR){
								
														return_result(1);
														
													}
												});
											
											}

										}
										else
										{

											//insert if receiver has no account
											var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_accounts";
											var lastGrpId = 0;

											var arrInsert = {
												account_id       : arrParams.account_id,
												current_balance  : new_receiver_balance,
												date_created     : standardDT,
												date_updated     : standardDT,
												status           : 1
											}

											var arrLength = Object.keys(arrInsert).length;
											var num_rows_insert = 0;

											methods.getLastGroupId(queGroupMax, function(queGrpRes){

												lastGrpId = queGrpRes[0].lastGrpId;
												var plusOneGroupID = lastGrpId+1;

												var querInsert = `insert into jaze_jazepay_accounts (group_id, meta_key, meta_value) VALUES (?,?,?)`;
												for (var key in arrInsert) 
											 	{
													DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
														num_rows_insert += data.affectedRows;
														if(parseInt(num_rows_insert) === arrLength){
															return_result(1);
														}
													});

											  	};

											});
										}

									});
									
								}

							});
						
						}

					}
			
				});

	
			});

		});
	
	}
	catch(err)
	{ 
		console.log("procJazePayAccountForPaymentMultiple",err); return_result(0); 
	}

};

//------------------------------------------------------------------  jazepay pay multiple invoice  -----------------------------------------------------------------------------------//

//------------------------------------------------------------------  generate jazepay reports -----------------------------------------------------------------------------------//

methods.generateJazepayReports  = function(arrParams, return_result){
	try
	{

		var multi_array = [];
		var main_array = [];


		methods.__cashPurchaseReport(arrParams,function(report_1){	

			if(report_1!=null){
				multi_array.push(report_1);		
			}	
			
			    		
			methods.__creditNotesReport(arrParams,function(report_2){

				if(report_2!=null){
			
					multi_array.push(report_2);

				}
			
				methods.__fundsReports(arrParams,function(report_3){	

					if(report_3!=null){					
						multi_array.push(report_3);				
					}


					methods.__internationalTransferReports(arrParams,function(report_4){	

						if(report_4!=null){					
							multi_array.push(report_4);				
						}

						methods.__invoiceReceivedReports(arrParams,function(report_5){	

							if(report_5!=null){					
								multi_array.push(report_5);				
							}


							methods.__transferReceivedReports(arrParams,function(report_6){	

								if(report_6!=null){					
									multi_array.push(report_6);				
								}


								methods.__transferSentReports(arrParams,function(report_7){	

									if(report_7!=null){					
										multi_array.push(report_7);				
									}

									methods.__nationalTransferReports(arrParams,function(report_8){	

										if(report_8!=null){					
											multi_array.push(report_8);				
										}


										methods.__orderSentReports(arrParams,function(report_9){	

											if(report_9!=null){					
												multi_array.push(report_9);				
											}


											methods.__paymentsSentReports(arrParams,function(report_10){	

												if(report_10!=null){					
													multi_array.push(report_10);				
												}
												

												methods.__quotationReceivedReports(arrParams,function(report_11){	

													if(report_11!=null){					
														multi_array.push(report_11);				
													}


													methods.__receivedReceiptsReports(arrParams,function(report_12){	

														if(report_12!=null){					
															multi_array.push(report_12);				
														}

														methods.__systemPartnersReports(arrParams,function(report_13){	

															if(report_13!=null){					
																multi_array.push(report_13);				
															}


															if(multi_array.length > 0 )
															{

																async.forEach(Object.keys(multi_array), function (i, callback){ 
																		
																	setTimeout(() => {

																		main_array.push(multi_array[i]);
																		callback();

																	}, 1000 );

																},function(err) {
																
																	var ret_all = Array.prototype.concat.apply([], main_array);
																	ret_all.sort(function(a, b){ 
															       	 	return new Date(b.date) - new Date(a.date); 
															    	});

																	return_result(ret_all);
																});

															}
																else{return_result(null);
															}

									
														});
													});
												});
											});
										});
									});
								});
							});
						});
					});
				});


			});

		});

	}
	catch(err)
	{ console.log("generateJazepayReports",err); return_result(0); }
};

methods.__cashPurchaseReport =  function (arrParams,return_result){
	
	try
	{				
		var trans_type_arr = arrParams.transaction_type.split(',');
		if(trans_type_arr.includes('1') || trans_type_arr.includes('0'))
		{

			var arrValues = [];
			var objvalues  = {};

			var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
			var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

			var total_amount = 0;

			var query = " SELECT * FROM jaze_jazepay_cash_sale WHERE group_id IN (SELECT group_id FROM jaze_jazepay_cash_sale WHERE meta_key = 'profile_id' AND meta_value =?) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_cash_sale WHERE meta_key ='date_created' AND DATE(meta_value)  BETWEEN '"+from_date+"' AND '"+to_date+"' ) ";
		
			DB.query(query,[arrParams.account_id], function(data, error){
						

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {	

								var accParam = {account_id:jsonItems.account_id};
								HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){
										
									var payment_type = jsonItems.payment_type;
									if(parseInt(jsonItems.payment_type)==0){
										payment_type = '3';
									}

									objvalues = {
										id             : jsonItems.group_id.toString(),								
										account_id     : retAccountDetails.account_id,
										account_type   : retAccountDetails.account_type,
										category       : retAccountDetails.category,
										name           : retAccountDetails.name,
										logo           : retAccountDetails.logo,
										amount         : parseFloat(jsonItems.total).toFixed(2),
										transaction_type : '1',
										payment_type   : payment_type,					
										date           : jsonItems.date_created
									};

									counter -= 1;	
									arrValues.push(objvalues);
									if (counter === 0){ 
										resolve(arrValues);
									}			
									
								});				
							})
						}],
						function (result, current) {
							if (counter === 0){ 
								
							  	result[0].sort(function(a, b){ 
			                   	 	return new Date(b.date) - new Date(a.date); 
			                	});

				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null);
				}

			});

		}else{return_result(null);}
	}
	catch(err)
	{ console.log("__cashPurchaseReport",err); return_result(null); }	
};

methods.__creditNotesReport =  function (arrParams,return_result){
	
	try
	{				

		var trans_type_arr = arrParams.transaction_type.split(',');
		if(trans_type_arr.includes('2') || trans_type_arr.includes('0'))
		{

			var arrValues = [];
			var objvalues  = {};

			var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
			var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

			var query = " SELECT * FROM jaze_jazepay_credit_notes WHERE group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'profile_id' AND meta_value =?) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key ='date_created' AND DATE(meta_value)  BETWEEN '"+from_date+"' AND '"+to_date+"' ) ";
		
			DB.query(query,[arrParams.account_id], function(data, error){


				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {	

								var accParam = {account_id:jsonItems.account_id};
								HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){		
						
									objvalues = {
										id             : jsonItems.group_id.toString(),								
										account_id     : retAccountDetails.account_id,
										account_type   : retAccountDetails.account_type,
										category       : retAccountDetails.category,
										name           : retAccountDetails.name,
										logo           : retAccountDetails.logo,
										amount         : parseFloat(jsonItems.total).toFixed(2),
										transaction_type : '2',
										payment_type   : '3',					
										date           : jsonItems.date_created
									};

									counter -= 1;	
									arrValues.push(objvalues);
									if (counter === 0){ 
										resolve(arrValues);
									}			
									
								});				
							})
						}],
						function (result, current) {
							if (counter === 0){ 
								
							  	result[0].sort(function(a, b){ 
			                   	 	return new Date(b.date) - new Date(a.date); 
			                	});

				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null);
				}

			});

		}else{return_result(null);}


	}
	catch(err)
	{ console.log("__creditNotesReport",err); return_result(null); }	
};

methods.__fundsReports =  function (arrParams,return_result){
	
	try
	{				

		var trans_type_arr = arrParams.transaction_type.split(',');
		if(trans_type_arr.includes('3') || trans_type_arr.includes('0'))
		{

			var arrValues  = [];
			var objvalues  = {};

			var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
			var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

			var query = " SELECT * FROM jaze_jazepay_statements WHERE group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'account_id' AND meta_value=?) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key ='type' AND meta_value ='1' ) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key ='date_created' AND DATE(meta_value)  BETWEEN '"+from_date+"' AND '"+to_date+"' ) ";
		
			DB.query(query,[arrParams.account_id], function(data, error){

	

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {	

								var accParam = {account_id:jsonItems.account_id};
								HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){
					
									var total_amount = parseFloat(jsonItems.amount)+parseFloat(jsonItems.charges);

									objvalues = {
										id             : jsonItems.group_id.toString(),								
										account_id     : retAccountDetails.account_id,
										account_type   : retAccountDetails.account_type,
										category       : retAccountDetails.category,
										name           : retAccountDetails.name,
										logo           : retAccountDetails.logo,
										amount         : total_amount.toFixed(2),
										transaction_type : '3',
										payment_type   : '2',					
										date           : jsonItems.date_created
									};

									counter -= 1;	
									arrValues.push(objvalues);
									if (counter === 0){ 
										resolve(arrValues);
									}			
									
								});				
							})
						}],
						function (result, current) {
							if (counter === 0){ 
								
							  	result[0].sort(function(a, b){ 
			                   	 	return new Date(b.date) - new Date(a.date); 
			                	});


				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null);
				}

			});


		}else{return_result(null);}

	}
	catch(err)
	{ console.log("__fundsReports",err); return_result(null); }	
};

methods.__internationalTransferReports =  function (arrParams,return_result){
	
	try
	{				

		var trans_type_arr = arrParams.transaction_type.split(',');
		if(trans_type_arr.includes('4') || trans_type_arr.includes('0'))
		{

			var arrValues  = [];
			var objvalues  = {};

			var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
			var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

			var query = "SELECT * FROM jaze_jazepay_statements WHERE group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'account_id' AND meta_value=?) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key ='category_type' AND meta_value='3' ) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key ='type' AND meta_value='2' ) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key ='date_created' AND DATE(meta_value)  BETWEEN '"+from_date+"' AND '"+to_date+"' ) ";

			DB.query(query,[arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
				

								var queryIn = `SELECT * FROM jaze_jazepay_international_account_list WHERE group_id=?`

								DB.query(queryIn,[jsonItems.category_id], function(dataIn, error){

									if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
									{
										methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

							
											objvalues = {
												id             : jsonItems.group_id.toString(),								
												account_id     : '',
												account_type   : '',
												category       : '',
												name           : retConvertedIn[0].bank_name,
												logo           : '',
												amount         : parseFloat(jsonItems.amount).toFixed(2),
												transaction_type : '4',
												payment_type   : '1',					
												date           : jsonItems.date_created
											};

											
											counter -= 1;
											arrValues.push(objvalues);
											if (counter === 0){ 
												resolve(arrValues);
											}	
										});
									}	
								});
							})
						}],
						function (result, current) {
							if (counter === 0){ 

				  				return_result(result[0]);					
							}
						});
					});

				}
				else
				{
					return_result(null);
				}

			});

		}else{return_result(null);}
	}
	catch(err)
	{ console.log("__internationalTransferReports",err); return_result(null); }	
};

methods.__invoiceReceivedReports =  function (arrParams,return_result){
	
	try
	{				


		var trans_type_arr = arrParams.transaction_type.split(',');
		if(trans_type_arr.includes('5') || trans_type_arr.includes('0'))
		{

			var arrValues = [];
			var objvalues  = {};

			var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
			var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

			var total_amount = 0;

			var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'type' AND meta_value ='2' ) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'invoice_id' AND meta_value !='0' ) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
		
			DB.query(query,[arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {	

								var accParam = {account_id:jsonItems.account_id};
								HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){

									methods.getInvoiceReceivedTotalAmount(jsonItems.group_id,function(metaVal){
																				
										var sub_total = 0;
										var tax = 5/100;
										var tax_compute = 0;
										var total = 0;

										metaVal.forEach(function(val){
											sub_total += parseFloat(val.total);
										});

										tax_compute = parseFloat(tax)*parseFloat(sub_total);
										total = parseFloat(tax_compute)+parseFloat(sub_total);


										objvalues = {
											id           : jsonItems.group_id.toString(),		
											account_id   : retAccountDetails.account_id,
											account_type : retAccountDetails.account_type,
											name         : retAccountDetails.name,
											logo         : retAccountDetails.logo,
											amount       : total.toFixed(2),
											transaction_type : '5',
											payment_type : '',
											date         : jsonItems.date_updated
										};

										counter -= 1;	
										arrValues.push(objvalues);
										if (counter === 0){ 
											resolve(arrValues);
										}			
										
									});				
								});				
							})
						}],
						function (result, current) {
							if (counter === 0){ 
												
				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null);
				}

			});
		}else{return_result(null);}

	}
	catch(err)
	{ console.log("__invoiceReceivedReports",err); return_result(null); }	
};

methods.__transferReceivedReports =  function (arrParams,return_result){
	
	try
	{				

		var trans_type_arr = arrParams.transaction_type.split(',');
		if(trans_type_arr.includes('6') || trans_type_arr.includes('0'))
		{

			var arrValues  = [];
			var objvalues  = {};

			var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
			var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

			var query = "SELECT * FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE group_id IN (SELECT group_id FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE meta_key = 'profile_id' AND meta_value=?) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE meta_key ='date' AND DATE(meta_value)  BETWEEN '"+from_date+"' AND '"+to_date+"' ) ";

			DB.query(query,[arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
					

								var accParam = {account_id:jsonItems.account_id};
								HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){

									objvalues = {
										id             : jsonItems.group_id.toString(),								
										account_id     : retAccountDetails.account_id,
										account_type   : retAccountDetails.account_type,
										category       : retAccountDetails.category,
										name           : retAccountDetails.name,
										logo           : retAccountDetails.logo,
										amount         : parseFloat(jsonItems.amount).toFixed(2),
										transaction_type : '6',
										payment_type   : '1',					
										date           : jsonItems.date
									};

									
									counter -= 1;
									arrValues.push(objvalues);
									if (counter === 0){ 
										resolve(arrValues);
									}	
								});							

					
							})
						}],
						function (result, current) {
							if (counter === 0){ 

				  				return_result(result[0]);					
							}
						});
					});

				}
				else
				{
					return_result(null);
				}

			});

		}else{return_result(null);}
	}
	catch(err)
	{ console.log("__transferReceivedReports",err); return_result(null); }	
};

methods.__transferSentReports =  function (arrParams,return_result){
	
	try
	{				

		var trans_type_arr = arrParams.transaction_type.split(',');
		if(trans_type_arr.includes('7') || trans_type_arr.includes('0'))
		{

			var arrValues  = [];
			var objvalues  = {};

			var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
			var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

			var query = "SELECT * FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE group_id IN (SELECT group_id FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE meta_key = 'account_id' AND meta_value=?) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_jazenet_transfer_transaction_list WHERE meta_key ='date' AND DATE(meta_value)  BETWEEN '"+from_date+"' AND '"+to_date+"' ) ";

			DB.query(query,[arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
					

								var accParam = {account_id:jsonItems.profile_id};
								HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){

									objvalues = {
										id             : jsonItems.group_id.toString(),								
										account_id     : retAccountDetails.account_id,
										account_type   : retAccountDetails.account_type,
										category       : retAccountDetails.category,
										name           : retAccountDetails.name,
										logo           : retAccountDetails.logo,
										amount         : parseFloat(jsonItems.amount).toFixed(2),
										transaction_type : '7',
										payment_type   : '1',					
										date           : jsonItems.date
									};

									
									counter -= 1;
									arrValues.push(objvalues);
									if (counter === 0){ 
										resolve(arrValues);
									}	
								});							

					
							})
						}],
						function (result, current) {
							if (counter === 0){ 

				  				return_result(result[0]);					
							}
						});
					});

				}
				else
				{
					return_result(null);
				}

			});

		}else{return_result(null);}
	}
	catch(err)
	{ console.log("__transferSentReports",err); return_result(null); }	
};

methods.__nationalTransferReports =  function (arrParams,return_result){
	
	try
	{				

		var trans_type_arr = arrParams.transaction_type.split(',');
		if(trans_type_arr.includes('8') || trans_type_arr.includes('0'))
		{

			var arrValues  = [];
			var objvalues  = {};

			var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
			var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

			var query = "SELECT * FROM jaze_jazepay_statements WHERE group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'account_id' AND meta_value=?) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key ='category_type' AND meta_value='2' ) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key ='type' AND meta_value='2' ) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key ='date_created' AND DATE(meta_value)  BETWEEN '"+from_date+"' AND '"+to_date+"' ) ";

			DB.query(query,[arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
				

								var queryIn = `SELECT * FROM jaze_jazepay_national_account_list WHERE group_id=?`

								DB.query(queryIn,[jsonItems.category_id], function(dataIn, error){

									if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
									{
										methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

							
											objvalues = {
												id             : jsonItems.group_id.toString(),								
												account_id     : '',
												account_type   : '',
												category       : '',
												name           : retConvertedIn[0].bank_name,
												logo           : '',
												amount         : parseFloat(jsonItems.amount).toFixed(2),
												transaction_type : '8',
												payment_type   : '1',					
												date           : jsonItems.date_created
											};

											
											counter -= 1;
											arrValues.push(objvalues);
											if (counter === 0){ 
												resolve(arrValues);
											}	
										});
									}	
								});
							})
						}],
						function (result, current) {
							if (counter === 0){ 

				  				return_result(result[0]);					
							}
						});
					});

				}
				else
				{
					return_result(null);
				}

			});

		}else{return_result(null);}
	}
	catch(err)
	{ console.log("__nationalTransferReports",err); return_result(null); }	
};

methods.__orderSentReports =  function (arrParams,return_result){
	
	try
	{				


		var trans_type_arr = arrParams.transaction_type.split(',');
		if(trans_type_arr.includes('9') || trans_type_arr.includes('0'))
		{

			var arrValues = [];
			var objvalues  = {};

			var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
			var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

			var total_amount = 0;

			var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'order_status' AND meta_value ='1' ) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'invoice_status' AND (meta_value !='' AND meta_value !='0')  ) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
		
			DB.query(query,[arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {	

								var accParam = {account_id:jsonItems.account_id};
								HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){

									methods.getOrderQuotationRequestTotalAmount(jsonItems.group_id,function(metaVal){
												
										var sub_total = 0;
										var tax = 5/100;
										var tax_compute = 0;
										var total = 0;

										metaVal.forEach(function(val){
											sub_total += parseFloat(val.total);
										});
									
										tax_compute = parseFloat(tax)*parseFloat(sub_total);
										total = parseFloat(tax_compute)+parseFloat(sub_total);

										objvalues = {
											id           : jsonItems.group_id.toString(),										
											account_id   : retAccountDetails.account_id,
											account_type : retAccountDetails.account_type,
											name         : retAccountDetails.name,
											logo         : retAccountDetails.logo,
											amount       : total.toFixed(2),	
											transaction_type : '9',
											payment_type : '',									
											date         : jsonItems.date_updated
										};

										counter -= 1;	
										arrValues.push(objvalues);
										if (counter === 0){ 
											resolve(arrValues);
										}			

									});				
								});				
							
					
							})
						}],
						function (result, current) {
							if (counter === 0){ 								
				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null);
				}

			});

		}else{return_result(null);}

	}
	catch(err)
	{ console.log("__orderSentReports",err); return_result(null); }	
};

methods.__paymentsSentReports =  function (arrParams,return_result){
	
	try
	{		

		var trans_type_arr = arrParams.transaction_type.split(',');
		if(trans_type_arr.includes('10') || trans_type_arr.includes('0'))
		{	

			var arrValues = [];
			var objvalues  = {};

			var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
			var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

			var total_amount = 0;

			var query = "SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'account_id' AND meta_value=?) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'status' AND meta_value ='1' ) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
		
			DB.query(query,[arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {	

								var accParam = {account_id:jsonItems.profile_id};
								HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){
					
						
									objvalues = {
										id             : jsonItems.group_id.toString(),
										account_id     : retAccountDetails.account_id,
										account_type   : retAccountDetails.account_type,
										name           : retAccountDetails.name,
										logo           : retAccountDetails.logo,
										amount         : parseFloat(jsonItems.total).toFixed(2),
										transaction_type : '10',
										payment_type   : jsonItems.payment_type,					
										date           : jsonItems.date_created
									};

									counter -= 1;	
									arrValues.push(objvalues);
									if (counter === 0){ 
										resolve(arrValues);
									}			
									
								});				
							})
						}],
						function (result, current) {
							if (counter === 0){ 
							
				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null);
				}

			});


		}else{return_result(null);}

	}
	catch(err)
	{ console.log("__paymentsSentReports",err); return_result(null); }	
};

methods.__quotationReceivedReports =  function (arrParams,return_result){
	
	try
	{				

		var trans_type_arr = arrParams.transaction_type.split(',');
		if(trans_type_arr.includes('11') || trans_type_arr.includes('0'))
		{	

			var arrValues = [];
			var objvalues  = {};

			var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
			var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

			var total_amount = 0;

			var query = "SELECT * FROM jaze_jazepay_quotations WHERE group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value=?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'status' AND ( meta_value='1' OR meta_value='0' ) ) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";


			DB.query(query,[arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {	

								var accParam = {account_id:jsonItems.account_id};
								HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){

									objvalues = {
										id           : jsonItems.group_id.toString(),
										account_id   : retAccountDetails.account_id,
										account_type : retAccountDetails.account_type,
										name         : retAccountDetails.name,
										logo         : retAccountDetails.logo,
										amount       : jsonItems.total,
										transaction_type : '11',
										payment_type   : '',
										date         : jsonItems.date_created
									};

									counter -= 1;	
									arrValues.push(objvalues);
									if (counter === 0){ 
										resolve(arrValues);
									}			
			
								});				
						
							})
						}],
						function (result, current) {
							if (counter === 0){ 
							
				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null);
				}

			});


		}else{return_result(null);}

	}
	catch(err)
	{ console.log("__quotationReceivedReports",err); return_result(null); }	
};

methods.__receivedReceiptsReports =  function (arrParams,return_result){
	
	try
	{				

		var trans_type_arr = arrParams.transaction_type.split(',');
		if(trans_type_arr.includes('12') || trans_type_arr.includes('0'))
		{	

			var arrValues = [];
			var objvalues  = {};

			var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
			var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

			var total_amount = 0;

			var query = "SELECT * FROM jaze_jazepay_invoice_payments WHERE group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'account_id' AND meta_value=?) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'status' AND meta_value ='1' ) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'receipt_id' AND meta_value !='0' ) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_invoice_payments WHERE meta_key = 'date_created' AND DATE(meta_value) BETWEEN '"+from_date+"' AND '"+to_date+"')  ";
		
			DB.query(query,[arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {	

								var accParam = {account_id:jsonItems.profile_id};
								HelperGeneralJazenet.getAccountDetails(accParam,function(retAccountDetails){
					
							
									objvalues = {
										id             : jsonItems.group_id.toString(),												
										account_id     : retAccountDetails.account_id,
										account_type   : retAccountDetails.account_type,
										name           : retAccountDetails.name,
										logo           : retAccountDetails.logo,
										amount         : parseFloat(jsonItems.total).toFixed(2),
										transaction_type : '12',
										payment_type   : jsonItems.payment_type,
										date           : jsonItems.receipt_date_created
									};

									counter -= 1;	
									arrValues.push(objvalues);
									if (counter === 0){ 
										resolve(arrValues);
									}			
									
								});				
							})
						}],
						function (result, current) {
							if (counter === 0){ 								
				  				return_result(result[0]);					
							}
						});

					});

				}
				else
				{
					return_result(null);
				}

			});

		}else{return_result(null);}
	}
	catch(err)
	{ console.log("__receivedReceiptsReports",err); return_result(null); }	
};

methods.__systemPartnersReports =  function (arrParams,return_result){
	
	try
	{				

		var trans_type_arr = arrParams.transaction_type.split(',');
		if(trans_type_arr.includes('13') || trans_type_arr.includes('0'))
		{

			var arrValues  = [];
			var objvalues  = {};

			var from_date =  dateFormat(arrParams.from_date,'yyyy-mm-dd');
			var to_date   =  dateFormat(arrParams.to_date,'yyyy-mm-dd');

			var query = "SELECT * FROM jaze_jazepay_statements WHERE group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key = 'account_id' AND meta_value=?) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key ='category_type' AND meta_value='1' ) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key ='type' AND meta_value='2' ) ";
				query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_statements WHERE meta_key ='date_created' AND DATE(meta_value)  BETWEEN '"+from_date+"' AND '"+to_date+"' ) ";

			DB.query(query,[arrParams.account_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{
					
					methods.convertMultiMetaArray(data, function(retConverted){	

						var counter = retConverted.length;		
						var jsonItems = Object.values(retConverted);

						promiseForeach.each(jsonItems,[function (jsonItems) {
							return new Promise(function (resolve, reject) {		
				

								var queryIn = `SELECT * FROM jaze_jazepay_system_partner_transaction_list WHERE group_id=?`;

								
								DB.query(queryIn,[jsonItems.category_id], function(dataIn, error){

									if(typeof dataIn !== 'undefined' && dataIn !== null && parseInt(dataIn.length) > 0)
									{
										methods.convertMultiMetaArray(dataIn, function(retConvertedIn){	

						

											var accParam = { account_id: retConvertedIn[0].partner_id };

											methods.getSystemPartnerDetailsForSearch(accParam, function(retAccountDetails){					
		

												objvalues = {
													id             : jsonItems.group_id.toString(),								
													account_id     : retConvertedIn[0].partner_id,
													account_type   : '',
													category       : '',
													name           : retAccountDetails.name,
													logo           : retAccountDetails.logo,
													amount         : parseFloat(jsonItems.amount).toFixed(2),
													transaction_type : '13',
													payment_type   : '1',					
													date           : jsonItems.date_created
												};

												
												counter -= 1;
												arrValues.push(objvalues);
												if (counter === 0){ 
													resolve(arrValues);
												}	

											});
										});
									}	
								});
							})
						}],
						function (result, current) {
							if (counter === 0){ 

				  				return_result(result[0]);					
							}
						});
					});

				}
				else
				{
					return_result(null);
				}

			});

		}else{return_result(null);}
	}
	catch(err)
	{ console.log("__systemPartnersReports",err); return_result(null); }	
};

//------------------------------------------------------------------  generate jazepay reports -----------------------------------------------------------------------------------//


//------------------------------------------------------------------- receivable quote reject request quotation ---------------------------------------------------------------------------//


methods.quotationReceivedQuoteReject =  function (arrParams,return_result){
	
	try
	{				
		var arr_val_len = arrParams.product_values.length;
		var arr_val = arrParams.product_values;

		if(parseInt(arrParams.flag) === 1)
		{

			if(parseInt(arr_val_len) > 0)
			{
				var query = `SELECT * FROM jaze_jazepay_quotation_request WHERE group_id=?`;
				DB.query(query, [arrParams.id], function (data, error) {

					if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
					{ 	

						methods.convertMultiMetaArray(data, function(retConverted){	

							var tranParam = { account_id:retConverted[0].profile_id, profile_id:retConverted[0].account_id };
							methods.getQuotationID(tranParam,function(retQuotation_id){

								var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_quotations";
								var lastGrpId = 0;

								var total_price = 0;
								arr_val.forEach(function(val){
									total_price += parseFloat(parseFloat(val.price) * val.qty);
								});

								var computed_disc = total_price;
								var comp_disc = 0;
								var cur_disc  = parseFloat(arrParams.discount).toFixed(2);
								var discount  = 0;

								if(parseInt(arrParams.discount)!==0){				
									discount  = parseFloat(cur_disc)/100;
									comp_disc = parseFloat(total_price)*parseFloat(discount);
									computed_disc = parseFloat(total_price)-parseFloat(comp_disc);
								}

								var arrInsert = {
									account_id           : retConverted[0].profile_id,
									profile_id           : retConverted[0].account_id,
									request_id           : retConverted[0].group_id,
									type                 : retConverted[0].type,
									total                : computed_disc.toFixed(2),
									discount             : arrParams.discount,
									tax                  : '5',
									date_created         : newdateForm,
									date_updated         : newdateForm,
									status               : '0',
									ref_no               : retConverted[0].group_id,
									order_status         : '',
									order_ref_no         : '',
									order_date_created   : '',
									order_date_updated   : '',
									invoice_status       : '',
									invoice_ref_no       : '',
									invoice_date_created : '',
									invoice_date_updated : '',
									balance              : computed_disc.toFixed(2),
									balance_date_updated : newdateForm,
									payment_id           : '0',
									quotation_request_id : retConverted[0].quotation_request_id,
									quotation_id         : retQuotation_id,
									order_id             : '0',
									invoice_id           : '0'
								}

								var arrLength = Object.keys(arrInsert).length;
								var num_rows_insert = 0;

								var transParam = {account_id:arrParams.account_id,request_account_id:arrParams.request_account_id,amount:arrParams.amount};
								methods.getLastGroupId(queGroupMax, function(queGrpRes){

									lastGrpId = queGrpRes[0].lastGrpId;
									var plusOneGroupID = lastGrpId+1;

									var quot_req = {quotation_id: plusOneGroupID,status:1};
									for (var ukey in quot_req) 
								 	{
										DB.query(`update jaze_jazepay_quotation_request SET meta_value =? WHERE meta_key =? AND group_id =?`, [quot_req[ukey],ukey,retConverted[0].group_id], function(dataS, error){});

								  	};

									//insert quotations
									var querInsert = `insert into jaze_jazepay_quotations (group_id, meta_key, meta_value) VALUES (?,?,?)`;
									for (var key in arrInsert) 
								 	{
										DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
											num_rows_insert += data.affectedRows;
											if(parseInt(num_rows_insert) === arrLength){

												methods.processQuotationMetaInsert(plusOneGroupID,arr_val, function(retMeta){	

													return_result(retMeta);

												});
																				
											}
										});

								  	};

								});
							});

						});
					}
					else
					{ 
						return_result(2);

					}

				});
			}
			else
			{
			 	return_result(0);
			}
		}
		else
		{
		
			DB.query(`update jaze_jazepay_quotation_request SET meta_value =? WHERE meta_key =? AND group_id =?`, ['2','status',arrParams.id], function(dataS, error){
				if(dataS!=null){
					return_result(3);
				}else{
					return_result(0);
				} 
			});

		}

	}
	catch(err)
	{ console.log("quotationReceivedQuoteReject",err); return_result(0); }	
};


methods.processQuotationMetaInsert =  function(quotation_id,arrObjects,return_result){
	try
	{	

		var counter   = arrObjects.length;		
		var jsonItems = Object.values(arrObjects);

		var querInsert = `insert into jaze_jazepay_quotations_meta (group_id, meta_key, meta_value) VALUES (?,?,?)`;
		var queGroupMaxMeta = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_quotations_meta";


		var lastGrpId  = 0;

		var ctr = 1;
		for (let i = 0; i < counter; i++) {

			setTimeout( function timer(){

				methods.getLastGroupId(queGroupMaxMeta, function(queGrpResMeta){	


					lastGrpId = queGrpResMeta[0].lastGrpId;
					var plusOneGroupID = lastGrpId+1;

					var prod_id = 0;
					var qty = 0;	

			
					if(parseInt(jsonItems[i].id) !== 0 || jsonItems[i].id !=="")
					{
						prod_id = jsonItems[i].id;
						qty     = jsonItems[i].qty;
					}

					var objInsert = {
						quantity     : qty,
						amount       : jsonItems[i].price,
						description  : jsonItems[i].desc,
						code         : jsonItems[i].code,
						product_id   : prod_id,
						quotation_id : quotation_id,
						date_created : newdateForm,
						status       : 1
					};

					for (var key in objInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,objInsert[key]], function(data, error){});	

				  	};

			  		if(parseInt(counter) === parseInt(ctr)){
					  	return_result(1);
					}
			
				  	ctr++;

				});

			}, i*3000 );

		}

	}
	catch(err)
	{ console.log("processQuotationMetaInsert",err); return_result(0); }	

};

methods.getQuotationID =  function (quoParam,return_result){
	
	try
	{				

		var query = `SELECT t1.* FROM(
			SELECT group_id,
					(CASE WHEN meta_key = 'quotation_id' THEN meta_value END) AS quotation_id
					FROM  jaze_jazepay_quotations
				) AS t1 WHERE quotation_id != '' 

			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'account_id' AND meta_value =?)
			AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value =?)
			ORDER BY group_id DESC LIMIT 1`;

		DB.query(query, [quoParam.account_id,quoParam.profile_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
	
				var quo_id = parseFloat(data[0].quotation_id)+1;

				return_result(quo_id);

				
			}
			else{ return_result(1); }

		});
	}
	catch(err)
	{ 
		console.log("getQuotationID",err); return_result(1); 
	}

};

//------------------------------------------------------------------- receivable quote reject request quotation ---------------------------------------------------------------------------//


methods.receivableorderReceivedAcceptReject =  function(arrParams,return_result){
	try
	{				

		var query = `SELECT * FROM jaze_jazepay_quotations WHERE group_id=?`;
		DB.query(query, [arrParams.id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 	

				methods.convertMultiMetaArray(data, function(retConverted){	

					var updateQuer = `update jaze_jazepay_quotations SET meta_value =? WHERE meta_key =? AND group_id =?`;

					var updateParam = {
						order_date_updated   : newdateForm,
						order_status         : arrParams.flag
					};

					var arrLength = Object.keys(updateParam).length;
					var num_rows = 0;

					for (var key in updateParam) 
					{	
						DB.query(updateQuer, [updateParam[key],key,retConverted[0].group_id], function(dataR, error){
							num_rows += dataR.affectedRows;
							if(parseInt(num_rows) === arrLength){
								return_result(1);
							}

						});

					}

				});
			}
			else
			{

				return_result(2);
			}

		});

	}
	catch(err)
	{ console.log("receivableorderReceivedAcceptReject",err); return_result(null); }	


};




methods.receivableGenerateInvoice =  function(arrParams,return_result){
	try
	{				
			
		if(parseInt(arrParams.flag) === 1)
		{

			var arr_val_len = arrParams.product_values.length;
			var arr_val = arrParams.product_values;
			
			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_quotations";
			var lastGrpId = 0;

			var total_price = 0;
			arr_val.forEach(function(val){
				if(parseFloat(val.price) !==0){
					total_price += parseFloat(parseFloat(val.price) * val.qty);
				}
			
			});

			var computed_disc = total_price;
			var comp_disc = 0;
			var cur_disc  = parseFloat(arrParams.discount).toFixed(2);
			var discount  = 0;

			if(parseInt(arrParams.discount)!==0){				
				discount  = parseFloat(cur_disc)/100;
				comp_disc = parseFloat(total_price)*parseFloat(discount);
				computed_disc = parseFloat(total_price)-parseFloat(comp_disc);
			}

			var arrInsert = {
				account_id           : arrParams.account_id,
				profile_id           : arrParams.request_account_id,
				request_id           : '0',
				type                 : arrParams.type,
				total                : computed_disc.toFixed(2),
				discount             : arrParams.discount,
				tax                  : '5',
				date_created         : newdateForm,
				date_updated         : newdateForm,
				status               : '0',
				ref_no               : '',
				order_status         : '',
				order_ref_no         : '',
				order_date_created   : '',
				order_date_updated   : '',
				invoice_status       : '0',
				invoice_ref_no       : arrParams.reference,
				invoice_date_created : newdateForm,
				invoice_date_updated : newdateForm,
				balance              : computed_disc.toFixed(2),
				balance_date_updated : newdateForm,
				payment_id           : '0',
				quotation_request_id : '0',
				quotation_id         : '0',
				order_id             : '0',
				invoice_id           : '0'
			}
		
			var arrLength = Object.keys(arrInsert).length;
			var num_rows_insert = 0;

			var transParam = {account_id:arrParams.account_id,request_account_id:arrParams.request_account_id,amount:arrParams.amount};
			methods.getLastGroupId(queGroupMax, function(queGrpRes){

				lastGrpId = queGrpRes[0].lastGrpId;
				var plusOneGroupID = lastGrpId+1;

				//insert quotations
				var querInsert = `insert into jaze_jazepay_quotations (group_id, meta_key, meta_value) VALUES (?,?,?)`;
				for (var key in arrInsert) 
			 	{
					DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
						num_rows_insert += data.affectedRows;
						if(parseInt(num_rows_insert) === arrLength){

							methods.processQuotationMetaInsert(plusOneGroupID,arr_val, function(retMeta){	

								return_result(retMeta);

							});
															
						}
					});

			  	};
	  		});
	 
  		}
  		else
  		{


			var query = `SELECT *  FROM jaze_jazepay_quotations WHERE group_id=?`;
	
			DB.query(query, [arrParams.id], function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 

					var updateQuer = `update jaze_jazepay_quotations SET meta_value =? WHERE meta_key =? AND group_id =?`;
					
					var tranParam = { account_id:arrParams.account_id, profile_id:arrParams.request_account_id,flag:1 };
					methods.getInvoiceID(tranParam,function(invoice_id){

						var	arrUpdate = {
							invoice_status       : '0',
							invoice_ref_no       : arrParams.reference,
							invoice_date_created : newdateForm,
							invoice_date_updated : newdateForm,
							invoice_id           : invoice_id
						};

						var arrLengthS = Object.keys(arrUpdate).length;
						var num_rows_update = 0;
						for (var keyS in arrUpdate) 
					 	{	
							DB.query(updateQuer, [arrUpdate[keyS],keyS,data[0].group_id], function(dataS, error){
					 		num_rows_update += dataS.affectedRows;
								if(parseInt(num_rows_update) === arrLengthS){
									
									return_result(1); 
									
								}

							});
						
						}

					});

				}else{return_result(2);}
	

			});

  		}


	}
	catch(err)
	{ console.log("receivableGenerateInvoice",err); return_result(null); }	


};


methods.getInvoiceID =  function (arrParams,return_result){
	
	try
	{				

		var query = " SELECT t1.* FROM( SELECT group_id,(CASE WHEN meta_key = 'invoice_id' THEN meta_value END) AS invoice_id FROM  jaze_jazepay_quotations ) AS t1 WHERE invoice_id != ''  ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'account_id' AND meta_value =?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'profile_id' AND meta_value =?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_quotations WHERE meta_key = 'invoice_id' AND meta_value !='0') ";
			query += "ORDER BY group_id DESC LIMIT 1 ";

		DB.query(query, [arrParams.account_id,arrParams.profile_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
			
				var quo_id = parseFloat(data[0].invoice_id)+1;
				return_result(quo_id);
			}
			else{ return_result(1); }

		});
	}
	catch(err)
	{ 
		console.log("getInvoiceID",err); return_result(1); 
	}

};


methods.receivableGenerateCreditNotes =  function(arrParams,return_result){
	try
	{				
			
		if(parseInt(arrParams.flag) === 1)
		{

			var tranParam = { account_id:arrParams.account_id, profile_id:arrParams.request_account_id,flag:1 };
			methods.getCreditID(tranParam,function(credit_id){

				var arr_val_len = arrParams.product_values.length;
				var arr_val = arrParams.product_values;
				
				var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_credit_notes";
				var lastGrpId = 0;

				var total_price = 0;
				arr_val.forEach(function(val){
					if(parseFloat(val.price) !==0){
						total_price += parseFloat(parseFloat(val.price) * val.qty);
					}
				
				});

				var computed_disc = total_price;
				var comp_disc = 0;
				var cur_disc  = parseFloat(arrParams.discount).toFixed(2);
				var discount  = 0;

				if(parseInt(arrParams.discount)!==0){				
					discount  = parseFloat(cur_disc)/100;
					comp_disc = parseFloat(total_price)*parseFloat(discount);
					computed_disc = parseFloat(total_price)-parseFloat(comp_disc);
				}

				var arrInsert = {
					account_id      : arrParams.account_id,
					profile_id      : arrParams.request_account_id,
					invoice_id      : '0',
					paid_status     : '1',
					invoice_id_code : '',
					credit_id       : credit_id,
					type            : arrParams.type,
					total           : computed_disc,
					discount        : arrParams.discount,
					tax             : '5',
					date_created    : newdateForm,
					date_updated    : newdateForm,
					status          : '1',
					ref_no          : arrParams.reference,
				};
			
				var arrLength = Object.keys(arrInsert).length;
				var num_rows_insert = 0;

				var transParam = {account_id:arrParams.account_id,request_account_id:arrParams.request_account_id,amount:arrParams.amount};
				methods.getLastGroupId(queGroupMax, function(queGrpRes){

					lastGrpId = queGrpRes[0].lastGrpId;
					var plusOneGroupID = lastGrpId+1;

					//insert quotations
					var querInsert = `insert into jaze_jazepay_credit_notes (group_id, meta_key, meta_value) VALUES (?,?,?)`;
					for (var key in arrInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){							
							num_rows_insert += data.affectedRows;
							if(parseInt(num_rows_insert) === arrLength){

								methods.processCreditNotesMetaInsert(plusOneGroupID,arr_val, function(retMeta){	
									return_result(retMeta);
								});
																
							}
						});

				  	};
		  		});
	  		});
	 
  		}
  		else
  		{

			var query = `SELECT *  FROM jaze_jazepay_quotations WHERE group_id=?`;
		
			DB.query(query, [arrParams.invoice_id], function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					
					methods.convertMultiMetaArray(data, function(retConverted){

						var arr_val_len = arrParams.product_values.length;
						var arr_val = arrParams.product_values;

						var updateQuer = `update jaze_jazepay_quotations SET meta_value =? WHERE meta_key =? AND group_id =?`;
						
						var total_price = 0;
						arr_val.forEach(function(val){
							if(parseFloat(val.price) !==0){
								total_price += parseFloat(parseFloat(val.price) * val.qty);
							}
						
						});

						var	arrUpdate = {
							total                : total_price,
							invoice_status       : '0',
							balance              : '-'+total_price,
							invoice_date_created : newdateForm,
							invoice_date_updated : newdateForm,
						};

						var arrLengthS = Object.keys(arrUpdate).length;
						var num_rows_update = 0;
						for (var keyS in arrUpdate) 
					 	{	
							DB.query(updateQuer, [arrUpdate[keyS],keyS,retConverted[0].group_id], function(dataS, error){
					 		num_rows_update += dataS.affectedRows;
								if(parseInt(num_rows_update) === arrLengthS){
									
									var tranParam = { account_id:arrParams.account_id, profile_id:arrParams.request_account_id,flag:1 };
									methods.getCreditID(tranParam,function(credit_id){

										var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_credit_notes";
										var lastGrpId = 0;
				
										var computed_disc = total_price;
										var comp_disc = 0;
										var cur_disc  = parseFloat(arrParams.discount).toFixed(2);
										var discount  = 0;

										if(parseInt(arrParams.discount)!==0){				
											discount  = parseFloat(cur_disc)/100;
											comp_disc = parseFloat(total_price)*parseFloat(discount);
											computed_disc = parseFloat(total_price)-parseFloat(comp_disc);
										}

										var arrInsert = {
											account_id      : retConverted[0].account_id,
											profile_id      : retConverted[0].profile_id,
											invoice_id      : retConverted[0].group_id,
											paid_status     : '1',
											invoice_id_code : retConverted[0].invoice_id,
											credit_id       : credit_id,
											type            : arrParams.type,
											total           : computed_disc,
											discount        : arrParams.discount,
											tax             : '5',
											date_created    : newdateForm,
											date_updated    : newdateForm,
											status          : '1',
											ref_no          : arrParams.reference,
										};
									
										var arrLength = Object.keys(arrInsert).length;
										var num_rows_insert = 0;

										var transParam = {account_id:retConverted[0].account_id,request_account_id:retConverted[0].profile_id,amount:total_price};
										methods.getLastGroupId(queGroupMax, function(queGrpRes){

											lastGrpId = queGrpRes[0].lastGrpId;
											var plusOneGroupID = lastGrpId+1;

											//insert quotations
											var querInsert = `insert into jaze_jazepay_credit_notes (group_id, meta_key, meta_value) VALUES (?,?,?)`;
											for (var key in arrInsert) 
										 	{
												DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(dataI, error){							
													num_rows_insert += dataI.affectedRows;
													if(parseInt(num_rows_insert) === arrLength){

														methods.processCreditNotesMetaInsert(plusOneGroupID,arr_val, function(retMeta){	
															return_result(retMeta);
														});
																						
													}
												});

										  	};
								  		});
							  		});
									
								}

							});
						
						}

					});

				}else{return_result(2);}
			});

  		}


	}
	catch(err)
	{ console.log("receivableGenerateCreditNotes",err); return_result(null); }	


};

methods.processCreditNotesMetaInsert =  function(credit_note_id,arrObjects,return_result){
	try
	{	

		var counter   = arrObjects.length;		
		var jsonItems = Object.values(arrObjects);

		var querInsert = `insert into jaze_jazepay_credit_notes_meta (group_id, meta_key, meta_value) VALUES (?,?,?)`;
		var queGroupMaxMeta = "SELECT MAX(group_id) AS lastGrpId FROM jaze_jazepay_credit_notes_meta";


		var lastGrpId  = 0;

		var ctr = 1;
		for (let i = 0; i < counter; i++) {

			setTimeout( function timer(){

				methods.getLastGroupId(queGroupMaxMeta, function(queGrpResMeta){	

					lastGrpId = queGrpResMeta[0].lastGrpId;
					var plusOneGroupID = lastGrpId+1;

					var prod_id = 0;
					var qty = 0;	

					if(parseInt(jsonItems[i].id) !== 0 || jsonItems[i].id !=="")
					{
						prod_id = jsonItems[i].id;
						qty     = jsonItems[i].qty;
					}

					var objInsert = {
						quantity       : qty,
						amount         : jsonItems[i].price,
						description    : jsonItems[i].desc,
						code           : jsonItems[i].code,
						product_id     : prod_id,
						credit_note_id : credit_note_id,
						date_created   : newdateForm,
						status         : 1
					};

					for (var key in objInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,objInsert[key]], function(data, error){});	
				  	};

			  		if(parseInt(counter) === parseInt(ctr)){
				  		return_result(1);
					}
			
				  	ctr++;

				});

			}, i*3000 );

		}

	}
	catch(err)
	{ console.log("processCreditNotesMetaInsert",err); return_result(0); }	

};

methods.getCreditID =  function (arrParams,return_result){
	
	try
	{				

		var query = " SELECT t1.* FROM( SELECT group_id,(CASE WHEN meta_key = 'credit_id' THEN meta_value END) AS credit_id FROM  jaze_jazepay_credit_notes ) AS t1 WHERE credit_id != '' ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'account_id' AND meta_value =?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'profile_id' AND meta_value =?) ";
			query += " AND group_id IN (SELECT group_id FROM jaze_jazepay_credit_notes WHERE meta_key = 'credit_id' AND meta_value !='0') ";
			query += "ORDER BY group_id DESC LIMIT 1 ";

		DB.query(query, [arrParams.account_id,arrParams.profile_id], function (data, error) {

			if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
			{ 
				var quo_id = parseFloat(data[0].credit_id)+1;
				return_result(quo_id);
			}
			else{ return_result(1); }

		});
	}
	catch(err)
	{ 
		console.log("getCreditID",err); return_result(1); 
	}

};


//------------------------------------------------------------------- General Module Start ---------------------------------------------------------------------------//



methods.authenticationCreditCard =  function (arrParams,return_result){
	
	try
	{				
		var query = `SELECT * FROM jaze_user_credit_card WHERE group_id IN (SELECT group_id FROM jaze_user_credit_card WHERE meta_key = 'account_id' AND meta_value =?)
					AND group_id =?
					AND group_id IN (SELECT group_id FROM jaze_user_credit_card WHERE meta_key = 'csv' AND meta_value =?)`;

		DB.query(query,[arrParams.account_id,arrParams.id,arrParams.csv], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				return_result(1);
			}	
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("authenticationCreditCard",err); return_result(null); }	
};


methods.getJazePayBalance = function(account_id,return_result)
{
	var jazePay_balance= "0.00";
	try
	{
		if(parseInt(account_id) !== 0)
		{
			var query = " SELECT meta_value  FROM jaze_jazepay_accounts WHERE meta_key in ('current_balance') ";
				query += "and group_id in (SELECT group_id  FROM jaze_jazepay_accounts WHERE  meta_key = 'account_id' and meta_value = '"+account_id+"'  ";
				query += "and group_id in (SELECT group_id  FROM jaze_jazepay_accounts WHERE  meta_key = 'status' and meta_value = '1'))";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					if (parseInt(data[0].meta_value) > 0)
					{  return_result(parseFloat(data[0].meta_value).toFixed(2)); }
					else
					{ return_result(jazePay_balance); }
				}
				else
				{ return_result(jazePay_balance); }
			});
		}
		else
		{ return_result(jazePay_balance); }
	}
	catch(err)
	{
		console.log('getAccountJazePayBalance',err);
		return_result(jazePay_balance);
	}
}


function sort_name(array,keyword)
{
	return array.filter(o => o.account_name.toLowerCase().includes(keyword.toLowerCase()))
			.sort((a, b) => a.account_name.toLowerCase().indexOf(keyword.toLowerCase()) - b.account_name.toLowerCase().indexOf(keyword.toLowerCase()));
}

/*
* split values push to array
*/
function __convert_array(array_val) {

	var list = [];

	if(typeof array_val !== 'undefined' && array_val !== null && array_val !== "")
	{
		if(array_val.toString().indexOf(',') != -1 ){
			list = array_val.split(',');
		}
		else {
			list.push(array_val);
		}
	}	
	return list;
}



methods.convertMultiMetaArrayCompany =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndexCompany(list,arrvalues[i].account_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndexCompany(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].account_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}


methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMultiMetaArray",err); return_result(null);}
};


function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].group_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}


methods.convertMultiMetaArrayProduct =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndexProduct(list,arrvalues[i].product_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							product_id : arrvalues[i].product_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMultiMetaArrayProduct",err); return_result(null);}
};


function __getGroupIndexProduct(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].product_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}


methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
};


function pad_with_zeroes(number, length) {

    var my_string = '' + number;
    while (my_string.length < length) {
        my_string = '0' + my_string;
    }

    return my_string;

}

methods.checkSecurityPin =  function (arrParams,return_result){
	
	try
	{				

		var query = "SELECT * FROM jaze_user_credential_details WHERE account_id=? AND jaze_security_pin=?";
		DB.query(query,[arrParams.account_id,arrParams.security_pin], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{
				return_result(data[0].account_id);
			}
			else
			{
				return_result(null);
			}

		});

	}
	catch(err)
	{ console.log("checkSecurityPin",err); return_result(null); }	
};



methods.__getCountryDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT country_name FROM country  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('');
	}
};

methods.__getStateDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT state_name FROM states  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].state_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('');
	}
};

methods.__getCityDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT city_name FROM cities  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].city_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('');
	}
};

methods.__getAreaDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT area_name FROM area  WHERE area_id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].area_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('');
	}
};


function sortingArrayObject(key, order='asc') {

	return function(a, b) {
	  if(!a.hasOwnProperty(key) || 
		 !b.hasOwnProperty(key)) {
		  return 0; 
	  }
	  
	  const varA = (typeof a[key] === 'string') ? 
		a[key].toUpperCase() : a[key];
	  const varB = (typeof b[key] === 'string') ? 
		b[key].toUpperCase() : b[key];
		
	  let comparison = 0;
	  if (varA > varB) {
		comparison = 1;
	  } else if (varA < varB) {
		comparison = -1;
	  }
	  return (
		(order == 'desc') ? 
		(comparison * -1) : comparison
	  );
	};
}

function removeDuplicates(array) {
	return array.filter((a, b) => array.indexOf(a) === b)
};


//------------------------------------------------------------------- General Module End ---------------------------------------------------------------------------//



module.exports = methods;