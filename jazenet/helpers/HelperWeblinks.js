const methods = {};
const DB = require('../../helpers/db_base.js');


const moment = require("moment-timezone");

// const express = require('express');
// const router = express.Router();

const standardDTUTC = moment().utc().format('YYYY-MM-DD HH:mm:ss');
const getFavicons = require('get-website-favicon');


methods.getWeblinkList = function(arrParams,return_result){

	var arrRows = [];
	var newObj = {};

	var quer = `SELECT t1.* FROM(
		SELECT group_id,
		(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
		(SELECT c.meta_value FROM jaze_user_weblinks_details c WHERE c.meta_key = 'link_name' AND c.group_id = jaze_user_weblinks_details.group_id) AS link_name,
		(SELECT c.meta_value FROM jaze_user_weblinks_details c WHERE c.meta_key = 'link_url' AND c.group_id = jaze_user_weblinks_details.group_id) AS link_url,
		(SELECT c.meta_value FROM jaze_user_weblinks_details c WHERE c.meta_key = 'logo_url' AND c.group_id = jaze_user_weblinks_details.group_id) AS logo_url,
		(SELECT c.meta_value FROM jaze_user_weblinks_details c WHERE c.meta_key = 'sort_order' AND c.group_id = jaze_user_weblinks_details.group_id) AS sort_order
		FROM  jaze_user_weblinks_details
		WHERE group_id IN (SELECT group_id FROM  jaze_user_weblinks_details  WHERE meta_key = 'account_id' AND meta_value =? )
		AND  group_id IN (SELECT group_id FROM  jaze_user_weblinks_details  WHERE meta_key = 'status' AND meta_value ='1' )
	) AS t1 WHERE account_id != "" ORDER BY CAST(sort_order AS UNSIGNED ) ASC`;

	DB.query(quer,[arrParams.account_id], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){


			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
						
					newObj = {
						
						id:val.group_id.toString(),
						name:val.link_name,
						link:val.link_url,
						logo:val.logo_url,
						sort_order:val.sort_order
					};

					arrRows.push(newObj);
				
			  	}

		  		const result = await returnData(arrRows);
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 300);
			  });
			}
			 
			forEach().then(() => {
				return_result(arrRows);
			})


        }
        else
        { 
        	return_result(null);
        }

	});
};



methods.getUrlLogo = function(url,return_result){

	var ret = '';
	
	if(url == G_STATIC_WEB_URL)
	{
		ret = G_STATIC_WEB_URL+"images/favicon.ico";
	}
	else
	{
		getFavicons(url).then(data=>{		
			try
			{	
				if(typeof data.icons !== 'undefined' && data.icons !== null ){
					ret = data.icons[0].src;
					return_result(ret);
				}else{
					ret = G_STATIC_IMG_URL+'uploads/jazenet/others/default_weblink.png';
					return_result(ret);
				}

				
			}
			catch(err)
			{ 
				console.log("getUrlLogo",err); return_result(ret); 
			}
		
		});
	}
};


methods.deleteQuery = function(arrParams,return_result){

	DB.query("delete from jaze_user_weblinks_details where group_id IN (?)", [arrParams.id], function(data, error){
 	   	if(data != null){
			return_result(data.affectedRows);
 	   	}else{
 	   		return_result(0);
 	   	}
	});
};


methods.insertNewWebLink  = function(arrParams,return_result){

	try
	{
		if(arrParams !== null)
		{	
						
			var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_user_weblinks_details";
  			var lastGrpId = 0;

			methods.getLastGroupId(queGroupMax, function(queGrpRes){

			  	queGrpRes.forEach(function(row) {
					lastGrpId = row.lastGrpId;
			  	});

				methods.getLastSortOrder(arrParams, function(retSortOrder){

					methods.getUrlLogo(arrParams.link, function(retLogo){

						var logo = G_STATIC_IMG_URL+'uploads/jazenet/others/default_weblink.png';

						if(retLogo!==null){
							logo = retLogo;
						}

						var arrInsert = {
							account_id:arrParams.account_id,
							link_name:arrParams.name,
							link_url:arrParams.link,
							logo_url:logo.toString(),
							sort_order:retSortOrder,
							create_date:standardDTUTC,
							status:1
						};
					
						var plusOneGroupID = lastGrpId+1;
				  		var querInsert = `insert into jaze_user_weblinks_details (group_id, meta_key, meta_value) VALUES (?,?,?)`;

					 	for (var key in arrInsert) 
					 	{
							DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
								if(parseInt(data.affectedRows) > 0){
									return_result(data.affectedRows)
						 	   	}else{
					   				return_result(null);
						 	   	}
					 		 
					 	 	});
					  	};

				  	});
			  	});

			});
		}
		else
		{ 
			return_result(null); 
		}
	}
	catch(err)
	{ 
		console.log("insertNewWebLink",err); return_result(null); 
	}	
};


methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
};


methods.getLastSortOrder = function(arrParams,return_result){

	try
	{
		var query = `SELECT t1.* FROM(
				SELECT group_id,
				(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
				(SELECT c.meta_value FROM jaze_user_weblinks_details c WHERE c.meta_key = 'sort_order' AND c.group_id = jaze_user_weblinks_details.group_id) AS sort_order
			FROM  jaze_user_weblinks_details
			WHERE group_id IN (SELECT group_id FROM  jaze_user_weblinks_details  WHERE meta_key = 'account_id' AND meta_value =? )
			AND  group_id IN (SELECT group_id FROM  jaze_user_weblinks_details  WHERE meta_key = 'status' AND meta_value ='1' )
			) AS t1 WHERE account_id != "" ORDER BY CAST(sort_order AS UNSIGNED ) DESC LIMIT 1`;

		DB.query(query, [arrParams.account_id], function(data, error){
	 	   if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
				return_result(parseInt(data[0].sort_order)+1);
	 	   	}else{
				return_result(1);
	 	   	}
		});

	}
	catch(err)
	{ 
		console.log("getLastSortOrder",err); return_result(null); 
	}		
};

methods.updateWebLinks = function(arrParams,return_result){

	try
	{
		var updateQue = `update jaze_user_weblinks_details SET meta_value =? WHERE meta_key ='sort_order' AND group_id =?`;
		var arrGroup_id = arrParams.id;

		for (sort = 0; sort < arrGroup_id.length; sort++) {
			var sort_order = sort +1;
			DB.query(updateQue, [sort_order,arrGroup_id[sort]], function(data, error){
	 			if(data == null){
		 	   		return_result(0);
		 	   	}else{
		  			return_result(data.affectedRows);
		 	   	}
			});
		}
	}
	catch(err)
	{ 
		console.log("updateWebLinks",err); return_result(0); 
	}
};


module.exports = methods;
