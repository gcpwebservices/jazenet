const methods = {};
const DB = require('../../helpers/db_base.js');
const moment = require("moment-timezone");



methods.searchCity = function(arrParams,return_result)
{ 
	var newObj = {};
	var arrRows = [];
	var like = "%"+arrParams.key.toLowerCase()+"%";

	try
	{
		var quer = `SELECT co.timezone,co.id AS country_id, s.id AS state_id ,c.id AS city_id, c.city_name, co.country_name, s.state_name
			FROM  country AS co 
			INNER JOIN states AS s ON s.country_id = co.id
			INNER JOIN cities AS c ON c.state_id = s.id 
		WHERE LOWER(c.city_name) LIKE ? AND co.timezone >''`;

		DB.query(quer, [like], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				var delay = parseInt(data.length)*20;
				const forEach = async () => {
				  	const values = data;
				  	for (const val of values){
				  	
	  			 	 	var date = new Date(Date.parse(new Date().toLocaleString('en-US', { timeZone: val.timezone })));
				  		newObj = {
							city_id:val.city_id.toString(),
				  			city_name:val.city_name,
				  			state_name:val.state_name,
				  			country_name:val.country_name,
				  			time_zone:val.timezone,
			  				day: getDay(date),
				  			date:formatDate(date),
			  				gmt:'GMT '+moment().tz(val.timezone).format('Z')
				  		};
				 
						arrRows.push(newObj);

				  	}

			  		const result = await returnData(arrRows);
				}
				 
				const returnData = x => {
				  return new Promise((resolve, reject) => {
				    setTimeout(() => {
				      resolve(x);
				    }, delay);
				  });
				}
					 
				forEach().then(() => {
					return_result(arrRows.sort(sortingArrayObject('name')));
				})

			}
			else
			{
				return_result(null); 
			}

		});

	}
	catch(err)
	{ 
		console.log("searchCity",err); 
		return_result(null); 
	}	
}


methods.getCityInfo = function(arrParams,return_result)
{ 
	try
	{
		if(parseInt(arrParams) !== 0)
		{
			DB.query(`SELECT * FROM cities WHERE id=?`, [arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
					return_result(data[0].city_name);
				}else{
					return_result('');
				}

			});
		}
		else
		{ 
			return_result(''); 
		}
	}
	catch(err)
	{ 
		console.log("getCityInfo",err); 
		return_result(''); 
	}	
}


// methods.getMasterTimeInfo = function(arrParams,return_result)
// { 
// 	var newObj = {};

// 	try
// 	{
	
// 		var master_id = 34917;
// 		if(parseInt(arrParams.master_city_id) !== 0)
// 		{	
// 			master_id = arrParams.master_city_id;
// 		}


// 		var query =`SELECT * FROM jaze_master_time_information WHERE group_id IN (SELECT group_id FROM jaze_master_time_information  WHERE meta_key = 'city_id' AND meta_value =?)`;

// 		DB.query(query, [master_id], function(data, error){

// 			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){


// 				methods.convertMultiMetaArray(data, function(retConverted){

		

// 					methods.__getCityDetails(retConverted[0].city_id, function(retCity){	

// 					  	var date = new Date(Date.parse(new Date().toLocaleString('en-US', { timeZone: retConverted[0].timezone })));
					
// 				 		newObj = {
// 				  			city_id   : retConverted[0].city_id.toString(),
// 				  			name      : retCity,
// 				  			time_zone : retConverted[0].timezone,
// 				  			day       : getDay(date),
// 				  			date      : formatDate(date),
// 				  			gmt       : 'GMT '+moment().tz(retConverted[0].timezone).format('Z')

// 				  		};

// 						return_result(newObj);

// 					});
// 				});

// 			}else{

// 				var query =`SELECT * FROM jaze_master_time_information WHERE group_id IN (SELECT group_id FROM jaze_master_time_information  WHERE meta_key = 'city_id' AND meta_value ='34917')`;

// 				DB.query(query, null, function(data, error){

// 					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){


// 						methods.convertMultiMetaArray(data, function(retConverted){

				

// 							methods.__getCityDetails(retConverted[0].city_id, function(retCity){	

// 							  	var date = new Date(Date.parse(new Date().toLocaleString('en-US', { timeZone: retConverted[0].timezone })));
							
// 						 		newObj = {
// 						  			city_id   : retConverted[0].city_id.toString(),
// 						  			name      : retCity,
// 						  			time_zone : retConverted[0].timezone,
// 						  			day       : getDay(date),
// 						  			date      : formatDate(date),
// 						  			gmt       : 'GMT '+moment().tz(retConverted[0].timezone).format('Z')

// 						  		};

// 								return_result(newObj);

// 							});
// 						});

// 					}

// 				});

// 			}

// 		});
	
// 	}
// 	catch(err)
// 	{ 
// 		console.log("getMasterTimeInfo",err); 
// 		return_result(''); 
// 	}	
// }

methods.getMasterTimeInfo = function(arrParams,return_result)
{ 
	var newObj = {};

	try
	{
		if(parseInt(arrParams.master_city_id) !== 0)
		{	
			var query = `SELECT m.*,country.country_name,state.id AS state_id, state.state_name,c.city_name 
				FROM  time_information_status_master m 
				INNER JOIN country country ON country.id = m.country_id 
				INNER JOIN cities c ON c.id = m.city_id 
				INNER JOIN states state ON state.id = c.state_id 
			WHERE  m.status = '1' AND  country.status = '1'  AND  c.status = '1' AND m.timezone != '' AND m.city_id =?`;

			DB.query(query, [arrParams.master_city_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				  	var date = new Date(Date.parse(new Date().toLocaleString('en-US', { timeZone: data[0].timezone })));
				
			 		newObj = {
			  			city_id:data[0].city_id.toString(),
			  			name:data[0].city_name,
			  			time_zone:data[0].timezone,
			  			day: getDay(date),
			  			date:formatDate(date),
			  			gmt:'GMT '+moment().tz(data[0].timezone).format('Z')

			  		};

					return_result(newObj);

				}else{

					var quer = `SELECT co.country_name, 
						co.id AS country_id, 
						co.timezone AS timezone,
						c.id AS city_id,
						c.city_name AS city_name,
						s.id AS state_id,
						s.state_name AS state_name
					FROM country AS co 
					INNER JOIN states AS s ON co.id = s.country_id
					INNER JOIN cities AS c ON c.state_id = s.id
					WHERE  co.status = '1' AND c.status = '1'  AND co.timezone != '' AND c.id=?`;

					DB.query(quer, [arrParams.master_city_id], function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

						  	var date = new Date(Date.parse(new Date().toLocaleString('en-US', { timeZone: data[0].timezone })));
						
					 		newObj = {
					  			city_id:data[0].city_id.toString(),
					  			name:data[0].city_name,
					  			time_zone:data[0].timezone,
					  			day: getDay(date),
					  			date:formatDate(date),
					  			gmt:'GMT '+moment().tz(data[0].timezone).format('Z')

					  		};

							return_result(newObj);

						}else{
							return_result('');
						}

					});
				}

			});
		}
		else
		{ 
			return_result('');
		}
	}
	catch(err)
	{ 
		console.log("getMasterTimeInfo",err); 
		return_result(''); 
	}	
}



// methods.getChildTimeInfo = function(arrParams,return_result)
// { 
// 	try
// 	{

// 		var arrRows = [];
// 		var newObj = {};
// 		var delay = 100;

// 		if(parseInt(arrParams.account_id) !== 0)
// 		{
// 			var query = `SELECT t1.* FROM(
// 					SELECT group_id,
// 					(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
// 					(SELECT c.meta_value FROM jaze_user_timeinfo_history c WHERE c.meta_key = 'timezone_name' AND c.group_id = jaze_user_timeinfo_history.group_id) AS timezone_name,
// 					(SELECT c.meta_value FROM jaze_user_timeinfo_history c WHERE c.meta_key = 'city_id' AND c.group_id = jaze_user_timeinfo_history.group_id) AS city_id,
// 					(SELECT c.meta_value FROM jaze_user_timeinfo_history c WHERE c.meta_key = 'sort_order' AND c.group_id = jaze_user_timeinfo_history.group_id) AS sort_order
// 					FROM  jaze_user_timeinfo_history
// 					WHERE group_id IN (SELECT group_id FROM  jaze_user_timeinfo_history  WHERE meta_key = 'account_id' AND meta_value =? )
// 					AND  group_id IN (SELECT group_id FROM  jaze_user_timeinfo_history  WHERE meta_key = 'status' AND meta_value ='1' )
// 				) AS t1 WHERE account_id != "" `;
			   
// 				DB.query(query, [arrParams.account_id], function(data, error){

// 					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
// 					{
						
// 						var child_count = parseInt(data.length);
// 					  	delay = parseInt(data.length)*20;
// 						const forEach = async () => {
// 						  	const values = data;

// 						  	for (const val of values){
									

// 								methods.getCityInfo(val.city_id, function(retCity){	

// 								  	var dateM = new Date(Date.parse(new Date().toLocaleString('en-US', { timeZone: val.timezone_name })));
								  	
// 							  		newObj = {
// 							  			sort_order:val.sort_order,
// 							  			city_id:val.city_id.toString(),
// 							  			name:retCity,
// 							  			time_zone:val.timezone_name,
// 							  			day: getDay(dateM),
// 							  			date:formatDate(dateM),
// 							  			gmt:'GMT '+moment().tz(val.timezone_name).format('Z')

// 							  		};

// 									arrRows.push(newObj);
// 								});
							
// 						  	}

// 					  		const result = await returnData(arrRows);
// 						}
						 
// 						const returnData = x => {
// 						  return new Promise((resolve, reject) => {
// 						    setTimeout(() => {
// 						      resolve(x);
// 						    }, delay);
// 						  });
// 						}
						 
// 						forEach().then(() => {

// 							var sortedArray = arrRows.sort(function(a, b) {
// 							    return a.sort_order > b.sort_order ? 1 : a.sort_order < b.sort_order ? -1 : 0;
// 							});

// 							if( child_count != 10 || child_count < 10) {

// 								var limit = 10 - child_count;

// 								var sort_order = 0;
// 								var quer = `SELECT m.*,country.country_name,state.id AS state_id, state.state_name,c.city_name 
// 									FROM  time_information_status_master m 
// 									INNER JOIN country country ON country.id = m.country_id 
// 									INNER JOIN cities c ON c.id = m.city_id 
// 									INNER JOIN states state ON state.id = c.state_id 
// 								WHERE  m.status = '1' AND  country.status = '1'  AND  c.status = '1' AND m.timezone != ''
// 								AND m.city_id !=?
// 								LIMIT ?`;

// 								DB.query(quer, [arrParams.master_city_id,limit], function(dataC, error){

// 									var	delayC = parseInt(dataC.length)*20;
// 									const forEach = async () => {
// 									  	const valuesC = dataC;

// 									  	for (const valC of valuesC){

// 										 	sort_order++;
// 									 	 	var dateC = new Date(Date.parse(new Date().toLocaleString('en-US', { timeZone: valC.timezone })));

// 									  		newObj = {
// 									  			sort_order: parseInt(sort_order+child_count).toString(),
// 									  			city_id:valC.city_id.toString(),
// 									  			name:valC.city_name,
// 									  			time_zone:valC.timezone,
// 								  				day: getDay(dateC),
// 									  			date:formatDate(dateC),
// 								  				gmt:'GMT '+moment().tz(valC.timezone).format('Z')

// 									  		};

// 											sortedArray.push(newObj);
										
// 									  	}

// 								  		const result = await returnData(sortedArray);
// 									}
									 
// 									const returnData = x => {
// 									  return new Promise((resolve, reject) => {
// 									    setTimeout(() => {
// 									      resolve(x);
// 									    }, delayC);
// 									  });
// 									}
									 
// 									forEach().then(() => {

// 										return_result(sortedArray);

// 									})

// 								});

// 							}

							
// 						})

// 					}
// 					else
// 					{

// 						var sort_order = 0;
// 						var quer = `SELECT m.*,country.country_name,state.id AS state_id, state.state_name,c.city_name 
// 							FROM  time_information_status_master m 
// 							INNER JOIN country country ON country.id = m.country_id 
// 							INNER JOIN cities c ON c.id = m.city_id 
// 							INNER JOIN states state ON state.id = c.state_id 
// 						WHERE  m.status = '1' AND  country.status = '1'  AND  c.status = '1' AND m.timezone != ''
// 						LIMIT 10`;

// 						DB.query(quer, [arrParams.city_id], function(dataC, error){
// 							var	delayC = parseInt(dataC.length)*20;
// 							const forEach = async () => {
// 							  	const valuesC = dataC;

// 							  	for (const valC of valuesC){

// 								 	sort_order++;
// 							 		var date = new Date(Date.parse(new Date().toLocaleString('en-US', { timeZone: valC.timezone })));
// 							  		newObj = {
// 							  			sort_order: sort_order.toString(),
// 							  			city_id:valC.city_id.toString(),
// 							  			name:valC.city_name,
// 							  			time_zone:valC.timezone,
// 							  			day: getDay(date),
// 						  				date:formatDate(date),
// 							  			gmt:valC.gmt

// 							  		};

// 									arrRows.push(newObj);
								
// 							  	}

// 						  		const result = await returnData(arrRows);
// 							}
							 
// 							const returnData = x => {
// 							  return new Promise((resolve, reject) => {
// 							    setTimeout(() => {
// 							      resolve(x);
// 							    }, delayC);
// 							  });
// 							}
							 
// 							forEach().then(() => {

// 								return_result(arrRows);

// 							})

// 						});

// 					}

// 				});
// 		}
// 		else
// 		{ 
// 			var sort_order = 0;
// 			var quer = `SELECT m.*,country.country_name,state.id AS state_id, state.state_name,c.city_name 
// 				FROM  time_information_status_master m 
// 				INNER JOIN country country ON country.id = m.country_id 
// 				INNER JOIN cities c ON c.id = m.city_id 
// 				INNER JOIN states state ON state.id = c.state_id 
// 			WHERE  m.status = '1' AND  country.status = '1'  AND  c.status = '1' AND m.timezone != ''
// 			LIMIT 10`;

// 			DB.query(quer, [arrParams.city_id], function(dataC, error){
// 				var	delayC = parseInt(dataC.length)*20;
// 				const forEach = async () => {
// 				  	const valuesC = dataC;

// 				  	for (const valC of valuesC){

// 					 	sort_order++;
// 				 		var date = new Date(Date.parse(new Date().toLocaleString('en-US', { timeZone: valC.timezone })));
// 				  		newObj = {
// 				  			sort_order: sort_order.toString(),
// 				  			city_id:valC.city_id.toString(),
// 				  			name:valC.city_name,
// 				  			time_zone:valC.timezone,
// 				  			day: getDay(date),
// 			  				date:formatDate(date),
// 				  			gmt:valC.gmt

// 				  		};

// 						arrRows.push(newObj);
					
// 				  	}

// 			  		const result = await returnData(arrRows);
// 				}
				 
// 				const returnData = x => {
// 				  return new Promise((resolve, reject) => {
// 				    setTimeout(() => {
// 				      resolve(x);
// 				    }, delayC);
// 				  });
// 				}
				 
// 				forEach().then(() => {

// 					return_result(arrRows);

// 				})

// 			});

			
// 		}
// 	}
// 	catch(err)
// 	{ 
// 		console.log("getTimeInfo",err); 
// 		return_result("0"); 
// 	}	
// }

methods.getChildTimeInfo = function(arrParams,return_result)
{ 
	try
	{

		var arrRows = [];
		var newObj = {};
		var delay = 100;

		if(parseInt(arrParams.account_id) !== 0)
		{
			var query = `SELECT t1.* FROM(
					SELECT group_id,
					(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
					(SELECT c.meta_value FROM jaze_user_timeinfo_history c WHERE c.meta_key = 'timezone_name' AND c.group_id = jaze_user_timeinfo_history.group_id) AS timezone_name,
					(SELECT c.meta_value FROM jaze_user_timeinfo_history c WHERE c.meta_key = 'city_id' AND c.group_id = jaze_user_timeinfo_history.group_id) AS city_id,
					(SELECT c.meta_value FROM jaze_user_timeinfo_history c WHERE c.meta_key = 'sort_order' AND c.group_id = jaze_user_timeinfo_history.group_id) AS sort_order
					FROM  jaze_user_timeinfo_history
					WHERE group_id IN (SELECT group_id FROM  jaze_user_timeinfo_history  WHERE meta_key = 'account_id' AND meta_value =? )
					AND  group_id IN (SELECT group_id FROM  jaze_user_timeinfo_history  WHERE meta_key = 'status' AND meta_value ='1' )
				) AS t1 WHERE account_id != "" `;
			   
				DB.query(query, [arrParams.account_id], function(data, error){


					if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
					{
						
						var child_count = parseInt(data.length);
					  	delay = parseInt(data.length)*20;
						const forEach = async () => {
						  	const values = data;

						  	for (const val of values){
									

								methods.getCityInfo(val.city_id, function(retCity){	

								  	var dateM = new Date(Date.parse(new Date().toLocaleString('en-US', { timeZone: val.timezone_name })));
								  	
							  		newObj = {
							  			sort_order:val.sort_order,
							  			city_id:val.city_id.toString(),
							  			name:retCity,
							  			time_zone:val.timezone_name,
							  			day: getDay(dateM),
							  			date:formatDate(dateM),
							  			gmt:'GMT '+moment().tz(val.timezone_name).format('Z')

							  		};

									arrRows.push(newObj);
								});
							
						  	}

					  		const result = await returnData(arrRows);
						}
						 
						const returnData = x => {
						  return new Promise((resolve, reject) => {
						    setTimeout(() => {
						      resolve(x);
						    }, delay);
						  });
						}
						 
						forEach().then(() => {

							var sortedArray = arrRows.sort(function(a, b) {
							    return a.sort_order > b.sort_order ? 1 : a.sort_order < b.sort_order ? -1 : 0;
							});

							if( child_count != 10 || child_count < 10) {

								var limit = 10 - child_count;

								var sort_order = 0;
				
								var quer = `SELECT t1.* FROM(
										SELECT group_id,
										(SELECT c.meta_value FROM jaze_master_time_information c WHERE c.meta_key = 'timezone' AND c.group_id = jaze_master_time_information.group_id) AS timezone,
										(SELECT c.meta_value FROM jaze_master_time_information c WHERE c.meta_key = 'city_id' AND c.group_id = jaze_master_time_information.group_id) AS city_id
									FROM  jaze_master_time_information
									WHERE  group_id IN (SELECT group_id FROM  jaze_master_time_information  WHERE meta_key = 'city_id' AND meta_value !=? )
									) AS t1  GROUP BY group_id LIMIT ?`;

								DB.query(quer, [arrParams.master_city_id,limit], function(dataC, error){

					
									var	delayC = parseInt(dataC.length)*20;
									const forEach = async () => {
									  	const valuesC = dataC;

									  	for (const valC of valuesC){

								  			methods.getCityInfo(valC.city_id, function(retCity){	

											 	sort_order++;
										 	 	var dateC = new Date(Date.parse(new Date().toLocaleString('en-US', { timeZone: valC.timezone })));

										  		newObj = {
										  			sort_order: parseInt(sort_order+child_count).toString(),
										  			city_id:valC.city_id.toString(),
										  			name:retCity,
										  			time_zone:valC.timezone,
									  				day: getDay(dateC),
										  			date:formatDate(dateC),
									  				gmt:'GMT '+moment().tz(valC.timezone).format('Z')

										  		};
												sortedArray.push(newObj);

										  	});
										
									  	}

								  		const result = await returnData(sortedArray);
									}
									 
									const returnData = x => {
									  return new Promise((resolve, reject) => {
									    setTimeout(() => {
									      resolve(x);
									    }, delayC);
									  });
									}
									 
									forEach().then(() => {
										return_result(sortedArray);
									})
							
								});

							}

							
						})

					}
					else
					{


						var sort_order = 0;

						var quer = `SELECT t1.* FROM(
								SELECT group_id,
								(SELECT c.meta_value FROM jaze_master_time_information c WHERE c.meta_key = 'timezone' AND c.group_id = jaze_master_time_information.group_id) AS timezone,
								(SELECT c.meta_value FROM jaze_master_time_information c WHERE c.meta_key = 'city_id' AND c.group_id = jaze_master_time_information.group_id) AS city_id
							FROM  jaze_master_time_information
							WHERE  group_id IN (SELECT group_id FROM  jaze_master_time_information  WHERE meta_key = 'city_id' AND meta_value !=? )
							) AS t1  GROUP BY group_id LIMIT 10`;

						DB.query(quer, [arrParams.master_city_id], function(dataC, error){
							var	delayC = parseInt(dataC.length)*20;
							const forEach = async () => {
							  	const valuesC = dataC;

							  	for (const valC of valuesC){

							  		methods.getCityInfo(valC.city_id, function(retCity){	

								 		sort_order++;
								 		var date = new Date(Date.parse(new Date().toLocaleString('en-US', { timeZone: valC.timezone })));
								  		newObj = {
								  			sort_order: sort_order.toString(),
								  			city_id:valC.city_id.toString(),
								  			name:retCity,
								  			time_zone:valC.timezone,
								  			day: getDay(date),
							  				date:formatDate(date),
								  			gmt:valC.gmt

								  		};

										arrRows.push(newObj);
								 	});
							  	}

						  		const result = await returnData(arrRows);
							}
							 
							const returnData = x => {
							  return new Promise((resolve, reject) => {
							    setTimeout(() => {
							      resolve(x);
							    }, delayC);
							  });
							}
							 
							forEach().then(() => {

								return_result(arrRows);

							})

						});

					}

				});
		}
		else
		{ 
			var sort_order = 0;

			var quer = `SELECT t1.* FROM(
				SELECT group_id,
				(SELECT c.meta_value FROM jaze_master_time_information c WHERE c.meta_key = 'timezone' AND c.group_id = jaze_master_time_information.group_id) AS timezone,
				(SELECT c.meta_value FROM jaze_master_time_information c WHERE c.meta_key = 'city_id' AND c.group_id = jaze_master_time_information.group_id) AS city_id
			FROM  jaze_master_time_information
			WHERE  group_id IN (SELECT group_id FROM  jaze_master_time_information  WHERE meta_key = 'city_id' AND meta_value !=? )
			) AS t1  GROUP BY group_id LIMIT 10`;
			
			DB.query(quer, [arrParams.master_city_id], function(dataC, error){
				var	delayC = parseInt(dataC.length)*20;
				const forEach = async () => {
				  	const valuesC = dataC;

				  	for (const valC of valuesC){

				  		methods.getCityInfo(valC.city_id, function(retCity){	

						 	sort_order++;
					 		var date = new Date(Date.parse(new Date().toLocaleString('en-US', { timeZone: valC.timezone })));
					  		newObj = {
					  			sort_order: sort_order.toString(),
					  			city_id:valC.city_id.toString(),
					  			name:retCity,
					  			time_zone:valC.timezone,
					  			day: getDay(date),
				  				date:formatDate(date),
					  			gmt:valC.gmt

					  		};

							arrRows.push(newObj);
					  	});
				  	}

			  		const result = await returnData(arrRows);
				}
				 
				const returnData = x => {
				  return new Promise((resolve, reject) => {
				    setTimeout(() => {
				      resolve(x);
				    }, delayC);
				  });
				}
				 
				forEach().then(() => {

					return_result(arrRows);

				})

			});
		}
	}
	catch(err)
	{ 
		console.log("getChildTimeInfo",err); 
		return_result("0"); 
	}	
}

methods.updateTimeHistoryDetails = function(arrParams,return_result)
{ 
	var newObj = {};

	try
	{
		if(parseInt(arrParams.account_id) !== 0 && parseInt(arrParams.sort_order) !==0)
		{	
			var query = `SELECT t1.* FROM(
				SELECT group_id,
				(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
				(SELECT c.meta_value FROM jaze_user_timeinfo_history c WHERE c.meta_key = 'timezone_name' AND c.group_id = jaze_user_timeinfo_history.group_id) AS timezone_name,
				(SELECT c.meta_value FROM jaze_user_timeinfo_history c WHERE c.meta_key = 'city_id' AND c.group_id = jaze_user_timeinfo_history.group_id) AS city_id,
				(SELECT c.meta_value FROM jaze_user_timeinfo_history c WHERE c.meta_key = 'sort_order' AND c.group_id = jaze_user_timeinfo_history.group_id) AS sort_order
				FROM  jaze_user_timeinfo_history
				WHERE group_id IN (SELECT group_id FROM  jaze_user_timeinfo_history  WHERE meta_key = 'account_id' AND meta_value =? )
				AND  group_id IN (SELECT group_id FROM  jaze_user_timeinfo_history  WHERE meta_key = 'sort_order' AND meta_value =? )
				AND  group_id IN (SELECT group_id FROM  jaze_user_timeinfo_history  WHERE meta_key = 'status' AND meta_value ='1' )
			) AS t1 WHERE account_id != ""`;

			DB.query(query, [arrParams.account_id,arrParams.sort_order], function(data, error){


				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
				{

					var group_id = data[0].group_id;
		
					methods.getCountryStateCityInfo(arrParams.city_id, function(retFullInfo){

						var	arrInsert = {
							account_id:arrParams.account_id,
							city_id:retFullInfo.city_id,
							timezone_name:retFullInfo.time_zone,
							sort_order:arrParams.sort_order,
							status:1
						};

						DB.query("DELETE FROM jaze_user_timeinfo_history WHERE group_id =?", [group_id], function(data, error){

						 	if(parseInt(data.affectedRows) > 0){

			 			  		var querInsert = `insert into jaze_user_timeinfo_history (group_id, meta_key, meta_value) VALUES (?,?,?)`;

							 	for (var key in arrInsert) 
							 	{
									DB.query(querInsert,[group_id,key,arrInsert[key]], function(data, error){

							 		 	if(parseInt(data.affectedRows) > 0){
											return_result(data.affectedRows)
								 	   	}else{
							   				return_result(null);
								 	   	}
							 	 	});
							  	};

					 	   	}else{
				   				return_result(null);
					 	   	}
					
						});
					});

				}
				else
				{
				
					var query = `SELECT t1.* FROM(
						SELECT group_id,
						(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
						(SELECT c.meta_value FROM jaze_user_timeinfo_history c WHERE c.meta_key = 'sort_order' AND c.group_id = jaze_user_timeinfo_history.group_id) AS sort_order
						FROM  jaze_user_timeinfo_history
						WHERE group_id IN (SELECT group_id FROM  jaze_user_timeinfo_history  WHERE meta_key = 'account_id' AND meta_value =? )
						AND  group_id IN (SELECT group_id FROM  jaze_user_timeinfo_history  WHERE meta_key = 'status' AND meta_value ='1' )
					) AS t1 WHERE account_id != "" ORDER BY sort_order DESC LIMIT 1`;

					DB.query(query, [arrParams.account_id], function(data, error){

						if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
						{

							var timeParams = {city_id:arrParams.city_id, sort_order:data[0].sort_order,account_id:arrParams.account_id};
							methods.insertTimeHistory(timeParams, function(retUpdated){
								return_result(retUpdated);

							});

						}
						else
						{

							var timeParams = {city_id:arrParams.city_id, sort_order:0,account_id:arrParams.account_id};
							methods.insertTimeHistory(timeParams, function(retUpdated){
								return_result(retUpdated);
							});
						}


					});

				}

			});
		}
		else
		{ 
			return_result(null); 
		}
	}
	catch(err)
	{ 
		console.log("updateTimeHistoryDetails",err); 
		return_result(''); 
	}	
}


methods.insertTimeHistory = function(arrParams,return_result)
{ 

	try
	{

		if(parseInt(arrParams.city_id) !== 0 ){

			methods.getCountryStateCityInfo(arrParams.city_id, function(retAllInfo){
		
				var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_user_timeinfo_history";
		  		var lastGrpId = 0;

				methods.getLastGroupId(queGroupMax, function(queGrpRes){

				  	queGrpRes.forEach(function(row) {
						lastGrpId = row.lastGrpId;
				  	});

					var	arrInsert = {
						account_id:arrParams.account_id,
						city_id:retAllInfo.city_id,
						timezone_name:retAllInfo.time_zone,
						sort_order:parseInt(arrParams.sort_order)+1,
						status:1
					};

			
					var plusOneGroupID = lastGrpId+1;

			  		var querInsert = `insert into jaze_user_timeinfo_history (group_id, meta_key, meta_value) VALUES (?,?,?)`;

				 	for (var key in arrInsert) 
				 	{
						DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){

				 		 	if(parseInt(data.affectedRows) > 0){
								return_result(data.affectedRows)
					 	   	}else{
				   				return_result(null);
					 	   	}
				 	 	});
				  	};

					

				});

			});

		}else{return_result(null); }

	}
	catch(err)
	{ 
		console.log("insertTimeHistory",err); 
		return_result(null); 
	}	
}


methods.getCountryStateCityInfo = function(city_id,return_result)
{ 
	var newObj = {};

	try
	{
		if( parseInt(city_id) !==0 )
		{	
			var query = `SELECT co.timezone,co.id AS country_id, s.id AS state_id ,c.id AS city_id, c.city_name
			FROM  country AS co 
				INNER JOIN states AS s ON s.country_id = co.id
				INNER JOIN cities AS c ON c.state_id = s.id 
			WHERE c.id =? AND co.timezone >''`;

			DB.query(query, [city_id], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){
				
			 		newObj = {
			  			city_id:data[0].city_id.toString(),
			  			name:data[0].city_name,
			  			time_zone:data[0].timezone
			  		};

					return_result(newObj);

				}else{
					return_result(null);
				}

			});
		}
		else
		{ 
			return_result(null); 
		}
	}
	catch(err)
	{ 
		console.log("getCountryStateCityInfo",err); 
		return_result(null); 
	}	
}


methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
}

function formatDate(current_datetime) {
	const months = ["January", "February", "March","April", "May", "June", "July", "August", "September", "October", "November", "December"];
	let formatted_date = current_datetime.getDate() + " " + months[current_datetime.getMonth()] + " " + current_datetime.getFullYear()
	return formatted_date;
}

function getDay(date){

	const days = ["Sunday", "Monday", "Tuesday","Wednesday", "Thursday", "Friday", "Saturday"];

	var dt = new Date(date);
	return days[dt.getDay()];
}

function sortingArrayObject(key, order='asc') {

	return function(a, b) {
	  if(!a.hasOwnProperty(key) || 
		 !b.hasOwnProperty(key)) {
		  return 0; 
	  }
	  
	  const varA = (typeof a[key] === 'string') ? 
		a[key].toUpperCase() : a[key];
	  const varB = (typeof b[key] === 'string') ? 
		b[key].toUpperCase() : b[key];
		
	  let comparison = 0;
	  if (varA > varB) {
		comparison = 1;
	  } else if (varA < varB) {
		comparison = -1;
	  }
	  return (
		(order == 'desc') ? 
		(comparison * -1) : comparison
	  );
	};
}

methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].group_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}


methods.__getCityDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT city_name FROM cities  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].city_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};
module.exports = methods;

