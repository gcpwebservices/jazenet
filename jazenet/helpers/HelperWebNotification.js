const methods = {};

const DB = require('../../helpers/db_base.js');



const HelperRestriction = require('../helpers/HelperRestriction.js');

//const serviceAccount = require('../service_account/jazenet-pushnotification-firebase-adminsdk-esihm-cb1e468724.json');
// const fcm = require('fcm-notification');
// const FCM = new fcm(serviceAccount);
var promiseForeach = require('promise-foreach');





methods.getAccountDetails = function(arrParams,return_result){



	var queAccountDetails = '';

	if(arrParams.account_type == '1' || arrParams.account_type == '2'){



		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 

			MAX(CASE WHEN account_type THEN account_type END) AS account_type, 

			CONCAT(MAX(CASE WHEN meta_key = 'fname' THEN meta_value END),' ',MAX(CASE WHEN meta_key = 'lname' THEN meta_value END)) AS fullname,

			MAX(CASE WHEN meta_key = 'profile_logo' THEN meta_value END) AS logo 

			FROM jaze_user_basic_details WHERE account_id=?`;



	}else{



		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 

			MAX(CASE WHEN account_type THEN account_type END) AS account_type,

			MAX(CASE WHEN meta_key = 'company_name' THEN meta_value END) AS fullname, 

			MAX(CASE WHEN meta_key = 'company_logo' THEN meta_value END) AS  logo  

			FROM jaze_user_basic_details WHERE account_id =?`;

	}



	DB.query(queAccountDetails,[arrParams.account_id], function(data, error){



	   	if(data != null){



			var newObj = {};

			var logo = '';

			var fullnmae = '';



		  	data.forEach(function(val,key,arr) {



		   		if(val.logo){

			  		if(val.logo != ''){

			  			if(val.account_type == '1'){

			  				logo = G_STATIC_IMG_URL+'uploads/jazenet/individual_account/members_thumb/thumb_'+val.logo;

			  			}else{

			  				logo = G_STATIC_IMG_URL+'uploads/companylogo/'+val.logo;

			  			}

			  		}

		  		}



		  		if(val.fullname){

	  				if(val.fullname !=''){

						fullnmae = val.fullname;

		  			}

		  		}

		  	

				newObj = { 

					account_id: val.account_id.toString(),

					account_type: val.account_type.toString(),

					name: fullnmae,

					logo: logo,

				};

		  	});



	 		return_result(newObj);

	

 	   	}else{

 	   		return_result(null);

 	   	}



	});

};

methods.getAccountDetailsFull = function(arrParams,return_result){



	var queAccountDetails = '';

	if(arrParams.account_type == '1' || arrParams.account_type == '2'){



		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 

			MAX(CASE WHEN account_type THEN account_type END) AS account_type, 

			CONCAT(MAX(CASE WHEN meta_key = 'fname' THEN meta_value END),' ',MAX(CASE WHEN meta_key = 'lname' THEN meta_value END)) AS fullname,

			MAX(CASE WHEN meta_key = 'profile_logo' THEN meta_value END) AS logo,

			MAX(CASE WHEN meta_key = 'email' THEN meta_value END) AS email,

			MAX(CASE WHEN meta_key = 'mobile' THEN meta_value END) AS mobile,

			MAX(CASE WHEN meta_key = 'mobile_country_code_id' THEN meta_value END) AS country_code

			FROM jaze_user_basic_details WHERE account_id=?`;



	}else{



		queAccountDetails = `SELECT MAX(CASE WHEN account_id THEN account_id END) AS account_id, 

			MAX(CASE WHEN account_type THEN account_type END) AS account_type,

			MAX(CASE WHEN meta_key = 'company_name' THEN meta_value END) AS fullname, 

			MAX(CASE WHEN meta_key = 'company_logo' THEN meta_value END) AS  logo,

			MAX(CASE WHEN meta_key = 'company_email' THEN meta_value END) AS email,

			MAX(CASE WHEN meta_key = 'company_phone' THEN meta_value END) AS mobile,

			MAX(CASE WHEN meta_key = 'company_phone_code' THEN meta_value END) AS country_code

			FROM jaze_user_basic_details WHERE account_id =?`;

	}



	DB.query(queAccountDetails,[arrParams.account_id], function(data, error){



	   	if(data != null){



			var newObj = {};

			var logo = G_STATIC_IMG_URL +'uploads/jazenet/dummy_profile.png';

			var newPhoneCode;

			var fullname = '';

			

		  	data.forEach(function(val,key,arr) {



		  		if(val.logo != ''){

		  			if(val.account_type == '1'){

		  				logo = G_STATIC_IMG_URL+'uploads/jazenet/individual_account/members_thumb/thumb_'+val.logo;

		  			}else{

		  				logo = G_STATIC_IMG_URL+'uploads/companylogo/'+val.logo;

		  			}

		  		}



		  		if(val.fullname!=''){

		  			fullname = val.fullname;

		  		}



  				methods.getMobileCountryCode(val.country_code, function(phone_code){



  					if(phone_code!=null){

  						newPhoneCode = '+'+phone_code+val.mobile;

  					}else{

  						newPhoneCode = val.mobile;

  					}



					newObj = { 

						app_id: arrParams.new_app_id.toString(),

						api_token: arrParams.api_token,

						account_id: val.account_id.toString(),

						account_type: val.account_type.toString(),

						name: fullname,

						logo: logo,

						email: val.email,

						mobile: newPhoneCode

					};



					return_result(newObj);

		  		});



 	

		  	});



 	   	}else{

 	   		return_result(null);

 	   	}



	});

};

methods.getAccountTypes = function(arrParams,return_result){





	var arrRows = [];

	var newObj = {};

	var arrAccountParams = {};

	var queLinkAccoType = `SELECT account_id,account_type FROM jaze_user_credential_details WHERE account_id =? `;

	DB.query(queLinkAccoType,[arrParams.account_id], function(data, error){



	   	if(data != null){



	   		function asyncFunction (item, cb) {

			  setTimeout(() => {

					return_result(item);

				cb();

			  }, 100);

			}



			let requests = data.map((val, key, array) => {

				arrAccountParams = { account_id:val.account_id, account_type:val.account_type};

				methods.getAccountDetails(arrAccountParams, function(returned){		



			    	return new Promise((resolve) => {

			      		asyncFunction(returned, resolve);

				    });



				});

			})



			Promise.all(requests);

	

 	   	}else{

 	   		return_result(null);

 	   	}



	});

};

methods.getFcmToken = function(arrTemplate,arrParams, return_result){

	var arrRows = [];

	var quer = `SELECT t1.* FROM(
		SELECT group_id,
		(CASE WHEN meta_key = 'account_id' THEN meta_value END) AS account_id,
		(SELECT c.meta_value FROM jaze_user_login_history c WHERE c.meta_key = 'fcm_token' AND c.group_id = jaze_user_login_history.group_id) AS fcm_token
		FROM  jaze_user_login_history
		WHERE group_id IN (SELECT group_id FROM  jaze_user_login_history  WHERE meta_key = 'account_id' AND meta_value =? )
		AND group_id IN (SELECT group_id FROM  jaze_user_login_history  WHERE meta_key = 'status' AND meta_value ='1' )
		) AS t1 WHERE account_id != "" AND fcm_token !='' ORDER BY group_id DESC`;

	DB.query(quer,[arrParams.request_account_id], function(data, error){

		if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
		{	
			const forEach = async () => {
			  	const values = data;
			  	for (const val of values){
					arrRows.push(val.fcm_token);
			  	}

			  	const result = await returnData(arrRows);
			}
			 
			const returnData = x => {
			  return new Promise((resolve, reject) => {
			    setTimeout(() => {
			      resolve(x);
			    }, 100);
			  });
			}
			 
			forEach().then(() => {
				return_result(null);
				// FCM.sendToMultipleToken(arrTemplate, arrRows, function(err, response) {
				//     if(err){
				//         console.log('err--', err);
		        // 		return_result(null);
				//     }else {
				//         console.log('response-----', response);
		        // 		return_result(response);
				//     }
				 
				// })

			})
 	   	}
 	   	else
 	   	{
 	   		return_result(null);
 	   	}

	});

};

methods.getFcmTokenWeb = function(arrTemplate,arrParams){

	try
	{
		var query = " SELECT * FROM  jaze_user_login_history ";
		    query +=" WHERE group_id IN (SELECT group_id FROM  jaze_user_login_history  WHERE meta_key = 'account_id' AND meta_value = '"+arrParams.request_account_id+"' ) ";
			query +=" AND group_id IN (SELECT group_id FROM  jaze_user_login_history  WHERE meta_key = 'status' AND meta_value ='1' ) ";

			DB.query(query, null, function (data, error) {

				if (typeof data !== 'undefined' && data !== null && parseInt(data.length) !== 0)
				{ 
					HelperRestriction.convertMultiMetaArray(data, function(data_result){

						var counter = data_result.length;
						
						var array_rows = [];

							if(parseInt(counter) !== 0)
							{
									promiseForeach.each(data_result,[function (list) {
						
										return new Promise(function (resolve, reject) {
			
											array_rows.push(list.fcm_token);
																						
											counter -= 1;
																						
											resolve(array_rows);
										});
									}],
									function (result, current) {
										
										if (counter === 0)
										{  return_result(null);
											// FCM.sendToMultipleToken(arrTemplate, result[0], function(err, response) {
											// 	if(err){
											// 		console.log('web notification err--', err);
											// 		//return_result(null);
											// 	}else {
											// 		console.log('web notification response-----', response);
											// 		//return_result(response);
											// 	}
											// })
										}
									});
							}
					});
				}
			});
	}
	catch(err)
	{ 
		console.log("template error: " + err);
	}
};





module.exports = methods;