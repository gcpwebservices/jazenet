const methods = {};
const DB = require('../../helpers/db_base.js');
const HelperNotification = require('../../jazecom/helpers/HelperNotification.js');

const promiseForeach = require('promise-foreach');
const dateFormat = require('dateformat');
const now = new Date();
const standardDT = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
const newdateForm = dateFormat(new Date(standardDT), 'yyyy-mm-dd HH:MM:ss');

const STATIC_IMG_URL = "https://www.img.chaaby.com/images/master.resources/uploads/jazenet/"; 
const STATIC_IMG_URL_PROFILE = "https://www.img.chaaby.com/images/master.resources/"; 
const moment = require("moment");
const fs = require('fs');
const download = require('download-file');
const url = require('url');
const http = require('http');
const path = require('path');


methods.getCompanyProfile =  function(arrParams,return_result){
	try
	{
		
		var arrAccounts = [];

		var queryAccountType = `SELECT account_id,account_type FROM jaze_user_basic_details WHERE account_id =?`;

		DB.query(queryAccountType,[arrParams.account_id], function(data, error){
			

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				var queryAccDetails = `SELECT * FROM jaze_user_basic_details WHERE account_id =? and account_type=?`;

				DB.query(queryAccDetails,[data[0].account_id,data[0].account_type], function(allData, error){
	
					if(typeof allData !== 'undefined' && allData !== null && parseInt(allData.length) > 0)
					{

						methods.convertMultiMetaArrayCompany(allData, function(retConverted){	

							var counter = retConverted.length;		
							var jsonItems = Object.values(retConverted);

							promiseForeach.each(jsonItems,[function (val) {
								return new Promise(function (resolve, reject) {		
						
							  		var default_logo = '';

									var newObj = {};

							  		if(parseInt(data[0].account_type) === 1)
							  		{
							  			if(val.profile_logo){
											default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/individual_account/members_thumb/thumb_' + val.profile_logo + '';
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : (val.fname +" "+val.lname).toString(),
											first_name	  : val.fname.toString(),
											last_name	  : val.lname.toString(),
											logo		  : default_logo,
											currency	  : val.currency_id,
											category_id   : '',
											profession    : val.profession
										}	

							  		}
							  		else if(parseInt(data[0].account_type) === 2)
							  		{

				  						if(val.professional_logo){
											default_logo = G_STATIC_IMG_URL + "uploads/jazenet/professional_account/"+val.account_id+"/logo_thumb/thumb_" + val.professional_logo;
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : (val.fname +" "+val.lname).toString(),
											first_name	  : val.fname.toString(),
											last_name	  : val.lname.toString(),
											logo		  : default_logo,
											currency	  : val.currency_id,
											category_id   : '',
											profession    : val.profession
										}	

							  		}
						  			else if(parseInt(data[0].account_type) === 3)
					  				{

		  								if(val.listing_images){
				  							default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/listing_images/' + val.listing_images + '';
										
										}else if(val.company_logo){
											default_logo = G_STATIC_IMG_URL + 'uploads/jazenet/company/'+val.account_id+'/logo/' + val.company_logo + '';
										}else{
											default_logo = "";
										}

										newObj = {
											account_id	  : val.account_id.toString(),
											account_type  : data[0].account_type.toString(),
											name		  : val.company_name,
											first_name	  : '',
											last_name	  : '',
											logo		  : default_logo,
											currency	  : val.currency_id,
											category_id   : val.category_id,
											profession    : val.profession
										}	



					  				}

									counter -= 1;
									if (counter === 0){ 
										resolve(newObj);
									}
									
								})
							}],
							function (result, current) {
								if (counter === 0){ 
									return_result(result[0]);
								}
							});

						});


					}
					else
					{
						return_result(null);
					}
				});

		
			}
			else
			{
				return_result(null);
			}
		});

	}
	catch(err)
	{ console.log("getCompanyProfile",err); return_result(null);}
};


// invite to team // 



methods.inviteToTeam = function(arrParams,return_result){

	try
	{
			
		var queATeam = ` SELECT * FROM jaze_company_team_departments WHERE group_id IN (SELECT group_id FROM jaze_company_team_departments WHERE meta_key ='account_id' AND meta_value =?) `;
		DB.query(queATeam,[arrParams.account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{

				methods.convertMultiMetaArray(data, function(retConverted){

					var queRequest = ` SELECT * FROM jaze_company_team_details WHERE group_id IN (SELECT group_id FROM jaze_company_team_details WHERE meta_key ='account_id' AND meta_value =?) 
					AND group_id IN (SELECT group_id FROM jaze_company_team_details WHERE meta_key ='team_account_id' AND meta_value =?)`;

					DB.query(queRequest,[arrParams.account_id,arrParams.request_account_id], function(dataR, error){


						if(typeof dataR !== 'undefined' && dataR !== null && parseInt(dataR.length) > 0)
						{

							methods.convertMultiMetaArray(dataR, function(retConvertedR){
								if(parseInt(retConvertedR[0].status) === 0){
									return_result(4);
								}else if(parseInt(retConvertedR[0].status) === 1){
									return_result(5);
								}

							});
						}
						else
						{
							
							var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_company_team_details";
				  			var lastGrpId = 0;

							methods.getLastGroupId(queGroupMax, function(queGrpRes){

					  			lastGrpId = queGrpRes[0].lastGrpId;

					  			var paramReceiver = { account_id: arrParams.request_account_id };

					  			methods.getCompanyProfile(paramReceiver,function(retReceiver){

					  				if(retReceiver!=null)
					  				{
				  						var paramSender = { account_id: arrParams.account_id };
					  					methods.getCompanyProfile(paramSender,function(retSender){

							  				if(retSender!=null)
							  				{

								  				methods.getJazecomStatus(arrParams,function(retJazecom){

								  					var profession = '';
								  					if(retReceiver.profession){
								  						profession = retReceiver.profession;
								  					}

													var arrInsert = {
														account_id         : arrParams.account_id,
														team_account_id    : retReceiver.account_id,
														first_name         : retReceiver.first_name,
														last_name          : retReceiver.last_name,
														team_position      : profession,
														create_by          : arrParams.account_id,
														jdiary_status      : retJazecom.jaze_diary,
														jchat_status       : retJazecom.jaze_chat,
														jmail_status       : retJazecom.jaze_mail,
														jcall_status       : retJazecom.jaze_call,
														use_proff_profile  : 1,
														add_to_link        : 1,
														team_department_id : retConverted[0].group_id,
														team_logo_url      : '',
														create_date        : newdateForm,
														status             : 0
													};

															
													var plusOneGroupID = lastGrpId+1;
											  		var querInsert = `insert into jaze_company_team_details (group_id, meta_key, meta_value) VALUES (?,?,?)`;

												 	for (var key in arrInsert) 
												 	{
														DB.query(querInsert,[plusOneGroupID,key,arrInsert[key]], function(data, error){
												 		 	if(parseInt(data.affectedRows) > 0){
										 						return_result(1);
													 	   	}else{
											   					return_result(0);
													 	   	}
												 	 	});
												  	};

											  		var notiParams = {	
														group_id:plusOneGroupID.toString(),
														account_id: retSender.account_id.toString(),
														account_type: retSender.account_type.toString(),
														request_account_id: retReceiver.account_id.toString(),
														request_account_type: retReceiver.account_type.toString(),
														message: '',
														flag:'14'
													}; 

													HelperNotification.sendPushNotification(notiParams, function(retNoti){						
														console.log('notification status '+retNoti);
													});

								 				});
							 				}

						 				});
					 				}
					 				else
					 				{
										return_result(3);
					 				}

		 						});
							});

						}
					});
				});

			}
			else
			{
				return_result(2);
			}

		});

	}
	catch(err)
	{ 
		console.log("inviteToTeam",err); return_result(0); 
	}	
};


methods.getJazecomStatus = function(arrParams,return_result){

	try{	

		var jazecom = {
			jaze_chat  : '0',
			jaze_mail  : '0',
			jaze_feed  : '0',
			jaze_diary : '0',
			jaze_call  : '0'
		};

		let query  = "SELECT * FROM jaze_user_jazecome_status WHERE group_id IN (SELECT group_id FROM jaze_user_jazecome_status WHERE meta_key = 'account_id' AND meta_value =?) ";

		DB.query(query,[arrParams.request_account_id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

				methods.convertMultiMetaArray(data, function(retConverted){

					jazecom.jaze_chat = retConverted[0].jaze_chat;
					jazecom.jaze_mail = retConverted[0].jaze_mail;
					jazecom.jaze_feed = retConverted[0].jaze_feed;
					jazecom.jaze_diary = retConverted[0].jaze_diary;

					return_result(jazecom);

				});
				
			}
			else
			{
				return_result(jazecom);
			}
		});
	}
	catch(err)
	{
		return_result(jazecom);
	}

};


methods.teamAcceptReject = function(arrParams,return_result){

	try
	{

		var queATeam = ` SELECT * FROM jaze_company_team_details WHERE group_id =?`;
		DB.query(queATeam,[arrParams.id], function(data, error){

			if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0)
			{	
				methods.convertMultiMetaArray(data, function(retConverted){

					var senderParam = {account_id:arrParams.account_id};
	  				methods.getCompanyProfile(senderParam,function(retSender){

		  				if(retSender!=null)
		  				{

		  					var recParam = {account_id: retConverted[0].account_id};

  							methods.getCompanyProfile(recParam,function(retReceiver){
  								
				  				if(retReceiver!=null)
				  				{

									var query = `UPDATE jaze_company_team_details SET meta_value =1 WHERE meta_key ='status' AND group_id =?`;
									if(parseInt(arrParams.flag) === 2){
										query = `DELETE FROM jaze_company_team_details WHERE group_id=?`;
									}

									DB.query(query, [arrParams.id], function(data, error){
								 		if(data !== null){
							 				return_result(1);
								 	   	}else{
							 	   			return_result(0);
								 	   	}
									});


									var flag = '15';
									if(parseInt(arrParams.flag) === 2){
										flag = '16';
									}

						  			var notiParams = {	
										group_id             : retConverted[0].group_id.toString(),
										account_id           : retSender.account_id.toString(),
										account_type         : retSender.account_type.toString(),
										request_account_id   : retReceiver.account_id.toString(),
										request_account_type : retReceiver.account_type.toString(),
										message              : '',
										flag                 : flag
									}; 

									HelperNotification.sendPushNotification(notiParams, function(retNoti){						
										console.log('notification status '+retNoti);
									});

								}else{
									return_result(0);
								}

							});	

						}else{
							return_result(0);
						}

					});
				});
			}
			else
			{
				return_result(3);
			}

		});

	}
	catch(err)
	{ 
		console.log("teamAcceptReject",err); return_result(0); 
	}	
};



methods.getLastGroupId = function(qry,return_result){

	DB.query(qry, null, function(data, error){
 	   	if(data != null){
			return_result(data);
 	   	}else{
			return_result(0);
 	   	}
	});
};


methods.__getCountryDetails = function(arrParams,return_result){

	try{	

			if(arrParams ==''){
				arrParams = 0;
			}

			let query  = " SELECT country_name FROM country  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].country_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}

};

methods.__getStateDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT state_name FROM states  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].state_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.__getCityDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT city_name FROM cities  WHERE id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].city_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};


methods.__getAreaDetails = function(arrParams,return_result){

	try{
			let query  = " SELECT area_name FROM area  WHERE area_id =? ";

			DB.query(query,[arrParams], function(data, error){

				if(typeof data !== 'undefined' && data !== null && parseInt(data.length) > 0){

					return_result(data[0].area_name);
				}
				else
				{return_result('')}
			});
	}
	catch(err)
	{
		return_result('')
	}
};

methods.convertMultiMetaArray =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndex(list,arrvalues[i].group_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							group_id : arrvalues[i].group_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndex(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].group_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}


methods.convertMultiMetaArrayCompany =  function(arrvalues,return_result){
	try
	{
		if(typeof arrvalues !== 'undefined' && arrvalues !== null && parseInt(arrvalues.length) > 0)
		{
			var list = [];

			for (var i in arrvalues) 
			{
				var index = __getGroupIndexCompany(list,arrvalues[i].account_id);

				if(index != ""){
					
					Object.assign(list[index], {[arrvalues[i].meta_key]: arrvalues[i].meta_value});
				}
				else {

					var obj_return = {
							account_id : arrvalues[i].account_id,
							[arrvalues[i].meta_key] :  arrvalues[i].meta_value
						};

					list.push(obj_return);
				}									
			}
			
			return_result(list);
		}
		else
		{ return_result(null); }
	}
	catch(err)
	{ console.log("convertMetaArray",err); return_result(null);}
};


function __getGroupIndexCompany(list_array,index_value)
{
	var retern_index = "";

	if(list_array !== null)
	{
		Object.keys(list_array).forEach(function(prop) {

			var value = list_array[prop].account_id;

			if(parseInt(value) === parseInt(index_value))
			{
				retern_index = prop;
			}
		  });
	}
		
	return retern_index;
}

function sortingArrayObject(key, order='asc') {

	return function(a, b) {
	  if(!a.hasOwnProperty(key) || 
		 !b.hasOwnProperty(key)) {
		  return 0; 
	  }
	  
	  const varA = (typeof a[key] === 'string') ? 
		a[key].toUpperCase() : a[key];
	  const varB = (typeof b[key] === 'string') ? 
		b[key].toUpperCase() : b[key];
		
	  let comparison = 0;
	  if (varA > varB) {
		comparison = 1;
	  } else if (varA < varB) {
		comparison = -1;
	  }
	  return (
		(order == 'desc') ? 
		(comparison * -1) : comparison
	  );
	};
}


function removeDuplicates(array) {
  return array.filter((a, b) => array.indexOf(a) === b)
};




module.exports = methods;
