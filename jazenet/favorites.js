require('events').EventEmitter.prototype._maxListeners = 1000;
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const express = require('express');
const router = express.Router();

const bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

const HelperRestriction = require('./helpers/HelperRestriction.js');
const HelperFavorites = require('./helpers/HelperFavorites.js');

const { check, validationResult } = require('express-validator');

const dateFormat = require('dateformat');
const now = new Date();
const standardDT = dateFormat(now,'yyyy-mm-dd HH:MM:ss');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};


router.post('/favorites/add_to_favorites',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('fav_account_id').isNumeric(),
	check('fav_account_type').isNumeric(),
	check('fav_type').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered. dawd wd" 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  account_id = req.body.account_id;
	var  api_token = req.body.api_token;
	var  fav_account_id = req.body.fav_account_id;
	var  fav_account_type = req.body.fav_account_type;
	var  fav_type = req.body.fav_type;


	var arrParams = {
		account_id:account_id,
		api_token:api_token,
		fav_account_id:fav_account_id,
		fav_account_type:fav_account_type,
		fav_type:fav_type
	};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{
			HelperRestriction.checkAccountCredentials(arrParams, function(queCred){
				if(queCred.length)
				{	
				
					var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_user_favorite_list";
			  		var lastGrpId = 0;

					HelperFavorites.getLastGroupId(queGroupMax, function(queGrpRes){
	
					  	queGrpRes.forEach(function(row) {
							lastGrpId = row.lastGrpId;
					  	});

				  		var plusOneGroupID = lastGrpId+1;


						var	arrInsert = {
							account_id:account_id,
							fav_account_id:fav_account_id,
							fav_account_type:fav_account_type,
							fav_type:fav_type,
							create_date:standardDT,
							status:1
						};

				  		var querInsert = `insert into jaze_user_favorite_list (group_id, meta_key, meta_value) VALUES (?,?,?)`;
						HelperFavorites.insertResult(arrInsert,querInsert,plusOneGroupID, function(insertRes){	

							if(insertRes!=0)
							{

								// jsonRes = {
								// 	success: '1',
								// 	result: {},
								// 	successMessage: 'New Favorites is successfully saved',
								// 	errorMessage: errorMessage 
						  // 		};

					  	// 		res.status(201).json(jsonRes);
			 				// 	res.end();	

			 					setTimeout(function(){
		 							HelperFavorites.getFavoritesListAllDetails(arrParams, function(retFavAccounts){

										jsonRes = {
											success: '1',
											result: {favorites_list:retFavAccounts},
											successMessage: 'New Favorites is successfully saved',
											errorMessage: errorMessage 
								  		};

							  			res.status(201).json(jsonRes);
										res.end();	

									});
	 							}, 200);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to save favorites. Please try again." 
						  		};

					  			res.status(statRes).json(jsonRes);
			 					res.end();
							}

						});

					});

				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Account not found." 
			  		};

		  			res.status(statRes).json(jsonRes);
 					res.end();

				}

			});

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

});


// router.post('/favorites/remove_to_favorites',[

// 	check('api_token').isAlphanumeric(),
// 	check('account_id').isNumeric(),
// 	check('fav_id').isNumeric()

// 	], (req,res,next) => {

// 	var _send = res.send;
// 	var sent = false;

// 	res.send = function(data){
// 	   if(sent) return;
// 	   _send.bind(res)(data);
// 	   sent = true;
// 	};

//   	var errors = validationResult(req);
//   	if (!errors.isEmpty()) {
//   		jsonRes = {
// 			success: success,
// 			result: {},
// 			successMessage: successMessage,
// 			errorMessage: "No mandatory fileds entered." 
//   		};

// 		res.status(statRes).json(jsonRes);
//  		res.end();
//   	}

// 	var  account_id = req.body.account_id;
// 	var  api_token = req.body.api_token;
// 	var  fav_id = req.body.fav_id;

// 	var arrParams = {
// 		account_id:account_id,
// 		api_token:api_token,
// 		fav_id:fav_id
// 	};


// 	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
// 		if(queParam.length)
// 		{
// 			HelperRestriction.checkAccountCredentials(arrParams, function(queCred){
// 				if(queCred.length)
// 				{	
				
// 					HelperFavorites.checkFavorites(arrParams, function(retCheck){

// 						if(retCheck!=null){

// 							var delQue = `delete from jaze_user_favorite_list where group_id =?`;
// 							HelperFavorites.deleteQuery(delQue,fav_id, function(delRes){

// 								if(delRes!=0){

// 									jsonRes = {
// 										success: '1',
// 										result: {},
// 										successMessage: 'Account favorites is successfully removed.',
// 										errorMessage: errorMessage 
// 							  		};

// 						  			res.status(201).json(jsonRes);
// 				 					res.end();	

// 								}else{

// 									jsonRes = {
// 										success: success,
// 										result: {},
// 										successMessage: successMessage,
// 										errorMessage: "Unable to process request. Please try again." 
// 							  		};

// 						  			res.status(statRes).json(jsonRes);
// 				 					res.end()
// 								}
// 							});

// 						}else{

// 							jsonRes = {
// 								success: success,
// 								result: {},
// 								successMessage: successMessage,
// 								errorMessage: "Favorites account not found. Please try again." 
// 					  		};

// 				  			res.status(statRes).json(jsonRes);
// 		 					res.end()

// 						}

// 					});

// 				}
// 				else
// 				{
// 					jsonRes = {
// 						success: success,
// 						result: {},
// 						successMessage: successMessage,
// 						errorMessage: "Account not found." 
// 			  		};

// 		  			res.status(statRes).json(jsonRes);
//  					res.end();

// 				}

// 			});

// 		}
// 		else
// 		{
// 			jsonRes = {
// 				success: success,
// 				result: {},
// 				successMessage: successMessage,
// 				errorMessage: "Device not found." 
// 	  		};

//   			res.status(statRes).json(jsonRes);
//  			res.end();
// 		}

// 	});

// });


router.post('/favorites/remove_to_favorites',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('fav_account_id').isNumeric(),
	check('fav_account_type').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  account_id = req.body.account_id;
	var  api_token = req.body.api_token;
	var  fav_account_id = req.body.fav_account_id;
	var  fav_account_type = req.body.fav_account_type;

	var arrParams = {
		account_id:account_id,
		api_token:api_token,
		fav_account_id:fav_account_id,
		fav_account_type:fav_account_type
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{

			HelperFavorites.checkFavorites(arrParams, function(retCheck){

				if(retCheck!=null){

					var delQue = `delete from jaze_user_favorite_list where group_id =?`;
					HelperFavorites.deleteQuery(delQue,retCheck, function(delRes){

						if(delRes!=0){

							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'Account favorites is successfully removed.',
								errorMessage: errorMessage 
					  		};

				  			res.status(201).json(jsonRes);
		 					res.end();	

						}else{

							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Unable to process request. Please try again." 
					  		};

				  			res.status(statRes).json(jsonRes);
		 					res.end()
						}
					});

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Favorites account not found. Please try again." 
			  		};

		  			res.status(statRes).json(jsonRes);
 					res.end()

				}

			});

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

});


router.post('/favorites/favorites_list',[

	check('api_token').isAlphanumeric(),
	check('app_id').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  api_token = req.body.api_token;
	var  app_id = req.body.app_id;

	var arrParams = {
		api_token:api_token,
		app_id:app_id
	};


	HelperFavorites.getAppID(arrParams, function(queParam){
		if(queParam!=null)
		{

			HelperFavorites.getFavoritesListAllDetails(queParam, function(retFavAccounts){

				jsonRes = {
					success: '1',
					result: {favorites_list:retFavAccounts},
					successMessage: 'Favorites List',
					errorMessage: errorMessage 
		  		};

	  			res.status(201).json(jsonRes);
				res.end();	

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

});




module.exports = router;