require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();
const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperFollowing = require('../helpers/HelperFollowing.js');
const { check, validationResult } = require('express-validator');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};

//------------------------------------------------------------------- following Module Start ---------------------------------------------------------------------------//

/*
 * following List
 */
router.post('/following/following_list', [
	check('api_token').isLength({ min: 1 }),
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token         : req.body.api_token,
		account_id    	  : req.body.account_id,
		follow_account_id : "0"
	};


	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperFollowing.getFollowingList(reqParams, function(retFollowing){

				if(retFollowing!=null){

					jsonRes = {
						success: '1',
						result: {following_list:retFollowing },
						successMessage: 'following List',
						errorMessage: errorMessage 	
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {
							following_list:[],
						},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

/*
 * Update or unfollow
*/
router.post('/following/update_following_groups', [
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('follow_account_id').isNumeric(),
	check('follow_id').isNumeric(),
	check('flag').isLength({ min: 1 })
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token    		: req.body.api_token,
		account_id   		: req.body.account_id
	};

	// flag :   0 = remove follower, 1 = update group
	
	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			var reqParams = {		
					api_token    		: req.body.api_token,
					follow_account_id   : req.body.account_id,
					account_id   		: req.body.follow_account_id,
					follow_id			: req.body.follow_id,
					group_id     		: req.body.group_id,
					flag     			: req.body.flag
				};

			HelperFollowing.updateFollowersDetails(reqParams, function(updateStatus){

				if(updateStatus !== null)
				{ 
					if(parseInt(req.body.flag) === 0)
					{
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'successfully unfollowed',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else
					{
						var reqParams = { account_id : req.body.account_id, follow_account_id : req.body.follow_account_id };

						HelperFollowing.getFollowingList(reqParams, function(follow_list){

							if(follow_list !== null)
							{
								jsonRes = {
									success: '1',
									result: {following_list:follow_list },
									successMessage: 'following List',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "following List empty" 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
						});
					}
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to precess request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
})

//------------------------------------------------------------------- following Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Followers Module Start ---------------------------------------------------------------------------//

/*
 * Followers List
 */
router.post('/followers/followers_list', [
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token    		: req.body.api_token,
		account_id   		: req.body.account_id,
		follow_account_id   : "0"
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperFollowing.getFollowersList(reqParams, function(follow_list){

				if(follow_list !== null)
				{
					jsonRes = {
						success: '1',
						result: {'followers_list' : follow_list},
						successMessage: 'Follow List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Followers List empty" 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
})

/*
 * Followers List
 */
router.post('/followers/followers_group_list', [
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('follow_account_id').isNumeric()
	], (req,res,next) => {
 
	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
	  }
	  
	var reqParams = {		
		api_token    		: req.body.api_token,
		account_id   		: req.body.account_id,
		follow_account_id   : req.body.follow_account_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperFollowing.getFollowersGroupList(reqParams, function(follow_list){

				if(follow_list !== null)
				{
					jsonRes = {
						success: '1',
						result: {followers_group_list : follow_list},
						successMessage: 'Followers Group List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Followers Group List empty" 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
})


/*
 * Update or Delete Followers Group List
 */
router.post('/followers/update_followers_details', [
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('follow_account_id').isNumeric(),
	check('follow_id').isNumeric(),
	check('flag').isLength({ min: 1 })
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token    		: req.body.api_token,
		account_id   		: req.body.account_id,
		follow_account_id   : req.body.follow_account_id,
		follow_id			: req.body.follow_id,
		group_id     		: req.body.group_id,
		flag     			: req.body.flag
	};

	// flag :   0 = remove follower, 1 = update group,  2 = block follower, 3 = unblock follower

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperFollowing.updateFollowersDetails(reqParams, function(updateStatus){

				if(updateStatus !== null)
				{
					if(parseInt(reqParams.flag) === 0)
					{
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'successfully removed follower',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else
					{
						HelperFollowing.getFollowersList(reqParams, function(follow_list){

							if(follow_list !== null)
							{
								jsonRes = {
									success: '1',
									result: {'followers_group_details' : follow_list},
									successMessage: 'Follow List',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Followers List empty" 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
						});
					}
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to precess request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
})

//------------------------------------------------------------------- Followers Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Company/Profile Follow Module Start ---------------------------------------------------------------------------//

router.post('/followers/follow_list', [
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('profile_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token    		: req.body.api_token,
		account_id   		: req.body.profile_id,
		follow_account_id   : req.body.account_id
	};

	var reqAPIParams = { api_token : req.body.api_token, account_id : req.body.account_id,	}

	HelperRestriction.checkParametersApiToken(reqAPIParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperFollowing.getFollowersGroupList(reqParams, function(follow_list){

				if(follow_list !== null)
				{
					jsonRes = {
						success: '1',
						result: {follow_list : follow_list.business_group},
						successMessage: 'Followers Group List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Followers Group List empty" 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/followers/update_followers', [
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('profile_id').isNumeric(),
	check('flag').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token    		: req.body.api_token,
		account_id   		: req.body.profile_id,
		follow_account_id   : req.body.account_id,
		follow_id		  : 0,
		group_id 	   	  : req.body.group_id,
		flag     		  : req.body.flag
	};	
		// flag :   0 = unfollow, 1 = update group

	var reqAPIParams = { api_token : req.body.api_token, account_id : req.body.account_id	}

	HelperRestriction.checkParametersApiToken(reqAPIParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperFollowing.getFollowId(reqParams, function(follow_id){
				
				if(parseInt(follow_id) !== 0)
				{
					Object.assign(reqParams, {follow_id: follow_id.toString()});

					HelperFollowing.updateFollowersDetails(reqParams, function(updateStatus){

						if(updateStatus !== null)
						{
							if(parseInt(reqParams.flag) === 0)
							{
								jsonRes = {
									success: '1',
									result: {},
									successMessage: 'successfully unfollowed',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();
							}
							else
							{
								HelperFollowing.getFollowersGroupList(reqParams, function(follow_list){

									if(follow_list !== null)
									{
										jsonRes = {
											success: '1',
											result: {follow_list : follow_list.business_group},
											successMessage: 'Follow List',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();
									}
									else
									{
										jsonRes = {
											success: success,
											result: {},
											successMessage: successMessage,
											errorMessage: "Followers List empty" 
										};

										res.status(statRes).json(jsonRes);
										res.end();
									}
								});
							}
						}
						else
						{
							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Unable to precess request. Please try again." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}
					});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to precess request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Company/Profile Follow Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Followers Group Module Start ---------------------------------------------------------------------------//

/*
 * Followers Group List
 */
router.post('/followers/group_list', [
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token    : req.body.api_token,
		account_id   : req.body.account_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperFollowing.getGroupList(reqParams, function(follow_list){

				if(follow_list !== null)
				{
					jsonRes = {
						success: '1',
						result: {group_list : follow_list},
						successMessage: 'Follow Group List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Followers Group List empty" 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});


/*
 * Followers Group Members List
 */
router.post('/followers/group_members_list', [
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('group_id').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token    		: req.body.api_token,
		account_id   		: req.body.account_id,
		group_id   			: req.body.group_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperFollowing.getGroupDetails(reqParams, function(group_details){

				HelperFollowing.getFollowersGroupMembersList(reqParams, function(follow_list){

					if(group_details !== null)
					{
						jsonRes = {
							success: '1',
							result: {group_details : group_details, group_members_list : follow_list},
							successMessage: 'Follow  Group Members List',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Followers List empty" 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
				});
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
})

/*
 * Followers Group 
 * Create and update group and  add members
 */
router.post('/followers/update_group', [
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('title').isLength({ min: 1 }),
	check('group_type').isNumeric(),
	check('follow_ids').isLength({ min: 1 }),
	check('group_id').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		title        : req.body.title,
		group_type   : req.body.group_type,
		follow_ids    : req.body.follow_ids,
		group_id     : req.body.group_id,
	};

	//group_type :  1 = business, 2 = special

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperFollowing.createFollowersGroup(reqParams, function(group_id){

				Object.assign(reqParams, {group_id: group_id});

				HelperFollowing.setFollowersGroupMembers(1,reqParams, function(follow_list){

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'successfully updated',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
				});
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

/*
 * Delete Group 
 * delete group and  move members to another group add/ remove members
 */
router.post('/followers/delete_group', [
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('group_id').isNumeric(),
	check('new_group_id').isNumeric(),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		group_id     : req.body.group_id,
		new_group_id : req.body.new_group_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperFollowing.deleteFollowersGroup(reqParams, function(group_id){

				if(group_id !== null)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'successfully removed',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to precess request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});


/*
 * Followers Group 
 * add/ remove members
 */
router.post('/followers/update_group_members', [
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('follow_ids').isLength({ min: 1 }),
	check('group_id').isLength({ min: 1 }),
	check('flag').isNumeric(),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		follow_ids    : req.body.follow_ids,
		group_id     : req.body.group_id,
		flag     	  : req.body.flag,
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperFollowing.setFollowersGroupMembers(reqParams.flag,reqParams, function(follow_list){

				if(parseInt(follow_list) === 1)
				{
					HelperFollowing.getGroupDetails(reqParams, function(group_details){

						HelperFollowing.getFollowersGroupMembersList(reqParams, function(follow_list){

							jsonRes = {
								success: '1',
								result: {group_details : group_details, group_members_list : follow_list},
								successMessage: 'Follow  Group Members List',
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();
						});
					});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to precess request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

//------------------------------------------------------------------- Followers Group Module End ---------------------------------------------------------------------------//



module.exports = router;