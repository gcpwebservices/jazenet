require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperJazePost = require('../helpers/HelperJazePost.js');

const { check, validationResult } = require('express-validator');


const STATIC_IMG_URL = G_STATIC_IMG_URL+"uploads/jazenet/";


const mime = require('mime-types');
const multer = require('multer');
const fs = require('fs');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};


router.post('/jazepost/post_event', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),

	check('category_id').isNumeric(),
	check('title').isLength({ min: 1 }),
	check('start_date').isLength({ min: 1 }),
	check('end_date').isLength({ min: 1 }),
	check('start_time').isLength({ min: 1 }),
	check('end_time').isLength({ min: 1 }),

	check('contact_name').isLength({ min: 1 }),
	check('contact_no').isLength({ min: 1 }),
	check('contact_email').isLength({ min: 1 }),

	check('country_id').isLength({ min: 1 }),
	check('state_id').isLength({ min: 1 }),
	check('city_id').isLength({ min: 1 }),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,

		category_id : req.body.category_id,
		title       : req.body.title,
		start_date  : req.body.start_date,
		end_date    : req.body.end_date,
		start_time  : req.body.start_time,
		end_time    : req.body.end_time,
		
		contact_name   : req.body.contact_name,
		contact_no     : req.body.contact_no,
		contact_email  : req.body.contact_email,

		venue       : req.body.venue,
		event_link  : req.body.event_link,
		
		country_id  : req.body.country_id,
		state_id    : req.body.state_id,
		city_id     : req.body.city_id,
		area_id     : req.body.area_id,
		
		street_name    : req.body.street_name,
		street_no      : req.body.street_no,
		building_name  : req.body.building_name,
		building_no    : req.body.building_no,

		event_desc     : req.body.event_desc,

		file_name    : req.body.file_name,
		file_ext     : req.body.file_ext,
		youtube_url  : req.body.youtube_url,

		latitude     : req.body.latitude,
		longitude    : req.body.longitude
	};


	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazePost.postNewEvent(reqParams, function(retStatus){

				if(parseInt(retStatus) === 1){

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'New event is successfully saved.',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '0',
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});



var storage = multer.diskStorage({
	destination: function (req, file, callback) {
	
	function asyncFunction (item, cb) {
	  	setTimeout(() => {

			
			var out_path = G_STATIC_IMG_PATH+'event_images/';

			var main_path = out_path;

			if (!fs.existsSync(out_path)){
				fs.mkdirSync(out_path,'0757', true); 
			}


			callback(null, main_path);

			cb();
	  	}, 100);
	}

	let requests = 0;

		if(req.body.account_id !=''){

			return new Promise((resolve) => {
				requests = req.body.account_id;
		  		asyncFunction(requests, resolve);
		    });

			Promise.all(requests);
		}
  },
  	filename: function (req, file, callback) {

  		var ext = getExtension(file.originalname);
  		var cleanName = getCleanFileName(file.originalname);
  		var completeFileName = req.body.account_id+'events_'+Date.now()+'.'+ext;

    	callback(null, completeFileName);
  	}
});


router.post('/jazepost/file_uploads', function(req, res) {

	var upload = multer({
		storage: storage,
	}).single('file');

	upload(req, res, function(err) {

 	 	var extName = req.file.filename.split(".");
 	 	var filename = extName[extName.length - 2];

	 	var extArray = req.file.filename.split(".");
 	 	var extention = extArray[extArray.length - 1];

 	  	var file_properties = req.file.mimetype.split("/");

 		var base_url = '';
        var filetype = '';

 		var complete_filename = filename+'.'+extention;
 		base_url = STATIC_IMG_URL+'/event_images/'+complete_filename;

 		filetype = file_properties[0]; 

 		var arrParams = {account_id:req.body.account_id, file_name:req.file.filename};
 		
    	HelperProfessional.getProfilePicture(arrParams,function(retStatus){

	        HelperDocuments.getFileTypes('.'+extention, function(reType){

	            var results = {
	                file_name:filename,
	                file_format:filetype,
	                file_type:reType.toString(),
	                extention:extention,
	                file_path:base_url,
	            };
	           
	            jsonRes = {
	                success: '1',
	                result: results,
	                successMessage: 'File is successfully uploaded.',
	                errorMessage: errorMessage 
	            };
	            res.status(201).json(jsonRes);
	            res.end();
	        });

        });

	});
});

function getExtension(path) {
    var basename = path.split(/[\\/]/).pop(),  // extract file name from full path ...
                                             	 // (supports `\\` and `/` separators)
        pos = basename.lastIndexOf(".");       // get last position of `.`
    if (basename === "" || pos < 1)            // if file name is empty or ...
        return "";                             //  `.` not found (-1) or comes first (0)
    return basename.slice(pos + 1);            // extract extension ignoring `.`
}

function getFilename(path) {
    var basename = path.split(/[\\/]/).pop(),      
        pos = basename.split('.'); 
    if (basename === "" || pos.length < 1)           
        return "";                          
    return pos[0];            
}


function getCleanFileName(path) {

	var basename = path.split(/\.(?=[^\.]+$)/);
	var cleanString = basename[0].replace(/[|&;$%@"<>()+1234567890,]/g, "");
         
    return cleanString;           
}




router.post('/jazepost/property_form', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('flag').isNumeric(),



	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		flag        : req.body.flag
	};

	// 1 = residential
	// 2 = commercial

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			if(parseInt(reqParams.flag) == 1 || parseInt(reqParams.flag) == 2)
			{

				HelperJazePost.getCategoryAttributesProperty(reqParams, function(retStatus){

					jsonRes = {
						success: '1',
						result: {property_form:retStatus},
						successMessage: 'New event is successfully saved.',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
			
				});
			}
			else
			{

				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/jazepost/post_property', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),

	check('title').isLength({ min: 1 }),
	check('price').isLength({ min: 1 }),

	check('attributes').isLength({ min: 1 }),

	check('country_id').isNumeric(),
	check('state_id').isNumeric(),
	check('city_id').isNumeric(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		title       : req.body.title,
		price       : req.body.price,
		extras            : req.body.extras,
		sellers_comments  : req.body.sellers_comments,
		country_id  : req.body.country_id,
		state_id    : req.body.state_id,
		city_id     : req.body.city_id,
		area_id     : req.body.area_id,
		images      : req.body.images,
		flag        : req.body.flag,
		attributes  : req.body.attributes
	};

	// 1 = residential
	// 2 = commercial

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazePost.postProperty(reqParams, function(retStatus){

				if(parseInt(retStatus) === 1){

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'New property is successfully posted.',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '0',
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/jazepost/auto_form', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('flag').isNumeric(),



	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		flag        : req.body.flag
	};

	// 1 = Cars
	// 2 = Motorbikes
	// 3 = Trucks

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			if(parseInt(reqParams.flag) == 1 || parseInt(reqParams.flag) == 2 || parseInt(reqParams.flag) == 3)
			{
				
				HelperJazePost.getCategoryAttributesAuto(reqParams, function(retStatus){

					jsonRes = {
						success: '1',
						result: {auto_form:retStatus},
						successMessage: 'Auto form.',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				});
			}
			else
			{

				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});



router.post('/jazepost/post_auto', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),

	check('title').isLength({ min: 1 }),
	check('price').isLength({ min: 1 }),
	check('attributes').isLength({ min: 1 }),

	check('country_id').isNumeric(),
	check('state_id').isNumeric(),
	check('city_id').isNumeric(),


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		title       : req.body.title,
		price       : req.body.price,
		extras            : req.body.extras,
		sellers_comments  : req.body.sellers_comments,
		
		country_id  : req.body.country_id,
		state_id    : req.body.state_id,
		city_id     : req.body.city_id,
		area_id     : req.body.area_id,
		
		images : req.body.images,

		flag   : req.body.flag,
		attributes   : req.body.attributes
	};

	// 1 = cars
	// 2 = motorbikes
	// 3 = trucks

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazePost.postCars(reqParams, function(retStatus){

				if(parseInt(retStatus) === 1){

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'New auto is successfully posted.',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '0',
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});



module.exports = router;