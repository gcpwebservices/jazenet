require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();


const HelperJazepay = require('../helpers/HelperJazepay.js');
const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperEncrypt	= require('../../helpers/HelperEncrypt.js');
const HelperGeneral = require('../../helpers/HelperGeneral.js');
const HelperGeneralJazenet = require('../helpers/HelperGeneralJazenet.js');

const { check, validationResult } = require('express-validator');
const crypto = require('crypto');
const fetch = require('node-fetch');


var query = require('array-query');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};



router.post('/jazepay/home_balance',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getJazepayHomeBalance(arrParams, function(retDetails){

				jsonRes = {
					success: '1',
					result: {
						jazepay_home_balance : retDetails
					},
					successMessage: 'Cash purcahse details',
					errorMessage: "" 
				};

				res.status(201).json(jsonRes);
		 		res.end();
		
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/company_products',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('request_account_id','Invalid request_account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		request_account_id : req.body.request_account_id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getCompanyProducts(arrParams, function(retProd){

				if(retProd!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							company_products : retProd
				
						},
						successMessage: 'Company Product List',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							company_products : {}
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});





//------------------------------------------------------------------- System Partners Payment Module Start ---------------------------------------------------------------------------//

router.post('/system_partners_list',[
	check('api_token','Invalid api token').isAlphanumeric(),
	check('account_id','Invalid account id').isNumeric(),
	check('country_id','Invalid country').isNumeric(),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		country_id   : req.body.country_id,
		state_id     : req.body.state_id,
		city_id      : req.body.city_id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperJazepay.getExternalSystemPartners(arrParams,0, function(comp_result){

				if(comp_result !=null){

					jsonRes = {
						success: '1',
						result: {system_partners_list: comp_result},
						successMessage: 'System Partners List',
						errorMessage: "" 
						};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: '0',
						result: {},
						successMessage: "" ,
						errorMessage: "No result found."  
					};

					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

//------------------------------------------------------------------- System Partners Payment Module End ---------------------------------------------------------------------------//

// ------------------------------------------------------ General Search Module Start  --------------------------------------------------------------//  

router.post('/jazepay/search_accounts',[

	check('api_token','Invalid api token').isAlphanumeric(),
	check('account_id','Invalid account id').isNumeric(),
	check('account_type','Invalid account type').isNumeric(),
    check('country_id','Invalid country').isNumeric(),
    check('state_id','Invalid state').isNumeric(),
	check('city_id','Invalid city').isNumeric(),

	check('flag','Invalid flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		account_type   : req.body.account_type,
		country_id   : req.body.country_id,
		state_id     : req.body.state_id,
		city_id      : req.body.city_id,
		keyword 	: req.body.keyword,
		flag         : req.body.flag
	};

	// flag :   1 = intra jazenet transfer account name,  2 = intra jazenet transfer account jazenet_id, 3 = company search,

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperJazepay.searchAccounts(arrParams, function(search_result){

				if(search_result !=null){

					jsonRes = {
						success: '1',
						result: {search_result: search_result},
						successMessage: 'Company Result',
						errorMessage: "" 
						};
					res.status(201).json(jsonRes);
					res.end();

				}
				else
				{
					jsonRes = {
						success: '0',
						result: {},
						successMessage: "" ,
						errorMessage: "No result found."  
					};

					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

// ------------------------------------------------------ General Search Module Start  --------------------------------------------------------------//  


//------------------------------------------------------------------- Intra transfer Module Start ----------------------------------------------------------------------------------//


router.post('/jazepay/intra_transfer',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('request_account_id','Invalid request_account_id').isNumeric(),
    check('amount','Invalid amount').isNumeric(),
    check('is_favorites','Invalid is_favorites').isNumeric(),
    check('security_pin','Invalid security_pin').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token             : req.body.api_token,
		account_id            : req.body.account_id,
		request_account_id    : req.body.request_account_id,
		amount                : req.body.amount,
		remarks               : req.body.remarks,
		is_favorites          : req.body.is_favorites,
		security_pin		  : crypto.createHash('md5').update(req.body.security_pin).digest("hex")
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.checkSecurityPin(arrParams, function(retSec){

				if(retSec!=null)
				{

					HelperJazepay.procInstaTransfer(arrParams, function(retStatus){

						if(parseInt(retStatus) === 1)
						{

							setTimeout(function(){ 

								HelperJazepay.getJazePayBalance(arrParams.account_id, function(retBalance){

									jsonRes = {
										success: '1',
										result: {
											jazepay_balance:retBalance
										},
										successMessage: 'Transaction has been completed.',
										errorMessage: "" 
									};
									res.status(201).json(jsonRes);
									res.end();	

								});



							}, 500);

				

						}
						else if(parseInt(retStatus) === 2)
						{

							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'Transaction cannot be process. Insufficient funds',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
						else
						{
							jsonRes = {
								success: '0',
								result: {},
								successMessage: '',	
								errorMessage: "Unable to process request. Please try again." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}
				
					});
				}
				else
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Security pin did not match. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/intra_transfer_history',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric()



	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		month       : req.body.month,
		year        : req.body.year,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getIntraTransferHistory(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					jsonRes = {
						success: '1',
						result: {transaction_histrory: retHistory},
						successMessage: 'Intra Jazenet Transfer History',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
	
		
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


//------------------------------------------------------------------- Intra transfer Module End ------------------------------------------------------------------------------------//



router.post('/jazepay/intra_favorites_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getIntraFavoritesList(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					jsonRes = {
						success: '1',
						result: {intra_favorites_list: retHistory},
						successMessage: 'Intra Favorites List',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
	
		
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/intra_remove_favorites',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('favorite_id','Invalid favorite_id').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		favorite_id : req.body.favorite_id

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.removeIntraFavorites(arrParams, function(retStatus){

				if(parseInt(retStatus) == 1)
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Account is successfully removed.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}
				else if(parseInt(retStatus) == 2)
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Unable to process request. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
	
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



// router.post('/jazepay/add_funds',[

// 	check('api_token','Invalid api_token').isLength({ min: 1 }),
// 	check('account_id','Invalid account_id').isNumeric(),
// 	check('payment_id','Invalid payment_id').isNumeric(),
// 	check('payment_type','Invalid payment_type').isNumeric(),
// 	check('amount','Invalid amount').isNumeric()

// 	], (req,res,next) => {

// 	var _send = res.send;
// 	var sent = false;

// 	res.send = function(data){
// 	   if(sent) return;
// 	   _send.bind(res)(data);
// 	   sent = true;
// 	};

//   	var errors = validationResult(req);
//   	if (!errors.isEmpty()) {
//   		jsonRes = {
// 			success: success,
// 			result: {}, 
// 			successMessage: successMessage,
// 			errorMessage: errors.array()[0].msg
//   		};

// 		res.status(statRes).json(jsonRes);
//  		res.end();
//   	}


// 	var arrParams = {

// 		api_token    : req.body.api_token,
// 		account_id   : req.body.account_id,
// 		payment_id   : req.body.payment_id,
// 		payment_type : req.body.payment_type,
// 		amount       : req.body.amount

// 	};

// 	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

// 		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
// 		{

// 			HelperJazepay.jazePayAddFunds(arrParams, function(retStatus){

// 				if(parseInt(retStatus) == 1)
// 				{

// 					jsonRes = {
// 						success: '1',
// 						result: {},
// 						successMessage: 'Transaction is successfully processed.',
// 						errorMessage: "" 
// 					};
// 					res.status(201).json(jsonRes);
// 					res.end();

// 				}
// 				else if(parseInt(retStatus) == 2)
// 				{

// 					jsonRes = {
// 						success: '1',
// 						result: {},
// 						successMessage: 'Account Payment not found.',
// 						errorMessage: "" 
// 					};
// 					res.status(201).json(jsonRes);
// 					res.end();
// 				}
// 				else
// 				{
// 					jsonRes = {
// 						success: '1',
// 						result: {},
// 						successMessage: 'Unable to process request. Please try again.',
// 						errorMessage: "" 
// 					};
// 					res.status(201).json(jsonRes);
// 					res.end();
// 				}
	
// 			});

// 		}
// 		else
// 		{
// 			jsonRes = {
// 				success: '0',
// 				result: {},
// 				successMessage: successMessage,
// 				errorMessage: "Device not found." 
// 			};

// 			res.status(statRes).json(jsonRes);
// 			res.end();
// 		}

// 	});

// });




router.post('/jazepay/add_funds',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('payment_id','Invalid payment_id').isNumeric(),
	check('payment_type','Invalid payment_type').isNumeric(),
	check('amount','Invalid amount').isNumeric(),
    check('security_pin','Invalid security_pin').isNumeric(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		payment_id   : req.body.payment_id,
		payment_type : req.body.payment_type,
		amount       : req.body.amount,
		security_pin		  : crypto.createHash('md5').update(req.body.security_pin).digest("hex")

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperJazepay.checkSecurityPin(arrParams, function(retSec){

				if(retSec!=null)
				{

					HelperJazepay.jazePayAddFunds(arrParams, function(retStatus,card_details){

						if(parseInt(retStatus) == 1)
						{

							jsonRes = {
								success: '1',
								result: {
									payment_info : card_details
								},
								successMessage: 'Transaction is successfully processed.',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();

						}
						else if(parseInt(retStatus) == 2)
						{

							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'Account Payment not found.',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
						else
						{
							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'Unable to process request. Please try again.',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
			
					});
									}
				else
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Security pin did not match. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}
	
			});
		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/system_partners_payments',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('request_account_id','Invalid request_account_id').isNumeric(),
	check('account_name','Invalid request_account_id').isLength({ min: 1 }),
	check('account_number','Invalid request_account_id').isNumeric(),
    check('amount','Invalid amount').isNumeric(),
    check('is_favorites','Invalid is_favorites').isNumeric(),
    check('security_pin','Invalid security_pin').isNumeric(),



	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token             : req.body.api_token,
		account_id            : req.body.account_id,
		request_account_id    : req.body.request_account_id,
		account_name          : req.body.account_name,
		account_number        : req.body.account_number,
		amount                : req.body.amount,
		remarks               : req.body.remarks,
		is_favorites          : req.body.is_favorites,
		security_pin		  : crypto.createHash('md5').update(req.body.security_pin).digest("hex")
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.checkSecurityPin(arrParams, function(retSec){

				if(retSec!=null)
				{

					HelperJazepay.procSystemPartnersPayment(arrParams, function(retStatus){

						if(parseInt(retStatus) === 1)
						{

							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'Transaction has been completed.',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();

						}
						else if(parseInt(retStatus) === 2)
						{

							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'Transaction cannot be process. Insufficient funds',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
						else
						{
							jsonRes = {
								success: '0',
								result: {},
								successMessage: '',	
								errorMessage: "Unable to process request. Please try again." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}
				
					});

				}
				else
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Security pin did not match. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}
	
			});
		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/national_transfers',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('bank_name','Invalid bank_name').isLength({ min: 1 }),
	check('branch_name','Invalid branch_name').isLength({ min: 1 }),
	check('account_name','Invalid account_name').isLength({ min: 1 }),
	check('account_number','Invalid account_number').isNumeric(),
	check('iban_number','Invalid iban_number').isNumeric(),
    check('amount','Invalid amount').isNumeric(),
    check('is_favorites','Invalid is_favorites').isNumeric(),
    check('security_pin','Invalid security_pin').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token             : req.body.api_token,
		account_id            : req.body.account_id,
		bank_name             : req.body.bank_name,
		branch_name           : req.body.branch_name,
		account_name          : req.body.account_name,
		account_number        : req.body.account_number,
		account_name          : req.body.account_name,
		iban_number           : req.body.iban_number,
		amount                : req.body.amount,
		remarks               : req.body.remarks,
		is_favorites          : req.body.is_favorites,
		security_pin		  : crypto.createHash('md5').update(req.body.security_pin).digest("hex")
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.checkSecurityPin(arrParams, function(retSec){

				if(retSec!=null)
				{

					HelperJazepay.procNationalTransfers(arrParams, function(retStatus){

						if(parseInt(retStatus) === 1)
						{

							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'Transaction has been completed.',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();

						}
						else if(parseInt(retStatus) === 2)
						{

							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'Transaction cannot be process. Insufficient funds',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
						else
						{
							jsonRes = {
								success: '0',
								result: {},
								successMessage: '',	
								errorMessage: "Unable to process request. Please try again." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}
				
					});

				}
				else
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Security pin did not match. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}
	
			});
		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/international_transfer',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('bank_name','Invalid bank_name').isLength({ min: 1 }),
	check('branch_name','Invalid branch_name').isLength({ min: 1 }),
	check('account_name','Invalid account_name').isLength({ min: 1 }),
	check('account_number','Invalid account_number').isNumeric(),
	check('iban_number','Invalid iban_number').isNumeric(),
	check('swift_code','Invalid swift_code').isNumeric(),
    check('amount','Invalid amount').isNumeric(),
    check('is_favorites','Invalid is_favorites').isNumeric(),
    check('security_pin','Invalid security_pin').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token             : req.body.api_token,
		account_id            : req.body.account_id,
		bank_name             : req.body.bank_name,
		branch_name           : req.body.branch_name,
		bank_address          : req.body.bank_address,
		account_name          : req.body.account_name,
		account_number        : req.body.account_number,
		account_name          : req.body.account_name,
		iban_number           : req.body.iban_number,
		swift_code            : req.body.swift_code,
		amount                : req.body.amount,
		remarks               : req.body.remarks,
		is_favorites          : req.body.is_favorites,
		security_pin		  : crypto.createHash('md5').update(req.body.security_pin).digest("hex")
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.checkSecurityPin(arrParams, function(retSec){

				if(retSec!=null)
				{

					HelperJazepay.procInternationalTransfer(arrParams, function(retStatus){

						if(parseInt(retStatus) === 1)
						{

							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'Transaction has been completed.',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();

						}
						else if(parseInt(retStatus) === 2)
						{

							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'Transaction cannot be process. Insufficient funds',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
						else
						{
							jsonRes = {
								success: '0',
								result: {},
								successMessage: '',	
								errorMessage: "Unable to process request. Please try again." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}
				
					});
				}
				else
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Security pin did not match. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}
	
			});
		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/transfers_favorites_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('flag','Invalid flag').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		flag       : req.body.flag

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			var arrFalgs = [1,2,3];
			if(arrFalgs.includes(parseInt(arrParams.flag)))
			{

				HelperJazepay.getTransfersFavorites(arrParams, function(retFavorites){

					if(retFavorites!=null)
					{

						var obj_name;
						if(parseInt(arrParams.flag) === 1){
							obj_name = 'system_partners_favorites_list';
						}else if(parseInt(arrParams.flag) === 2){
							obj_name = 'national_transfers_favorites_list';
						}else if(parseInt(arrParams.flag) === 3){
							obj_name = 'international_transfers_favorites_list';
						}

						jsonRes = {
							success: '1',
							result: {
								[obj_name] : retFavorites
							},
							successMessage: 'Jazepay Transfers Favorites List.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'No result found',	
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
			
				});	
			}
			else
			{

				jsonRes = {
					success: '0',
					result: {},
					successMessage: '',	
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();

			}

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/transfers_history_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('flag','Invalid flag').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		flag       : req.body.flag

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			var arrFalgs = [1,2,3];
			if(arrFalgs.includes(parseInt(arrParams.flag)))
			{

				HelperJazepay.getTransferHistoryStatements(arrParams, function(retFavorites){

					if(retFavorites!=null)
					{

						var obj_name;
						if(parseInt(arrParams.flag) === 1){
							obj_name = 'system_partners_history_list';
						}else if(parseInt(arrParams.flag) === 2){
							obj_name = 'national_transfers_history_list';
						}else if(parseInt(arrParams.flag) === 3){
							obj_name = 'international_transfers_history_list';
						}

						jsonRes = {
							success: '1',
							result: {
								[obj_name] : retFavorites
							},
							successMessage: 'Jazepay Transfers History List.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'No result found',	
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
			
				});	
			}
			else
			{

				jsonRes = {
					success: '0',
					result: {},
					successMessage: '',	
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();

			}

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/remove_transfer_favortites',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('favorite_id','Invalid favorite_id').isLength({ min: 1 }),
	check('flag','Invalid flag').isNumeric(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		favorite_id : req.body.favorite_id,
		flag        : req.body.flag

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{


			var arrFalgs = [1,2,3];
			if(arrFalgs.includes(parseInt(arrParams.flag)))
			{


				HelperJazepay.removeTransfersFavorties(arrParams, function(retStatus){

					if(parseInt(retStatus) == 1)
					{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Account is successfully removed.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else if(parseInt(retStatus) == 2)
					{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Unable to process request. Please try again.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
		
				});
			}
			else
			{

				jsonRes = {
					success: '0',
					result: {},
					successMessage: '',	
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/payment_history',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		month       : req.body.month,
		year        : req.body.year,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getPaymentHistory(arrParams, function(retHistory,total_amount){

				if(retHistory!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							payment_history : retHistory,
							total_amount    : total_amount.toFixed(2)
						},
						successMessage: 'Jazepay Payment History',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payment_history : [],
							total_amount    : '0.00'
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/search_payment_history',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate(),


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		from_date    : req.body.from_date,
		to_date      : req.body.to_date,
		ref_no       : req.body.ref_no,
		name         : req.body.name,
		payment_type : req.body.payment_type,

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.searchPaymentHistory(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					var output =[];
					//2 not empty 1 empty
					if(arrParams.ref_no!="" && arrParams.name!="" && arrParams.payment_type!="")
					{
						output = query("ref_no").search(arrParams.ref_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);
					}
					else if(arrParams.ref_no=="" && arrParams.name!="" && arrParams.payment_type!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
								.and(query("payment_type").is(arrParams.payment_type))
									.on(retHistory);
					}
					else if(arrParams.ref_no!="" && arrParams.name=="" && arrParams.payment_type!="")
					{
						output = query("ref_no").search(arrParams.ref_no)
								.and(query("payment_type").is(arrParams.payment_type))
									.on(retHistory);
					}
					else if(arrParams.ref_no!="" && arrParams.name!="" && arrParams.payment_type=="")
					{
						output = query("ref_no").search(arrParams.ref_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}

					//2 empty 1 not empty
					else if(arrParams.ref_no!="" && arrParams.name=="" && arrParams.payment_type=="")
					{
						output = query("ref_no").search(arrParams.ref_no).on(retHistory);
					}
					else if(arrParams.ref_no=="" && arrParams.name!="" && arrParams.payment_type=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase()).on(retHistory);
					}
					else if(arrParams.ref_no=="" && arrParams.name=="" && arrParams.payment_type!="")
					{
						output = query("payment_type").is(arrParams.payment_type).on(retHistory);
					}
					else
					{
						output = retHistory;
					}
					

					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								search_payment_history : output
							},
							successMessage: 'Search Jazepay Payment History',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								search_payment_history : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payment_history : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/statement_history',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		name        : req.body.name,
		transaction : req.body.transaction,
		trans_type  : req.body.trans_type,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getStatementHistroy(arrParams, function(retHistory,total_amount){

				if(retHistory!=null)
				{

					var output =[];
					//2 not empty 1 empty
					if(arrParams.transaction!="" && arrParams.name!="" && arrParams.trans_type!="")
					{
						output = query("ref_no").search(arrParams.transaction)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("trans_type").search(arrParams.trans_type))
										.on(retHistory);
					}
					else if(arrParams.transaction=="" && arrParams.name!="" && arrParams.trans_type!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
								.and(query("trans_type").search(arrParams.trans_type))
									.on(retHistory);
					}
					else if(arrParams.transaction!="" && arrParams.name=="" && arrParams.trans_type!="")
					{
						output = query("ref_no").search(arrParams.transaction)
								.and(query("trans_type").search(arrParams.trans_type))
									.on(retHistory);
					}
					else if(arrParams.transaction!="" && arrParams.name!="" && arrParams.trans_type=="")
					{
						output = query("ref_no").search(arrParams.transaction)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}

					//2 empty 1 not empty
					else if(arrParams.transaction!="" && arrParams.name=="" && arrParams.trans_type=="")
					{
						output = query("ref_no").search(arrParams.transaction).on(retHistory);
					}
					else if(arrParams.transaction=="" && arrParams.name!="" && arrParams.trans_type=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase()).on(retHistory);
					}
					else if(arrParams.transaction=="" && arrParams.name=="" && arrParams.trans_type!="")
					{
						output = query("trans_type").search(arrParams.trans_type).on(retHistory);
					}
					else if(arrParams.transaction=="" && arrParams.name=="" && arrParams.trans_type=="")
					{
						output = retHistory;
					}
	
			
					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								statement_history : output
							},
							successMessage: 'Search Jazepay statement history',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								statement_history : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							statement_history : [],
				
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});





router.post('/jazepay/payables_creditors_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		month       : req.body.month,
		year        : req.body.year,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getPayablesCreditorsList(arrParams, function(retHistory,total_amount){

				if(retHistory!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							payables_creditors_list : retHistory,
							total_amount            : total_amount.toFixed(2)
						},
						successMessage: 'Jazepay Payable Creditors Lists',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payables_creditors_list : [],
							total_amount            : '0.00'
				
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/payables_creditor_transactions',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('creditor_account_id','Invalid creditor_account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		creditor_account_id  : req.body.creditor_account_id,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date,
		ref_no      : req.body.ref_no,
		trans_type  : req.body.trans_type

	};

	// ref_type 1 = invoice
	// ref_type 2 = payment
	// ref_type 3 = Cash Purcahse

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getPayablesCreditorTransactions(arrParams, function(retHistory){

				console.log(retHistory);

				if(retHistory!=null)
				{

					var output = [];
					if(arrParams.ref_no!="" && arrParams.trans_type!="")
					{
						output = query("ref_no").search(arrParams.ref_no)
									.and(query("trans_type").search(arrParams.trans_type))
										.on(retHistory.transactions);
						retHistory.transactions = output;
					}
					else if(arrParams.ref_no=="" && arrParams.trans_type!="")
					{
						output = query("trans_type").search(arrParams.trans_type).on(retHistory.transactions);
						retHistory.transactions = output;
					}

					else if(arrParams.ref_no!="" && arrParams.trans_type=="")
					{
						output = query("ref_no").search(arrParams.ref_no).on(retHistory.transactions);
						retHistory.transactions = output;
					}					
					else
					{
						output = retHistory.transactions;
					}

			

					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								payables_creditor_transactions : retHistory
							},
							successMessage: 'Jazepay Payable Creditor Transactions',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								payables_creditor_transactions : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payables_creditor_transactions : [],
				
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/add_funds_history',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		month       : req.body.month,
		year        : req.body.year,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date

	};

	// ref_type 1 = invoice
	// ref_type 2 = payment

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getAddFundsHistory(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							add_funds_history : retHistory,
						},
						successMessage: 'Jazepay Payable Creditors Lists',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							add_funds_history : [],
				
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/payables_creditors_maintenance',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		month       : req.body.month,
		year        : req.body.year,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date

	};

	// ref_type 1 = invoice
	// ref_type 2 = payment

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getPayablesCreditorsMaintenance(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							payables_creditors_maintenance : retHistory,
						},
						successMessage: 'Jazepay Payable Creditors Lists',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payables_creditors_maintenance : [],
				
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/payables_creditors_maintenance_registration',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('creditor_account_id','Invalid creditor_account_id').isNumeric(),
	check('registration_no','Invalid registration_no').isLength({ min: 1 }),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		creditor_account_id : req.body.creditor_account_id,
		registration_no     : req.body.registration_no,
		credit_limit        : req.body.credit_limit,
		discount            : req.body.discount,
		vat_no              : req.body.vat_no,
		payment_terms       : req.body.payment_terms,
		contact_person      : req.body.contact_person

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.procCreditorRegistration(arrParams, function(retStatus){

				if(parseInt(retStatus)===1)
				{	

					setTimeout(function(){ 
	
						var addParam = {profile_id: arrParams.creditor_account_id, account_id: arrParams.account_id};
						HelperJazepay.getCreditorRegistration(addParam, function(retAdd1){	

							jsonRes = {
								success: '1',
								result: {
									payables_creditors_maintenance_registration : retAdd1,
								},
								successMessage: 'Creditor registration is complete.',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();

						});

					}, 500);
				}
				else if(parseInt(retStatus)===0)
				{

					jsonRes = {
						success: '1',
						result: {
							payables_creditors_maintenance_registration : {},
				
						},
						successMessage: 'Unable to process request. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/payables_creditors_maintenance_address_update',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('creditor_account_id','Invalid creditor_account_id').isNumeric(),
	check('flag','Invalid flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		creditor_account_id : req.body.creditor_account_id,
		country_id       : req.body.country_id,
		state_id         : req.body.state_id,
		city_id          : req.body.city_id,
		area_id          : req.body.area_id,
		street_name      : req.body.street_name,
		street_number    : req.body.street_number,
		building_name    : req.body.building_name,
		building_number  : req.body.building_number,
		flag             : req.body.flag,

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			var arrFlags = [1,2,3];

				if(arrFlags.includes(parseInt(arrParams.flag)))
				{
					HelperJazepay.procCreditorAddress(arrParams, function(retStatus){

						if(parseInt(retStatus)===1)
						{

							setTimeout(function(){ 

								var addParam = {profile_id: arrParams.creditor_account_id, account_id: arrParams.account_id, type: arrParams.flag};
								HelperJazepay.getCreditorAddressDetails(addParam, function(retResult){	

									var obj = '';
									if(parseInt(arrParams.flag) === 1){
										obj = 'physical_address';
									}else if(parseInt(arrParams.flag) === 2){
										obj = 'delivery_address';
									}else if(parseInt(arrParams.flag) === 3){
										obj = 'postal_address';
									}

									jsonRes = {
										success: '1',
										result: {
											payables_creditors_maintenance:{
												[obj] : retResult,
											}
											
										},
										successMessage: 'Creditor address is successfully saved.',
										errorMessage: "" 
									};
									res.status(201).json(jsonRes);
									res.end();
							
								});

							}, 500);

						}
						else if(parseInt(retStatus)===0)
						{

							jsonRes = {
								success: '1',
								result: {
									payables_creditors_maintenance : {},
						
								},
								successMessage: 'Unable to process request. Please try again.',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
					});
				}
				else
				{
					jsonRes = {
						success: '0',
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid flag provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}


		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/payables_quotation_request',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('request_account_id','Invalid request_account_id').isNumeric(),
	check('quotation_request_no','Invalid quotation_request_no').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		quotation_request_no      : req.body.quotation_request_no,
		request_account_id   : req.body.request_account_id,
		reference            : req.body.reference,
		product_values       : req.body.product_values,
		flag                 : req.body.flag,

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			if(parseInt(arrParams.flag) === 1 || parseInt(arrParams.flag) === 2)
			{

				HelperJazepay.reqestQuotation(arrParams, function(retResult){	


					if(parseInt(retResult) === 1)
					{
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Request quotation is successfully send.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Unable to process request. Please try again.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

					
			
				});

			}
			else
			{
				jsonRes = {
					success: '0',
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}


		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/get_transaction_no',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('flag','Invalid flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		flag        : req.body.flag,

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getTransactionNumber(arrParams, function(retResult){	

				var obj = '';;
				var in_obj_key_1 = '';
				var in_obj_key_2 = '';

				var in_obj_val_1 = '';
				var in_obj_val_2 = '';

				var ret_mes = '';

				if(parseInt(arrParams.flag) === 1){

					obj = 'quotation_no';

					in_obj_val_1 = retResult.quo_req_id;
					in_obj_val_2 = retResult.quo_req_no;

					in_obj_key_1 = 'quo_req_id';
					in_obj_key_2 = 'quo_req_no';

					ret_mes = 'Quotation Request Number';

				}else if(parseInt(arrParams.flag) === 2){

					obj = 'quotation_no';

					in_obj_val_1 = retResult.quo_req_id;
					in_obj_val_2 = retResult.quo_req_no;

					in_obj_key_1 = 'quo_req_id';
					in_obj_key_2 = 'quo_req_no';

					ret_mes = 'Quotation Number';

				}else if(parseInt(arrParams.flag) === 3){

					obj = 'payment_no';

					in_obj_val_1 = retResult.quo_req_id;
					in_obj_val_2 = retResult.quo_req_no;

					in_obj_key_1 = 'payment_no_req_id';
					in_obj_key_2 = 'payment_no';

					ret_mes = 'Payment Number';

				}else if(parseInt(arrParams.flag) === 4){

					obj = 'order_no';

					in_obj_val_1 = retResult.quo_req_id;
					in_obj_val_2 = retResult.quo_req_no;

					in_obj_key_1 = 'order_no_req_id';
					in_obj_key_2 = 'order_no';

					ret_mes = 'Order Number';
				}

				jsonRes = {
					success: '1',
					result: {
						[obj]:{
							[in_obj_key_1] : in_obj_val_1,
							[in_obj_key_2] : in_obj_val_2
						}
						
					},
					successMessage: ret_mes,
					errorMessage: "" 
				};
				res.status(201).json(jsonRes);
				res.end();
		
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/payables_quotation_request_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date,
		name        : req.body.name,
		quotation_request_no : req.body.quotation_request_no,
		quotation_no         : req.body.quotation_no,
		quotation_status     : req.body.quotation_status,

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getQuotationRequest(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					var output =[];
					//3 empty 1 not empty
					if(arrParams.quotation_request_no!="" && arrParams.name!="" && arrParams.quotation_no!="" && arrParams.quotation_status!="")
					{
						output = query("quotation_request_no").search(arrParams.quotation_request_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("quotation_no").search(arrParams.quotation_no))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no !="" && arrParams.name=="" && arrParams.quotation_no=="" && arrParams.quotation_status=="")
					{
						output = query("quotation_request_no").search(arrParams.quotation_request_no)							
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name!="" && arrParams.quotation_no=="" && arrParams.quotation_status=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())							
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name=="" && arrParams.quotation_no!="" && arrParams.quotation_status=="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name=="" && arrParams.quotation_no=="" && arrParams.quotation_status!="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
										.on(retHistory);
					}

					//2 empty 2 not empty
					if(arrParams.quotation_request_no!="" && arrParams.name!="" && arrParams.quotation_no=="" && arrParams.quotation_status=="")
					{
						output = query("quotation_request_no").search(arrParams.quotation_request_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name!="" && arrParams.quotation_no!="" && arrParams.quotation_status=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name)
									.and(query("quotation_status").is(arrParams.quotation_status))
											.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name=="" && arrParams.quotation_no!="" && arrParams.quotation_status!="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
									.and(query("quotation_no").search(arrParams.quotation_no))
											.on(retHistory);
					}
					else if(arrParams.quotation_request_no !="" && arrParams.name=="" && arrParams.quotation_no=="" && arrParams.quotation_status!="")
					{
						output = query("quotation_status").is(arrParams.quotation_status)
								.and(query("quotation_request_no").search(arrParams.quotation_request_no))
										.on(retHistory);			
					}
					else if(arrParams.quotation_request_no !="" && arrParams.name=="" && arrParams.quotation_no!="" && arrParams.quotation_status=="")
					{
						output = query("quotation_request_no").search(arrParams.quotation_request_no)
									.and(query("quotation_no").search(arrParams.quotation_no))
											.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name!="" && arrParams.quotation_no=="" && arrParams.quotation_status!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("quotation_status").is(arrParams.quotation_status))
											.on(retHistory);
					}

					//3 not empty 1 empty
					else if(arrParams.quotation_request_no !="" && arrParams.name!="" && arrParams.quotation_no!="" && arrParams.quotation_status=="")
					{
						output = query("quotation_request_no").search(arrParams.quotation_request_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("quotation_no").search(arrParams.quotation_no))
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name!="" && arrParams.quotation_no!="" && arrParams.quotation_status!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("quotation_no").search(arrParams.quotation_no))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no !="" && arrParams.name=="" && arrParams.quotation_no!="" && arrParams.quotation_status!="")
					{
						output = query("quotation_request_no").search(arrParams.quotation_request_no)
									.and(query("quotation_no").search(arrParams.quotation_no))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no !="" && arrParams.name!="" && arrParams.quotation_no=="" && arrParams.quotation_status!="")
					{
						output = query("quotation_request_no").search(arrParams.quotation_request_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name=="" && arrParams.quotation_no=="" && arrParams.quotation_status=="")
					{
						output = retHistory;
					}
			

					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								quotation_request : output
							},
							successMessage: 'Quotation Request List',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								quotation_request : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							quotation_request : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/payables_quotation_request_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getQuotationRequestDetails(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							quotation_request_details : retHistory
				
						},
						successMessage: 'Request quotation details',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							quotation_request : {}
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/payables_quotation_received_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date,
		name        : req.body.name,
		order_no    : req.body.order_no,
		quotation_no         : req.body.quotation_no,
		quotation_status     : req.body.quotation_status,

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getQuotationReceivedList(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					var output =[];
					//3 empty 1 not empty
					if(arrParams.quotation_no!="" && arrParams.name!="" && arrParams.order_no!="" && arrParams.quotation_status!="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("order_no").search(arrParams.order_no))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_no !="" && arrParams.name=="" && arrParams.order_no=="" && arrParams.quotation_status=="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)							
										.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name!="" && arrParams.order_no=="" && arrParams.quotation_status=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())							
										.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name=="" && arrParams.order_no!="" && arrParams.quotation_status=="")
					{
						output = query("order_no").search(arrParams.order_no)
										.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name=="" && arrParams.order_no=="" && arrParams.quotation_status!="")
					{
						output = query("quotation_status").is(arrParams.quotation_status)
										.on(retHistory);
					}

					//2 empty 2 not empty
					if(arrParams.quotation_no!="" && arrParams.name!="" && arrParams.order_no=="" && arrParams.quotation_status=="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
										.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name!="" && arrParams.order_no!="" && arrParams.quotation_status=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name)
									.and(query("order_no").search(arrParams.order_no))
											.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name=="" && arrParams.order_no!="" && arrParams.quotation_status!="")
					{
						output = query("order_no").search(arrParams.order_no)
									.and(query("quotation_status").is(arrParams.quotation_status))
											.on(retHistory);
					}
					else if(arrParams.quotation_no !="" && arrParams.name=="" && arrParams.order_no=="" && arrParams.quotation_status!="")
					{
						output = query("quotation_status").is(arrParams.quotation_status)
								.and(query("quotation_no").search(arrParams.quotation_no))
										.on(retHistory);			
					}
					else if(arrParams.quotation_no !="" && arrParams.name=="" && arrParams.order_no!="" && arrParams.quotation_status=="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
									.and(query("order_no").search(arrParams.order_no))
											.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name!="" && arrParams.order_no=="" && arrParams.quotation_status!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("quotation_status").is(arrParams.quotation_status))
											.on(retHistory);
					}

					//3 not empty 1 empty
					else if(arrParams.quotation_no !="" && arrParams.name!="" && arrParams.order_no!="" && arrParams.quotation_status=="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("order_no").search(arrParams.order_no))
										.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name!="" && arrParams.order_no!="" && arrParams.quotation_status!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("order_no").search(arrParams.order_no))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_no !="" && arrParams.name=="" && arrParams.order_no!="" && arrParams.quotation_status!="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
									.and(query("order_no").search(arrParams.order_no))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_no !="" && arrParams.name!="" && arrParams.order_no=="" && arrParams.quotation_status!="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name=="" && arrParams.order_no=="" && arrParams.quotation_status=="")
					{
						output = retHistory;
					}
			
					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								quotation_received_list : output
							},
							successMessage: 'Quotation Request List',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								quotation_received_list : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							quotation_received_list : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/payables_quotation_received_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getQuotationReceiveDetails(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							quotation_request_details : retHistory
				
						},
						successMessage: 'Request quotation details',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							quotation_request : {}
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/payables_quotation_received_accept_reject',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric(),
	check('flag','Invalid flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		quotation_request_no : req.body.quotation_request_no,
		id                   : req.body.id,
		flag                 : req.body.flag

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			if(parseInt(arrParams.flag) === 1 || parseInt(arrParams.flag) === 2)
			{


				HelperJazepay.acceptRejectQuotationReceived(arrParams, function(retResult){	


					if(parseInt(retResult) === 1)
					{	

						var successMessage = "Quotation received is accepted";
						if(parseInt(arrParams.flag) == 2){
							successMessage = "Quotation received is rejected";
						}

						jsonRes = {
							success: '1',
							result: {},
							successMessage: successMessage,
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else if(parseInt(retResult) === 2)
					{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
									
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Unable to process request. Please try again.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					
					}

				});

			}
			else
			{
				jsonRes = {
					success: '0',
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}


		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/update_quotation_request',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric(),
	check('flag','Invalid flag').isNumeric(),
	check('product_values','Invalid product_values').isLength({ min: 1 }),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token       : req.body.api_token,
		account_id      : req.body.account_id,
		id              : req.body.id,
		flag            : req.body.flag,
		product_values  : req.body.product_values,
	};


	// flag = 1 update
	// flag = 2 cancel

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.updateQuotationRequest(arrParams, function(retStatus){

				if(parseInt(retStatus)===1)
				{

					var resMes = 'Quotation is successfully cancelled.';
					if(parseInt(arrParams.flag) === 1){
						resMes = 'Quotation is successfully updated.';
					}

					jsonRes = {
						success: '1',
						result: {},
						successMessage: resMes,
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else if(parseInt(retStatus)===2)
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Unable to process request. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
		
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/payables_order_quotation_request',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('request_account_id','Invalid request_account_id').isNumeric(),
	check('quotation_request_no','Invalid quotation_request_no').isNumeric(),
	check('invoice_ref_no','Invalid invoice_ref_no').isNumeric(),


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		quotation_request_no : req.body.quotation_request_no,
		invoice_ref_no       : req.body.invoice_ref_no,
		request_account_id   : req.body.request_account_id,
		product_values       : req.body.product_values

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.reqestOrderQuotation(arrParams, function(retResult){	

				if(parseInt(retResult) === 1)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Order quotation is successfully send.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Unable to process request. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/payables_order_quotation_request_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date,
		order_no      : req.body.order_no,
		quotation_no  : req.body.quotation_no,
		invoice_no    : req.body.invoice_no,
		name          : req.body.name,
		status        : req.body.status,

	};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getOrderQuotationRequestList(arrParams, function(retHistory){

			
				if(retHistory!=null)
				{

					var output =[];
					//4 empty 1 not empty
					if(arrParams.order_no!="" && arrParams.quotation_no!="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("order_no").search(arrParams.order_no)
									.and(query("quotation_no").search(arrParams.quotation_no))
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("invoice_no").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.order_no!="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)							
										.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no!="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("quotation_no").search(arrParams.quotation_no)							
										.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)
										.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
										.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("status").is(arrParams.status)
										.on(retHistory);
					}

					//3 empty 2 not empty
					else if(arrParams.order_no!="" && arrParams.quotation_no!="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)
									.and(query("quotation_no").search(arrParams.quotation_no))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no!="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("quotation_no").search(arrParams.quotation_no)
									.and(query("invoice_no").search(arrParams.invoice_no))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)									
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())					
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.order_no!="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("status").is(arrParams.status)						
									.and(query("order_no").search(arrParams.order_no))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no!="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())						
									.and(query("quotation_no").search(arrParams.quotation_no))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no!="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("quotation_no").search(arrParams.quotation_no)						
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)						
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.order_no!="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)						
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}

					//3 not empty 2 empty
					else if(arrParams.order_no!="" && arrParams.quotation_no!="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.status=="" )
					{		
						output = query("order_no").search(arrParams.order_no)
									.and(query("quotation_no").search(arrParams.quotation_no))
									.and(query("invoice_no").search(arrParams.invoice_no))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no!="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("quotation_no").search(arrParams.quotation_no)
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)																
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.order_no!="" && arrParams.quotation_no=="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory)
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no!="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("quotation_no").search(arrParams.quotation_no)							
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("status").is(arrParams.status))
									.on(retHistory)
					}
					else if(arrParams.order_no!="" && arrParams.quotation_no!="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)							
									.and(query("quotation_no").search(arrParams.quotation_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory)
					}
					else if(arrParams.order_no!="" && arrParams.quotation_no=="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("order_no").search(arrParams.order_no)							
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("status").is(arrParams.status))
									.on(retHistory)
					}
					else if(arrParams.order_no!="" && arrParams.quotation_no=="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)							
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory)
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = retHistory;
					}
			
					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								payables_order_quotation_request_list : output
							},
							successMessage: 'Quotation Request List',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								payables_order_quotation_request_list : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payables_order_quotation_request_list : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/payables_order_quotation_request_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getOrderRequestQuotationDetails(arrParams, function(retDetails){

				if(retDetails!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							payables_order_quotation_request_details : retDetails
				
						},
						successMessage: 'Order Quotation Request Details',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/payables_invoice_received_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date,
		invoice_no  : req.body.invoice_no,
		order_no    : req.body.order_no,
		payment_no  : req.body.payment_no,
		name        : req.body.name,
		status      : req.body.status,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getInvoiceReceivedList(arrParams, function(retHistory){

				if(retHistory!=null)
				{
					
					var output =[];
					//4 empty 1 not empty
					if(arrParams.invoice_no!="" && arrParams.order_no!="" && arrParams.payment_no!="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)
									.and(query("order_no").search(arrParams.order_no))
									.and(query("payment_no").search(arrParams.payment_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.invoice_no!="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)							
										.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no!="" && arrParams.payment_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)							
										.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no!="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("payment_no").search(arrParams.payment_no)
										.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
										.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("status").is(arrParams.status)
										.on(retHistory);
					}

					//3 empty 2 not empty
					else if(arrParams.invoice_no!="" && arrParams.order_no!="" && arrParams.payment_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)
									.and(query("order_no").search(arrParams.order_no))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no!="" && arrParams.payment_no!="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)
									.and(query("payment_no").search(arrParams.payment_no))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("payment_no").search(arrParams.payment_no)									
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())					
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.invoice_no!="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("status").is(arrParams.status)						
									.and(query("invoice_no").search(arrParams.invoice_no))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no!="" && arrParams.payment_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)						
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no!="" && arrParams.payment_no=="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("order_no").search(arrParams.order_no)						
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())			
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.invoice_no!="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)						
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}

					//3 not empty 2 empty
					else if(arrParams.invoice_no!="" && arrParams.order_no!="" && arrParams.payment_no!="" && arrParams.name=="" && arrParams.status=="" )
					{		
						output = query("invoice_no").search(arrParams.invoice_no)
									.and(query("order_no").search(arrParams.order_no))
									.and(query("payment_no").search(arrParams.payment_no))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no!="" && arrParams.payment_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)
									.and(query("payment_no").search(arrParams.payment_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no!="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("payment_no").search(arrParams.payment_no)																
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.invoice_no!="" && arrParams.order_no=="" && arrParams.payment_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)
									.and(query("payment_no").search(arrParams.payment_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory)
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no!="" && arrParams.payment_no=="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("order_no").search(arrParams.order_no)							
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("status").is(arrParams.status))
									.on(retHistory)
					}
					else if(arrParams.invoice_no!="" && arrParams.order_no!="" && arrParams.payment_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)							
									.and(query("order_no").search(arrParams.order_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory)
					}
					else if(arrParams.invoice_no!="" && arrParams.order_no=="" && arrParams.payment_no!="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)							
									.and(query("payment_no").search(arrParams.payment_no))
									.and(query("status").is(arrParams.status))
									.on(retHistory)
					}
					else if(arrParams.invoice_no!="" && arrParams.order_no=="" && arrParams.payment_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)							
									.and(query("payment_no").search(arrParams.payment_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory)
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = retHistory;
					}

			
					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								payables_order_quotation_request_list : output
							},
							successMessage: 'Order quotaion Request list',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								payables_order_quotation_request_list : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payables_order_quotation_request_list : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}


			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/payables_invoice_received_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getInvoiceReceiveDetails(arrParams, function(retDetails){

				if(retDetails!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							invoice_received_details : retDetails
						},
						successMessage: 'Invoice received details',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							invoice_received_details : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/payables_payments_made_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		from_date    : req.body.from_date,
		to_date      : req.body.to_date,
		payment_no   : req.body.payment_no,
		name         : req.body.name,
		amount       : req.body.amount,
		payment_type : req.body.payment_type,

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getPaymentsMadeList(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					var output =[];
					//3 empty 1 not empty
					if(arrParams.payment_no!="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("quotation_no").search(arrParams.payment_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("amount").search(arrParams.amount))
									.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);
					}
					else if(arrParams.payment_no !="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = query("payment_no").search(arrParams.payment_no)							
										.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())							
										.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("amount").search(arrParams.amount)
										.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("payment_type").is(arrParams.payment_type)
										.on(retHistory);
					}

					//2 empty 2 not empty
					if(arrParams.payment_no!="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = query("payment_no").search(arrParams.payment_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
										.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name)
									.and(query("order_no").search(arrParams.amount))
											.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("amount").search(arrParams.amount)
									.and(query("payment_type").is(arrParams.payment_type))
											.on(retHistory);
					}
					else if(arrParams.payment_no !="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("payment_no").is(arrParams.payment_no)
								.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);			
					}
					else if(arrParams.payment_no !="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("payment_no").search(arrParams.payment_no)
									.and(query("amount").search(arrParams.amount))
											.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("payment_type").is(arrParams.payment_type))
											.on(retHistory);
					}

					//3 not empty 1 empty
					else if(arrParams.payment_no !="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("payment_no").search(arrParams.payment_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("amount").search(arrParams.amount))
										.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("amount").search(arrParams.amount))
									.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);
					}
					else if(arrParams.payment_no !="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("amount").search(arrParams.amount)
									.and(query("payment_type").is(arrParams.payment_type))
									.and(query("payment_no").search(arrParams.payment_no))
										.on(retHistory);
					}
					else if(arrParams.payment_no !="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("payment_no").search(arrParams.payment_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = retHistory;
					}
			
					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								payables_payments_made_list : output
							},
							successMessage: 'Payment made list',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								payables_payments_made_list : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payables_payments_made_list : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/payables_payments_made_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getPaymentsMadeDetails(arrParams, function(retDetails){

				if(retDetails!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							payables_payments_made_details : retDetails
						},
						successMessage: 'Payments made details',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payables_payments_made_details : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/payables_cash_purchase_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		from_date    : req.body.from_date,
		to_date      : req.body.to_date,
		document_no  : req.body.document_no,
		name         : req.body.name,
		amount       : req.body.amount,
		payment_type : req.body.payment_type,

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getCashPurchaseList(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					var output =[];
					//3 empty 1 not empty
					if(arrParams.document_no!="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("document_no").search(arrParams.document_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("amount").search(arrParams.amount))
									.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);
					}
					else if(arrParams.document_no !="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = query("document_no").search(arrParams.document_no)							
										.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())							
										.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("amount").search(arrParams.amount)
										.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("payment_type").is(arrParams.payment_type)
										.on(retHistory);
					}

					//2 empty 2 not empty
					if(arrParams.document_no!="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = query("document_no").search(arrParams.document_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
										.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name)
									.and(query("order_no").search(arrParams.amount))
											.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("amount").search(arrParams.amount)
									.and(query("payment_type").is(arrParams.payment_type))
											.on(retHistory);
					}
					else if(arrParams.document_no !="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("document_no").is(arrParams.document_no)
								.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);			
					}
					else if(arrParams.document_no !="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("document_no").search(arrParams.document_no)
									.and(query("amount").search(arrParams.amount))
											.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("payment_type").is(arrParams.payment_type))
											.on(retHistory);
					}

					//3 not empty 1 empty
					else if(arrParams.document_no !="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("document_no").search(arrParams.document_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("amount").search(arrParams.amount))
										.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("amount").search(arrParams.amount))
									.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);
					}
					else if(arrParams.document_no !="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("amount").search(arrParams.amount)
									.and(query("payment_type").is(arrParams.payment_type))
									.and(query("document_no").search(arrParams.document_no))
										.on(retHistory);
					}
					else if(arrParams.document_no !="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("document_no").search(arrParams.document_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = retHistory;
					}
			
					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								payables_cash_purchase_list : output
							},
							successMessage: 'Payment made list',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								payables_cash_purchase_list : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payables_cash_purchase_list : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/payables_cash_purchase_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getCashPurchaseDetails(arrParams, function(retDetails){

				if(retDetails!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							payables_cash_purchase_details : retDetails
						},
						successMessage: 'Cash purcahse details',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payables_cash_purchase_details : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/payables_receipt_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		from_date    : req.body.from_date,
		to_date      : req.body.to_date,
		receipt_no   : req.body.receipt_no,
		name         : req.body.name,
		payment_no   : req.body.payment_no,
		amount       : req.body.amount,

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getPayablesReceiptList(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					var output =[];
					//3 empty 1 not empty
					if(arrParams.receipt_no!="" && arrParams.name!="" && arrParams.payment_no!="" && arrParams.amount!="")
					{

						output = query("receipt_no").search(arrParams.receipt_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("payment_no").search(arrParams.payment_no))
									.and(query("amount").search(arrParams.amount))
										.on(retHistory);
				
					}
					else if(arrParams.receipt_no !="" && arrParams.name=="" && arrParams.payment_no=="" && arrParams.amount=="")
					{
						output = query("receipt_no").search(arrParams.receipt_no)							
										.on(retHistory);
					}
					else if(arrParams.receipt_no =="" && arrParams.name!="" && arrParams.payment_no=="" && arrParams.amount=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())							
										.on(retHistory);
					}
					else if(arrParams.receipt_no =="" && arrParams.name=="" && arrParams.payment_no!="" && arrParams.amount=="")
					{
						output = query("payment_no").search(arrParams.payment_no)
										.on(retHistory);
					}
					else if(arrParams.receipt_no =="" && arrParams.name=="" && arrParams.payment_no=="" && arrParams.amount!="")
					{
						output = query("amount").search(arrParams.amount)
										.on(retHistory);
					}

					//2 empty 2 not empty
					if(arrParams.receipt_no!="" && arrParams.name!="" && arrParams.payment_no=="" && arrParams.amount=="")
					{
						output = query("receipt_no").search(arrParams.receipt_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
										.on(retHistory);
					}
					else if(arrParams.receipt_no =="" && arrParams.name!="" && arrParams.payment_no!="" && arrParams.amount=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name)
									.and(query("payment_no").search(arrParams.payment_no))
											.on(retHistory);
					}
					else if(arrParams.receipt_no =="" && arrParams.name=="" && arrParams.payment_no!="" && arrParams.amount!="")
					{
						output = query("payment_no").search(arrParams.payment_no)
									.and(query("amount").search(arrParams.amount))
											.on(retHistory);
					}
					else if(arrParams.receipt_no !="" && arrParams.name=="" && arrParams.payment_no=="" && arrParams.amount!="")
					{
						output = query("receipt_no").search(arrParams.receipt_no)
								.and(query("amount").search(arrParams.amount))
										.on(retHistory);			
					}
					else if(arrParams.receipt_no !="" && arrParams.name=="" && arrParams.payment_no!="" && arrParams.amount=="")
					{
						output = query("receipt_no").search(arrParams.receipt_no)
									.and(query("amount").search(arrParams.amount))
											.on(retHistory);
					}
					else if(arrParams.receipt_no =="" && arrParams.name!="" && arrParams.payment_no=="" && arrParams.amount!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("amount").search(arrParams.amount))
											.on(retHistory);
					}

					//3 not empty 1 empty
					else if(arrParams.receipt_no !="" && arrParams.name!="" && arrParams.payment_no!="" && arrParams.amount=="")
					{
						output = query("receipt_no").search(arrParams.receipt_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("payment_no").search(arrParams.payment_no))
										.on(retHistory);
					}
					else if(arrParams.receipt_no =="" && arrParams.name!="" && arrParams.payment_no!="" && arrParams.amount!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("payment_no").search(arrParams.payment_no))
									.and(query("amount").search(arrParams.amount))
										.on(retHistory);
					}
					else if(arrParams.receipt_no !="" && arrParams.name=="" && arrParams.payment_no!="" && arrParams.amount!="")
					{
						output = query("amount").search(arrParams.amount)
									.and(query("payment_no").search(arrParams.payment_no))
									.and(query("receipt_no").search(arrParams.receipt_no))
										.on(retHistory);
					}
					else if(arrParams.receipt_no !="" && arrParams.name!="" && arrParams.payment_no=="" && arrParams.amount!="")
					{
						output = query("receipt_no").search(arrParams.receipt_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("amount").search(arrParams.amount))
										.on(retHistory);
					}
					else if(arrParams.receipt_no =="" && arrParams.name=="" && arrParams.payment_no=="" && arrParams.amount=="")
					{
						output = retHistory;
					}
			
					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								payables_receipt_list : output
							},
							successMessage: 'Payables Receipt List',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								payables_receipt_list : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payables_cash_purchase_list : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/payables_receipt_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getPayableReceiptDetails(arrParams, function(retDetails){

				if(retDetails!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							payables_receipt_details : retDetails
						},
						successMessage: 'Payables receipt details',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payables_receipt_details : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/payables_credit_notes_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		from_date    : req.body.from_date,
		to_date      : req.body.to_date,
		credit_no    : req.body.credit_no,
		name         : req.body.name,
		invoice_no   : req.body.invoice_no,
		amount       : req.body.amount,
		status       : req.body.status,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getPayablesCreditNotesList(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					var output =[];
					//3 empty 2 not empty
					if(arrParams.credit_no!="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.amount!="" && arrParams.status!="" )
					{
						output = query("credit_no").search(arrParams.credit_no)
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("amount").search(arrParams.amount))
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.credit_no!="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount=="" && arrParams.status=="" )
					{
						output = query("credit_no").search(arrParams.credit_no)							
										.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.amount=="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)							
										.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.amount=="" && arrParams.status=="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
										.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("amount").search(arrParams.amount)
										.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount=="" && arrParams.status!="" )
					{
						output = query("status").is(arrParams.status)
										.on(retHistory);
					}

					//3 empty 2 not empty
					else if(arrParams.credit_no!="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.amount=="" && arrParams.status=="" )
					{
						output = query("credit_no").search(arrParams.credit_no)
									.and(query("invoice_no").search(arrParams.invoice_no))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.amount=="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())								
									.and(query("amount").search(arrParams.amount))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount!="" && arrParams.status!="" )
					{
						output = query("amount").search(arrParams.amount)					
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.credit_no!="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount=="" && arrParams.status!="" )
					{
						output = query("status").is(arrParams.status)						
									.and(query("credit_no").search(arrParams.credit_no))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)						
									.and(query("amount").search(arrParams.amount))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.amount=="" && arrParams.status!="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)						
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount!="" && arrParams.status!="" )
					{
						output = query("amount").search(arrParams.amount)			
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.credit_no!="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("credit_no").search(arrParams.credit_no)						
									.and(query("amount").search(arrParams.amount))
									.on(retHistory);
					}

					//3 not empty 2 empty
					else if(arrParams.credit_no!="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.amount=="" && arrParams.status=="" )
					{		
						output = query("credit_no").search(arrParams.credit_no)
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("amount").search(arrParams.amount))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.amount!="" && arrParams.status!="" )
					{
						output = query("payment_no").search(arrParams.payment_no)																
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.credit_no!="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("credit_no").search(arrParams.credit_no)
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("amount").search(arrParams.amount))
									.on(retHistory)
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.amount!="" && arrParams.status!="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)							
									.and(query("amount").search(arrParams.amount))
									.and(query("status").is(arrParams.status))
									.on(retHistory)
					}
					else if(arrParams.credit_no!="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("credit_no").search(arrParams.credit_no)							
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("amount").search(arrParams.amount))
									.on(retHistory)
					}
					else if(arrParams.credit_no!="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.amount=="" && arrParams.status!="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)							
									.and(query("payment_no").search(arrParams.payment_no))
									.and(query("status").is(arrParams.status))
									.on(retHistory)
					}
					else if(arrParams.credit_no!="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())						
									.and(query("amount").search(arrParams.amount))
									.and(query("credit_no").search(arrParams.credit_no))
									.on(retHistory)
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount=="" && arrParams.status=="" )
					{
						output = retHistory;
					}
			
					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								payables_credit_notes_list : output
							},
							successMessage: 'Credit notes list.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								payables_credit_notes_list : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payables_credit_notes_list : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/payables_credit_notes_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getPayablesCreditNotesDetails(arrParams, function(retDetails){

				if(retDetails!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							payables_credit_notes_details : retDetails
						},
						successMessage: 'Payables receipt details',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payables_credit_notes_details : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/payables_pay_invoice_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getPayablesInvoicePayDetails(arrParams, function(retDetails){

				if(retDetails!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							payables_pay_invoice_details : retDetails
						},
						successMessage: 'Payables pay invoice details details',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payables_pay_invoice_details : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/payables_pay_invoice',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric(),
	check('payment_no_req_id','Invalid payment_no_req_id').isNumeric(),
	check('payment_type','Invalid payment_type').isNumeric(),
	check('payment_id','Invalid payment_id').isNumeric(),
	check('security_pin','Invalid security_pin').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token         : req.body.api_token,
		account_id        : req.body.account_id,
		id                : req.body.id,
		payment_no_req_id : req.body.payment_no_req_id,
		payment_type      : req.body.payment_type,
		payment_id        : req.body.payment_id,
		security_pin	  : crypto.createHash('md5').update(req.body.security_pin).digest("hex")
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.checkSecurityPin(arrParams, function(retSec){

				if(retSec!=null)
				{
					HelperJazepay.payablesPayInvoice(arrParams, function(retDetails){

						if(parseInt(retDetails)===1)
						{

							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'Payment is complete.',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
						else if(parseInt(retDetails)===2)
						{

							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'No result found.',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
						else
						{

							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'Unable to process request. Please try again.',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
						}

					});
				}
				else
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Security pin did not match. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/unpaid_payments',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		name        : req.body.name,
		amount      : req.body.amount,
		total       : req.body.total
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getJazepayUnpaidPayments(arrParams, function(retHistory){

				if(retHistory!=null)
				{	

					var output =[];
					//2 not empty 1 empty
					if(arrParams.amount!="" && arrParams.name!="" && arrParams.total!="")
					{
						output = query("amount").search(arrParams.amount)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("total").search(arrParams.total))
										.on(retHistory);
					}
					else if(arrParams.amount=="" && arrParams.name!="" && arrParams.total!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
								.and(query("total").search(arrParams.total))
									.on(retHistory);
					}
					else if(arrParams.amount!="" && arrParams.name=="" && arrParams.total!="")
					{
						output = query("amount").search(arrParams.amount)
								.and(query("total").search(arrParams.total))
									.on(retHistory);
					}
					else if(arrParams.amount!="" && arrParams.name!="" && arrParams.total=="")
					{
						output = query("amount").search(arrParams.amount)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}

					//2 empty 1 not empty
					else if(arrParams.amount!="" && arrParams.name=="" && arrParams.total=="")
					{
						output = query("amount").search(arrParams.amount).on(retHistory);
					}
					else if(arrParams.amount=="" && arrParams.name!="" && arrParams.total=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase()).on(retHistory);
					}
					else if(arrParams.amount=="" && arrParams.name=="" && arrParams.total!="")
					{
						output = query("total").search(arrParams.total).on(retHistory);
					}
					else if(arrParams.amount=="" && arrParams.name=="" && arrParams.total=="")
					{
						output = retHistory;
					}
					

					if(output.length > 0 )
					{

						jsonRes = {
							success: '1',
							result: {
								unpaid_payments : output
							},
							successMessage: 'Jazepay unpaid payments.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								unpaid_payments : []
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
			
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							unpaid_payments : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



// ------------------------------------------------------------------------------------------- receivable debtors list start ----------------------------------------------------------------------------- //

router.post('/jazepay/receivable_debtors_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		month       : req.body.month,
		year        : req.body.year,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getReceivableDebtorsList(arrParams, function(retHistory,total_amount){

				if(retHistory!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_debtors_list : retHistory,
							total_amount            : total_amount.toFixed(2)
						},
						successMessage: 'Jazepay Payable Creditors Lists',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_debtors_list : [],
							total_amount            : '0.00'
				
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


// ------------------------------------------------------------------------------------------- receivable debtors list end ----------------------------------------------------------------------------- //

// ------------------------------------------------------------------------------------------- receivable debtors transaction list start ----------------------------------------------------------------------------- //

router.post('/jazepay/receivable_debtor_transactions',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('debtor_account_id','Invalid debtor_account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		debtor_account_id  : req.body.debtor_account_id,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date,
		ref_no      : req.body.ref_no,
		trans_type  : req.body.trans_type

	};

	// ref_type 1 = invoice
	// ref_type 2 = payment
	// ref_type 3 = Cash Purcahse

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getReceivableDebtorTransactions(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					var output = [];
					if(arrParams.ref_no!="" && arrParams.trans_type!="")
					{
						output = query("ref_no").search(arrParams.ref_no)
									.and(query("trans_type").search(arrParams.trans_type))
										.on(retHistory.transactions);
						retHistory.transactions = output;
					}
					else if(arrParams.ref_no=="" && arrParams.trans_type!="")
					{
						output = query("trans_type").search(arrParams.trans_type).on(retHistory.transactions);
						retHistory.transactions = output;
					}

					else if(arrParams.ref_no!="" && arrParams.trans_type=="")
					{
						output = query("ref_no").search(arrParams.ref_no).on(retHistory.transactions);
						retHistory.transactions = output;
					}					
					else
					{
						output = retHistory;
					}

					jsonRes = {
						success: '1',
						result: {
							receivable_debtor_transactions : output
						},
						successMessage: 'Jazepay Payable Creditor Transactions',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_debtor_transactions : [],
				
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


// ------------------------------------------------------------------------------------------- receivable debtors transaction list end ----------------------------------------------------------------------------- //



// ------------------------------------------------------------------------------------------- receivable debtor maintenance start ----------------------------------------------------------------------------- //

router.post('/jazepay/receivable_debtors_maintenance',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		month       : req.body.month,
		year        : req.body.year,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date

	};

	// ref_type 1 = invoice
	// ref_type 2 = payment

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.receivableDebtorsMaintenance(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							payables_creditors_maintenance : retHistory,
						},
						successMessage: 'Jazepay Payable Creditors Lists',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							payables_creditors_maintenance : [],
				
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


// ------------------------------------------------------------------------------------------- receivable debtor maintenance end ----------------------------------------------------------------------------- //


//------------------------------------------------------------------  receivable debtor retistration start  -----------------------------------------------------------------------------------------------//

router.post('/jazepay/receivable_debtors_maintenance_registration',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('debtor_account_id','Invalid debtor_account_id').isNumeric(),
	check('registration_no','Invalid registration_no').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token           : req.body.api_token,
		account_id          : req.body.account_id,
		debtor_account_id   : req.body.debtor_account_id,
		registration_no     : req.body.registration_no,
		credit_limit        : req.body.credit_limit,
		discount            : req.body.discount,
		vat_no              : req.body.vat_no,
		payment_terms       : req.body.payment_terms,
		contact_person      : req.body.contact_person

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.procDebtorRegistration(arrParams, function(retStatus){

				if(parseInt(retStatus)===1)
				{	

					setTimeout(function(){ 
	
						var addParam = {profile_id: arrParams.debtor_account_id, account_id: arrParams.account_id};
						HelperJazepay.getCreditorRegistration(addParam, function(retAdd1){	

							jsonRes = {
								success: '1',
								result: {
									receivable_debtors_maintenance_registration : retAdd1,
								},
								successMessage: 'Receivable Deptor registration is complete.',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();

						});

					}, 500);
				}
				else if(parseInt(retStatus)===0)
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_debtors_maintenance_registration : {},
				
						},
						successMessage: 'Unable to process request. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});

//------------------------------------------------------------------ receivable debtor retistration end  -----------------------------------------------------------------------------------------------//


//------------------------------------------------------------------  receivable debtor address update start   -----------------------------------------------------------------------------------------------//

router.post('/jazepay/receivable_debtors_maintenance_address_update',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('debtor_account_id','Invalid debtor_account_id').isNumeric(),
	check('flag','Invalid flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		debtor_account_id : req.body.debtor_account_id,
		country_id       : req.body.country_id,
		state_id         : req.body.state_id,
		city_id          : req.body.city_id,
		area_id          : req.body.area_id,
		street_name      : req.body.street_name,
		street_number    : req.body.street_number,
		building_name    : req.body.building_name,
		building_number  : req.body.building_number,
		flag             : req.body.flag,

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			var arrFlags = [1,2,3];

			if(arrFlags.includes(parseInt(arrParams.flag)))
			{
				HelperJazepay.procDebtorAddress(arrParams, function(retStatus){

					if(parseInt(retStatus)===1)
					{

						setTimeout(function(){ 
							
							var addParam = {profile_id: arrParams.debtor_account_id, account_id: arrParams.account_id, type: arrParams.flag};
							HelperJazepay.getCreditorAddressDetails(addParam, function(retResult){	

								var obj = '';
								if(parseInt(arrParams.flag) === 1){
									obj = 'physical_address';
								}else if(parseInt(arrParams.flag) === 2){
									obj = 'delivery_address';
								}else if(parseInt(arrParams.flag) === 3){
									obj = 'postal_address';
								}

								jsonRes = {
									success: '1',
									result: {
										receivable_debtors_maintenance_address_update:{
											[obj] : retResult,
										}
										
									},
									successMessage: 'Debtor address is successfully saved.',
									errorMessage: "" 
								};
								res.status(201).json(jsonRes);
								res.end();
						
							});

						}, 500);

					}
					else if(parseInt(retStatus)===0)
					{

						jsonRes = {
							success: '1',
							result: {
								receivable_debtors_maintenance_address_update : {},
					
							},
							successMessage: 'Unable to process request. Please try again.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
				});
			}
			else
			{
				jsonRes = {
					success: '0',
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});

//------------------------------------------------------------------ receivable debtor address update end   -----------------------------------------------------------------------------------------------//


// ----------------------------------------------------------------- receivable quotation request received list  ---------------------------------------------------------------------------------------- //

router.post('/jazepay/receivable_quotation_request_received_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date,
		name        : req.body.name,
		quotation_request_no : req.body.quotation_request_no,
		quotation_no         : req.body.quotation_no,
		quotation_status     : req.body.quotation_status,

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getReceivableQuotationRequestReceivedList(arrParams, function(retHistory){

				if(retHistory!=null)
				{

	
					var output =[];
					//3 empty 1 not empty
					if(arrParams.quotation_request_no!="" && arrParams.name!="" && arrParams.quotation_no!="" && arrParams.quotation_status!="")
					{
						output = query("quotation_request_no").search(arrParams.quotation_request_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("quotation_no").search(arrParams.quotation_no))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no !="" && arrParams.name=="" && arrParams.quotation_no=="" && arrParams.quotation_status=="")
					{
						output = query("quotation_request_no").search(arrParams.quotation_request_no)							
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name!="" && arrParams.quotation_no=="" && arrParams.quotation_status=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())							
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name=="" && arrParams.quotation_no!="" && arrParams.quotation_status=="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name=="" && arrParams.quotation_no=="" && arrParams.quotation_status!="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
										.on(retHistory);
					}

					//2 empty 2 not empty
					if(arrParams.quotation_request_no!="" && arrParams.name!="" && arrParams.quotation_no=="" && arrParams.quotation_status=="")
					{
						output = query("quotation_request_no").search(arrParams.quotation_request_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name!="" && arrParams.quotation_no!="" && arrParams.quotation_status=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name)
									.and(query("quotation_status").is(arrParams.quotation_status))
											.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name=="" && arrParams.quotation_no!="" && arrParams.quotation_status!="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
									.and(query("quotation_no").search(arrParams.quotation_no))
											.on(retHistory);
					}
					else if(arrParams.quotation_request_no !="" && arrParams.name=="" && arrParams.quotation_no=="" && arrParams.quotation_status!="")
					{
						output = query("quotation_status").is(arrParams.quotation_status)
								.and(query("quotation_request_no").search(arrParams.quotation_request_no))
										.on(retHistory);			
					}
					else if(arrParams.quotation_request_no !="" && arrParams.name=="" && arrParams.quotation_no!="" && arrParams.quotation_status=="")
					{
						output = query("quotation_request_no").search(arrParams.quotation_request_no)
									.and(query("quotation_no").search(arrParams.quotation_no))
											.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name!="" && arrParams.quotation_no=="" && arrParams.quotation_status!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("quotation_status").is(arrParams.quotation_status))
											.on(retHistory);
					}

					//3 not empty 1 empty
					else if(arrParams.quotation_request_no !="" && arrParams.name!="" && arrParams.quotation_no!="" && arrParams.quotation_status=="")
					{
						output = query("quotation_request_no").search(arrParams.quotation_request_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("quotation_no").search(arrParams.quotation_no))
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name!="" && arrParams.quotation_no!="" && arrParams.quotation_status!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("quotation_no").search(arrParams.quotation_no))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no !="" && arrParams.name=="" && arrParams.quotation_no!="" && arrParams.quotation_status!="")
					{
						output = query("quotation_request_no").search(arrParams.quotation_request_no)
									.and(query("quotation_no").search(arrParams.quotation_no))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no !="" && arrParams.name!="" && arrParams.quotation_no=="" && arrParams.quotation_status!="")
					{
						output = query("quotation_request_no").search(arrParams.quotation_request_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_request_no =="" && arrParams.name=="" && arrParams.quotation_no=="" && arrParams.quotation_status=="")
					{
						output = retHistory;
					}
			

					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								receivable_quotation_request_received_list : output
							},
							successMessage: 'Receivable quotation request received list',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								receivable_quotation_request_received_list : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							quotation_request : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


// ----------------------------------------------------------------- receivable quotation request received list  ---------------------------------------------------------------------------------------- //

// ----------------------------------------------------------------- receivable quotation request received details  ---------------------------------------------------------------------------------------- //



router.post('/jazepay/receivable_quotation_request_received_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getReceivableQuotationRequestReceivedDetails(arrParams, function(retDetails){

				if(retDetails!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_quotation_request_received_details : retDetails				
						},
						successMessage: 'Receivable quotation request received details.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});

// ----------------------------------------------------------------- receivable quotation request received details  ---------------------------------------------------------------------------------------- //


// ----------------------------------------------------------------- receivable quotation list  ---------------------------------------------------------------------------------------- //


router.post('/jazepay/receivable_quotations_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date,
		name        : req.body.name,
		quotation_no         : req.body.quotation_no,
		quotation_status     : req.body.quotation_status,
		order_no     : req.body.order_no,

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getReceivableQuotationsList(arrParams, function(retHistory){

				if(retHistory!=null)
				{


	
					var output =[];
					//3 empty 1 not empty
					if(arrParams.quotation_no!="" && arrParams.name!="" && arrParams.order_no!="" && arrParams.quotation_status!="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("order_no").search(arrParams.order_no))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_no !="" && arrParams.name=="" && arrParams.order_no=="" && arrParams.quotation_status=="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)							
										.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name!="" && arrParams.order_no=="" && arrParams.quotation_status=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())							
										.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name=="" && arrParams.order_no!="" && arrParams.quotation_status=="")
					{
						output = query("order_no").search(arrParams.order_no)
										.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name=="" && arrParams.order_no=="" && arrParams.quotation_status!="")
					{
						output = query("quotation_status").is(arrParams.quotation_status)
										.on(retHistory);
					}

					//2 empty 2 not empty
					if(arrParams.quotation_no!="" && arrParams.name!="" && arrParams.order_no=="" && arrParams.quotation_status=="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
										.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name!="" && arrParams.order_no!="" && arrParams.quotation_status=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name)
									.and(query("order_no").search(arrParams.order_no))
											.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name=="" && arrParams.order_no!="" && arrParams.quotation_status!="")
					{
						output = query("order_no").search(arrParams.order_no)
									.and(query("quotation_status").is(arrParams.quotation_status))
											.on(retHistory);
					}
					else if(arrParams.quotation_no !="" && arrParams.name=="" && arrParams.order_no=="" && arrParams.quotation_status!="")
					{
						output = query("quotation_status").is(arrParams.quotation_status)
								.and(query("quotation_no").search(arrParams.quotation_no))
										.on(retHistory);			
					}
					else if(arrParams.quotation_no !="" && arrParams.name=="" && arrParams.order_no!="" && arrParams.quotation_status=="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
									.and(query("order_no").search(arrParams.order_no))
											.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name!="" && arrParams.order_no=="" && arrParams.quotation_status!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("quotation_status").is(arrParams.quotation_status))
											.on(retHistory);
					}

					//3 not empty 1 empty
					else if(arrParams.quotation_no !="" && arrParams.name!="" && arrParams.order_no!="" && arrParams.quotation_status=="")
					{
						output = query("order_no").search(arrParams.order_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("order_no").search(arrParams.order_no))
										.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name!="" && arrParams.order_no!="" && arrParams.quotation_status!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("order_no").search(arrParams.order_no))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_no !="" && arrParams.name=="" && arrParams.order_no!="" && arrParams.quotation_status!="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
									.and(query("order_no").search(arrParams.order_no))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_no !="" && arrParams.name!="" && arrParams.quotation_no=="" && arrParams.quotation_status!="")
					{
						output = query("quotation_no").search(arrParams.quotation_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("quotation_status").is(arrParams.quotation_status))
										.on(retHistory);
					}
					else if(arrParams.quotation_no =="" && arrParams.name=="" && arrParams.order_no=="" && arrParams.quotation_status=="")
					{
						output = retHistory;
					}



					if(output.length > 0 )
					{

						jsonRes = {
							success: '1',
							result: {
								receivable_quotations_list : output
							},
							successMessage: 'Receivable quotations list',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								receivable_quotations_list : []
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					};

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							quotation_request : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});

// ----------------------------------------------------------------- receivable quotation list  ---------------------------------------------------------------------------------------- //

// ----------------------------------------------------------------- receivable quotation details  ---------------------------------------------------------------------------------------- //



router.post('/jazepay/receivable_quotation_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getReceivableQuotationDetails(arrParams, function(retDetails){

				if(retDetails!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_quotation_details : retDetails				
						},
						successMessage: 'Receivable quotation details.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});

// ----------------------------------------------------------------- receivable quotation details  ---------------------------------------------------------------------------------------- //


// ----------------------------------------------------------------- receivable order_received list   ---------------------------------------------------------------------------------------- //


router.post('/jazepay/receivable_order_received_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date,
		order_no      : req.body.order_no,
		quotation_no  : req.body.quotation_no,
		invoice_no    : req.body.invoice_no,
		name          : req.body.name,
		status        : req.body.status,

	};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getReceivableOrderReceivedList(arrParams, function(retHistory){

			
				if(retHistory!=null)
				{

					var output =[];
					//4 empty 1 not empty
					if(arrParams.order_no!="" && arrParams.quotation_no!="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("order_no").search(arrParams.order_no)
									.and(query("quotation_no").search(arrParams.quotation_no))
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("invoice_no").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.order_no!="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)							
										.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no!="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("quotation_no").search(arrParams.quotation_no)							
										.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)
										.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
										.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("status").is(arrParams.status)
										.on(retHistory);
					}

					//3 empty 2 not empty
					else if(arrParams.order_no!="" && arrParams.quotation_no!="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)
									.and(query("quotation_no").search(arrParams.quotation_no))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no!="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("quotation_no").search(arrParams.quotation_no)
									.and(query("invoice_no").search(arrParams.invoice_no))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)									
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())					
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.order_no!="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("status").is(arrParams.status)						
									.and(query("order_no").search(arrParams.order_no))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no!="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())						
									.and(query("quotation_no").search(arrParams.quotation_no))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no!="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("quotation_no").search(arrParams.quotation_no)						
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)						
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.order_no!="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)						
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}

					//3 not empty 2 empty
					else if(arrParams.order_no!="" && arrParams.quotation_no!="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.status=="" )
					{		
						output = query("order_no").search(arrParams.order_no)
									.and(query("quotation_no").search(arrParams.quotation_no))
									.and(query("invoice_no").search(arrParams.invoice_no))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no!="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("quotation_no").search(arrParams.quotation_no)
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)																
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.order_no!="" && arrParams.quotation_no=="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory)
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no!="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("quotation_no").search(arrParams.quotation_no)							
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("status").is(arrParams.status))
									.on(retHistory)
					}
					else if(arrParams.order_no!="" && arrParams.quotation_no!="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)							
									.and(query("quotation_no").search(arrParams.quotation_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory)
					}
					else if(arrParams.order_no!="" && arrParams.quotation_no=="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("order_no").search(arrParams.order_no)							
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("status").is(arrParams.status))
									.on(retHistory)
					}
					else if(arrParams.order_no!="" && arrParams.quotation_no=="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)							
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory)
					}
					else if(arrParams.order_no=="" && arrParams.quotation_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = retHistory;
					}
			
					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								receivable_order_received_list : output
							},
							successMessage: 'Receivable order received list',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								receivable_order_received_list : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_order_received_list : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/receivable_order_received_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getReceivableOrderReceivedDetails(arrParams, function(retDetails){

				if(retDetails!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_order_received_details : retDetails
				
						},
						successMessage: 'Receivable order received Details',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



// ----------------------------------------------------------------- receivable order received list  ---------------------------------------------------------------------------------------- //


router.post('/jazepay/receivable_invoice_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		from_date   : req.body.from_date,
		to_date     : req.body.to_date,
		invoice_no  : req.body.invoice_no,
		order_no    : req.body.order_no,
		payment_no  : req.body.payment_no,
		name        : req.body.name,
		status      : req.body.status,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getReceivableInvoiceList(arrParams, function(retHistory){

				if(retHistory!=null)
				{
					
					var output =[];
					//4 empty 1 not empty
					if(arrParams.invoice_no!="" && arrParams.order_no!="" && arrParams.payment_no!="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)
									.and(query("order_no").search(arrParams.order_no))
									.and(query("payment_no").search(arrParams.payment_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.invoice_no!="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)							
										.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no!="" && arrParams.payment_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)							
										.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no!="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("payment_no").search(arrParams.payment_no)
										.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
										.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("status").is(arrParams.status)
										.on(retHistory);
					}

					//3 empty 2 not empty
					else if(arrParams.invoice_no!="" && arrParams.order_no!="" && arrParams.payment_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)
									.and(query("order_no").search(arrParams.order_no))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no!="" && arrParams.payment_no!="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)
									.and(query("payment_no").search(arrParams.payment_no))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("payment_no").search(arrParams.payment_no)									
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())					
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.invoice_no!="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("status").is(arrParams.status)						
									.and(query("invoice_no").search(arrParams.invoice_no))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no!="" && arrParams.payment_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)						
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no!="" && arrParams.payment_no=="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("order_no").search(arrParams.order_no)						
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())			
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.invoice_no!="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)						
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}

					//3 not empty 2 empty
					else if(arrParams.invoice_no!="" && arrParams.order_no!="" && arrParams.payment_no!="" && arrParams.name=="" && arrParams.status=="" )
					{		
						output = query("invoice_no").search(arrParams.invoice_no)
									.and(query("order_no").search(arrParams.order_no))
									.and(query("payment_no").search(arrParams.payment_no))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no!="" && arrParams.payment_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("order_no").search(arrParams.order_no)
									.and(query("payment_no").search(arrParams.payment_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no!="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("payment_no").search(arrParams.payment_no)																
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.invoice_no!="" && arrParams.order_no=="" && arrParams.payment_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)
									.and(query("payment_no").search(arrParams.payment_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory)
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no!="" && arrParams.payment_no=="" && arrParams.name!="" && arrParams.status!="" )
					{
						output = query("order_no").search(arrParams.order_no)							
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("status").is(arrParams.status))
									.on(retHistory)
					}
					else if(arrParams.invoice_no!="" && arrParams.order_no!="" && arrParams.payment_no=="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)							
									.and(query("order_no").search(arrParams.order_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory)
					}
					else if(arrParams.invoice_no!="" && arrParams.order_no=="" && arrParams.payment_no!="" && arrParams.name=="" && arrParams.status!="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)							
									.and(query("payment_no").search(arrParams.payment_no))
									.and(query("status").is(arrParams.status))
									.on(retHistory)
					}
					else if(arrParams.invoice_no!="" && arrParams.order_no=="" && arrParams.payment_no!="" && arrParams.name!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)							
									.and(query("payment_no").search(arrParams.payment_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory)
					}
					else if(arrParams.invoice_no=="" && arrParams.order_no=="" && arrParams.payment_no=="" && arrParams.name=="" && arrParams.status=="" )
					{
						output = retHistory;
					}

			
					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								receivable_invoice_list : output
							},
							successMessage: 'Receivable invoice list',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								receivable_invoice_list : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_invoice_list : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}


			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/receivable_invoice_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getReceivableInvoiceDetails(arrParams, function(retDetails){

				if(retDetails!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_invoice_details : retDetails
						},
						successMessage: 'Invoice received details',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_invoice_details : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/receivable_credit_notes_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		from_date    : req.body.from_date,
		to_date      : req.body.to_date,
		credit_no    : req.body.credit_no,
		name         : req.body.name,
		invoice_no   : req.body.invoice_no,
		amount       : req.body.amount,
		status       : req.body.status,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getReceivableCreditNotesList(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					var output =[];
					//3 empty 2 not empty
					if(arrParams.credit_no!="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.amount!="" && arrParams.status!="" )
					{
						output = query("credit_no").search(arrParams.credit_no)
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("amount").search(arrParams.amount))
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.credit_no!="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount=="" && arrParams.status=="" )
					{
						output = query("credit_no").search(arrParams.credit_no)							
										.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.amount=="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)							
										.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.amount=="" && arrParams.status=="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
										.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("amount").search(arrParams.amount)
										.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount=="" && arrParams.status!="" )
					{
						output = query("status").is(arrParams.status)
										.on(retHistory);
					}

					//3 empty 2 not empty
					else if(arrParams.credit_no!="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.amount=="" && arrParams.status=="" )
					{
						output = query("credit_no").search(arrParams.credit_no)
									.and(query("invoice_no").search(arrParams.invoice_no))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.amount=="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())								
									.and(query("amount").search(arrParams.amount))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount!="" && arrParams.status!="" )
					{
						output = query("amount").search(arrParams.amount)					
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.credit_no!="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount=="" && arrParams.status!="" )
					{
						output = query("status").is(arrParams.status)						
									.and(query("credit_no").search(arrParams.credit_no))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)						
									.and(query("amount").search(arrParams.amount))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.amount=="" && arrParams.status!="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)						
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount!="" && arrParams.status!="" )
					{
						output = query("amount").search(arrParams.amount)			
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.credit_no!="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("credit_no").search(arrParams.credit_no)						
									.and(query("amount").search(arrParams.amount))
									.on(retHistory);
					}

					//3 not empty 2 empty
					else if(arrParams.credit_no!="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.amount=="" && arrParams.status=="" )
					{		
						output = query("credit_no").search(arrParams.credit_no)
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no!="" && arrParams.name!="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("amount").search(arrParams.amount))
									.on(retHistory);
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.amount!="" && arrParams.status!="" )
					{
						output = query("payment_no").search(arrParams.payment_no)																
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("status").is(arrParams.status))
									.on(retHistory);
					}
					else if(arrParams.credit_no!="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("credit_no").search(arrParams.credit_no)
									.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("amount").search(arrParams.amount))
									.on(retHistory)
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.amount!="" && arrParams.status!="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)							
									.and(query("amount").search(arrParams.amount))
									.and(query("status").is(arrParams.status))
									.on(retHistory)
					}
					else if(arrParams.credit_no!="" && arrParams.invoice_no!="" && arrParams.name=="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("credit_no").search(arrParams.credit_no)							
									.and(query("invoice_no").search(arrParams.invoice_no))
									.and(query("amount").search(arrParams.amount))
									.on(retHistory)
					}
					else if(arrParams.credit_no!="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.amount=="" && arrParams.status!="" )
					{
						output = query("invoice_no").search(arrParams.invoice_no)							
									.and(query("payment_no").search(arrParams.payment_no))
									.and(query("status").is(arrParams.status))
									.on(retHistory)
					}
					else if(arrParams.credit_no!="" && arrParams.invoice_no=="" && arrParams.name!="" && arrParams.amount!="" && arrParams.status=="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())						
									.and(query("amount").search(arrParams.amount))
									.and(query("credit_no").search(arrParams.credit_no))
									.on(retHistory)
					}
					else if(arrParams.credit_no=="" && arrParams.invoice_no=="" && arrParams.name=="" && arrParams.amount=="" && arrParams.status=="" )
					{
						output = retHistory;
					}
				
				

					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								receivable_credit_notes_list : output
							},
							successMessage: 'Credit notes list.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								receivable_credit_notes_list : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_credit_notes_list : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/receivable_credit_notes_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getReceivableCreditNotesDetails(arrParams, function(retDetails){

				if(retDetails!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_credit_notes_details : retDetails
						},
						successMessage: 'Receivable credit notes details',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_credit_notes_details : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/receivable_payments_received_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		from_date    : req.body.from_date,
		to_date      : req.body.to_date,
		payment_no   : req.body.payment_no,
		name         : req.body.name,
		amount       : req.body.amount,
		payment_type : req.body.payment_type,

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getReceivablePaymentsMadeList(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					var output =[];
					//3 empty 1 not empty
					if(arrParams.payment_no!="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("quotation_no").search(arrParams.payment_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("amount").search(arrParams.amount))
									.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);
					}
					else if(arrParams.payment_no !="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = query("payment_no").search(arrParams.payment_no)							
										.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())							
										.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("amount").search(arrParams.amount)
										.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("payment_type").is(arrParams.payment_type)
										.on(retHistory);
					}

					//2 empty 2 not empty
					if(arrParams.payment_no!="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = query("payment_no").search(arrParams.payment_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
										.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name)
									.and(query("order_no").search(arrParams.amount))
											.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("amount").search(arrParams.amount)
									.and(query("payment_type").is(arrParams.payment_type))
											.on(retHistory);
					}
					else if(arrParams.payment_no !="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("payment_no").is(arrParams.payment_no)
								.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);			
					}
					else if(arrParams.payment_no !="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("payment_no").search(arrParams.payment_no)
									.and(query("amount").search(arrParams.amount))
											.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("payment_type").is(arrParams.payment_type))
											.on(retHistory);
					}

					//3 not empty 1 empty
					else if(arrParams.payment_no !="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("payment_no").search(arrParams.payment_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("amount").search(arrParams.amount))
										.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("amount").search(arrParams.amount))
									.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);
					}
					else if(arrParams.payment_no !="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("amount").search(arrParams.amount)
									.and(query("payment_type").is(arrParams.payment_type))
									.and(query("payment_no").search(arrParams.payment_no))
										.on(retHistory);
					}
					else if(arrParams.payment_no !="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("payment_no").search(arrParams.payment_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);
					}
					else if(arrParams.payment_no =="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = retHistory;
					}
			
					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								receivable_payments_received_list : output
							},
							successMessage: 'Receivable payments received list',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								receivable_payments_received_list : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_payments_received_list : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazepay/receivable_payments_received_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getReceivablePaymentsReceivedDetails(arrParams, function(retDetails){

				if(retDetails!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_payments_received_details : retDetails
						},
						successMessage: 'Receivable payments received details',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_payments_received_details : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});





router.post('/jazepay/receivable_cash_sales_purchase_list',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		from_date    : req.body.from_date,
		to_date      : req.body.to_date,
		document_no  : req.body.document_no,
		name         : req.body.name,
		amount       : req.body.amount,
		payment_type : req.body.payment_type,

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getReceivableCashSaleList(arrParams, function(retHistory){

				if(retHistory!=null)
				{

					var output =[];
					//3 empty 1 not empty
					if(arrParams.document_no!="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("document_no").search(arrParams.document_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("amount").search(arrParams.amount))
									.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);
					}
					else if(arrParams.document_no !="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = query("document_no").search(arrParams.document_no)							
										.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())							
										.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("amount").search(arrParams.amount)
										.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("payment_type").is(arrParams.payment_type)
										.on(retHistory);
					}

					//2 empty 2 not empty
					if(arrParams.document_no!="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = query("document_no").search(arrParams.document_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
										.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("name.toLowerCase()").search(arrParams.name)
									.and(query("order_no").search(arrParams.amount))
											.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("amount").search(arrParams.amount)
									.and(query("payment_type").is(arrParams.payment_type))
											.on(retHistory);
					}
					else if(arrParams.document_no !="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("document_no").is(arrParams.document_no)
								.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);			
					}
					else if(arrParams.document_no !="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("document_no").search(arrParams.document_no)
									.and(query("amount").search(arrParams.amount))
											.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("payment_type").is(arrParams.payment_type))
											.on(retHistory);
					}

					//3 not empty 1 empty
					else if(arrParams.document_no !="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type=="")
					{
						output = query("document_no").search(arrParams.document_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("amount").search(arrParams.amount))
										.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name!="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.and(query("amount").search(arrParams.amount))
									.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);
					}
					else if(arrParams.document_no !="" && arrParams.name=="" && arrParams.amount!="" && arrParams.payment_type!="")
					{
						output = query("amount").search(arrParams.amount)
									.and(query("payment_type").is(arrParams.payment_type))
									.and(query("document_no").search(arrParams.document_no))
										.on(retHistory);
					}
					else if(arrParams.document_no !="" && arrParams.name!="" && arrParams.amount=="" && arrParams.payment_type!="")
					{
						output = query("document_no").search(arrParams.document_no)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
									.and(query("payment_type").is(arrParams.payment_type))
										.on(retHistory);
					}
					else if(arrParams.document_no =="" && arrParams.name=="" && arrParams.amount=="" && arrParams.payment_type=="")
					{
						output = retHistory;
					}
			
					if(output.length > 0 ){

						jsonRes = {
							success: '1',
							result: {
								receivable_cash_sales_purchase_list : output
							},
							successMessage: 'Receivable cash purchase list',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								receivable_cash_sales_purchase_list : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_cash_sales_purchase_list : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/receivable_cash_sales_purchase_details',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		id          : req.body.id,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.getCashPurchaseDetails(arrParams, function(retDetails){

				if(retDetails!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_cash_sales_purchase_details : retDetails
						},
						successMessage: 'Cash purcahse details',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							receivable_cash_sales_purchase_details : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/pay_multiple_invoice',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('payment_values','Invalid payment_values').isLength({ min: 1 }),
	check('security_pin','Invalid security_pin').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token         : req.body.api_token,
		account_id        : req.body.account_id,
		payment_values    : req.body.payment_values,
		security_pin	  : crypto.createHash('md5').update(req.body.security_pin).digest("hex")
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.checkSecurityPin(arrParams, function(retSec){

				if(retSec!=null)
				{
					HelperJazepay.payablesPayMultipleInvoice(arrParams, function(retDetails){

						if(parseInt(retDetails)===1)
						{

							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'Payment is complete.',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
						else if(parseInt(retDetails)===2)
						{

							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'No result found.',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
						else
						{

							jsonRes = {
								success: '1',
								result: {},
								successMessage: 'Unable to process request. Please try again.',
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
						}

					});
				}
				else
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Security pin did not match. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/authentication_credit_card',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isLength({ min: 1 }),
	check('csv','Invalid csv').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		id         : req.body.id,
		csv	       : req.body.csv
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.authenticationCreditCard(arrParams, function(retSec){

				if(retSec!=null)
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Authenticated.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}
				else
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Invalid Card details. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});

const fs = require('fs');
const url = require('url');
const path = require('path');
const pdfpath = './uploads/pdf/';


router.post('/jazepay/generate_reports',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('from_date','Invalid from_date').isISO8601().toDate(),
	check('to_date','Invalid to_date').isISO8601().toDate(),


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token        : req.body.api_token,
		account_id       : req.body.account_id,
		from_date        : req.body.from_date,
		to_date          : req.body.to_date,
		transaction_type : req.body.transaction_type,
		payment_type     : req.body.payment_type,
		name             : req.body.name,

	};


		// 1 = cash purchase
		// 2 = credit notes received
		// 3 = fund receipt
		// 4 = international transfers
		// 5 = invocie received
		// 6 = jazepay transfers received
		// 7 = jazepay transfers sen5
		// 8 = national transfers
		// 9 = order sent
		// 10 = payment made sent
		// 11 = quotation received
		// 12 = received receipts
		// 13 = system partners payment


		//1 = jazepay
		//2 = card
		//3 = cash

	


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.generateJazepayReports(arrParams, function(retHistory){

			
				if(retHistory!=null)
				{
					
					var output =[];
					//2 not empty 1 empty

					if(arrParams.payment_type!="" && arrParams.name!="" )
					{
						output = query("payment_type").is(arrParams.payment_type)
								.and(query("name.toLowerCase()").search(arrParams.name.toLowerCase()))
										.on(retHistory);
					}
					else if(arrParams.payment_type=="" && arrParams.name!="" )
					{
						output = query("name.toLowerCase()").search(arrParams.name.toLowerCase())
									.on(retHistory);
					}
					else if(arrParams.payment_type!="" && arrParams.name=="" )
					{
						output = query("payment_type").is(arrParams.payment_type)
									.on(retHistory);
					}
					else
					{
						output = retHistory;
					}
					

					if(output.length > 0 ){






		

					
			        	const PDFDocument = require('../routes/pdfkit-tables.js');
			        	const doc = new PDFDocument({ size: "A4", margin: 50, bufferPages: true });
			   			// const PDFDocument = require("pdfkit");
						//const doc = new PDFDocument;

					

						function generateHeader(doc) {
						  doc
						    .image("jazenet.jpg", 50, 45, { width: 125 })
						    .fontSize(10)
						    .text("Fortune Tower, JLT Cluster C.", 200, 65, { align: "right" })
						    .text("Dubai, UAE", 200, 80, { align: "right" })
						    .moveDown(3);
						}

						function generateTitle(doc) {
						  doc
						    .fontSize(13)
						    .font('Helvetica-Bold')
						    .text("Jazepay Reports", { width: 190, align: "center" } )
						    .moveDown(3);
						}

						// function generateDate(doc,from_date,to_date) {
						//   doc
						//     .fontSize(10)
						//     .font('Helvetica')
						//     .text("Date: "+from_date+" To "+from_date, { width: 125 } )
						//     .moveDown(3);
						// }

						// function generateHr(doc, y) {
						//   doc
						//     .strokeColor("#aaaaaa")
						//     .lineWidth(1)
						//     .moveTo(50, y)
						//     .lineTo(550, y)
						//     .stroke();
						// }

						// function generateTableRow(
						//   doc,
						//   y,
						//   date,
						//   name,
						//   payment_type,
						//   payment_method,
						//   amount,
						//   addpage
						// ) {
						//   doc
						//     .fontSize(10)
						//     .text(date, 50, y)
						//     .text(name, 150, y)
						//     .text(payment_type, 280, y, { width: 90, align: "right" })
						//     .text(payment_method, 370, y, { width: 90, align: "right" })
						//     .text(amount, 0, y, { align: "right" }).moveDown(3)


						// }




						// function generateInvoiceTable(doc, invoice) {
						//   let i;
						//   const invoiceTableTop = 700;

						//   doc.font("Helvetica-Bold");
						//   generateTableRow(
						//     doc,
						//     invoiceTableTop,
						//     "Date",
						//     "Name",
						//     "Payment Type",
						//     "Payment Method",
						//     "Amount"
						//   );

						//   generateHr(doc, invoiceTableTop + 20);
						//   doc.font("Helvetica");

						//   for (i = 0; i < invoice.length; i++) {
						//     const item = invoice[i];
						//     const position = invoiceTableTop + (i + 1) * 30;

						
					 //    	  	generateTableRow(
						// 	      	doc,
						// 	      	position,
						// 	     	item.date,item.name,item.transaction_type,item.payment_type,item.amount
						// 	    );
						
					 //        generateHr(doc, position + 20);

						//   }

						//   doc.font("Helvetica");
						// }



			
				

				    	
			    		generateHeader(doc);
			    		generateTitle(doc);
			    		// generateDate(doc,arrParams.from_date,arrParams.to_date);
	    			 	//generateInvoiceTable(doc, output);



		    			const table1 = {
						    headers: ['Date', 'Name', 'Payment Type', 'Payment Method', 'Amount'],
						    rows: []
						};

						var arr = [];

						output.forEach(element => { 
				  			arr = [element.date,element.name,element.transaction_type,element.payment_type,element.amount];
				  			table1.rows.push(arr);
						}); 
									

						doc.table(table1, {
						    prepareHeader: () => doc.font('Helvetica').fontSize(10),
						   	prepareRow: (row, i) => doc.font('Helvetica').fontSize(9)
						});

			
						// function generateFooter(doc) {
						//   doc
						//     .fontSize(10)
						//     .text(
						//       "Payment is due within 15 days. Thank you for your business.",
						//       50,
						//       780,
						//       { align: "center", width: 500 }
						//     )
			   //  			.moveDown(3);
						// }

						// generateFooter(doc);


						var store = pdfpath+'example.pdf';
						doc.end()
						doc.pipe(fs.createWriteStream(store));
					

// const invoice = {
//   shipping: {
//     name: "John Doe",
//     address: "1234 Main Street",
//     city: "San Francisco",
//     state: "CA",
//     country: "US",
//     postal_code: 94111
//   },
//   items: [
//     {
//       item: "TC 100",
//       description: "Toner Cartridge",
//       quantity: 2,
//       amount: 6000
//     },
//     {
//       item: "USB_EXT",
//       description: "USB Cable Extender",
//       quantity: 1,
//       amount: 2000
//     }
//   ],
//   subtotal: 8000,
//   paid: 0,
//   invoice_nr: 1234
// };


// 						var store = pdfpath+'example.pdf';

// 						// function createInvoice(invoice, store) {
// 							let doc = new PDFDocument({ size: "A4", margin: 50 });

// 							generateHeader(doc);
// 							generateCustomerInformation(doc, invoice);
// 							generateInvoiceTable(doc, output);
// 							generateFooter(doc);

// 							doc.end()
// 							doc.pipe(fs.createWriteStream(store));

// 						// }


// 						function generateHeader(doc) {
// 						  doc
// 						    .image("jazenet.jpg", 50, 45, { width: 50 })
// 						    .fillColor("#444444")
// 						    .fontSize(20)
// 						    .text("ACME Inc.", 110, 57)
// 						    .fontSize(10)
// 						    .text("ACME Inc.", 200, 50, { align: "right" })
// 						    .text("123 Main Street", 200, 65, { align: "right" })
// 						    .text("New York, NY, 10025", 200, 80, { align: "right" })
// 						    .moveDown();
// 						}

// 						function generateCustomerInformation(doc, invoice) {
// 						  doc
// 						    .fillColor("#444444")
// 						    .fontSize(20)
// 						    .text("Invoice", 50, 160);

// 						  generateHr(doc, 185);

// 						  const customerInformationTop = 200;

// 						  doc
// 						    .fontSize(10)
// 						    .text("Invoice Number:", 50, customerInformationTop)
// 						    .font("Helvetica-Bold")
// 						    .text(invoice.invoice_nr, 150, customerInformationTop)
// 						    .font("Helvetica")
// 						    .text("Invoice Date:", 50, customerInformationTop + 15)
// 						    .text(formatDate(new Date()), 150, customerInformationTop + 15)
// 						    .text("Balance Due:", 50, customerInformationTop + 30)
// 						    .text(
// 						      formatCurrency(invoice.subtotal - invoice.paid),
// 						      150,
// 						      customerInformationTop + 30
// 						    )

// 						    .font("Helvetica-Bold")
// 						    .text(invoice.shipping.name, 300, customerInformationTop)
// 						    .font("Helvetica")
// 						    .text(invoice.shipping.address, 300, customerInformationTop + 15)
// 						    .text(
// 						      invoice.shipping.city +
// 						        ", " +
// 						        invoice.shipping.state +
// 						        ", " +
// 						        invoice.shipping.country,
// 						      300,
// 						      customerInformationTop + 30
// 						    )
// 						    .moveDown();

// 						  generateHr(doc, 252);
// 						}

// 						function generateInvoiceTable(doc, invoice) {
// 						  let i;
// 						  const invoiceTableTop = 330;

// 						  doc.font("Helvetica-Bold");
// 						  generateTableRow(
// 							doc,
// 							invoiceTableTop,
// 							"Date",
// 							"Name",
// 							"Payment Type",
// 							"Payment Method",
// 							"Amount"
// 						  );
// 						  generateHr(doc, invoiceTableTop + 20);
// 						  doc.font("Helvetica");

// 						  for (i = 0; i < invoice.length; i++) {

// 						    const position = invoiceTableTop + (i + 1) * 30;

				

// 				    	    generateTableRow(
// 								doc,
// 								position,
// 								invoice[i].date,invoice[i].name,invoice[i].transaction_type,invoice[i].payment_type,invoice[i].amount
// 						    );
					
						   
// 		    	    	 	doc.moveDown(3)
// 						    generateHr(doc, position + 20);
// 						  }

// 						  const subtotalPosition = invoiceTableTop + (i + 1) * 30;
// 						  generateTableRow(
// 						    doc,
// 						    subtotalPosition,
// 						    "",
// 						    "",
// 						    "Subtotal",
// 						    "",
// 						    formatCurrency(invoice.subtotal)
// 						  );

// 						  const paidToDatePosition = subtotalPosition + 20;
// 						  generateTableRow(
// 						    doc,
// 						    paidToDatePosition,
// 						    "",
// 						    "",
// 						    "Paid To Date",
// 						    "",
// 						    formatCurrency(invoice.paid)
// 						  );

// 						  const duePosition = paidToDatePosition + 25;
// 						  doc.font("Helvetica-Bold");
// 						  generateTableRow(
// 						    doc,
// 						    duePosition,
// 						    "",
// 						    "",
// 						    "Balance Due",
// 						    "",
// 						    formatCurrency(invoice.subtotal - invoice.paid)
// 						  );
// 						  doc.font("Helvetica");
// 						}

// 						function generateFooter(doc) {
// 						  doc
// 						    .fontSize(10)
// 						    .text(
// 						      "Payment is due within 15 days. Thank you for your business.",
// 						      50,
// 						      780,
// 						      { align: "center", width: 500 }
// 						    );
// 						}

// 						function generateTableRow(
// 						  doc,
// 						  y,
// 						  date,
// 						  name,
// 						  payment_type,
// 						  payment_method,
// 						  amount,
// 						  addpage
// 						) {
// 						  doc
// 						    .fontSize(10)
// 						    .text(date, 50, y)
// 						    .text(name, 150, y)
// 						    .text(payment_type, 280, y, { width: 90, align: "right" })
// 						    .text(payment_method, 370, y, { width: 90, align: "right" })
// 						    .text(amount, 0, y, { align: "right" })
// 						}

// 						function generateHr(doc, y) {
// 						  doc
// 						    .strokeColor("#aaaaaa")
// 						    .lineWidth(1)
// 						    .moveTo(50, y)
// 						    .lineTo(550, y)
// 						    .stroke();
// 						}

// 						function formatCurrency(cents) {
// 						  return "$" + (cents / 100).toFixed(2);
// 						}

// 						function formatDate(date) {
// 						  const day = date.getDate();
// 						  const month = date.getMonth() + 1;
// 						  const year = date.getFullYear();

// 						  return year + "/" + month + "/" + day;
// 						}



		


						jsonRes = {
							success: '1',
							result: {
								jazepay_generate_reports : output
							},
							successMessage: 'Jazepay generate reports',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{
						jsonRes = {
							success: '1',
							result: {
								jazepay_generate_reports : [],
							},
							successMessage: 'No result found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
				}
				else 
				{

					jsonRes = {
						success: '1',
						result: {
							generate_reports : []
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/authentication_credit_card',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('id','Invalid id').isLength({ min: 1 }),
	check('csv','Invalid csv').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		id         : req.body.id,
		csv	       : req.body.csv
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazepay.authenticationCreditCard(arrParams, function(retSec){

				if(retSec!=null)
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Authenticated.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}
				else
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Invalid Card details. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



//------------------------------------------------------------------- receivable quote reject request quotation ---------------------------------------------------------------------------//


router.post('/jazepay/receivable_quotation_request_quote_reject',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('request_account_id','Invalid request_account_id').isNumeric(),
	check('id','Invalid id').isNumeric(),
	check('discount','Invalid discount').isNumeric(),
	check('flag','Invalid flag').isNumeric(),


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		request_account_id   : req.body.request_account_id,
		id                   : req.body.id,
		discount             : req.body.discount,
		product_values       : req.body.product_values,
		flag                 : req.body.flag

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

	
			HelperJazepay.quotationReceivedQuoteReject(arrParams, function(retResult){	

				if(parseInt(retResult) === 1)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Quotation request is successfully send.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else if(parseInt(retResult) === 3)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Quotation request is successfully rejected.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else if(parseInt(retResult) === 2)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'No result found',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
				
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Unable to process request. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});

//------------------------------------------------------------------- receivable quote reject request quotation ---------------------------------------------------------------------------//



router.post('/jazepay/receivable_order_received_accept_reject',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('request_account_id','Invalid request_account_id').isNumeric(),
	check('id','Invalid id').isNumeric(),
	check('flag','Invalid flag').isNumeric(),


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		request_account_id   : req.body.request_account_id,
		id                   : req.body.id,
		flag                 : req.body.flag

	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

	
			HelperJazepay.receivableorderReceivedAcceptReject(arrParams, function(retResult){	

				if(parseInt(retResult) === 1)
				{	
					var sucessReturn = "Order request is successfully accepted."
					if(parseInt(arrParams.flag) === 2)
					{
						sucessReturn = "Order request is successfully rejected."
					}
					jsonRes = {
						success: '1',
						result: {},
						successMessage: sucessReturn,
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else if(parseInt(retResult) === 2)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'No result found',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
				
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Unable to process request. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazepay/receivable_generate_invoice',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('request_account_id','Invalid request_account_id').isNumeric(),
	check('id','Invalid id').isNumeric(),
	check('discount','Invalid discount').isNumeric(),
	check('flag','Invalid flag').isNumeric(),


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		request_account_id   : req.body.request_account_id,
		discount             : req.body.discount,
		type                 : req.body.type,
		reference            : req.body.reference,
		product_values       : req.body.product_values,
		id                   : req.body.id,
		flag                 : req.body.flag

	};

	// 1 = manual invoice
	// 2 = from request invoice

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

	
			HelperJazepay.receivableGenerateInvoice(arrParams, function(retResult){	

				if(parseInt(retResult) === 1)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Quotation request is successfully send.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else if(parseInt(retResult) === 2)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'No result found',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
				
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Unable to process request. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazepay/receivable_generate_creditnotes',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('account_id','Invalid account_id').isNumeric(),
	check('request_account_id','Invalid request_account_id').isNumeric(),
	check('invoice_id','Invalid invoice_id').isNumeric(),
	check('discount','Invalid discount').isNumeric(),
	check('flag','Invalid flag').isNumeric(),


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		request_account_id   : req.body.request_account_id,
		discount             : req.body.discount,
		type                 : req.body.type,
		reference            : req.body.reference,
		product_values       : req.body.product_values,
		invoice_id           : req.body.invoice_id,
		flag                 : req.body.flag

	};

	// 1 = manual generate
	// 2 = from request invoice

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

	
			HelperJazepay.receivableGenerateCreditNotes(arrParams, function(retResult){	

				if(parseInt(retResult) === 1)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Credit notes is successfully send.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else if(parseInt(retResult) === 2)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'No result found',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
				
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Unable to process request. Please try again.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});





router.post('/jazepay/testing',[

	check('api_token','Invalid api_token').isLength({ min: 1 }),
	check('tran_no','Invalid tran_no').isLength({ min: 1 }),
	check('id','Invalid id').isNumeric(),
	check('trans_type','Invalid trans_type').isNumeric(),
	check('account_id','Invalid account_id').isNumeric(),
	check('request_account_id','Invalid request_account_id').isNumeric(),
	check('flag','Invalid flag').isNumeric(),


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		request_account_id : req.body.request_account_id,
		id         : req.body.id,
		tran_no    : req.body.tran_no,
		trans_type : req.body.trans_type,
		flag       : req.body.flag,
	};


	// trans_type: 
	// 1 = quotation request 
	// 2 = quotation
	// 3 = order
	// 4 = invoice
	// 5 = payments
	// 6 = credit
	// 7 = receipt
	// 8 = cash purchase


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{


			var senParam = {account_id:arrParams.account_id};
			HelperGeneralJazenet.getAccountDetails(senParam, function(retSender){

				var recParam = {account_id:arrParams.request_account_id};
				HelperGeneralJazenet.getAccountDetails(recParam, function(retReceiver){	

					const pdfpath = './uploads/pdf/';
					const puppeteer = require("puppeteer");
					const user = 'jazenet';
					const pass = 'pv85@fn';
					const btoa = require('btoa');

					var file_s = '';
					var parameter = '';

					if(parseInt(arrParams.trans_type) === 1){
						file_s = 'QR-';
						parameter = 'quotation-request';
					}else if(parseInt(arrParams.trans_type) === 2){
						file_s = 'Q-';
						parameter = 'quotation';
					}else if(parseInt(arrParams.trans_type) === 3){
						file_s = 'O-';
						parameter = 'order';
					}else if(parseInt(arrParams.trans_type) === 4){
						file_s = 'I-';
						parameter = 'invoice';
					}else if(parseInt(arrParams.trans_type) === 5){
						file_s = 'P-';
						parameter = 'payment';
					}else if(parseInt(arrParams.trans_type) === 6){
						file_s = 'CN-';
						parameter = 'credit';
					}else if(parseInt(arrParams.trans_type) === 7){
						file_s = 'R-';
						parameter = 'receipt';
					}else if(parseInt(arrParams.trans_type) === 8){
						file_s = 'CS-';
						parameter = 'cash';
					}
				
				
					var file_name = file_s+arrParams.tran_no;

					async function timeout(ms) {
				  		return new Promise(resolve => setTimeout(resolve, ms));
					}

					var convert = btoa(arrParams.id);
					var get_url = 'https://www.staging.jazenet.com/jazeaccounting/generate_mobile_pdf/'+convert+'/'+parameter;

					(async () => {
					 
					    let browser = await puppeteer.launch();
					    let page = await browser.newPage();
					    await page.authenticate({'username':user, 'password':pass});
					    await page.goto(get_url, {waitUntil: 'networkidle2' });
			         	await timeout(1500);
		   				await page.pdf({path: pdfpath+file_name+'.pdf', printBackground: true, width: '1024px' , height: '768px'});
					    await page.emulateMedia('screen');
						await browser.close();

						setTimeout(function(){

							HelperEncrypt.fileCopyEncrypt(file_name+'.pdf', function(result)
							{
								if(result !== null && parseInt(result.length) > 0)
								{	

									var attachment_obj = {
										file_name : result[0].file_name,
										iv        : result[0].iv
									};

									attachment_arr.push(attachment_obj);

									var send_mail_param = {
										account_id    : arrParams.account_id,
										to_account_id : arrParams.request_account_id,
										subject       : 'attach mail',
										attachment    : [attachment_obj],
										reply_id      : 0,
										mail_id       : 0,
										flag          : 1,
										return_folder_id : 0

									};


									HelperJazeEMail.saveSendEmail(send_mail_param, function(retGroupId){

										if(parseInt(retGroupId) != 0){

											if(parseInt(send_mail_param.folder) != 0)
											{					
												HelperJazeEMail.getMailList(send_mail_param, function(mail_details){				

													HelperJazeEMail.getAllEmailCount(send_mail_param.account_id, null, function (mail_count) {		
									
														var email_arr = [];	
									
														mail_details.message_count = mail_count;
									
														email_arr.push(mail_details);
																	
														jsonRes = {
															success: '1',
															result : {email_object : email_arr},
															successMessage: 'Email Sent Successfully...',
															errorMessage: errorMessage
														};
											
														res.status(201).json(jsonRes);
														res.end();
													});
												});
											}
											else
											{
												jsonRes = {
													success: '1',
													result : {},
													successMessage: 'Email Sent Successfully...',
													errorMessage: errorMessage
												};
									
												res.status(201).json(jsonRes);
												res.end();
											}
										}
										else{

											jsonRes = {
												success: success,
												result: {},
												successMessage: successMessage,
												errorMessage: "Unable to process request. Please try again." 
											};
								
											res.status(201).json(jsonRes);
											res.end();
										}
									});
								}
								else
								{
					
									jsonRes = {
										success: success,
										result: [],
										successMessage: successMessage,
										errorMessage: "Unable to process request. Please try again." 
									};
					
									res.status(201).json(jsonRes);
									res.end();
								}
							});

						}, 2000);
						
					})();

				});

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

}); 	



module.exports = router;