require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperJobs = require('../helpers/HelperJobs.js');
const HelperProfessional = require('../helpers/HelperProfessional.js');
const HelperDocuments = require('../helpers/HelperDocuments.js');


const { check, validationResult } = require('express-validator');

const mime = require('mime-types');
const multer = require('multer');
const fs = require('fs');

const STATIC_IMG_URL = G_STATIC_IMG_URL+"uploads/jazenet/";

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};
var stat;

var statRes = 422;
var jsonRes = {};



router.post('/jazejobs/jobs',[

	check('category_id').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {category_id : req.body.category_id};


	HelperJobs.getAttributeParentJobs(arrParams, function(retChilds){


		if(retChilds!=null)
		{	

			HelperJobs.getCategoryAttributesJobs(arrParams, function(retCategory){

				if(retCategory !== null){
					success = '1';
					successMessage = 'Jazenet Jobs';
					errorMessage = '';
					stat = 201;
					results = {
						portal_category_titles:retChilds,
						controllers:retCategory
					};

				}else{
					success = '0';
					successMessage = 'No result found.';
					errorMessage = '';
					stat = statRes;
					results = {};
				}

				jsonRes = {
					success: success,
					result: results,
					successMessage: successMessage,
					errorMessage: "" 
		 		};
				res.status(stat).json(jsonRes);
				res.end();

			});

		}else{

			jsonRes = {
				success: success,
				result: {},
				successMessage: 'No result found.',
				errorMessage: "" 
	 		};
			res.status(statRes).json(jsonRes);
			res.end();
		}

	});


});

router.post('/jazejobs/search_people',[

	check('category_id').isNumeric(),
	check('page').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		category_id : req.body.category_id,
		country_id  : req.body.country_id,
		state_id    : req.body.state_id,
		city_id     : req.body.city_id,
		page        : req.body.page,
		lon         : req.body.longitude,
		lat         : req.body.latitude,
		attributes  : req.body.attributes
	};

	var pagination_details_empty  = {
		total_page   : '0',
    	current_page : '0',
    	next_page    : '0',
    	total_result : '0'
    };

	HelperJobs.searchJobs(arrParams, function(retCompany,pagination_details){

		if(retCompany!=null){

			jsonRes = {

				success: '1',
				result: {
					pagination_details:pagination_details,
					searched_job_company :retCompany
				},
				successMessage: 'Searched result.',
				errorMessage: "" 
			};

			res.status(201).json(jsonRes);
			res.end();

		}else{

			jsonRes = {
				success: '1',
				result: {
					pagination_details:pagination_details_empty,
					searched_job_company :[]
				},
				successMessage:"",
				errorMessage: "No result found." 
	 		};
			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazejobs/people_details',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('account_group_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		group_id     : req.body.account_group_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJobs.getPeopleDetails(arrParams, function(retDetails){
			
				if(retDetails!=null)
				{	
					jsonRes = {
						success: '1',
						result: {
							profile_details:retDetails
						},
						successMessage: 'Profile Details.',
						errorMessage: "" 
						};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
			 		};
					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazejobs/department_list',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		department_name   : req.body.department_name,
		department_id     : req.body.department_id,
		flag         : req.body.flag,
	};


	// 0 = list
	// 1 = add
	// 2 = edit
	// 3 = delete

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			var arrFlags = [1,2,3];

			if(arrFlags.includes(parseInt(arrParams.flag)))
			{
				HelperJobs.shortilistGroups(arrParams, function(retProcess){
			
					if(retProcess == 1)
					{	
						setTimeout(function(){ 

							HelperJobs.getShortListGroups(arrParams, function(retGroups){

								jsonRes = {
									success: '1',
									result: {
										department_list:retGroups
									},
									successMessage: 'New group is successfully created.',
									errorMessage: "" 
									};

								res.status(201).json(jsonRes);
								res.end();

							});

					 	}, 300);

					}
					else if(retProcess == 2)
					{

						setTimeout(function(){ 

							HelperJobs.getShortListGroups(arrParams, function(retGroups){

								jsonRes = {
									success: '1',
									result: {
										department_list:retGroups
									},
									successMessage: 'Group is successfully updated.',
									errorMessage: "" 
									};

								res.status(201).json(jsonRes);
								res.end();
								
							});

					 	}, 300);

					}
					else if(retProcess == 3)
					{
						setTimeout(function(){ 

							HelperJobs.getShortListGroups(arrParams, function(retGroups){

								jsonRes = {
									success: '1',
									result: {
										department_list:retGroups
									},
									successMessage: 'Group is successfully deleted.',
									errorMessage: "" 
									};

								res.status(201).json(jsonRes);
								res.end();
								
							});

					 	}, 300);

					}
					else if(retProcess == 4)
					{
						jsonRes = {
							success: '0',
							result: {},
							successMessage: '',
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: '0',
							result: {},
							successMessage: '',
							errorMessage: "Unable to process this request. Please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}

				});
			}
			else if(parseInt(arrParams.flag) == 0)
			{

				HelperJobs.getShortListGroups(arrParams, function(retGroups){

					if(retGroups!=null){

						jsonRes = {
							success: '1',
							result: {
								department_list:retGroups
							},
							successMessage: 'Department List',
							errorMessage: "" 
							};

						res.status(201).json(jsonRes);
						res.end();

					}else{

						jsonRes = {
							success: '0',
							result: {},
							successMessage: '',
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else
			{

				jsonRes = {
					success: '0',
					result: {},
					successMessage: '',
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
		
			}
		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazejobs/add_shortlist',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('department_id').isLength({ min: 1 }),
	check('account_group_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

  	var department_id = req.body.department_id.split(",");

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		department_id : department_id,
		account_group_id   : req.body.account_group_id,
		flag			   : req.body.flag,
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			if(parseInt(arrParams.flag) === 1)
			{
				HelperJobs.addToShortListGroup(arrParams, function(retDetails){
			
					if(retDetails!=null)
					{	
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Successfully Added.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
	
					}else{
	
						jsonRes = {
							success: success,
							result: {},
							successMessage: 'No result found.',
							errorMessage: "" 
						 };
						res.status(statRes).json(jsonRes);
						res.end();
					}
	
				});
			}
			else if(parseInt(arrParams.flag) === 3)
			{
				HelperJobs.removeToShortListGroup(arrParams, function(retDetails){
			
					if(parseInt(retDetails) !== 0)
					{	
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Successfully Removed.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
	
					}else{
	
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "some error occurred, please try again." 
						};
						res.status(statRes).json(jsonRes);
						res.end();
					}
	
				});
			}
			
		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazejobs/request_cv',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('department_id').isLength({ min: 1 }),
	check('account_group_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

  	var department_id = req.body.department_id.split(",");

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		department_id : department_id,
		account_group_id   : req.body.account_group_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJobs.addToRequestCv(arrParams, function(retDetails){
		
				if(retDetails==1)
				{	
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Request cv sent.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else if(retDetails==2){


					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Awaiting cofirmation. CV is already requested.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();


				}else if(retDetails==3){

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'cv is already received.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else if(retDetails==0){

					jsonRes = {
						success: success,
						result: {},
						successMessage: 'Unable to process this request. Please try again.',
						errorMessage: "" 
			 		};
					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazejobs/accept_reject_request_cv',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('id').isNumeric(),
	check('group_id').isNumeric(),
	check('status').isNumeric(),


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		id     		 : req.body.id,
		group_id     : req.body.group_id,
		status       : req.body.status
	};

	// 1 accept 
	// 2 reject

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperJobs.setAcceptRejectRequestCv(arrParams, function(retDetails){

				if(parseInt(retDetails) === 1)
				{

					if(parseInt(arrParams.status) === 1 )
					{ successMessage = 'CV request is accepted.'; }
					else 
					{ successMessage = 'CV request is rejected.'; }

					jsonRes = {
						success: '1',
						result: {},
						successMessage: successMessage,
						errorMessage: "" 
					 };
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: 'Unable to process this request. Please try again.',
						errorMessage: "" 
					 };
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



// ---------------------------------------------- Job Manager -------------------------------------------------------- //

router.post('/job_manager/department_list',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJobs.getShortListGroupsDepartment(arrParams, function(retDetails){
		
				jsonRes = {
					success: '1',
					result: {
						department_list:retDetails
					},
					successMessage: 'Job Manger Department List',
					errorMessage: "" 
				};
				res.status(201).json(jsonRes);
				res.end();

			});
		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/job_manager/people_list',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('department_id').isNumeric(),


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		department_id : req.body.department_id,

	};



	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperJobs.getShortListGroupsPeople(arrParams, function(retDetails){

				if(retDetails!=null)
				{
					jsonRes = {
						success: '1',
						result: {
							people_list:retDetails
						},
						successMessage: 'Job Manger People List',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}
				else
				{
					jsonRes = {
						success: '1',
						result: {
							people_list:[]
						},
						successMessage: "",
						errorMessage: "No result found." 
					};
					res.status(201).json(jsonRes);
					res.end();
				}

			});	

		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});

router.post('/job_manager/shortlist_status',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('account_group_id').isNumeric(),
	check('department_id').isNumeric(),
	check('status').isNumeric(),


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		account_group_id : req.body.account_group_id,
		department_id    : req.body.department_id,
		status           : req.body.status
	};

	// status: 
	// 0 = rejected
	// 1 = active
	// 2 = maybe

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			var arrFlags = [0,1,2];
			if(arrFlags.includes(parseInt(arrParams.status)))
			{
				HelperJobs.updateShortListStatus(arrParams, function(retStatus){

					if(parseInt(retStatus) == 1)
					{
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Shortlist status is successfully updated.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else if(parseInt(retStatus) == 2)
					{
						jsonRes = {
							success: '1',
							result: {},
							successMessage: "",
							errorMessage: "No result found." 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: '0',
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process request. Please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}

				});	

			}
			else
			{
				jsonRes = {
					success: '0',
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid status provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



// ----- job vacancy search  -- //


router.post('/jazejobs/search_jobs',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('category_id').isNumeric(),
	check('page').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		category_id : req.body.category_id,
		country_id  : req.body.country_id,
		state_id    : req.body.state_id,
		city_id     : req.body.city_id,
		page        : req.body.page,
		lon         : req.body.longitude,
		lat         : req.body.latitude,
		attributes  : req.body.attributes
	};

	var pagination_details_empty  = {
		total_page   : '0',
    	current_page : '0',
    	next_page    : '0',
    	total_result : '0'
    };

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		
		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJobs.searchEmployer(arrParams, function(retCompany,pagination_details){

				if(retCompany!=null){

					jsonRes = {

						success: '1',
						result: {
							pagination_details:pagination_details,
							searched_job_details:retCompany
						},
						successMessage: 'Searched result.',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();

				}else{
					jsonRes = {
						success: success,
						result: {
							pagination_details:pagination_details_empty,
							searched_job_details:[]
						},
						successMessage:"",
						errorMessage: "No result found." 
			 		};
					res.status(statRes).json(jsonRes);
					res.end();
				}

			});


		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


// ----- job vacancy search  -- //


// ----- job vacancy details  -- //

router.post('/jazejobs/job_details',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('group_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		group_id     : req.body.group_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJobs.getEmployerJobDetails(arrParams, function(retDetails){
			
				if(retDetails!=null)
				{	
					jsonRes = {
						success: '1',
						result: {
							employment_details:retDetails
						},
						successMessage: 'Employment Details.',
						errorMessage: "" 
						};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
			 		};
					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});

// ----- job vacancy details  -- //




// ----- job vacancy questions  -- //

router.post('/jazejobs/job_questions',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('group_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		group_id     : req.body.group_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJobs.getEmployerQuestions(arrParams, function(retDetails){
			
				if(retDetails!=null)
				{	
					jsonRes = {
						success: '1',
						result: {
							employment_questions:retDetails
						},
						successMessage: 'Employment Questions.',
						errorMessage: "" 
						};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
			 		};
					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});

// ----- job vacancy questions  -- //


router.post('/jazejobs/apply_jobs',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('group_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		group_id     : req.body.group_id,
		questions    : req.body.questions
	};


	if(parseInt(arrParams.account_id) !== 0 ){

		HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

			if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
			{

				HelperJobs.applyForJobVacancy(arrParams, function(retDetails){
				
					if(parseInt(retDetails) === 1)
					{	
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Application is successfully submited.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else if(parseInt(retDetails) === 2)
					{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'This account does not have job profile. Please create a job profile first.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else if(parseInt(retDetails) === 3)
					{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'This account is already submited to this job vacancy.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else if(parseInt(retDetails) === 0)
					{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Unable to process request please try again.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				});
			}
			else
			{

				jsonRes = {
					success: '0',
					result: {},
					successMessage: successMessage,
					errorMessage: "Device not found." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		});

	}else{

		jsonRes = {
			success: '0',
			result: {},
			successMessage: successMessage,
			errorMessage: "Only registred user can process this request." 
		};

		res.status(statRes).json(jsonRes);
		res.end();

	}
	
});



router.post('/jazejobs/cancel_apply_jobs',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('group_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		group_id     : req.body.group_id
	};


	if(parseInt(arrParams.account_id) !== 0 ){

		HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

			if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
			{

				HelperJobs.cancelApplyForJobVacancy(arrParams, function(retDetails){
				
					if(parseInt(retDetails) === 1)
					{	
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Application is successfully withdraw.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else if(parseInt(retDetails) === 3)
					{

						jsonRes = {
							success: '0',
							result: {},
							successMessage: 'No result found',
							errorMessage: "" 
						};
						res.status(statRes).json(jsonRes);
						res.end();
					}
					else if(parseInt(retDetails) === 0)
					{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Unable to process request please try again.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				});
			}
			else
			{

				jsonRes = {
					success: '0',
					result: {},
					successMessage: successMessage,
					errorMessage: "Device not found." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		});

	}else{

		jsonRes = {
			success: '0',
			result: {},
			successMessage: successMessage,
			errorMessage: "Only registred user can process this request." 
		};

		res.status(statRes).json(jsonRes);
		res.end();

	}
	

});


router.post('/jazejobs/view_cv',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('request_account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		request_account_id : req.body.request_account_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJobs.viewAccountCv(arrParams, function(retDetails){
			
				if(retDetails!=null)
				{	
					jsonRes = {
						success: '1',
						result: {
							cv_details:retDetails
						},
						successMessage: 'cv details',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
			 		};
					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


var storage = multer.diskStorage({
	destination: function (req, file, callback) {
	
	function asyncFunction (item, cb) {
	  	setTimeout(() => {

			
			var out_path = G_STATIC_IMG_PATH+'individual_account/jobs/'+req.body.account_id+'/';

			var main_path = out_path;

			if (!fs.existsSync(out_path)){
				fs.mkdirSync(out_path,'0757', true); 
			}


			callback(null, main_path);

			cb();
	  	}, 100);
	}

	let requests = 0;

		if(req.body.account_id !=''){

			return new Promise((resolve) => {
				requests = req.body.account_id;
		  		asyncFunction(requests, resolve);
		    });

			Promise.all(requests);
		}
  },
  	filename: function (req, file, callback) {

  		var ext = getExtension(file.originalname);
  		var cleanName = getCleanFileName(file.originalname);
  		var completeFileName = req.body.account_id+Date.now()+'.'+ext;

    	callback(null, completeFileName);
  	}
});


router.post('/jazejobs/cv_builder_upload_profile_photo', function(req, res) {

	var upload = multer({
		storage: storage,
	}).single('file');

	upload(req, res, function(err) {

 	 	var extName = req.file.filename.split(".");
 	 	var filename = extName[extName.length - 2];

	 	var extArray = req.file.filename.split(".");
 	 	var extention = extArray[extArray.length - 1];

 	  	var file_properties = req.file.mimetype.split("/");

 		var base_url = '';
        var filetype = '';

 		var complete_filename = filename+'.'+extention;

 		base_url = STATIC_IMG_URL+'/individual_account/jobs/'+req.body.account_id+'/'+complete_filename;

 		filetype = file_properties[0]; 

 		var arrParams = {account_id:req.body.account_id, file_name:req.file.filename};
 		
    	HelperProfessional.getProfilePicture(arrParams,function(retStatus){

	        HelperDocuments.getFileTypes('.'+extention, function(reType){

	            var results = {
	                file_name:filename,
	                file_format:filetype,
	                file_type:reType.toString(),
	                extention:extention,
	                file_path:base_url,
	            };
	           
	            jsonRes = {
	                success: '1',
	                result: results,
	                successMessage: 'File is successfully uploaded.',
	                errorMessage: errorMessage 
	            };
	            res.status(201).json(jsonRes);
	            res.end();
	        });

        });

	});
});

function getExtension(path) {
    var basename = path.split(/[\\/]/).pop(),  // extract file name from full path ...
                                             	 // (supports `\\` and `/` separators)
        pos = basename.lastIndexOf(".");       // get last position of `.`
    if (basename === "" || pos < 1)            // if file name is empty or ...
        return "";                             //  `.` not found (-1) or comes first (0)
    return basename.slice(pos + 1);            // extract extension ignoring `.`
}

function getFilename(path) {
    var basename = path.split(/[\\/]/).pop(),      
        pos = basename.split('.'); 
    if (basename === "" || pos.length < 1)           
        return "";                          
    return pos[0];            
}


function getCleanFileName(path) {

	var basename = path.split(/\.(?=[^\.]+$)/);
	var cleanString = basename[0].replace(/[|&;$%@"<>()+1234567890,]/g, "");
         
    return cleanString;           
}


router.post('/jazejobs/cv_builder_basic_details',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('first_name').isLength({ min: 1 }),
	check('last_name').isLength({ min: 1 }),
	check('dob').isLength({ min: 1 }),
	check('gender').isLength({ min: 1 }),
	check('nationality_id').isNumeric(),
	check('mobile_code_id').isNumeric(),
	check('mobile_number').isLength({ min: 1 }),
	check('mobile_code_id_2').isLength({ min: 1 }),
	check('mobile_number_2').isLength({ min: 1 }),
	check('country_id').isNumeric(),
	check('city_id').isNumeric(),
	check('flag').isNumeric()



	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token        : req.body.api_token,
		account_id       : req.body.account_id,
		first_name       : req.body.first_name,
		last_name        : req.body.last_name,
		file_name        : req.body.profile_logo_file_name,
		dob              : req.body.dob,
		gender           : req.body.gender,
		nationality_id   : req.body.nationality_id,
		mobile_code_id   : req.body.mobile_code_id,
		mobile_number    : req.body.mobile_number,
		mobile_code_id_2 : req.body.mobile_code_id_2,
		mobile_number_2  : req.body.mobile_number_2,
		country_id       : req.body.country_id,
		city_id          : req.body.city_id,
		flag             : req.body.flag
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			if(parseInt(arrParams.flag) === 1)
			{

				HelperJobs.addCvBasicDetails(arrParams, function(retDetails){
				
					if(parseInt(retDetails)===1)
					{	
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Basic details is successfully saved.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: 'Unable to process this request. Please try again.',
							errorMessage: "" 
				 		};
						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else if(parseInt(arrParams.flag) === 2)
			{

				HelperJobs.addCvBasicDetails(arrParams, function(retDetails){

					if(parseInt(retDetails)===1)
					{	
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Basic details is successfully updated.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else if(parseInt(retDetails)===2)
					{	

						jsonRes = {
							success: success,
							result: {},
							successMessage: 'No result found.',
							errorMessage: "" 
				 		};
						res.status(statRes).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: 'Unable to process this request. Please try again.',
							errorMessage: "" 
				 		};
						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else
			{

				jsonRes = {
					success: '0',
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();

			}

		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazejobs/cv_builder_qualifications',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('institute_name').isLength({ min: 1 }),
	check('diploma').isLength({ min: 1 }),
	check('field_of_study').isLength({ min: 1 }),
	check('from_date').isLength({ min: 1 }),
	check('to_date').isLength({ min: 1 }),
	check('country_id').isNumeric(),
	check('city_id').isNumeric(),
	check('flag').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token        : req.body.api_token,
		account_id       : req.body.account_id,
		institute_name   : req.body.institute_name,
		title            : req.body.diploma,
		subject          : req.body.field_of_study,
		from_date        : req.body.from_date,
		to_date          : req.body.to_date,
		city_id          : req.body.city_id,
		country_id       : req.body.country_id,
		flag             : req.body.flag,
		id               : req.body.id
	};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			if(parseInt(arrParams.flag) === 0)
			{

				HelperJobs.addCvQualificationDetails(arrParams, function(retDetails){
				
					if(parseInt(retDetails) !==2 || parseInt(retDetails) !==0)
					{	
						jsonRes = {
							success: '1',
							result: {
								qualification_list:retDetails
							},
							successMessage: 'Cv qualification list.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: 'No result found',
							errorMessage: "" 
				 		};
						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else if(parseInt(arrParams.flag) === 1)
			{

				HelperJobs.addCvQualificationDetails(arrParams, function(retDetails){
				
					if(parseInt(retDetails)===1)
					{	
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Basic details is successfully saved.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: 'Unable to process this request. Please try again.',
							errorMessage: "" 
				 		};
						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else if(parseInt(arrParams.flag) === 2)
			{

				HelperJobs.addCvQualificationDetails(arrParams, function(retDetails){
				
					if(parseInt(retDetails)===1)
					{	
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Qualification details is successfully updated.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else if(parseInt(retDetails)===2)
					{	

						jsonRes = {
							success: success,
							result: {},
							successMessage: 'No result found.',
							errorMessage: "" 
				 		};
						res.status(statRes).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: 'Unable to process this request. Please try again.',
							errorMessage: "" 
				 		};
						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else
			{

				jsonRes = {
					success: '0',
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();

			}

		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazejobs/cv_builder_work_experience',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_name').isLength({ min: 1 }),
	check('job_title').isLength({ min: 1 }),
	check('description').isLength({ min: 1 }),
	check('from_date').isLength({ min: 1 }),

	check('country_id').isNumeric(),
	check('city_id').isNumeric(),
	check('flag').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token        : req.body.api_token,
		account_id       : req.body.account_id,
		company_name     : req.body.company_name,
		job_title        : req.body.job_title,
		description      : req.body.description,
		from_date        : req.body.from_date,
		to_date          : req.body.to_date,
		city_id          : req.body.city_id,
		country_id       : req.body.country_id,
		flag             : req.body.flag,
		id               : req.body.id
	};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			if(parseInt(arrParams.flag) === 0)
			{

				HelperJobs.addCVWorkExperience(arrParams, function(retDetails){
				
					if(parseInt(retDetails) !== 2 && parseInt(retDetails) !== 0)
					{	
						jsonRes = {
							success: '1',
							result: {
								work_experience:retDetails
							},
							successMessage: 'Cv qualification list.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: 'No result found',
							errorMessage: "" 
				 		};
						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else if(parseInt(arrParams.flag) === 1)
			{

				HelperJobs.addCVWorkExperience(arrParams, function(retDetails){

					if(parseInt(retDetails)===1)
					{	
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Work experience is successfully saved.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: 'Unable to process this request. Please try again.',
							errorMessage: "" 
				 		};
						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else if(parseInt(arrParams.flag) === 2)
			{

				HelperJobs.addCVWorkExperience(arrParams, function(retDetails){
				
					if(parseInt(retDetails)===1)
					{	
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Work experience is successfully updated.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else if(parseInt(retDetails)===2)
					{	

						jsonRes = {
							success: success,
							result: {},
							successMessage: 'No result found.',
							errorMessage: "" 
				 		};
						res.status(statRes).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: 'Unable to process this request. Please try again.',
							errorMessage: "" 
				 		};
						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else
			{

				jsonRes = {
					success: '0',
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();

			}

		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazejobs/cv_builder_about_me',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('about').isLength({ min: 1 }),
	check('skills').isLength({ min: 1 }),
	check('languages').isLength({ min: 1 }),
	check('privacy').isNumeric(),
	check('is_relocate').isNumeric(),
	check('flag').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		about        : req.body.about,
		skills       : req.body.skills,
		languages    : req.body.languages,
		privacy      : req.body.privacy,
		is_relocate  : req.body.is_relocate,
		id           : req.body.id,
		flag         : req.body.flag
	};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			if(parseInt(arrParams.flag) === 0)
			{

				HelperJobs.addCvAboutDetails(arrParams, function(retDetails){
				
					if(parseInt(retDetails) !==2 && parseInt(retDetails) !==0)
					{	
						jsonRes = {
							success: '1',
							result: {
								about_details:retDetails
							},
							successMessage: 'Cv about details',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: 'No result found',
							errorMessage: "" 
				 		};
						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else if(parseInt(arrParams.flag) === 1)
			{

				HelperJobs.addCvAboutDetails(arrParams, function(retDetails){

					if(parseInt(retDetails)===1)
					{	
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'cv about details is successfully saved.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: 'Unable to process this request. Please try again.',
							errorMessage: "" 
				 		};
						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else if(parseInt(arrParams.flag) === 2)
			{

				HelperJobs.addCvAboutDetails(arrParams, function(retDetails){
				
					if(parseInt(retDetails)===1)
					{	
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'cv about details is successfully updated.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else if(parseInt(retDetails)===2)
					{	

						jsonRes = {
							success: success,
							result: {},
							successMessage: 'No result found.',
							errorMessage: "" 
				 		};
						res.status(statRes).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: 'Unable to process this request. Please try again.',
							errorMessage: "" 
				 		};
						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else
			{

				jsonRes = {
					success: '0',
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();

			}

		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazejobs/remove_cv',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id
	};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJobs.removeAccountCv(arrParams, function(retDetails){



				if(parseInt(retDetails)===1)
				{	
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'cv account details is success removed.',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else if(parseInt(retDetails)===2){

					jsonRes = {
						success: success,
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
			 		};
					res.status(statRes).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: 'Unable to process request. Please try again.',
						errorMessage: "" 
			 		};
					res.status(statRes).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{

			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});

module.exports = router;