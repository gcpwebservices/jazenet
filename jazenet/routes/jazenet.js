require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperJazenet = require('../helpers/HelperJazenet.js');

const { check, validationResult } = require('express-validator');
const crypto = require('crypto');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};



//------------------------------------------------------------------- Login & Logout Module Start ---------------------------------------------------------------------------//

router.post('/login', [

	check('api_token').isLength({ min: 1 }),
	check('app_id').isNumeric({ min: 1 }),
	check('jazemail_id').isLength({ min: 1 }),
	check('password').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	    var reqParams = {		
			account_id		: 0,
			account_type	: 1,
			api_token		: req.body.api_token,
			app_id		    : req.body.app_id,
			jazemail_id		: req.body.jazemail_id,
			password		: crypto.createHash('md5').update(req.body.password).digest("hex")
		};

		HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

			if (typeof queParamRes !== 'undefined' && queParamRes !== null && parseInt(queParamRes.length) !== 0)
        	{
				// check Credential Details valid or not
				HelperJazenet.checkCredentialDetails(reqParams, function(account_id){

					if(parseInt(account_id) !== 0)
					{
						//update device
						Object.assign(reqParams, {account_id: account_id});
						
						HelperJazenet.updateAppDeviceDetails(1,reqParams, function(return_device){	
												
							if(return_device !== null)
							{
								Object.assign(reqParams, {api_token: return_device.device_details.api_token});

						 		//get & Save Link account Details
								var reqParams_account = {		
									api_token  		  : reqParams.api_token,
									master_account_id : reqParams.account_id,
									account_id 		  : reqParams.account_id,
									account_type 	  : reqParams.account_type,
									app_id		      : reqParams.app_id,
								};

								HelperJazenet.getMasterChildLinkAccountList(reqParams_account, function(account_list){	

									if(account_list !== null)
									{
										Object.assign(return_device, account_list);

										jsonRes = {
											success: '1',
											result: return_device,
											successMessage: 'successfully login Jazenet',
											errorCode: "0", 
											errorMessage: errorMessage 
										};
								
										res.status(201).json(jsonRes);
										res.end();
									}
									else
									{
										jsonRes = {
											success: success,
											result: {},
											successMessage: successMessage,
											errorCode: "0", 
											errorMessage: "Unable to login. Please try again." 
										};
				
										res.status(201).json(jsonRes);
										res.end()
									}
								});
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorCode: "0", 
									errorMessage: "Unable to login. Please try again." 
								};
		
								res.status(201).json(jsonRes);
								res.end()
							}
						});		
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorCode: "0", 
							errorMessage: "Invalid account details. Please check Email and Password." 
						};

						res.status(201).json(jsonRes);
						res.end();

					}
				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorCode: "100", 
					errorMessage: "Device not found."
					
				};
				res.status(201).json(jsonRes);
				res.end();
			}
		});
});


router.post('/logout', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isLength({ min: 1 }),
	check('app_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		app_id	   : req.body.app_id
	};


	HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

		if (typeof queParamRes !== 'undefined' && queParamRes !== null && parseInt(queParamRes.length) !== 0)
		{
			HelperJazenet.removeAppDeviceDetails(reqParams, function(account_list){	

				if(account_list !== null)
				{
					jsonRes = {
						success: '1',
						result: account_list,
						successMessage: 'You have been successfully logged out.',
						errorCode: "0", 
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorCode: "0", 
						errorMessage: "Unable to log out. Device not found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found."
			};
			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

//------------------------------------------------------------------- Login & Logout Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Update Device Module Start ---------------------------------------------------------------------------//

router.post('/update_device', [

	check('account_id').isNumeric(),
	check('master_account_id').isNumeric(),
	check('device_id').isLength({ min: 1 }),
	check('device_type').isNumeric(),
	check('device_fcm_token').isLength({ min: 1 }),
	check('app_id').isNumeric(),
	check('api_token').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		account_id		    : req.body.master_account_id,
		active_account_id   : req.body.account_id,
		active_account_type : req.body.account_type,
		device_name		  : req.body.device_name,
		device_id		  : req.body.device_id,
		device_type		  : req.body.device_type,
		device_fcm_token  : req.body.device_fcm_token,
		app_id			  : req.body.app_id,
		api_token		  : req.body.api_token
	};


	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			if(reqParams.api_token == 'jazenet')
			{ 
				HelperJazenet.saveAppDeviceDetails(reqParams, function(return_device){	
					
					if(return_device !== null)
					{
						jsonRes = {
							success: '1',
							result: {device_details : return_device.device_details, master_account_details : {},child_account_details : [],link_account_details : []},
							successMessage: 'Jazenet device is saved.',
							errorCode: "0", 
							errorMessage: errorMessage 
						};
				
						res.status(201).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorCode: "101", 
							errorMessage: "Unable to update device. Please reinstall the app." 
						};

						res.status(201).json(jsonRes);
						res.end()
					}
				});	
			}
			else if(reqParams.api_token != 'jazenet' && parseInt(reqParams.account_id) == 0)
			{
				HelperJazenet.updateAppDeviceDetails(2,reqParams, function(return_device){	
												
					if(return_device !== null)
					{
						jsonRes = {
							success: '1',
							result: {device_details : return_device.device_details, master_account_details : {},child_account_details : [],link_account_details : []},
							successMessage: 'Jazenet device is updated.',
							errorCode: "0", 
							errorMessage: errorMessage 
						};
				
						res.status(201).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorCode: "101", 
							errorMessage: "Unable to update device. Please reinstall the app." 
						};

						res.status(201).json(jsonRes);
						res.end()
					}
				});	
			}
			else if(reqParams.api_token != 'jazenet' && parseInt(reqParams.account_id) !== 0)
			{
				HelperJazenet.updateAppDeviceDetails(2,reqParams, function(return_device){	
												
					if(return_device !== null)
					{
						Object.assign(reqParams, {api_token: return_device.device_details.api_token});

						 //get & Save Link account Details
						var reqParams_account = {		
							api_token  		  : reqParams.api_token,
							master_account_id : reqParams.account_id,
							account_id 		  : reqParams.active_account_id,
							account_type      : reqParams.account_type,
							app_id		      : reqParams.app_id,
						};

						HelperJazenet.getMasterChildLinkAccountList(reqParams_account, function(account_list){	

							if(account_list !== null)
							{
								Object.assign(return_device, account_list);

								jsonRes = {
									success: '1',
									result: return_device,
									successMessage: 'Jazenet device is updated.',
									errorCode: "0", 
									errorMessage: errorMessage 
								};
						
								res.status(201).json(jsonRes);
								res.end();
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorCode: "101", 
									errorMessage: "Unable to update device. Please reinstall the app." 
								};
		
								res.status(201).json(jsonRes);
								res.end()
							}
						});
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorCode: "101", 
							errorMessage: "Unable to update device. Please reinstall the app." 
						};

						res.status(201).json(jsonRes);
						res.end()
					}
				});		
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorCode: "100", 
					errorMessage: "Device not found." 
				};

				res.status(201).json(jsonRes);
				res.end();
			}
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found." 
	  		};

  			res.status(201).json(jsonRes);
 			res.end();
		}
	});
});

//------------------------------------------------------------------- Update Device Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Home Module Start ---------------------------------------------------------------------------//

router.post('/jazenet_home_details', [

	check('api_token').isAlphanumeric(),
	check('app_id').isNumeric(),
	check('master_account_id').isNumeric(),
	check('account_id').isNumeric()


], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {		
		api_token			: req.body.api_token, 
		app_id				: req.body.app_id, 
		account_id			: req.body.master_account_id,
		active_account_id	: req.body.account_id,
		active_account_type : req.body.account_type
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{ 
			HelperJazenet.getActiveAccountDetails(reqParams, function(retActiveDetails){	

				if(parseInt(Object.getOwnPropertyNames(retActiveDetails).length) !== 0)
				{ Object.assign(reqParams, {active_account_type: retActiveDetails.account_type}); }
				else 
				{ Object.assign(reqParams, {active_account_type: '0'}); }

					HelperJazenet.getHomeNotificatioNCount(reqParams.active_account_id, function(retCount){

						HelperJazenet.getHomeMenuList(reqParams, function(retMenu){

							HelperJazenet.getAccessMenuList(reqParams, function(retAccessMenu){

								jsonRes = {
									success: '1',
									result: {
										home_notification_count : retCount.total_count.toString(),
										active_account_details  : retActiveDetails, 
										jazenet_menu            : retMenu,
										access_menu             : retAccessMenu
									},
									successMessage: 'Jazenet home module list.',
									errorCode: "0", 
									errorMessage: errorMessage
								};

								statRes = 201;
								res.status(statRes).json(jsonRes);
								res.end();

							});
						});
					});
			});
		}	
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found." 
		  	};

	  		res.status(statRes).json(jsonRes);
	 		res.end();
		}
	});	
});

//------------------------------------------------------------------- Home Module End ---------------------------------------------------------------------------//


router.post('/account/security_pin_verification', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('security_pin').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {		
		api_token  : req.body.api_token, 
		account_id : req.body.account_id,
		security_pin : crypto.createHash('md5').update(req.body.security_pin).digest("hex")
	};

	HelperRestriction.checkParametersApiTokenJazenetJazecom(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	

			HelperJazenet.accountVerification(reqParams, function(retPinStatus){

				if(retPinStatus!=null){

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Account security pin is verified.',
						errorMessage: errorMessage 
					};
			
					res.status(201).json(jsonRes);
					res.end();



				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid security pin. Please try again." 
			  		};

		  			res.status(statRes).json(jsonRes);
		 			res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});



router.post('/account/communication_history', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('request_account_id').isNumeric(),
	check('flag').isNumeric()


], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {		
		api_token		: req.body.api_token, 
		account_id		: req.body.account_id,
		request_account_id : req.body.request_account_id,
		flag		       : req.body.flag

	};
	
	var arrFlags = [1,2,3,4];

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{			

			if(arrFlags.includes(parseInt(reqParams.flag)))
			{

				HelperJazenet.checkCommunicationHistory(reqParams, function(returnList){	

					jsonRes = {
							success: '1',
							result: {conversation_history:returnList},
							successMessage: 'Communication History',
							errorMessage: errorMessage 
						};
				
					res.status(201).json(jsonRes);
					res.end();

				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
		  		};

	  			res.status(statRes).json(jsonRes);
	 			res.end();
			}

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});



//------------------------------------------------------------------- Jazenet to Jazecom Module Start ---------------------------------------------------------------------------//


router.post('/jazenet_jazecom_connection', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('reciv_account_id').isNumeric(),
	check('app_id').isNumeric(),
	check('flag').isNumeric(),

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {		
		api_token    	   : req.body.api_token, 
		account_id 	 	   : req.body.account_id,
		account_type 	   : req.body.account_type,
		reciv_account_id   : req.body.reciv_account_id,
		app_id 		 	   : req.body.app_id,
		flag 		 	   : req.body.flag,
	};
	
	// flag 
	//  0 = switch app
	//  1 = chat
	//  2 = mail
	//  3 = feed

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			//get reciver account type 
			HelperJazenet.getAccountType(reqParams.reciv_account_id, function(reciv_account_type){

				Object.assign(reqParams, {reciv_account_type: reciv_account_type .toString()});

				//check two device status
				HelperJazenet.getJazenetJazecomDeviceDetails(reqParams, function(jazecomDeviceDetails){

					var jazecom_jazenet_connection =
						{
							flag : reqParams.flag,
							jazenet_device_details : jazecomDeviceDetails
						}

					if(parseInt(reqParams.flag) === 0)
					{
							jsonRes = {
								success: '1',
								result: {jazecom_jazenet_connection: jazecom_jazenet_connection},
								successMessage: 'jazecom connection details',
								errorMessage: errorMessage 
							};
					
							res.status(201).json(jsonRes);
							res.end();
					}
					else if(parseInt(reqParams.flag) === 1)
					{
						//chat
						HelperJazenet.getJazenetJazecomChatDetails(reqParams, function(chatDetails){

							if(chatDetails !== null)
							{
								var jazecom_chat_details = {
									status_flag   		:  chatDetails.status_flag,
									connect_flag		:  chatDetails.connect_flag,
									convers_id 	  	 	:  chatDetails.convers_id,
									receiver_account_id : reqParams.reciv_account_id,
								}

								Object.assign(jazecom_jazenet_connection, {jazecom_chat_details});

								jsonRes = {
									success: '1',
									result: {jazecom_jazenet_connection : jazecom_jazenet_connection},
									successMessage: chatDetails.status_msg,
									errorMessage: errorMessage 
								};
						
								res.status(201).json(jsonRes);
								res.end();
							}
							else
							{
								jsonRes = {
									success: '1',
									result: {},
									successMessage: 'no more jazecom connection details',
									errorMessage: errorMessage 
								};
						
								res.status(201).json(jsonRes);
								res.end();
							}
						});
					}
					else if(parseInt(reqParams.flag) === 2)
					{
						//mail
						HelperJazenet.getJazenetJazecomMailDetails(reqParams, function(mailDetails){

							if(mailDetails !== null)
							{
								var jazecom_mail_details = {
									status_flag   		  :  mailDetails.status_flag,
									mail_account_id 	  :  mailDetails.mail_account_id,
									receiver_account_id   :  mailDetails.receiver_account_id,
									receiver_account_name :  mailDetails.receiver_account_name,
									receiver_account_logo :  mailDetails.receiver_account_logo,
									receiver_account_email:  mailDetails.receiver_account_email,
								}
 
								Object.assign(jazecom_jazenet_connection, {jazecom_mail_details});

								jsonRes = {
									success: '1',
									result: {jazecom_jazenet_connection : jazecom_jazenet_connection},
									successMessage: mailDetails.status_msg,
									errorMessage: errorMessage 
								};
						
								res.status(201).json(jsonRes);
								res.end();
							}
							else
							{
								jsonRes = {
									success: '1',
									result: {},
									successMessage: 'no more jazecom connection details',
									errorMessage: errorMessage 
								};
						
								res.status(201).json(jsonRes);
								res.end();
							}
						});
					}
					else if(parseInt(reqParams.flag) === 3)
					{
						//feed
						HelperJazenet.getJazenetJazecomFeedDetails(reqParams, function(feedDetails){

							if(feedDetails !== null)
							{
								var jazecom_feed_details = {
									status_flag   		  :  feedDetails.status_flag,
									receiver_account_id   :  feedDetails.receiver_account_id,
									receiver_account_name :  feedDetails.receiver_account_name,
									receiver_account_logo :  feedDetails.receiver_account_logo,
								}
 
								Object.assign(jazecom_jazenet_connection, {jazecom_feed_details});

								jsonRes = {
									success: '1',
									result: {jazecom_jazenet_connection : jazecom_jazenet_connection},
									successMessage: feedDetails.status_msg,
									errorMessage: errorMessage 
								};
						
								res.status(201).json(jsonRes);
								res.end();
							}
							else
							{
								jsonRes = {
									success: '1',
									result: {},
									successMessage: 'no more jazecom connection details',
									errorMessage: errorMessage 
								};
						
								res.status(201).json(jsonRes);
								res.end();
							}
						});
					}
					else if(parseInt(reqParams.flag) === 4)
					{
						//diary
						HelperJazenet.getJazenetJazecomDiaryDetails(reqParams, function(diaryDetails){

							if(diaryDetails !== null)
							{
								var jazecom_diary_details = {
									status_flag   		  :  diaryDetails.status_flag,
									receiver_account_id   :  diaryDetails.receiver_account_id,
									receiver_account_name :  diaryDetails.receiver_account_name,
									receiver_account_logo :  diaryDetails.receiver_account_logo,
								}
 
								Object.assign(jazecom_jazenet_connection, {jazecom_diary_details});

								jsonRes = {
									success: '1',
									result: {jazecom_jazenet_connection : jazecom_jazenet_connection},
									successMessage: diaryDetails.status_msg,
									errorMessage: errorMessage 
								};
						
								res.status(201).json(jsonRes);
								res.end();
							}
							else
							{
								jsonRes = {
									success: '1',
									result: {},
									successMessage: 'no more jazecom connection details',
									errorMessage: errorMessage 
								};
						
								res.status(201).json(jsonRes);
								res.end();
							}
						});
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Invalid details." 
						};
		
						res.status(statRes).json(jsonRes);
						res.end();
					}
				});
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

//------------------------------------------------------------------- Jazenet to Jazecom Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Forgot email/password Module Start ---------------------------------------------------------------------------//

router.post('/reset_email_password', [
	check('api_token').isLength({ min: 1 }),
	check('app_id').isNumeric(),
	check('flag').isNumeric(),
	check('country_code_id').isLength({ min: 1 }),
], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};
	
	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {		
			api_token		: req.body.api_token, 
			account_id		: 0,
			app_id			: req.body.app_id,
			email 			: req.body.email,
			country_code_id	: req.body.country_code_id,
			mobile_number	: req.body.mobile_number,
			flag		    : req.body.flag
		};

		// flag : 1 = email, 2 = password
		
	HelperRestriction.checkParametersApiTokenJazenetJazecom(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{		
			HelperJazenet.resetEmailPassword(reqParams, function(verifStatus){

				if(verifStatus !== null)
				{
					jsonRes = {
						success: '1',
						result: {verif_account : verifStatus.account_id.toString(),flag : reqParams.flag},
						successMessage: 'jazenet will send verification code to your email.',
						errorMessage: "", 
					};
			
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					var errorMessage = "";

					if(parseInt(reqParams.flag) === 1)
					{ errorMessage = "We can't find a user with that e-mail address."; }
					else
					{ errorMessage = "We can't find a user with that mobile number."; }

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: errorMessage
					};
	
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

router.post('/resend_verificationcode', [
	check('api_token').isLength({ min: 1 }),
	check('app_id').isNumeric(),
	check('flag').isNumeric(),
	check('account_id').isNumeric()
], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {		
			api_token		: req.body.api_token, 
			account_id		: req.body.account_id,
			app_id			: req.body.app_id,
			flag		    : req.body.flag
		};

		// flag : 1 = email, 2 = password

	var reqAPIParams = { api_token : req.body.api_token, account_id : 0	}

	HelperRestriction.checkParametersApiTokenJazenetJazecom(reqAPIParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{		
			HelperJazenet.resendVerificationCode(reqParams, function(verifStatus){

				if(verifStatus !== null)
				{
					jsonRes = {
						success: '1',
						result: {verif_account : verifStatus.account_id.toString(),flag : reqParams.flag},
						successMessage: 'jazenet will send verification code to your email.',
						errorMessage: errorMessage 
					};
			
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "We can't find a user."
					};
	
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

router.post('/check_reset_verification_code', [
	check('api_token').isLength({ min: 1 }),
	check('app_id').isNumeric(),
	check('verif_code').isLength({ min: 1 }),
], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {		
			api_token		: req.body.api_token, 
			account_id		: 0,
			app_id			: req.body.app_id,
			verif_code 		: req.body.verif_code
		};

		// flag : 1 = email, 2 = password

	HelperRestriction.checkParametersApiTokenJazenetJazecom(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{		
			HelperJazenet.checkResetVerificationCode(reqParams, function(status,verifResponse){

				if(parseInt(status) === 1 && verifResponse !== null)
				{
					jsonRes = {
						success: '1',
						result: {verif_response : verifResponse},
						successMessage: 'successfully verified identity.',
						errorMessage: errorMessage 
					};
			
					res.status(201).json(jsonRes);
					res.end();
				}
				else if(parseInt(status) === 2)
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "The code has expired. Please re-send the verification code to try again"
					};
	
					res.status(statRes).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "This password reset token is invalid"
					};
	
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

router.post('/reset_password', [
	check('api_token').isLength({ min: 1 }),
	check('app_id').isNumeric(),
	check('account_id').isNumeric(),
	check('password').isLength({ min: 1 }),
], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {		
			api_token		: req.body.api_token, 
			account_id		: req.body.account_id,
			app_id			: req.body.app_id,
			password 		: crypto.createHash('md5').update(req.body.password).digest("hex")
		};

		var reqAPIParams = { api_token : req.body.api_token, account_id : 0	}

	HelperRestriction.checkParametersApiTokenJazenetJazecom(reqAPIParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{		
			HelperJazenet.resetPassword(reqParams, function(status){

				if(parseInt(status) === 1)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'your password has been successfully updated',
						errorMessage: errorMessage 
					};
			
					res.status(201).json(jsonRes);
					res.end();
				}
				else if(parseInt(status) === 2)
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "you will not be able to use the same password used before."
					};
	
					res.status(statRes).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to precess request. Please try again." 
					};
	
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

//------------------------------------------------------------------- Forgot email/password Module End ---------------------------------------------------------------------------//




module.exports = router;
