require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperProfessional = require('../helpers/HelperProfessional.js');
const HelperDocuments = require('../helpers/HelperDocuments.js');

const { check, validationResult } = require('express-validator');

const storagePath = G_STATIC_IMG_PATH+"professional_account/";
const STATIC_IMG_URL = G_STATIC_IMG_URL+"uploads/jazenet/professional_account/";

const crypto = require('crypto');
const mime = require('mime-types');
const multer = require('multer');
const fs = require('fs');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};

var Jimp = require('jimp');

//------------------------------------------------------------------- Professional Profile Module Start ---------------------------------------------------------------------------//

router.post('/professional/professional_profile', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('prof_id').isNumeric(),
	//check('account_type').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		prof_id      : req.body.prof_id,
		account_type : 2,
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			//HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				//if(parseInt(retAccount) === 1)
				//{
					HelperProfessional.getProfessionalProfileDetails(reqParams, function(retSearched){

						if(retSearched!=null){
							
							jsonRes = {
								success: '1',
								result: {'professional_detail_info' : retSearched},
								successMessage: 'Professional Profile Details',
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();

						}else{

							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "No result found." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}
					});
				//}
				//else
				//{
					// jsonRes = {
					// 	success: success,
					// 	result: {},
					// 	successMessage: successMessage,
					// 	errorMessage: "Invalid account details provided." 
					// };

					// res.status(statRes).json(jsonRes);
					// res.end();
				//}


			//});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Professional Profile Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Accreditation Module Start ---------------------------------------------------------------------------//

router.post('/professional/accreditation_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('prof_id').isNumeric(),
	check('account_type').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		prof_id : req.body.prof_id,
		account_type : req.body.account_type
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{

					HelperProfessional.getAccreditationList(reqParams.prof_id, function(review_list){

						if(review_list!=null){
							
							jsonRes = {
								success: '1',
								result: {'accreditation_list' : review_list},
								successMessage: 'Professional Accreditation List',
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();

						}else{

							jsonRes = {
								success: '1',
								result: {'accreditation_list' : []},
								successMessage: "No result found.",
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
					});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Accreditation Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Notice Module Start ---------------------------------------------------------------------------//

router.post('/professional/notice_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('prof_id').isNumeric(),
	check('account_type').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		prof_id : req.body.prof_id,
		account_type : req.body.account_type
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{

					HelperProfessional.getNoticeList(reqParams.prof_id, function(review_list){

						if(review_list!=null){
							
							jsonRes = {
								success: '1',
								result: {'notice_list' : review_list},
								successMessage: 'Professional Notice List',
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();

						}else{

							jsonRes = {
								success: '1',
								result: {'notice_list' : []},
								successMessage: "No result found.",
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
					});
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Notice Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Photos Module Start ---------------------------------------------------------------------------//

router.post('/professional/photos_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('prof_id').isNumeric(),
	check('account_type').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		prof_id : req.body.prof_id,
		account_type : req.body.account_type
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{

					HelperProfessional.getPhotoList(reqParams.prof_id, function(review_list){

						if(review_list!=null){
							
							jsonRes = {
								success: '1',
								result: {'photos_list' : review_list},
								successMessage: 'Professional Photo List',
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();

						}else{

							jsonRes = {
								success: '1',
								result: {'photos_list' : []},
								successMessage: "No result found.",
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
					});

				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Photos Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Professional History Module Start ---------------------------------------------------------------------------//

router.post('/professional/professional_history', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('prof_id').isNumeric(),
	check('account_type').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		prof_id    : req.body.prof_id,
		account_type    : req.body.account_type
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{

					HelperProfessional.getProfessionalHistoryList(reqParams.prof_id, function(review_list){

						if(review_list!=null){
							
							jsonRes = {
								success: '1',
								result: {'professional_history' : review_list},
								successMessage: 'Professional Professional List',
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();

						}else{

							jsonRes = {
								success: '1',
								result: {'professional_history' : []},
								successMessage: "No result found.",
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
					});
				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Professional History Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Qualification Module Start ---------------------------------------------------------------------------//

router.post('/professional/qualification_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('prof_id').isNumeric(),
	check('account_type').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		prof_id : req.body.prof_id,
		account_type : req.body.account_type
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{
					HelperProfessional.getQualificationList(reqParams.prof_id, function(review_list){

						if(review_list!=null){
							
							jsonRes = {
								success: '1',
								result: {'qualification_list' : review_list},
								successMessage: 'Professional Qualification List',
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();

						}else{

							jsonRes = {
								success: '1',
								result: {'qualification_list' : []},
								successMessage: "No result found.",
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
					});
				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});
//------------------------------------------------------------------- Qualification Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Testimonials Module Start ---------------------------------------------------------------------------//

router.post('/professional/testimonial_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('prof_id').isNumeric(),
	check('account_type').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		prof_id : req.body.prof_id,
		account_type : req.body.account_type,
		flag : req.body.flag
	};

	// flag 1 = inbox list
	// flag 2 = normal list

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{	

					if(parseInt(reqParams.flag) === 0)
					{

						HelperProfessional.getTestimonialListInbox(reqParams.prof_id, function(review_list){

						
							if(review_list!=null){

								HelperProfessional.getTestimonialListInboxCount(reqParams.prof_id,0, function(list_count){
								
									jsonRes = {
										success: '1',
										result: {
											'testimonial_count' : list_count.toString(),
											'testimonial_list'  : review_list
										},
										successMessage: 'Professional Testimonial List',
										errorMessage: errorMessage 
									};
									res.status(201).json(jsonRes);
									res.end();

								});

								}else{

									jsonRes = {
										success: '1',
										result: {
											'testimonial_count' : '0',
											'testimonial_list'  : []
										},
										successMessage: "No result found.",
										errorMessage: errorMessage 
									};
									res.status(201).json(jsonRes);
									res.end();
								}
							

						});
										
					}
					else if(parseInt(reqParams.flag) === 1)
					{
						HelperProfessional.getTestimonialList(reqParams.prof_id, function(review_list){

							if(review_list!=null){
								

								HelperProfessional.getTestimonialListInboxCount(reqParams.prof_id,0, function(list_count){

									jsonRes = {
										success: '1',
										result: {
											'testimonial_count' : list_count.toString(),
											'testimonial_list'  : review_list
										},
										successMessage: 'Professional Testimonial List',
										errorMessage: errorMessage 
									};
									res.status(201).json(jsonRes);
									res.end();

								});

							}else{

								jsonRes = {
									success: '1',
									result: {
										'testimonial_count' : '0',
										'testimonial_list'  : []
									},
									successMessage: "No result found.",
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();
							}
						});
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Invalid flag provided." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/professional/post_testimonial', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('prof_id').isNumeric(),
	check('account_type').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		prof_id : req.body.prof_id,
		message    : req.body.message,
		account_type    : req.body.account_type
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{

					HelperProfessional.postTestimonial(reqParams, function(retSearched){

						if(parseInt(retSearched) !== 0){
							
							HelperProfessional.getTestimonialList(reqParams.prof_id, function(review_list){

								if(review_list!=null){
									
									jsonRes = {
										success: '1',
										result: {'testimonial_list' : review_list},
										successMessage: 'post successfully sent',
										errorMessage: errorMessage 
									};
									res.status(201).json(jsonRes);
									res.end();
				
								}else{
				
									jsonRes = {
										success: '1',
										result: {'testimonial_list' : []},
										successMessage: "Unable to process request. Please try again.",
										errorMessage: errorMessage 
									};
									res.status(201).json(jsonRes);
									res.end();
								}
							});

						}else{

							jsonRes = {
								success: '1',
								result: {'testimonial_list' : []},
								successMessage: "No result found.",
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
					});

				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

//------------------------------------------------------------------- Testimonials Module End  ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Company Favourite Module Start ---------------------------------------------------------------------------//

router.post('/professional/save_to_favorite', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('prof_id').isNumeric(),
	check('account_type').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		prof_id    : req.body.prof_id,
		account_type    : req.body.account_type
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{
					HelperProfessional.saveAccountFavourite(reqParams, function(fav_status){

						if(parseInt(fav_status) !== 0){
							
							var success_message = "";
							var result = "0";
							if(parseInt(fav_status) === 1)
							{ success_message = "Successfully add to Favorites"; result = "1"; }
							else
							{ success_message = "Successfully removed to Favorites"; }

							jsonRes = {
								success: '1',
								result: {fav_status:result},
								successMessage: success_message,
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();

						}else{

							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "some error occurred, please try again." 
							};
							res.status(statRes).json(jsonRes);
							res.end();
						}
					});

				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Company Favourite Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Professional Profile Essentials  ---------------------------------------------------------------------------//
router.post('/professional/professional_essentials', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('flag').isNumeric(),


], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {		
		api_token  : req.body.api_token, 
		account_id : req.body.account_id,
		account_type : req.body.account_type,
		flag       : req.body.flag,

		title_id   : req.body.title_id,
		first_name : req.body.first_name,
		last_name  : req.body.last_name,
		email      : req.body.email,
		dob        : req.body.dob,
		profession : req.body.profession,
		nationality_id : req.body.nationality_id,
		mobile_country_code : req.body.mobile_country_code,
		mobile_number  : req.body.mobile_number,
		mobile_country_code_2 : req.body.mobile_country_code_2,
		mobile_number_2  : req.body.mobile_number_2,
		country_id     : req.body.country_id,
		state_id       : req.body.state_id,
		city_id        : req.body.city_id,
		area_id        : req.body.area_id,

		street_name    : req.body.street_name,
		street_no      : req.body.street_no,
		building_name  : req.body.building_name,
		building_no    : req.body.building_no

	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{

					if(parseInt(reqParams.flag) == 1)
					{

						HelperProfessional.getUserIDEssentials(reqParams, function(retPinStatus){


									jsonRes = {
									success: '1',
									result: retPinStatus,
									successMessage: 'Professional essential information.',
									errorMessage: errorMessage 
								};
						
								res.status(201).json(jsonRes);
								res.end();

							if(retPinStatus!=null){

								jsonRes = {
									success: '1',
									result: retPinStatus,
									successMessage: 'Professional essential information.',
									errorMessage: errorMessage 
								};
						
								res.status(201).json(jsonRes);
								res.end();


							}else{

								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "No result found" 
						  		};

					  			res.status(statRes).json(jsonRes);
					 			res.end();
							}

						});
					}
					else if(parseInt(reqParams.flag)==2)
					{

						HelperProfessional.updateProfessionalEssDetails(reqParams, function(retUpdate){

							if(parseInt(retUpdate)==1){


								setTimeout(function(){  

									HelperProfessional.getUserIDEssentials(reqParams, function(retResult){

										jsonRes = {	
											success: '1',
											result: retResult,
											successMessage: 'Professional essential information is successfully updated.',
											errorMessage: errorMessage 
										};
								
										res.status(201).json(jsonRes);
										res.end();

									
									});

								}, 500);

							}else if(parseInt(retUpdate)==2){

								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "No result found" 
						  		};

					  			res.status(statRes).json(jsonRes);
					 			res.end();

							}else{

								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
						  		};

					  			res.status(statRes).json(jsonRes);
					 			res.end();
							}

						});
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Invalid flag provided." 
				  		};

			  			res.status(statRes).json(jsonRes);
			 			res.end();
					}
				}		
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

//------------------------------------------------------------------- Professional Profile Essentials  ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Professional Upload Profile Picture  ---------------------------------------------------------------------------//



var storage = multer.diskStorage({
	destination: function (req, file, callback) {
	
	function asyncFunction (item, cb) {
	  	setTimeout(() => {

			
			var out_path = storagePath+req.body.account_id+'/';
			var inner_path = out_path+'/logo/';

			var main_path = inner_path;

			if (!fs.existsSync(out_path)){
				fs.mkdirSync(out_path,'0757', true); 
			}

			if (!fs.existsSync(inner_path)){
				fs.mkdirSync(inner_path,'0757', true); 
			}

			callback(null, main_path);

			cb();
	  	}, 100);
	}

	let requests = 0;

		if(req.body.account_id !=''){

			return new Promise((resolve) => {
				requests = req.body.account_id;
		  		asyncFunction(requests, resolve);
		    });

			Promise.all(requests);
		}
  },
  	filename: function (req, file, callback) {

  		var ext = getExtension(file.originalname);
  		var cleanName = getCleanFileName(file.originalname);
  		var completeFileName = req.body.account_id+'_'+Date.now()+'.'+ext;

    	callback(null, completeFileName);
  	}
});


router.post('/professional/update_profile_logo', function(req, res) {

	var upload = multer({
		storage: storage,
	}).single('file');

	upload(req, res, function(err) {

 	 	var extName = req.file.filename.split(".");
 	 	var filename = extName[extName.length - 2];

	 	var extArray = req.file.filename.split(".");
 	 	var extention = extArray[extArray.length - 1];

 	  	var file_properties = req.file.mimetype.split("/");

 		var base_url = '';
        var filetype = '';

 		var complete_filename = filename+'.'+extention;
 		base_url = STATIC_IMG_URL+req.body.account_id+'/logo/'+complete_filename;

		var tocropImage = STATIC_IMG_URL+req.body.account_id+'/logo/'+complete_filename;
		var toStorageImage = storagePath+req.body.account_id+'/logo_thumb/'+'thumb_'+complete_filename;

		async function resize() {
		  // Read the image.
		  const image = await Jimp.read(tocropImage);
		  // Resize the image to width 150 and heigth 150.
		  await image.resize(150, 150);
		  // Save and overwrite the image
		  await image.writeAsync(toStorageImage);
		}
		resize();


 		filetype = file_properties[0]; 

 		var arrParams = {account_id:req.body.account_id, file_name:req.file.filename};
 		
    	HelperProfessional.getProfilePicture(arrParams,function(retStatus){

	        HelperDocuments.getFileTypes('.'+extention, function(reType){

	            var results = {
	                file_name:filename,
	                file_format:filetype,
	                file_type:reType.toString(),
	                extention:extention,
	                file_path:base_url,
	            };
	           
	            jsonRes = {
	                success: '1',
	                result: results,
	                successMessage: 'File is successfully uploaded.',
	                errorMessage: errorMessage 
	            };
	            res.status(201).json(jsonRes);
	            res.end();
	        });

        });

	});
});

function getExtension(path) {
    var basename = path.split(/[\\/]/).pop(),  // extract file name from full path ...
                                             	 // (supports `\\` and `/` separators)
        pos = basename.lastIndexOf(".");       // get last position of `.`
    if (basename === "" || pos < 1)            // if file name is empty or ...
        return "";                             //  `.` not found (-1) or comes first (0)
    return basename.slice(pos + 1);            // extract extension ignoring `.`
}

function getFilename(path) {
    var basename = path.split(/[\\/]/).pop(),      
        pos = basename.split('.'); 
    if (basename === "" || pos.length < 1)           
        return "";                          
    return pos[0];            
}


function getCleanFileName(path) {

	var basename = path.split(/\.(?=[^\.]+$)/);
	var cleanString = basename[0].replace(/[|&;$%@"<>()+1234567890,]/g, "");
         
    return cleanString;           
}

//------------------------------------------------------------------- Professional Upload Profile Picture  ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Professional About Update  ---------------------------------------------------------------------------//
router.post('/professional/update_about', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		account_type : req.body.account_type,
		about      : req.body.about,
		flag       : req.body.flag
	};


	// flag 2 update 
	// flag 3 delete

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{

					if(parseInt(reqParams.flag) === 1)
					{

						HelperProfessional.getProfAbout(reqParams, function(retabout){

							if(retabout!=null)
							{
								jsonRes = {
									success: '1',
									result: {about:retabout},
									successMessage: 'About information.',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();	
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			

						});

					}
					else if(parseInt(reqParams.flag) === 2 )
					{
						
						HelperProfessional.updateAbout(reqParams, function(retUpdate){

							if(parseInt(retUpdate) == 1)
							{
								setTimeout(function(){ 
									
									HelperProfessional.getProfAbout(reqParams, function(retabout){

										if(retabout!=null)
										{
											jsonRes = {
												success: '1',
												result: {about:retabout},
												successMessage: 'About information.',
												errorMessage: errorMessage 
											};
											res.status(201).json(jsonRes);
											res.end();	
										}
										else
										{
											jsonRes = {
												success: success,
												result: {},
												successMessage: successMessage,
												errorMessage: "Unable to process request. Please try again." 
											};

											res.status(statRes).json(jsonRes);
											res.end();
										}
						

									});


								}, 1000);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			

						});
					}
					// else if(parseInt(reqParams.flag) === 3)
					// {

					// 	HelperProfessional.updateAbout(reqParams, function(retUpdate){

					// 		if(parseInt(retUpdate) == 1)
					// 		{
					// 			jsonRes = {
					// 				success: '1',
					// 				result: {},
					// 				successMessage: 'About information is successfully removed.',
					// 				errorMessage: errorMessage 
					// 			};
					// 			res.status(201).json(jsonRes);
					// 			res.end();	
					// 		}
					// 		else
					// 		{
					// 			jsonRes = {
					// 				success: success,
					// 				result: {},
					// 				successMessage: successMessage,
					// 				errorMessage: "Unable to process request. Please try again." 
					// 			};

					// 			res.status(statRes).json(jsonRes);
					// 			res.end();
					// 		}
			
					// 	});

					// }
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Invalid flag provided." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}

				}		
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Professional About Update  ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Professional History Update  ---------------------------------------------------------------------------//
router.post('/professional/update_professional_history', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		account_type    : req.body.account_type,
		flag          : req.body.flag,
		id            : req.body.id,

		company_name  : req.body.company_name,
		position      : req.body.position,
		city_id       : req.body.city_id,
		country_id    : req.body.country_id,
		from_month    : req.body.from_month,
		from_year     : req.body.from_year,
		to_month      : req.body.to_month,
		to_year       : req.body.to_year

	};


	// flag 0 list 
	// flag 1 add
	// flag 2 update 
	// flag 3 delete

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{

					if(parseInt(reqParams.flag) === 0 )
					{
						
						HelperProfessional.getmyProfessionalHistory(reqParams, function(retList){

							if(retList!=null)
							{
								jsonRes = {
									success: '1',
									result: {professional_history:retList},
									successMessage: 'Professional History',
									errorMessage: errorMessage 
								};

								res.status(201).json(jsonRes);
								res.end();	
							}
							else
							{

								jsonRes = {
									success: '1',
									result: {professional_history:[]},
									successMessage: 'No result found.',
									errorMessage: errorMessage 
								};

								res.status(201).json(jsonRes);
								res.end();
							}
			

						});
					}
					else if(parseInt(reqParams.flag) === 1)
					{

						HelperProfessional.updateProfessionalHistory(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getmyProfessionalHistory(reqParams, function(retList){

										jsonRes = {
											success: '1',
											result: {professional_history:retList},
											successMessage: 'Professional history is successfully added.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	

									});

						 		}, 150);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else if(parseInt(reqParams.flag) === 2)
					{

						HelperProfessional.updateProfessionalHistory(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getmyProfessionalHistory(reqParams, function(retList){

										jsonRes = {
											success: '1',
											result: {professional_history:retList},
											successMessage: 'Professional history is successfully updated.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	

									});

						 		}, 150);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else if(parseInt(reqParams.flag) === 3)
					{

						HelperProfessional.updateProfessionalHistory(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getmyProfessionalHistory(reqParams, function(retList){

										jsonRes = {
											success: '1',
											result: {professional_history:retList},
											successMessage: 'Professional history is successfully removed.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	

									});

						 		}, 150);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Invalid flag provided." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
		
				}		
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Professional History Update  ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Qualification Update  ---------------------------------------------------------------------------//
router.post('/professional/update_qualification', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		account_type    : req.body.account_type,
		flag          : req.body.flag,
		id            : req.body.id,

		institution   : req.body.institution,
		course        : req.body.course,
		city_id       : req.body.city_id,
		country_id    : req.body.country_id,
		from_month    : req.body.from_month,
		from_year     : req.body.from_year,
		to_month      : req.body.to_month,
		to_year       : req.body.to_year

	};


	// flag 0 list 
	// flag 1 add
	// flag 2 update 
	// flag 3 delete

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{

					if(parseInt(reqParams.flag) === 0 )
					{
						
						HelperProfessional.getMyQualificationList(reqParams, function(retList){

							if(retList!=null)
							{
								jsonRes = {
									success: '1',
									result: {qualification_history:retList},
									successMessage: 'Qualification History',
									errorMessage: errorMessage 
								};

								res.status(201).json(jsonRes);
								res.end();	
							}
							else
							{

								jsonRes = {
									success: '1',
									result: {qualification_history:[]},
									successMessage: 'No result found.',
									errorMessage: errorMessage 
								};

								res.status(201).json(jsonRes);
								res.end();
							}
			

						});
					}
					else if(parseInt(reqParams.flag) === 1)
					{

						HelperProfessional.updateQualificationHistory(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getMyQualificationList(reqParams, function(retList){

										jsonRes = {
											success: '1',
											result: {qualification_history:retList},
											successMessage: 'Qualification history is successfully added.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	

									});

						 		}, 150);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else if(parseInt(reqParams.flag) === 2)
					{

						HelperProfessional.updateQualificationHistory(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getMyQualificationList(reqParams, function(retList){

										jsonRes = {
											success: '1',
											result: {qualification_history:retList},
											successMessage: 'Qualification history is successfully updated.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	

									});

						 		}, 150);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else if(parseInt(reqParams.flag) === 3)
					{

						HelperProfessional.updateQualificationHistory(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getMyQualificationList(reqParams, function(retList){

										jsonRes = {
											success: '1',
											result: {qualification_history:retList},
											successMessage: 'Qualification history is successfully removed.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	

									});

						 		}, 150);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Invalid flag provided." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
			
				}		
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Upload Photos  ---------------------------------------------------------------------------//

// flag 1 Photos
// flag 2 accreditation
// flag 3 notice


var storageUpload = multer.diskStorage({
	destination: function (req, file, callback) {
	
	function asyncFunction (item, cb) {
	  	setTimeout(() => {

			var out_path = storagePath+req.body.account_id+'/';

			var inner_path;
		
			if(parseInt(req.body.flag) == 1){
				inner_path = out_path+'/professional_photos/';
			}
			else if(parseInt(req.body.flag) == 2)
			{
				inner_path = out_path+'/accreditation/';
			}
			else if(parseInt(req.body.flag) == 3)
			{
				inner_path = out_path+'/notice_board/';
			}

			var main_path = inner_path;

			if (!fs.existsSync(out_path)){
				fs.mkdirSync(out_path,'0757', true); 
			}

			if (!fs.existsSync(inner_path)){
				fs.mkdirSync(inner_path,'0757', true); 
			}

			callback(null, main_path);

			cb();
	  	}, 100);
	}

	let requests = 0;

		if(req.body.account_id !=''){

			return new Promise((resolve) => {
				requests = req.body.account_id;
		  		asyncFunction(requests, resolve);
		    });

			Promise.all(requests);
		}
  },
  	filename: function (req, file, callback) {

  		var ext = getExtension(file.originalname);
  		var cleanName = getCleanFileName(file.originalname);
  		var completeFileName = req.body.account_id+'_'+Date.now()+'.'+ext;

    	callback(null, completeFileName);
  	}
});


router.post('/professional/upload_image', function(req, res) {

	var upload = multer({
		storage: storageUpload,
	}).single('file');

	upload(req, res, function(err) {

 	 	var extName = req.file.filename.split(".");
 	 	var filename = extName[extName.length - 2];

	 	var extArray = req.file.filename.split(".");
 	 	var extention = extArray[extArray.length - 1];

 	  	var file_properties = req.file.mimetype.split("/");

 		var base_url = '';
        var filetype = '';

 		var complete_filename = filename+'.'+extention;



 		if(parseInt(req.body.flag) == 1)
 		{
			base_url = STATIC_IMG_URL+req.body.account_id+'/professional_photos/'+complete_filename;
 		}
 		else if(parseInt(req.body.flag) == 2)
 		{
			base_url = STATIC_IMG_URL+req.body.account_id+'/accreditation/'+complete_filename;
 		}
		else if(parseInt(req.body.flag) == 3)
 		{
			base_url = STATIC_IMG_URL+req.body.account_id+'/notice_board/'+complete_filename;
 		}


 		filetype = file_properties[0]; 

 		var arrParams = {account_id:req.body.account_id, file_name:req.file.filename};
 		
        HelperDocuments.getFileTypes('.'+extention, function(reType){

            var results = {
                file_name:filename,
                file_format:filetype,
                file_type:reType.toString(),
                extention:extention,
                file_path:base_url,
            };
           
            jsonRes = {
                success: '1',
                result: results,
                successMessage: 'File is successfully uploaded.',
                errorMessage: errorMessage 
            };
            res.status(201).json(jsonRes);
            res.end();
        });

	});
});

//------------------------------------------------------------------- Photos Update  ---------------------------------------------------------------------------//
router.post('/professional/update_photos', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		account_type    : req.body.account_type,
		flag          : req.body.flag,
		id            : req.body.id,

		title         : req.body.title,
		file_name     : req.body.file_name,
		file_ext      : req.body.file_ext

	};


	// flag 0 list 
	// flag 1 add
	// flag 2 update 
	// flag 3 delete

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{

					if(parseInt(reqParams.flag) === 0 )
					{
						
						HelperProfessional.getMyPhotos(reqParams, function(retList){

							if(retList!=null)
							{
								jsonRes = {
									success: '1',
									result: {photos:retList},
									successMessage: 'Photos History',
									errorMessage: errorMessage 
								};

								res.status(201).json(jsonRes);
								res.end();	
							}
							else
							{

								jsonRes = {
									success: '1',
									result: {photos:[]},
									successMessage: 'No result found.',
									errorMessage: errorMessage 
								};

								res.status(201).json(jsonRes);
								res.end();
							}
			

						});
					}
					else if(parseInt(reqParams.flag) === 1)
					{

						HelperProfessional.updateMyPhotos(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getMyPhotos(reqParams, function(retList){

										jsonRes = {
											success: '1',
											result: {photos:retList},
											successMessage: 'Photo is successfully added.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	

									});

						 		}, 150);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else if(parseInt(reqParams.flag) === 2)
					{

						HelperProfessional.updateMyPhotos(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getMyPhotos(reqParams, function(retList){

										jsonRes = {
											success: '1',
											result: {photos:retList},
											successMessage: 'Photo is successfully updated.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	

									});

						 		}, 150);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else if(parseInt(reqParams.flag) === 3)
					{

						HelperProfessional.updateMyPhotos(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getMyPhotos(reqParams, function(retList){

										jsonRes = {
											success: '1',
											result: {photos:retList},
											successMessage: 'Photo is successfully removed.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	

									});

						 		}, 150);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Invalid flag provided." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
				
				}		
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


//------------------------------------------------------------------- Accreditation Update  ---------------------------------------------------------------------------//
router.post('/professional/update_accreditation', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		account_type    : req.body.account_type,
		flag          : req.body.flag,
		id            : req.body.id,

		title         : req.body.title,
		organization  : req.body.organization,
		file_name     : req.body.file_name,
		file_ext      : req.body.file_ext,
		month         : req.body.month,
		year          : req.body.year

	};


	// flag 0 list 
	// flag 1 add
	// flag 2 update 
	// flag 3 delete

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{

					if(parseInt(reqParams.flag) === 0 )
					{
						
						HelperProfessional.getMyAccreditation(reqParams, function(retList){

							if(retList!=null)
							{
								jsonRes = {
									success: '1',
									result: {accreditation:retList},
									successMessage: 'Accreditation History',
									errorMessage: errorMessage 
								};

								res.status(201).json(jsonRes);
								res.end();	
							}
							else
							{

								jsonRes = {
									success: '1',
									result: {accreditation:[]},
									successMessage: 'No result found.',
									errorMessage: errorMessage 
								};

								res.status(201).json(jsonRes);
								res.end();
							}
			

						});
					}
					else if(parseInt(reqParams.flag) === 1)
					{

						HelperProfessional.updateAccreditation(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getMyAccreditation(reqParams, function(retList){

										jsonRes = {
											success: '1',
											result: {accreditation:retList},
											successMessage: 'Accreditation is successfully added.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	

									});

						 		}, 150);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else if(parseInt(reqParams.flag) === 2)
					{

						HelperProfessional.updateAccreditation(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getMyAccreditation(reqParams, function(retList){

										jsonRes = {
											success: '1',
											result: {accreditation:retList},
											successMessage: 'Accreditation is successfully updated.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	

									});

						 		}, 150);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else if(parseInt(reqParams.flag) === 3)
					{

						HelperProfessional.updateAccreditation(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getMyAccreditation(reqParams, function(retList){

										jsonRes = {
											success: '1',
											result: {accreditation:retList},
											successMessage: 'Accreditation is successfully removed.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	

									});

						 		}, 150);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Invalid flag provided." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}

				}		
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


//------------------------------------------------------------------- Notice Update  ---------------------------------------------------------------------------//
router.post('/professional/update_notice', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		account_type    : req.body.account_type,
		flag          : req.body.flag,
		id            : req.body.id,

		title         : req.body.title,
		notice        : req.body.notice,
		file_name     : req.body.file_name,
		file_ext      : req.body.file_ext,
		groups        : req.body.groups

	};


	// flag 0 list 
	// flag 1 add
	// flag 2 update 
	// flag 3 delete

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{

					if(parseInt(reqParams.flag) === 0 )
					{
						
						HelperProfessional.getMyNotice(reqParams, function(retList){

							if(retList!=null)
							{
								jsonRes = {
									success: '1',
									result: {notice:retList},
									successMessage: 'Notice list',
									errorMessage: errorMessage 
								};

								res.status(201).json(jsonRes);
								res.end();	
							}
							else
							{

								jsonRes = {
									success: '1',
									result: {notice:[]},
									successMessage: 'No result found.',
									errorMessage: errorMessage 
								};

								res.status(201).json(jsonRes);
								res.end();
							}
			

						});
					}
					else if(parseInt(reqParams.flag) === 1)
					{

						HelperProfessional.updateNotice(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getMyNotice(reqParams, function(retList){

										jsonRes = {
											success: '1',
											result: {notice:retList},
											successMessage: 'Notice is successfully added.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	

									});

						 		}, 1000);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else if(parseInt(reqParams.flag) === 2)
					{

						HelperProfessional.updateNotice(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getMyNotice(reqParams, function(retList){

										jsonRes = {
											success: '1',
											result: {notice:retList},
											successMessage: 'Notice is successfully updated.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	

									});

						 		}, 1000);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else if(parseInt(reqParams.flag) === 3)
					{

						HelperProfessional.updateNotice(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getMyNotice(reqParams, function(retList){

										jsonRes = {
											success: '1',
											result: {notice:retList},
											successMessage: 'Notice is successfully removed.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	

									});

						 		}, 1000);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Invalid flag provided." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
				}		
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Testimonial Update  ---------------------------------------------------------------------------//
router.post('/professional/update_testimonials', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		account_type    : req.body.account_type,
		flag          : req.body.flag,
		id            : req.body.id,
		status        : req.body.status,
	};


	// flag 0 list 
	// flag 2 update status
	// flag 3 delete


	// status 1 accept
	// status 2 reject

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{
					if(parseInt(reqParams.flag) === 0 )
					{
						
						HelperProfessional.getMyTestimonialsInbox(reqParams, function(inboxList){

							HelperProfessional.getMyTestimonials(reqParams, function(retList){

								if(retList!=null)
								{
									jsonRes = {
										success: '1',
										result: {
											testimonials_inbox_list:inboxList,
											testimonial_list:retList
										},
										successMessage: 'Testimonial list',
										errorMessage: errorMessage 
									};

									res.status(201).json(jsonRes);
									res.end();	
								}
								else
								{

									jsonRes = {
										success: '1',
										result: {
											testimonials_inbox_list:inboxList,
											testimonial_list:[]
										},
										successMessage: 'No result found.',
										errorMessage: errorMessage 
									};

									res.status(201).json(jsonRes);
									res.end();
								}
				

							});

						});
					}
					else if(parseInt(reqParams.flag) === 2)
					{	

						if(parseInt(reqParams.status) == 1 || parseInt(reqParams.status) == 2)
						{

							HelperProfessional.updateTestimonials(reqParams, function(retStatus){

								if(parseInt(retStatus) === 1)
								{

									setTimeout(function(){ 

										HelperProfessional.getMyTestimonialsInbox(reqParams, function(inboxList){

											HelperProfessional.getMyTestimonials(reqParams, function(retList){

												var sucMessege = "Testimonial is successfully accepted.";
												if(parseInt(reqParams.status) == 2)
												{
													sucMessege = "Testimonial is successfully rejected.";
												}

												if(retList!=null)
												{
													jsonRes = {
														success: '1',
														result: {
															testimonials_inbox_list:inboxList,
															testimonial_list:retList
														},
														successMessage: sucMessege,
														errorMessage: errorMessage 
													};

													res.status(201).json(jsonRes);
													res.end();	
												}
												else
												{

													jsonRes = {
														success: '1',
														result: {
															testimonials_inbox_list:inboxList,
															testimonial_list:[]
														},
														successMessage: 'No result found.',
														errorMessage: errorMessage 
													};

													res.status(201).json(jsonRes);
													res.end();
												}
								

											});

										});

							 		}, 150);
								}
								else
								{
									jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "Unable to process request. Please try again." 
									};

									res.status(statRes).json(jsonRes);
									res.end();
								}
				
							});
						}
						else
						{
							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Invalid status provided." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}

					}
					else if(parseInt(reqParams.flag) === 3)
					{

						HelperProfessional.updateTestimonials(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 

									HelperProfessional.getMyTestimonialsInbox(reqParams, function(inboxList){

										HelperProfessional.getMyTestimonials(reqParams, function(retList){

											if(retList!=null)
											{
												jsonRes = {
													success: '1',
													result: {
														inbox_list:inboxList,
														testimonial_list:retList
													},
													successMessage: 'Testimonial is successfully deleted.',
													errorMessage: errorMessage 
												};

												res.status(201).json(jsonRes);
												res.end();	
											}
											else
											{

												jsonRes = {
													success: '1',
													result: {
														inbox_list:inboxList,
														testimonial_list:[]
													},
													successMessage: 'No result found.',
													errorMessage: errorMessage 
												};

												res.status(201).json(jsonRes);
												res.end();
											}
							

										});

									});

						 		}, 150);
							}
							else
							{
								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
			
						});

					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Invalid flag provided." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
				}		
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});



router.post('/professional/get_title_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	//check('title_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		account_type    : req.body.account_type,
		//title_id          : req.body.title_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{
					HelperProfessional.__getProfessionalTitleRequest(reqParams, function(retTitle){

						if(retTitle!=null)
						{
							jsonRes = {
								success: '1',
								result: {title_list:retTitle},
								successMessage: 'Title List',
								errorMessage: errorMessage 
							};

							res.status(201).json(jsonRes);
							res.end();	

						}
						else
						{

							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "No result found." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}
					});
				}		
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Followers Module Start ---------------------------------------------------------------------------//

router.post('/professional/followers_group_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('account_type').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		account_type   : req.body.account_type,
		id           : req.body.id,
		title        : req.body.title,
		type         : req.body.type,
		flag         : req.body.flag
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	

			HelperProfessional.__checkAccountParameters(reqParams, function(retAccount){

				if(parseInt(retAccount) === 1)
				{

					if(parseInt(reqParams.flag) === 0)
					{
						HelperProfessional.getProfessionalFollowersList(reqParams, function(follow_list){

							jsonRes = {
								success: '1',
								result: {'followers_list' : follow_list},
								successMessage: 'Professional Follow List',
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();

						});
					}
					else if(parseInt(reqParams.flag) === 1)
					{

						HelperProfessional.addGroups(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 


									HelperProfessional.getProfessionalFollowersList(reqParams, function(follow_list){

										jsonRes = {
											success: '1',
											result: {'followers_list' : follow_list},
											successMessage: 'Group is successfully created.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();

									});

								}, 1000);

						
							}
							else
							{

								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}

						});

					}
					else if(parseInt(reqParams.flag) === 3)
					{
						
						HelperProfessional.deleteGroups(reqParams, function(retStatus){

							if(parseInt(retStatus) === 1)
							{

								setTimeout(function(){ 


									HelperProfessional.getProfessionalFollowersList(reqParams, function(follow_list){

										jsonRes = {
											success: '1',
											result: {'followers_list' : follow_list},
											successMessage: 'Group is successfully created.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();

									});

								}, 1000);

						
							}
							else
							{

								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}

						});
					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Invalid flag provided." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}

				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid account details provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});



//------------------------------------------------------------------- professional communication  ---------------------------------------------------------------------------//

router.post('/professional/communication', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('prof_id').isNumeric(),
	check('flag').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		prof_id    : req.body.prof_id,
		jaze_diary : req.body.jaze_diary,
		jaze_feed  : req.body.jaze_feed,
		jaze_call  : req.body.jaze_call,
		jaze_chat  : req.body.jaze_chat,
		jaze_mail  : req.body.jaze_mail,
		flag       : req.body.flag,
	};


	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	

			if(parseInt(reqParams.flag) === 1)
			{
				HelperProfessional.getProfessionalCommunication(reqParams, function(retResult){

					jsonRes = {
						success: '1',
						result: {
							communication:retResult
						},
						successMessage: 'company communication',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				});
			}
			else if(parseInt(reqParams.flag) === 2)
			{

				HelperProfessional.updateProfessionalCommunication(reqParams, function(retComu){


					if(parseInt(retComu) === 1)
					{

						setTimeout(function(){ 


							HelperProfessional.getProfessionalCommunication(reqParams, function(retResult){

								jsonRes = {
									success: '1',
									result: {
										communication:retResult
									},
									successMessage: 'communication is successfully updated.',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();

							});


						}, 3000);

				
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process request. Please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}

				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Professional sign up  ---------------------------------------------------------------------------//

router.post('/professional/professional_signup', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('account_type').isNumeric()


], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {		
		api_token  : req.body.api_token, 
		account_id : req.body.account_id,
		account_type : req.body.account_type,
		new_account_id : parseInt(req.body.account_id)+1,

		title_id   : req.body.title_id,
		first_name : req.body.first_name,
		last_name  : req.body.last_name,
		email      : req.body.email,
		dob        : req.body.dob,
		profession : req.body.profession,
		nationality_id : req.body.nationality_id,

		mobile_country_code : req.body.mobile_country_code,
		mobile_number  : req.body.mobile_number,
		mobile_country_code_2 : req.body.mobile_country_code_2,
		mobile_number_2  : req.body.mobile_number_2,

		country_id     : req.body.country_id,
		state_id       : req.body.state_id,
		city_id        : req.body.city_id,
		area_id        : req.body.area_id,

		street_name    : req.body.street_name,
		street_no      : req.body.street_no,
		building_name  : req.body.building_name,
		building_no    : req.body.building_no

	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	

			HelperProfessional.professionalSignup(reqParams, function(retStatus){

				if(parseInt(retStatus)==1){

					jsonRes = {	
						success: '1',
						result: {
							prof_id:reqParams.new_account_id.toString()
						},
						successMessage: 'Professional account is successfully created.',
						errorMessage: errorMessage 
					};
			
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
			  		};

		  			res.status(statRes).json(jsonRes);
		 			res.end();
				}

			});
		
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});



router.post('/professional/professional_signup_id_mail', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('prof_id').isNumeric(),
	check('jazemail_id').isLength({ min: 1 }),
	check('password').isLength({ min: 1 }),
	check('confirm_password').isLength({ min: 1 }),
	check('security_pin').isLength({ min: 6 }),
	check('confirm_security_pin').isLength({ min: 6 })


], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {	

		api_token    : req.body.api_token, 
		account_id   : req.body.account_id,
		account_type : req.body.account_type,
		prof_id      : req.body.prof_id,

		jazenet_id         : req.body.jazenet_id,
		password           : crypto.createHash('md5').update(req.body.password).digest("hex"),
		confirm_password   : crypto.createHash('md5').update(req.body.confirm_password).digest("hex"),

		security_pin         : crypto.createHash('md5').update(req.body.security_pin).digest("hex"),
		confirm_security_pin : crypto.createHash('md5').update(req.body.confirm_security_pin).digest("hex"),

	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	

			HelperProfessional.professionalSignupJazemail(reqParams, function(retStatus){

				if(parseInt(retStatus)==1)
				{

					jsonRes = {	
						success: '1',
						result: {
							prof_id     : reqParams.prof_id.toString(),
							jazenet_id : reqParams.jazenet_id
						},
						successMessage: 'Professional jazenet ID is successfully created.',
						errorMessage: errorMessage 
					};
			
					res.status(201).json(jsonRes);
					res.end();

				}
				else if(parseInt(retStatus)==0)
				{

					jsonRes = {
						success: '0',
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
			  		};

		  			res.status(201).json(jsonRes);
		 			res.end();
				}
				else
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: retStatus,
						errorMessage: '' 
			  		};

		  			res.status(201).json(jsonRes);
		 			res.end();
				}

			});
		
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

//------------------------------------------------------------------- Professional sign up  ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Professional Jazenet ID -------------------------------------------------------------------------//


router.post('/professional/generate_jazenet_id', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('account_type').isNumeric(),
	check('prof_id').isNumeric(),
	check('prof_type').isNumeric(),


], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {	

		api_token    : req.body.api_token, 
		account_id   : req.body.account_id,
		account_type : req.body.account_type,
		prof_id      : req.body.prof_id,
		prof_type    : req.body.prof_type,

	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperProfessional.progessionalJaznetID(reqParams, function(retStatus){

				if(parseInt(retStatus) === 0)
				{
					jsonRes = {	
						success: '1',
						result: {
							jazenet_id : []
						},
						successMessage: '',
						errorMessage: 'Unable to process request. Please try again.' 
					};
			
					res.status(201).json(jsonRes);
					res.end();
				}
				else if(parseInt(retStatus) === 2)
				{
					jsonRes = {	
						success: '1',
						result: {
							jazenet_id : []
						},
						successMessage: '',
						errorMessage: 'Account already have jazenet_id.' 
					};
			
					res.status(201).json(jsonRes);
					res.end();
				}
				else if(parseInt(retStatus) === 3)
				{
					jsonRes = {	
						success: '1',
						result: {
							jazenet_id : []
						},
						successMessage: '',
						errorMessage: 'Account not found.' 
					};
			
					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{
					
					jsonRes = {	
						success: '1',
						result: {
							jazenet_id : retStatus
						},
						successMessage: 'Professional jazenet ID.',
						errorMessage: errorMessage 
					};
			
					res.status(201).json(jsonRes);
					res.end();
				}
	

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

//------------------------------------------------------------------- Professional Jazenet ID -------------------------------------------------------------------------//

module.exports = router;
