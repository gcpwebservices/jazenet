require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();


const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperJazeProduct = require('../helpers/HelperJazeProduct.js');


const { check, validationResult } = require('express-validator');


var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};
var stat;

var statRes = 422;
var jsonRes = {};


router.post('/jazeproduct/auto',[

	check('category_id').isNumeric()



	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {category_id : req.body.category_id};


	HelperJazeProduct.getAttributeParent(arrParams, function(retChilds){
	
		if(retChilds!=null)
		{	

			HelperJazeProduct.getCategoryAttributes(arrParams, function(retCategory){

				if(retCategory !== null){
					success = '1';
					successMessage = 'Jazeproduct Auto';
					errorMessage = '';
					stat = 201;
					results = {
						portal_category_titles:retChilds,
						controllers:retCategory
					};

				}else{
					success = '0';
					successMessage = 'No result found.';
					errorMessage = '';
					stat = statRes;
					results = {};
				}

				jsonRes = {
					success: success,
					result: results,
					successMessage: successMessage,
					errorMessage: "" 
		 		};
				res.status(stat).json(jsonRes);
				res.end();

			});

		}else{

			jsonRes = {
				success: success,
				result: {},
				successMessage: 'No result found.',
				errorMessage: "" 
	 		};
			res.status(statRes).json(jsonRes);
			res.end();
		}

	});


});

router.post('/jazeproduct/search_auto',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),

	check('city_id').isNumeric(),
	check('state_id').isNumeric(),
	check('country_id').isNumeric(),

	check('category_id').isNumeric(),

	check('page').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		city_id      : req.body.city_id,
		state_id     : req.body.state_id,
		country_id   : req.body.country_id,

		category_id  : req.body.category_id,
		attributes   : req.body.attributes,
		page         : req.body.page
		
	};

	// HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

	// 	if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
	// 	{

	HelperJazeProduct.searchAuto(arrParams, function(retSearch,pagination_details){

		if(retSearch !=null){
			jsonRes = {
				success: '1',
				result: {
					pagination_details:pagination_details,
					searched_auto:retSearch
				},
				successMessage: 'Searched result.',
				errorMessage: "" 
				};
			res.status(201).json(jsonRes);
			res.end();

		}else{

			jsonRes = {
				success: '0',
				result: {
					pagination_details:pagination_details,
					searched_auto:[]
				},
				successMessage: '',
				errorMessage: "No result found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

	// 	}
	// 	else
	// 	{
	// 		jsonRes = {
	// 			success: '0',
	// 			result: {},
	// 			successMessage: successMessage,
	// 			errorMessage: "Device not found." 
	// 		};

	// 		res.status(statRes).json(jsonRes);
	// 		res.end();
	// 	}

	// });



});


router.post('/jazeproduct/property',[

	check('category_id').isNumeric(),
	check('country_id').isNumeric(),
	check('state_id').isNumeric(),
	check('city_id').isNumeric()



	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		category_id : req.body.category_id,
		country_id  : req.body.country_id,
		state_id    : req.body.state_id,
		city_id     : req.body.city_id
	};


	HelperJazeProduct.getAttributeParentProperty(arrParams, function(retChilds){


	
		if(retChilds!=null)
		{	

			HelperJazeProduct.getCategoryAttributesProperty(arrParams, function(retCategory){

	
				if(retCategory !== null){
					success = '1';
					successMessage = 'Jazeproduct Property';
					errorMessage = '';
					stat = 201;
					results = {
						portal_category_titles:retChilds,
						controllers:retCategory
					};

				}else{
					success = '0';
					successMessage = 'No result found.';
					errorMessage = '';
					stat = statRes;
					results = {};
				}

				jsonRes = {
					success: success,
					result: results,
					successMessage: successMessage,
					errorMessage: "" 
		 		};
				res.status(stat).json(jsonRes);
				res.end();

			});

		}else{

			jsonRes = {
				success: success,
				result: {},
				successMessage: 'No result found.',
				errorMessage: "" 
	 		};
			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazeproduct/search_property',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),

	check('city_id').isNumeric(),
	check('state_id').isNumeric(),
	check('country_id').isNumeric(),

	check('category_id').isNumeric(),

	check('page').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		city_id      : req.body.city_id,
		state_id     : req.body.state_id,
		country_id   : req.body.country_id,

		category_id  : req.body.category_id,
		attributes   : req.body.attributes,
		page         : req.body.page
		
	};


	HelperJazeProduct.searchProperty(arrParams, function(retSearch,pagination_details){

		if(retSearch !=null){
			jsonRes = {
				success: '1',
				result: {
					pagination_details:pagination_details,
					searched_property:retSearch
				},
				successMessage: 'Searched result.',
				errorMessage: "" 
				};
			res.status(201).json(jsonRes);
			res.end();

		}else{

			jsonRes = {
				success: '0',
				result: {
					pagination_details:pagination_details,
					searched_property:[]
				},
				successMessage: '',
				errorMessage: "No result found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazeproduct/property_details',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('product_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		product_id   : req.body.product_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazeProduct.getPropertyDetails(arrParams, function(retDetails){
			
				if(retDetails!=null)
				{	
					jsonRes = {
						success: '1',
						result: {
							property_details:retDetails
						},
						successMessage: 'Property Details.',
						errorMessage: "" 
						};
					res.status(201).json(jsonRes);
					res.end();


				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
			 		};
					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});

});


router.post('/jazeproduct/auto_details',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('product_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		product_id   : req.body.product_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazeProduct.getAutoDetails(arrParams, function(retDetails){
			
				if(retDetails!=null)
				{	
					jsonRes = {
						success: '1',
						result: {
							auto_details:retDetails
						},
						successMessage: 'Auto Details.',
						errorMessage: "" 
						};
					res.status(201).json(jsonRes);
					res.end();


				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
			 		};
					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazeproduct/product_info',[

	check('account_id').isNumeric(),
	check('api_token').isLength({ min: 1 }),
	check('product_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		account_id   : req.body.account_id,
		api_token    : req.body.api_token,
		product_id   : req.body.product_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazeProduct.getProductInfo(arrParams, function(retDetails){
			
				if(retDetails!=null)
				{	
					jsonRes = {
						success: '1',
						result: {
							product_info:retDetails
						},
						successMessage: 'Product Information.',	
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();


				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
			 		};
					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazeproduct/product_reviews',[

	check('product_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		product_id   : req.body.product_id,
		flag         : 2,
	};

	HelperJazeProduct.getProductReviews(arrParams, function(retDetails){
	
		if(retDetails!=null)
		{	
			jsonRes = {
				success: '1',
				result: {
					product_reviews:retDetails
				},
				successMessage: 'Product Reviews',	
				errorMessage: "" 
			};
			res.status(201).json(jsonRes);
			res.end();


		}else{

			jsonRes = {
				success: success,
				result: {},
				successMessage: 'No result found.',
				errorMessage: "" 
	 		};
			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazeproduct/product_filter',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('product_id').optional({ checkFalsy: true }).isInt({ min: 1 }),
	check('master_id').optional({ checkFalsy: true }).isInt({ min: 1 }),
	check('product_config').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		product_id   : req.body.product_id,
		master_id    : req.body.master_id,
		product_config   : req.body.product_config,
		country_id     : req.body.country_id,
		state_id       : req.body.state_id,
		city_id        : req.body.city_id
	};

	HelperJazeProduct.getProductFilter(arrParams, function(retDetails){
	
		if(retDetails!=null)
		{	
			jsonRes = {
				success: '1',
				result: {
					product_info:retDetails
				},
				successMessage: 'Product Information.',	
				errorMessage: "" 
			};
			res.status(201).json(jsonRes);
			res.end();


		}else{

			jsonRes = {
				success: success,
				result: {},
				successMessage: 'No result found.',
				errorMessage: "" 
	 		};
			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


module.exports = router;