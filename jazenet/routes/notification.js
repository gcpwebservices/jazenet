require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperNotification = require('../helpers/HelperNotification.js');
const { check, validationResult } = require('express-validator');


var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};

//-------------------------------------------------------------------  Home Notification List Module Start ---------------------------------------------------------------------------//

router.post('/notification/home_notification', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg  
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		date       : req.body.date,
		status     : req.body.status
	};

	// flag:
	// call  = 1
	// diary = 2
	// mail  = 3
	// feed  = 4
	// jobs  = 5
	// notice  = 6 { notice  = 6, follow = 7, team = 8 }
	// pay  = 9

	/* 
	 * Sort:
	 */
	// 1 : mail
	// 2 : call
	// 3 : feed
	// 4 : pay
	// 5 : notice
	// 6 : diary
	// 7 : jobs
	

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperNotification.getHomeNotificationList(reqParams, function(notification_list){

				jsonRes = {
					success: '1',
					result: notification_list,
					successMessage: 'Jazeproduct Property',
					errorMessage: "" 
				};

				res.status(201).json(jsonRes);
				res.end();
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//-------------------------------------------------------------------  Home Notification List Module End ---------------------------------------------------------------------------//
//-------------------------------------------------------------------  Update Status notification Module Start ---------------------------------------------------------------------------//

router.post('/notification/update_read_status',[
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('id','Invalid id').isNumeric(),
	check('group_id','Invalid group id').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg  
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		id         : req.body.id,
		group_id   : req.body.group_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperNotification.updateReadStatus(reqParams, function(updateRet){

				if(updateRet !== null)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Status is successfully updated.',
						errorMessage: errorMessage 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
				else 
				{
					jsonRes = {
						success: '0',
						result: {},
						successMessage: 'Unable to process request. Please try again.',
						errorMessage: errorMessage 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});

});

//-------------------------------------------------------------------  Update Status notification Module End ---------------------------------------------------------------------------//


module.exports = router;