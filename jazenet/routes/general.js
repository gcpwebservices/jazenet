require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperDateTime = require('../helpers/HelperDateTime.js');
const HelperWeather = require('../helpers/HelperWeather.js');
const HelperEmergency = require('../helpers/HelperEmergency.js');
const HelperFavorites = require('../helpers/HelperFavorites.js');
const HelperWeblinks = require('../helpers/HelperWeblinks.js');
const HelperJazeCategory = require('../helpers/HelperJazeCategory.js');



const HelperGeneral = require('../helpers/HelperGeneral.js');

const { check, validationResult } = require('express-validator');


var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};

//------------------------------------------------------------------- Time Info  Module Start  ---------------------------------------------------------------------------//

router.post('/general/search_city', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('key','Invalid key').isLength({ min: 1 })
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage:  errors.array()[0].msg  
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		key	   	   : req.body.key,
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperDateTime.searchTimeInfoCity(reqParams, function(retSearched){

				if(retSearched!=null)
				{
					jsonRes = {
						success: '1',
						result: { city_list:retSearched },
						successMessage: 'City List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

router.post('/general/time_info', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('master_city_id','Invalid master city id').isInt({ min: 1 }),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg  
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		master_city_id : req.body.master_city_id
	};
	
	HelperRestriction.checkParametersApiToken(reqParams, function(queParam)
	{
		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperDateTime.getMasterTimeInfo(reqParams, function(master_time)
			{
				HelperDateTime.getChildTimeInfo(reqParams, function(child_list)
				{
					jsonRes = {
						success: '1',
						result: { master_time:master_time, child_time:child_list },
						successMessage: 'Time Info',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				});
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

router.post('/general/update_time', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('master_city_id','Invalid master city id').isInt({ min: 1 }),
	check('child_city_id','Invalid child city id').isInt({ min: 1 }),
	check('sort_order','Invalid sort order').isInt({ min: 1 }),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg  
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		city_id	   : req.body.child_city_id,
		master_city_id : req.body.master_city_id,
		sort_order : req.body.sort_order
	};
	
	HelperRestriction.checkParametersApiToken(reqParams, function(queParam)
	{
		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			if(parseInt(reqParams.account_id) !==0)
			{
				HelperDateTime.saveUserPersonaliseCityList(reqParams, function(updateStatus){

					if(parseInt(updateStatus.status) === 1)
					{
						HelperDateTime.getMasterTimeInfo(reqParams, function(master_time)
						{
							HelperDateTime.getChildTimeInfo(reqParams, function(child_list)
							{ 
								jsonRes = {
									success: '1',
									result: { master_time:master_time, child_time:child_list },
									successMessage: 'successfully updated list',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();

							});
						});
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: updateStatus.message
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Please login to your account to use this feature." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Time Info  Module End  ---------------------------------------------------------------------------//
//------------------------------------------------------------------- Weather Info  Module Start  ---------------------------------------------------------------------------//

router.post('/general/weather_info', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('city_id','Invalid api token').isInt({ min: 1 })
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg 
  		};
		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		city_id	   : req.body.city_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperWeather.getWeatherDetails(reqParams.city_id, function(weather_details)
			{
					jsonRes = {
						success: '1',
						result:  weather_details ,
						successMessage: 'Weather info',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});
//------------------------------------------------------------------- Weather Info  Module End  ---------------------------------------------------------------------------//
//------------------------------------------------------------------- Favorites list  Module Start  ---------------------------------------------------------------------------//

router.post('/general/favorites_list_new', [
	check('account_id','Invalid account id').isInt({ min: 1 }),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperFavorites.getFavoritesList(reqParams, function(favorites_list)
			{
				if(favorites_list!=null)
				{
					jsonRes = {
						success: '1',
						result: {favorites_list:favorites_list},
						successMessage: 'Favorites List',
						errorMessage: errorMessage 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {		
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found" 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

router.post('/general/favorites_list', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperFavorites.getFavoritesListAllDetails(reqParams, function(favorites_list)
			{
				if(favorites_list!=null)
				{
					jsonRes = {
						success: '1',
						result: {favorites_list:favorites_list},
						successMessage: 'Favorites List',
						errorMessage: errorMessage 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {		
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found" 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

router.post('/general/add_to_favorites',[
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('fav_account_id','Invalid favorite account id').isInt({ min: 1 }),
	check('fav_account_type','Invalid favorite account type').isInt({ min: 1 }),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg  
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		account_id       :req.body.account_id,
		api_token        :req.body.api_token,
		fav_account_id   :req.body.fav_account_id,
		fav_account_type :req.body.fav_account_type
	};
	
	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperFavorites.addToFavorites(arrParams, function(retSaveStat)
			{
				if(parseInt(retSaveStat) === 1)
				{
					HelperFavorites.getFavoritesList(arrParams, function(favorites_list)
					{
						if(favorites_list!=null)
						{
							jsonRes = {
								success: '1',
								result: {favorites_list:favorites_list},
								successMessage: 'Account is successfully added to favorites',
								errorMessage: errorMessage 
							};

							res.status(201).json(jsonRes);
							res.end();
						}
						else
						{
							jsonRes = {		
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "No result found" 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}
					});
				}
				else if(parseInt(retSaveStat) === 2)
				{
					jsonRes = {		
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "account already exists, please choose another one." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {		
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

router.post('/general/remove_to_favorites',[
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('fav_id','Invalid favorite id').isInt({ min: 1 })
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
			account_id	: req.body.account_id,
			api_token	: req.body.api_token,
			fav_id		: req.body.fav_id
		};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperFavorites.removeToFavorites(arrParams, function(retDelStatus)
			{
				if(parseInt(retDelStatus) == 1)
				{
					HelperFavorites.getFavoritesList(arrParams, function(favorites_list)
					{
						if(favorites_list!=null)
						{
							jsonRes = {
								success: '1',
								result: {favorites_list:favorites_list},
								successMessage: 'Account is successfully removed from favorites list.',
								errorMessage: errorMessage 
							};

							res.status(201).json(jsonRes);
							res.end();
						}
						else
						{
							jsonRes = {		
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "No result found" 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}
					});
				}
				else
				{
					jsonRes = {		
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

//------------------------------------------------------------------- Favorites list  Module End  ---------------------------------------------------------------------------//
//------------------------------------------------------------------- Weblinks list  Module Start  ---------------------------------------------------------------------------//

router.post('/general/weblinks', [
	check('account_id','Invalid account id').isInt({ min: 1 }),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('flag','Invalid flag').isLength({ min: 1 })
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		id 		   : req.body.id,
		name       : req.body.name,
		link       : req.body.link,
		flag       : req.body.flag
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperWeblinks.getWeblink(reqParams, function(status,retList,success_message,error_message){

				if(parseInt(status) === 1)
				{
					jsonRes = {
						success: '1',
						result: retList,
						successMessage: success_message,
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {		
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: error_message 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
 			jsonRes = {		
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

//------------------------------------------------------------------- Weblinks list  Module End  ---------------------------------------------------------------------------//
//------------------------------------------------------------------- city information  Module Start  ---------------------------------------------------------------------------//


router.post('/general/city_information', [
	//check('account_id','Invalid account id').isNumeric(),
	//check('api_token','Invalid api token').isLength({ min: 1 }),
	check('category_id','Invalid category id').isNumeric(),
	check('country_id','Invalid country id').isLength({ min: 1 }),
	check('state_id','Invalid state id').isLength({ min: 1 }),
	check('city_id','Invalid city id').isLength({ min: 1 })
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg   
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		category_id  : req.body.category_id,
		country_id   : req.body.country_id,
		state_id     : req.body.state_id,
		city_id      : req.body.city_id
	};

	// 0 = sub category
	// 1 = listing
	// 2 = no child
	// 3 = special 

	// HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

	// 	if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
	// 	{
			HelperJazeCategory.getCityInformation(reqParams, function(city_list)
			{
				if( city_list !== null )
				{	
					if(parseInt(reqParams.category_id) == 0)
					{
						HelperEmergency.getEmergencyDetails(reqParams, function(emergency_list){
				
							jsonRes = {
								success: '1',
								result: {
									emergency_info:emergency_list,
									city_information:city_list
								},
								successMessage: 'City Information',
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();
				
						});
					}
					else
					{

						jsonRes = {
							success: '1',
							result: {
								emergency_info:[],
								city_information:city_list
							},
							successMessage: 'City Information',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}


				}
				else
				{
					jsonRes = {		
						success: '1',
						result: {
							emergency_info:[],
							city_information:[]
						},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
					
				}
			});
	// 	}
	// 	else
	// 	{
	// 		jsonRes = {
	// 			success: success,
	// 			result: {},
	// 			successMessage: successMessage,
	// 			errorMessage: "Device not found." 
	// 		};

	// 		res.status(statRes).json(jsonRes);
	// 		res.end();

	// 	}
	// });
});


router.post('/general/city_information_company', [
	// check('account_id','Invalid account id').isNumeric(),
	// check('api_token','Invalid api token').isLength({ min: 1 }),
	check('category_id','Invalid category id').isInt(),
	check('country_id','Invalid country id').isInt(),
	check('state_id','Invalid state id').isInt(),
	check('city_id','Invalid city id').isInt(),
	check('latitude','Invalid latitude').isLength({ min: 1 }),
	check('longitude','Invalid longitude').isLength({ min: 1 }),
	check('flag','Invalid flag').isInt(),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		category_id : req.body.category_id,
		country_id  : req.body.country_id,
		state_id    : req.body.state_id,
		city_id     : req.body.city_id,
		latitude    : req.body.latitude,
		longitude   : req.body.longitude,
		flag        : req.body.flag
	};

	// flag = 1 list
	// flag = 2 details

	// HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

	// 	if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
	// 	{
			if(parseInt(reqParams.flag) == 1)
			{
				HelperJazeCategory.getCityInformationCompany(reqParams, function(retList)
				{
					if( retList !== null )
					{	
						jsonRes = {
							success: '1',
							result: {city_information_company:retList},
							successMessage: 'City Information Company List',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {		
							success: '1',
							result: {city_information_company:[]},
							successMessage: "No result found.",
							errorMessage: errorMessage 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
				});
			}
			else if(parseInt(reqParams.flag) == 2)
			{

				HelperJazeCategory.getCityInformationCompanyDetails(reqParams, function(retDetails){

					if( retDetails !== null ){	

						jsonRes = {
							success: '1',
							result: {city_information_company_details:retDetails},
							successMessage: 'City Information Company Details',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
				
					}else{

						jsonRes = {		
							success: '1',
							result: {city_information_company_details:[]},
							successMessage: "No result found.",
							errorMessage: errorMessage 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
				});

			}
			else
			{
				jsonRes = {		
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
	// 	}
	// 	else
	// 	{
	// 		jsonRes = {
	// 			success: success,
	// 			result: {},
	// 			successMessage: successMessage,
	// 			errorMessage: "Device not found." 
	// 		};

	// 		res.status(statRes).json(jsonRes);
	// 		res.end();
	// 	}
	// });
});

router.post('/general/emergency_info', [
	check('account_id','Invalid account id').isNumeric(),
	check('api_token','Invalid api token').isLength({ min: 1 }),
	check('city_id','Invalid city id').isInt({ min: 1 })
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: errors.array()[0].msg 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		city_id	   : req.body.city_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperEmergency.getEmergencyDetails(reqParams, function(emergency_list){

				if(emergency_list!=null)
				{
					jsonRes = {
						success: '1',
						result: {emergency_info:emergency_list},
						successMessage: 'Emergency Information',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				
				}
				else
				{
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found" 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});


router.post('/general/tourist_information', [


	check('city_id').isLength({ min: 1 }),
	check('category_id').isLength({ min: 1 })


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		category_id : req.body.category_id,
		city_id     : req.body.city_id
	};

	HelperJazeCategory.getTouristInformation(reqParams, function(retTourist,flag){

		if( retTourist !== null )
		{	

			jsonRes = {
				success: '1',
				result: {
					flag:flag,
					tourist_information:retTourist
				},
				successMessage: 'Tourist Information',
				errorMessage: errorMessage 
			};
			res.status(201).json(jsonRes);
			res.end();

		}
		else
		{
 			jsonRes = {		
				success: success,
				result: {
					flag:'0',
					tourist_information:{
						details:[],
						photos:[],
						map:[]
					}
				},
				successMessage: successMessage,
				errorMessage: "No result found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
			
		}
	});


});


//------------------------------------------------------------------- city information  Module End  ---------------------------------------------------------------------------//














router.post('/general/government', [


	check('country_id').isLength({ min: 1 }),
	check('state_id').isLength({ min: 1 }),
	check('city_id').isLength({ min: 1 }),
	check('flag').isLength({ min: 1 }),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		

		country_id  : req.body.country_id,
		state_id    : req.body.state_id,
		city_id     : req.body.city_id,
		flag        : req.body.flag,
		latitude    : req.body.latitude,
		longitude   : req.body.longitude
	};

	// flag = 1 Government List
	// flag = 2 Government Company List

	if(parseInt(reqParams.flag) == 1)
	{
		HelperJazeCategory.getCityInformationGovernment(reqParams, function(retList){

			if( retList !== null ){	

				jsonRes = {
					success: '1',
					result: {government_list:retList},
					successMessage: 'Government List',
					errorMessage: errorMessage 
				};
				res.status(201).json(jsonRes);
				res.end();
		
			}else{

	 			jsonRes = {		
					success: '1',
					result: {government_list:[]},
					successMessage: successMessage,
					errorMessage: "No result found." 
				};

				res.status(201).json(jsonRes);
				res.end();
			}

		});

	}
	else if(parseInt(reqParams.flag) == 2)
	{

		HelperJazeCategory.getGeoIDList(reqParams, function(retDetails){

			if( retDetails !== null ){	

				jsonRes = {
					success: '1',
					result: {government_company_list:retDetails},
					successMessage: 'Government Company List',
					errorMessage: errorMessage 
				};
				res.status(201).json(jsonRes);
				res.end();
		
			}else{

	 			jsonRes = {		
					success: '1',
					result: {government_company_list:[]},
					successMessage: successMessage,
					errorMessage: "No result found." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
		});

	}
	else
	{
		jsonRes = {		
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "Invalid flag provided." 
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

});


router.post('/general/government_notice', [


	check('country_id').isLength({ min: 1 }),
	check('state_id').isLength({ min: 1 }),
	check('city_id').isLength({ min: 1 }),
	check('page').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {
		country_id  : 0,
		state_id    : 0,
		city_id     : 0,
		page        : req.body.page
	};

	if(parseInt(req.body.country_id) !==0 
		&& parseInt(req.body.state_id) ==0 
		&& parseInt(req.body.city_id) ==0 ){
		reqParams = {		
			country_id  : req.body.country_id,
			state_id    : 0,
			city_id     : 0,
			page        : req.body.page
		};
	}
	else if(parseInt(req.body.country_id) !==0 
		&& parseInt(req.body.state_id) !==0 
		&& parseInt(req.body.city_id) ==0 ){
		reqParams = {		
			country_id  : 0,
			state_id    : req.body.state_id,
			city_id     : 0,
			page        : req.body.page
		};
	}
	else if(parseInt(req.body.country_id) !==0 
		&& parseInt(req.body.state_id) !==0 
		&& parseInt(req.body.city_id) !==0 ){
		reqParams = {		
			country_id  : 0,
			state_id    : 0,
			city_id     : req.body.city_id,
			page        : req.body.page
		};
	}
	else{
		reqParams = {		
			country_id  : 0,
			state_id    : 0,
			city_id     : 0,
			page        : req.body.page
		};
	}
	
	HelperJazeCategory.getGovernmentNotice(reqParams, function(retList,pagination_details){

		if(retList !== null){	

			jsonRes = {
				success: '1',
				result: {
					pagination_details:pagination_details,
					government_notice:retList
				},
				successMessage: 'Government Notice',
				errorMessage: errorMessage 
			};
			res.status(201).json(jsonRes);
			res.end();
	
		}else{

 			jsonRes = {		
				success: '1',
				result: {
					pagination_details:pagination_details,
					government_notice:[]
				},
				successMessage: successMessage,
				errorMessage: "No result found." 
			};

			res.status(201).json(jsonRes);
			res.end();
		}
	});

});




// ------------------------------------------------------ Home City List Module Start   --------------------------------------------------------------//  

router.post('/general/get_home_city', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		flag 		: 0
	};

	HelperRestriction.checkParametersApiTokenJazenetJazecom(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperGeneral.getHomeCity(reqParams, function(retResult){

				if(retResult!=null){

					jsonRes = {
						success: '1',
						result: {
							city_home_list:retResult
						},
						successMessage: 'City Home List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();


				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

router.post('/general/get_home_city_demo', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		flag 		: 1
	};

	HelperRestriction.checkParametersApiTokenJazenetJazecom(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperGeneral.getHomeCity(reqParams, function(retResult){

				if(retResult!=null){

					jsonRes = {
						success: '1',
						result: {
							city_home_list:retResult
						},
						successMessage: 'City Home List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();


				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

// ------------------------------------------------------ Home City List Module End   --------------------------------------------------------------// 

// ------------------------------------------------------ General Search Module Start  --------------------------------------------------------------//   


router.post('/general/jazemainsearch', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
    check('page').isNumeric(),
    check('country').isNumeric(),
    check('state').isNumeric(),
	check('city').isNumeric(),
	check('geo_type').isNumeric(),
	check('category').isNumeric(),
	check('geo_child_id').isNumeric(),
    
], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id	: req.body.account_id,
        keyword 	: req.body.keyword,
        api_token	: req.body.api_token,
        limit 		: req.body.page,
        country 	: req.body.country,
        state 		: req.body.state,
		city 		: req.body.city,
		area 		: 0,
		geo_type 	: req.body.geo_type,
		category 	: req.body.category,
		geo_child_id 	: req.body.geo_child_id
	};
	// console.log('reqParams',reqParams);
	// category  :  1= company,  2 = profile,  3 = products,  4 = services,  5 = keywords  

	// geo_type  :  1= international,  2 = national,  3 = local,  4 = selected state result,  5 = all result without current country,  6 = all result without current state
 
	// geo_child_id :  selected state id

	// detail_flag : 1 = company profile, 2 = profile, 3 = products category, 4 = products listing, 5 = products details, 6 = service category, 7 = service listing, 8 = Government Company, 9 = auto, 10 = property, 
	//               11 =  keyword company listing, 12 =  keyword company profile, 13 =  keyword products listing, 14 =  keyword products detail, 15 =  work profile,
	//               16 =  grocery products listing,  17 =  grocery products detail,  18 =  restaurants products listing,  19 =  restaurants products detail,

	var retObject = {
			search_param           : {},
			pagination_details     : {},
			category_count         : {},
			type_count		   	   : {},
			search_result_general  : {}
		};

	HelperRestriction.checkParametersApiToken(reqParams, function (queParamRes) {

        if (typeof queParamRes !== 'undefined' && queParamRes !== null && parseInt(queParamRes.length) !== 0)
        {  
			HelperGeneral.getCountResult(reqParams,function(count_category) {

				retObject.category_count = count_category;
				
				if(parseInt(reqParams.category) === 1)
				{
					HelperGeneral.getCompanyLocalCount(reqParams,function(type_count) {
						
						//Company
						HelperGeneral.getAutofillCompanyResults(reqParams,function(comp_results) {

							//Company International or National
							HelperGeneral.getAutofillCompanyInternationalNationalResults(reqParams,function(inter_result,national_result) {

								var company_result = [];

								var pagination_details  = {
									total_page   : "0",
									current_page : "0",
									next_page    : "0"
								};

								var status = 0;

								if(comp_results.result !== null)
								{ 
									company_result = sort_name_key(comp_results.result,reqParams.keyword);

									var next_page = (parseInt(reqParams.limit) + 1);

									if(parseInt(comp_results.tot_page) <= parseInt(next_page))
									{ next_page = "0"; }

									  pagination_details  = {
											total_page   : comp_results.tot_page.toString(),
											current_page : reqParams.limit.toString(),
											next_page    : next_page.toString()
										};

									status = 1;	
								}


								if(inter_result !== null || national_result !== null)
								{ status = 1; }


								if(parseInt(status) === 1)
								{
									var search_param = {
											geo_type           : reqParams.geo_type,
											category           : reqParams.category,
											keyword            : reqParams.keyword,
											country            : reqParams.country,
											state              : reqParams.state,
											city               : reqParams.city
									} 
					
									var result = {
										company_result       : company_result,
										prof_result          : [],
										prod_result          : [],
										services_result      : [],
										keywords_result      : [],
										international_result : inter_result,
										national_result      : national_result
									}

									retObject.search_param = search_param;		

									retObject.pagination_details = pagination_details;		

									retObject.type_count = type_count;		

									retObject.search_result_general = result;		
									
									jsonRes = {
										success: '1',
										result: retObject,
										successMessage: "General Search Result",
										errorMessage: "" 
									};		
															
									res.status(201).json(jsonRes);
									res.end();	
								}
								else
								{
									jsonRes = {
										success: '0',
										result: {},
										successMessage: "",
										errorMessage: "no result found" 
									};		
															
									res.status(201).json(jsonRes);
									res.end();	
								}	
							});		
						});
					});
				}
				else if(parseInt(reqParams.category) === 2)
				{  
					HelperGeneral.getProfileLocalCount(reqParams,function(type_count) {
						
						//Profile
						HelperGeneral.getAutofillProfileResults(reqParams,function(prof_results) {
							
							//Profile International or National
							HelperGeneral.getAutofillProfileInternationalNationalResults(reqParams,function(inter_result,national_result) {

								var profile_result = [];

								var pagination_details  = {
									total_page   : "0",
									current_page : "0",
									next_page    : "0"
								};

								var status = 0;

								if(prof_results.result !== null)
								{ 
									profile_result = prof_results.result;

									var next_page = (parseInt(reqParams.limit) + 1);

									if(parseInt(prof_results.tot_page) <= parseInt(next_page))
									{ next_page = "0"; }

									  pagination_details  = {
											total_page   : prof_results.tot_page.toString(),
											current_page : reqParams.limit.toString(),
											next_page    : next_page.toString()
										};

									status = 1;	
								}


								if(inter_result !== null || national_result !== null)
								{ status = 1; }


								if(parseInt(status) === 1)
								{
									var search_param = {
											geo_type           : reqParams.geo_type,
											category           : reqParams.category,
											keyword            : reqParams.keyword,
											country            : reqParams.country,
											state              : reqParams.state,
											city               : reqParams.city
									} 
					
									var result = {
										company_result       : [],
										prof_result          : profile_result,
										prod_result          : [],
										services_result      : [],
										keywords_result      : [],
										international_result : inter_result,
										national_result      : national_result
									}
					
									retObject.search_param = search_param;		

									retObject.pagination_details = pagination_details;		

									retObject.type_count = type_count;		

									retObject.search_result_general = result;

									
									jsonRes = {
										success: '1',
										result: retObject,
										successMessage: "General Search Result",
										errorMessage: "" 
									};		
															
									res.status(201).json(jsonRes);
									res.end();	
								}
								else
								{
									jsonRes = {
										success: '0',
										result: {},
										successMessage: "",
										errorMessage: "no result found" 
									};		
															
									res.status(201).json(jsonRes);
									res.end();	
								}	
							});		
						});
					});
				}
				else if(parseInt(reqParams.category) === 3)
				{
					HelperGeneral.getProductLocalCount(reqParams,function(type_count) {
						
						//Products
						HelperGeneral.getAutofillProductCategoryResults(reqParams,function(prod_results) {
							
							//Products International or National
							HelperGeneral.getAutofillProductInternationalNationalResults(reqParams,function(inter_result,national_result) {

								var products_result = [];

								var pagination_details  = {
									total_page   : "0",
									current_page : "0",
									next_page    : "0"
								};

								var status = 0;

								if(prod_results.result !== null)
								{ 
									products_result = prod_results.result;

									var next_page = (parseInt(reqParams.limit) + 1);

									if(parseInt(prod_results.tot_page) <= parseInt(next_page))
									{ next_page = "0"; }

									  pagination_details  = {
											total_page   : prod_results.tot_page.toString(),
											current_page : reqParams.limit.toString(),
											next_page    : next_page.toString()
										};

									status = 1;	
								}


								if(inter_result !== null || national_result !== null)
								{ status = 1; }


								if(parseInt(status) === 1)
								{
									var search_param = {
											geo_type           : reqParams.geo_type,
											category           : reqParams.category,
											keyword            : reqParams.keyword,
											country            : reqParams.country,
											state              : reqParams.state,
											city               : reqParams.city
									} 
					
									var result = {
										company_result       : [],
										prof_result          : [],
										prod_result          : products_result,
										services_result      : [],
										keywords_result      : [],
										international_result : inter_result,
										national_result      : national_result
									}

									retObject.search_param = search_param;		

									retObject.pagination_details = pagination_details;		

									retObject.type_count = type_count;		

									retObject.search_result_general = result;		
									
									jsonRes = {
										success: '1',
										result: retObject,
										successMessage: "General Search Result",
										errorMessage: "" 
									};		
															
									res.status(201).json(jsonRes);
									res.end();	
								}
								else
								{
									jsonRes = {
										success: '0',
										result: {},
										successMessage: "",
										errorMessage: "no result found" 
									};		
															
									res.status(201).json(jsonRes);
									res.end();	
								}	
							});		
						});
					});
				}
				else if(parseInt(reqParams.category) === 4)
				{ 
					// services
					HelperGeneral.getAutofillServicesResults(reqParams,function(service_results) {
						
						if(service_results !== null)
						{
							var pagination_details  = {
								total_page   : "0",
								current_page : "0",
								next_page    : "0"
							};

							var type_count = {
								local_count      	 : "0",
								national_count       : "0",
								international_count  : "0"					                        
							};
					

							var search_param = {
								geo_type           : reqParams.geo_type,
								category           : reqParams.category,
								keyword            : reqParams.keyword,
								country            : reqParams.country,
								state              : reqParams.state,
								city               : reqParams.city
							} 
			
							var result = {
								company_result       : [],
								prof_result          : [],
								prod_result          : [],
								services_result      : service_results,
								keywords_result      : [],
								international_result : [],
								national_result      : []
							}
			
							retObject.search_param = search_param;		

							retObject.pagination_details = pagination_details;		

							retObject.type_count = type_count;		

							retObject.search_result_general = result;		
							
							jsonRes = {
								success: '1',
								result: retObject,
								successMessage: "General Search Result",
								errorMessage: "" 
							};		
													
							res.status(201).json(jsonRes);
							res.end();
						}
						else
						{
							jsonRes = {
								success: '0',
								result: {},
								successMessage: "",
								errorMessage: "no result found" 
							};		
															
							res.status(201).json(jsonRes);
							res.end();	
						}	
					});
				}
				else if(parseInt(reqParams.category) === 5)
				{
					HelperGeneral.getKeywordLocalCount(reqParams,function(type_count) {
						
						HelperGeneral.getAutofillKeywordResults(reqParams,function(key_results) {

							//Keyword International or National
							HelperGeneral.getAutofillKeywordInternationalNationalResults(reqParams,function(inter_result,national_result) {

								var keywords_result = [];

								var pagination_details  = {
									total_page   : "0",
									current_page : "0",
									next_page    : "0"
								};

								var status = 0;

								if(key_results.result !== null)
								{ 
									keywords_result = key_results.result;

									var next_page = (parseInt(reqParams.limit) + 1);

									if(parseInt(key_results.tot_page) <= parseInt(next_page))
									{ next_page = "0"; }

									pagination_details  = {
											total_page   : key_results.tot_page.toString(),
											current_page : reqParams.limit.toString(),
											next_page    : next_page.toString()
										};

									status = 1;	
								}


								if(inter_result !== null || national_result !== null)
								{ status = 1; }


								if(parseInt(status) === 1)
								{
									var search_param = {
											geo_type           : reqParams.geo_type,
											category           : reqParams.category,
											keyword            : reqParams.keyword,
											country            : reqParams.country,
											state              : reqParams.state,
											city               : reqParams.city
									} 

									var result = {
										company_result       : [],
										prof_result          : [],
										prod_result          : [],
										services_result      : [],
										keywords_result      : keywords_result,
										international_result : inter_result,
										national_result      : national_result
									}

									retObject.search_param = search_param;		

									retObject.pagination_details = pagination_details;		

									retObject.type_count = type_count;		

									retObject.search_result_general = result;		
									
									jsonRes = {
										success: '1',
										result: retObject,
										successMessage: "General Search Result",
										errorMessage: "" 
									};		
															
									res.status(201).json(jsonRes);
									res.end();	
								}
								else
								{
									jsonRes = {
										success: '0',
										result: {},
										successMessage: "",
										errorMessage: "no result found" 
									};		
															
									res.status(201).json(jsonRes);
									res.end();	
								}	
							});	

						});
					});
				}
			});
        }
        else
        {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };
            res.status(statRes).json(jsonRes);
            res.end();
        }
   });
   
});

function sort_name_key(array,keyword)
{
	return array.filter(o => o.name.toLowerCase().includes(keyword.toLowerCase()))
                  .sort((a, b) => a.name.toLowerCase().indexOf(keyword.toLowerCase()) - b.name.toLowerCase().indexOf(keyword.toLowerCase()));
}

// ------------------------------------------------------ General Search Module End  --------------------------------------------------------------// 


module.exports = router;