require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();
const request = require('request');

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperDocuments = require('../helpers/HelperDocuments.js');

const { check, validationResult } = require('express-validator');


const multer = require('multer');
const fs = require('fs');


const urlPath = G_STATIC_IMG_URL+"uploads/jazenet/documents/";
const storagePath = G_STATIC_IMG_PATH+"documents/"

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};


const fs_conversation = require('../../jazecom/firestore.js');


function getExtension(path) {
    var basename = path.split(/[\\/]/).pop(),  // extract file name from full path ...
                                             	 // (supports `\\` and `/` separators)
        pos = basename.lastIndexOf(".");       // get last position of `.`
    if (basename === "" || pos < 1)            // if file name is empty or ...
        return "";                             //  `.` not found (-1) or comes first (0)
    return basename.slice(pos + 1);            // extract extension ignoring `.`
}

function getFilename(path) {
    var basename = path.split(/[\\/]/).pop(),      
        pos = basename.split('.'); 
    if (basename === "" || pos.length < 1)           
        return "";                          
    return pos[0];            
}


function getCleanFileName(path) {

	var basename = path.split(/\.(?=[^\.]+$)/);
	var cleanString = basename[0].replace(/[|&;$%@"<>()+1234567890,]/g, "");
         
    return cleanString;           
}


var storage = multer.diskStorage({
	destination: function (req, file, callback) {
	
	function asyncFunction (item, cb) {
	  	setTimeout(() => {

			var main_path;
			var out_path = storagePath+req.body.account_id+'/';;

			if (!fs.existsSync(out_path)){
				fs.mkdirSync(out_path,'0757', true); 
			}

			if(parseInt(req.body.flag) == 1)
			{	
				main_path = storagePath+req.body.account_id+'/library/';
				if (!fs.existsSync(main_path)){
			   		fs.mkdirSync(main_path,'0757', true); 
				}
		
			}
			else if(parseInt(req.body.flag) == 2)
			{
				main_path = storagePath+req.body.account_id+'/activity/';
				if (!fs.existsSync(main_path)){
	   		 		fs.mkdirSync(main_path,'0757', true); 					
				}
						
			}

			console.log(req.body.flag);
			console.log(main_path);

			callback(null, main_path);

			cb();
	  	}, 100);
	}

	let requests = 0;

		if(req.body.account_id !=''){

			return new Promise((resolve) => {
				requests = req.body.account_id;
		  		asyncFunction(requests, resolve);
		    });

			Promise.all(requests);
		}
  },
  	filename: function (req, file, callback) {

  		var ext = getExtension(file.originalname);
  		var cleanName = getCleanFileName(file.originalname);
  		var completeFileName = req.body.account_id+'_'+Date.now()+'.'+ext;

    	callback(null, completeFileName);
  	}
});


router.post('/documents/file_uploads', function(req, res) {

	var upload = multer({
		storage: storage,
	}).single('file');

	upload(req, res, function(err) {

 	 	var extName = req.file.filename.split(".");
 	 	var filename = extName[extName.length - 2];

	 	var extArray = req.file.filename.split(".");
 	 	var extention = extArray[extArray.length - 1];

 	  	var file_properties = req.file.mimetype.split("/");

 		var base_url = '';
        var filetype = '';

        if(file_properties[0] === "*")
        { 
          filetype = "application";
        }
        else
        { 
          filetype = file_properties[0]; 
        }

 		var complete_filename = filename+'.'+extention;

 		if(parseInt(req.body.flag) == 1)
 		{
 			base_url = urlPath+req.body.account_id+'/library/'+complete_filename;

 		}
 		else if(parseInt(req.body.flag) == 2)
 		{
			base_url = urlPath+req.body.account_id+'/activity/'+complete_filename;
 		}


        HelperDocuments.getFileTypes('.'+extention, function(reType){

            var results = {
                file_name:filename,
                file_format:filetype,
                file_type:reType.toString(),
                extention:extention,
                file_path:base_url
            };
           
            jsonRes = {
                success: '1',
                result: results,
                successMessage: 'File is successfully uploaded.',
                errorMessage: errorMessage 
            };
            res.status(201).json(jsonRes);
            res.end();
        });

	})
});


router.post('/documents/library', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isLength({ min: 1 }),
	check('flag').isLength({ min: 1 })


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		document_id : req.body.document_id,
		title       : req.body.title,
		file_name   : req.body.file_name,
		file_ext    : req.body.file_ext,
		flag	    : req.body.flag,
	};

	// 0 = List
	// 1 = add
	// 2 = edit
	// 3 = delete

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			if(parseInt(reqParams.flag)==0)
			{

				HelperDocuments.getLibraryList(reqParams, function(retLibrary){

					if(retLibrary!=null){

						jsonRes = {
							success: '1',
							result: {
								library_list:retLibrary
							},
							successMessage: 'Library List',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();


					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else if(parseInt(reqParams.flag)==1)
			{

				HelperDocuments.addToLibrary(reqParams, function(returnInsert){
					if(returnInsert!=0){
						
						setTimeout(function(){

							HelperDocuments.getLibraryList(reqParams, function(retLibrary){

								jsonRes = {
									success: '1',
									result: {
										library_list:retLibrary
									},
									successMessage: 'New document is successfully saved.',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();

							});

						 }, 500);
			
					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
				});

			}
			else if(parseInt(reqParams.flag)==2)
			{

				HelperDocuments.updateLibrary(reqParams, function(returnUpdate){

					if(returnUpdate==1)
					{

						setTimeout(function(){

							HelperDocuments.getLibraryList(reqParams, function(retLibrary){

								jsonRes = {
									success: '1',
									result: {
										library_list:retLibrary
									},
									successMessage: 'Document is successfully updated.',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();

							});

						 }, 500);
			
					}
					else if(returnUpdate==0)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process request Please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}


				});

			}
			else if(parseInt(reqParams.flag)==3)
			{

				HelperDocuments.removeToLibrary(reqParams, function(retDel){

					if(retDel==1)
					{

						setTimeout(function(){

							HelperDocuments.getLibraryList(reqParams, function(retLibrary){

								jsonRes = {
									success: '1',
									result: {
										library_list:retLibrary
									},
									successMessage: 'Document is successfully removed.',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();

							});

						 }, 500);
			
					}
					else if(retDel==0)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process request Please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else
			{

				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();

			}
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/documents/send_document', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isLength({ min: 1 }),
	// check('title').isLength({ min: 1 }),
	// check('subject').isLength({ min: 1 }),
	// check('file_name').isLength({ min: 1 }),
	// check('file_ext').isLength({ min: 1 }),
	check('flag').isLength({ min: 1 })


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		document_id : req.body.document_id,
	
		title       : req.body.title,
		file_name   : req.body.file_name,
		file_ext    : req.body.file_ext,

		flag	      : req.body.flag,
		to_account_id : req.body.to_account_id,
		subject	      : req.body.subject,
		remarks	      : req.body.remarks,
		message       : req.body.message
	};

	// 1 = email
	// 2 = chat

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{


			if(parseInt(reqParams.flag) === 2)
			{

				HelperDocuments.checkConversationList(reqParams,function(retConList){

					if(parseInt(retConList) !== 0)
					{

						HelperDocuments.sendDocument(reqParams, function(retSend){

							if(retSend == 1 ){

								jsonRes = {
									success: '1',
									result: {},
									successMessage: 'New document is successfully sent.',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();

							}else if(retSend == 2){

								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Document not found." 
								};

								res.status(statRes).json(jsonRes);
								res.end();

							}else if(retSend == 0){

								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Unable to process request. Please try again." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
							}
						});
					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "You have no conversation history with this account." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
				});

			}
			else if(parseInt(reqParams.flag) === 1)
			{

				HelperDocuments.sendDocument(reqParams, function(retSend){

					if(retSend == 1 ){

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'New document is successfully sent.',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();

					}else if(retSend == 2){

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Document not found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}else if(retSend == 0){

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process request. Please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/documents/send_library', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isLength({ min: 1 }),
	check('library_id').isLength({ min: 1 }),
	check('to_account_id').isLength({ min: 1 }),
	check('subject').isLength({ min: 1 }),
	check('flag').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		library_id    : req.body.library_id,
		to_account_id : req.body.to_account_id,
		subject       : req.body.subject,
		remarks 	  : req.body.remarks,
		flag          : req.body.flag
	};


	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{


			HelperDocuments.sendLibrary(reqParams, function(retSend){

				if(retSend!=0){

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Document is successfully sent.',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/documents/activity', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isLength({ min: 1 }),
	check('page').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		company_id    : req.body.company_id,
		date          : req.body.date,
		page          : req.body.page,
		subject       : req.body.subject,
		category      : req.body.category
	};

	// category 
	// 0 = all
	// 1 = sent 
	// 2 = receive

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperDocuments.getActivityList(reqParams, function(company,retSend,pagination){

				if(retSend!=null){

					jsonRes = {
						success: '1',
						result: {
							pagination_details : pagination,
							company_person     : company,
							activity_list      : retSend
						},
						successMessage: 'Activity List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {
							pagination_details : pagination,
							company_person     : company,
							activity_list      : []
						},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(201).json(jsonRes);
			res.end();

		}
	});
});


router.post('/documents/save_activity', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isLength({ min: 1 }),
	check('document_id').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		document_id : req.body.document_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperDocuments.saveActivity(reqParams, function(retSend){

				if(retSend == 1 ){

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Document is successfully saved to library.',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else if(retSend == 2){

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Document not found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();

				}else if(retSend == 0){

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


module.exports = router;