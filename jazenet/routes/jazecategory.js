require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperJazeCategory = require('../helpers/HelperJazeCategory.js');

const { check, validationResult } = require('express-validator');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};
var stat;

var statRes = 422;
var jsonRes = {};



router.post('/jazecategory/jazefood_home',[

	check('account_id').isNumeric(),
	check('api_token').isLength({ min: 1 })


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {
		category_id : 10,
		account_id  : req.body.account_id,
		api_token   : req.body.api_token
	};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){	

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazeCategory.getJazefoodHome(arrParams, function(retFood){


				if(retFood !== null){

					success = '1';
					successMessage = 'Jazefood Home.';
					errorMessage = '';
					stat = 201;
					results = {
						jazefood_home:retFood
					};

				}else{
					success = '0';
					successMessage = 'No result found.';
					errorMessage = '';
					stat = statRes;
					results = {
						jazefood_home:[]
					};
				}

				jsonRes = {
					success: success,
					result: results,
					successMessage: successMessage,
					errorMessage: "" 
		 		};
				res.status(stat).json(jsonRes);
				res.end();

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazecategory/eatingout',[

	check('category_id').isNumeric(),
	check('account_id').isNumeric(),
	check('api_token').isLength({ min: 1 }),
	check('country_id').isNumeric(),
	check('state_id').isNumeric(),
	check('city_id').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		category_id : req.body.category_id,
		account_id  : req.body.account_id,
		api_token   : req.body.api_token,
		country_id  : req.body.country_id,
		state_id    : req.body.state_id,
		city_id     : req.body.city_id
	};

	var banners_list = [];

	banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/jazefood/banners/mobile/ads1.png");
	banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/jazefood/banners/mobile/ads2.png");
	banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/jazefood/banners/mobile/ads3.png");


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperJazeCategory.getAttributeParent(arrParams, function(retChilds){
			
				if(retChilds!=null)
				{	

					HelperJazeCategory.getCategoryAttributes(arrParams, function(retCategory){

			
						if(retCategory !== null){

							HelperJazeCategory.getFeaturedCompany(arrParams, function(retCompany){

								jsonRes = {
									success: '1',
									result:  {
										banners:banners_list,
										featured_company:retCompany,
										portal_category_titles:retChilds,
										controllers:retCategory
									},
									successMessage: 'Jazenet Category.',
									errorMessage: "" 
						 		};

								res.status(201).json(jsonRes);
								res.end();

							});
					
						}else{

							jsonRes = {
								success: '0',
								result: {},
								successMessage: 'No result found.',
								errorMessage: "" 
					 		};
							res.status(statRes).json(jsonRes);
							res.end();
						}
		
					});

				}else{

					jsonRes = {
						success: '0',
						result: {},
						successMessage: 'No result found.',
						errorMessage: "" 
			 		};
					res.status(statRes).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});



router.post('/jazecategory/search_eatingout',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),

	check('city_id').isNumeric(),
	check('state_id').isNumeric(),
	check('country_id').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		city_id      : req.body.city_id,
		state_id     : req.body.state_id,
		country_id   : req.body.country_id,

		category_id  : req.body.category_id,
		attributes   : req.body.attributes,
		page         : req.body.page,
		lat          : req.body.latitude,
		lon          : req.body.longitude
		
	};


	// multiselect_image_button = 1
	// multiselect_dropdown = 2
	// color_picker = 3
	// image_button = 4
	// dropdown = 5
	// radio_button = 6
	// select_range = 7
	// input_range = 8
	// text = 9


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazeCategory.searchEatingOut(arrParams, function(retSearch,pagination_details){


				if(retSearch !=null){
					jsonRes = {
						success: '1',
						result: {
							pagination_details:pagination_details,
							search_eatingout:retSearch
						},
						successMessage: 'Searched result.',
						errorMessage: "" 
						};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '0',
						result: {
							pagination_details:pagination_details,
							search_eatingout:[]
						},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
		
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}

	});

});





// router.post('/jazecategory/search_company',[

// 	check('category_id').isNumeric(),
// 	check('page').isNumeric()



// 	], (req,res,next) => {

// 	var _send = res.send;
// 	var sent = false;

// 	res.send = function(data){
// 	   if(sent) return;
// 	   _send.bind(res)(data);
// 	   sent = true;
// 	};

//   	var errors = validationResult(req);
//   	if (!errors.isEmpty()) {
//   		jsonRes = {
// 			success: success, 
// 			result: {}, 
// 			successMessage: successMessage,
// 			errorMessage: "No mandatory fileds entered." 
//   		};

// 		res.status(statRes).json(jsonRes);
//  		res.end();
//   	}

// 	var arrParams = {
// 		category_id : req.body.category_id,
// 		country_id  : req.body.country_id,
// 		state_id    : req.body.state_id,
// 		city_id     : req.body.city_id,
// 		page        : req.body.page,
// 		lon         : req.body.longitude,
// 		lat         : req.body.latitude
// 	};

// 	var pagination_details_empty  = {
// 		total_page   : '0',
//     	current_page : '0',
//     	next_page    : '0',
//     	total_result : '0'
//     };

// 	HelperJazeCategory.searchCompanyCategory(arrParams, function(retCompany,pagination_details){

// 		if(retCompany!=null){

// 			HelperJazeCategory.getCompanyCategoryAttributes(arrParams, function(retAttributes){
		
// 				if(parseInt(arrParams.page) !==0 )
// 				{

// 					jsonRes = {

// 						success: '1',
// 						result: {
// 							pagination_details:pagination_details,
// 							controllers:[],
// 							searched_company:retCompany
// 						},
// 						successMessage: 'Searched result.',
// 						errorMessage: "" 
// 					};

// 					res.status(201).json(jsonRes);
// 					res.end();

// 				}
// 				else
// 				{

// 					jsonRes = {

// 						success: '1',
// 						result: {
// 							pagination_details:pagination_details,
// 							controllers:retAttributes,
// 							searched_company:retCompany
// 						},
// 						successMessage: 'Searched result.',
// 						errorMessage: "" 
// 					};

// 					res.status(201).json(jsonRes);
// 					res.end();
				
// 				}
// 			});

// 		}else{
// 			jsonRes = {
// 				success: '1',
// 				result: {
// 					pagination_details:pagination_details_empty,
// 					controllers:[],
// 					searched_company:[]
// 				},
// 				successMessage:"",
// 				errorMessage: "No result found." 
// 	 		};
// 			res.status(statRes).json(jsonRes);
// 			res.end();
// 		}

// 	});

// });


router.post('/jazecategory/category_list', [

	// check('api_token').isAlphanumeric(),
	// check('app_id').isNumeric(),
	// check('account_id').isNumeric(),
	check('category_id').isNumeric()


], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {		
		// api_token		: req.body.api_token, 
		// app_id			: req.body.app_id, 
		// account_id		: req.body.account_id,
		category_id		: req.body.category_id
	};

	// HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

	// 	if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
	// 	{	

		HelperJazeCategory.getCategoryListFromJazenet(reqParams, function(returnList){	

			if(parseInt(returnList)!==0){

				jsonRes = {
					success: '1',
					result: {category_list:returnList},
					successMessage: 'Category List',
					errorMessage: errorMessage 
				};
		
				res.status(201).json(jsonRes);
				res.end();

			}else{

				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "No result found." 
		  		};

	  			res.status(statRes).json(jsonRes);
	 			res.end();
			}



		});
		// }
		// else
		// {
		// 	jsonRes = {
		// 		success: success,
		// 		result: {},
		// 		successMessage: successMessage,
		// 		errorMessage: "Device not found." 
	 //  		};

  // 			res.status(statRes).json(jsonRes);
 	// 		res.end();
		// }
	// });
});


router.post('/jazecategory/search_company',[

	check('category_id').isNumeric(),
	check('page').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		category_id : req.body.category_id,
		country_id  : req.body.country_id,
		state_id    : req.body.state_id,
		city_id     : req.body.city_id,
		page        : req.body.page,
		lon         : req.body.longitude,
		lat         : req.body.latitude,
		attributes  : req.body.attributes
	};

	var pagination_details_empty  = {
		total_page   : '0',
    	current_page : '0',
    	next_page    : '0',
    	total_result : '0'
    };

	HelperJazeCategory.searchCategory(arrParams, function(retCategory,retCompany,pagination_details){

		if(retCompany!=null){

			jsonRes = {

				success: '1',
				result: {
					pagination_details:pagination_details,
					controllers:retCategory,
					searched_company:retCompany
				},
				successMessage: 'Searched result.',
				errorMessage: "" 
			};

			res.status(201).json(jsonRes);
			res.end();

		}else{
			jsonRes = {
				success: '1',
				result: {
					pagination_details:pagination_details_empty,
					controllers:[],
					searched_company:[]
				},
				successMessage:"",
				errorMessage: "No result found." 
	 		};
			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazecategory/concierge', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric()


], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {		
		api_token		: req.body.api_token, 
		account_id		: req.body.account_id,

	};


	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	

			HelperJazeCategory.getConcierge(reqParams, function(returnList){	

				if(parseInt(returnList)!==0){

					jsonRes = {
						success: '1',
						result: {concierge:returnList},
						successMessage: 'Concierge',
						errorMessage: errorMessage 
					};
			
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
			  		};

		  			res.status(statRes).json(jsonRes);
		 			res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});




router.post('/jazecategory/search_jazefood',[
	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),

	check('city_id').isNumeric(),
	check('state_id').isNumeric(),
	check('country_id').isNumeric(),
	check('keyword').isLength({ min: 1 })
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var reqParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,

		city_id      : req.body.city_id,
		state_id     : req.body.state_id,
		country_id   : req.body.country_id,
		keyword   : req.body.keyword	
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperJazeCategory.searchJazefood(reqParams, function(returnList){	
			
				if(returnList !== null)
				{
					jsonRes = {
						success: "1",
						result: {jazefood_search_result : returnList},
						successMessage : 'search result.',
						errorMessage: errorMessage 
					};
		
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: "0",
						result: {},
						successMessage : 'No result found.',
						errorMessage: errorMessage 
					};
		
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});


module.exports = router;