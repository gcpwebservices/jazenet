require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();
const request = require('request');

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperCompany = require('../helpers/HelperCompany.js');
const HelperProfessional = require('../helpers/HelperProfessional.js');
const HelperDocuments = require('../helpers/HelperDocuments.js');

const { check, validationResult } = require('express-validator');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};

const storagePath = G_STATIC_IMG_PATH+"company/";
const STATIC_IMG_URL = G_STATIC_IMG_URL+"uploads/jazenet/company/";
const crypto = require('crypto');

const mime = require('mime-types');
const multer = require('multer');
const fs = require('fs');
const http = require('http');
const download = require('download-file');
const url = require('url');
const path = require('path');

var Jimp = require('jimp');
//------------------------------------------------------------------- Company Profile Module End ---------------------------------------------------------------------------//

router.post('/company/company_profile', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id,
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperCompany.getCompanyProfileDetails(reqParams, function(retSearched){

				if(retSearched!=null){
					
					jsonRes = {
						success: '1',
						result: {'company_detail_info' : retSearched},
						successMessage: 'Company Profile Details',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Company Profile Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Branch Module Start ---------------------------------------------------------------------------//

router.post('/company/branch_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric(),
	check('country_id').isNumeric(),
	check('state_id').isNumeric(),
	check('city_id').isNumeric(),
	check('result_flag').isNumeric(),
	check('branch_id').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id,
		country_id : req.body.country_id,
		state_id   : req.body.state_id,
		city_id    : req.body.city_id,
		result_flag  : req.body.result_flag,
		branch_id  	 : req.body.branch_id,
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperCompany.getBranchList(reqParams, function(branch_list){

				if(branch_list!=null){
					
					jsonRes = {
						success: '1',
						result: {'branch_list' : branch_list},
						successMessage: 'Company Branch List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {'branch_list' : []},
						successMessage: "No result found.",
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/company/branch_location_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperCompany.getBranchLocationList(reqParams.company_id, function(location_list){

				if(location_list!=null){
					
					jsonRes = {
						success: '1',
						result: {'branch_location_list' : location_list},
						successMessage: 'Branch Location List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {'location_list' : []},
						successMessage: "No result found.",
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});



router.post('/company/branch_manager', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric()



	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperCompany.getBranchManagerList(reqParams, function(branch_list){

				if(branch_list!=null){
					
					jsonRes = {
						success: '1',
						result: {'branch_list' : branch_list},
						successMessage: 'Company Branch List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {'branch_list' : []},
						successMessage: "No result found.",
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


//------------------------------------------------------------------- Branch Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Accreditation Module Start ---------------------------------------------------------------------------//

router.post('/company/accreditation_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id,
		title      : req.body.title,
		file_name  : req.body.file_name,
		flag       : req.body.flag,
		id         : req.body.id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			if(parseInt(reqParams.flag) === 0)
			{

				HelperCompany.getAccreditationList(reqParams.company_id, function(review_list){

					if(review_list!=null){
						
						jsonRes = {
							success: '1',
							result: {'accreditation_list' : review_list},
							successMessage: 'Company Accreditation List',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();

					}else{

						jsonRes = {
							success: '1',
							result: {'accreditation_list' : []},
							successMessage: "No result found.",
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
				});
			}
			else if(parseInt(reqParams.flag) === 1)
			{

				HelperCompany.addAccreditation(reqParams, function(retInsert){

					if(parseInt(retInsert) === 1){


						setTimeout(function(){ 

							HelperCompany.getAccreditationList(reqParams.company_id, function(review_list){

								jsonRes = {
									success: '1',
									result: {'accreditation_list' : review_list},
									successMessage: 'Company Accreditation is successfully created',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();

							});

						}, 500);
				

					}else{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: "Unable to process request. Please try again.",
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
				});

			}
			else if(parseInt(reqParams.flag) === 2)
			{

				HelperCompany.updateAccreditation(reqParams, function(retUpdate){

					if(parseInt(retUpdate) === 1){

						setTimeout(function(){ 

							HelperCompany.getAccreditationList(reqParams.company_id, function(review_list){

								jsonRes = {
									success: '1',
									result: {'accreditation_list' : review_list},
									successMessage: 'Company Accreditation is successfully updated.',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();

							});

						}, 500);
				

					}else if(parseInt(retUpdate) === 2){

						jsonRes = {
							success: '1',
							result: {'accreditation_list' : []},
							successMessage: "No result found.",
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else
					{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: "Unable to process request. Please try again.",
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();

					}

				});

			}
			else if(parseInt(reqParams.flag) === 3)
			{
			
				HelperCompany.delAccreditation(reqParams, function(retUpdate){

					if(parseInt(retUpdate) === 1){

						setTimeout(function(){ 

							HelperCompany.getAccreditationList(reqParams.company_id, function(review_list){

								jsonRes = {
									success: '1',
									result: {'accreditation_list' : review_list},
									successMessage: 'Company Accreditation is successfully deleted.',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();

							});

						}, 500);
				

					}else if(parseInt(retUpdate) === 2){

						jsonRes = {
							success: '1',
							result: {'accreditation_list' : []},
							successMessage: "No result found.",
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else
					{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: "Unable to process request. Please try again.",
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();

					}

				});

			}
			else
			{

				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Accreditation Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Division Module Start ---------------------------------------------------------------------------//

router.post('/company/division_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperCompany.getDivisionList(reqParams.company_id, function(review_list){

				if(review_list!=null){
					
					jsonRes = {
						success: '1',
						result: {'division_list' : review_list},
						successMessage: 'Company Division List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {'division_list' : []},
						successMessage: "No result found.",
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Division Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Notice Module Start ---------------------------------------------------------------------------//

router.post('/company/notice_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperCompany.getNoticeList(reqParams.company_id, function(review_list){

				if(review_list!=null){
					
					jsonRes = {
						success: '1',
						result: {'notice_list' : review_list},
						successMessage: 'Company Notice List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {'notice_list' : []},
						successMessage: "No result found.",
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Notice Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Photos Module Start ---------------------------------------------------------------------------//

router.post('/company/photos_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperCompany.getPhotoList(reqParams.company_id, function(review_list){

				if(review_list!=null){
					
					jsonRes = {
						success: '1',
						result: {'photos_list' : review_list},
						successMessage: 'Company Photo List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {'photos_list' : []},
						successMessage: "No result found.",
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Photos Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Portfolio Module Start ---------------------------------------------------------------------------//

router.post('/company/portfolio_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperCompany.getPortfolioList(reqParams.company_id, function(review_list){

				if(review_list!=null){
					
					jsonRes = {
						success: '1',
						result: {'portfolio_list' : review_list},
						successMessage: 'Company Portfolio List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {'portfolio_list' : []},
						successMessage: "No result found.",
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Portfolio Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Reviews Module Start ---------------------------------------------------------------------------//

router.post('/company/review_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperCompany.getReviewList(reqParams.company_id, function(review_list){

				if(review_list!=null){
					
					jsonRes = {
						success: '1',
						result: {'review_list' : review_list},
						successMessage: 'Company Review List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {'review_list' : []},
						successMessage: "No result found.",
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/company/post_review', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id,
		message    : req.body.message,
		rating     : req.body.rating,
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperCompany.postReview(reqParams, function(retSearched){

				if(retSearched!=null){
					
					HelperCompany.getReviewList(reqParams.company_id, function(review_list){

						if(review_list!=null){
							
							jsonRes = {
								success: '1',
								result: {'review_list' : review_list},
								successMessage: 'post successfully sent',
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();
		
						}else{
		
							jsonRes = {
								success: '1',
								result: {'review_list' : []},
								successMessage: "Unable to process request. Please try again.",
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
					});

				}else{

					jsonRes = {
						success: '1',
						result: {'review_list' : []},
						successMessage: "No result found.",
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

//------------------------------------------------------------------- Reviews Module End  ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Team Module Start ---------------------------------------------------------------------------//

router.post('/company/team_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric(),
	check('flag').isNumeric(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id,
		flag       : req.body.flag
	};

//0 normal, 1 pending

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			if(parseInt(reqParams.flag) === 0)
			{
				HelperCompany.getTeamList(reqParams.company_id, function(team_list){

					if(team_list!==null && parseInt(team_list.length) > 0){
						
						jsonRes = {
							success: '1',
							result: {'team_list' : team_list},
							successMessage: 'Company Team List',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();

					}else{

						jsonRes = {
							success: '1',
							result: {'team_list' : []},
							successMessage: "No result found.",
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
				});
			}
			else if(parseInt(reqParams.flag) === 1)
			{
				HelperCompany.getPendingTeamList(reqParams.company_id, function(team_list){

					if(team_list!=null && parseInt(team_list.length) > 0){
						
						jsonRes = {
							success: '1',
							result: {'team_list' : team_list},
							successMessage: 'Company Team List',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();

					}else{

						jsonRes = {
							success: '1',
							result: {'team_list' : []},
							successMessage: "No result found.",
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/company/manage_team', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric(),
	check('flag').isNumeric()



	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id,
		flag       : req.body.flag,
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			if(parseInt(reqParams.flag) === 0)
			{
				HelperCompany.getManageTeamList(reqParams, function(team_list){

					if(team_list!=null){
						
						jsonRes = {
							success: '1',
							result: {'manage_team' : team_list},
							successMessage: 'Company Manage Team',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();

					}else{

						jsonRes = {
							success: '1',
							result: {'manage_team' : []},
							successMessage: "No result found.",
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
				});
			}
			else if(parseInt(reqParams.flag) === 1)
			{
				HelperCompany.getPendingManageTeam(reqParams, function(team_list){

					if(team_list!=null){
						
						jsonRes = {
							success: '1',
							result: {'manage_team_pending_request' : team_list},
							successMessage: 'Company Manage Team',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();

					}else{

						jsonRes = {
							success: '1',
							result: {'manage_team_pending_request' : []},
							successMessage: "No result found.",
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/company/team_manage_department', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric(),
	//check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id,
		//flag       : req.body.flag,
		department_name      : req.body.department_name
		//id         : req.body.id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperCompany.manageDepartmentList(reqParams.company_id, function(dept_list){

				if(dept_list!=null){
					
					jsonRes = {
						success: '1',
						result: {'department_list' : dept_list},
						successMessage: 'Department List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {'department_list' : []},
						successMessage: "No result found.",
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


//------------------------------------------------------------------- Team Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Company Favourite Module Start ---------------------------------------------------------------------------//

router.post('/company/save_to_favorite', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperCompany.saveCompanyFavourite(reqParams, function(fav_status){

				if(parseInt(fav_status) !== 0){
					
					var success_message = "";

					var favStatus = "0";

					if(parseInt(fav_status) === 1)
					{ success_message = "Successfully add to Favorites"; favStatus = "1";  }
					else
					{ success_message = "Successfully removed to Favorites"; favStatus = "0"; }

					jsonRes = {
						success: '1',
						result: {fav_status : favStatus},
						successMessage: success_message,
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "some error occurred, please try again." 
					};
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Company Favourite Module End ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Company Keyword Search Module Start ---------------------------------------------------------------------------//

router.post('/company/search_keyword',[
	check('account_id').isNumeric(),
	check('api_token').isLength({ min: 1 }),
	check('keyword').isLength({ min: 1 }),
	check('page').isNumeric(),
	check('country_id').isNumeric(),
	check('state_id').isNumeric(),
	check('city_id').isNumeric(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {
		account_id  : req.body.account_id,
		api_token 	: req.body.api_token,
		keyword 	: req.body.keyword,
		country_id  : req.body.country_id,
		state_id    : req.body.state_id,
		city_id     : req.body.city_id,
		page        : req.body.page,
		lon         : req.body.longitude,
		lat         : req.body.latitude
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperCompany.getSearchKeywordCompanyList(reqParams, function(retCompany){

				if(retCompany.result !=null){

					var next_page = (parseInt(reqParams.page) + 1);

					if(parseInt(retCompany.tot_page) <= parseInt(next_page))
					{ next_page = "0"; }

					var pagination_details  = {
						tot_count    : retCompany.tot_count.toString(),
						total_page   : retCompany.tot_page.toString(),
						current_page : reqParams.page.toString(),
						next_page    : next_page.toString()
					};

					jsonRes = {
						success: '1',
						result: {
							pagination_details : pagination_details,
							searched_company   : retCompany.result
						},
						successMessage: 'Searched result.',
						errorMessage: "" 
					};
		
					res.status(201).json(jsonRes);
					res.end();
	
				}else{
					jsonRes = {
						success: '0',
						result: {
							pagination_details:{},
							searched_company:[]
						},
						successMessage:"",
						errorMessage: "No result found." 
					};
					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
	});
});


//------------------------------------------------------------------- Company Keyword Search Module End ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Company Vacancies Start ---------------------------------------------------------------------------//


router.post('/company/vacancy_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperCompany.getCompanyVacancies(reqParams, function(vacancies){

				if(vacancies!=null){
				
					jsonRes = {

						success: '1',
						result: {
							vacancy_list:vacancies
						},
						successMessage: 'Vacancy List.',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {
							vacancy_list:[]
						},
						successMessage:"",
						errorMessage: "No result found." 
			 		};
					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


//------------------------------------------------------------------- Company Vacancies End ---------------------------------------------------------------------------//



//------------------------------------------------------------------- Company Profile Essentials  ---------------------------------------------------------------------------//
router.post('/company/company_essentials', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('company_id').isNumeric(),
	check('flag').isNumeric(),


], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
	}
	
	var reqParams = {		
		api_token  : req.body.api_token, 
		account_id : req.body.account_id,
		company_id : req.body.company_id,

		name        : req.body.name,
		category_id : req.body.category_id,
		email           : req.body.email,
		company_website : req.body.company_website,
		mobile_phone_code   : req.body.mobile_phone_code,
		mobile_number         : req.body.mobile_number,
		landline_phone_code : req.body.landline_phone_code,
		landline_number       : req.body.landline_number,

		country_id     : req.body.country_id,
		state_id       : req.body.state_id,
		city_id        : req.body.city_id,
		area_id        : req.body.area_id,

		street_name    : req.body.street_name,
		street_no      : req.body.street_no,
		bldg_name      : req.body.bldg_name,
		bldg_no        : req.body.bldg_no,
		postal_code    : req.body.postal_code,
		currency_id    : req.body.currency_id,

		is_merchant_type  : req.body.is_merchant_type,

		latitude   : req.body.latitude,
		longitude  : req.body.longitude,

		keywords    : req.body.keywords,

		flag       : req.body.flag

	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			
			if(parseInt(reqParams.flag)==1)
			{

				HelperCompany.getEssentialsDetails(reqParams, function(retEssentials){

					if(retEssentials!=null){

						jsonRes = {
							success: '1',
							result: {
								essential_details:retEssentials
							},
							successMessage: 'Essential Details',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();


					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else if(parseInt(reqParams.flag)==2)
			{

				HelperCompany.updateEssentials(reqParams, function(retUpdate){



					if(parseInt(retUpdate)==1)
					{

						
						setTimeout(function(){

							HelperCompany.getEssentialsDetails(reqParams, function(retEssentials){

								jsonRes = {
									success: '1',
									result: {
										essential_details:retEssentials
									},
									successMessage: 'Essential details is successfully updated.',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();
				
							});

						}, 3000);
	
					}
					else if(parseInt(retUpdate)==2)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}


				});

			}
			else
			{

				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();

			}
		

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}
	});
});

//------------------------------------------------------------------- Company Profile Essentials  ---------------------------------------------------------------------------//

//------------------------------------------------------------------- Company Upload Picture  ---------------------------------------------------------------------------//

var storage = multer.diskStorage({
	destination: function (req, file, callback) {
	
	function asyncFunction (item, cb) {
	  	setTimeout(() => {

			var out_path = storagePath+req.body.company_id+'/';
			var inner_path;

			if(req.body.flag == '1')
			{
				inner_path   = out_path+'logo/';	

			}
			else if(req.body.flag == '2')
			{
				inner_path = out_path+'listing_images/';
			}
			else if(req.body.flag == '3')
			{
				inner_path = out_path+'cover_image/';
			}
			else if(req.body.flag == '4')
			{
				inner_path = out_path+'jazephotos/';
			}
			else if(req.body.flag == '5')
			{
				inner_path = out_path+'jazeaccreditations/';
			}

			var main_path = inner_path;

			if (!fs.existsSync(out_path)){
				fs.mkdirSync(out_path,'0757', true); 
			}

			if (!fs.existsSync(inner_path)){
				fs.mkdirSync(inner_path,'0757', true); 
			}

			//console.log(main_path);

			callback(null, main_path);

			// if(req.body.flag == '1')
			// {
			// 	inner_path   = out_path+'logo_thumb/';	
			// 	callback(null, inner_path);

			// }


			cb();
	  	}, 100);
	}

	let requests = 0;

		if(req.body.company_id !=''){

			return new Promise((resolve) => {
				requests = req.body.company_id;
		  		asyncFunction(requests, resolve);
		    });

			Promise.all(requests);
		}
  },
  	filename: function (req, file, callback) {

  		var ext = getExtension(file.originalname);
  		var cleanName = getCleanFileName(file.originalname);
  		var completeFileName = req.body.company_id+'_'+Date.now()+'.'+ext;

    	callback(null, completeFileName);
  	}
});


router.post('/company/upload_photos', function(req, res) {

	var upload = multer({
		storage: storage,
	}).single('file');

	upload(req, res, function(err) {



 	 	var extName = req.file.filename.split(".");
 	 	var filename = extName[extName.length - 2];

	 	var extArray = req.file.filename.split(".");
 	 	var extention = extArray[extArray.length - 1];

 	  	var file_properties = req.file.mimetype.split("/");

 		var base_url = '';
        var filetype = '';

 		var complete_filename = filename+'.'+extention;


		if(req.body.flag == '1')
		{
			base_url = STATIC_IMG_URL+req.body.company_id+'/logo/'+complete_filename;


			var tocropImage = STATIC_IMG_URL+req.body.company_id+'/logo/'+complete_filename;
			var toStorageImage = storagePath+req.body.company_id+'/logo_thumb/'+'thumb_'+complete_filename;

			const dirPath = storagePath+req.body.company_id+'/logo_thumb/';
			fs.readdir(dirPath, (err, files) => {
			  if (err) throw err;

			  for (const file of files) {
			    fs.unlink(path.join(dirPath, file), err => {
			      if (err) throw err;
			    });
			  }
			});

			setTimeout(function(){ 

				async function resize() {
				  // Read the image.
				  const image = await Jimp.read(tocropImage);
				  // Resize the image to width 150 and heigth 150.
				  await image.resize(150, 150);
				  // Save and overwrite the image
				  await image.writeAsync(toStorageImage);
				}
				resize();

			}, 1000);

		}
		else if(req.body.flag == '2')
		{
			base_url = STATIC_IMG_URL+req.body.company_id+'/listing_images/'+complete_filename;
		}
		else if(req.body.flag == '3')
		{
			base_url = STATIC_IMG_URL+req.body.company_id+'/cover_image/'+complete_filename;
		}
		else if(req.body.flag == '4')
		{
			base_url = STATIC_IMG_URL+req.body.company_id+'/jazephotos/'+complete_filename;
		}
		else if(req.body.flag == '5')
		{
			base_url = STATIC_IMG_URL+req.body.company_id+'/jazeaccreditations/'+complete_filename;
		}

 		filetype = file_properties[0]; 

 	
 		if( parseInt(req.body.flag) == 1 || parseInt(req.body.flag) == 2)
 		{

			var arrParams = {
	 			account_id  : req.body.company_id, 
	 			file_name   : complete_filename,
				flag        : 1
	 		
	 		};

	 		if(parseInt(req.body.flag) == 2){
				arrParams = {
		 			account_id  : req.body.company_id, 
		 			file_name   : complete_filename,
		 			flag        : 2
		 		
		 		};
	 		}

	    	HelperCompany.getCompanyLogos(arrParams,function(retStatus){

		        HelperDocuments.getFileTypes('.'+extention, function(reType){

		            var results = {
		                file_name:filename,
		                file_format:filetype,
		                file_type:reType.toString(),
		                extention:extention,
		                file_path:base_url,
		            };
		           
		            jsonRes = {
		                success: '1',
		                result: results,
		                successMessage: 'File is successfully uploaded.',
		                errorMessage: errorMessage 
		            };
		            res.status(201).json(jsonRes);
		            res.end();

		        });

	        });

 		}
		else
		{

	        HelperDocuments.getFileTypes('.'+extention, function(reType){

	            var results = {
	                file_name:filename,
	                file_format:filetype,
	                file_type:reType.toString(),
	                extention:extention,
	                file_path:base_url,
	            };
	           
	            jsonRes = {
	                success: '1',
	                result: results,
	                successMessage: 'File is successfully uploaded.',
	                errorMessage: errorMessage 
	            };
	            res.status(201).json(jsonRes);
	            res.end();

	        });
		} 		


	});

});




function getExtension(path) {
    var basename = path.split(/[\\/]/).pop(),  // extract file name from full path ...
                                             	 // (supports `\\` and `/` separators)
        pos = basename.lastIndexOf(".");       // get last position of `.`
    if (basename === "" || pos < 1)            // if file name is empty or ...
        return "";                             //  `.` not found (-1) or comes first (0)
    return basename.slice(pos + 1);            // extract extension ignoring `.`
}

function getFilename(path) {
    var basename = path.split(/[\\/]/).pop(),      
        pos = basename.split('.'); 
    if (basename === "" || pos.length < 1)           
        return "";                          
    return pos[0];            
}


function getCleanFileName(path) {

	var basename = path.split(/\.(?=[^\.]+$)/);
	var cleanString = basename[0].replace(/[|&;$%@"<>()+1234567890,]/g, "");
         
    return cleanString;           
}


router.post('/company/company_photo_video', [

	check('api_token').isLength({ min: 1 }),
	check('company_id').isNumeric(),
	check('account_id').isNumeric(),
	check('item').isLength({ min: 1 }),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id,
		item       : req.body.item,
		flag       : req.body.flag
	};

	// 1 = company_logo
	// 2 = listing_images
	// 3 = cover_image
	// 4 = you tube

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			var arrFlags = ['1','2','3','4'];
			if(arrFlags.includes(reqParams.flag))
			{

				HelperCompany.saveUpdateCompanyPhotoVideo(reqParams, function(retStatus){

					if(parseInt(retStatus)==1)
					{	
						var message = 'Photos is successfully saved.';
						if(parseInt(reqParams.flag) === 4){
							message = 'video url is successfully saved.';
						}
						
						jsonRes = {
							success: '1',
							result: {},
							successMessage: message,
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
		
					}
					else if(parseInt(retStatus)==2)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}

				});

			}
			else
			{
				
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();

			}
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

router.post('/company/company_profile_images', [

	check('api_token').isLength({ min: 1 }),
	check('company_id').isNumeric(),
	check('account_id').isNumeric(),


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id,
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperCompany.getCompanyProfilePhotosDetails(reqParams, function(retStatus){

	
				jsonRes = {
					success: '1',
					result: {
						company_profile_photos:retStatus
					},
					successMessage: 'company profile photos',
					errorMessage: errorMessage 
				};
				res.status(201).json(jsonRes);
				res.end();

		

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Company Upload Picture  ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Company About Update  ---------------------------------------------------------------------------//
router.post('/company/update_about', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id,
		about      : req.body.about,
		flag       : req.body.flag
	};


	// flag 2 update 
	// flag 3 delete

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			if(parseInt(reqParams.flag) === 1)
			{

				
				HelperCompany.getCompanyAbout(reqParams, function(retabout){

					if(retabout!=null)
					{
						jsonRes = {
							success: '1',
							result: {about:retabout},
							successMessage: 'About information.',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();	
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process request. Please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
	

				});



			}
			else if(parseInt(reqParams.flag) === 2 )
			{
				
				HelperCompany.updateAbout(reqParams, function(retUpdate){

					if(parseInt(retUpdate) == 1)
					{

						setTimeout(function(){ 

							HelperCompany.getCompanyAbout(reqParams, function(retabout){

								if(retabout!=null)
								{
															
									jsonRes = {
										success: '1',
										result: {about:retabout},
										successMessage: 'About information is successfully saved.',
										errorMessage: errorMessage 
									};
									res.status(201).json(jsonRes);
									res.end();	
								}
								else
								{
									jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "Unable to process request. Please try again." 
									};

									res.status(statRes).json(jsonRes);
									res.end();
								}
				
							});

						}, 1000);

					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process request. Please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
	

				});
			}
			// else if(parseInt(reqParams.flag) === 3)
			// {

			// 	HelperCompany.updateAbout(reqParams, function(retUpdate){

			// 		if(parseInt(retUpdate) == 1)
			// 		{
			// 			jsonRes = {
			// 				success: '1',
			// 				result: {},
			// 				successMessage: 'About information is successfully removed.',
			// 				errorMessage: errorMessage 
			// 			};
			// 			res.status(201).json(jsonRes);
			// 			res.end();	
			// 		}
			// 		else
			// 		{
			// 			jsonRes = {
			// 				success: success,
			// 				result: {},
			// 				successMessage: successMessage,
			// 				errorMessage: "Unable to process request. Please try again." 
			// 			};

			// 			res.status(statRes).json(jsonRes);
			// 			res.end();
			// 		}
	
			// 	});

			// }
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Company About Update  ---------------------------------------------------------------------------//


//------------------------------------------------------------------- Company photo manager  ---------------------------------------------------------------------------//
router.post('/company/photo_manager', [

	check('api_token').isLength({ min: 1 }),
	check('company_id').isNumeric(),
	check('account_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id,
		id         : req.body.id,
		file_name  : req.body.file_name,
		title      : req.body.title,
		flag       : req.body.flag
	};



	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			if(parseInt(reqParams.flag) === 0 )
			{
				
				HelperCompany.getCompanyPhotos(reqParams, function(retPhotots){

					if(retPhotots !== null)
					{
						jsonRes = {
							success: '1',
							result: {
								company_photos:retPhotots
							},
							successMessage: 'Company Photos',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();	
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found" 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
	

				});
			}
			else if(parseInt(reqParams.flag) === 1 )
			{
				
				HelperCompany.addCompanyPhotos(reqParams, function(retUpdate){

					if(parseInt(retUpdate) == 1)
					{	
						setTimeout(function(){ 

							HelperCompany.getCompanyPhotos(reqParams, function(retPhotots){

								if(retPhotots !== null)
								{
									jsonRes = {
										success: '1',
										result: {
											company_photos:retPhotots
										},
										successMessage: 'Photo is successfully uploaded.',
										errorMessage: errorMessage 
									};
									res.status(201).json(jsonRes);
									res.end();	
								}
								else
								{
									jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "No result found" 
									};

									res.status(statRes).json(jsonRes);
									res.end();
								}
				

							});

						}, 1000);
			
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process request. Please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
	

				});
			}
			else if(parseInt(reqParams.flag) === 2)
			{

				HelperCompany.updateCompanyPhoto(reqParams, function(retUpdate){

					if(parseInt(retUpdate) == 1)
					{	
						setTimeout(function(){ 

							HelperCompany.getCompanyPhotos(reqParams, function(retPhotots){

								if(retPhotots !== null)
								{
									jsonRes = {
										success: '1',
										result: {
											company_photos:retPhotots
										},
										successMessage: 'Photo is successfully updated.',
										errorMessage: errorMessage 
									};
									res.status(201).json(jsonRes);
									res.end();	
								}
								else
								{
									jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "No result found" 
									};

									res.status(statRes).json(jsonRes);
									res.end();
								}
				

							});

						}, 1000);
			
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process request. Please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
	

				});

			}
			else if(parseInt(reqParams.flag) === 3)
			{


				HelperCompany.delCompanyPhoto(reqParams, function(retUpdate){

					if(parseInt(retUpdate) == 1)
					{	
						setTimeout(function(){ 

							HelperCompany.getCompanyPhotos(reqParams, function(retPhotots){

								if(retPhotots !== null)
								{
									jsonRes = {
										success: '1',
										result: {
											company_photos:retPhotots
										},
										successMessage: 'Photo is successfully deleted.',
										errorMessage: errorMessage 
									};
									res.status(201).json(jsonRes);
									res.end();	
								}
								else
								{
									jsonRes = {
										success: success,
										result: {},
										successMessage: successMessage,
										errorMessage: "No result found" 
									};

									res.status(statRes).json(jsonRes);
									res.end();
								}
				

							});

						}, 1000);
			
					}
					else if(parseInt(retUpdate) == 2)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process request. Please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
	

				});

			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/company/billing_shipping', [

	check('api_token').isLength({ min: 1 }),
	check('company_id').isNumeric(),
	check('account_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		id                    : req.body.id,
		api_token             : req.body.api_token,
		account_id            : req.body.account_id,
		company_id            : req.body.company_id,
		first_name            : req.body.first_name,
		last_name             : req.body.last_name,
		mobile_country_code   : req.body.mobile_country_code,
		mobile_no             : req.body.mobile_no,
		landline_country_code : req.body.landline_country_code,
		landline_no           : req.body.landline_no,
		country            : req.body.country_id,
		state              : req.body.state_id,
		city               : req.body.city_id,
		area               : req.body.area_id,
		street_name           : req.body.street_name,
		street_no             : req.body.street_no,
		building_name         : req.body.building_name,
		building_no           : req.body.building_no,
		shipping_note         : req.body.shipping_note,
		landmark              : req.body.landmark,
		location_type         : req.body.location_type,
		is_default            : req.body.is_default,
		flag                  : req.body.flag
	};

	// 1 = add
	// 2 = update
	// 3 = delete

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			if(parseInt(req.body.flag) == 0)
			{
				HelperCompany.getShippingDetails(reqParams, function(retBilling){

					if(retBilling!=null)
					{
						jsonRes = {
							success: '1',
							result: {billing_shipping:retBilling},
							successMessage: 'Company billing address.',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: '1',
							result: {billing_shipping:[]},
							successMessage: 'No result found.',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
		
				});
			}
			else if(parseInt(req.body.flag) == 1)
			{
				HelperCompany.addBillingAddress(reqParams, function(retStatus){

					if(retStatus == 1){
						setTimeout(function(){ 
							HelperCompany.getShippingDetails(reqParams, function(retBilling){
								jsonRes = {
									success: '1',
									result: {billing_shipping:retBilling},
									successMessage: 'New billing address is successfully saved.',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();
							});
						}, 300);
					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
			
				});
			}
			else if(parseInt(req.body.flag) == 2)
			{

				HelperCompany.updateBillingAddress(reqParams, function(retStatus){

					if(retStatus == 1){
						setTimeout(function(){ 
							HelperCompany.getShippingDetails(reqParams, function(retBilling){

								jsonRes = {
									success: '1',
									result: {billing_shipping:retBilling},
									successMessage: 'Billing address is successfully updated.',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();
							});
						}, 300);

					}
					else if(retStatus == 2)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					
					}

				});


			}
			else if(parseInt(req.body.flag) == 3)
			{
				var group_ids = req.body.id.split(",");
				var sucMess = "Billing address is successfully deleted.";
				if(group_ids.length > 1){
					sucMess = "Billing address are successfully deleted.";
				}
				HelperCompany.delBillingAddress(group_ids,reqParams, function(retStatus){
					if(retStatus == 1){

						setTimeout(function(){ 
							HelperCompany.getShippingDetails(reqParams, function(retBilling){

								jsonRes = {
									success: '1',
									result: {billing_shipping:retBilling},
									successMessage: sucMess,
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();
							});
						}, 500);

					}
					else if(retStatus == 2)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					
					}
				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/company/bank_payment', [

	check('api_token').isLength({ min: 1 }),
	check('company_id').isNumeric(),
	check('account_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		id              : req.body.id,
		api_token       : req.body.api_token,
		company_id      : req.body.company_id,
		account_id      : req.body.account_id,
		holder_name     : req.body.holder_name,
		account_no      : req.body.account_no,
		bank_name       : req.body.bank_name,
		branch_name     : req.body.branch_name,
		iban_no         : req.body.iban_no,
		swift_code      : req.body.swift_code,
		is_default      : req.body.is_default,
		flag            : req.body.flag
	};

	// 1 = add
	// 2 = update
	// 3 = delete

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			if(parseInt(req.body.flag) == 0)
			{
				HelperCompany.getBankDetails(reqParams, function(retBank){
					HelperCompany.getCreditCardDetails(reqParams, function(retCard){
						
						var returnCard = [];
						var returnBank = [];

						if(retCard!=null)
						{
							returnCard = retCard;
						}


						if(retBank!=null)
						{
							returnBank = retBank;
						}

						var obj_payment = {
							bank:returnBank,
							card:returnCard
						};	

						if(retCard ==null && retBank==null)
						{
							jsonRes = {
								success: '1',
								result: {payment_info:obj_payment},
								successMessage: "No result found.",
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
						else
						{

							jsonRes = {
								success: '1',
								result: {payment_info:obj_payment},
								successMessage: "Company Bank details.",
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
				
						}
			
				
					});
				});
			}
			else if(parseInt(req.body.flag) == 1)
			{
				HelperCompany.addBankDetails(reqParams, function(retStatus){

					if(retStatus == 1){
						setTimeout(function(){ 
							HelperCompany.getBankDetails(reqParams, function(retBank){
								HelperCompany.getCreditCardDetails(reqParams, function(retCard){
						
									var returnCard = [];
									var returnBank = [];

									if(retCard!=null)
									{
										returnCard = retCard;
									}


									if(retBank!=null)
									{
										returnBank = retBank;
									}

									var obj_payment = {
										bank:returnBank,
										card:returnCard
									};	

									if(retCard ==null && retBank==null)
									{
										jsonRes = {
											success: '1',
											result: {payment_info:obj_payment},
											successMessage: "No result found.",
											errorMessage: "" 
										};
										res.status(201).json(jsonRes);
										res.end();
									}
									else
									{

										jsonRes = {
											success: '1',
											result: {payment_info:obj_payment},
											successMessage: "Company Bank details is successfully saved.",
											errorMessage: "" 
										};
										res.status(201).json(jsonRes);
										res.end();
							
									}
								});
							});
						}, 300);
					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
			
				});
			}
			else if(parseInt(req.body.flag) == 2)
			{

				HelperCompany.updateBankDetails(reqParams, function(retStatus){

					if(retStatus == 1){
						setTimeout(function(){ 
							HelperCompany.getBankDetails(reqParams, function(retBank){
								HelperCompany.getCreditCardDetails(reqParams, function(retCard){
							
									var returnCard = [];
									var returnBank = [];

									if(retCard!=null)
									{
										returnCard = retCard;
									}


									if(retBank!=null)
									{
										returnBank = retBank;
									}

									var obj_payment = {
										bank:returnBank,
										card:returnCard
									};	

									if(retCard ==null && retBank==null)
									{
										jsonRes = {
											success: '1',
											result: {payment_info:obj_payment},
											successMessage: "No result found.",
											errorMessage: "" 
										};
										res.status(201).json(jsonRes);
										res.end();
									}
									else
									{

										jsonRes = {
											success: '1',
											result: {payment_info:obj_payment},
											successMessage: "Company Bank details is successfully updated.",
											errorMessage: "" 
										};
										res.status(201).json(jsonRes);
										res.end();
							
									}
								});
							});
						}, 500);

					}
					else if(retStatus == 2)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					
					}

				});


			}
			else if(parseInt(req.body.flag) == 3)
			{
				var group_ids = req.body.id.split(",");
				var sucMess = "Bank details is successfully deleted.";
				if(group_ids.length > 1){
					sucMess = "Bank details are successfully deleted.";
				}

				HelperCompany.delBankDetails(group_ids,reqParams, function(retStatus){
					if(retStatus == 1){

						setTimeout(function(){ 
							HelperCompany.getBankDetails(reqParams, function(retBank){
								HelperCompany.getCreditCardDetails(reqParams, function(retCard){
									var obj_payment = {
										bank:retBank,
										card:retCard
									};	
									jsonRes = {
										success: '1',
										result: {payment_info:obj_payment},
										successMessage: sucMess,
										errorMessage: "" 
									};
									res.status(201).json(jsonRes);
									res.end();
								});
							});
						}, 500);

					}
					else if(retStatus == 2)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					
					}
				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/company/card_payment', [

	check('api_token').isLength({ min: 1 }),
	check('company_id').isNumeric(),
	check('account_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		id            : req.body.id,
		api_token     : req.body.api_token,
		company_id    : req.body.company_id,
		account_id    : req.body.account_id,
		bank_name     : req.body.bank_name,
		card_name     : req.body.card_name,
		card_no       : req.body.card_no,
		exp_month     : req.body.exp_month,
		exp_year      : req.body.exp_year,
		csv           : req.body.csv,
		is_default    : req.body.is_default,
		flag          : req.body.flag
	};

	// 1 = add
	// 2 = update
	// 3 = delete

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			if(parseInt(req.body.flag) == 0)
			{
				HelperCompany.getCreditCardDetails(reqParams, function(retCard){
					HelperCompany.getBankDetails(reqParams, function(retBank){

						var returnCard = [];
						var returnBank = [];

						if(retCard!=null)
						{
							returnCard = retCard;
						}


						if(retBank!=null)
						{
							returnBank = retBank;
						}

						var obj_payment = {
							card:returnCard,
							bank:returnBank
						};	

						if(retCard ==null && retBank==null)
						{
							jsonRes = {
								success: '1',
								result: {payment_info:obj_payment},
								successMessage: "No result found.",
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
						}
						else
						{

							jsonRes = {
								success: '1',
								result: {payment_info:obj_payment},
								successMessage: "Company Card details.",
								errorMessage: "" 
							};
							res.status(201).json(jsonRes);
							res.end();
				
						}
			
				
					});
				});
			}
			else if(parseInt(req.body.flag) == 1)
			{
				HelperCompany.addCardDetails(reqParams, function(retStatus){

					if(retStatus == 1){
						setTimeout(function(){ 
							HelperCompany.getCreditCardDetails(reqParams, function(retCard){
								HelperCompany.getBankDetails(reqParams, function(retBank){
							
									var returnCard = [];
									var returnBank = [];

									if(retCard!=null)
									{
										returnCard = retCard;
									}


									if(retBank!=null)
									{
										returnBank = retBank;
									}

									var obj_payment = {
										card:returnCard,
										bank:returnBank
									};	

									if(retCard ==null && retBank==null)
									{
										jsonRes = {
											success: '1',
											result: {payment_info:obj_payment},
											successMessage: "No result found.",
											errorMessage: "" 
										};
										res.status(201).json(jsonRes);
										res.end();
									}
									else
									{

										jsonRes = {
											success: '1',
											result: {payment_info:obj_payment},
											successMessage: "Company card payment is successfully saved.",
											errorMessage: "" 
										};
										res.status(201).json(jsonRes);
										res.end();
							
									}
								});
							});
						}, 300);
					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
			
				});
			}
			else if(parseInt(req.body.flag) == 2)
			{

				HelperCompany.updateCardDetails(reqParams, function(retStatus){

					if(retStatus == 1){
						setTimeout(function(){ 
							HelperCompany.getCreditCardDetails(reqParams, function(retCard){
								HelperCompany.getBankDetails(reqParams, function(retBank){
							
									var returnCard = [];
									var returnBank = [];

									if(retCard!=null)
									{
										returnCard = retCard;
									}


									if(retBank!=null)
									{
										returnBank = retBank;
									}

									var obj_payment = {
										card:returnCard,
										bank:returnBank
									};	

									if(retCard ==null && retBank==null)
									{
										jsonRes = {
											success: '1',
											result: {payment_info:obj_payment},
											successMessage: "No result found.",
											errorMessage: "" 
										};
										res.status(201).json(jsonRes);
										res.end();
									}
									else
									{

										jsonRes = {
											success: '1',
											result: {payment_info:obj_payment},
											successMessage: "Company card payment is successfully updated.",
											errorMessage: "" 
										};
										res.status(201).json(jsonRes);
										res.end();
							
									}								
								});
							});
						}, 300);

					}
					else if(retStatus == 2)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					
					}

				});


			}
			else if(parseInt(req.body.flag) == 3)
			{
				var group_ids = req.body.id.split(",");
				var sucMess = "Card details is successfully deleted.";
				if(group_ids.length > 1){
					sucMess = "Card details are successfully deleted.";
				}

				HelperCompany.delCardDetails(group_ids,reqParams, function(retStatus){
					if(retStatus == 1){

						setTimeout(function(){ 
							HelperCompany.getCreditCardDetails(reqParams, function(retCard){
								HelperCompany.getBankDetails(reqParams, function(retBank){
									
									var returnCard = [];
									var returnBank = [];

									if(retCard!=null)
									{
										returnCard = retCard;
									}


									if(retBank!=null)
									{
										returnBank = retBank;
									}

									var obj_payment = {
										card:returnCard,
										bank:returnBank
									};

									jsonRes = {
										success: '1',
										result: {payment_info:obj_payment},
										successMessage: sucMess,
										errorMessage: "" 
									};
									res.status(201).json(jsonRes);
									res.end();
								});
							});
						}, 500);

					}
					else if(retStatus == 2)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					
					}
				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


//------------------------------------------------------------------- Company About Update  ---------------------------------------------------------------------------//
router.post('/company/portfolio', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric(),
	check('request_company_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		id         : req.body.id,
		company_id : req.body.company_id,
		request_company_id : req.body.request_company_id,
		flag       : req.body.flag
	};


	// flag 0 list 
	// flag 1 add
	// flag 3 delete

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			var arrFlags = ['1','2','3'];
			if(parseInt(reqParams.flag) === 0 )
			{
				
				HelperCompany.getPortfolio(reqParams, function(retport){

					if(retport!= null)
					{
						jsonRes = {
							success: '1',
							result: {portfolio_list:retport},
							successMessage: 'Portfolio',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();	
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
	

				});
			}
			else if(arrFlags.includes(reqParams.flag))
			{

				HelperCompany.updatePortfolio(reqParams, function(retUpdate){

					if(parseInt(reqParams.flag) === 1)
					{
						if(parseInt(retUpdate) == 1)
						{	

							setTimeout(function(){ 

								HelperCompany.getPortfolio(reqParams, function(retport){

									if(retport!= null)
									{
										jsonRes = {
											success: '1',
											result: {portfolio_list:retport},
											successMessage: 'Account is successfully added to your portfolio',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	
									}
									else
									{
										jsonRes = {
											success: success,
											result: {},
											successMessage: successMessage,
											errorMessage: "No result found." 
										};

										res.status(statRes).json(jsonRes);
										res.end();
									}
					

								});

							 }, 1000);
			
						}
						else if(parseInt(retUpdate) == 3)
						{
							jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "Account is already in your port" 
								};

								res.status(statRes).json(jsonRes);
								res.end();
						}
						else
						{
							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Unable to process request. Please try again." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}

					}
					else if(parseInt(reqParams.flag) === 3)
					{
						if(parseInt(retUpdate) == 1)
						{	

							setTimeout(function(){ 

								HelperCompany.getPortfolio(reqParams, function(retport){

									if(retport!= null)
									{
										jsonRes = {
											success: '1',
											result: {portfolio_list:retport},
											successMessage: 'Account is successfully removed to your portfolio.',
											errorMessage: errorMessage 
										};
										res.status(201).json(jsonRes);
										res.end();	
									}
									else
									{
										jsonRes = {
											success: success,
											result: {},
											successMessage: successMessage,
											errorMessage: "No result found." 
										};

										res.status(statRes).json(jsonRes);
										res.end();
									}
					

								});

							 }, 1000);
			
						}
						else if(parseInt(retUpdate) == 2)
						{
							jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "No result found." 
								};

								res.status(statRes).json(jsonRes);
								res.end();
						}
						else
						{
							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Unable to process request. Please try again." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}
					}

			
	
				});

			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


//------------------------------------------------------------------- security    ---------------------------------------------------------------------------//
router.post('/company/security', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		company_id   : req.body.company_id,

		old_password : crypto.createHash('md5').update(req.body.old_password).digest("hex"),
		new_password : crypto.createHash('md5').update(req.body.new_password).digest("hex"),
		con_password : crypto.createHash('md5').update(req.body.con_password).digest("hex"),

		old_sec_pin  : crypto.createHash('md5').update(req.body.old_sec_pin).digest("hex"),
		new_sec_pin  : crypto.createHash('md5').update(req.body.new_sec_pin).digest("hex"),
		con_sec_pin  : crypto.createHash('md5').update(req.body.con_sec_pin).digest("hex"),

		new_country_code : req.body.new_country_code,
		new_mobile_no    : req.body.new_mobile_no,

		old_email       : req.body.old_email,
		new_email       : req.body.new_email,
		con_email       : req.body.con_email,

		flag       : req.body.flag
	};


	// flag 1 jaze password 
	// flag 2 security pin 
	// flag 3 mobile number 
	// flag 4 email


	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			var arrFlags = ['1','2','3','4'];
			if( arrFlags.includes(reqParams.flag))
			{
				
				HelperCompany.updateJazeSecurity(reqParams, function(retUpdate){

					var sucmess = "";
					if(parseInt(reqParams.flag) === 1){
						sucmess = "Password is successfully updated.";
					}else if(parseInt(reqParams.flag) === 2){
						sucmess = "Security pin is successfully updated.";
					}else if(parseInt(reqParams.flag) === 3){
						sucmess = "Mobile number is successfully updated.";
					}else if(parseInt(reqParams.flag) === 4){
						sucmess = "Email is successfully updated.";
					}

					if(retUpdate == '1' )
					{
						jsonRes = {
							success: '1',
							result: {},
							successMessage: sucmess,
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();	
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage:  retUpdate
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
	

				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Company communication  ---------------------------------------------------------------------------//

router.post('/company/communication', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric(),
	check('flag').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id,
		jaze_diary : req.body.jaze_diary,
		jaze_feed  : req.body.jaze_feed,
		jaze_call  : req.body.jaze_call,
		jaze_chat  : req.body.jaze_chat,
		jaze_mail  : req.body.jaze_mail,
		flag       : req.body.flag,
	};


	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	

			if(parseInt(reqParams.flag) === 1)
			{
				HelperCompany.getCompanyCommunication(reqParams, function(retResult){

					jsonRes = {
						success: '1',
						result: {
							communication:retResult
						},
						successMessage: 'company communication',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				});
			}
			else if(parseInt(reqParams.flag) === 2)
			{

				HelperCompany.updateCompanyCommunication(reqParams, function(retComu){


					if(parseInt(retComu) === 1)
					{

						setTimeout(function(){ 


							HelperCompany.getCompanyCommunication(reqParams, function(retResult){

								jsonRes = {
									success: '1',
									result: {
										communication:retResult
									},
									successMessage: 'communication is successfully updated.',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();

							});


						}, 3000);

				
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process request. Please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}

				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


//------------------------------------------------------------------- Company jazepay  ---------------------------------------------------------------------------//

router.post('/company/update_jazepay', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric(),
	check('flag').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		company_id : req.body.company_id,
		status     : req.body.status,
		flag       : req.body.flag,
	};


	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	

			if(parseInt(reqParams.flag) === 1)
			{
				HelperCompany.getJazePayStatus(reqParams, function(retResult){

					jsonRes = {
						success: '1',
						result: {
							jazepay:retResult
						},
						successMessage: 'Jazepay',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				});
			}
			else if(parseInt(reqParams.flag) === 2)
			{

				HelperCompany.updateJazePaystatus(reqParams, function(retComu){


					if(parseInt(retComu) === 1)
					{

						setTimeout(function(){ 

							HelperCompany.getJazePayStatus(reqParams, function(retResult){

								jsonRes = {
									success: '1',
									result: {
										jazepay:retResult
									},
									successMessage: 'jazepay status is successfully updated.',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();

							});

						}, 1000);

				
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process request. Please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}

				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

// ------------------------------------------------------ Company Category Selection Module Start  --------------------------------------------------------------//   

router.post('/company/category_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('category_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		category_id  : req.body.category_id,
		flag  		 : req.body.flag
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){
 
		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	
			HelperCompany.getCategoryParentDetails(reqParams, function(parentDetails){	

				HelperCompany.getCategoryList(reqParams, function(cat_list){

					if(cat_list !== null)
					{
						jsonRes = {
							success: '1',
							result: {parent_category_details : parentDetails,category_list : cat_list},
							successMessage: 'Category List',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else
					{
						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Invalide Category" 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
				});
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorCode: "100", 
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});

// ------------------------------------------------------ Company Category Selection Module End  --------------------------------------------------------------//   


// ------------------------------------------------------ Company Team Permission Module Start  --------------------------------------------------------------//   

router.post('/company/team_permission', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		company_id   : req.body.company_id

	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){
 
		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{	

			HelperCompany.getTeamPermission(reqParams, function(perm_list){

				if(perm_list !== null)
				{
					jsonRes = {
						success: '1',
						result: {
							team_permission : perm_list
						},
						successMessage: 'Company Team Perimission List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: '1',
						result: {
							team_permission : []
						},
						successMessage: successMessage,
						errorMessage: "No result found" 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
			});
		
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}
	});
});



// ------------------------------------------------------ Company Team Permission Module End    --------------------------------------------------------------//   

module.exports = router;
