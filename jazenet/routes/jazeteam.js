require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();
const request = require('request');

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperJazeteam = require('../helpers/HelperJazeteam.js');

const { check, validationResult } = require('express-validator');

const dateFormat = require('dateformat');
const now = new Date();
const standardDT = dateFormat(now,'yyyy-mm-dd HH:MM:ss');
const crypto = require('crypto');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};
var stat;

var statRes = 422;
var jsonRes = {};

router.post('/jazeteam/invite_team',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('request_account_id').isNumeric(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		request_account_id    : req.body.request_account_id
	};


	if(parseInt(arrParams.request_account_id) !== 0  && parseInt(arrParams.account_id) !== 0){

		HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

			if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
			{

				HelperJazeteam.inviteToTeam(arrParams, function(retDetails){

					if(parseInt(retDetails) === 1)
					{	
						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Team invitiation sent.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else if(parseInt(retDetails) === 2)
					{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'This account does not have a team..',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else if(parseInt(retDetails) === 3)
					{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Request account not found.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else if(parseInt(retDetails) === 4)
					{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Pending team request. Waiting for the approval.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else if(parseInt(retDetails) === 5)
					{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Request account is already in your team.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else if(parseInt(retDetails) === 0)
					{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Unable to process request please try again.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				});
			}
			else
			{

				jsonRes = {
					success: '0',
					result: {},
					successMessage: successMessage,
					errorMessage: "Device not found." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		});

	}else{

		jsonRes = {
			success: '0',
			result: {},
			successMessage: successMessage,
			errorMessage: "Only registred users can process this request." 
		};

		res.status(statRes).json(jsonRes);
		res.end();

	}
	
});



router.post('/jazeteam/team_accept_reject',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success, 
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		id           : req.body.id,
		flag         : req.body.flag
	};


	if(parseInt(arrParams.request_account_id) !== 0  && parseInt(arrParams.account_id) !== 0){

		HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

			if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
			{

				HelperJazeteam.teamAcceptReject(arrParams, function(retDetails){

					if(parseInt(retDetails) === 1)
					{	
						var message = "Team invitation is accepted.";
						if(parseInt(arrParams.flag) === 2){
							message = "Team invitation is rejected.";
						}
						
						jsonRes = {
							success: '1',
							result: {},
							successMessage: message,
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();

					}
					else if(parseInt(retDetails) === 3)
					{

						jsonRes = {
							success: '0',
							result: {},
							successMessage: 'No result found',
							errorMessage: "" 
						};
						res.status(statRes).json(jsonRes);
						res.end();

					}
					else if(parseInt(retDetails) === 0)
					{

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'Unable to process request please try again.',
							errorMessage: "" 
						};
						res.status(201).json(jsonRes);
						res.end();
					}

				});
			}
			else
			{

				jsonRes = {
					success: '0',
					result: {},
					successMessage: successMessage,
					errorMessage: "Device not found." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		});

	}else{

		jsonRes = {
			success: '0',
			result: {},
			successMessage: successMessage,
			errorMessage: "Only registred users can process this request." 
		};

		res.status(statRes).json(jsonRes);
		res.end();

	}
	

});



module.exports = router;