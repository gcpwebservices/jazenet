require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();


const HelperJazeStore = require('../helpers/HelperJazeStore.js');
const HelperRestriction = require('../helpers/HelperRestriction.js');

const { check, validationResult } = require('express-validator');

const dateFormat = require('dateformat');
const now = new Date();
const standardDT = dateFormat(now,'yyyy-mm-dd HH:MM:ss');


var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};
var stat;

var statRes = 422;
var jsonRes = {};


router.post('/jazestore/jazestore_home',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),

	check('city_id').isNumeric(),
	check('state_id').isNumeric(),
	check('country_id').isNumeric(),

	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		city_id      : req.body.city_id,
		state_id     : req.body.state_id,
		country_id   : req.body.country_id,
		flag         : req.body.flag
	};

	var banners_list = [];

	banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/jazestore/banners/mobile/jazestore-slide1.jpg");
	banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/jazestore/banners/mobile/jazestore-slide2.jpg");
	banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/jazestore/banners/mobile/jazestore-slide3.jpg");

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazeStore.getJazeStoreGeo(arrParams, function(retProducts){

				if(retProducts !=null)
				{

					if(parseInt(arrParams.flag) === 0){
						Object.assign(retProducts[0],{jazestore_banners:banners_list});
					}else{
						Object.assign(retProducts[0],{jazestore_banners:[]});
					}
					
					jsonRes = {
						success: '1',
						result: {jaze_store: retProducts[0]},
						successMessage: 'JazeStore',
						errorMessage: "" 
					};
					res.status(201).json(jsonRes);
					res.end();

				}
				else
				{

					var obj_result = {
						special_offers : [],
						new_arrivals   : [],
						best_offers    : [],
						categories     : [],
						jazestore_banners:[],
					}

					jsonRes = {
						success: '1',
						result: {
							jaze_store:obj_result
						},
						successMessage: '',
						errorMessage: "No result found." 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
		
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazestore/category_list',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('parent_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		parent_id    : req.body.parent_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperJazeStore.getJazeStoreCategoryList(arrParams, function(retCategory){

				if(retCategory !=null){

					jsonRes = {
						success: '1',
						result: {
							jazestore_category_list: retCategory
						},
						successMessage: 'JazeStore Category List',
						errorMessage: "" 
						};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '0',
						result: {
							jazestore_category_list:{
								parent_category : {},
								child_list: []
							}
						},
						successMessage: '',
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
		
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazestore/search_product',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('keywords').isLength({ min: 1 }),
	check('country_id').isNumeric(),
	check('state_id').isNumeric(),
	check('city_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		keywords     : req.body.keywords,
		country_id   : req.body.country_id,
		state_id     : req.body.state_id,
		city_id      : req.body.city_id,
		flag         : req.body.flag
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			
			var arrFlags = [1,2,3];

			if(arrFlags.includes(parseInt(arrParams.flag)))
			{

				HelperJazeStore.searchJazeStoreProducts(arrParams, function(retSearched){

					HelperJazeStore.searchJazeStoreProductsCount(arrParams, function(retCount){




						if(retSearched !=null){

							jsonRes = {
								success: '1',
								result: {
									jazestore_product_details: retCount,
									jazestore_product: retSearched
								},
								successMessage: 'Jazestore product search.',
								errorMessage: "" 
								};
							res.status(201).json(jsonRes);
							res.end();

						}
						else
						{

							var obj_result = {
								keywords  : [],
								product   : [],
								company   : [],
								brand     : []
							};

							// var obj_result_count = {
							// 	product_count   : '0',
							// 	company_count   : '0',
							// 	brand_count     : '0'
							// };

							jsonRes = {
								success: '1',
								result: {
									jazestore_product_details: retCount,
									jazestore_product:obj_result
								},
								successMessage: '',
								errorMessage: "No result found." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}
				
					});
				});
			}
			else
			{

				jsonRes = {
					success: '0',
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();

			}


		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazestore/company_profile',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		company_id   : req.body.company_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			

			HelperJazeStore.getJazeStoreCompanyProfile(arrParams, function(retProfile){

				if(retProfile !=null){

					jsonRes = {
						success: '1',
						result: {
							jazestore_company_profile: retProfile
						},
						successMessage: 'Jazestore company profile.',
						errorMessage: "" 
						};
					res.status(201).json(jsonRes);
					res.end();

				}
				else
				{


					jsonRes = {
						success: '1',
						result: {
							jazestore_company_profile: []
						},
						successMessage: '',
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
		
			});
		

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazestore/company_category',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('company_id').isNumeric(),
	check('category_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		company_id   : req.body.company_id,
		category_id  : req.body.category_id
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			

			HelperJazeStore.getCompanyCategoryProducts(arrParams, function(retProfile){

				if(retProfile !=null){

					jsonRes = {
						success: '1',
						result: {category_product_list:retProfile},
						successMessage: 'Jazestore company category products.',
						errorMessage: "" 
						};
					res.status(201).json(jsonRes);
					res.end();

				}
				else
				{

					var ret_obj = {
						category_product_list : {},
						product_list : []
					}

					jsonRes = {
						success: '1',
						result: {
							category_product_list:ret_obj,
				
						},
						successMessage: '',
						errorMessage: "No result found." 
					};

					res.status(201).json(jsonRes);
					res.end();

				}
		
			});
		

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazestore/product_attributes',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('category_id').isNumeric(),
	check('country_id').isNumeric(),
	check('state_id').isNumeric(),
	check('city_id').isNumeric(),
	check('page').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		category_id  : req.body.category_id,
		country_id   : req.body.country_id,
		state_id     : req.body.state_id,
		city_id      : req.body.city_id,
		page         : req.body.page
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			
			HelperJazeStore.getProductsFromCategoryAttributes(arrParams, function(retProducts,pagination_details){

				if(parseInt(arrParams.page) === 0)
				{

					HelperJazeStore.getJazeStoreProductAttributes(arrParams, function(retAttributes){

						if(retAttributes !=null)
						{

							jsonRes = {
								success: '1',
								result: {
									page_details : pagination_details,								
									attributes   : retAttributes,
									products     : retProducts
								},
								successMessage: 'Jazestore prdouct attributes.',
								errorMessage: "" 
								};

							res.status(201).json(jsonRes);
							res.end();

						}
						else
						{

							var pagination_empty  = {
								total_page   : '0',
					        	current_page : '0',
					        	next_page    : '0',
					          	total_result : '0'
					        };

							jsonRes = {
								success: '1',
								result: {
									page_details : pagination_empty,								
									attributes   : [],
									products     : []
								},
								successMessage: 'No result found.',
								errorMessage: "" 
								};
							res.status(201).json(jsonRes);
							res.end();
						}
				
					});

				}
				else
				{

					jsonRes = {
						success: '1',
						result: {
							page_details : pagination_details,
							products     : retProducts,
							attributes   : []
						},
						successMessage: 'Jazestore prdouct attributes.',
						errorMessage: "" 
						};

					res.status(201).json(jsonRes);
					res.end();
				}

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazestore/search_attributes',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),

	check('city_id').isNumeric(),
	check('state_id').isNumeric(),
	check('country_id').isNumeric(),

	check('category_id').isNumeric(),
	check('attributes').isLength({ min: 1 }),

	check('page').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {

		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		city_id      : req.body.city_id,
		state_id     : req.body.state_id,
		country_id   : req.body.country_id,

		category_id  : req.body.category_id,
		attributes   : req.body.attributes,
		page         : req.body.page
		
	};
	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			
			HelperJazeStore.searchProductAttribtues(arrParams, function(retProducts,pagination_details){

				if(retProducts!=null)
				{

					var obj_result = {
						page_details : pagination_details,								
						products     : retProducts
					}

					jsonRes = {
						success: '1',
						result: {
							search_attributes_result : obj_result
						},
						successMessage: 'Jazestore attributes products.',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();

				}
				else
				{	

					var pagination_empty  = {
						total_page   : '0',
			        	current_page : '0',
			        	next_page    : '0',
			          	total_result : '0'
			        };

					var obj_result = {
						page_details : pagination_empty,								
						products     : []
					}

					jsonRes = {
						success: '1',
						result: {
							search_attributes_result : obj_result
						},
						successMessage: 'No result found',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();

				}
				

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});



});



router.post('/jazestore/brand_products',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('country_id').isNumeric(),
	check('state_id').isNumeric(),
	check('city_id').isNumeric(),
	check('brand_id').isNumeric(),
	check('flag').isNumeric(),
	check('page').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,		
		country_id   : req.body.country_id,		
		state_id     : req.body.state_id,		
		city_id      : req.body.city_id,		
		brand_id     : req.body.brand_id,		
		flag         : req.body.flag,
		page         : req.body.page,	
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			
			if(parseInt(arrParams.flag) === 3 || parseInt(arrParams.flag) === 4 )
			{


				HelperJazeStore.getProductsBrandPerID(arrParams, function(retProducts,retPaginate){

					if(retProducts !=null && retProducts.length > 0)
					{

						var control_obj = {
							controller_type        : '',
							attribute_id           : '',
							attribute_label        : '',
							home_status            : '',
							home_sort_order        : '',
							adv_status             : '',
							adv_sort_order         : '',
							attribute_option_label : '',
							items_value_list       : [],	
						};

						var arrControllers = [];

						arrControllers.push(control_obj);

						jsonRes = {
							success: '1',
							result: {
								page_details : retPaginate,		
								controllers  : 	arrControllers,					
								products     : retProducts
							},
							successMessage: 'Jazestore brand products.',
							errorMessage: "" 
							};

						res.status(201).json(jsonRes);
						res.end();

					}
					else
					{

						var pagination_empty  = {
							total_page   : '0',
				        	current_page : '0',
				        	next_page    : '0',
				          	total_result : '0'
				        };

						jsonRes = {
							success: '1',
							result: {
								page_details : pagination_empty,													
								products     : []
							},
							successMessage: 'No result found.',
							errorMessage: "" 
							};
						res.status(201).json(jsonRes);
						res.end();
					}
				

				});

			}
			else
			{
				jsonRes = {
					success: '0',
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});



});




router.post('/jazestore/cart_details',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id		
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			
			HelperJazeStore.getCartList(arrParams, function(retCart){

				if(retCart!=null)
				{

		
					jsonRes = {
						success: '1',
						result: {
							cart_details : retCart
						},
						successMessage: 'Jazestore cart details.',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();

				}
				else
				{	

		

					jsonRes = {
						success: '1',
						result: {
							cart_details : []
						},
						successMessage: 'No result found',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();

				}
				

			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});



});


router.post('/jazestore/add_to_cart',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('product_id').isNumeric(),
	check('company_id').isNumeric(),
	check('qty').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		product_id  : req.body.product_id,		
		company_id  : req.body.company_id,		
		qty         : req.body.qty		
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			
			HelperJazeStore.addtoCart(arrParams, function(retCart){
	
				if(parseInt(retCart) === 1)
				{

					HelperJazeStore.getCartList(arrParams, function(retCartList){

						jsonRes = {
							success: '1',
							result: {
								cart_details : retCartList
							},
							successMessage: 'Item is successfully added to cart.',
							errorMessage: "" 
						};

						res.status(201).json(jsonRes);
						res.end();
					});

				}
				else
				{	

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Unable to process request. Please try again.',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
				
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazestore/update_cart',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('item_id').isNumeric(),
	check('qty').isInt({gt: 0})

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		item_id     : req.body.item_id,			
		qty         : req.body.qty		
	};

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			
			HelperJazeStore.UpdateCart(arrParams, function(updateStat){
	
				if(parseInt(updateStat) === 1)
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Cart is successfully updated.',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();

				}
				else if(parseInt(updateStat) === 2)
				{	

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'No result found',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Unable to process request. Please try again.',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
				
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazestore/remove_to_cart',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id,
		item_id     : req.body.item_id,		
		flag        : req.body.flag		
	};

	//flag 1 = empty cart
	//flag 2 = remove item

	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			
			if(parseInt(arrParams.flag) === 1 || parseInt(arrParams.flag) === 2)
			{
				HelperJazeStore.removeToCart(arrParams, function(updateStat){
						
					var sucMessage = 'Cart is now empty.';
			
					if(parseInt(updateStat) === 1)
					{

						if(parseInt(arrParams.flag) === 2)
						{	
							var item_id_split = arrParams.item_id.split(',');
							sucMessage = 'Item is successfully removed';
							if(parseInt(item_id_split.length) > 1){
								sucMessage = 'Items are successfully removed';
							}
						}

					}
					else if(parseInt(updateStat) === 0)
					{
						sucMessage = 'Unable to process request. Please try again.';
					}
					else
					{
						sucMessage = 'No result found.';
					}

					jsonRes = {
						success: '1',
						result: {},
						successMessage: sucMessage,
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();
					
				});
			}
			else
			{
				jsonRes = {
					success: '0',
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
		

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});




router.post('/jazestore/checkout_information',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {
		api_token   : req.body.api_token,
		account_id  : req.body.account_id
	};
	
	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			
			HelperJazeStore.getCheckoutInformation(arrParams, function(retInformation){
							
				jsonRes = {
					success: '1',
					result: {checkout_information: retInformation},
					successMessage: 'Checkout information',
					errorMessage: "" 
				};

				res.status(201).json(jsonRes);
				res.end();
					
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});


router.post('/jazestore/checkout',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('item_id').isLength({ min: 1 }),
	check('payment_id').isNumeric(),
	check('payment_type').isNumeric(),
	check('delivery_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		item_id      : req.body.item_id.split(','),
		payment_id   : req.body.payment_id,
		payment_type : req.body.payment_type,
		delivery_id  : req.body.delivery_id
	};
	
	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			
			HelperJazeStore.processCheckout(arrParams, function(retstatus){

				if(parseInt(retstatus) === 1)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Order has been placed successfully.',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
				else if(parseInt(retstatus) === 2)
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: '',
						errorMessage: "No result found." 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
				else
				{
					jsonRes = {
						success: '1',
						result: {},
						successMessage: '',
						errorMessage: "Something went terribly wrong. Would like to try again?" 
					};

					res.status(201).json(jsonRes);
					res.end();
				}	
					
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazestore/order_list',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id
	};
	
	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			
			HelperJazeStore.getOrderList(arrParams, function(retstatus){

				if(retstatus!=null)
				{


					jsonRes = {
						success: '1',
						result: {
							order_list:retstatus,
						},
						successMessage: 'Order List',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();
				
				}
				else
				{
					jsonRes = {
						success: '1',
						result: {order_list:[]},
						successMessage: 'No result found.',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
	
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

})

router.post('/jazestore/order_details',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('order_id').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		order_id     : req.body.order_id
	};
	
	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			
			HelperJazeStore.getOrderDetailsPerId(arrParams, function(retstatus){

				if(retstatus!=null)
				{

					jsonRes = {
						success: '1',
						result: {order_details:retstatus},
						successMessage: 'Order Details',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();
				
				}
				else
				{
					jsonRes = {
						success: '1',
						result: {order_list:[]},
						successMessage: 'No result found.',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
	
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});



router.post('/jazestore/search_keywords',[

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('country_id').isNumeric(),
	check('state_id').isNumeric(),
	check('city_id').isNumeric(),
	check('keywords').isLength({ min: 1 }),
	check('page').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {}, 
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var arrParams = {
		api_token    : req.body.api_token,
		account_id   : req.body.account_id,
		country_id   : req.body.country_id,
		state_id     : req.body.state_id,
		city_id      : req.body.city_id,
		keywords     : req.body.keywords,
		page         : req.body.page
	};
	
	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			
			HelperJazeStore.getKeywordSearch(arrParams, function(retProducts,pagination_details){

				if(retProducts!=null)
				{

					jsonRes = {
						success: '1',
						result: {
							page_details:pagination_details,
							search_keywords:retProducts
						},
						successMessage: 'Searched Keywords',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();
				
				}
				else
				{		
					var pagination_empty  = {
						total_page   : '0',
			        	current_page : '0',
			        	next_page    : '0',
			          	total_result : '0'
			        };

					jsonRes = {
						success: '1',
						result: {
							page_details:pagination_empty,
							search_keywords:[]
						},
						successMessage: 'No result found.',
						errorMessage: "" 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
	
			});

		}
		else
		{
			jsonRes = {
				success: '0',
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();
		}

	});

});






module.exports = router;