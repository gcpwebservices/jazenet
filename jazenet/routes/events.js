require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();


const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperEvents = require('../helpers/HelperEvents.js');

const { check, validationResult } = require('express-validator');



var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};



router.post('/events/events_list', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('city_id').isNumeric(),
	check('category_id').isNumeric(),
	check('flag').isNumeric()
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		date          : req.body.date,
		city_id       : req.body.city_id,
		category_id   : req.body.category_id,
		flag   : req.body.flag
	};


	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperEvents.getEventsList(reqParams, function(retEvents,retEventCalendar){

				if(retEvents!=null){

					jsonRes = {
						success: '1',
						result: {
							event_calendar:retEventCalendar,
							events_list:retEvents
						},
						successMessage: 'Events List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {
							event_calendar:retEventCalendar,
							events_list	:[]
						},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/events/event_details', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('event_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		date          : req.body.date,
		city_id       : req.body.city_id,
		event_id      : req.body.event_id,
		latitude      : req.body.lat,
		longitude     : req.body.lon
	};


	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperEvents.getEventDetails(reqParams, function(retEvents){

				if(retEvents!=null){

					jsonRes = {
						success: '1',
						result: {
							event_details:retEvents
						},
						successMessage: 'Events List',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {
							events_list	:[]
						},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/events/add_to_diary', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('event_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		event_id      : req.body.event_id,
		flag          : req.body.flag
	};


	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperEvents.addToDiaryEvents(reqParams, function(retEvents){

				if(retEvents==1){

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Event is successfully saved to diary.',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else if(retEvents==2){

					jsonRes = {
						success: '1',
						result: {},
						successMessage: successMessage,
						errorMessage: "Event is already in the diary." 
					};

					res.status(201).json(jsonRes);
					res.end();

				}else if(retEvents==3){

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Event not found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();

				}else if(retEvents==4){

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Invalid flag provided." 
					};

					res.status(statRes).json(jsonRes);
					res.end();

				}else if(retEvents==5){

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Event is successfully removed to diary.',
						errorMessage: errorMessage 
					};
					
					res.status(201).json(jsonRes);
					res.end();


				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/events/event_category', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('category_id').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		category_id   : req.body.category_id
	};

	if(parseInt(reqParams.account_id) !==0 )
	{
		HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

			if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
			{

				HelperEvents.getEventCategoryList(reqParams, function(retEvents,retVideo){

					if(retEvents!=null){

						jsonRes = {
							success: '1',
							result: {
								event_category:retEvents,
							},
							successMessage: 'Events Category List',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();

					}else{

						jsonRes = {
							success: '1',
							result: {
								event_category	:[],
							},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Device not found." 
				};

				res.status(statRes).json(jsonRes);
				res.end();

			}
		});

	}
	else
	{
		HelperEvents.getEventCategoryList(reqParams, function(retEvents,retVideo){

			if(retEvents!=null){

				jsonRes = {
					success: '1',
					result: {
						event_category:retEvents,
					},
					successMessage: 'Events Category List',
					errorMessage: errorMessage 
				};
				res.status(201).json(jsonRes);
				res.end();

			}else{

				jsonRes = {
					success: '1',
					result: {
						event_category	:[],
					},
					successMessage: successMessage,
					errorMessage: "No result found." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}
		});
	}
});




router.post('/events/sort_event_category', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('category_id').isLength({ min: 1 })


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		category_id   : req.body.category_id
	};


	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperEvents.updateEventCategorySorting(reqParams, function(retStatus){

				if(parseInt(retStatus) == 1){

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'Event category sorting is successfully saved.',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});


});



router.post('/events/events_year', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('city_id').isNumeric(),
	check('year').isNumeric(),

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		year          : req.body.year,
		city_id       : req.body.city_id
	};


	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperEvents.getEventsListYear(reqParams, function(retEventCalendar){

				if(retEventCalendar!=null){

					jsonRes = {
						success: '1',
						result: {
							event_calendar:retEventCalendar,
	
						},
						successMessage: 'Events year list',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: '1',
						result: {
							event_calendar:[],
						},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(201).json(jsonRes);
					res.end();
				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


module.exports = router;