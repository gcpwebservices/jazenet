require('events').EventEmitter.prototype._maxListeners = 1000;
const express = require('express');
const router = express.Router();
const request = require('request');

const HelperRestriction = require('../helpers/HelperRestriction.js');
const HelperJazenet = require('../helpers/HelperJazenet.js');
const HelperAccount = require('../helpers/HelperAccount.js');

const mime = require('mime-types');
const multer = require('multer');
const fs = require('fs');


const { check, validationResult } = require('express-validator');

const dateFormat = require('dateformat');
const now = new Date();
const standardDT = dateFormat(now,'yyyy-mm-dd HH:MM:ss');
const crypto = require('crypto');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};

const urlPath = G_STATIC_IMG_PATH+"individual_account/members/";
const urlPath_thumb = G_STATIC_IMG_PATH+"individual_account/members_thumb/";

const STATIC_IMG_URL = G_STATIC_IMG_URL+"uploads/jazenet/individual_account/members_thumb/";


router.post('/account/me_info', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperAccount.getMeAccountDetails(reqParams, function(retSearched){

				if(retSearched!=null){
					
					jsonRes = {
						success: '1',
						result: retSearched,
						successMessage: 'Essential info',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

router.post('/account/update_essential_info', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id : req.body.account_id,
		first_name : req.body.first_name,
		last_name  : req.body.last_name,
		dob  	   : req.body.dob,
		mobile_country_code : req.body.mobile_country_code,
		mobile_no      : req.body.mobile_no,
		nationality_id : req.body.nationality_id,
		country_id     : req.body.country_id,
		state_id       : req.body.state_id,
		city_id        : req.body.city_id,
		area_id        : req.body.area_id,
		currency_id    : req.body.currency_id
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperAccount.updateEssentials(reqParams, function(retUdpate){
	
				if(parseInt(retUdpate) > 0){

					HelperAccount.getEssentialDetails(reqParams, function(retEssentials){

						jsonRes = {
							success: '1',
							result: {essential_info:retEssentials},
							successMessage: 'Essential information are successfully updated.',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					});

				}else{

			 		jsonRes = {
					success: success,
			 			result: {},
							successMessage: successMessage,
				 			errorMessage: "Unable to process this request. Please try again." 
			 		};

			 		res.status(statRes).json(jsonRes);
					res.end();
	 			}

		 	});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


//------------------------------------------------------------------- Link Account Module Start ---------------------------------------------------------------------------//

router.post('/account/account_list', [

	check('api_token').isLength({ min: 1 }),
	check('master_account_id').isLength({ min: 1 }),
	check('account_id').isLength({ min: 1 }),
	check('account_type').isLength({ min: 1 })

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		master_account_id : req.body.master_account_id,
		account_id 	 : req.body.account_id,
		account_type : req.body.account_type,
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperAccount.getMasterChildLinkAccountList(reqParams, function(retChilds){

				if(retChilds!=null){

					jsonRes = {
						success: '1',
						result: retChilds,
						successMessage: 'User Accounts',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/account/add_link_account', [

	check('app_id').isNumeric(),
	check('api_token').isLength({ min: 1 }),
	check('master_account_id').isNumeric(),
	check('account_id').isNumeric(),
	check('jazemail_id').isLength({ min: 1 }),
	check('password').isLength({ min: 3 })
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		app_id  : req.body.app_id,
		api_token  : req.body.api_token,
		master_account_id : req.body.master_account_id,
		account_id 		: req.body.account_id,
		jazemail_id		: req.body.jazemail_id,
		password		: crypto.createHash('md5').update(req.body.password).digest("hex")
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperAccount.saveLinkAccountDetails(reqParams, function(retChilds){

				if(parseInt(retChilds)==1){

					HelperAccount.getMasterChildLinkAccountList(reqParams, function(retChilds){

						jsonRes = {
							success: '1',
							result: retChilds,
							successMessage: 'Account is successfully added to link accounts.',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();

					});
					
				}else if(parseInt(retChilds)==2){

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Account already exsist!." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
					
				}else if(parseInt(retChilds)==3){

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "invalid credential details. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				
				}else {

				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Unable to process request. Please try again." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/account/remove_link_account', [

	check('app_id').isNumeric(),
	check('api_token').isLength({ min: 1 }),
	check('master_account_id').isNumeric(),
	check('account_id').isNumeric(),
	check('link_group_id').isNumeric(),
	check('link_account_id').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {	
		app_id  : req.body.app_id,
		api_token  : req.body.api_token,
		master_account_id : req.body.master_account_id,
		account_id 		: req.body.account_id,
		link_group_id		: req.body.link_group_id,
		link_account_id		: req.body.link_account_id
	};


	
	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperAccount.removeLinkAccountDetails(reqParams, function(retChilds){

				if(parseInt(retChilds)==1){

					HelperAccount.getMasterChildLinkAccountList(reqParams, function(retChilds){

						jsonRes = {
							success: '1',
							result: retChilds,
							successMessage: 'Account is successfully removed to link accounts.',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();

					});
					
				}else {
					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();

				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

//------------------------------------------------------------------- Link Account Module End ---------------------------------------------------------------------------//

router.post('/account/billing_shipping', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isLength({ min: 1 }),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		id                    : req.body.id,
		api_token             : req.body.api_token,
		account_id            : req.body.account_id,
		first_name            : req.body.first_name,
		last_name             : req.body.last_name,
		mobile_country_code   : req.body.mobile_country_code,
		mobile_no             : req.body.mobile_no,
		landline_country_code : req.body.landline_country_code,
		landline_no           : req.body.landline_no,
		country            : req.body.country_id,
		state              : req.body.state_id,
		city               : req.body.city_id,
		area               : req.body.area_id,
		street_name           : req.body.street_name,
		street_no             : req.body.street_no,
		building_name         : req.body.building_name,
		building_no           : req.body.building_no,
		shipping_note         : req.body.shipping_note,
		landmark              : req.body.landmark,
		location_type         : req.body.location_type,
		is_default            : req.body.is_default,
		flag                  : req.body.flag
	};

	// 1 = add
	// 2 = update
	// 3 = delete

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			if(parseInt(req.body.flag) == 1)
			{
				HelperAccount.addBillingAddress(reqParams, function(retStatus){

					if(retStatus == 1){
						setTimeout(function(){ 
							HelperAccount.getShippingDetails(reqParams, function(retBilling){
								jsonRes = {
									success: '1',
									result: {billing_shipping:retBilling},
									successMessage: 'New billing address is successfully saved.',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();
							});
						}, 300);
					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
			
				});
			}
			else if(parseInt(req.body.flag) == 2)
			{

				HelperAccount.updateBillingAddress(reqParams, function(retStatus){

					if(retStatus == 1){
						setTimeout(function(){ 
							HelperAccount.getShippingDetails(reqParams, function(retBilling){

								jsonRes = {
									success: '1',
									result: {billing_shipping:retBilling},
									successMessage: 'Billing address is successfully updated.',
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();
							});
						}, 300);

					}
					else if(retStatus == 2)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					
					}

				});


			}
			else if(parseInt(req.body.flag) == 3)
			{
				var group_ids = req.body.id.split(",");
				var sucMess = "Billing address is successfully deleted.";
				if(group_ids.length > 1){
					sucMess = "Billing address are successfully deleted.";
				}
				HelperAccount.delBillingAddress(group_ids,reqParams, function(retStatus){
					if(retStatus == 1){

						setTimeout(function(){ 
							HelperAccount.getShippingDetails(reqParams, function(retBilling){

								jsonRes = {
									success: '1',
									result: {billing_shipping:retBilling},
									successMessage: sucMess,
									errorMessage: errorMessage 
								};
								res.status(201).json(jsonRes);
								res.end();
							});
						}, 500);

					}
					else if(retStatus == 2)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					
					}
				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});



router.post('/account/card_payment', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isLength({ min: 1 }),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		id            : req.body.id,
		api_token     : req.body.api_token,
		account_id    : req.body.account_id,
		bank_name     : req.body.bank_name,
		card_name     : req.body.card_name,
		card_no       : req.body.card_no,
		exp_month     : req.body.exp_month,
		exp_year      : req.body.exp_year,
		csv           : req.body.csv,
		is_default    : req.body.is_default,
		flag          : req.body.flag
	};

	// 1 = add
	// 2 = update
	// 3 = delete

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			if(parseInt(req.body.flag) == 1)
			{
				HelperAccount.addCardDetails(reqParams, function(retStatus){

					if(retStatus == 1){
						setTimeout(function(){ 
							HelperAccount.getCreditCardDetails(reqParams, function(retCard){
								HelperAccount.getBankDetails(reqParams, function(retBank){
									var obj_payment = {
										card:retCard,
										bank:retBank
									};	
									jsonRes = {
										success: '1',
										result: {payment_info:obj_payment},
										successMessage: sucMess,
										errorMessage: errorMessage 
									};
									res.status(201).json(jsonRes);
									res.end();
								});
							});
						}, 300);
					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
			
				});
			}
			else if(parseInt(req.body.flag) == 2)
			{

				HelperAccount.updateCardDetails(reqParams, function(retStatus){

					if(retStatus == 1){
						setTimeout(function(){ 
							HelperAccount.getCreditCardDetails(reqParams, function(retCard){
								HelperAccount.getBankDetails(reqParams, function(retBank){
									var obj_payment = {
										card:retCard,
										bank:retBank
									};	
									jsonRes = {
										success: '1',
										result: {payment_info:obj_payment},
										successMessage: sucMess,
										errorMessage: errorMessage 
									};
									res.status(201).json(jsonRes);
									res.end();
								});
							});
						}, 300);

					}
					else if(retStatus == 2)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					
					}

				});


			}
			else if(parseInt(req.body.flag) == 3)
			{
				var group_ids = req.body.id.split(",");
				var sucMess = "Bank details is successfully deleted.";
				if(group_ids.length > 1){
					sucMess = "Bank details are successfully deleted.";
				}

				HelperAccount.delCardDetails(group_ids,reqParams, function(retStatus){
					if(retStatus == 1){

						setTimeout(function(){ 
							HelperAccount.getCreditCardDetails(reqParams, function(retCard){
								HelperAccount.getBankDetails(reqParams, function(retBank){
									var obj_payment = {
										card:retCard,
										bank:retBank
									};	
									jsonRes = {
										success: '1',
										result: {payment_info:obj_payment},
										successMessage: sucMess,
										errorMessage: errorMessage 
									};
									res.status(201).json(jsonRes);
									res.end();
								});
							});
						}, 500);

					}
					else if(retStatus == 2)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					
					}
				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/account/bank_payment', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isLength({ min: 1 }),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		id                    : req.body.id,
		api_token             : req.body.api_token,
		account_id            : req.body.account_id,
		holder_name           : req.body.holder_name,
		account_no            : req.body.account_no,
		bank_name             : req.body.bank_name,
		branch_name           : req.body.branch_name,
		iban_no               : req.body.iban_no,
		swift_code            : req.body.swift_code,
		is_default            : req.body.is_default,
		flag                  : req.body.flag
	};

	// 1 = add
	// 2 = update
	// 3 = delete

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			if(parseInt(req.body.flag) == 1)
			{
				HelperAccount.addBankDetails(reqParams, function(retStatus){

					if(retStatus == 1){
						setTimeout(function(){ 
							HelperAccount.getBankDetails(reqParams, function(retBank){
								HelperAccount.getCreditCardDetails(reqParams, function(retCard){
									var obj_payment = {
										bank:retBank,
										card:retCard
									};	
									jsonRes = {
										success: '1',
										result: {payment_info:obj_payment},
										successMessage: sucMess,
										errorMessage: errorMessage 
									};
									res.status(201).json(jsonRes);
									res.end();
								});
							});
						}, 300);
					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
			
				});
			}
			else if(parseInt(req.body.flag) == 2)
			{

				HelperAccount.updateBankDetails(reqParams, function(retStatus){

					if(retStatus == 1){
						setTimeout(function(){ 
							HelperAccount.getBankDetails(reqParams, function(retBank){
								HelperAccount.getCreditCardDetails(reqParams, function(retCard){
									var obj_payment = {
										bank:retBank,
										card:retCard
									};	
									jsonRes = {
										success: '1',
										result: {payment_info:obj_payment},
										successMessage: sucMess,
										errorMessage: errorMessage 
									};
									res.status(201).json(jsonRes);
									res.end();
								});
							});
						}, 300);

					}
					else if(retStatus == 2)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					
					}

				});


			}
			else if(parseInt(req.body.flag) == 3)
			{
				var group_ids = req.body.id.split(",");
				var sucMess = "Bank details is successfully deleted.";
				if(group_ids.length > 1){
					sucMess = "Bank details are successfully deleted.";
				}

				HelperAccount.delBankDetails(group_ids,reqParams, function(retStatus){
					if(retStatus == 1){

						setTimeout(function(){ 
							HelperAccount.getBankDetails(reqParams, function(retBank){
								HelperAccount.getCreditCardDetails(reqParams, function(retCard){
									var obj_payment = {
										bank:retBank,
										card:retCard
									};	
									jsonRes = {
										success: '1',
										result: {payment_info:obj_payment},
										successMessage: sucMess,
										errorMessage: errorMessage 
									};
									res.status(201).json(jsonRes);
									res.end();
								});
							});
						}, 500);

					}
					else if(retStatus == 2)
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process this request please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					
					}
				});
			}
			else
			{
				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


//------------------------------------------------------------------- Security Module Start ---------------------------------------------------------------------------//

router.post('/account/update_emailid', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('old_email').isLength({ min: 1 }),
	check('new_email').isLength({ min: 1 }),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id 		: req.body.account_id,
		old_email		: req.body.old_email,
		new_email		: req.body.new_email
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperAccount.updateAccountEmailId(reqParams, function(retChilds){

				if(parseInt(retChilds)==1){

					jsonRes = {
						success: '1',
						result: {new_email : reqParams.new_email.toString() },
						successMessage: 'email successfully updated.',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
					
				}else if(parseInt(retChilds)==2){

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "email not valid." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
					
				}else if(parseInt(retChilds)==3){

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				
				}else {

				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Unable to process request. Please try again." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

router.post('/account/update_mobile', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('old_code_id').isNumeric(),
	check('old_number').isNumeric(),
	check('new_code_id').isNumeric(),
	check('new_number').isNumeric(),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id 		: req.body.account_id,
		old_code_id		: req.body.old_code_id,
		old_number		: req.body.old_number,
		new_code_id		: req.body.new_code_id,
		new_number		: req.body.new_number
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperAccount.updateAccountMobileNumber(reqParams, function(retChilds){

				if(parseInt(retChilds)==1){

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'mobile number successfully updated.',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
					
				}else if(parseInt(retChilds)==2){

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "invalid mobile number." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
					
				}else if(parseInt(retChilds)==3){

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				
				}else {

				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Unable to process request. Please try again." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});

router.post('/account/update_securitypin', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('old_pin').isLength({ min: 1 }),
	check('new_pin').isLength({ min: 1 }),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  : req.body.api_token,
		account_id 	: req.body.account_id,
		old_pin		: crypto.createHash('md5').update(req.body.old_pin).digest("hex"),
		new_pin		: crypto.createHash('md5').update(req.body.new_pin).digest("hex")
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperAccount.updateAccountSecurityPin(reqParams, function(retChilds){

				if(parseInt(retChilds)==1){

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'security pin successfully updated.',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
					
				}else if(parseInt(retChilds)==2){

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "invalid security pin." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
					
				}else if(parseInt(retChilds)==3){

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				
				}else {

				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Unable to process request. Please try again." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


router.post('/account/update_password', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('old_password').isLength({ min: 1 }),
	check('new_password').isLength({ min: 1 }),
	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token  			: req.body.api_token,
		account_id 			: req.body.account_id,
		old_password		: crypto.createHash('md5').update(req.body.old_password).digest("hex"),
		new_password		: crypto.createHash('md5').update(req.body.new_password).digest("hex")
	};

	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{
			HelperAccount.updateAccountPassword(reqParams, function(retChilds){

				if(parseInt(retChilds)==1){

					jsonRes = {
						success: '1',
						result: {},
						successMessage: 'password successfully updated.',
						errorMessage: errorMessage 
					};
					res.status(201).json(jsonRes);
					res.end();
					
				}else if(parseInt(retChilds)==2){

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "invalid password." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
					
				}else if(parseInt(retChilds)==3){

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "Unable to process request. Please try again." 
					};

					res.status(statRes).json(jsonRes);
					res.end();
				
				}else {

				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Unable to process request. Please try again." 
				};

				res.status(statRes).json(jsonRes);
				res.end();
			}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});


//------------------------------------------------------------------- Security Module End ---------------------------------------------------------------------------//



//------------------------------------------------------------------- Profile Logo Module Start ---------------------------------------------------------------------------//

var storage = multer.diskStorage({
	destination: function (req, file, callback) {
	
	function asyncFunction (item, cb) {

	  	setTimeout(() => {
		
			callback(null, urlPath);

			cb();
	  	}, 100);
	}

	let requests = 0;

		if(req.body.account_id !=''){

			return new Promise((resolve) => {

				requests = req.body.account_id;
			
		  		asyncFunction(requests, resolve);

		    });

			Promise.all(requests);
		}
  },
  	filename: function (req, file, callback) {
		
  		var ext = getExtension(file.originalname);
  		var cleanName = getCleanFileName(file.originalname);
  		var completeFileName = req.body.account_id+'_'+Date.now()+'.'+ext;

    	callback(null, completeFileName);
  	}
});


router.post('/account/update_profile_pic', function(req, res) {

	var upload = multer({
		storage: storage,
	}).single('file');

	upload(req, res, function(err) {

		console.log(req.file);
		if(req.file.filename !== "")
		{
			//copy file in thumb path
			fs.copyFile(req.file.path,urlPath_thumb+'thumb_'+req.file.filename, (err) => {

					var reqParams = {
						filename :req.file.filename,
						ext		 :req.file.filename.split(".")[1],
						account_id : req.body.account_id
					};
					
					HelperAccount.updateProfilePic(reqParams, function(retCard){

						if(parseInt(retCard) == 1)
						{
							var results = {
								file_name:reqParams.filename,
								file_type:reqParams.ext,
								file_url: STATIC_IMG_URL+'thumb_'+reqParams.filename
							};

							jsonRes = {
								success: '1',
								result: {profile_image:results},
								successMessage: 'Profile picture is successfully updated.',
								errorMessage: errorMessage 
							};
							res.status(201).json(jsonRes);
							res.end();

						}
						else if(parseInt(retCard) == 2)
						{
							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "Unable to process request. Please try again." 
							};

							res.status(statRes).json(jsonRes);
							res.end();

						}
						else
						{
							jsonRes = {
								success: success,
								result: {},
								successMessage: successMessage,
								errorMessage: "No result found." 
							};

							res.status(statRes).json(jsonRes);
							res.end();
						}

					});	
			});
		}
	})
});



router.post('/account/job_profile', [

	check('api_token').isLength({ min: 1 }),
	check('account_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var reqParams = {		
		api_token          : req.body.api_token,
		account_id         : req.body.account_id,

		profile_logo       : req.body.profile_logo_filename,
		profile_logo_ext   : req.body.profile_logo_filename_ext,
		first_name         : req.body.first_name,
		last_name          : req.body.last_name,
		professional_title : req.body.professional_title,
		gender             : req.body.gender,
		nationality        : req.body.nationality_id,
		work_experience    : req.body.work_experience,
		notice_period      : req.body.notice_period,
		education_level    : req.body.education_level,
		location           : req.body.location,
		commitment         : req.body.commitment,
		expectation        : req.body.expectation,
		skill              : req.body.skill,
		about              : req.body.about,
		comment            : req.body.comment,

		jaze_call_status   : req.body.jaze_call_status,
		jaze_chat_status   : req.body.jaze_chat_status,
		jaze_mail_status   : req.body.jaze_mail_status,
		flag               : req.body.flag
	};


	HelperRestriction.checkParametersApiToken(reqParams, function(queParam){

		if(typeof queParam !== 'undefined' && queParam !== null && parseInt(queParam.length) > 0)
		{

			HelperAccount.addJobProfile(reqParams, function(retStatus){

				if(parseInt(reqParams.flag) == 0)
				{

					if(retStatus!==3){

						jsonRes = {
							success: '1',
							result: {
								job_profile:retStatus
							},
							successMessage: 'Job Profile.',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();
					}
					else
					{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Job profile not found. Please create a new one." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}
			

				}
				else
				{

					if(retStatus==1){

						jsonRes = {
							success: '1',
							result: {},
							successMessage: 'New job profile is successfully saved.',
							errorMessage: errorMessage 
						};
						res.status(201).json(jsonRes);
						res.end();

					}else if(retStatus==2){

						jsonRes = {
							success: '1',
							result: {},
							successMessage: successMessage,
							errorMessage: "job profile is successfully updated." 
						};

						res.status(201).json(jsonRes);
						res.end();

					}else if(retStatus==3){

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Job profile not found. Please create a new one." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}else if(retStatus==4){

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Job profile already exist in this account." 
						};

						res.status(statRes).json(jsonRes);
						res.end();

					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "Unable to process request. Please try again." 
						};

						res.status(statRes).json(jsonRes);
						res.end();
					}

				}
			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
			};

			res.status(statRes).json(jsonRes);
			res.end();

		}
	});
});



var storageJobProfile = multer.diskStorage({
	destination: function (req, file, callback) {
	
	function asyncFunction (item, cb) {
	  	setTimeout(() => {


			var storagePath = G_STATIC_IMG_PATH+"individual_account/jobs/"

			var inner_path = storagePath+req.body.account_id+'/';;

			if (!fs.existsSync(inner_path)){
				fs.mkdirSync(inner_path,'0757', true); 
			}

			callback(null, inner_path);

			cb();
	  	}, 100);
	}

	let requests = 0;

		if(req.body.account_id !=''){

			return new Promise((resolve) => {
				requests = req.body.account_id;
		  		asyncFunction(requests, resolve);
		    });

			Promise.all(requests);
		}
  },
  	filename: function (req, file, callback) {

  		var ext = getExtension(file.originalname);
  		var cleanName = getCleanFileName(file.originalname);
  		var completeFileName = req.body.account_id+'_'+Date.now()+'.'+ext;

    	callback(null, completeFileName);
  	}
});


router.post('/account/upload_job_profile_pic', function(req, res) {

	var upload = multer({
		storage: storageJobProfile,
	}).single('file');

	upload(req, res, function(err) {

 	 	var extName = req.file.filename.split(".");
 	 	var filename = extName[extName.length - 2];

	 	var extArray = req.file.filename.split(".");
 	 	var extention = extArray[extArray.length - 1];

 	  	var file_properties = req.file.mimetype.split("/");

 		var base_url = '';
        var filetype = '';

        var url_path = G_STATIC_IMG_URL+'uploads/jazenet/individual_account/jobs/'

	 	filetype = file_properties[0]

 		var complete_filename = filename+'.'+extention;

 		
		base_url = url_path+req.body.account_id+'/'+complete_filename;

        HelperAccount.getFileTypes('.'+extention, function(reType){

            var results = {
                file_name:filename,
                file_format:filetype,
                file_type:reType,
                extention:extention,
                file_path:base_url
            };
           
            jsonRes = {
                success: '1',
                result: results,
                successMessage: 'File is successfully uploaded.',
                errorMessage: errorMessage 
            };
            res.status(201).json(jsonRes);
            res.end();
        });

	})
});


function getExtension(path) {
    var basename = path.split(/[\\/]/).pop(),  // extract file name from full path ...
                                             	 // (supports `\\` and `/` separators)
        pos = basename.lastIndexOf(".");       // get last position of `.`
    if (basename === "" || pos < 1)            // if file name is empty or ...
        return "";                             //  `.` not found (-1) or comes first (0)
    return basename.slice(pos + 1);            // extract extension ignoring `.`
}

function getFilename(path) {
    var basename = path.split(/[\\/]/).pop(),      
        pos = basename.split('.'); 
    if (basename === "" || pos.length < 1)           
        return "";                          
    return pos[0];            
}


function getCleanFileName(path) {

	var basename = path.split(/\.(?=[^\.]+$)/);
	var cleanString = basename[0].replace(/[|&;$%@"<>()+1234567890,]/g, "");
         
    return cleanString;           
}

//------------------------------------------------------------------- Profile Logo Module End ---------------------------------------------------------------------------//

module.exports = router;
