require('events').EventEmitter.prototype._maxListeners = 1000;

const express = require('express');

const router = express.Router();
const { check, validationResult } = require('express-validator');

//const serviceAccount = require('../service_account/jazenet-pushnotification-firebase-adminsdk-esihm-cb1e468724.json');
//const fcm = require('fcm-notification');
//const FCM = new fcm(serviceAccount);

const HelperNotification = require('../helpers/HelperWebNotification.js');

const dateFormat = require('dateformat');
const now = new Date();
const standardDT = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
const newdateForm = dateFormat(standardDT,'yyyy-mm-dd HH:MM:ss');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};
var statRes = 422;


router.post('/notification/webnotification',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);

  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  account_id = req.body.account_id;
	var  api_token = req.body.api_token;
	

	var arrParams = {
		request_account_id:account_id,
		api_token:api_token
	};


	var	template = {
		data: {   
			title : 'test web title',
			body : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
			sender_logo:G_STATIC_IMG_URL+'uploads/jazenet/jazemail/41014/456.jpg',
		}
	};	


	HelperNotification.getFcmToken(template,arrParams, function(retResponse){


		if(retResponse!=null)
		{

			jsonRes = {
				success: '1',
				result: retResponse,
				successMessage: 'Web Push Notification',
				errorMessage: errorMessage 
			};

			res.status(201).json(jsonRes);
			res.end();


		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "No result found." 
	  		};
  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

});


module.exports = router;