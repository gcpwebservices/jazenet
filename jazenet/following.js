require('events').EventEmitter.prototype._maxListeners = 1000;
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const express = require('express');
const router = express.Router();

const HelperRestriction = require('./helpers/HelperRestriction.js');
const HelperFollowing = require('./helpers/HelperFollowing.js');

const { check, validationResult } = require('express-validator');

const dateFormat = require('dateformat');
const now = new Date();
const standardDT = dateFormat(now,'yyyy-mm-dd HH:MM:ss');

var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};


//list of accounts that i follow
router.post('/following/following_list',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}


	var  account_id = req.body.account_id;
	var  api_token = req.body.api_token;



	var arrParams = {
		account_id:account_id,
		api_token:api_token
	};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{

			HelperFollowing.getMyFollowingMeta(arrParams, function(retMyFollowing){

				if(retMyFollowing!=null){

					jsonRes = {
						success: '1',
						result: {following:retMyFollowing},
						successMessage: "Groups",
						errorMessage: "" 
			  		};

		  			res.status(201).json(jsonRes);
		 			res.end();

				}else{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
			  		};

		  			res.status(statRes).json(jsonRes);
		 			res.end();
				}

			});
		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

});

//following person's group list
router.post('/following/following_group_list',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('following_id').isNumeric()


	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  account_id = req.body.account_id;
	var  following_id = req.body.following_id;
	var  api_token = req.body.api_token;

	var arrParams = {
		account_id:account_id,
		api_token:api_token,
		following_id:following_id
	};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{

			HelperFollowing.checkMyFollowingMeta(arrParams, function(retMyFollowing){

				if(retMyFollowing!=null)
				{

					HelperFollowing.getAvailableNotAvailableGroups(retMyFollowing,arrParams, function(retMyFollowing){

						jsonRes = {
							success: '1',
							result: retMyFollowing,
							successMessage: "Groups",
							errorMessage: "" 
				  		};

			  			res.status(201).json(jsonRes);
			 			res.end();

					});
				}
				else
				{

					jsonRes = {
						success: success,
						result: {},
						successMessage: successMessage,
						errorMessage: "No result found." 
			  		};

		  			res.status(statRes).json(jsonRes);
		 			res.end();

				}

			});

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

});

//following request
router.post('/following/following_request',[

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
	check('following_id').isNumeric(),
	check('flag').isNumeric()

	], (req,res,next) => {

	var _send = res.send;
	var sent = false;

	res.send = function(data){
	   if(sent) return;
	   _send.bind(res)(data);
	   sent = true;
	};

  	var errors = validationResult(req);
  	if (!errors.isEmpty()) {
  		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered." 
  		};

		res.status(statRes).json(jsonRes);
 		res.end();
  	}

	var  account_id = req.body.account_id;
	var  api_token = req.body.api_token;
	var  following_id = req.body.following_id;
	var  id = req.body.id;
	var  flag = req.body.flag;

	var arrID = id.split(",");

	var arrParams = {
		account_id:account_id,
		api_token:api_token,
		following_id:following_id,
		arr_id:arrID,
		id:id,
		flag:flag
	};


	HelperRestriction.checkParametersApiToken(arrParams, function(queParam){
		if(queParam.length)
		{
			if(flag == '0')
			{

				HelperFollowing.getMyFollowingGroupsPerFollowingID(arrParams, function(retGrpIDS){

					if(retGrpIDS!=null){

						HelperFollowing.getMyFollowingGroupsMetaPerID(arrParams,retGrpIDS, function(retGrpMeta){

							if(retGrpMeta!=null){

								HelperFollowing.delGroupsMetaFollowing(retGrpMeta, function(retUnfollow){

									if(retUnfollow!=0){

										jsonRes = {
											success: '1',
											result: {},
											successMessage: "Successfully unfollowed",
											errorMessage: "" 
								  		};

							  			res.status(201).json(jsonRes);
							 			res.end();

							
									}else{

										jsonRes = {
											success: success,
											result: {},
											successMessage: successMessage,
											errorMessage: "Unable to process request. Please try again." 
								  		};

							  			res.status(statRes).json(jsonRes);
							 			res.end();
									}

								});
				
							}else{

								jsonRes = {
									success: success,
									result: {},
									successMessage: successMessage,
									errorMessage: "No result found." 
						  		};

					  			res.status(statRes).json(jsonRes);
					 			res.end();

							}

						});


					}else{

						jsonRes = {
							success: success,
							result: {},
							successMessage: successMessage,
							errorMessage: "No result found." 
				  		};

			  			res.status(statRes).json(jsonRes);
			 			res.end();
					}

				});

			}
			else if(flag == '1')
			{

				HelperFollowing.getGroupsToFollow(arrParams, function(retGrpIDS){

					var queGroupMax = "SELECT MAX(group_id) AS lastGrpId FROM jaze_follower_groups_meta";
			  		var lastGrpId = 0;

	 				HelperFollowing.getLastGroupId(queGroupMax, function(queGrpRes){

					  	queGrpRes.forEach(function(row) {
							lastGrpId = row.lastGrpId;
					  	});

			  			var plusOneGroupID;
			  			var arrInsert;
			  			var arrToInsert = [];

  						if(retGrpIDS.length > 1){
		  				
				  		  	retGrpIDS.forEach(function(row) {

								arrInsert = {
									follower_group_id:row,
									follower_account_id:account_id,
									joined_date:standardDT,
									status:1
								};

								arrToInsert.push(arrInsert);

							});

							successMessage = 'Successfully followed groups.';

	  		  			}else{

  							arrInsert = {
								follower_group_id:id,
								follower_account_id:account_id,
								joined_date:standardDT,
								status:1
							};

							arrToInsert = arrInsert;
							lastGrpId = lastGrpId + 1;

							successMessage = 'Successfully followed group.';
	  		  			}


						var querInsert = `insert into jaze_follower_groups_meta (group_id, meta_key, meta_value) VALUES (?,?,?)`;
						HelperFollowing.insertResult(arrToInsert,querInsert,lastGrpId,retGrpIDS.length, function(insertRes){	

							if(insertRes!=0){

								jsonRes = {
									success: '1',
									result: {},
									successMessage: successMessage,
									errorMessage: "" 
						  		};

					  			res.status(201).json(jsonRes);
					 			res.end();

							}else{

								jsonRes = {
									success: success,
									result: {},
									successMessage: '',
									errorMessage: "Unable to process request. Please try again." 
						  		};

					  			res.status(statRes).json(jsonRes);
					 			res.end();

							}
				
						});

					});
				});

			}
			else
			{

				jsonRes = {
					success: success,
					result: {},
					successMessage: successMessage,
					errorMessage: "Invalid flag provided." 
		  		};

	  			res.status(statRes).json(jsonRes);
	 			res.end();
			}

		}
		else
		{
			jsonRes = {
				success: success,
				result: {},
				successMessage: successMessage,
				errorMessage: "Device not found." 
	  		};

  			res.status(statRes).json(jsonRes);
 			res.end();
		}

	});

});




module.exports = router;