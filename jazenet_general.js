'use strict';
// content of fcm_node.js

const bodyParser = require('body-parser');
const express = require('express');
const router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
const app = express();



const path = require('path');
const fs = require('fs');
const url = require('url');
const download = require('download-file');

const mime = require('mime-types');
const multer = require('multer');

const { check, validationResult } = require('express-validator');


const HelperRestriction = require('./helpers/HelperRestriction.js');
const HelperGeneral = require('./helpers/HelperGeneral.js');
const HelperNotification = require('./jazecom/helpers/HelperNotification.js');



var success = '0';
var successMessage = '';
var errorMessage = '';
var results = {};

var statRes = 422;
var jsonRes = {};


// ------------------------------------------------------ General Search Module Start  --------------------------------------------------------------//

router.post('/general/searchCompanyProfessionalDetails', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
    check('account_type').isNumeric(),
    check('page').isNumeric(),
    check('flag').isAlphanumeric(),
    check('result_flag').isAlphanumeric(),
    check('country').matches('[1-9]'),
    check('state').matches('[1-9]'),
    check('city').matches('[1-9]')

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};


	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id: req.body.account_id,
        account_type: req.body.account_type,
        keyword : req.body.keyword,
        api_token: req.body.api_token,
        limit : req.body.page,
        flag : req.body.flag,
        result_flag : req.body.result_flag,
        country : req.body.country,
        state : req.body.state,
        city : req.body.city
	};

    //flag =  1 : chat, 2 : mail, 3 : call, 4 : feed, 5 : dairy, 6 : general, 7 : document chat, 8 : document mail,

    //result_flag = 1 : company & govt , 2 : professional & work, 3 : user

    HelperGeneral.checkGeneralParametersApiToken(reqParams, function (queParamRes) {

        if (typeof queParamRes !== 'undefined' && queParamRes !== null && parseInt(queParamRes.length) !== 0)
        {
            HelperGeneral.getBlockAccountIdList(reqParams,function(block_accounts) {

                Object.assign(reqParams, {block_accounts: block_accounts});

                HelperGeneral.getAccountContactList(reqParams, function(request_status){

                    Object.assign(reqParams, {request_status: request_status});

                    HelperGeneral.getAutofillCompanyResults(reqParams,function(comp_results,company_id_list) {

                        HelperGeneral.getAutofillProfileResults(reqParams,company_id_list,function(prof_results) {

                            HelperGeneral.getAutofillUserResults(reqParams,function(user_results) {

                                var pagination_details  = {
                                            total_page   : "0",
                                            current_page : "0",
                                            next_page    : "0"
                                };

                                var search_param = {
                                            flag               : reqParams.flag,
                                            result_flag        : reqParams.result_flag,
                                            keyword            : reqParams.keyword,
                                            country            : reqParams.country,
                                            state              : reqParams.state,
                                            city               : reqParams.city
                                };

                                var result_count = {
                                            user_count         : user_results.tot_count,
                                            professional_count : prof_results.tot_count,
                                            company_count      : comp_results.tot_count
                                };

                                var result = {
                                        company_result       : [],
                                        prof_result          : [],
                                        user_result         : []
                                }

                                var status = 0;

                                if(comp_results.result !== null && parseInt(reqParams.result_flag) === 1)
                                    {
                                        var next_page = (parseInt(reqParams.limit) + 1);

                                        if(parseInt(comp_results.tot_page) <= parseInt(next_page))
                                        { next_page = "0"; }

                                        pagination_details  = {
                                            total_page   : comp_results.tot_page.toString(),
                                            current_page : reqParams.limit.toString(),
                                            next_page    : next_page.toString()
                                        };

                                        result = {
                                            company_result       : sort_name(comp_results.result,reqParams.keyword),
                                            prof_result          : [],
                                            user_result         : []
                                        }

                                        status = 1;
                                }
                                else if(prof_results.result !== null && parseInt(reqParams.result_flag) === 2)
                                    {
                                        var next_page = (parseInt(reqParams.limit) + 1);

                                        if(parseInt(prof_results.tot_page) <= parseInt(next_page))
                                        { next_page = "0"; }

                                        pagination_details  = {
                                            total_page   : prof_results.tot_page.toString(),
                                            current_page : reqParams.limit.toString(),
                                            next_page    : next_page.toString()
                                        };

                                        result = {
                                            company_result       : [],
                                            prof_result          : sort_name(prof_results.result,reqParams.keyword),
                                            user_result         : []
                                        }

                                        status = 1;
                                }
                                else if(user_results.result !== null && parseInt(reqParams.result_flag) === 3)
                                    {
                                        var next_page = (parseInt(reqParams.limit) + 1);

                                        if(parseInt(user_results.tot_page) <= parseInt(next_page))
                                        { next_page = "0"; }

                                        pagination_details  = {
                                            total_page   : user_results.tot_page.toString(),
                                            current_page : reqParams.limit.toString(),
                                            next_page    : next_page.toString()
                                        };

                                        result = {
                                            company_result       : [],
                                            prof_result          : [],
                                            user_result         : sort_name(user_results.result,reqParams.keyword),
                                        }

                                        status = 1;
                                }

                                if(parseInt(status) === 1)
                                    {
                                        jsonRes = {
                                                success: '1',
                                                result: {search_param : search_param, pagination_details : pagination_details, result_count : result_count, search_result : result},
                                                successMessage: "account search result",
                                                errorMessage: ""
                                        };

                                        res.status(201).json(jsonRes);
                                        res.end();
                                }
                                else
                                    {
                                        jsonRes = {
                                            success: '0',
                                            result: {},
                                            successMessage: "",
                                            errorMessage: "no result found"
                                        };

                                        res.status(201).json(jsonRes);
                                        res.end();
                                 }
                            });
                        });
                    });
                });
            });
        }
        else
        {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };
            res.status(statRes).json(jsonRes);
            res.end();
        }
   });
});


router.post('/general/searchCompanyProfessionalDetails_new', [

	check('api_token').isAlphanumeric(),
	check('account_id').isNumeric(),
    check('account_type').isNumeric(),
    check('page').isNumeric(),
    check('flag').isAlphanumeric(),
    check('result_flag').isAlphanumeric(),
    check('country').matches('[1-9]'),
    check('state').matches('[1-9]'),
    check('city').matches('[1-9]')

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};


	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
		account_id: req.body.account_id,
        account_type: req.body.account_type,
        keyword : req.body.keyword,
        api_token: req.body.api_token,
        limit : req.body.page,
        flag : req.body.flag,
        result_flag : req.body.result_flag,
        country : req.body.country,
        state : req.body.state,
        city : req.body.city
	};

    //flag =  1 : chat, 2 : mail, 3 : call, 4 : feed, 5 : dairy, 6 : general, 7 : document chat, 8 : document mail,

    //result_flag = 1 : company & govt , 2 : professional & work, 3 : user

    HelperGeneral.checkGeneralParametersApiToken(reqParams, function (queParamRes) {

        if (typeof queParamRes !== 'undefined' && queParamRes !== null && parseInt(queParamRes.length) !== 0)
        {
            HelperGeneral.getBlockAccountIdList(reqParams,function(block_accounts) {

                Object.assign(reqParams, {block_accounts: block_accounts});

                HelperGeneral.getAccountContactList(reqParams, function(request_status){

                    Object.assign(reqParams, {request_status: request_status});

                    HelperGeneral.getAutofillCompanyResults(reqParams,function(comp_results,company_id_list) {

                        HelperGeneral.getAutofillProfileResults(reqParams,company_id_list,function(prof_results) {

                            HelperGeneral.getAutofillUserResults(reqParams,function(user_results) {

                                var pagination_details  = {
                                            total_page   : "0",
                                            current_page : "0",
                                            next_page    : "0"
                                };

                                var search_param = {
                                            flag               : reqParams.flag,
                                            result_flag        : reqParams.result_flag,
                                            keyword            : reqParams.keyword,
                                            country            : reqParams.country,
                                            state              : reqParams.state,
                                            city               : reqParams.city
                                };

                                var result_count = {
                                            user_count         : user_results.tot_count,
                                            professional_count : prof_results.tot_count,
                                            company_count      : comp_results.tot_count
                                };

                                var result = {
                                        company_result       : [],
                                        prof_result          : [],
                                        user_result         : []
                                }

                                var status = 0;

                                if(comp_results.result !== null && parseInt(reqParams.result_flag) === 1)
                                    {
                                        var next_page = (parseInt(reqParams.limit) + 1);

                                        if(parseInt(comp_results.tot_page) <= parseInt(next_page))
                                        { next_page = "0"; }

                                        pagination_details  = {
                                            total_page   : comp_results.tot_page.toString(),
                                            current_page : reqParams.limit.toString(),
                                            next_page    : next_page.toString()
                                        };

                                        result = {
                                            company_result       : sort_name(comp_results.result,reqParams.keyword),
                                            prof_result          : [],
                                            user_result         : []
                                        }

                                        status = 1;
                                }
                                else if(prof_results.result !== null && parseInt(reqParams.result_flag) === 2)
                                    {
                                        var next_page = (parseInt(reqParams.limit) + 1);

                                        if(parseInt(prof_results.tot_page) <= parseInt(next_page))
                                        { next_page = "0"; }

                                        pagination_details  = {
                                            total_page   : prof_results.tot_page.toString(),
                                            current_page : reqParams.limit.toString(),
                                            next_page    : next_page.toString()
                                        };

                                        result = {
                                            company_result       : [],
                                            prof_result          : sort_name(prof_results.result,reqParams.keyword),
                                            user_result         : []
                                        }

                                        status = 1;
                                }
                                else if(user_results.result !== null && parseInt(reqParams.result_flag) === 3)
                                    {
                                        var next_page = (parseInt(reqParams.limit) + 1);

                                        if(parseInt(user_results.tot_page) <= parseInt(next_page))
                                        { next_page = "0"; }

                                        pagination_details  = {
                                            total_page   : user_results.tot_page.toString(),
                                            current_page : reqParams.limit.toString(),
                                            next_page    : next_page.toString()
                                        };

                                        result = {
                                            company_result       : [],
                                            prof_result          : [],
                                            user_result         : sort_name(user_results.result,reqParams.keyword),
                                        }

                                        status = 1;
                                }

                                if(parseInt(status) === 1)
                                    {
                                        jsonRes = {
                                                success: '1',
                                                result: {search_param : search_param, pagination_details : pagination_details, result_count : result_count, search_result : result},
                                                successMessage: "account search result",
                                                errorMessage: ""
                                        };

                                        res.status(201).json(jsonRes);
                                        res.end();
                                }
                                else
                                    {
                                        jsonRes = {
                                            success: '0',
                                            result: {},
                                            successMessage: "",
                                            errorMessage: "no result found"
                                        };

                                        res.status(201).json(jsonRes);
                                        res.end();
                                 }
                            });
                        });
                    });
                });
            });
        }
        else
        {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };
            res.status(statRes).json(jsonRes);
            res.end();
        }
   });
});

// ------------------------------------------------------ General Search Module End  --------------------------------------------------------------//

// ------------------------------------------------------ Geo Search Module Start  --------------------------------------------------------------//

router.post('/general/searchCountryStateCityDetails', [

	check('api_token').isAlphanumeric(),
    check('account_id').isNumeric(),
    check('page').isNumeric(),
    check('flag').isAlphanumeric(),
    check('more_result_flag').isAlphanumeric(),
    check('country').isNumeric(),
    check('state').isNumeric(),
    check('city').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};


	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
        api_token       : req.body.api_token,
        account_id: req.body.account_id,
        keyword : req.body.keyword,
        flag : req.body.flag,
        more_result_flag : req.body.more_result_flag,
        limit : req.body.page,
        country : req.body.country,
        state : req.body.state,
        city : req.body.city,
        area : req.body.area
	};

    var count = 1;

    HelperGeneral.checkGeneralParametersApiToken(reqParams, function (queParamRes) {

        if (typeof queParamRes !== 'undefined' && queParamRes !== null && parseInt(queParamRes.length) !== 0)
        {
            if(parseInt(reqParams.flag) === 0)
            {
                //all results

                count++;

                HelperGeneral.getAutofillCountryResults(reqParams,count,function(country_results) {

                    if(parseInt(count) === 2)
                     {
                         count++;

                         HelperGeneral.getAutofillStateResults(reqParams,count,function(state_results) {

                             if(parseInt(count) === 3)
                             {
                                 count++;

                                 HelperGeneral.getAutofillCityResults(reqParams,count,function(city_results) {

                                     if(parseInt(count) === 4)
                                     {
                                         count++;

                                          var pagination_details  = {};

                                            if(parseInt(reqParams.more_result_flag) === 1)
                                             {
                                                 var next_page = (parseInt(reqParams.limit) + 1);

                                                 if(parseInt(country_results.tot_page) < parseInt(next_page))
                                                 { next_page = "0"; }

                                                 pagination_details  = {
                                                     total_page : country_results.tot_page.toString(),
                                                     current_page : reqParams.limit.toString(),
                                                     next_page   : next_page.toString()
                                                 };
                                             }
                                             else if(parseInt(reqParams.more_result_flag) === 2)
                                             {
                                                 var next_page = (parseInt(reqParams.limit) + 1);

                                                 if(parseInt(state_results.tot_page) < parseInt(next_page))
                                                 { next_page = "0"; }

                                                 pagination_details  = {
                                                     total_page : state_results.tot_page.toString(),
                                                     current_page : reqParams.limit.toString(),
                                                     next_page   : next_page.toString()
                                                 };
                                             }
                                             else if(parseInt(reqParams.more_result_flag) === 3)
                                             {
                                                 var next_page = (parseInt(reqParams.limit) + 1);

                                                 if(parseInt(city_results.tot_page) < parseInt(next_page))
                                                 { next_page = "0"; }

                                                 pagination_details  = {
                                                     total_page : city_results.tot_page.toString(),
                                                     current_page : reqParams.limit.toString(),
                                                     next_page   : next_page.toString()
                                                 };
                                             }



                                         var result = [];

                                         var country_count =  "0",
                                             state_count =  "0",
                                             city_count = "0";


                                         if(country_results !== null)
                                         {  result  = result.concat(country_results.result);  country_count = country_results.tot_count.toString(); }

                                         if(state_results !== null)
                                         {  result  = result.concat(state_results.result);  state_count = state_results.tot_count.toString(); }

                                         if(city_results !== null)
                                         {  result  = result.concat(city_results.result);  city_count = city_results.tot_count.toString(); }


                                        var search_param = {
                                             flag               : reqParams.flag,
                                             more_result_flag   : reqParams.more_result_flag,
                                             keyword            : reqParams.keyword,
                                             country            : reqParams.country,
                                             state              : reqParams.state,
                                             city               : reqParams.city,
                                             area               : "0"
                                        }


                                        var result_count = {
                                            country_count    : country_count,
                                            state_count      : state_count,
                                            city_count       : city_count,
                                            area_count       : "0"
                                        }


                                         var objectName = {
                                                 geo_search_param       : search_param,
                                                 geo_pagination_details : pagination_details,
                                                 geo_result_count       : result_count,
                                                 geo_search_result      : result
                                             };

                                         jsonRes = {
                                                 success: '1',
                                                 result: objectName,
                                                 successMessage: "location search list",
                                                 errorMessage: ""
                                         };

                                         res.status(201).json(jsonRes);
                                         res.end();
                                     }
                                 });
                             }
                         });
                     }
                 });
            }
            else  if(parseInt(reqParams.flag) === 1)
            {
                // Country
                HelperGeneral.getAutofillCountryResults(reqParams,2,function(country_results) {

                    if(country_results !== null)
                    {
                            var pagination_details  = {};

                            var search_param = {
                                flag               : reqParams.flag,
                                more_result_flag   : reqParams.more_result_flag,
                                keyword            : reqParams.keyword,
                                country            : reqParams.country,
                                state              : reqParams.state,
                                city               : reqParams.city,
                                area               : "0"
                           }


                           var result_count = {
                               country_count    : country_results.tot_count.toString(),
                               state_count      : "0",
                               city_count       : "0"
                           }


                            var objectName = {
                                geo_search_param       : search_param,
                                geo_pagination_details : pagination_details,
                                geo_result_count       : result_count,
                                geo_search_result      : country_results.result
                                };

                            jsonRes = {
                                    success: '1',
                                    result: objectName,
                                    successMessage: "location search list",
                                    errorMessage: ""
                            };

                            res.status(201).json(jsonRes);
                            res.end();
                    }
                    else{
                        jsonRes = {
                            success: success,
                            result: {},
                            successMessage: successMessage,
                            errorMessage: "no result found"
                    };

                    res.status(201).json(jsonRes);
                    res.end();
                    }
                });
            }
            else  if(parseInt(reqParams.flag) === 2)
            {

                // State
                HelperGeneral.getAutofillStateResults(reqParams,3,function(state_results) {

                    if(state_results !== null)
                    {
                            var pagination_details  = {};

                            var search_param = {
                                flag               : reqParams.flag,
                                more_result_flag   : reqParams.more_result_flag,
                                keyword            : reqParams.keyword,
                                country            : reqParams.country,
                                state              : reqParams.state,
                                city               : reqParams.city,
                                area               : "0"
                           }


                           var result_count = {
                               country_count    : "0",
                               state_count      : state_results.tot_count.toString(),
                               city_count       : "0"
                           }


                            var objectName = {
                                    geo_search_param       : search_param,
                                    geo_pagination_details : pagination_details,
                                    geo_result_count       : result_count,
                                    geo_search_result      : state_results.result
                                };

                            jsonRes = {
                                    success: '1',
                                    result: objectName,
                                    successMessage: "state search list",
                                    errorMessage: ""
                            };

                            res.status(201).json(jsonRes);
                            res.end();
                    }
                    else{
                        jsonRes = {
                            success: success,
                            result: {},
                            successMessage: successMessage,
                            errorMessage: "no result found"
                    };

                    res.status(201).json(jsonRes);
                    res.end();
                    }
                });
            }
            else  if(parseInt(reqParams.flag) === 3)
            {
                // city
                HelperGeneral.getAutofillCityResults(reqParams,4,function(city_results) {

                    if(city_results !== null)
                    {
                            var pagination_details  = {};

                            var search_param = {
                                flag               : reqParams.flag,
                                more_result_flag   : reqParams.more_result_flag,
                                keyword            : reqParams.keyword,
                                country            : reqParams.country,
                                state              : reqParams.state,
                                city               : reqParams.city,
                                area               : "0"
                           }


                           var result_count = {
                               country_count    : "0",
                               state_count      : "0",
                               city_count       : city_results.tot_count.toString()
                           }


                            var objectName = {
                                    geo_search_param       : search_param,
                                    geo_pagination_details : pagination_details,
                                    geo_result_count       : result_count,
                                    geo_search_result      : city_results.result
                                };

                            jsonRes = {
                                    success: '1',
                                    result: objectName,
                                    successMessage: "city search list",
                                    errorMessage: ""
                            };

                            res.status(201).json(jsonRes);
                            res.end();
                    }
                    else{
                        jsonRes = {
                            success: success,
                            result: {},
                            successMessage: successMessage,
                            errorMessage: "no result found"
                    };

                    res.status(201).json(jsonRes);
                    res.end();
                    }
                });
            }
            else  if(parseInt(reqParams.flag) === 4)
            {
                // for get location details
            }
            else  if(parseInt(reqParams.flag) === 5)
            {
                if(parseInt(reqParams.city) !== 0 && parseInt(reqParams.state) !== 0 && parseInt(reqParams.country) !== 0)
                {
                     // get all area list under city
                     count = 5;

                     HelperGeneral.getAutofillAreaResults(reqParams,count,function(area_results) {

                         if(parseInt(count) === 5)
                         {
                                 jsonRes = {
                                         success: '1',
                                         result: {area_list : area_results},
                                         successMessage: "geo area list",
                                         errorMessage: ""
                                 };

                                 res.status(201).json(jsonRes);
                                 res.end();
                         }
                     });
                }
                else if(parseInt(reqParams.state) !== 0 && parseInt(reqParams.country) !== 0)
                {
                    // get all city list under state
                    count = 4;

                    HelperGeneral.getAutofillCityResults(reqParams,count,function(city_results) {

                        if(parseInt(count) === 4)
                        {
                                jsonRes = {
                                        success: '1',
                                        result: {citylist : city_results},
                                        successMessage: "geo city list",
                                        errorMessage: ""
                                };

                                res.status(201).json(jsonRes);
                                res.end();
                        }
                    });
                }
                else if(parseInt(reqParams.country) !== 0)
                {
                    // get all state list under country
                    count = 3;

                    HelperGeneral.getAutofillStateResults(reqParams,count,function(state_results) {

                        if(parseInt(count) === 3)
                        {
                                jsonRes = {
                                        success: '1',
                                        result: {state_list : state_results},
                                        successMessage: "geo state list",
                                        errorMessage: ""
                                };

                                res.status(201).json(jsonRes);
                                res.end();
                        }
                    });
                }
                else
                {
                    // get all country list
                    count++;

                    HelperGeneral.getAutofillCountryResults(reqParams,count,function(country_results) {

                        if(parseInt(count) === 2)
                        {
                                jsonRes = {
                                        success: '1',
                                        result: {country_list : country_results},
                                        successMessage: "geo country list",
                                        errorMessage: ""
                                };

                                res.status(201).json(jsonRes);
                                res.end();
                        }
                    });
                }
            }
            else  if(parseInt(reqParams.flag) === 6)
            {
                if(parseInt(reqParams.country) !== 0)
                {
                    // get all city list under country
                    count = 4;

                    HelperGeneral.getAutofillCityResults(reqParams,count,function(city_results) {

                        if(parseInt(count) === 4)
                        {
                                jsonRes = {
                                        success: '1',
                                        result: {citylist : city_results},
                                        successMessage: "geo city list",
                                        errorMessage: ""
                                };

                                res.status(201).json(jsonRes);
                                res.end();
                        }
                    });
                }
                else{
                    jsonRes = {
                        success: success,
                        result: {},
                        successMessage: successMessage,
                        errorMessage: "no result found"
                    };

                    res.status(201).json(jsonRes);
                    res.end();
                }
            }
            else{
                jsonRes = {
                    success: success,
                    result: {},
                    successMessage: successMessage,
                    errorMessage: "no result found"
                };

                res.status(201).json(jsonRes);
                res.end();
            }
        }
        else
        {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };
            res.status(statRes).json(jsonRes);
            res.end();
        }
   });

});

router.post('/general/getCountryStateCityDetails', [

	check('api_token').isAlphanumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};


	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
        api_token       : req.body.api_token,
        country_code    : req.body.country_code,
        country_key     : req.body.country_key,
        state_key       : req.body.state_key,
        city_key        : req.body.city_key,
        flag            : 4,
        more_result_flag : 0,
        country         : 0,
        state           : 0,
        city            : 0,
        area            : 0
	};

    var count = 1;

    HelperGeneral.checkGeneralParametersApiToken(reqParams, function (queParamRes) {

        if (typeof queParamRes !== 'undefined' && queParamRes !== null && parseInt(queParamRes.length) !== 0)
        {
            count++;

            HelperGeneral.getAutofillCountryResults(reqParams,count,function(country_results) {

                if(parseInt(count) === 2)
                {
                    count++;
                    if(country_results !== null)
                    { Object.assign(reqParams, {country: country_results[0].country_id}); }

                    HelperGeneral.getAutofillStateResults(reqParams,count,function(state_results) {

                        if(parseInt(count) === 3)
                        {
                            count++;
                            if(state_results !== null)
                            { Object.assign(reqParams, {state: state_results[0].state_id}); }

                            HelperGeneral.getAutofillCityResults(reqParams,count,function(city_results) {

                                if(parseInt(count) === 4)
                                {
                                    count++;

                                    var objectName = {}

                                    if(city_results !== null)
                                    {
                                        objectName = {
                                            geo_result      : city_results[0]
                                        }
                                    }
                                    else if(state_results !== null)
                                    {
                                        objectName = {
                                            geo_result      : state_results[0]
                                        }
                                    }
                                    else if(country_results !== null)
                                    {
                                        objectName = {
                                            geo_result      : country_results[0]
                                        }
                                    }

                                    if (Object.keys(objectName).length !== 0)
                                    {
                                        jsonRes = {
                                            success: '1',
                                            result: objectName,
                                            successMessage: "location result",
                                            errorMessage: ""
                                        };

                                        res.status(201).json(jsonRes);
                                        res.end();
                                    }
                                    else
                                    {
                                        jsonRes = {
                                            success: success,
                                            result: {},
                                            successMessage: successMessage,
                                            errorMessage: "empty result"
                                        };

                                        res.status(201).json(jsonRes);
                                        res.end();
                                    }
                                }
                            });
                        }
                    });
                }
            });
        }
        else
        {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };
            res.status(statRes).json(jsonRes);
            res.end();
        }
   });
});

router.post('/general/getCountryStateCityDetailss', [

	check('api_token').isAlphanumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};


	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
        api_token       : req.body.api_token,
        country_code    : req.body.country_code,
        country_key     : req.body.country_key,
        state_key       : req.body.state_key,
        city_key        : req.body.city_key,
        flag            : 4,
        more_result_flag : 0,
        country         : 0,
        state           : 0,
        city            : 0,
        area            : 0
	};



    HelperGeneral.checkGeneralParametersApiToken(reqParams, function (queParamRes) {

        if (typeof queParamRes !== 'undefined' && queParamRes !== null && parseInt(queParamRes.length) !== 0)
        {
            var count = 4;

            HelperGeneral.getAutofillCityResults(reqParams,count,function(city_results) {

                if(parseInt(count) === 4)
                {

                    if(city_results !== null)
                    {
                        //checking city our list
                        var city_list = [34917,30611,32870,34250,31654,34732,32618,33437,34639,34952];

                        if(city_list.indexOf(parseInt(city_results[0].city_id)) !== -1)
			            {
                            jsonRes = {
                                success: '1',
                                result: {geo_result : city_results[0]},
                                successMessage: "location result",
                                errorMessage: ""
                            };

                            res.status(201).json(jsonRes);
                            res.end();
                        }
                        else
                        {
                            jsonRes = {
                                success: success,
                                result: {},
                                successMessage: successMessage,
                                errorMessage: "empty result"
                            };

                            res.status(201).json(jsonRes);
                            res.end();
                        }
                    }
                    else
                    {
                        jsonRes = {
                            success: success,
                            result: {},
                            successMessage: successMessage,
                            errorMessage: "empty result"
                        };

                        res.status(201).json(jsonRes);
                        res.end();
                    }
                }
            });
        }
        else
        {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };
            res.status(statRes).json(jsonRes);
            res.end();
        }
   });
});

router.post('/general/getPhoneCodeList', [
    check('api_token').isAlphanumeric(),
    check('account_id').isNumeric()
], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};


	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
        account_id   	: req.body.account_id,
        api_token       : req.body.api_token
	};


    HelperGeneral.checkGeneralParametersApiToken(reqParams, function (queParamRes) {

        if (typeof queParamRes !== 'undefined' && queParamRes !== null && parseInt(queParamRes.length) !== 0)
        {
            HelperGeneral.getPhoneCodeList(reqParams,function(code_results) {

                if(code_results !== null)
                {
                    jsonRes = {
                        success: '1',
                        result: {phonecode_result : code_results},
                        successMessage: "phone code result",
                        errorMessage: ""
                    };

                    res.status(201).json(jsonRes);
                    res.end();
                }
                else
                {
                    jsonRes = {
                        success: success,
                        result: {},
                        successMessage: successMessage,
                        errorMessage: "empty result"
                    };

                    res.status(201).json(jsonRes);
                    res.end();
                }
            });
        }
        else
        {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };
            res.status(statRes).json(jsonRes);
            res.end();
        }
   });
});

// ------------------------------------------------------ Geo Search Module End  --------------------------------------------------------------//

// ------------------------------------------------------ Card Validation Module Start  --------------------------------------------------------------//

router.post('/general/getCreditCardType', [

	check('api_token').isAlphanumeric(),
    check('card_number').isNumeric(),

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

    var creditCardType = require('credit-card-type');

	var reqParams = {
        api_token       : req.body.api_token,
        card_number: req.body.card_number,
    };


    HelperGeneral.checkGeneralParametersApiToken(reqParams, function (queParamRes) {

        if (typeof queParamRes !== 'undefined' && queParamRes !== null && parseInt(queParamRes.length) !== 0)
        {

            var visaCards = creditCardType(reqParams.card_number)[0];

            if (typeof visaCards !== 'undefined' && visaCards !== null && parseInt(visaCards.length) !== 0)
            {
                var objectName = {
                    name       : visaCards.niceType,
                    logo       : G_STATIC_IMG_URL+"uploads/jazenet/creditcard_logos/"+visaCards.type+".png"
                };

                jsonRes = {
                    success: '1',
                    result: objectName,
                    successMessage: "credit card logo.",
                    errorMessage: ""
                };

                res.status(201).json(jsonRes);
                res.end();
            }
            else
            {
                jsonRes = {
                    success: success,
                    result: {},
                    successMessage: successMessage,
                    errorMessage: "Invalid Card Details."
                };
                res.status(201).json(jsonRes);
                res.end();
            }
        }
        else
        {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };
            res.status(201).json(jsonRes);
            res.end();
        }
    });
});


router.post('/general/checkCreditCard', [

	check('api_token').isAlphanumeric(),
    check('card_number').isNumeric(),
    check('exp_month').isNumeric(),
    check('exp_year').isNumeric(),

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};


	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
        api_token       : req.body.api_token,
        card_number: req.body.card_number,
        exp_month : req.body.exp_month,
        exp_year : req.body.exp_year
    };

    const CreditCard = require('node-creditcard');

    HelperGeneral.checkGeneralParametersApiToken(reqParams, function (queParamRes) {

        if (typeof queParamRes !== 'undefined' && queParamRes !== null && parseInt(queParamRes.length) !== 0)
        {
            const creditcard = new CreditCard({
                number    : reqParams.card_number,
                expiration: reqParams.exp_month+'/'+reqParams.exp_year
            });

            if (creditcard.isValid) {

                const validation = creditcard.validate();

                if(validation.validCardNumber === true)
                {
                    if(validation.isExpired === false)
                    {
                        var objectName = {
                            name       : validation.brand,
                            logo       : G_STATIC_IMG_URL+"uploads/jazenet/creditcard_logos/"+validation.brand.toLowerCase()+".png"
                        };

                        jsonRes = {
                            success: '1',
                            result: objectName,
                            successMessage: "credit card details.",
                            errorMessage: ""
                        };

                        res.status(201).json(jsonRes);
                        res.end();
                    }
                    else
                    {
                        jsonRes = {
                            success: success,
                            result: {},
                            successMessage: successMessage,
                            errorMessage: "Credit Card is expired."
                        };
                        res.status(201).json(jsonRes);
                        res.end();
                    }
                }
                else
                {
                    jsonRes = {
                        success: success,
                        result: {},
                        successMessage: successMessage,
                        errorMessage: "Credit Card number is invalid."
                    };
                    res.status(201).json(jsonRes);
                    res.end();
                }
            }
            else
            {
                jsonRes = {
                    success: success,
                    result: {},
                    successMessage: successMessage,
                    errorMessage: "Invalid Card Details."
                };
                res.status(201).json(jsonRes);
                res.end();
            }
        }
        else
        {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };
            res.status(201).json(jsonRes);
            res.end();
        }
    });
});
// ------------------------------------------------------ Card Validation Module End  --------------------------------------------------------------//

// ------------------------------------------------------ General Currency Module Start  --------------------------------------------------------------//

router.post('/general/getCurrencyList', [

	check('api_token').isAlphanumeric(),
    check('account_id').isNumeric()

], (req, res, next) => {

	var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};


	var errors = validationResult(req);

	if (!errors.isEmpty()) {
		jsonRes = {
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		};

		res.status(statRes).json(jsonRes);
		res.end();
	}

	var reqParams = {
        api_token       : req.body.api_token,
        account_id      : req.body.account_id
	};

    HelperGeneral.checkGeneralParametersApiToken(reqParams, function (queParamRes) {

        if (typeof queParamRes !== 'undefined' && queParamRes !== null && parseInt(queParamRes.length) !== 0)
        {

            HelperGeneral.getCurrencyList(reqParams,function(currency_results) {

                if(currency_results !== null)
                {
                        jsonRes = {
                                success: '1',
                                result: {currency_list : currency_results},
                                successMessage: "currency list",
                                errorMessage: ""
                        };

                        res.status(201).json(jsonRes);
                        res.end();
                }
                else{
                    jsonRes = {
                        success: success,
                        result: {},
                        successMessage: successMessage,
                        errorMessage: "no result found"
                    };

                    res.status(201).json(jsonRes);
                    res.end();
                }
            });
        }
        else
        {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };
            res.status(statRes).json(jsonRes);
            res.end();
        }
   });

});

// ------------------------------------------------------ General Currency Module End  --------------------------------------------------------------//

//------------------------------------------------------- upload Document Functions Start -----------------------------------------------------------//
var main_path_single;
var main_file_type;
var storage = multer.diskStorage({

    destination: function (req, file, callback) {

    function asyncFunction (item, cb) {
        setTimeout(() => {

          var chat_path;

          if(req.body.flag=='1')
          {

            //create conversation folder
            chat_path = G_STATIC_IMG_PATH+"jazechat/conversation/"+item+"/";

            if(!fs.existsSync(chat_path)){
              fs.mkdirSync(chat_path,'0757', true);
            }

            //create document folder
            var file_type = file.mimetype.split("/");
            var filetype = '';

          if(file_type[0] === "*")
          {
            filetype = "application";
          }
          else
          {
            filetype = file_type[0];
          }

            chat_path = G_STATIC_IMG_PATH+"jazechat/conversation/"+item+"/"+filetype+"/";

          }
          else if(req.body.flag=='2')
          {


            chat_path = G_STATIC_IMG_PATH+"jazechat/conversation/"+item+"/";

          }
          else if(req.body.flag=='3')
          {

            //create conversation folder
            chat_path = G_STATIC_IMG_PATH+"jazechat/broadcast/"+item+"/";

            if(!fs.existsSync(chat_path)){
              fs.mkdirSync(chat_path,'0757', true);
            }

            //create document folder
            var file_type = file.mimetype.split("/");

            var filetype = '';

            if(file_type[0] === "*")
            {
            filetype = "application";
            }
            else
            {
              filetype = file_type[0];
            }

            chat_path = G_STATIC_IMG_PATH+"jazechat/broadcast/"+item+"/"+filetype+"/";

          }
          else if(req.body.flag=='4')
          {

            chat_path = G_STATIC_IMG_PATH+"jazefeed/"+item+"/";

            if(!fs.existsSync(chat_path)){
              fs.mkdirSync(chat_path,'0757', true);
            }

          }

          if(!fs.existsSync(chat_path)){
              fs.mkdirSync(chat_path,'0757', true);
          }


          main_file_type = filetype;
          main_path_single = chat_path;

        callback(null, chat_path);
        cb();
      }, 100);
    }

   let requests = 0;

       if(req.body.convers_id !=''){

           return new Promise((resolve) => {
               requests = req.body.convers_id;
                 asyncFunction(requests, resolve);

           });

       Promise.all(requests);

       }

 },

  filename: function (req, file, callback) {
      //callback(null, file.originalname);

      var extension = getExtension(file.originalname);

      var filename = getTheLastDot(file.originalname);

      var nonFilteredNewFile = getCleanFileName(file.originalname);
      var filteredNewFile = getFilename(nonFilteredNewFile);


      var arrCount = [];


      if (fs.existsSync(path.join(main_path_single,file.originalname))) {

          fs.readdir(path.join(main_path_single), function (err, files) {

            if (!err) {

              files.forEach(function (file) {

                var filteredOldFile = getCleanFileName(file);
                var finalFilter = getFilename(filteredOldFile);

                if( finalFilter == filteredNewFile ){
                  arrCount.push(file);
                }

              });

              setTimeout(() => {
                callback(null, filename[0]+"("+arrCount.length+")."+extension);
              }, 500);

            }else{
              callback(null, file.originalname);
            }

        });

      }else{

        callback(null, file.originalname);

      }

  }


});


router.post('/general/uploadDocument', function(req, res) {

	var upload = multer({
		storage: storage,
		fileFilter: function(req, file, callback) {

			callback(null, true)
		}
    }).single('file');



	upload(req, res, function(err) {


        //var file_type = req.file.mimetype.split("/");
       var file_type = main_file_type.split("/");


        var filetype = '';
        if(file_type[0] === "*")
        {
          filetype = "application";
        }
        else
        {
          filetype = file_type[0];
        }


        var extention = getExtension(req.file.filename);

        var path = "";

         if(req.body.flag==1){
                path = G_STATIC_IMG_URL+"uploads/jazenet/jazechat/conversation/"+req.body.convers_id+"/"+filetype+"/";
         }
         else if(req.body.flag==3){
            path = G_STATIC_IMG_URL+"uploads/jazenet/jazechat/broadcast/"+req.body.convers_id+"/"+filetype+"/";
     }
         else if(req.body.flag==4){
                path = G_STATIC_IMG_URL+"uploads/jazenet/jazefeed/"+req.body.convers_id+"/";
         }

        HelperGeneral.getFileTypes('.'+extention, function(reType){
            var results = {
                file_name:req.file.filename,
                file_format:filetype,
                file_type:reType,
                extention:extention,
                file_path:path+req.file.filename
            };

            jsonRes = {
                success: '1',
                result: results,
                successMessage: 'File is successfully uploaded.',
                errorMessage: errorMessage
            };
            res.status(201).json(jsonRes);
            res.end();
        });
	})
});


var main_path;
var storageMulti = multer.diskStorage({
  destination: function (req, file, callback) {

  function asyncFunction (item, cb) {
      setTimeout(() => {

      var file_path;



      if(req.body.flag == '5'){

        file_path = G_STATIC_IMG_PATH+"jazemail/"+item+"/";

      }else if(req.body.flag == '6'){

        file_path = G_STATIC_IMG_PATH+"jazefeed/"+item+"/";
      }


      main_path = file_path;

      if (!fs.existsSync(file_path)){
            fs.mkdirSync(file_path,'0757', true);
      }

      callback(null, file_path);

      cb();
      }, 100);
  }

  let requests = 0;

    if(req.body.api_token !=''){

      return new Promise((resolve) => {
        requests = req.body.account_id;

          asyncFunction(requests, resolve);

        });

      Promise.all(requests);
    }
  },
    filename: function (req, file, callback) {

      var extension = getExtension(file.originalname);

      var filename = getTheLastDot(file.originalname);

      var nonFilteredNewFile = getCleanFileName(file.originalname);
      var filteredNewFile = getFilename(nonFilteredNewFile);


      var arrCount = [];
      var file_posDot,file_posDup;

        if (fs.existsSync(path.join(main_path,file.originalname))) {

        fs.readdir(path.join(main_path), function (err, files) {

          if (!err) {

            files.forEach(function (file) {

              var filteredOldFile = getCleanFileName(file);
              var finalFilter = getFilename(filteredOldFile);

              if( finalFilter == filteredNewFile ){
                arrCount.push(file);
              }

            });

              setTimeout(() => {
                callback(null, filename[0]+"("+arrCount.length+")."+extension);
              }, 100);

          }else{
            callback(null, file.originalname);
          }

      });

        }else{

          callback(null, file.originalname);

        }

    }
});


router.post('/general/multi_upload', function(req, res) {

  var arrFiles = [];
  var arrObjects = [];
  var result = {};
  var uploadMulti = multer({
    storage: storageMulti,
  }).array('file',10);

  uploadMulti(req, res, function(err) {

    HelperGeneral.getMultiple(req.files,req.body.account_id,req.body.flag, function(returnResult){

            jsonRes = {
                success: '1',
                result: {feed_media : returnResult},
                successMessage: 'File is successfully uploaded.',
                errorMessage: errorMessage
            };
            res.status(201).json(jsonRes);
            res.end();

      });

  })
});





//------------------------------------------------------- upload Document Functions End -----------------------------------------------------------//


router.post('/general/GetuploadDocument',[



], function(req, res) {

    var _send = res.send;
	var sent = false;

	res.send = function (data) {
		if (sent) return;
		_send.bind(res)(data);
		sent = true;
	};

	var errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			success: success,
			result: {},
			successMessage: successMessage,
			errorMessage: "No mandatory fileds entered."
		});
    }



    var account_id = req.body.account_id;
    var api_token = req.body.api_token;
    var conversation_id = req.body.convers_id;
    var flag = req.body.flag;

    var arrMedia = [];
    var arrApp = [];
    var obj_all = {};
    var subpath = '';

    if(flag == 1){
        subpath = 'jazechat/conversation';
    }else if(flag == 2){
        subpath = 'jazechat/groups';
    }else if(flag == 4){
        subpath = 'jazefeed';
    }



    var rootPath = path.join(G_STATIC_IMG_PATH+subpath+'/'+conversation_id);

    var arrParams = {account_id: account_id,api_token: api_token};
    HelperGeneral.checkGeneralParametersApiToken(arrParams, function(queParam){
        if(queParam.length)
        {
            HelperRestriction.checkAccountCredentials(arrParams, function(queCred){
                if(queCred.length)
                {

                    fs.readdir(rootPath, function (err, rootp) {

                        if (err)
                        {

                            jsonRes = {
                                success: success,
                                result: {},
                                successMessage: successMessage,
                                errorMessage: "No result found."
                                };

                            res.status(statRes).json(jsonRes);
                            res.end();
                        }
                        else
                        {

                            var appParams = {item:rootp, conversation_id:conversation_id,flag:flag};

                            HelperGeneral.getApplications(appParams, function(appRes){

                                if(appRes != null){
                                    arrApp = appRes;
                                }

                                var medParams = {item:rootp, conversation_id:conversation_id,flag:flag};

                                HelperGeneral.getMedia(medParams, function(medRes){


                                    if(medRes != null){
                                        arrMedia = medRes;
                                    }

                                    obj_all = {images:arrMedia,applications:arrApp};
                                    var main_obj = {};
                                    if(flag == 1){
                                        main_obj = {conversation_media_list: obj_all};
                                    }else{
                                        main_obj = {media_list: obj_all};
                                    }

                                    jsonRes = {
                                        success: '1',
                                        result: main_obj,
                                        successMessage: 'File List',
                                        errorMessage: errorMessage
                                    };
                                    res.status(201).json(jsonRes);
                                    res.end();


                                });

                            });
                        }

                    });

                }
                else
                {
                    jsonRes = {
                        success: success,
                        result: {},
                        successMessage: successMessage,
                        errorMessage: "Account not found."
                    };

                    res.status(statRes).json(jsonRes);
                    res.end();

                }
            });
        }
        else
        {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };

            res.status(statRes).json(jsonRes);
            res.end();
        }

    });

});


router.post('/general/media_forward', [

  check('api_token').isAlphanumeric(),
  check('account_id').isNumeric(),
  check('convers_id').isNumeric(),
  check('file_url').isLength({ min: 1 }),
  check('flag').isNumeric()

], (req, res, next) => {

  var _send = res.send;
  var sent = false;

  res.send = function (data) {
    if (sent) return;
    _send.bind(res)(data);
    sent = true;
  };

  var errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      success: success,
      result: {},
      successMessage: successMessage,
      errorMessage: "No mandatory fileds entered."
    });
  }

  var api_token = req.body.api_token;
  var account_id = req.body.account_id;
  var convers_id = req.body.convers_id;
  var file_url = req.body.file_url;
  var flag = req.body.flag;

  var arrParams = {
    account_id: req.body.account_id,
    api_token: req.body.api_token
  };

  HelperGeneral.checkGeneralParametersApiToken(arrParams, function (queParam) {
    if (queParam.length) {

      var parsed = url.parse(file_url);
      var file_name = path.basename(parsed.pathname);

      var file_ext = file_name.split(".");

      var file_type = mime.lookup(file_url).split("/");

      if(flag == 1 ){
          var main_folder = G_STATIC_IMG_PATH+"jazechat/conversation/"+convers_id+"/";
      }

      //create the main folder first
      if (!fs.existsSync(main_folder)){
            fs.mkdirSync(main_folder,'0757', true);
      }

      var target = G_STATIC_IMG_PATH+"jazechat/conversation/"+convers_id+"/"+file_type[0]+"/";

      var options = {
          directory: target,
          filename: file_name
      }

      download(file_url, options, function(err){

          if (!err){

              HelperGeneral.getFileTypes('.'+file_ext[1], function(reType){
                  var results = {
                      file_name:file_name,
                      file_format:file_type[0],
                      file_type:reType,
                      extention:file_ext[1],
                      file_path:G_STATIC_IMG_URL+"uploads/jazenet/jazechat/conversation/"+convers_id+'/'+file_type[0]+'/'+file_name
                  };

                  jsonRes = {
                      success: '1',
                      result: results,
                successMessage: "File is successfully forwarded.",
                      errorMessage: errorMessage
                  };
                  res.status(201).json(jsonRes);
                  res.end();
              });

          }else{

          jsonRes = {
            success: success,
            result: {},
            successMessage: successMessage,
            errorMessage: "Failed to forward the file. Please try again."
          };

          res.status(statRes).json(jsonRes);
          res.end();
          }
      });
    }
    else
    {
      jsonRes = {
        success: success,
        result: {},
        successMessage: successMessage,
        errorMessage: "Device not found."
      };

      res.status(statRes).json(jsonRes);
      res.end();
    }

  });
});


router.post('/general/removeDocument', [

  check('api_token').isAlphanumeric(),
  check('account_id').isNumeric(),
  check('file_url').isLength({ min: 1 })

], (req, res, next) => {

  var _send = res.send;
  var sent = false;

  res.send = function (data) {
    if (sent) return;
    _send.bind(res)(data);
    sent = true;
  };

  var errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      success: success,
      result: {},
      successMessage: successMessage,
      errorMessage: "No mandatory fileds entered."
    });
  }

  var api_token = req.body.api_token;
  var account_id = req.body.account_id;
  var file_url = req.body.file_url;


  var arrParams = {
    api_token:api_token,
    account_id:account_id,
    file_url:file_url
  };


  var source = '/home/chaaby/subdomain/img/images/master.resources/uploads';

  var trimed = file_url.substring(file_url.indexOf("uploads") + 7);

  var sourcPath = source+trimed;

  HelperGeneral.checkGeneralParametersApiToken(arrParams, function(queParam){
    if(queParam.length)
    {
      HelperRestriction.checkAccountCredentials(arrParams, function(queCred){
        if(queCred.length)
        {
          fs.unlink(sourcPath, function (err) {
              if (err){

                jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "File not found."
                };

                res.status(statRes).json(jsonRes);
              res.end();

              }else{

                      jsonRes = {
                        success: '1',
                        result: results,
                  successMessage: "File is successfully deleted.",
                        errorMessage: errorMessage
                    };
                    res.status(201).json(jsonRes);
                    res.end();
              }

          });
        }
        else
        {
          jsonRes = {
            success: success,
            result: {},
            successMessage: successMessage,
            errorMessage: "Account not found."
            };

            res.status(statRes).json(jsonRes);
          res.end();
        }

      });
    }
    else
    {
      jsonRes = {
        success: success,
        result: {},
        successMessage: successMessage,
        errorMessage: "Device not found."
        };

        res.status(statRes).json(jsonRes);
      res.end();
    }

  });

});




// ------------------------------------------------------------------------------- General Pushnotification Module Start  --------------------------------------------------------------------------------------------//

    router.post('/general/sendPushNotification', [

            check('api_token').isAlphanumeric(),
            check('account_id').isNumeric(),
            check('request_account_id').isLength({ min: 1 }),
            check('group_id').isLength({ min: 1 }),
            check('flag').isNumeric()

        ], (req, res, next) => {

            var _send = res.send;
            var sent = false;

            res.send = function (data) {
                if (sent) return;
                _send.bind(res)(data);
                sent = true;
            };

            var errors = validationResult(req.body);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    success: success,
                    result: {},
                    successMessage: successMessage,
                    errorMessage: "No mandatory fileds entered."
                });
            }

            var reqParams = {
                api_token	         :req.body.api_token,
                account_id	         :req.body.account_id,
                request_account_id	 :req.body.request_account_id,
                group_id             :req.body.group_id,
                message              :req.body.message,
                flag                 :req.body.flag
            };

            HelperGeneral.checkGeneralParametersApiToken(reqParams, function (queParamRes) {

                if (queParamRes.length) {

                        HelperNotification.sendPushNotification(reqParams, function(returnNoti){

                            if(parseInt(returnNoti) === 1)
                            {
                                jsonRes = {
                                    success: '1',
                                    result: {},
                                    successMessage: "notification successfully sent.",
                                    errorMessage: errorMessage
                                };
                                res.status(201).json(jsonRes);
                                res.end();
                            }
                            else
                            {
                                jsonRes = {
                                    success: success,
                                    result: {},
                                    successMessage: successMessage,
                                    errorMessage: "unable to complete the process please try later."
                                };

                                res.status(statRes).json(jsonRes);
                                res.end();
                            }
                        });
                }
                else {
                    jsonRes = {
                        success: success,
                        result: {},
                        successMessage: successMessage,
                        errorMessage: "Device not found."
                    };

                    res.status(statRes).json(jsonRes);
                    res.end();
                }
        });
    });


//------------------------------------------------------------------------------- General Pushnotification Module End  --------------------------------------------------------------------------------------------//

// ------------------------------------------------------------------------------- General Preload Module Start  --------------------------------------------------------------------------------------------//
router.post('/general/get_preload_details', [

    check('api_token').isAlphanumeric(),
    check('flag').isNumeric()

], (req, res, next) => {

    var _send = res.send;
    var sent = false;

    res.send = function (data) {
        if (sent) return;
        _send.bind(res)(data);
        sent = true;
    };

    var errors = validationResult(req.body);
    if (!errors.isEmpty()) {
        return res.status(422).json({
            success: success,
            result: {},
            successMessage: successMessage,
            errorMessage: "No mandatory fileds entered."
        });
    }

    var reqParams = {
        api_token	         :req.body.api_token,
        account_id	         :"0",
        flag                 :req.body.flag
    };

    HelperGeneral.checkGeneralParametersApiToken(reqParams, function (queParamRes) {

        if (queParamRes.length) {

            var banners_list = [];

            var login_type = [];

            var welcome_note = {};


            if(parseInt(reqParams.flag) === 1)
            {
                //jazecom login
                banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/preload_configuration/mobile/jazecom_login_banners/call.png");
                banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/preload_configuration/mobile/jazecom_login_banners/chat.png");
                banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/preload_configuration/mobile/jazecom_login_banners/diary.png");
                banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/preload_configuration/mobile/jazecom_login_banners/feed.png");
                banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/preload_configuration/mobile/jazecom_login_banners/mail.png");
            }
            else if(parseInt(reqParams.flag) === 2)
            {
                //jazenet login
                banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/preload_configuration/mobile/jazecom_login_banners/call.png");
                banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/preload_configuration/mobile/jazecom_login_banners/chat.png");
                banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/preload_configuration/mobile/jazecom_login_banners/diary.png");
                banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/preload_configuration/mobile/jazecom_login_banners/feed.png");
                banners_list.push(G_STATIC_IMG_URL+"uploads/jazenet/preload_configuration/mobile/jazecom_login_banners/mail.png");
            }

            var configuration = {
                banners : banners_list,
                login_type:login_type,
                welcome_note: welcome_note
            }

            jsonRes = {
                success: '1',
                result: {configuration_details:configuration},
                successMessage: "Preload Details.",
                errorMessage: errorMessage
            };
            res.status(201).json(jsonRes);
            res.end();
        }
        else {
            jsonRes = {
                success: success,
                result: {},
                successMessage: successMessage,
                errorMessage: "Device not found."
            };

            res.status(statRes).json(jsonRes);
            res.end();
        }
    });
});
// ------------------------------------------------------------------------------- General Preload Module End  --------------------------------------------------------------------------------------------//


//------------------------------------------------------- Private Functions -----------------------------------------------------------//



function getExtension(path) {
    var basename = path.split(/[\\/]/).pop(),  // extract file name from full path ...
                                               // (supports `\\` and `/` separators)
        pos = basename.lastIndexOf(".");       // get last position of `.`

    if (basename === "" || pos < 1)            // if file name is empty or ...
        return "";                             //  `.` not found (-1) or comes first (0)

    return basename.slice(pos + 1);            // extract extension ignoring `.`
}


function getFilename(path) {
    var basename = path.split(/[\\/]/).pop(),
        pos = basename.split('.');
    if (basename === "" || pos.length < 1)
        return "";
    return pos[0];
}


function getCleanFileName(path) {

  var basename = path.split(/\.(?=[^\.]+$)/);
  var cleanString = basename[0].replace(/[|&;$%@"<>()+1234567890,]/g, "");

    return cleanString;
}

function getTheLastDot(path){

  var basename = path.split(/\.(?=[^\.]+$)/);
  return basename;
}



function sort_name(array,keyword)
{
return array.filter(o => o.name.toLowerCase().includes(keyword.toLowerCase()))
                  .sort((a, b) => a.name.toLowerCase().indexOf(keyword.toLowerCase()) - b.name.toLowerCase().indexOf(keyword.toLowerCase()));
}

module.exports = router;
