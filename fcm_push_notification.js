'use strict';
const methods = {};

const express = require('express');
const router = express.Router();
// const app = express();

var admin = require('firebase');

var firestore = admin.firestore();

 const fs_notification = firestore.collection('jaze_chat_notification');

 const HelperNotification = require('./jazecom/helpers/HelperNotification.js');
 const HelperJazechat = require('./jazecom/helpers/HelperJazechat.js');


//---------------------------------------------------------------- Realtime Push Notification Module Start--------------------------------------------------------//

let pushserver = fs_notification.onSnapshot(querySnapshot => {
    querySnapshot.docChanges().forEach(change => {

		if (parseInt(Object.keys(change.doc.data()).length) !== 0) {
		
			  if (change.type === 'modified' || change.type === 'added') {

				var convers_data = change.doc.data();

				var objkeys = Object.keys(convers_data);

				objkeys.forEach(function(row) {
					
					var objValues = convers_data[row].toString().split("_");

					var notiParams = {		
								group_id	:	change.doc.id,
								account_id	: objValues[0],
								account_type: objValues[1],
								request_account_id	: objValues[2],
								request_account_type: objValues[3],
								msg_type: objValues[4],
								msg: objValues[5],
								convers_flag: objValues[6],
								flag:4
						};

						if(parseInt(notiParams.convers_flag) === 3)
						{
							//Broadcast
							let writeBatch = firestore.batch();
							writeBatch.delete(firestore.doc('jaze_chat_notification/'+change.doc.id));
							writeBatch.commit();	
						}
						else
						{
							//console.log("---------------------- Push Notification "+change.doc.id+" Start ----------------------");

							HelperNotification.sendPushNotification(notiParams, function(return_result) {

								if(parseInt(return_result) === 1)
								{
									HelperJazechat.updateConversationChatDate(notiParams.request_account_id,change.doc.id, row,function (return_date) {
									
										if(parseInt(return_date) !== 0)
										{
											//delete entry							
											let writeBatch = firestore.batch();
											writeBatch.delete(firestore.doc('jaze_chat_notification/'+change.doc.id));
											writeBatch.commit();	
											//console.log("----------------------  Push Notification "+change.doc.id+" End ---------------------- ");
										}			 
									});	
								}
								else
								{console.log("error push");}						   
							});	
						}
				});
			  }
		 }
    });
  }, err => {
	console.log(`Listener failed: ${err}`);
	setTimeout(pushserver, 5000);
  });
//---------------------------------------------------------------- Realtime Push Notification Module End--------------------------------------------------------//





router.post('/check/checkFn', function(req, res) {

	const moment = require("moment");
	const dateFormat = require('dateformat');


	const currentTimeInIndia = moment().utc();

// //	const newdateForm = dateFormat(new Date(currentTimeInIndia), 'yyyy-mm-dd HH:MM:ss');

	const newdateForm = moment().utc().format('YYYY-MM-DD hh:mm:ss');

// 	console.log(currentTimeInIndia);


// 	console.log(dateFormat(new Date(newdateForm), 'yyyy-mm-dd HH:MM:ss'));


	var newdate = new Date(newdateForm);
	newdate.setDate(newdate.getDate() + 3);
	const datefull = dateFormat(newdate, 'yyyy-mm-dd HH:MM:ss');


	console.log(datefull);


	// 	var notiParams = {		
	// 							group_id:'1',
	// 							account_id: '100',
	// 							account_type: '1',
	// 							request_account_id: '100',
	// 							request_account_type: '3',
	// 							message: 'test message',
	// 							message_type: '1',
	// 							flag:4
	// 					};

	// 					HelperNotification.chekMuteStatus(notiParams, function(account_results) {

	// 	 	console.log(account_results);
			
	// });



				
});







module.exports = router;